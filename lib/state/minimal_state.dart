import 'package:hive_flutter/adapters.dart';

import '/data/base_notification.dart';
import '/data/hive_icon.dart';
import '/data/logger.dart';
import '/data/notifications/todo_notification.dart';
import '/data/todo.dart';
import '/data/todo_list.dart';
import '/util/adapters/tz_date_time.dart';

import 'todo_db.dart';
import 'todo_list_db.dart';
import 'secure_storage.dart';
import 'recently_deleted_db.dart';

Future<void> initMinimalHive({
  bool loadTodoAdapters = true,
  bool loadLoggerAdapters = true,
}) async {
  await Hive.initFlutter();

  if (loadTodoAdapters) {
    final adapters = <TypeAdapter>[
      TimeOfDayAdapter(),
      ColorAdapter(),
      TodoAdapter(),
      HiveIconAdapter(),
      TodoListAdapter(),
      TimeZoneAdapter(),
      LocationAdapter(),
      TZDateTimeAdapter(),
      NotifIdAdapter(),
      TodoNotifMetaAdapter()
    ];

    for (final adapter in adapters) {
      if (Hive.isAdapterRegistered(adapter.typeId)) continue;
      Hive.registerAdapter(adapter);
    }
  }

  if (loadLoggerAdapters) {
    final adapters = <TypeAdapter>[LogEntryAdapter(), LogLevelAdapter()];

    for (final adapter in adapters) {
      if (Hive.isAdapterRegistered(adapter.typeId)) continue;
      Hive.registerAdapter(adapter);
    }
  }
}

/// Creates a [TodoDatabase] without referencing any [Ref]s.
/// Should only be used when the app is started in the background
/// / without the main app being run.
///
/// *For eg. when reacting to a notification action*
Future<TodoDatabase> initMinimalTodoDb() async {
  final secureStorage = SecureStorageDatabase();
  final recentlyDeleted = RecentlyDeletedDatabase();

  await secureStorage.initStorage();
  await recentlyDeleted.init();

  final todoDb = TodoDatabase(secureStorage, recentlyDeleted);
  await todoDb.init();

  return todoDb;
}

/// Creates a [TodoListDatabase] without referencing any [Ref]s.
/// Should only be used when the app is started in the background
/// / without the main app being run.
///
/// *For eg. when reacting to a notification action*
Future<TodoListDatabase> initMinimalListDb() async {
  final secureStorage = SecureStorageDatabase();
  final recentlyDeleted = RecentlyDeletedDatabase();

  await secureStorage.initStorage();
  await recentlyDeleted.init();

  final listDb = TodoListDatabase(secureStorage, recentlyDeleted);
  await listDb.init();

  return listDb;
}

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:hive_flutter/hive_flutter.dart';

import 'database.dart';

final recentlyDeletedProvider = Provider<RecentlyDeletedDatabase>(
  (ref) => RecentlyDeletedDatabase(),
);

class RecentlyDeletedDatabase implements DatabaseWithNoId<String> {
  Box<String>? _box;

  @override
  Future<void> init() async {
    _box ??= await Hive.openBox<String>('recentlyDeleted');
  }

  @override
  Future<void> store(String value) => _box!.put(value, value);

  @override
  Future<void> storeAll(Iterable<String> values) => _box!.putAll(
        {for (var id in values) id: id},
      );

  @override
  Future<void> delete(String value) => _box!.delete(value);

  @override
  Future<void> deleteAll(Iterable<String> values) => _box!.deleteAll(values);

  Future<void> clear() => _box!.clear();

  bool contains(String id) => _box!.containsKey(id);

  @override
  Iterable<String> values([int? take]) {
    if (take == null) return _box!.values;
    return _box!.values.take(take);
  }
}

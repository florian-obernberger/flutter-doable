// ignore_for_file: deprecated_member_use_from_same_package

import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:notified_preferences/notified_preferences.dart';
import 'package:result/anyhow.dart';

import '/data/doable_theme_mode.dart';
import '/data/notifications/todo_notification.dart';
import '/data/sorting/todo_sorter.dart';
import '/remote/remote.dart';
import '/theme/text_theme.dart';
import '/types/app_language.dart';
import '/types/date_and_time_formats.dart';
import '/types/reader_mode.dart';
import '/types/sortable_element.dart';
import '/types/swipe_action.dart';
import '/types/version.dart';
import '/types/weekday.dart';
import '/widgets/pride/pride_month.dart';
import '/util/color_string.dart';
import '/util/duration_parser.dart';
import '/util/extensions/color_scheme.dart';

import 'settings/notification_settings.dart';

part 'settings/settings_provider.dart';
part 'settings/settings_widget.dart';

late final List<String> privateSettings;

typedef SettingsData = Map<String, dynamic>;

extension _EnumFromStringExtension on String {
  T parse<T extends Enum>(List<T> values) => values.asNameMap()[this]!;
}

extension _EnumListFromStringList on List<String> {
  List<T> parse<T extends Enum>(List<T> values) {
    final names = values.asNameMap();
    return map((value) => names[value]!).toList();
  }
}

extension NotifiedSettings on NotifiedPreferences {
  /// Creates an Enum List Setting.
  /// The enum values are stored and read as by their `name` property.
  @protected
  PreferenceNotifier<List<T>> createEnumListSetting<T extends Enum>({
    required String key,
    required List<T> initialValue,
    required List<T> values,
  }) =>
      // ignore: invalid_use_of_protected_member
      createSetting(
        key: key,
        initialValue: initialValue,
        read: (prefs, key) => prefs.getStringList(key)?.parse(values),
        write: (prefs, key, value) => prefs.setStringList(
          key,
          value.map((action) => action.name).toList(),
        ),
      );
}

abstract mixin class ImportInterface {
  /// Tries to import the given key-value pair. Returns weather or not the value
  /// could be imported.
  bool import(String key, Object? value);

  bool importEnumList<T extends Enum>(
    PreferenceNotifier<List<T>> notifier,
    List toImport, {
    required List<T> values,
  }) {
    switch (toImport) {
      case List<T>():
        notifier.value = toImport;
        return true;
      case List<String>():
        final nameMap = values.asNameMap();
        final imported = toImport
            .map((name) => nameMap[name])
            .where((e) => e != null)
            .cast<T>()
            .toList();

        if (imported.length != toImport.length) return false;
        notifier.value = imported;
        return true;
      case List<int>():
        final indexMap = values.asMap();
        final imported = toImport
            .map((idx) => indexMap[idx])
            .where((e) => e != null)
            .cast<T>()
            .toList();

        if (imported.length != toImport.length) return false;
        notifier.value = imported;
        return true;
      default:
        return false;
    }
  }
}

class Settings with NotifiedPreferences {
  static Settings of(BuildContext context) => SettingsProvider.of(context);

  static Settings? maybeOf(BuildContext context) =>
      SettingsProvider.maybeOf(context);

  @override
  Future<void> initialize([FutureOr<SharedPreferences>? preferences]) async {
    await super.initialize();

    _preferences = await SharedPreferences.getInstance();

    miscellaneous = MiscellaneousPreferences();
    updates = UpdatesPreferences();
    extensions = ExtensionsPreferences();
    general = GeneralPreferences();
    dateTime = DateTimePreferences();
    sorting = SortingPreferences();
    design = DesignPreferences();
    accessability = AccessabilityPreferences();
    fediverse = FediversePreferences();
    backup = BackupPreferences();

    await Future.wait([
      miscellaneous.initialize(preferences),
      updates.initialize(preferences),
      extensions.initialize(preferences),
      general.initialize(preferences),
      dateTime.initialize(preferences),
      sorting.initialize(preferences),
      design.initialize(preferences),
      accessability.initialize(preferences),
      fediverse.initialize(preferences),
      backup.initialize(preferences),
    ]);
  }

  void initializePrivateSettings() {
    privateSettings = [
      extensions.lastTodoList.key,
      extensions.todoListsOrder.key,
      accessability.enablePrideFlags.key,
      ...sorting.privateKeys(),
      design.customColorSchemes.key,
      design.customAccentColor.key,
      design.customSecondaryAccentColor.key,
      design.customTertiaryAccentColor.key,
      backup.autoExportPath.key,
    ];
  }

  late final SharedPreferences _preferences;

  late final MiscellaneousPreferences miscellaneous;
  late final UpdatesPreferences updates;
  late final ExtensionsPreferences extensions;
  late final GeneralPreferences general;
  late final DateTimePreferences dateTime;
  late final SortingPreferences sorting;
  late final DesignPreferences design;
  late final AccessabilityPreferences accessability;
  late final FediversePreferences fediverse;
  late final BackupPreferences backup;

  SettingsData export() {
    final sharedPrefs = _preferences;

    return Map.fromEntries(sharedPrefs.getKeys().difference({
      miscellaneous.hadIntroduction.key,
      miscellaneous.migratedToRemotes.key,
      ...backup.getKeys(),
      extensions.lastTodoList.key,
      extensions._lastUsedReaderMode.key,
      _lastSync.key,
    }).map((key) => MapEntry(key, sharedPrefs.get(key))));
  }

  Result<()> import(SettingsData settings) {
    final current = export();

    final changes = settings.entries.where((entry) {
      final MapEntry(key: key, value: value) = entry;
      return current[key] != value;
    });

    if (changes.isEmpty) return const Ok(());

    final importers = <ImportInterface>[
      miscellaneous,
      updates,
      extensions,
      general,
      dateTime,
      sorting,
      design,
      accessability,
      fediverse,
    ];

    for (final MapEntry(key: key, value: value) in changes) {
      try {
        for (final importer in importers) {
          if (importer.import(key, value)) break;
        }
      } catch (e) {
        return bail<()>(e).context(
          'Could not import entry ($key, $value) into settings',
        );
      }
    }

    return const Ok(());
  }

  late final _lastSync = createSetting<DateTime?>(
    key: '_lastSettingsSync',
    initialValue: null,
    read: (prefs, key) {
      final value = prefs.getString(key);
      if (value == null) return null;
      return DateTime.parse(value);
    },
    write: (prefs, key, value) => prefs.setStringOrNull(
      key,
      value?.toIso8601String(),
    ),
  );

  FutureResult<()> syncSettings(Remote remote, {bool force = false}) async {
    final localLastModified = DateTime.now();
    final lastSync = _lastSync.value;

    if (lastSync != null &&
        localLastModified
            .subtract(const Duration(minutes: 2))
            .isBefore(lastSync)) {
      // Prevent constant syncing by checking if at least two minutes passed.
      return const Ok(());
    }

    if (force) {
      _lastSync.value = localLastModified;

      return remote.pushSettings({
        'lastModified': localLastModified.toIso8601String(),
        'settings': export(),
      });
    }

    final local = export();

    final cloudRes = await remote.fetchSettings();
    if (cloudRes case Err(error: final err)) return Err(err);

    final cloud = cloudRes.unwrap();

    if (cloud
        case {
          'lastModified': String lastModifiedStr,
          'settings': Map settingsRaw
        }) {
      final settings = settingsRaw.cast<String, dynamic>();
      final lastModified = DateTime.tryParse(lastModifiedStr);

      if (lastModified == null) {
        return bail<()>(lastModifiedStr)
            .context('Could not parse last modified date');
      }

      late final SettingsData? changed;

      if (lastSync == null) {
        changed = local;
      } else if (lastSync.isAfter(lastModified)) {
        // check if there are local changes
        final changes = Map.fromEntries(local.entries.where((entry) {
          return entry.value != settings[entry.key];
        }));

        changed = changes.isEmpty ? null : local;
      } else if (lastSync.isBefore(lastModified)) {
        final imported = import(settings);

        if (imported case Err(error: final err)) return Err(err);
        changed = null;
      } else {
        changed = null;
      }

      _lastSync.value = localLastModified;
      if (changed != null) {
        return remote.pushSettings({
          'lastModified': localLastModified.toIso8601String(),
          'settings': changed,
        });
      }

      return const Ok(());
    } else if (cloud.isEmpty) {
      return syncSettings(remote, force: true);
    }

    return bail<()>(cloud).context('Cloud data was in an invalid format');
  }

  @override
  int get hashCode => export().hashCode;

  @override
  bool operator ==(covariant Settings other) => true;
}

class MiscellaneousPreferences with NotifiedPreferences, ImportInterface {
  late final showCompleted =
      createSetting(key: 'showCompleted', initialValue: false);

  late final hadIntroduction =
      createSetting(key: 'hadIntroduction', initialValue: false);

  late final migratedToRemotes =
      createSetting(key: 'migratedToRemotes', initialValue: false);

  @override
  bool import(String key, Object? value) {
    if (value is! bool) return false;

    if (key == showCompleted.key) {
      showCompleted.value = value;
    } else if (key == hadIntroduction.key) {
      hadIntroduction.value = value;
    } else if (key == migratedToRemotes.key) {
      migratedToRemotes.value = value;
    } else {
      return false;
    }

    return true;
  }
}

class UpdatesPreferences with NotifiedPreferences, ImportInterface {
  PreferenceNotifier<Version?> _createVersionSetting({required String key}) =>
      createSetting<Version?>(
        key: key,
        initialValue: null,
        read: (prefs, key) {
          final source = prefs.getString(key);
          if (source == null) return null;

          switch (Version.fromString(source)) {
            case Err():
              prefs.remove(key);
              return null;
            case Ok(:final value):
              return value;
          }
        },
        write: (prefs, key, value) =>
            prefs.setStringOrNull(key, value?.toString()),
      );

  late final autoUpdates =
      createSetting(key: 'useAutoUpdate', initialValue: true);

  late final updateSkip = _createVersionSetting(key: 'versionUpdateSkip');

  late final showChangelogDialog =
      createSetting(key: 'showChangelogDialog', initialValue: true);

  late final lastChangelogVersion =
      _createVersionSetting(key: 'lastChangelogVersion');

  @override
  bool import(String key, Object? value) {
    if (key == autoUpdates.key && value is bool) {
      autoUpdates.value = value;
    } else if (key == showChangelogDialog.key && value is bool) {
      showChangelogDialog.value = value;
    } else {
      return false;
    }

    return true;
  }
}

class ExtensionsPreferences with NotifiedPreferences, ImportInterface {
  late final enableTodoSearch =
      createSetting(key: 'enableTodoSearch', initialValue: false);

  late final enableMarkdown =
      createSetting(key: 'useMarkdown', initialValue: false);

  late final enableLists =
      createSetting(key: 'enableTodoLists', initialValue: false);

  late final enableRecurringTasks =
      createSetting(key: 'enableRecurringTasks', initialValue: false);

  late final enableImages =
      createSetting(key: 'enableImages', initialValue: false);

  late final enableReaderModer =
      createSetting(key: 'enableReaderModer', initialValue: false);

  late final enableNotifications =
      createSetting(key: 'useNotifications', initialValue: false);

  late final enableSwipeActions =
      createSetting(key: 'enableSwipeActions', initialValue: false);

  late final enableProgressBar =
      createSetting(key: 'enableProgressBar', initialValue: false);

  late final enableDateFilters =
      createSetting(key: 'enableDateFilters', initialValue: false);

  //! Todo Lists

  late final lastTodoList =
      createSetting<String?>(key: 'lastTodoList', initialValue: null);

  late final useListColor =
      createSetting(key: 'useListColor', initialValue: true);

  late final todoListsOrder =
      createSetting<List<String>?>(key: 'todoListsOrder', initialValue: null);

  late final hadListVisibilityIntroduction = createSetting<bool>(
      key: 'hadListVisibilityIntroduction', initialValue: false);

  //! Reader Mode

  late final readerMode = createEnumSetting(
    key: 'readerMode',
    initialValue: ReaderMode.saved,
    values: ReaderMode.values,
  );

  late final _lastUsedReaderMode =
      createSetting(key: 'lastUsedReaderMode', initialValue: false);

  ReaderMode get currentReaderMode {
    if (!enableReaderModer.value) return ReaderMode.edit;

    return switch (readerMode.value) {
      ReaderMode.saved =>
        _lastUsedReaderMode.value ? ReaderMode.reader : ReaderMode.edit,
      ReaderMode.edit => ReaderMode.edit,
      ReaderMode.reader => ReaderMode.reader,
    };
  }

  set currentReaderMode(ReaderMode mode) =>
      _lastUsedReaderMode.value = mode.isReader ?? _lastUsedReaderMode.value;

  late final alwaysOpenNotificationsInReaderMode = createSetting<bool>(
    key: 'alwaysOpenNotificationsInReaderMode',
    initialValue: false,
  );

  //! Notifications

  late final defaultNotifications = createJsonSetting<TodoNotificationSettings>(
    key: 'defaultNotifications',
    fromJson: (json) => TodoNotificationSettings.fromJson(
      (json as Map).cast<String, dynamic>(),
    ),
    initialValue: TodoNotificationSettings.fromGeneral([
      TodoDuration.atTimeOfEvent,
    ]),
  );

  late final defaultNotificationTime = createSetting<TimeOfDay>(
    key: 'defaultNotificationTime',
    initialValue: const TimeOfDay(hour: 16, minute: 00),
    read: (prefs, key) {
      final totalMinutes = prefs.getInt(key);
      if (totalMinutes == null) return null;

      return TimeOfDay(hour: totalMinutes ~/ 60, minute: totalMinutes % 60);
    },
    write: (prefs, key, value) => prefs.setInt(
      key,
      value.hour * 60 + value.minute,
    ),
  );

  late final notificationSnoozeDuration = createSetting<Duration>(
    key: 'notificationSnoozeDuration',
    initialValue: const Duration(hours: 1),
    read: (prefs, key) {
      final value = prefs.getString(key);
      if (value == null) return null;
      return parseDuration(value);
    },
    write: (prefs, key, value) => prefs.setStringOrNull(key, value.toString()),
  );

  // static const _notificationStringsFallback = TodoNotifStrings(
  //   channelName: 'Todos',
  //   markCompleted: 'Mark completed',
  //   snooze: 'Snooze',
  // );

  // late final notificationStrings = createJsonSetting<TodoNotifStrings>(
  //     key: 'notificationStrings',
  //     initialValue: _notificationStringsFallback,
  //     fromJson: (json) {
  //       if (json is! Map) return _notificationStringsFallback;
  //       final value = json.cast<String, String>();

  //       return TodoNotifStrings.fromMap(value);
  //     });

  late final enableNotificationSound = createSetting(
    key: 'enableNotificationSound',
    initialValue: true,
  );

  //! Swipe actions

  late final selectedSwipeActions = createEnumListSetting<SwipeAction>(
    key: 'selectedSwipeActions',
    initialValue: const [SwipeAction.star, SwipeAction.delete],
    values: SwipeAction.values,
  );

  late final hideCheckWhenCompleteSelected = createSetting<bool>(
      key: 'hideCheckWhenCompleteSelected', initialValue: true);

  @override
  bool import(String key, Object? value) {
    if (key == enableTodoSearch.key && value is bool) {
      enableTodoSearch.value = value;
    } else if (key == enableMarkdown.key && value is bool) {
      enableMarkdown.value = value;
    } else if (key == enableLists.key && value is bool) {
      enableLists.value = value;
    } else if (key == enableRecurringTasks.key && value is bool) {
      enableRecurringTasks.value = value;
    } else if (key == enableImages.key && value is bool) {
      enableImages.value = value;
    } else if (key == enableReaderModer.key && value is bool) {
      enableReaderModer.value = value;
    } else if (key == enableNotifications.key && value is bool) {
      enableNotifications.value = value;
    } else if (key == enableSwipeActions.key && value is bool) {
      enableSwipeActions.value = value;
    } else if (key == lastTodoList.key && value is String?) {
      lastTodoList.value = value;
    } else if (key == useListColor.key && value is bool) {
      useListColor.value = value;
    } else if (key == todoListsOrder.key && value is List<dynamic>) {
      todoListsOrder.value = value.cast<String>();
    } else if (key == hadListVisibilityIntroduction.key && value is bool) {
      hadListVisibilityIntroduction.value = value;
    } else if (key == readerMode.key && value is String) {
      readerMode.value = value.parse(ReaderMode.values);
    } else if (key == defaultNotifications.key && value is String) {
      defaultNotifications.value = TodoNotificationSettings.fromJson(
        jsonDecode(value),
      );
    } else if (key == defaultNotificationTime.key && value is int) {
      defaultNotificationTime.value =
          TimeOfDay(hour: value ~/ 60, minute: value % 60);
    } else if (key == enableNotificationSound.key && value is bool) {
      enableNotificationSound.value = value;
    } else if (key == selectedSwipeActions.key && value is List<dynamic>) {
      selectedSwipeActions.value =
          value.cast<String>().parse(SwipeAction.values);
    } else if (key == hideCheckWhenCompleteSelected.key && value is bool) {
      hideCheckWhenCompleteSelected.value = value;
    } else {
      return false;
    }

    return true;
  }
}

class GeneralPreferences with NotifiedPreferences, ImportInterface {
  late final appLanguage = createEnumSetting(
    key: 'language',
    initialValue: AppLanguage.system,
    values: AppLanguage.values,
  );

  late final inAppBrowser =
      createSetting(key: 'useCustomTabs', initialValue: true);

  late final sharedTextAsTitle =
      createSetting(key: 'sharedTextIsTitle', initialValue: false);

  late final deleteCompletedAfter = createSetting<Duration?>(
    key: 'deleteCompletedAfter',
    initialValue: null,
    read: (prefs, key) {
      final value = prefs.getString(key);
      if (value == null) return null;
      return parseDuration(value);
    },
    write: (prefs, key, value) => prefs.setStringOrNull(key, value?.toString()),
  );

  late final closeDetailsOnComplete =
      createSetting(key: 'closeDetailOnTodoCompletion', initialValue: false);

  late final trimTodoTexts =
      createSetting(key: 'trimTodoTexts', initialValue: true);

  late final highlightToday =
      createSetting(key: 'highlightToday', initialValue: true);

  late final highlightOverdue =
      createSetting(key: 'highlightOverdue', initialValue: true);

  late final checkMarkRight =
      createSetting(key: 'alignTodoCheckRight', initialValue: true);

  late final startOfTheWeek = createSetting<Weekday?>(
    key: 'startOfTheWeek',
    initialValue: null,
    read: (prefs, key) {
      final position = prefs.getInt(key);
      if (position == null) return null;
      return Weekday.fromIndex(position);
    },
    write: (prefs, key, value) => prefs.setIntOrNull(key, value?.index),
  );

  late final expandTodosByDefault =
      createSetting(key: 'expandTodosByDefault', initialValue: false);

  @override
  bool import(String key, Object? value) {
    if (key == appLanguage.key && value is String) {
      appLanguage.value = value.parse(AppLanguage.values);
    } else if (key == inAppBrowser.key && value is bool) {
      inAppBrowser.value = value;
    } else if (key == sharedTextAsTitle.key && value is bool) {
      sharedTextAsTitle.value = value;
    } else if (key == deleteCompletedAfter.key && value is String?) {
      deleteCompletedAfter.value = value == null ? null : parseDuration(value);
    } else if (key == closeDetailsOnComplete.key && value is bool) {
      closeDetailsOnComplete.value = value;
    } else if (key == trimTodoTexts.key && value is bool) {
      trimTodoTexts.value = value;
    } else if (key == highlightToday.key && value is bool) {
      highlightToday.value = value;
    } else if (key == highlightOverdue.key && value is bool) {
      highlightOverdue.value = value;
    } else if (key == checkMarkRight.key && value is bool) {
      checkMarkRight.value = value;
    } else if (key == startOfTheWeek.key && value is int) {
      startOfTheWeek.value = Weekday.fromIndex(value);
    } else if (key == expandTodosByDefault.key && value is bool) {
      expandTodosByDefault.value = value;
    } else {
      return false;
    }

    return true;
  }
}

class DateTimePreferences with NotifiedPreferences, ImportInterface {
  late final dateFormat = createEnumSetting(
    key: 'dateFormat',
    initialValue: DateFormat.european,
    values: DateFormat.values,
  );

  late final timeFormat = createEnumSetting(
    key: 'timeFormat',
    initialValue: TimeFormat.european,
    values: TimeFormat.values,
  );

  late final dateTimeSeparator =
      createSetting(key: 'dateTimeSeparator', initialValue: ' ');

  late final use24 = createSetting(key: 'use24', initialValue: true);

  void addListener(VoidCallback listener) {
    dateFormat.addListener(listener);
    timeFormat.addListener(listener);
    dateTimeSeparator.addListener(listener);
    use24.addListener(listener);
  }

  void removeListener(VoidCallback listener) {
    dateFormat.removeListener(listener);
    timeFormat.removeListener(listener);
    dateTimeSeparator.removeListener(listener);
    use24.removeListener(listener);
  }

  @override
  bool import(String key, Object? value) {
    if (key == dateFormat.key && value is String) {
      dateFormat.value = value.parse(DateFormat.values);
    } else if (key == timeFormat.key && value is String) {
      timeFormat.value = value.parse(TimeFormat.values);
    } else if (key == dateTimeSeparator.key && value is String) {
      dateTimeSeparator.value = value;
    } else if (key == use24.key && value is bool) {
      use24.value = value;
    } else {
      return false;
    }

    return true;
  }
}

@Deprecated('Use [SortingPreferences] instead')
class SortOrderPreferences with NotifiedPreferences, ImportInterface {
  @Deprecated('Use [SortingPreferences] instead')
  late final order = createSetting<List<SortableElement>>(
    key: 'sortingMode',
    initialValue: SortableElement.values,
    read: (prefs, key) {
      final value = prefs.getString(key);
      if (value == null) return null;
      return SortableElement.fromString(value);
    },
    write: (prefs, key, value) => prefs.setString(
      key,
      value.map((v) => v.index).join(),
    ),
  );

  @Deprecated('Use [SortingPreferences] instead')
  late final newestFirst =
      createSetting(key: 'sortNewestFirst', initialValue: true);

  @Deprecated('Use [SortingPreferences] instead')
  late final reflectChanges =
      createSetting(key: 'moveModifiedUp', initialValue: true);

  @override
  bool import(String key, Object? value) {
    if (key == order.key && value is List<dynamic>) {
      order.value = value.cast<String>().parse(SortableElement.values);
    } else if (key == newestFirst.key && value is bool) {
      newestFirst.value = value;
    } else if (key == reflectChanges.key && value is bool) {
      reflectChanges.value = value;
    } else {
      return false;
    }

    return true;
  }

  static const keys = {'sortingMode', 'sortNewestFirst', 'moveModifiedUp'};

  static Future<void> delete() async {
    final sharedPrefs = await SharedPreferences.getInstance();

    await Future.wait(keys.map(sharedPrefs.remove));
  }

  static Future<bool> isDeleted() async {
    final sharedPrefs = await SharedPreferences.getInstance();
    return !keys.any(sharedPrefs.containsKey);
  }
}

class SortingPreferences with NotifiedPreferences, ImportInterface {
  late final groupBy = createEnumListSetting<SortingGroup>(
    key: 'groupBy',
    initialValue: SortingGroup.values,
    values: SortingGroup.values,
  );

  late final sortCompleted = createSetting<bool>(
    key: 'sortCompleted',
    initialValue: false,
  );

  late final sortBy = createSetting<List<SortingMetric>>(
    key: 'sortingBy',
    initialValue: SortingMetric.values,
    read: (prefs, key) {
      final values = prefs.getStringList(key);
      if (values == null) return null;

      return values
          .map((value) => value.split(':'))
          .map((parts) => (
                parts.first.parse(SortingAttr.values),
                parts.last.parse(SortOrder.values)
              ))
          .map(SortingMetric.from)
          .toList();
    },
    write: (prefs, key, values) => prefs.setStringList(
      key,
      values
          .map((metric) => '${metric.attribute.name}:${metric.order.name}')
          .toList(),
    ),
  );

  late final hideHelpInformation = createSetting<bool>(
    key: 'sortingHideHelpInformation',
    initialValue: false,
  );

  Set<String> privateKeys() => {hideHelpInformation.key};

  @override
  bool import(String key, Object? value) {
    if (key == groupBy.key && value is List) {
      return importEnumList<SortingGroup>(
        groupBy,
        value,
        values: SortingGroup.values,
      );
    }

    if (key == sortBy.key && value is List) {
      switch (value) {
        case List<SortingMetric>():
          sortBy.value = value;
        case List<String>():
          sortBy.value = value
              .map((value) => value.split(':'))
              .map((parts) => (
                    parts.first.parse(SortingAttr.values),
                    parts.last.parse(SortOrder.values)
                  ))
              .map(SortingMetric.from)
              .toList();
        default:
          return false;
      }

      return true;
    }

    return false;
  }
}

class DesignPreferences with NotifiedPreferences, ImportInterface {
  late final fontPairing = createEnumSetting(
    key: 'fontPairing',
    initialValue: FontPairing.doable,
    values: FontPairing.values,
  );

  FontTheme get primaryFontTheme =>
      useSystemFont.value ? FontTheme.systemFont : fontPairing.value.primary;
  FontTheme get accentFontTheme => fontPairing.value.accent;
  FontTheme get textAccentFontTheme =>
      accentFontTheme.type.isTextAccent ? accentFontTheme : primaryFontTheme;
  FontTheme get monoFontTheme => fontPairing.value.mono;

  late final useSystemFont = createSetting<bool>(
    key: 'useSystemFont',
    initialValue: false,
  );

  late final dynamicColors =
      createSetting(key: 'useSystemColor', initialValue: true);

  late final customAccentColor = createSetting<Color?>(
    key: 'customColorScheme',
    initialValue: null,
    read: (prefs, key) {
      final value = prefs.getString(key);
      if (value == null) return null;
      return loadColor(value);
    },
    write: (prefs, key, value) {
      final color = value == null ? null : saveColor(value);
      prefs.setStringOrNull(key, color);
    },
  );

  late final customSecondaryAccentColor = createSetting<Color?>(
    key: 'customSecondaryAccentColor',
    initialValue: null,
    read: (prefs, key) {
      final value = prefs.getString(key);
      if (value == null) return null;
      return loadColor(value);
    },
    write: (prefs, key, value) {
      final color = value == null ? null : saveColor(value);
      prefs.setStringOrNull(key, color);
    },
  );

  late final customTertiaryAccentColor = createSetting<Color?>(
    key: 'customTertiaryAccentColor',
    initialValue: null,
    read: (prefs, key) {
      final value = prefs.getString(key);
      if (value == null) return null;
      return loadColor(value);
    },
    write: (prefs, key, value) {
      final color = value == null ? null : saveColor(value);
      prefs.setStringOrNull(key, color);
    },
  );

  late final enableCustomColorScheme =
      createSetting(key: 'useCustomColorScheme', initialValue: false);

  late final hideCustomColorSchemeHint = createSetting<bool>(
    key: 'hideCustomColorSchemeHint',
    initialValue: false,
  );

  late final customColorSchemes = createJsonSetting<
      ({
        ColorScheme lightScheme,
        ColorScheme darkScheme,
      })?>(
    key: 'customColorSchemeJson',
    initialValue: null,
    fromJson: _colorSchemeFromJson,
  );

  static ({
    ColorScheme darkScheme,
    ColorScheme lightScheme,
  })? _colorSchemeFromJson(dynamic json) {
    const msg = 'This should not throw, if it does, I fucked up';

    if (json == null) return null;

    final {
      'lightScheme': lightScheme,
      'darkScheme': darkScheme,
    } = json;

    return (
      lightScheme: ColorSchemeJson.fromJson(lightScheme).expect(msg),
      darkScheme: ColorSchemeJson.fromJson(darkScheme).expect(msg),
    );
  }

  late final _themeMode = createSetting<DoableThemeMode>(
    key: 'themeMode',
    initialValue: const DoableThemeMode(ThemeMode.system, DarkMode.dark),
    read: (prefs, key) {
      final setting = prefs.getString(key);
      if (setting == null) return null;
      return DoableThemeMode.fromSetting(setting);
    },
    write: (prefs, key, value) => prefs.setStringOrNull(
      key,
      value?.toSetting(),
    ),
  );

  late final themeMode = createSetting<DoableThemeMode>(
    key: 'doableThemeMode',
    initialValue: const DoableThemeMode(ThemeMode.system, DarkMode.dark),
    read: (prefs, key) {
      final modeInt = prefs.getInt(key);
      if (modeInt == null) {
        final mode = _themeMode.value;
        if (mode == null) return null;

        prefs.setIntOrNull(key, mode.toInt());
        return mode;
      }

      return DoableThemeMode.fromInt(modeInt);
    },
    write: (prefs, key, value) => prefs.setIntOrNull(key, value?.toInt()),
  );

  @override
  bool import(String key, Object? value) {
    if (key == fontPairing.key && value is String) {
      fontPairing.value = value.parse(FontPairing.values);
    } else if (key == dynamicColors.key && value is bool) {
      dynamicColors.value = value;
    } else if (key == customAccentColor.key && value is String?) {
      customAccentColor.value = value == null ? null : loadColor(value);
    } else if (key == customSecondaryAccentColor.key && value is String?) {
      customSecondaryAccentColor.value =
          value == null ? null : loadColor(value);
    } else if (key == customTertiaryAccentColor.key && value is String?) {
      customTertiaryAccentColor.value = value == null ? null : loadColor(value);
    } else if (key == enableCustomColorScheme.key && value is bool) {
      enableCustomColorScheme.value = value;
    } else if (key == customColorSchemes.key &&
        value is Map<dynamic, dynamic>) {
      customColorSchemes.value = _colorSchemeFromJson(
        value.cast<String, dynamic>(),
      );
    } else if (key == themeMode.key && value is String) {
      themeMode.value = DoableThemeMode.fromSetting(value);
    } else {
      return false;
    }

    return true;
  }
}

class AccessabilityPreferences with NotifiedPreferences, ImportInterface {
  late final reduceMotion =
      createSetting(key: 'reduceMotion', initialValue: false);

  late final textScaleFactor =
      createSetting<double?>(key: 'textScaleFactor', initialValue: null);

  TextScaler? get textScaler {
    if (textScaleFactor.value == null) return null;
    return TextScaler.linear(textScaleFactor.value!);
  }

  late final enablePrideFlags =
      createSetting(key: 'enablePrideFlags', initialValue: true);

  late final constantPrideFlag = createSetting<PrideMonth?>(
    key: 'constantPrideMonth',
    initialValue: null,
    read: (prefs, key) {
      final value = prefs.getString(key);
      if (value == null) return null;
      return PrideMonth.values.firstWhere((month) => month.name == value);
    },
    write: (prefs, key, value) => prefs.setStringOrNull(key, value?.name),
  );

  late final forceHighestRefreshRate =
      createSetting(key: 'forceHighestRefreshRate', initialValue: false);

  @override
  bool import(String key, Object? value) {
    if (key == reduceMotion.key && value is bool) {
      reduceMotion.value = value;
    } else if (key == textScaleFactor.key && value is double?) {
      textScaleFactor.value = value;
    } else if (key == enablePrideFlags.key && value is bool) {
      enablePrideFlags.value = value;
    } else if (key == forceHighestRefreshRate.key && value is bool) {
      forceHighestRefreshRate.value = value;
    } else {
      return false;
    }

    return true;
  }

  void addListener(void Function() listener) => [
        reduceMotion,
        textScaleFactor,
        enablePrideFlags,
        forceHighestRefreshRate
      ].addListener(listener);

  void removeListener(void Function() listener) => [
        reduceMotion,
        textScaleFactor,
        enablePrideFlags,
        forceHighestRefreshRate
      ].removeListener(listener);
}

class FediversePreferences with NotifiedPreferences, ImportInterface {
  late final _fediverseInstanceUrl =
      createSetting<String?>(key: 'fediverseInstanceUrl', initialValue: null);

  String? get instanceUrl => _fediverseInstanceUrl.value;
  set instanceUrl(String? url) {
    if (url == null) {
      _fediverseInstanceUrl.value = url;
      return;
    }

    final lower = url.toLowerCase();

    if (!(lower.startsWith(RegExp(r'https?://')))) {
      _fediverseInstanceUrl.value = 'https://$url';
    } else {
      _fediverseInstanceUrl.value = url;
    }
  }

  late final showButton =
      createSetting(key: 'showFediButton', initialValue: true);

  @override
  bool import(String key, Object? value) {
    if (key == _fediverseInstanceUrl.key && value is String?) {
      _fediverseInstanceUrl.value = value;
    } else if (key == showButton.key && value is bool) {
      showButton.value = value;
    } else {
      return false;
    }

    return true;
  }
}

class BackupPreferences with NotifiedPreferences {
  late final lastSync = createSetting<DateTime?>(
    key: 'lastSync',
    initialValue: null,
    read: (prefs, key) {
      final value = prefs.getString(key);
      if (value == null) return null;
      return DateTime.parse(value);
    },
    write: (prefs, key, value) =>
        prefs.setStringOrNull(key, value?.toIso8601String()),
  );

  late final nextcloud =
      createSetting(key: 'isLoggedInToNextcloud', initialValue: false);

  late final webdav =
      createSetting(key: 'isLoggedInToWebDAV', initialValue: false);

  bool get autoExport => autoExportPath.value != null;

  late final autoExportPath =
      createSetting<String?>(key: 'autoExportPath', initialValue: null);

  late final autoExportDuration = createSetting<Duration>(
    key: 'autoExportDuration',
    initialValue: const Duration(hours: 36),
    read: (prefs, key) {
      final value = prefs.getString(key);
      if (value == null) return null;
      return parseDuration(value);
    },
    write: (prefs, key, value) => prefs.setStringOrNull(key, value.toString()),
  );

  late final lastAutoExport = createSetting(
    key: 'lastAutoExport',
    initialValue: null,
    read: (prefs, key) {
      final value = prefs.getString(key);
      if (value == null) return null;
      return DateTime.parse(value);
    },
    write: (prefs, key, value) => prefs.setStringOrNull(
      key,
      value?.toIso8601String(),
    ),
  );

  bool get synchronize => nextcloud.value || webdav.value;

  Set<String> getKeys() => const {
        'lastSync',
        'isLoggedInToNextcloud',
        'isLoggedInToWebDAV',
        'autoExportPath',
        'autoExportDuration',
        'lastAutoExport',
      };
}

extension MultiListener on List<Listenable> {
  void addListener(VoidCallback listener) =>
      forEach((element) => element.addListener(listener));
  void removeListener(VoidCallback listener) =>
      forEach((element) => element.removeListener(listener));
}

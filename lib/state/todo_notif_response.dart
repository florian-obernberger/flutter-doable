import 'dart:async';

final todoNotificationsStream = StreamController<TodoNotifResponse>.broadcast();

TodoNotifResponse? initialNotification;

sealed class TodoNotifResponse {
  const TodoNotifResponse(this.todoId, this.notifId);

  final String todoId;
  final int notifId;
}

class OpenTodoNotifResponse extends TodoNotifResponse {
  const OpenTodoNotifResponse(super.todoId, super.notifId);
}

class CompleteTodoNotifResponse extends TodoNotifResponse {
  const CompleteTodoNotifResponse(super.todoId, super.notifId);
}

class SnoozeTodoNotifResponse extends TodoNotifResponse {
  const SnoozeTodoNotifResponse(super.todoId, super.notifId);
}

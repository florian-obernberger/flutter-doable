final timedOutConnectionStore = TimedOutConnectionStore._();

/// Maximum amount of times a connection may time out.
const _timeOutLimit = 3;

class TimedOutConnectionStore {
  TimedOutConnectionStore._() : _timedOutCount = {};

  final Map<String, int> _timedOutCount;

  void timedOut(String name) =>
      _timedOutCount[name] = (_timedOutCount[name] ?? 0) + 1;

  /// Represents the number connections have timed out.
  int timedOutCount(String name) => _timedOutCount[name] ?? 0;

  /// Weather or not the time out limit was hit.
  bool hitTimeOutLimit(String name) =>
      (_timedOutCount[name] ?? 0) > _timeOutLimit;
}

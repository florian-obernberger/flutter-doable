import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:result/result.dart';
import 'package:result/anyhow.dart' as anyhow;

import '/remote/remote.dart';
import '/remote/types.dart';
import '/sync/base_sync.dart';
import '/util/sync_data.dart';
import '/state/settings.dart';
import '/state/recently_deleted_db.dart';
import '/state/secure_storage.dart';

import 'database.dart';

final todoListDatabaseProvider = ChangeNotifierProvider<TodoListDatabase>(
  (ref) {
    return TodoListDatabase(
      ref.read(secureStorageDbProvider),
      ref.read(recentlyDeletedProvider),
    )..init();
  },
);

class TodoListDatabase
    with ChangeNotifier
    implements DatabaseWithId<TodoList, String> {
  TodoListDatabase(this._secureStorage, this._recentlyDeleted);

  final SecureStorageDatabase _secureStorage;
  final RecentlyDeletedDatabase _recentlyDeleted;

  Box<TodoList>? _listBox;
  Settings? _settings;

  bool get _hasSyncs => _settings!.backup.synchronize;

  @override
  void dispose() {
    _listBox?.listenable().removeListener(notifyListeners);
    _listBox?.close();

    super.dispose();
  }

  @override
  Future<void> init() async {
    _listBox ??= await Hive.openBox<TodoList>('lists');
    if (_settings == null) {
      _listBox!.listenable().addListener(notifyListeners);
      _settings ??= Settings();
      await _settings!.initialize();
    }
  }

  Future<int> clear() => _listBox!.clear();

  @override
  Future<void> store(TodoList value) async {
    await _listBox!.put(value.id, value.copyWith(lastModified: DateTime.now()));
    _updateOrder([value.id]);

    _getRemotes().asyncMap((remote) => remote.pushTodoList(value)).toList();
  }

  @override
  Future<void> storeAll(List<TodoList> values) async {
    final now = DateTime.now();
    await _listBox!.putAll({
      for (var value in values) value.id: value.copyWith(lastModified: now)
    });
    _updateOrder(values.map((l) => l.id).toList());

    _getRemotes().asyncMap((remote) => remote.pushTodoLists(values)).toList();
  }

  void _updateOrder(List<String> ids) {
    _settings!.extensions.todoListsOrder.value ??=
        values().map((l) => l.id).toList();
    final order = _settings!.extensions.todoListsOrder.value!.toList();

    for (var id in ids) {
      if (order.contains(id)) continue;
      order.add(id);
    }

    _settings!.extensions.todoListsOrder.value = order;
  }

  @override
  Future<void> delete(String id) async {
    await _listBox!.delete(id);
    if (_hasSyncs) {
      await _recentlyDeleted.store(id);
    }

    _getRemotes().asyncMap((remote) => remote.deleteTodoList(id)).toList();
  }

  @override
  Future<void> deleteAll(List<String> ids) async {
    await _listBox!.deleteAll(ids);
    if (_hasSyncs) {
      await _recentlyDeleted.storeAll(ids);
    }

    syncRemotes();
  }

  @override
  TodoList? get(String id) {
    return _listBox!.get(id);
  }

  @override
  Iterable<TodoList> getAll(Iterable<String> ids) sync* {
    for (var id in ids) {
      if (_listBox!.containsKey(id)) yield _listBox!.get(id)!;
    }
  }

  @override
  Iterable<TodoList> values([int? take]) {
    if (take == null) return _listBox!.values;
    return _listBox!.values.take(take);
  }

  @override
  bool contains(String id) => _listBox!.containsKey(id);

  bool get isEmpty => _listBox!.isEmpty;

  bool get isNotEmpty => _listBox!.isNotEmpty;

  @Deprecated('Transition to Remote')
  FutureResult<Sync, SyncError> syncLists({
    bool? isLoggedInToNextcloud,
    bool? isLoggedInToWebDAV,
    bool? forceSync,
  }) async {
    // if (!(await enableTodoListsPrefNotifier.initializedValue)) {
    //   return const Ok(unit);
    // }

    isLoggedInToNextcloud ??= _settings!.backup.nextcloud.value;
    isLoggedInToWebDAV ??= _settings!.backup.webdav.value;

    if (!isLoggedInToNextcloud && !isLoggedInToWebDAV) {
      notifyListeners();
      return const Ok(Sync.unchanged);
    }

    var result = Sync.unchanged;

    if (isLoggedInToNextcloud) {
      final nc = await _secureStorage.readTodoListNextcloudSync();
      final res = await _syncLists(nc, forceSync ?? false);
      if (res.isErr()) return res;
      result += res.unwrap();
    }

    if (isLoggedInToWebDAV) {
      final wd = await _secureStorage.readTodoListWebDAVSync();
      final res = await _syncLists(wd, forceSync ?? false);
      if (res.isErr()) return res;
      result += res.unwrap();
    }

    if (result == Sync.updated) {
      await _recentlyDeleted.clear();
    }

    notifyListeners();
    return const Ok(Sync.updated);
  }

  @Deprecated('Transition to Remote')
  FutureResult<Sync, SyncError> _syncLists(
    BaseSync<TodoList> bs,
    bool forceSync,
  ) async {
    final syncRes = await syncData<TodoList>(
      backup: _settings!.backup,
      bs: bs,
      rd: _recentlyDeleted,
      data: _listBox!.values,
      forceSync: forceSync,
    );

    switch (syncRes) {
      case Err():
        return Err(syncRes.error);
      case Ok() when syncRes.value == null:
        return const Ok(Sync.updated);
      case Ok():
        final todoSync = syncRes.value!;

        _listBox!.putAll({for (var todo in todoSync.updated) todo.id: todo});
        _listBox!.deleteAll(todoSync.deleted.map((t) => t.id));

        return const Ok(Sync.updated);
    }
  }

  anyhow.FutureResult<()> resetSync() async {
    if (_settings!.backup.synchronize) return const Ok(());

    final reset = _listBox!.values
        .map((list) => list.copyWith(hadInitialSync: false))
        .map((list) => MapEntry(list.id, list));

    return await anyhow.Guard.guardAsync<()>(() async {
      await _listBox!.putAll(Map.fromEntries(reset));
      return const ();
    });
  }

  FutureResult<(), (anyhow.Error, Remote)> syncRemotes({
    Set<RemoteImpl>? remotes,
    RemoteAction action = RemoteAction.sync,
  }) async {
    await for (final remote in _getRemotes(remotes)) {
      final res = await _syncRemote(remote);
      if (res case Err(:final error)) return Err((error, remote));
    }

    await _recentlyDeleted.clear();
    return const Ok(());
  }

  Stream<Remote> _getRemotes([Set<RemoteImpl>? impls]) async* {
    impls ??= RemoteImpl.fromSettings(_settings!);

    for (final impl in impls) {
      yield switch (impl) {
        RemoteImpl.nextcloud => await _secureStorage.readNextcloudRemote(),
        RemoteImpl.webDAV => await _secureStorage.readWebdavRemote(),
        _ => throw UnimplementedError(),
      };
    }
  }

  anyhow.FutureResult<()> _syncRemote(Remote remote) async {
    if (_recentlyDeleted.values().isNotEmpty) {
      final res = await Future.wait(
        _recentlyDeleted.values().map(remote.deleteTodoList),
      ).merge();

      if (res case Err(:final error)) {
        if (error.downcast<NotFound>().isOk()) return Err(error);
        // ignoring does not exist since it might be the case that this remote
        // already deleted the lists, while another one didn't.
      }
    }

    final res = await remote.syncTodoLists(_listBox!.values);
    if (res case Err(error: final err)) return Err(err);

    final (updated: updated, added: added, deleted: deleted) = res.unwrap();
    await _listBox!.putAll({
      for (var list in updated.followedBy(added)) list.id: list,
    });
    await _listBox!.deleteAll(deleted);

    return const Ok(());
  }
}

import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:animations/animations.dart';

import '/state/settings.dart';
import '/classes/logger.dart' show logger;
import '/types/window_breakpoint.dart';
import '/classes/motion.dart';

import '/pages/router_error_view.dart';
import '/pages/todo_view.dart';
import '/pages/todo_detail_view.dart';
import '/pages/introduction_view.dart';
import '/pages/settings_view.dart';
import '/pages/settings_view_expanded.dart';
import '/pages/feedback_view.dart';
import '/pages/update_view.dart';
import '/pages/todo_list_settings_view.dart';
import '/pages/settings_view.d/settings.dart';
import '/pages/update_view.d/update_checker.dart' show Update;

typedef GoRouterPageBuilder<T> = Page<T> Function(
    BuildContext context, GoRouterState state);

class Material3Route extends GoRoute {
  Material3Route({
    required super.path,
    required GoRouterWidgetBuilder super.builder,
    super.redirect,
  }) : super(pageBuilder: createPageBuilder(builder));

  static GoRouterPageBuilder? createPageBuilder(GoRouterWidgetBuilder builder) {
    return (BuildContext context, GoRouterState state) {
      final settings = Settings.of(context);

      return CustomTransitionPage(
        child: builder(context, state),
        transitionDuration: settings.accessability.reduceMotion.value
            ? Motion.medium2
            : Motion.medium4,
        transitionsBuilder: (context, animation, secondaryAnimation, child) {
          if (settings.accessability.reduceMotion.value) {
            return FadeTransition(
              opacity: CurveTween(curve: Easing.standard).animate(animation),
              child: child,
            );
          }

          return SharedAxisTransition(
            animation: animation,
            secondaryAnimation: secondaryAnimation,
            transitionType: SharedAxisTransitionType.horizontal,
            child: child,
          );
        },
      );
    };
  }
}

Widget _errorBuilder(BuildContext context, GoRouterState state) {
  logger.e(
    message: 'Ended up on a non existing page.',
    state.error ?? 'GoRouterState.error was null',
  );
  return RouterErrorView(state.matchedLocation);
}

GoRouter getRouter(Settings settings) {
  return GoRouter(
    initialLocation:
        settings.miscellaneous.hadIntroduction.value ? '/' : '/introduction',
    errorPageBuilder: Material3Route.createPageBuilder(_errorBuilder),
    routes: <GoRoute>[
      Material3Route(
        path: '/',
        builder: (context, state) => const TodoView(),
      ),
      Material3Route(
        path: '/introduction',
        builder: (context, state) => const IntroductionView(),
      ),
      Material3Route(
        path: '/details/:id',
        builder: (context, state) => TodoDetailsView(
          todoId: state.pathParameters['id']!,
          wasNotification: state.extra as bool,
        ),
      ),
      Material3Route(
        path: '/settings',
        builder: (context, state) {
          final breakpoint = WindowBreakpoint.of(context);
          if (breakpoint.isExpanded) return const SettingsViewExpanded();
          return SettingsView(
            initialPage: state.extra as SettingsPage?,
          );
        },
      ),
      Material3Route(
        path: '/todoListSettings',
        builder: (context, state) => const TodoListsSettingsView(),
      ),
      Material3Route(
        path: '/feedback',
        builder: (context, state) => const FeedbackView(),
      ),
      Material3Route(
        path: '/update',
        builder: (context, state) => UpdateView(state.extra as Update),
      )
    ],
  );
}

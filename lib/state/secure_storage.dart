import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import '/sync/nextcloud_sync.dart';
import '/sync/webdav_sync.dart';
import '/sync/todo_list_syncs/nextcloud_sync.dart';
import '/sync/todo_list_syncs/webdav_sync.dart';

import '/remote/neon/neon_remote.dart';
import '/remote/webdav/webdav_remote.dart';

final secureStorageDbProvider = ChangeNotifierProvider<SecureStorageDatabase>(
  (ref) {
    final db = SecureStorageDatabase();
    db.initStorage();
    return db;
  },
);

const FlutterSecureStorage _secureStorage = FlutterSecureStorage(
  aOptions: AndroidOptions(
    preferencesKeyPrefix: 'nc',
    sharedPreferencesName: 'at.flobii.doable',
  ),
);

class SecureStorageDatabase with ChangeNotifier {
  Future<void> initStorage() => _secureStorage.readAll();

  // Nextcloud Sync

  @Deprecated('Transition to Remote')
  Future<NextcloudSync> readNextcloudSync() async {
    return NextcloudSync(
      baseUrl: (await _secureStorage.read(key: 'baseUrl')) ?? '',
      folder: (await _secureStorage.read(key: 'folder')) ?? '',
      user: (await _secureStorage.read(key: 'user')) ?? '',
      password: (await _secureStorage.read(key: 'password')) ?? '',
    );
  }

  @Deprecated('Transition to Remote')
  Future<TodoListNextcloudSync> readTodoListNextcloudSync() async {
    return TodoListNextcloudSync(
      baseUrl: (await _secureStorage.read(key: 'baseUrl')) ?? '',
      folder: (await _secureStorage.read(key: 'folder')) ?? '',
      user: (await _secureStorage.read(key: 'user')) ?? '',
      password: (await _secureStorage.read(key: 'password')) ?? '',
    );
  }

  @Deprecated('Transition to Remote')
  Future<void> writeNextcloudSync(NextcloudSync nc) async {
    await _secureStorage.write(key: 'baseUrl', value: nc.baseUrl);
    await _secureStorage.write(key: 'folder', value: nc.folder);
    await _secureStorage.write(key: 'user', value: nc.user);
    await _secureStorage.write(key: 'password', value: nc.password);

    notifyListeners();
  }

  @Deprecated('Transition to Remote')
  Future<void> deleteNextcloudSync() async {
    await _secureStorage.delete(key: 'baseUrl');
    await _secureStorage.delete(key: 'folder');
    await _secureStorage.delete(key: 'user');
    await _secureStorage.delete(key: 'password');

    notifyListeners();
  }

  Future<NeonRemote> readNextcloudRemote() async {
    final [baseUrl, folder, user, password, language] = await Future.wait([
      _secureStorage.read(key: 'baseUrl'),
      _secureStorage.read(key: 'folder'),
      _secureStorage.read(key: 'user'),
      _secureStorage.read(key: 'password'),
      _secureStorage.read(key: 'language'),
    ]);

    return NeonRemote(
      baseUrl ?? '',
      folder: folder,
      user: user ?? '',
      password: password ?? '',
      language: language,
    );
  }

  Future<void> writeNextcloud({
    required String baseUrl,
    String? folder,
    required String user,
    required String password,
    String? language,
  }) =>
      Future.wait([
        _secureStorage.write(key: 'baseUrl', value: baseUrl),
        _secureStorage.write(key: 'folder', value: folder),
        _secureStorage.write(key: 'user', value: user),
        _secureStorage.write(key: 'password', value: password),
        _secureStorage.write(key: 'language', value: language),
      ]);

  Future<void> updateNextcloudFolder(String folder) => _secureStorage.write(
        key: 'folder',
        value: folder,
      );

  Future<void> deleteNextcloud() async {
    await _secureStorage.delete(key: 'baseUrl');
    await _secureStorage.delete(key: 'folder');
    await _secureStorage.delete(key: 'user');
    await _secureStorage.delete(key: 'password');

    notifyListeners();
  }

  // WebDAV Sync

  @Deprecated('Transition to Remote')
  Future<WebDAVSync> readWebDAVSync() async {
    return WebDAVSync(
      baseUrl: (await _secureStorage.read(key: 'webDAVBaseUrl')) ?? '',
      user: (await _secureStorage.read(key: 'webDAVUser')) ?? '',
      password: (await _secureStorage.read(key: 'webDAVPassword')) ?? '',
    );
  }

  @Deprecated('Transition to Remote')
  Future<TodoListWebDAVSync> readTodoListWebDAVSync() async {
    return TodoListWebDAVSync(
      baseUrl: (await _secureStorage.read(key: 'webDAVBaseUrl')) ?? '',
      user: (await _secureStorage.read(key: 'webDAVUser')) ?? '',
      password: (await _secureStorage.read(key: 'webDAVPassword')) ?? '',
    );
  }

  @Deprecated('Transition to Remote')
  Future<void> writeWebDAVSync(WebDAVSync nc) async {
    await _secureStorage.write(key: 'webDAVBaseUrl', value: nc.baseUrl);
    await _secureStorage.write(key: 'webDAVUser', value: nc.user);
    await _secureStorage.write(key: 'webDAVPassword', value: nc.password);

    notifyListeners();
  }

  @Deprecated('Transition to Remote')
  Future<void> deleteWebDAVSync() async {
    await _secureStorage.delete(key: 'webDAVBaseUrl');
    await _secureStorage.delete(key: 'webDAVUser');
    await _secureStorage.delete(key: 'webDAVPassword');

    notifyListeners();
  }

  Future<WebdavRemote> readWebdavRemote() async {
    final [baseUrl, folder, user, password] = await Future.wait([
      _secureStorage.read(key: 'webDAVBaseUrl'),
      _secureStorage.read(key: 'webDAVFolder'),
      _secureStorage.read(key: 'webDAVUser'),
      _secureStorage.read(key: 'webDAVPassword'),
    ]);

    return WebdavRemote(
      baseUrl ?? '',
      folder: folder,
      user: user ?? '',
      password: password ?? '',
    );
  }

  Future<void> writeWebdav({
    required String baseUrl,
    String? folder,
    required String user,
    required String password,
  }) =>
      Future.wait([
        _secureStorage.write(key: 'webDAVBaseUrl', value: baseUrl),
        _secureStorage.write(key: 'webDAVFolder', value: folder),
        _secureStorage.write(key: 'webDAVUser', value: user),
        _secureStorage.write(key: 'webDAVPassword', value: password),
      ]);

  Future<void> updateWebdavFolder(String folder) => _secureStorage.write(
        key: 'webDAVFolder',
        value: folder,
      );

  Future<void> deleteWebdav() async {
    await _secureStorage.delete(key: 'webDAVBaseUrl');
    await _secureStorage.delete(key: 'webDAVFolder');
    await _secureStorage.delete(key: 'webDAVUser');
    await _secureStorage.delete(key: 'webDAVPassword');

    notifyListeners();
  }
}

import 'dart:async';

abstract interface class Database<T> {
  FutureOr<void> init();

  FutureOr<void> store(T value);
  FutureOr<void> storeAll(List<T> values);

  Iterable<T> values([int? take]);
}

abstract interface class DatabaseWithNoId<T> extends Database<T> {
  FutureOr<void> delete(T value);
  FutureOr<void> deleteAll(List<T> values);
}

abstract interface class DatabaseWithId<T, S> extends Database<T> {
  T? get(S id);
  Iterable<T> getAll(Iterable<S> ids);

  FutureOr<void> delete(S id);
  FutureOr<void> deleteAll(List<S> ids);

  FutureOr<bool> contains(S id);
}

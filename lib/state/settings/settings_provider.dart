part of '../settings.dart';

class SettingsProvider extends InheritedWidget {
  SettingsProvider({
    super.key,
    required this.settings,
    required super.child,
  }) : _settingsHashCode = settings.hashCode;

  final Settings settings;
  final int _settingsHashCode;

  static Settings of(BuildContext context) =>
      context.dependOnInheritedWidgetOfExactType<SettingsProvider>()!.settings;

  static Settings? maybeOf(BuildContext context) =>
      context.dependOnInheritedWidgetOfExactType<SettingsProvider>()?.settings;

  @override
  bool updateShouldNotify(covariant SettingsProvider oldWidget) =>
      oldWidget._settingsHashCode != _settingsHashCode;
}

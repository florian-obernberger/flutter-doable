part of '../settings.dart';

/// A [StatefulWidget] that can read settings.
abstract class SettingsWidget extends StatefulWidget {
  /// A [StatefulWidget] that can read providers.
  const SettingsWidget({required this.toBeListened, super.key});

  final List<PreferenceNotifier> toBeListened;

  @override
  SettingsState createState();

  @override
  SettingsStatefulElement createElement() {
    return SettingsStatefulElement(this);
  }
}

/// A [State] that has access to [Settings] through [settings], allowing
/// it to read providers.
abstract class SettingsState<T extends SettingsWidget> extends State<T> {
  Settings? _settings;

  Settings get settings => _settings!;

  void _listener() => setState(() {});

  @override
  @mustCallSuper
  void didChangeDependencies() {
    if (_settings == null) {
      _settings = Settings.of(context);
      widget.toBeListened.addListener(_listener);
    }

    super.didChangeDependencies();
  }

  @override
  @mustCallSuper
  void dispose() {
    widget.toBeListened.removeListener(_listener);

    super.dispose();
  }
}

class SettingsStatefulElement extends StatefulElement {
  SettingsStatefulElement(SettingsWidget super.widget);
}

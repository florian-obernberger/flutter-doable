import 'package:flutter/material.dart';

import '/data/notifications/todo_notification.dart';

final class TodoNotificationSettings with ChangeNotifier {
  static const _generalKey = 'general-notif-settings';

  final Map<String, List<TodoDuration>> _data;

  TodoNotificationSettings(this._data);

  TodoNotificationSettings.fromGeneral(List<TodoDuration> general)
      : _data = {_generalKey: general};

  void addToList(String listId, TodoDuration duration) {
    _data.update(
      listId,
      (durations) => durations..add(duration),
      ifAbsent: () => [duration],
    );
    notifyListeners();
  }

  void setList(String listId, List<TodoDuration> durations) =>
      _data[listId] = durations;

  bool removeFromList(String listId, TodoDuration duration) {
    final removed = _data[listId]?.remove(duration) ?? false;
    notifyListeners();
    return removed;
  }

  void removeList(String listId) {
    _data.remove(listId);
    notifyListeners();
  }

  List<TodoDuration>? getList(String listId) => _data[listId];
  List<TodoDuration> getListOrGeneral(String? listId) => switch (listId) {
        String() => _data[listId] ?? getGeneral(),
        null => getGeneral(),
      };

  Set<String> getAllListIds() =>
      _data.keys.where((id) => id != _generalKey).toSet();

  void addToGeneral(TodoDuration duration) {
    _data.update(
      _generalKey,
      (durations) => durations..add(duration),
      ifAbsent: () => [duration],
    );
    notifyListeners();
  }

  void setGeneral(List<TodoDuration> durations) =>
      _data[_generalKey] = durations;

  bool removeFromGeneral(TodoDuration duration) {
    final removed = _data[_generalKey]?.remove(duration) ?? false;
    notifyListeners();
    return removed;
  }

  void removeGeneral() {
    _data[_generalKey];
    notifyListeners();
  }

  List<TodoDuration> getGeneral() => _data[_generalKey] ?? const [];

  TodoNotificationSettings.fromJson(Map<String, dynamic> json)
      : _data = json.cast<String, List<int>>().map((key, value) => MapEntry(
              key,
              value
                  .map((duration) => TodoDuration.fromMicroseconds(duration))
                  .toList(),
            ));

  Map<String, List<int>> toJson() => _data.map((key, value) => MapEntry(
        key,
        value.map((duration) => duration.inMicroseconds).toList(),
      ));
}

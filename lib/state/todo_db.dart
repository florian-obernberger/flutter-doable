import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:rrule/rrule.dart';
import 'package:result/result.dart';
import 'package:result/anyhow.dart' as anyhow;
import 'package:timezone/timezone.dart';

import '/data/applications_directory.dart';
import '/data/base_notification.dart';
import '/data/notifications/todo_notification.dart';
import '/remote/remote.dart';
import '/remote/types.dart';
import '/sync/base_sync.dart';
import '/classes/logger.dart';
import '/util/sync_data.dart';

import 'database.dart';
import 'settings.dart';
import 'secure_storage.dart';
import 'recently_deleted_db.dart';

final todoDatabaseProvider = ChangeNotifierProvider<TodoDatabase>(
  (ref) => TodoDatabase(
    ref.read(secureStorageDbProvider),
    ref.read(recentlyDeletedProvider),
  ),
);

class TodoDatabase with ChangeNotifier implements DatabaseWithId<Todo, String> {
  TodoDatabase(this._secureStorage, this._recentlyDeleted);

  final SecureStorageDatabase _secureStorage;
  final RecentlyDeletedDatabase _recentlyDeleted;

  Box<Todo>? _todoBox;
  Settings? _settings;
  bool _initialized = false;

  bool get _hasSyncs => _settings!.backup.synchronize;

  @override
  void dispose() {
    _todoBox?.listenable().removeListener(notifyListeners);
    _todoBox?.close();

    super.dispose();
  }

  bool get initialized => _initialized;

  @override
  Future<void> init() async {
    await _recentlyDeleted.init();
    _todoBox ??= await Hive.openBox<Todo>('todo');
    if (_settings == null) {
      _todoBox!.listenable().addListener(notifyListeners);
      _settings ??= Settings();
      await _settings!.initialize();
    }
    _initialized = true;
  }

  Future<int> clear() => _todoBox!.clear();

  @override
  Future<void> store(Todo value) async {
    await _todoBox!.put(value.id, value..lastModified = DateTime.now());

    await for (final remote in _getRemotes()) {
      final res = await remote.pushTodo(value);
      if (res is Ok) {
        await _todoBox!.put(value.id, value..hadInitialSync = true);
      }
    }
  }

  @override
  Future<void> storeAll(Iterable<Todo> values) async {
    final todos = values.toList();
    await _todoBox!.putAll({
      for (var todo in todos) todo.id: todo..lastModified = DateTime.now(),
    });

    await for (final remote in _getRemotes()) {
      final res = await remote.pushTodos(todos);
      if (res is Ok) {
        await _todoBox!.putAll({
          for (var todo in todos) todo.id: todo..hadInitialSync = true,
        });
      }
    }
  }

  @override
  Future<void> delete(String id) async {
    if (!_todoBox!.containsKey(id)) return;

    await _cancelNotification(_todoBox!.get(id)!);
    await _todoBox!.delete(id);
    if (_hasSyncs) await _recentlyDeleted.store(id);

    await todoImagesDirectory.image(id).deleteIfExists();

    await for (final remote in _getRemotes()) {
      final res = await remote.deleteTodo(id);
      if (res is Ok) await _recentlyDeleted.delete(id);
    }
  }

  @override
  Future<void> deleteAll(List<String> ids) async {
    if (ids.isEmpty) return;

    await _cancelAllNotifications(
        ids.map(_todoBox!.get).where((todo) => todo != null).cast<Todo>());
    await _todoBox!.deleteAll(ids);

    if (_hasSyncs) await _recentlyDeleted.storeAll(ids);

    await Future.wait(
      ids.map((id) => todoImagesDirectory.image(id).deleteIfExists()),
    );

    await _getRemotes().asyncMap((remote) => remote.deleteTodos(ids)).toList();
  }

  @override
  Todo? get(String id) {
    return _todoBox!.get(id);
  }

  @override
  Iterable<Todo> getAll(Iterable<String> ids) sync* {
    for (var id in ids) {
      if (_todoBox!.containsKey(id)) yield _todoBox!.get(id)!;
    }
  }

  @override
  Iterable<Todo> values([int? take]) {
    if (take == null) return _todoBox!.values;
    return _todoBox!.values.take(take);
  }

  @override
  bool contains(String id) => _todoBox!.containsKey(id);

  /// Deletes completed [Todos] in according with
  /// [deleteCompletedAfterPrefNotifier].
  Future<void> deleteCompleted({
    String? listId,
    bool ignoreDate = false,
  }) async {
    final deleted = (await _deleteCompleted(listId, ignoreDate)).toList();
    return deleteAll(deleted);
  }

  Future<void> scheduleNotifications(WidgetRef ref, Todo todo) async {
    if (todo.notificationMetadata == null) return;

    final notifications = todo.notifications(ref)!;
    if (todo.isCompleted) {
      await notifications.cancelAll();
    } else {
      await notifications.scheduleAll();
    }
  }

  Future<void> scheduleAllNotifications(WidgetRef ref) async {
    final todos = values().where((todo) => todo.notificationMetadata != null);
    if (todos.isEmpty) return;

    final futures = <Future>[];
    for (final todo in todos) {
      final notifications = todo.notifications(ref)!;

      futures.add(todo.isCompleted
          ? notifications.cancelAll()
          : notifications.scheduleAll());
    }

    await Future.wait(futures);
  }

  Future<void> recurrTodo(WidgetRef ref, Todo todo) async {
    if (!_settings!.extensions.enableRecurringTasks.value) return;

    var currentDate = todo.relevantDateTime?.toUtc();
    final rule = todo.rRule;

    if (currentDate == null || rule == null) return;

    final now = DateTime.now().toUtc();
    final afterDate = currentDate < now ? now : currentDate;

    final nextId = getNextId(todo.id, todo.rRule!);
    final nextDate =
        rule.getInstances(start: currentDate, after: afterDate).first.toLocal();

    final notifications = todo.notificationMetadata?.map((notif) {
      if (notif.isRelative) {
        return notif.copyWith(
          id: todoRelevantId(nextDate, notif.duration!),
          scheduledDate: TZDateTime.from(nextDate, local),
        );
      }

      final currentDate = notif.scheduledDate;
      final afterDate = currentDate < now ? now : currentDate;

      return notif.copyWith(
        id: NotifId.generate(),
        scheduledDate: TZDateTime.from(
          rule
              .getInstances(start: currentDate, after: afterDate)
              .first
              .toLocal(),
          local,
        ),
      );
    }).toSet();

    final newTodo = todo.copyWith(
      id: nextId,
      creationDate: DateTime.now(),
      hadInitialSync: false,
      relevantDate: nextDate,
      relevantTime:
          todo.relevantTime == null ? null : TimeOfDay.fromDateTime(nextDate),
      isCompleted: false,
      hasRecurred: false,
      notificationMetadata: notifications,
    )..completedDate = null;
    await scheduleNotifications(ref, newTodo);

    await storeAll([newTodo, todo..hasRecurred = true]);
  }

  bool containsNextRecurrence(Todo todo) {
    if (todo.rRuleString == null) return false;

    final nextId = getNextId(todo.id, todo.rRule!);
    return contains(nextId);
  }

  static String sanitizeCurrentId(String id) =>
      id.split('-recurr-').first.split(';recurr;').first;

  /// <rawId>;rrule;<count>;<FREQ>;<INTER>;
  static String getNextId(String id, RecurrenceRule rRule, [int step = 1]) {
    const sep = ':rrule:';
    const smallSep = ':';

    String rawId;
    int count = 0;

    if (id.contains('-recurr-')) {
      rawId = id.split('-recurr-').first;
    } else if (id.contains(sep)) {
      try {
        // <rawId>;rrule;<count>;<FREQ>;<INTER>;

        // [<rawId>, <count>;<FREQ>;<INTER>]
        final parts = id.split(sep);
        rawId = parts.first;

        //                   [<count>, <FREQ>, <INTER>]
        count = int.tryParse(parts.last.split(smallSep).first) ?? 0;
      } catch (err, stack) {
        rawId = id;

        logger.e(
          err,
          stackTrace: stack,
          tag: 'Todo DB',
          message: "Couldn't create next id",
        );
      }
    } else {
      rawId = id;
    }

    final freq = rRule.frequency.toString();
    final interval = rRule.actualInterval;
    count += step;

    // <rawId>;rrule;<count>;<FREQ>;<INTER>;
    return '$rawId$sep$count$smallSep$freq$smallSep$interval';
  }

  Future<void> _cancelNotification(Todo todo) async {
    if (todo.notificationMetadata == null) return;

    await Future.wait(todo.notificationMetadata!
        .map((notif) => BaseNotification.cancelId(notif.id.value)));

    return;
  }

  Future<void> _cancelAllNotifications(Iterable<Todo> todos) =>
      Future.wait(todos.map(_cancelNotification));

  Future<Iterable<String>> _deleteCompleted([
    String? listId,
    bool ignoreDate = false,
  ]) async {
    final duration = _settings!.general.deleteCompletedAfter.value;
    if ((duration == null || duration == const Duration(days: -1)) &&
        !ignoreDate) return const Iterable.empty();

    final now = DateTime.now();

    final toDelete = <Todo>[];
    final Iterable<Todo> completedTodos;

    if (listId == null) {
      completedTodos = _todoBox!.values.where(
        (t) => t.isCompleted && t.completedDate != null,
      );
    } else {
      completedTodos = _todoBox!.values.where(
        (t) => t.isCompleted && t.completedDate != null && t.listId == listId,
      );
    }

    if (ignoreDate) {
      toDelete.addAll(completedTodos.map((t) => t));
    } else {
      for (var todo in completedTodos) {
        final date = todo.completedDate!;

        if (date.add(duration!).isBefore(now)) {
          toDelete.add(todo);
        }
      }
    }

    final toDeleteIds = toDelete.map((t) => t.id).toList();

    await _todoBox!.deleteAll(toDeleteIds);
    await _cancelAllNotifications(toDelete);
    if (_hasSyncs) await _recentlyDeleted.storeAll(toDeleteIds);

    return toDeleteIds;
  }

  /// Returns [SyncOk] if the sync went ok, otherwise [SyncError].
  /// **Never returns [SyncOkWithData]**.
  /// If the user is not logged in to nextcloud returns null.
  @Deprecated('Transition to Remote')
  FutureResult<Sync, SyncError> syncTodos({
    bool? isLoggedInToNextcloud,
    bool? isLoggedInToWebDAV,
    bool? forceSync,
  }) async {
    isLoggedInToNextcloud ??= _settings!.backup.nextcloud.value;
    isLoggedInToWebDAV ??= _settings!.backup.webdav.value;

    notifyListeners();

    var result = Sync.unchanged;

    if (!isLoggedInToNextcloud && !isLoggedInToWebDAV) {
      return const Ok(Sync.unchanged);
    }

    await _deleteCompleted();

    if (isLoggedInToNextcloud) {
      final nc = await _secureStorage.readNextcloudSync();
      final res = await _syncTodos(nc, forceSync ?? false);
      if (res.isErr()) return res;
      result += res.unwrap();
    }

    if (isLoggedInToWebDAV) {
      final wd = await _secureStorage.readWebDAVSync();
      final res = await _syncTodos(wd, forceSync ?? false);
      if (res.isErr()) return res;
      result += res.unwrap();
    }

    if (result == Sync.updated) {
      await _recentlyDeleted.clear();
    }

    notifyListeners();
    return const Ok(Sync.updated);
  }

  @Deprecated('Transition to Remote')
  FutureResult<Sync, SyncError> _syncTodos(
    BaseSync<Todo> bs,
    bool forceSync,
  ) async {
    // final syncRes = await st.syncTodos(bs, _recentlyDeleted, _todoBox!.values);
    final syncRes = await syncData<Todo>(
      backup: _settings!.backup,
      bs: bs,
      rd: _recentlyDeleted,
      data: _todoBox!.values,
      forceSync: forceSync,
    );

    switch (syncRes) {
      case Err():
        return syncRes.map((_) => Sync.unchanged);
      case Ok():
        final todoSync = syncRes.value;
        if (todoSync == null) return const Ok(Sync.unchanged);

        final updated = todoSync.updated.map(
          (t) => MapEntry(t.id, t.copyWith(image: get(t.id)?.image)),
        );

        await _todoBox!.putAll(Map.fromEntries(updated));
        await _todoBox!.deleteAll(todoSync.deleted.map((t) => t.id));

        return const Ok(Sync.updated);
    }
  }

  anyhow.FutureResult<()> resetSync() async {
    if (_settings!.backup.synchronize) return const Ok(());

    final reset = _todoBox!.values
        .map((todo) => todo.copyWith(hadInitialSync: false))
        .map((todo) => MapEntry(todo.id, todo));

    return await anyhow.Guard.guardAsync<()>(() async {
      await _todoBox!.putAll(Map.fromEntries(reset));
      return const ();
    });
  }

  FutureResult<(), (anyhow.Error, Remote)> syncRemotes({
    Set<RemoteImpl>? remotes,
    RemoteAction action = RemoteAction.sync,
  }) async {
    await for (final remote in _getRemotes(remotes)) {
      final res = await _syncRemote(remote);
      if (res case Err(:final error)) return Err((error, remote));
    }

    await _recentlyDeleted.clear();
    return const Ok(());
  }

  Stream<Remote> _getRemotes([Set<RemoteImpl>? impls]) async* {
    impls ??= RemoteImpl.fromSettings(_settings!);

    for (final impl in impls) {
      yield switch (impl) {
        RemoteImpl.nextcloud => await _secureStorage.readNextcloudRemote(),
        RemoteImpl.webDAV => await _secureStorage.readWebdavRemote(),
        _ => throw UnimplementedError(),
      };
    }
  }

  anyhow.FutureResult<()> _syncRemote(Remote remote) async {
    if (_recentlyDeleted.values().isNotEmpty) {
      final res = await Future.wait(
        _recentlyDeleted.values().map(remote.deleteTodo),
      ).merge();

      if (res case Err(:final error)) {
        if (error.downcast<NotFound>().isOk()) return Err(error);
        // ignoring does not exist since it might be the case that this remote
        // already deleted the todos, while another one didn't.
      }
    }

    final res = await remote.syncTodos(_todoBox!.values);
    if (res case Err(error: final err)) return Err(err);

    final (updated: updated, added: added, deleted: deleted) = res.unwrap();
    await _todoBox!.putAll({
      for (var todo in updated.followedBy(added)) todo.id: todo,
    });
    await _todoBox!.deleteAll(deleted);

    return const Ok(());
  }
}

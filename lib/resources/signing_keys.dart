part of 'resources.dart';

class SigningKeys {
  SigningKeys._();

  static const String florianObernbergerPubkey =
      'assets/keys/florian_obernberger_pubkey.asc';

  static const List<String> values = [florianObernbergerPubkey];
}

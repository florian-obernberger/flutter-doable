part of 'resources.dart';

class DynamicImages {
  DynamicImages._();

  static const String m0 = 'assets/dynamic_images/m0.webp';
  static const String m1020 = 'assets/dynamic_images/m1020.webp';
  static const String m1080 = 'assets/dynamic_images/m1080.webp';
  static const String m1140 = 'assets/dynamic_images/m1140.webp';
  static const String m1200 = 'assets/dynamic_images/m1200.webp';
  static const String m390 = 'assets/dynamic_images/m390.webp';
  static const String m450 = 'assets/dynamic_images/m450.webp';
  static const String m480 = 'assets/dynamic_images/m480.webp';
  static const String m510 = 'assets/dynamic_images/m510.webp';
  static const String m540 = 'assets/dynamic_images/m540.webp';
  static const String m600 = 'assets/dynamic_images/m600.webp';
  static const String m660 = 'assets/dynamic_images/m660.webp';
  static const String m720 = 'assets/dynamic_images/m720.webp';
  static const String m900 = 'assets/dynamic_images/m900.webp';
  static const String m930 = 'assets/dynamic_images/m930.webp';
  static const String m960 = 'assets/dynamic_images/m960.webp';

  static const List<String> values = [
    m0,
    m1020,
    m1080,
    m1140,
    m1200,
    m390,
    m450,
    m480,
    m510,
    m540,
    m600,
    m660,
    m720,
    m900,
    m930,
    m960
  ];
}

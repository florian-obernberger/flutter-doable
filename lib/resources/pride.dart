part of 'resources.dart';

class Pride {
  Pride._();

  static const String disabilityPrideBanner =
      'assets/pride/disability_pride_banner.svg';
  static const String prideBanner = 'assets/pride/pride_banner.svg';

  static const List<String> values = [disabilityPrideBanner, prideBanner];
}

part of 'resources.dart';

class SvgIcons {
  SvgIcons._();

  static const String editNoteW400 = 'assets/icons/edit_note_w400.svg';
  static const String listHiddenW300 = 'assets/icons/list_hidden_w300.svg';
  static const String listHiddenW400 = 'assets/icons/list_hidden_w400.svg';
  static const String listW300 = 'assets/icons/list_w300.svg';
  static const String listW400 = 'assets/icons/list_w400.svg';
  static const String removeNoteW400 = 'assets/icons/remove_note_w400.svg';
  static const String sortAscendingW400 =
      'assets/icons/sort_ascending_w400.svg';
  static const String sortDescendingW400 =
      'assets/icons/sort_descending_w400.svg';
  static const String undoDoneW500 = 'assets/icons/undo_done_w500.svg';

  static const List<String> values = [
    editNoteW400,
    listHiddenW300,
    listHiddenW400,
    listW300,
    listW400,
    removeNoteW400,
    sortAscendingW400,
    sortDescendingW400,
    undoDoneW500
  ];
}

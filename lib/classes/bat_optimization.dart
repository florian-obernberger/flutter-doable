import 'package:disable_battery_optimization/disable_battery_optimization.dart';

final class BatOptimization {
  const BatOptimization._();

  static Future<bool?> get isDisabled =>
      DisableBatteryOptimization.isBatteryOptimizationDisabled;

  static Future<bool?> get isEnabled => isDisabled.inverted;

  static Future<bool?> disableBatteryOptimizations() =>
      DisableBatteryOptimization.showDisableBatteryOptimizationSettings();

  static Future<bool?> get isManufacturerDisabled =>
      DisableBatteryOptimization.isManufacturerBatteryOptimizationDisabled;

  static Future<bool?> get isManufacturerEnabled =>
      isManufacturerDisabled.inverted;

  static Future<bool?> disableManufacturerOptimizations({
    required String title,
    required String description,
  }) async =>
      DisableBatteryOptimization
          .showDisableManufacturerBatteryOptimizationSettings(
              title, description);

  static Future<bool?> get allDisabled async {
    final disabled = await isDisabled;
    final manufacturerDisabled = await isManufacturerDisabled;

    if (disabled == null) return manufacturerDisabled;
    if (manufacturerDisabled == null) return disabled;

    return disabled && manufacturerDisabled;
  }

  static Future<bool?> get anyEnabled => allDisabled.inverted;
}

extension FutureBoolHelpers on Future<bool?> {
  Future<bool?> get inverted =>
      then((value) => switch (value) { bool() => !value, null => null });

  Future<bool> get isTrue => then((value) => value == true);
  Future<bool> get isFalse => then((value) => value == false);
  Future<bool> get isNull => then((value) => value == null);
}

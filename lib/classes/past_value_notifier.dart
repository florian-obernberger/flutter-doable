import 'package:flutter/material.dart';

class PastValueNotifier<T> extends ValueNotifier<T> {
  PastValueNotifier(super._value);

  T? _previous;

  T? get previous => _previous;

  @override
  set value(T newValue) {
    _previous = value;
    super.value = newValue;
  }
}

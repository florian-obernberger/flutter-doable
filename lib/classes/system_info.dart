import 'dart:io';

import 'package:device_info_plus/device_info_plus.dart';

/// Provides unified system information for Android, Linux and Windows.
class SystemInfo {
  SystemInfo._();

  static final instance = SystemInfo._();

  late final BaseDeviceInfo deviceInfo;
  bool _isInitialized = false;

  Future<void> init([void Function()? callback]) async {
    if (_isInitialized) return;

    final plugin = DeviceInfoPlugin();

    if (Platform.isAndroid) {
      deviceInfo = await plugin.androidInfo;
    } else if (Platform.isLinux) {
      deviceInfo = await plugin.linuxInfo;
    } else if (Platform.isWindows) {
      deviceInfo = await plugin.windowsInfo;
    }

    _isInitialized = true;
    callback?.call();
  }

  bool get supportsDarkMode => switch (deviceInfo) {
        AndroidDeviceInfo(version: final version) => version.sdkInt >= 28,
        _ => false,
      };

  bool get supportsDynamicColor {
    final info = deviceInfo;
    return switch (info) {
      AndroidDeviceInfo() => info.version.sdkInt >= 31,
      _ => false,
    };
  }

  bool get hasClipboardPopup {
    final info = deviceInfo;
    return switch (info) {
      AndroidDeviceInfo() => info.version.sdkInt >= 33,
      _ => false,
    };
  }

  bool get supportsSystemFont {
    final info = deviceInfo;
    return switch (info) {
      AndroidDeviceInfo() => info.version.sdkInt >= 34,
      _ => false,
    };
  }

  String get deviceModelName {
    final info = deviceInfo;
    return switch (info) {
      AndroidDeviceInfo() => info.model,
      LinuxDeviceInfo() => info.prettyName,
      WindowsDeviceInfo() => info.computerName,
      _ => '',
    };
  }
}

class Motion {
  const Motion._();

  /// Equivalent to 50 ms.
  @Deprecated('Use Durations.short1 instead')
  static const short1 = Duration(milliseconds: 50);

  /// Equivalent to 100 ms.
  @Deprecated('Use Durations.short2 instead')
  static const short2 = Duration(milliseconds: 100);

  /// Equivalent to 150 ms.
  @Deprecated('Use Durations.short3 instead')
  static const short3 = Duration(milliseconds: 150);

  /// Equivalent to 200 ms.
  @Deprecated('Use Durations.short4 instead')
  static const short4 = Duration(milliseconds: 200);

  /// Equivalent to 250 ms.
  @Deprecated('Use Durations.medium1 instead')
  static const medium1 = Duration(milliseconds: 250);

  /// Equivalent to 300 ms.
  @Deprecated('Use Durations.medium2 instead')
  static const medium2 = Duration(milliseconds: 300);

  /// Equivalent to 350 ms.
  @Deprecated('Use Durations.medium3 instead')
  static const medium3 = Duration(milliseconds: 350);

  /// Equivalent to 400 ms.
  @Deprecated('Use Durations.medium4 instead')
  static const medium4 = Duration(milliseconds: 400);

  /// Equivalent to 450 ms.
  @Deprecated('Use Durations.long1 instead')
  static const long1 = Duration(milliseconds: 450);

  /// Equivalent to 500 ms.
  @Deprecated('Use Durations.long2 instead')
  static const long2 = Duration(milliseconds: 500);

  /// Equivalent to 550 ms.
  @Deprecated('Use Durations.long3 instead')
  static const long3 = Duration(milliseconds: 550);

  /// Equivalent to 600 ms.
  @Deprecated('Use Durations.long4 instead')
  static const long4 = Duration(milliseconds: 600);

  /// Equivalent to 700 ms.
  @Deprecated('Use Durations.extralong1 instead')
  static const extraLong1 = Duration(milliseconds: 700);

  /// Equivalent to 800 ms.
  @Deprecated('Use Durations.extralong2 instead')
  static const extraLong2 = Duration(milliseconds: 800);

  /// Equivalent to 900 ms.
  @Deprecated('Use Durations.extralong3 instead')
  static const extraLong3 = Duration(milliseconds: 900);

  /// Equivalent to 1000 ms.
  @Deprecated('Use Durations.extralong4 instead')
  static const extraLong4 = Duration(milliseconds: 1000);
}

import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:device_info_plus/device_info_plus.dart';
import 'package:flutter/material.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:result/anyhow.dart';

import '/state/settings.dart';
import '/types/abi.dart';

import 'system_info.dart';
import 'logger.dart';

final class BugReport {
  static String _jsonEncode(Object source) =>
      const JsonEncoder.withIndent('  ').convert(source);

  const BugReport({
    this.title,
    this.description,
    this.logs,
    this.settings,
    this.systemInfo,
  });

  static Future<BugReport> fromContext(
    BuildContext context, {
    String? title,
    String? description,
    bool includeLogs = true,
    bool includeSettings = true,
    bool includeSystemInfo = true,
  }) async {
    List<LogEntry>? logs;
    SettingsData? settingsData;
    Map<String, Map<String, dynamic>>? systemInfo;

    final language = Localizations.localeOf(context).toLanguageTag();
    final settings = Settings.of(context);

    if (includeLogs) {
      logs = logger.logs.toList()
        ..sort((a, b) => b.logTime.compareTo(a.logTime));
    }

    if (includeSettings) {
      settingsData = settings.export();
    }

    if (includeSystemInfo) {
      final packageInfo = await PackageInfo.fromPlatform();
      final deviceInfo = SystemInfo.instance.deviceInfo;
      final abi = Abi.fromIndex(packageInfo.buildNumber.split('').last)
          .context('Could not parse Abi from package info');

      if (abi case Err(:final error)) {
        logger.ah(error, level: LogLevel.debug, tag: 'BugReport');
      }

      systemInfo = {
        if (deviceInfo case AndroidDeviceInfo())
          'android': <String, dynamic>{
            if (deviceInfo.version.baseOS != null)
              'baseOs': deviceInfo.version.baseOS!,
            'sdkInt': deviceInfo.version.sdkInt,
            'brand': deviceInfo.brand,
            'model': deviceInfo.model,
          },
        'doable': <String, String>{
          'version': packageInfo.version,
          'abi': abi.unwrapOr(Abi.universal).name,
          'language': language,
        }
      };
    }

    return BugReport(
      title: title,
      description: description,
      logs: logs,
      settings: settingsData,
      systemInfo: systemInfo,
    );
  }

  /// The optional title of the bug report.
  ///
  /// Equates to the file name if shared as a file and the codeberg issue title
  /// if an issue is created.
  final String? title;

  /// The optional description of the report. Since this is user provided it
  /// will most likely only exist in issues and not in a standard bug report.
  final String? description;

  /// The optional logs that are associated with this bug report.
  final List<LogEntry>? logs;

  /// The optional settings of Doable.
  final SettingsData? settings;

  /// The optional information about the users device.
  final Map<String, Map<String, dynamic>>? systemInfo;

  Map<String, dynamic> toMap({bool includeText = false}) {
    return {
      if (includeText) ...{
        if (title != null) 'title': title,
        if (description != null) 'description': description,
      },
      if (logs != null) 'logs': logs!.map((log) => log.toMap()).toList(),
      if (settings != null) 'settings': settings!,
      if (systemInfo != null) 'systemInfo': systemInfo!
    };
  }

  String toJson() => _jsonEncode(toMap());

  Uint8List toBytes() => utf8.encode(toJson());

  String codebergIssueBody() {
    final buffer = StringBuffer();

    if (description != null) buffer.writeln(description);
    if (logs != null) {
      _writeJson(
        buffer,
        // Limiting the amount of logs to a max 15 to avoid creating too large
        // of an url. Stacktraces are also being compressed since they take up
        // the majority of space but are important to figure out where
        // unexpected errors occurred.
        logs!.take(15).map(_compressLogEntry).toList(),
        'Logs',
      );
    }

    if (settings != null) {
      _writeJson(buffer, settings!, 'Settings');
    }

    if (systemInfo != null) {
      _writeJson(buffer, systemInfo!, 'System info');
    }

    return buffer.toString();
  }

  Map<String, dynamic> _compressLogEntry(LogEntry entry) {
    final map = entry.toMap();
    final stackTrace = map['stackTrace'] as String?;
    if (stackTrace != null) map['stackTrace'] = _compress(stackTrace);
    return map;
  }

  String _compress(String json) {
    final enCodedJson = utf8.encode(json);
    final gZipJson = gzip.encode(enCodedJson);
    return base64.encode(gZipJson);
  }

  void _writeJson(StringBuffer buffer, Object object, String title) {
    buffer.writeln('<details>');
    buffer.writeln('<summary>$title</summary>');
    buffer.writeln('\n```json');
    buffer.writeln(_jsonEncode(object));
    buffer.writeln('```');
    buffer.writeln('</details>');
  }
}

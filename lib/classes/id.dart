import 'package:flutter/foundation.dart' show Key, ValueKey;
import 'package:uuid/v4.dart';

typedef Validator<T extends Object> = bool Function(T value);

sealed class BaseId<T extends Object> {
  static const _uuid = UuidV4();

  final T value;

  const BaseId(this.value);

  bool validate(Validator<T> validator) => validator(value);
  bool validateAll(Iterable<Validator<T>> validators) {
    for (final validator in validators) {
      if (!validator(value)) return false;
    }

    return true;
  }

  @override
  bool operator ==(Object other) => switch (other) {
        BaseId() => value == other.value,
        _ => false,
      };

  @override
  int get hashCode => value.hashCode;

  @override
  String toString() => '$runtimeType($value)';

  Key get key => ValueKey(value);
}

typedef Uuid = Id<String>;

class Id<T extends Comparable<T>> extends BaseId<T>
    implements Comparable<Id<T>> {
  const Id(super.value);

  static Uuid uuid() => Uuid(BaseId._uuid.generate());

  @override
  int compareTo(Id<T> other) => value.compareTo(other.value);
}

class NumId<T extends num> extends BaseId<T> implements Comparable<NumId> {
  const NumId(super.value);

  static NumId<int> generate() => NumId<int>(BaseId._uuid.generate().hashCode);

  @override
  int compareTo(NumId<num> other) => value.compareTo(other.value);
}

class IdGenerator<T extends Comparable<T>> {
  final List<Validator>? validators;
  final T Function(T previous) generator;
  final T initial;
  T _previous;

  IdGenerator(
    this.initial,
    this.generator, {
    this.validators,
    T? initialPrevious,
  }) : _previous = initialPrevious ?? initial;

  Id<T> generate() {
    if (validators?.isEmpty ?? true) return Id(generator(_previous));

    T next = _previous;
    while (!_checkValue(next)) {
      next = generator(next);
    }

    _previous = next;
    return Id(next);
  }

  bool _checkValue(T value) {
    if (value.compareTo(_previous) == 0) return false;
    if (validators == null) return true;
    return !validators!.any((v) => !v(value));
  }
}

extension ValidatorFunctions<T extends Object> on Validator<T> {
  bool validate(BaseId<T> id) => this(id.value);
  bool validateAll(Iterable<BaseId<T>> ids) =>
      ids.map((id) => id.value).any(this);
}

extension NextIntId on NumId<int> {
  NumId<int> next([int offset = 1]) => NumId(value + offset);
}

extension NextDoubleId on NumId<double> {
  NumId<double> next([double offset = 1.0]) => NumId(value + offset);
}

extension NextNumId on NumId<num> {
  NumId<num> next([num offset = 1]) => NumId(value + offset);
}

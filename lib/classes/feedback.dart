import 'package:flutter/services.dart';
import 'package:flutter_email_sender/flutter_email_sender.dart';
import 'package:path_provider/path_provider.dart';
import 'package:result/anyhow.dart';

import '/data/applications_directory.dart';
import '/state/secure_storage.dart';
import '/util/launch.dart';
import '/types/date_and_time_formats.dart';

import 'bug_report.dart';
import 'logger.dart';

enum FeedbackWay { email, codeberg }

enum FeedbackExtra { systemInfo, logOutputs, settings }

const _issueUrl =
    'https://codeberg.org/florian-obernberger/flutter-doable/issues/new';

String _newCodebergIssue(BugReport report) {
  return Uri.encodeFull(
    '$_issueUrl?title=[FEEDBACK]: ${report.title}&body=${report.codebergIssueBody()}',
  );
}

FutureResult<()> giveFeedback({
  required FeedbackWay way,
  required BugReport report,
  required SecureStorageDatabase secureStorage,
  required bool inAppBrowser,
}) async {
  switch (way) {
    case FeedbackWay.email:
      final tmp = await getTemporaryDirectory();
      final timeStamp = DateFormat.international.formatWithTime(
        DateTime.now(),
        TimeFormat.european,
        null,
        'T',
      );

      final reportFile = tmp.join('Bug_Report_$timeStamp.json');
      await reportFile.create();
      await reportFile.writeAsBytes(report.toBytes());

      final mail = Email(
        subject: '[FEEDBACK]: ${report.title}',
        body: report.description ?? '',
        recipients: const ['issues@doable.at'],
        isHTML: false,
        attachmentPaths: [reportFile.path],
      );

      try {
        await FlutterEmailSender.send(mail);

        return const Ok(());
      } on MissingPluginException catch (e) {
        logger.e(message: 'Tried to send issue mail.', e);
        return bail(e);
      } finally {
        await reportFile.delete();
      }
    case FeedbackWay.codeberg:
      return launchUrl(
        _newCodebergIssue(report),
        inAppBrowser: inAppBrowser,
      ).context('Could not launch feedback url');
  }
}

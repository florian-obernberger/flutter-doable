abstract class Syncable<T> {
  String get id;

  /// The time the Syncable was last modified.
  DateTime get lastModified;

  /// Weather the Syncable had it's initial sync or not.
  bool get hadInitialSync;

  T copyWith({
    DateTime? lastModified,
    bool? hadInitialSync,
    String? id,
  });
}

/// Functions of this type should convert the given data into a Toml string.
typedef SyncableExporter<T extends Syncable<T>> = String Function(
    Iterable<T> data);

typedef SyncableImporter<T extends Syncable<T>> = Iterable<T> Function(
    Map<String, dynamic> toml);

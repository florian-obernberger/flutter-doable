import 'package:format/format.dart';
import 'package:result/result.dart';
import 'package:result/anyhow.dart' as anyhow;

import '/data/logger.dart';

export '/data/logger.dart';

late final Logger logger;

Future<void> initLogger() async {
  logger = await Logger.init();
}

extension LogErr<T, E> on Err<T, E> {
  void log(LogLevel level, [String? message, String? tag]) =>
      logger.log(level, error: error, message: message, tag: tag);
}

extension AnyhowLogError on Logger {
  void ah(
    anyhow.Error error, {
    LogLevel level = LogLevel.error,
    String? message,
    StackTrace? stack,
    String? tag,
  }) =>
      logger.log(
        level,
        error: '{:?}'.format(error),
        message: message ?? '{}'.format(error),
        stackTrace: stack ?? error.stackTrace,
        tag: tag,
      );
}

extension LogAnyhowError on anyhow.Error {
  void log({LogLevel level = LogLevel.error, String? message, String? tag}) =>
      logger.ah(this, level: level, message: message, tag: tag);
}

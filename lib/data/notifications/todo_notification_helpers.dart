part of 'todo_notification.dart';

NotifId todoRelevantId(DateTime relevantDate, TodoDuration duration) {
  int value = relevantDate.millisecondsSinceEpoch;
  value += duration.inMilliseconds;

  value %= NotifId.maxValue;
  value *= -1;

  return NotifId.guard(value);
}

/// Adds all relevant notifications to a given Todo and returns the ones added.
List<TodoNotifMeta> addRelevantNotificationsToTodo(
  Todo todo,
  ExtensionsPreferences extensions,
) {
  if (todo.relevantDate == null) return [];

  final relevantDateTime = todo.relevantDateTime!;
  final durations =
      extensions.defaultNotifications.value.getListOrGeneral(todo.listId);
  final defaultTime = extensions.defaultNotificationTime.value;
  final relevantTime = todo.relevantTime;

  final scheduledDate = TZDateTime.from(
    relevantDateTime.copyWith(
      hour: relevantTime?.hour ?? defaultTime.hour,
      minute: relevantTime?.minute ?? defaultTime.minute,
      second: 0,
      millisecond: 0,
      microsecond: 0,
    ),
    local,
  );

  final metadata = <TodoNotifMeta>[];

  for (final duration in durations) {
    final meta = TodoNotifMeta(
      todoRelevantId(relevantDateTime, duration),
      scheduledDate,
      isRelative: true,
      isAutomatic: true,
      duration: duration,
    );

    todo.addNotification(meta);
    metadata.add(meta);
  }

  return metadata;
}

void removeRelevantNotificationsFromTodo(
  Todo todo, {
  bool cancelNotifications = true,
}) {
  if (todo.relevantDate == null) return;

  final relevantDateTime = todo.relevantDateTime!;

  for (final duration in TodoDuration.values) {
    final id = todoRelevantId(relevantDateTime, duration);
    if (cancelNotifications) BaseNotification.cancelId(id.value);
    todo.removeNotification(id);
  }
}

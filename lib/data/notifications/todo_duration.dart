part of 'todo_notification.dart';

extension type const TodoDuration._(Duration _duration)
    implements Duration, Comparable<Duration> {
  TodoDuration.fromDuration(this._duration)
      : assert(_values.contains(_duration));

  factory TodoDuration.fromMicroseconds(int microseconds) =>
      TodoDuration.fromDuration(Duration(microseconds: microseconds));

  static const _aWeekBefore = Duration(days: -DateTime.daysPerWeek);
  static const aWeekBefore = TodoDuration._(_aWeekBefore);

  static const _aDayBefore = Duration(days: -1);
  static const aDayBefore = TodoDuration._(_aDayBefore);

  static const _aDayAfter = Duration(days: 1);
  static const aDayAfter = TodoDuration._(_aDayAfter);

  static const _anHourBefore = Duration(hours: -1);
  static const anHourBefore = TodoDuration._(_anHourBefore);

  static const _halfHourBefore = Duration(minutes: -30);
  static const halfHourBefore = TodoDuration._(_halfHourBefore);

  static const _halfHourAfter = Duration(minutes: 30);
  static const halfHourAfter = TodoDuration._(_halfHourAfter);

  static const _atTimeOfEvent = Duration(microseconds: 0);
  static const atTimeOfEvent = TodoDuration._(_atTimeOfEvent);

  static const _values = <Duration>[
    _aWeekBefore,
    _aDayBefore,
    _aDayAfter,
    _anHourBefore,
    _halfHourBefore,
    _halfHourAfter,
    _atTimeOfEvent,
  ];

  static const values = <TodoDuration>[
    aWeekBefore,
    aDayBefore,
    anHourBefore,
    halfHourBefore,
    atTimeOfEvent,
    halfHourAfter,
    aDayAfter,
  ];

  String localized(BuildContext context) {
    final strings = Strings.of(context)!;

    return switch (this) {
      aWeekBefore => strings.aWeekBefore,
      aDayBefore => strings.aDayBefore,
      aDayAfter => strings.aDayAfter,
      anHourBefore => strings.anHourBefore,
      halfHourBefore => strings.halfHourBefore,
      halfHourAfter => strings.halfHourAfter,
      atTimeOfEvent => strings.atTimeOfTodo,
      _ when !values.contains(this) => throw ArgumentError.value(
          this, 'Valid TodoDuration that did not get matched'),
      _ => formatDuration(context, _duration, applyGermanFix: false),
    };
  }
}

part of 'todo_notification.dart';

final class TodoNotifMetaAdapter implements TypeAdapter<TodoNotifMeta> {
  @override
  TodoNotifMeta read(BinaryReader reader) {
    final id = reader.read() as NotifId;
    final scheduledDate = reader.read() as TZDateTime;
    final isRelative = reader.readBool();
    final isAutomatic = reader.readBool();
    final duration = reader.readInt();

    return TodoNotifMeta(
      id,
      scheduledDate,
      isRelative: isRelative,
      isAutomatic: isAutomatic,
      duration: duration == -1
          ? null
          : TodoDuration._(Duration(microseconds: duration)),
    );
  }

  @override
  void write(BinaryWriter writer, TodoNotifMeta obj) {
    writer
      ..write(obj.id)
      ..write(obj.scheduledDate)
      ..writeBool(obj.isRelative)
      ..writeBool(obj.isAutomatic)
      ..writeInt(obj.duration?.inMicroseconds ?? -1);
  }

  @override
  int get typeId => 55;
}

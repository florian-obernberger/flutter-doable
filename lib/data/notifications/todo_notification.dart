import 'dart:async';

import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart' hide Notification;
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:markdown/markdown.dart';
import 'package:timezone/timezone.dart';
import 'package:hive_flutter/adapters.dart';

import '/data/todo_list.dart';
import '/data/todo.dart';
import '/remote/handle_io_exception.dart';
import '/state/settings.dart';
import '/state/todo_list_db.dart';
import '/strings/strings.dart';
import '/util/extensions/color.dart';
import '/util/formatters/format_duration.dart';

import '../base_notification.dart';

part 'todo_duration.dart';
part 'todo_notification_helpers.dart';
part 'todo_notification.g.dart';

enum TodoNotificationAction {
  complete(_completeId),
  snooze(_snoozeId);

  static const _completeId = 'at.doable.complete_todo';
  static const _snoozeId = 'at.doable.snooze';

  const TodoNotificationAction(this.id);

  final String id;

  static TodoNotificationAction? fromId(String? id) => switch (id) {
        _completeId => complete,
        _snoozeId => snooze,
        _ => null,
      };
}

final class TodoNotifMeta extends NotifMeta with EquatableMixin {
  final bool isRelative;

  /// Weather this was created automatically by Doable or by the user.
  final bool isAutomatic;
  final TodoDuration? duration;

  const TodoNotifMeta(
    super.id,
    super.scheduledDate, {
    this.isRelative = false,
    this.isAutomatic = false,
    this.duration,
  }) : assert(!isRelative || duration != null);

  /// Creates a new [TodoNotifMeta] that starts [duration]
  /// from now.
  TodoNotifMeta snooze(Duration duration) => TodoNotifMeta(
        id,
        TZDateTime.now(local).add(duration),
        isRelative: isRelative,
        isAutomatic: isAutomatic,
        duration: this.duration,
      );

  @override
  bool shouldNotify() => scheduledDate.isAfter(
        DateTime.now().add(const Duration(seconds: 5)),
      );

  @override
  List<Object?> get props => [
        id.value,
        scheduledDate,
        isRelative,
        isAutomatic,
        duration,
      ];

  TodoNotifMeta copyWith({
    NotifId? id,
    TZDateTime? scheduledDate,
    bool? isRelative,
    bool? isAutomatic,
    TodoDuration? duration,
  }) =>
      TodoNotifMeta(
        id ?? this.id,
        scheduledDate ?? this.scheduledDate,
        isRelative: isRelative ?? this.isRelative,
        isAutomatic: isAutomatic ?? this.isAutomatic,
        duration: duration ?? this.duration,
      );
}

class TodoNotifStrings {
  final String markCompleted;
  final String snooze;
  final String channelName;

  const TodoNotifStrings({
    required this.markCompleted,
    required this.snooze,
    required this.channelName,
  });

  factory TodoNotifStrings.fromStrings(Strings strings) => TodoNotifStrings(
        channelName: strings.notifyTodosChannelName,
        markCompleted: strings.markCompleted,
        snooze: strings.snooze,
      );

  factory TodoNotifStrings.fromMap(Map<String, String> map) => TodoNotifStrings(
        markCompleted: map['markCompleted'] as String,
        snooze: map['snooze'] as String,
        channelName: map['channelName'] as String,
      );

  Map<String, String> toMap() => {
        'markCompleted': markCompleted,
        'snooze': snooze,
        'channelName': channelName,
      };
}

class MinimalTodoNotification extends BaseNotification {
  static const _notificationSound = RawResourceAndroidNotificationSound(
    'task_notification',
  );
  static const _channelId = 'ic_notification_todos';

  final TodoNotifMeta _metadata;
  final Todo todo;
  final TodoList? list;
  final TodoNotifStrings strings;

  final ExtensionsPreferences _extensions;
  final Brightness _brightness;

  const MinimalTodoNotification(
    TodoNotifMeta super.metadata, {
    required this.todo,
    required this.list,
    required this.strings,
    required ExtensionsPreferences extensions,
    required Brightness platformBrightness,
  })  : _metadata = metadata,
        _extensions = extensions,
        _brightness = platformBrightness;

  @override
  NotifMeta get metadata {
    if (!_metadata.isRelative || todo.relevantDate == null) return _metadata;
    final date = todo.relevantDateTime!.add(_metadata.duration!);

    return NotifMeta(_metadata.id, TZDateTime.from(date, local));
  }

  @override
  String getPayload() => todo.id;

  @override
  ({String? description, String title}) getInfo({required bool isAndroid}) {
    String title = todo.title;
    String? description = todo.description;

    if (_extensions.enableMarkdown.value && isAndroid) {
      title = markdownToHtml(title);
      if (description != null) description = markdownToHtml(description);
    }

    return (title: title, description: description);
  }

  @override
  Future<AndroidNotificationDetails> getDetails() async {
    final enableHtml = _extensions.enableMarkdown.value;
    late final StyleInformation styleInformation;
    late final AndroidBitmap<String>? image;

    if (_extensions.enableImages.value && todo.image != null) {
      image = FilePathAndroidBitmap(todo.image!);

      styleInformation = BigPictureStyleInformation(
        image,
        htmlFormatContent: enableHtml,
        htmlFormatTitle: enableHtml,
        htmlFormatContentTitle: enableHtml,
        htmlFormatSummaryText: enableHtml,
        hideExpandedLargeIcon: true,
        largeIcon: image,
      );
    } else {
      styleInformation = DefaultStyleInformation(enableHtml, enableHtml);
      image = null;
    }

    final color = list?.color?.listSchemeFromBrightness(_brightness).primary;

    final (channelId, channelName) = await _createChannel();

    return AndroidNotificationDetails(
      channelId,
      channelName,
      channelAction: AndroidNotificationChannelAction.update,
      groupKey: list?.id,
      channelShowBadge: true,
      importance: Importance.high,
      category: AndroidNotificationCategory.event,
      priority: Priority.high,
      colorized: false,
      color: color,
      visibility: NotificationVisibility.public,
      styleInformation: styleInformation,
      largeIcon: image,
      sound: _notificationSound,
      playSound: _extensions.enableNotificationSound.value,
      actions: <AndroidNotificationAction>[
        AndroidNotificationAction(
          TodoNotificationAction.complete.id,
          strings.markCompleted,
          cancelNotification: true,
          contextual: false,
        ),
        // AndroidNotificationAction(
        //   TodoNotificationAction.snooze.id,
        //   strings.snooze,
        //   cancelNotification: true,
        //   contextual: false,
        // ),
      ],
    );
  }

  @override
  bool shouldNotify() =>
      _extensions.enableNotifications.value && _metadata.shouldNotify();

  Future<(String, String)> _createChannel() async {
    final channelName = strings.channelName;

    if (Platform.isAndroid) {
      await FlutterLocalNotificationsPlugin()
          .resolvePlatformSpecificImplementation<
              AndroidFlutterLocalNotificationsPlugin>()!
          .createNotificationChannel(AndroidNotificationChannel(
            _channelId,
            channelName,
            sound: _notificationSound,
            playSound: _extensions.enableNotificationSound.value,
          ));
    }

    return (_channelId, channelName);
  }
}

final class TodoNotification extends MinimalTodoNotification {
  const TodoNotification._(
    super.metadata, {
    required super.todo,
    required super.list,
    required super.extensions,
    required super.strings,
    required super.platformBrightness,
  });

  factory TodoNotification.from(
    TodoNotifMeta metadata,
    WidgetRef ref,
    Todo todo,
  ) {
    final context = ref.context;

    return TodoNotification._(
      metadata,
      todo: todo,
      list: ref.read(todoListDatabaseProvider).get(todo.listId ?? ''),
      extensions: Settings.of(context).extensions,
      strings: TodoNotifStrings.fromStrings(Strings.of(context)!),
      platformBrightness: MediaQuery.of(context).platformBrightness,
    );
  }
}

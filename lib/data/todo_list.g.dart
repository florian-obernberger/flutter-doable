// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'todo_list.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class TodoListAdapter extends TypeAdapter<TodoList> {
  @override
  final int typeId = 4;

  @override
  TodoList read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return TodoList(
      id: fields[0] as String?,
      creationDate: fields[4] as DateTime?,
      lastModified: fields[5] as DateTime?,
      hadInitialSync: fields[6] as bool?,
      name: fields[1] as String,
      hiveIcon: fields[2] as HiveIcon,
      selectedHiveIcon: fields[3] as HiveIcon,
      color: fields[7] as Color?,
      isHidden: fields[8] as bool?,
    );
  }

  @override
  void write(BinaryWriter writer, TodoList obj) {
    writer
      ..writeByte(9)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.name)
      ..writeByte(2)
      ..write(obj.hiveIcon)
      ..writeByte(3)
      ..write(obj.selectedHiveIcon)
      ..writeByte(4)
      ..write(obj.creationDate)
      ..writeByte(5)
      ..write(obj.lastModified)
      ..writeByte(6)
      ..write(obj.hadInitialSync)
      ..writeByte(7)
      ..write(obj.color)
      ..writeByte(8)
      ..write(obj.isHidden);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is TodoListAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

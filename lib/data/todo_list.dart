import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:toml/toml.dart';
import 'package:uuid/uuid.dart';

import '/classes/syncable.dart';

import 'hive_icon.dart';
// import 'mappable.dart';

part 'todo_list.g.dart';

const _uuid = Uuid();

@immutable
@HiveType(typeId: 4)
class TodoList with EquatableMixin implements Syncable<TodoList> {
  TodoList({
    String? id,
    DateTime? creationDate,
    DateTime? lastModified,
    bool? hadInitialSync,
    required this.name,
    required this.hiveIcon,
    required this.selectedHiveIcon,
    this.color,
    bool? isHidden,
  })  : id = id ?? _uuid.v4(),
        creationDate = creationDate ?? DateTime.now(),
        lastModified = lastModified ?? DateTime.now(),
        hadInitialSync = hadInitialSync ?? false,
        isHidden = isHidden ?? false;

  TodoList.fromName(this.name, {this.color, this.isHidden = false})
      : hiveIcon = const HiveIcon(0xf091, fontFamily: 'MaterialIcons'),
        selectedHiveIcon = const HiveIcon(0xe2a3, fontFamily: 'MaterialIcons'),
        id = _uuid.v4(),
        creationDate = DateTime.now(),
        lastModified = DateTime.now(),
        hadInitialSync = false;

  /// Unique identifier.
  @override
  @HiveField(0)
  final String id;

  /// User facing name.
  @HiveField(1)
  final String name;

  /// Icon representing the [TodoList].
  @HiveField(2)
  final HiveIcon hiveIcon;

  /// Icon representing the [TodoList] when it's selected.
  @HiveField(3)
  final HiveIcon selectedHiveIcon;

  /// The creation [DateTime] of the [TodoList].
  @HiveField(4)
  final DateTime creationDate;

  /// The time this [TodoList] was last modified.
  @override
  @HiveField(5)
  final DateTime lastModified;

  @override
  @HiveField(6)
  final bool hadInitialSync;

  IconData get icon => hiveIcon.icon;

  IconData get selectedIcon => selectedHiveIcon.icon;

  /// The color of the list.
  @HiveField(7)
  final Color? color;

  /// Weather Todos in this list are hidden when viewing all Todos or not.
  @HiveField(8)
  final bool isHidden;

  @override
  TodoList copyWith({
    String? id,
    String? name,
    HiveIcon? hiveIcon,
    HiveIcon? selectedHiveIcon,
    bool? hadInitialSync,
    DateTime? lastModified,
    DateTime? creationDate,
    Color? color,
    bool? isHidden,
  }) =>
      TodoList(
        id: id ?? this.id,
        name: name ?? this.name,
        hiveIcon: hiveIcon ?? this.hiveIcon,
        selectedHiveIcon: selectedHiveIcon ?? this.selectedHiveIcon,
        hadInitialSync: hadInitialSync ?? this.hadInitialSync,
        lastModified: lastModified ?? this.lastModified,
        creationDate: creationDate ?? this.creationDate,
        color: color ?? this.color,
        isHidden: isHidden ?? this.isHidden,
      );

  TodoList withColor(Color? color) => TodoList(
        id: id,
        name: name,
        hiveIcon: hiveIcon,
        selectedHiveIcon: selectedHiveIcon,
        hadInitialSync: hadInitialSync,
        lastModified: lastModified,
        creationDate: creationDate,
        color: color,
        isHidden: isHidden,
      );

  factory TodoList.fromMap(Map<String, dynamic> map) => TodoList(
        id: map['id'] as String,
        name: map['name'] as String,
        hiveIcon: HiveIcon.fromMap(map['hiveIcon']),
        selectedHiveIcon: HiveIcon.fromMap(map['selectedHiveIcon']),
        lastModified: _parseDateTime(map['lastModified']),
        creationDate: _parseDateTime(map['creationDate']),
        color: _parseColor(map['color']),
        isHidden: map['isHidden'],
      );

  Map<String, dynamic> toMap() => {
        'id': id,
        'name': name,
        'hiveIcon': hiveIcon.toMap(),
        'selectedHiveIcon': selectedHiveIcon.toMap(),
        'creationDate': creationDate.toIso8601String(),
        'lastModified': lastModified.toIso8601String(),
        'hadInitialSync': hadInitialSync,
        if (color != null) 'color': color!.export(),
        'isHidden': isHidden,
      };

  @override
  List<Object?> get props => [
        id,
        name,
        hiveIcon,
        selectedHiveIcon,
        hadInitialSync,
        lastModified,
        creationDate,
        color,
        isHidden,
      ];
}

DateTime? _parseDateTime(dynamic date) => switch (date) {
      String() => DateTime.parse(date),
      DateTime() => date,
      TomlOffsetDateTime() => date.toUtcDateTime(),
      null => null,
      _ => throw FormatException('What the fuck is $date?')
    };

extension ListColor on Color {
  List<int> export() => [red, green, blue, alpha];
}

Color? _parseColor(dynamic color) => switch (color) {
      Color() => color,
      <int>[final red, final green, final blue, final alpha] => Color.fromARGB(
          alpha,
          red,
          green,
          blue,
        ),
      List() when color.length == 4 && color.first is int => _parseColor(
          color.cast<int>(),
        ),
      String() => color.split(',').length == 4
          ? _parseColor(color.split(',').map((e) => int.parse(e)))
          : null,
      null => null,
      _ => throw FormatException('What the fuck is $color?'),
    };

import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:format/format.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:result/result.dart';
import 'package:uuid/uuid.dart';

import '/util/extensions/string.dart';
import '/types/term_color.dart';

part 'logger.g.dart';

const _uuid = Uuid();

@HiveType(typeId: 6)
enum LogLevel {
  @HiveField(0)
  info(800),
  @HiveField(1)
  warn(900),
  @HiveField(2)
  error(1000),
  @HiveField(3)
  log(700),
  @HiveField(4)
  debug(300),
  @HiveField(5)
  wtf(1200);

  const LogLevel(this.level);

  final int level;
}

@HiveType(typeId: 5)
class LogEntry implements Comparable<LogEntry>, Formatable {
  LogEntry(
    this.level,
    this.logTime, {
    this.error,
    this.stackTrace,
    this.message,
    this.more,
    this.tag,
    String? id,
  }) : id = id ?? _uuid.v4();

  /// The id of the entry.
  @HiveField(0)
  final String id;

  /// The level of the log event.
  @HiveField(1)
  final LogLevel level;

  /// The error / exception that was thrown.
  @HiveField(2)
  final String? error;

  /// The stacktrace of the error / exception.
  @HiveField(3)
  final String? stackTrace;

  /// Additional message.
  @HiveField(4)
  final String? message;

  /// Optional customizable fields.
  @HiveField(5)
  final Map<String, String>? more;

  /// When the log occurred.
  @HiveField(6)
  final DateTime logTime;

  /// An optional tag that can be used to show the relations between the logs.
  @HiveField(7)
  final String? tag;

  @override
  String toString() => '{}'.format(this);

  Map<String, dynamic> toMap() {
    return {
      'level': level.name,
      'time': '$logTime',
      if (tag != null) 'tag': tag!,
      if (message != null) 'message': message!,
      if (error != null) 'error': error!,
      if (stackTrace != null) 'stackTrace': stackTrace!,
      if (more != null) 'more': more!,
    };
  }

  @override
  int compareTo(LogEntry other) => other.logTime.compareTo(logTime);

  @override
  String format(FormatOptions options) {
    if (options.debug && options.alternate) {
      return const JsonEncoder.withIndent('  ').convert(toMap());
    }

    final sb = StringBuffer();
    int requiredSpace = level.name.length + 2; // 2 → `: `

    if (options.alternate) {
      requiredSpace += 3; // approx width of emoji + sapce

      sb.write(switch (level) {
        LogLevel.info => TermColors.blue,
        LogLevel.warn => TermColors.yellow,
        LogLevel.error => TermColors.red,
        LogLevel.log => TermColors.reset,
        LogLevel.debug => TermColors.red,
        LogLevel.wtf => TermColors.magenta,
      });

      sb.write(switch (level) {
        LogLevel.info => '📘 ',
        LogLevel.warn => '📙 ',
        LogLevel.error => '📕 ',
        LogLevel.log => '📔 ',
        LogLevel.debug => '📕 ',
        LogLevel.wtf => '📓 ',
      });
    }

    sb.write(level.name.toCapitalized());

    if (message != null || options.alternate) {
      sb.write(': ');
      if (options.alternate) sb.write(TermColors.reset);
      sb.writeln(message ?? '???');
    }

    if (error case String error) {
      sb
        ..write('\n')
        ..write(' ' * requiredSpace)
        ..writeAll(error.split('\n'), '\n${' ' * requiredSpace}');
    }

    if (more != null) {
      sb
        ..write('\n')
        ..write(' ' * requiredSpace)
        ..writeAll(more.toString().split('\n'), '\n${' ' * requiredSpace}');
    }

    if (stackTrace case String stack) {
      sb
        ..write('\n')
        ..write(' ' * requiredSpace)
        ..writeAll(stack.split('\n'), '\n${' ' * requiredSpace}');
    }

    return sb.toString();
  }
}

final class Logger extends ChangeNotifier {
  static const _boxName = 'logs';

  final String? defaultTag;
  final Box<LogEntry>? _box;

  Logger._(this._box, [this.defaultTag]);

  static Future<Logger> init({String? defaultTag}) async {
    final box = await Hive.openBox<LogEntry>(_boxName);
    return Logger._(box, defaultTag);
  }

  Logger taggedLogger(String defaultTag) => Logger._(_box, defaultTag);

  void e(
    Object error, {
    DateTime? logTime,
    StackTrace? stackTrace,
    String? message,
    String? tag,
    Map<String, String>? more,
  }) =>
      log(
        LogLevel.error,
        logTime: logTime,
        error: error,
        stackTrace: stackTrace,
        message: message,
        tag: tag,
        more: more,
      );

  void i(
    String message, {
    DateTime? logTime,
    Object? error,
    StackTrace? stackTrace,
    String? tag,
    Map<String, String>? more,
  }) =>
      log(
        LogLevel.info,
        logTime: logTime,
        message: message,
        error: error,
        stackTrace: stackTrace,
        tag: tag,
        more: more,
      );

  void w(
    String message, {
    DateTime? logTime,
    Object? error,
    StackTrace? stackTrace,
    String? tag,
    Map<String, String>? more,
  }) =>
      log(
        LogLevel.info,
        logTime: logTime,
        message: message,
        error: error,
        stackTrace: stackTrace,
        tag: tag,
        more: more,
      );

  void log(
    LogLevel level, {
    DateTime? logTime,
    Object? error,
    StackTrace? stackTrace,
    String? message,
    String? tag,
    Map<String, String>? more,
  }) async {
    final finalTag = tag ?? defaultTag;

    final entry = LogEntry(
      level,
      logTime ?? DateTime.now(),
      error: error?.toString(),
      stackTrace: stackTrace?.toString(),
      message: message,
      tag: finalTag,
      more: more,
    );

    if (kDebugMode) print(format('{:#}', entry));

    await _box!.put(entry.id, entry);
    notifyListeners();
  }

  Option<LogEntry> get(String id) {
    final entry = _box!.get(id);
    if (entry == null) return const None();
    return Some(entry);
  }

  Iterable<Option<LogEntry>> getAll(Iterable<String> ids) sync* {
    for (var id in ids) {
      yield get(id);
    }
  }

  Future<void> delete(String id) => _box!.delete(id).then(
        (_) => notifyListeners(),
      );

  Future<void> deleteAll(Iterable<String> ids) =>
      _box!.deleteAll(ids).then((_) => notifyListeners());

  Future<int> clear() => _box!.clear().then((value) {
        notifyListeners();
        return value;
      });

  Iterable<LogEntry> get logs => _box!.values;

  Iterable<LogEntry> getTag(String tag) => _box!.values.where(
        (entry) => entry.tag == tag,
      );
}

extension LogEntryIterable on Iterable<LogEntry> {
  Option<LogEntry> get latest => isEmpty
      ? const None()
      : Some(reduce((a, b) => a.logTime.isBefore(b.logTime) ? b : a));

  Option<LogEntry> get oldest => isEmpty
      ? const None()
      : Some(reduce((a, b) => a.logTime.isAfter(b.logTime) ? b : a));
}

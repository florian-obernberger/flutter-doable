part of 'base_notification.dart';

const _typeId = 50;

final class NotifIdAdapter extends TypeAdapter<NotifId> {
  @override
  NotifId read(BinaryReader reader) => NotifId(reader.readInt32());

  @override
  void write(BinaryWriter writer, NotifId obj) {
    writer.writeInt32(obj.value);
  }

  @override
  final int typeId = _typeId + 0;
}

final class NotifMetaAdapter extends TypeAdapter<NotifMeta> {
  @override
  NotifMeta read(BinaryReader reader) => NotifMeta(
        reader.read() as NotifId,
        reader.read() as TZDateTime,
      );

  @override
  void write(BinaryWriter writer, NotifMeta obj) {
    writer
      ..write(obj.id)
      ..write(obj.scheduledDate);
  }

  @override
  final int typeId = _typeId + 1;
}

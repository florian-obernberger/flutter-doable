/// Implementations of this class should also implement a factory method
/// called "fromMap". An example is provided here.
abstract class Mappable {
  factory Mappable.fromMap() {
    throw UnimplementedError();
  }

  Map<String, dynamic> toMap();
}

extension GetMapExtension<K, V> on Map<K, V> {
  V? get(K key) {
    if (containsKey(key)) return this[key];
    return null;
  }
}

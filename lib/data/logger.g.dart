// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'logger.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class LogEntryAdapter extends TypeAdapter<LogEntry> {
  @override
  final int typeId = 5;

  @override
  LogEntry read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return LogEntry(
      fields[1] as LogLevel,
      fields[6] as DateTime,
      error: fields[2] as String?,
      stackTrace: fields[3] as String?,
      message: fields[4] as String?,
      more: (fields[5] as Map?)?.cast<String, String>(),
      tag: fields[7] as String?,
      id: fields[0] as String?,
    );
  }

  @override
  void write(BinaryWriter writer, LogEntry obj) {
    writer
      ..writeByte(8)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.level)
      ..writeByte(2)
      ..write(obj.error)
      ..writeByte(3)
      ..write(obj.stackTrace)
      ..writeByte(4)
      ..write(obj.message)
      ..writeByte(5)
      ..write(obj.more)
      ..writeByte(6)
      ..write(obj.logTime)
      ..writeByte(7)
      ..write(obj.tag);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is LogEntryAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class LogLevelAdapter extends TypeAdapter<LogLevel> {
  @override
  final int typeId = 6;

  @override
  LogLevel read(BinaryReader reader) {
    switch (reader.readByte()) {
      case 0:
        return LogLevel.info;
      case 1:
        return LogLevel.warn;
      case 2:
        return LogLevel.error;
      case 3:
        return LogLevel.log;
      case 4:
        return LogLevel.debug;
      case 5:
        return LogLevel.wtf;
      default:
        return LogLevel.info;
    }
  }

  @override
  void write(BinaryWriter writer, LogLevel obj) {
    switch (obj) {
      case LogLevel.info:
        writer.writeByte(0);
        break;
      case LogLevel.warn:
        writer.writeByte(1);
        break;
      case LogLevel.error:
        writer.writeByte(2);
        break;
      case LogLevel.log:
        writer.writeByte(3);
        break;
      case LogLevel.debug:
        writer.writeByte(4);
        break;
      case LogLevel.wtf:
        writer.writeByte(5);
        break;
    }
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is LogLevelAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

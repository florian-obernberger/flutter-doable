// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'todo.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class TodoAdapter extends TypeAdapter<Todo> {
  @override
  final int typeId = 0;

  @override
  Todo read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Todo(
      creationDate: fields[1] as DateTime,
      relevantDate: fields[3] as DateTime?,
      relevantTime: fields[14] as TimeOfDay?,
      title: fields[4] as String,
      description: fields[5] as String?,
      isImportant: fields[6] == null ? false : fields[6] as bool?,
      isCompleted: fields[7] == null ? false : fields[7] as bool?,
      completedDate: fields[2] as DateTime?,
      lastModified: fields[8] as DateTime,
      id: fields[0] as String?,
      hadInitialSync: fields[9] as bool?,
      listId: fields[13] as String?,
      rRuleString: fields[15] as String?,
      hasRecurred: fields[17] as bool?,
      image: fields[16] as String?,
      progress: fields[18] as double?,
      notificationMetadata:
          (fields[19] as List?)?.cast<TodoNotifMeta>().toSet(),
    );
  }

  @override
  void write(BinaryWriter writer, Todo obj) {
    writer
      ..writeByte(17)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.creationDate)
      ..writeByte(2)
      ..write(obj.completedDate)
      ..writeByte(3)
      ..write(obj.relevantDate)
      ..writeByte(14)
      ..write(obj.relevantTime)
      ..writeByte(4)
      ..write(obj.title)
      ..writeByte(5)
      ..write(obj.description)
      ..writeByte(6)
      ..write(obj.isImportant)
      ..writeByte(7)
      ..write(obj.isCompleted)
      ..writeByte(8)
      ..write(obj.lastModified)
      ..writeByte(9)
      ..write(obj.hadInitialSync)
      ..writeByte(13)
      ..write(obj.listId)
      ..writeByte(15)
      ..write(obj.rRuleString)
      ..writeByte(17)
      ..write(obj.hasRecurred)
      ..writeByte(16)
      ..write(obj.image)
      ..writeByte(18)
      ..write(obj.progress)
      ..writeByte(19)
      ..write(obj.notificationMetadata?.toList());
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is TodoAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

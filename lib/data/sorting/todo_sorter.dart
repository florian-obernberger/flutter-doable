import 'package:flutter/material.dart';

import '/state/settings.dart';
import '/data/todo.dart';
import '/util/extensions/todo.dart';
import '/util/extensions/iterable.dart';
import '/util/extensions/map.dart';

enum SortingGroup {
  starredOverdue,
  starredToday,
  starredDate,
  starredNoDate,
  overdue,
  today,
  date,
  noDate;

  /// Return true if [todo] is part of [this] Group.
  bool test(Todo todo) => switch (this) {
        starredOverdue => todo.isImportant && todo.isOverdue,
        starredToday => todo.isImportant && todo.isToday,
        starredDate => todo.isImportant && todo.relevantDate != null,
        starredNoDate => todo.isImportant,
        overdue => todo.isOverdue,
        today => todo.isToday,
        date => todo.relevantDate != null,
        noDate => true,
      };
}

enum SortOrder {
  ascending(1),
  descending(-1);

  const SortOrder(this.modifier);

  final int modifier;
}

enum SortingAttr {
  relevantDate,
  lastEdited,
  titleName,
  creationDate;

  int compare(Todo t1, Todo t2) => switch (this) {
        lastEdited => t1.lastModified.compareTo(t2.lastModified),
        creationDate => t1.creationDate.compareTo(t2.creationDate),
        relevantDate when t1.relevantDate != null && t2.relevantDate != null =>
          t1.relevantDateTime!.compareTo(t2.relevantDateTime!),
        titleName => t1.title.toLowerCase().compareTo(t2.title.toLowerCase()),
        relevantDate => 0,
      };
}

extension type const SortingMetric._((SortingAttr, SortOrder) _) {
  static List<SortingMetric> get values => SortingAttr.values
      .map((attr) => SortingMetric(attr, SortOrder.ascending))
      .toList();

  const SortingMetric.from((SortingAttr, SortOrder) from) : _ = from;

  const SortingMetric(SortingAttr metric, SortOrder order)
      : _ = (metric, order);

  SortingAttr get attribute => _.$1;
  SortOrder get order => _.$2;

  SortingMetric withOrder(SortOrder order) => SortingMetric(attribute, order);

  SortingMetric get reversedOrder => SortingMetric(
      attribute,
      switch (order) {
        SortOrder.ascending => SortOrder.descending,
        SortOrder.descending => SortOrder.ascending,
      });

  int compare(Todo t1, Todo t2) => attribute.compare(t1, t2) * order.modifier;

  int compareTo(SortingMetric other) {
    final attr = Enum.compareByIndex(attribute, other.attribute);
    if (attr != 0) return attr;
    return Enum.compareByIndex(order, other.order);
  }
}

final class TodoSorter {
  final List<SortingGroup> groupBy;
  final List<SortingMetric> sortBy;
  final bool applyToCompleted;

  const TodoSorter({
    required this.groupBy,
    required this.sortBy,
    required this.applyToCompleted,
  });

  factory TodoSorter.from(SortingPreferences sorting) => TodoSorter(
        groupBy: sorting.groupBy.value,
        sortBy: sorting.sortBy.value,
        applyToCompleted: sorting.sortCompleted.value,
      );

  factory TodoSorter.of(BuildContext context) =>
      TodoSorter.from(Settings.of(context).sorting);

  List<Todo> sortCompleted(List<Todo> todos) =>
      applyToCompleted ? sort(todos) : sortCompletedTodos(todos);

  List<Todo> sort(List<Todo> todos) =>
      sortTodos(todos, groupBy: groupBy, sortBy: sortBy);

  static List<Todo> sortCompletedTodos(List<Todo> todos) =>
      todos..sort((a, b) => b.completedDate!.compareTo(a.completedDate!));

  static List<Todo> sortTodos(
    List<Todo> todos, {
    required List<SortingGroup> groupBy,
    required List<SortingMetric> sortBy,
  }) {
    final groupsByImportance = groupBy.toList()..sort(Enum.compareByIndex);
    final grouped = todos
        .groupBy((todo) => groupsByImportance.firstWhere(
              (gr) => gr.test(todo),
              // noDate is the default, even if the list is empty
              orElse: () => SortingGroup.noDate,
            ))
        .mapValues((group) => group
          ..sort((t1, t2) {
            for (final metric in sortBy) {
              final comparison = metric.compare(t1, t2);
              if (comparison != 0) return comparison;
            }

            return 0;
          }));

    return [
      // Remove all used up groups to ensure that noDate is not used twice
      for (final group in groupBy) ...?grouped.remove(group),
      // Add noDate if it's still in the list to ensure that all todos get displayed
      ...?grouped[SortingGroup.noDate]
    ];
  }
}

import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:rrule/rrule.dart';
import 'package:uuid/v4.dart';

import '/data/base_notification.dart';
import '/classes/syncable.dart';

import 'notifications/todo_notification.dart';

part 'todo.g.dart';

const _uuid = UuidV4();

@HiveType(typeId: 0)
class Todo with EquatableMixin implements Syncable<Todo> {
  Todo({
    required this.creationDate,
    this.relevantDate,
    this.relevantTime,
    required this.title,
    this.description,
    bool? isImportant,
    bool? isCompleted,
    this.completedDate,
    required this.lastModified,
    String? id,
    bool? hadInitialSync,
    this.listId,
    this.rRuleString,
    bool? hasRecurred,
    this.image,
    this.progress,
    this.notificationMetadata,
  })  : isImportant = isImportant ?? false,
        isCompleted = isCompleted ?? false,
        hadInitialSync = hadInitialSync ?? false,
        hasRecurred = hasRecurred ?? false,
        id = id ?? _uuid.generate();

  /// The unique id of the Todo.
  @override
  @HiveField(0)
  final String id;

  /// When the Todo was created.
  @HiveField(1)
  DateTime creationDate;

  /// When the Todo was completed.
  @HiveField(2)
  DateTime? completedDate;

  /// When the Todo becomes / is relevant / due.
  @HiveField(3)
  DateTime? relevantDate;

  /// Time of the [relevantDate].
  @HiveField(14)
  TimeOfDay? relevantTime;

  /// DateTime consisting of both [relevantDate] and [relevantTime].
  DateTime? get relevantDateTime => relevantDate?.copyWith(
        hour: relevantTime?.hour ?? 23,
        minute: relevantTime?.minute ?? 59,
        second: 0,
        millisecond: 0,
        microsecond: 0,
      );

  set relevantDateTime(final DateTime? dateTime) {
    if (dateTime == null) {
      relevantDate = null;
      relevantTime = null;
      return;
    }

    relevantDate = dateTime.copyWith(
      hour: 23,
      minute: 59,
      second: 0,
      millisecond: 0,
      microsecond: 0,
    );

    relevantTime = TimeOfDay.fromDateTime(dateTime);
  }

  /// The title of the Todo.
  @HiveField(4)
  String title;

  /// The Description of the Todo; supports Markdown!
  @HiveField(5)
  String? description;

  /// Weather the Todo is (especially) important or not. Default is [false].
  @HiveField(6, defaultValue: false)
  bool isImportant;

  /// Weather the Todo is completed or not. (Default is [false]).
  @HiveField(7, defaultValue: false)
  bool isCompleted;

  /// The time the Todo was last modified.
  @override
  @HiveField(8)
  DateTime lastModified;

  /// If the item had already been synced with Nextcloud at least once.
  @override
  @HiveField(9)
  bool hadInitialSync;

  /// The id of a [TodoList]. If this is non null then the [Todo] is part of a
  /// list.
  ///
  /// _Part of the 'Lists' extension._
  @HiveField(13)
  String? listId;

  @HiveField(15)
  String? rRuleString;

  /// Weather the Todo has already recurred or not.
  @HiveField(17)
  bool hasRecurred;

  RecurrenceRule? get rRule =>
      rRuleString == null ? null : RecurrenceRule.fromString(rRuleString!);

  /// An Image that is stored along side the [Todo].
  @HiveField(16)
  String? image;

  /// The progress (in %) that this Todo is completed
  ///
  /// _Part of the 'Progress bar' extension._
  @HiveField(18)
  double? progress;

  @HiveField(19)
  Set<TodoNotifMeta>? notificationMetadata;

  List<TodoNotification>? notifications(WidgetRef ref) => notificationMetadata
      ?.map((meta) => TodoNotification.from(meta, ref, this))
      .toList();

  void addNotification(TodoNotifMeta notification) {
    notificationMetadata ??= <TodoNotifMeta>{};
    notificationMetadata!.add(notification);
  }

  void removeNotification(NotifId id) =>
      notificationMetadata?.removeWhere((notif) => notif.id == id);

  /// Shorthand for completing a Todo.
  void complete() {
    isCompleted = true;
    completedDate = DateTime.now();
    lastModified = DateTime.now();
  }

  /// Shorthand for uncompleting a Todo.
  void uncomplete() {
    isCompleted = false;
    completedDate = null;
    lastModified = DateTime.now();
  }

  @override
  Todo copyWith({
    DateTime? creationDate,
    DateTime? relevantDate,
    TimeOfDay? relevantTime,
    String? title,
    String? description,
    bool? isImportant,
    bool? isCompleted,
    DateTime? completedDate,
    DateTime? lastModified,
    String? id,
    bool? hadInitialSync,
    String? parent,
    Set<String>? children,
    String? listId,
    String? rRuleString,
    bool? hasRecurred,
    String? image,
    double? progress,
    Set<TodoNotifMeta>? notificationMetadata,
  }) =>
      Todo(
        creationDate: creationDate ?? this.creationDate,
        relevantDate: relevantDate ?? this.relevantDate,
        relevantTime: relevantTime ?? this.relevantTime,
        title: title ?? this.title,
        description: description ?? this.description,
        isImportant: isImportant ?? this.isImportant,
        isCompleted: isCompleted ?? this.isCompleted,
        completedDate: completedDate ?? this.completedDate,
        lastModified: lastModified ?? this.lastModified,
        id: id ?? this.id,
        hadInitialSync: hadInitialSync ?? this.hadInitialSync,
        listId: listId ?? this.listId,
        rRuleString: rRuleString ?? this.rRuleString,
        hasRecurred: hasRecurred ?? this.hasRecurred,
        image: image ?? this.image,
        progress: progress ?? this.progress,
        notificationMetadata: notificationMetadata ?? this.notificationMetadata,
      );

  /// Creates a clone of this [Todo].
  Todo clone({bool newId = false}) => Todo(
        creationDate: creationDate,
        relevantDate: relevantDate,
        relevantTime: relevantTime,
        title: title,
        description: description,
        isImportant: isImportant,
        isCompleted: isCompleted,
        completedDate: completedDate,
        lastModified: lastModified,
        id: newId ? _uuid.generate() : id,
        hadInitialSync: hadInitialSync,
        listId: listId,
        rRuleString: rRuleString,
        hasRecurred: hasRecurred,
        image: image,
        progress: progress,
        notificationMetadata: notificationMetadata?.toSet(),
      );

  @override
  List<Object?> get props => [
        creationDate,
        relevantDate,
        relevantTime,
        title,
        description,
        isImportant,
        isCompleted,
        completedDate,
        id,
        listId,
        rRuleString,
        hasRecurred,
        image,
        progress,
        notificationMetadata,
      ];
}

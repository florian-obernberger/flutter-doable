import 'dart:io';

import 'package:path/path.dart' as p;
import 'package:path_provider/path_provider.dart';

late Directory applicationsDirectory;
late ImageDirectory todoImagesDirectory;

extension type ImageDirectory(Directory directory) {
  static String imagePath(String name) => '$name.img';

  File image(String name) => directory.join(imagePath(name));
}

Future<void> initApplicationsDirectory() async {
  applicationsDirectory = await getApplicationDocumentsDirectory();
  todoImagesDirectory = ImageDirectory(
    applicationsDirectory.joinDirectory('todo_images'),
  );
}

extension BetterDirectory on Directory {
  File join(
    String path, [
    String? part2,
    String? part3,
    String? part4,
    String? part5,
    String? part6,
    String? part7,
    String? part8,
    String? part9,
    String? part10,
    String? part11,
    String? part12,
    String? part13,
    String? part14,
    String? part15,
  ]) =>
      File(p.join(this.path, path, part2, part3, part4, part5, part6, part7,
          part8, part9, part10, part11, part12, part13, part14, part15));
  Directory joinDirectory(
    String path, [
    String? part2,
    String? part3,
    String? part4,
    String? part5,
    String? part6,
    String? part7,
    String? part8,
    String? part9,
    String? part10,
    String? part11,
    String? part12,
    String? part13,
    String? part14,
    String? part15,
  ]) =>
      Directory(p.join(this.path, path, part2, part3, part4, part5, part6,
          part7, part8, part9, part10, part11, part12, part13, part14, part15));

  Future<FileSystemEntity?> deleteIfExists() =>
      exists().then((exists) => exists ? delete() : null);
}

extension BetterFile on File {
  String basename() => p.basename(path);
  String extension() => p.extension(path);
  Future<FileSystemEntity?> deleteIfExists() =>
      exists().then((exists) => exists ? delete() : null);
}

// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'hive_icon.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class HiveIconAdapter extends TypeAdapter<HiveIcon> {
  @override
  final int typeId = 2;

  @override
  HiveIcon read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return HiveIcon(
      fields[0] as int,
      fontFamily: fields[1] as String?,
      fontPackage: fields[2] as String?,
      matchTextDirection: fields[3] as bool,
    );
  }

  @override
  void write(BinaryWriter writer, HiveIcon obj) {
    writer
      ..writeByte(4)
      ..writeByte(0)
      ..write(obj.codePoint)
      ..writeByte(1)
      ..write(obj.fontFamily)
      ..writeByte(2)
      ..write(obj.fontPackage)
      ..writeByte(3)
      ..write(obj.matchTextDirection);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is HiveIconAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

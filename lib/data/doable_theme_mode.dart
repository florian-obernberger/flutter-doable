import 'package:flutter/material.dart';

export 'package:flutter/material.dart' show ThemeMode;

enum DarkMode { dark, darker, black }

extension type const DoableThemeMode._((ThemeMode, DarkMode) _) {
  const DoableThemeMode(final ThemeMode themeMode, final DarkMode darkMode)
      : _ = (themeMode, darkMode);

  ThemeMode get themeMode => _.$1;
  DarkMode get darkMode => _.$2;

  DoableThemeMode withThemeMode(ThemeMode themeMode) =>
      DoableThemeMode(themeMode, darkMode);

  DoableThemeMode withDarkMode(DarkMode darkMode) =>
      DoableThemeMode(themeMode, darkMode);

  bool get isDark => themeMode == ThemeMode.dark;
  bool get isLight => themeMode == ThemeMode.light;
  bool get isSystem => themeMode == ThemeMode.system;

  bool get isDarkMode => darkMode == DarkMode.dark;
  bool get isDarkerMode => darkMode == DarkMode.darker;
  bool get isBlackMode => darkMode == DarkMode.black;

  // Parsers

  static ThemeMode _parseThemeMode(String themeMode) => switch (themeMode) {
        'light' => ThemeMode.light,
        'dark' => ThemeMode.dark,
        'system' => ThemeMode.system,
        _ => throw Exception('$themeMode is not a valid ThemeMode.')
      };

  static DarkMode _parseDarkMode(String darkMode) => switch (darkMode) {
        'dark' => DarkMode.dark,
        'darker' => DarkMode.darker,
        'black' => DarkMode.black,
        _ => throw Exception('$darkMode is not a valid DarkMode.'),
      };

  factory DoableThemeMode.fromJson(Map<String, dynamic> json) {
    if (json
        case {
          'themeMode': String themeMode,
          'darkMode': String darkMode,
        }) {
      return DoableThemeMode(
        _parseThemeMode(themeMode),
        _parseDarkMode(darkMode),
      );
    }

    throw Exception('Invalid DoableThemeMode');
  }

  Map<String, String> toJson() => {
        'themeMode': themeMode.name,
        'darkMode': darkMode.name,
      };

  factory DoableThemeMode.fromSetting(String setting) {
    final fromTrueBlackMode = switch (setting) {
      'system' => const DoableThemeMode(ThemeMode.system, DarkMode.dark),
      'light' => const DoableThemeMode(ThemeMode.light, DarkMode.dark),
      'dark' => const DoableThemeMode(ThemeMode.dark, DarkMode.dark),
      'trueBlackSystem' =>
        const DoableThemeMode(ThemeMode.system, DarkMode.black),
      'trueBlackLight' =>
        const DoableThemeMode(ThemeMode.light, DarkMode.black),
      'trueBlack' => const DoableThemeMode(ThemeMode.dark, DarkMode.black),
      _ => null,
    };

    if (fromTrueBlackMode != null) return fromTrueBlackMode;

    final [themeMode, darkMode] = setting.split(':');
    return DoableThemeMode(
      _parseThemeMode(themeMode),
      _parseDarkMode(darkMode),
    );
  }

  String toSetting() => '${themeMode.name}:${darkMode.name}';

  factory DoableThemeMode.fromInt(int value) {
    final themeModeInt = value >> 8;
    final darkModeInt = value & 7;

    return DoableThemeMode(
      ThemeMode.values[themeModeInt.clamp(0, ThemeMode.values.length - 1)],
      DarkMode.values[darkModeInt.clamp(0, DarkMode.values.length - 1)],
    );
  }

  int toInt() => _.$1.index << 8 | _.$2.index;
}

extension ApplyDoableThemeMode on ColorScheme {
  ColorScheme applyDarkMode(DarkMode darkMode) => switch (darkMode) {
        DarkMode.dark => this,
        DarkMode.darker => _applyDarker(),
        DarkMode.black => _applyBlack(),
      };

  ColorScheme _applyDarker() => this;

  ColorScheme _applyBlack() => copyWith(
        surface: Colors.black,
        surfaceContainerLowest: Color.alphaBlend(
          surfaceContainerLowest.withOpacity(0.4),
          Colors.black,
        ),
        surfaceContainerLow: Color.alphaBlend(
          surfaceContainerLow.withOpacity(0.4),
          Colors.black,
        ),
        surfaceContainer: Color.alphaBlend(
          surfaceContainer.withOpacity(0.4),
          Colors.black,
        ),
        surfaceContainerHigh: Color.alphaBlend(
          surfaceContainerHigh.withOpacity(0.4),
          Colors.black,
        ),
        surfaceContainerHighest: Color.alphaBlend(
          surfaceContainerHighest.withOpacity(0.4),
          Colors.black,
        ),
      );
}

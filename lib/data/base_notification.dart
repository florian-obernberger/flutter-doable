import 'dart:async';

import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:meta/meta.dart';
import 'package:timezone/timezone.dart';

import '/classes/id.dart';
import '/remote/handle_io_exception.dart';

part 'base_notification.g.dart';

extension type const NotifId._(NumId<int> _id)
    implements NumId<int>, BaseId<int> {
  static const int maxValue = 0x7FFFFFFF;
  static const int minValue = -0x80000000;

  /// Creates a new [Id] that can be used for a Notification.
  /// [value] must be a 32-bit integer, meaning the value bust be
  /// in the range of `-0x80000000 .. 0x7FFFFFFF`
  NotifId(int value)
      : assert(
          minValue <= value && value <= maxValue,
          'Value not valid (-0x80000000 <= $value <= 0x7FFFFFFF)',
        ),
        _id = NumId(value);

  /// Creates a new [NotifId] that clamps the values to the required
  /// limits. See [NotificationId.new] for more.
  NotifId.guard(int value) : _id = NumId(value.clamp(-0x80000000, 0x7FFFFFFF));

  NotifId.generate() : _id = NumId.generate();
}

class NotifMeta {
  final NotifId id;
  final TZDateTime scheduledDate;

  const NotifMeta(this.id, this.scheduledDate);

  bool shouldNotify() => scheduledDate.isBefore(DateTime.now());
}

abstract class BaseNotification {
  final NotifMeta metadata;

  const BaseNotification(this.metadata);

  static Future<void> cancelId(int notifId) =>
      FlutterLocalNotificationsPlugin().cancel(notifId);

  /// Create and return the details of the notification.
  FutureOr<AndroidNotificationDetails> getDetails();

  /// Generate the title and description of the notification.
  FutureOr<({String title, String? description})> getInfo({
    required bool isAndroid,
  });

  FutureOr<String?> getPayload();

  bool shouldNotify() => metadata.shouldNotify();

  Future<void> cancel() =>
      FlutterLocalNotificationsPlugin().cancel(metadata.id.value);

  @mustCallSuper
  Future<void> schedule({bool force = false}) async {
    if (!force && !shouldNotify()) return;

    final plugin = FlutterLocalNotificationsPlugin();
    final details = await getDetails();
    final (title: title, description: description) = await getInfo(
      isAndroid: Platform.isAndroid,
    );
    final payload = await getPayload();

    if (!Platform.isAndroid) {
      return plugin.zonedSchedule(
        metadata.id.value,
        title,
        description,
        metadata.scheduledDate,
        NotificationDetails(android: details),
        androidScheduleMode: AndroidScheduleMode.exactAllowWhileIdle,
        uiLocalNotificationDateInterpretation:
            UILocalNotificationDateInterpretation.absoluteTime,
        payload: payload,
      );
    }

    return plugin
        .resolvePlatformSpecificImplementation<
            AndroidFlutterLocalNotificationsPlugin>()!
        .zonedSchedule(
          metadata.id.value,
          title,
          description,
          metadata.scheduledDate,
          details,
          scheduleMode: AndroidScheduleMode.exactAllowWhileIdle,
          payload: payload,
        );
  }
}

extension NotificationListScheduler on Iterable<BaseNotification> {
  Future<void> scheduleAll({bool force = false}) =>
      Future.wait(map((elem) => elem.schedule(force: force)));

  Future<void> cancelAll() => Future.wait(map((elem) => elem.cancel()));
}

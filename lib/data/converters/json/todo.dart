import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:result/result.dart';
import 'package:timezone/timezone.dart';

import '/data/todo.dart';
import '/data/base_notification.dart';
import '/data/notifications/todo_notification.dart';

import 'types.dart';

typedef ParsedTodo = Result<Todo, MalformedData<Object>>;

TimeOfDay? _parseTimeOfDay(Object? time) {
  if (time is! String) return null;

  final [hour, minute, ..._] = time.split(':').map(int.tryParse).toList();
  if (hour == null || minute == null) return null;
  return TimeOfDay(hour: hour, minute: minute);
}

String _storeTimeOfDay(TimeOfDay time) =>
    '${time.hour.toString().padLeft(2, '0')}:'
    '${time.minute.toString().padLeft(2, '0')}:'
    '00';

Json todoJsonDecode(String source) => jsonDecode(source,
    reviver: (key, value) => switch (key) {
          'creationDate' ||
          'lastModified' ||
          'completedDate' ||
          'relevantDate' ||
          'scheduledDate' =>
            switch (value) {
              String() => DateTime.tryParse(value),
              _ => value,
            },
          'relevantTime' => switch (value) {
              String() => _parseTimeOfDay(value),
              _ => value,
            },
          _ => value,
        });

String todoJsonEncode(Json source) => JsonEncoder.withIndent(
    '  ',
    (value) => switch (value) {
          DateTime() => value.toIso8601String(),
          TimeOfDay() => _storeTimeOfDay(value),
          _ => value,
        }).convert(source);

ParsedTodo _parseTodo(Object src, ParsedJson res, bool withId) =>
    res.andThen((todo) {
      if (todo
          case {
            'id': String id,
            'creationDate': DateTime creationDate,
            'lastModified': DateTime lastModified,
            'title': String title,
            'isImportant': bool isImportant,
            'isCompleted': bool isCompleted,
            'hadInitialSync': bool hadInitialSync,
          }) {
        return Ok(Todo(
          id: withId ? id : null,
          creationDate: creationDate,
          title: title,
          isImportant: isImportant,
          isCompleted: isCompleted,
          hadInitialSync: hadInitialSync,
          hasRecurred: todo['hasRecurred'] as bool?,
          description: todo['description'] as String?,
          listId: todo['listId'] as String?,
          lastModified: lastModified,
          completedDate: todo['completedDate'] as DateTime?,
          relevantDate: todo['relevantDate'] as DateTime?,
          relevantTime: todo['relevantTime'],
          rRuleString: todo['rRule'] as String?,
          progress: todo['progress'] as double?,
          notificationMetadata: (todo['notifications'] as List?)
              ?.cast<Map>()
              .map((data) => TodoNotifMeta(
                    NotifId(data['id']),
                    TZDateTime.from(data['scheduledDate'] as DateTime, local),
                    isRelative: data['isRelative'] as bool,
                    duration: data.containsKey('duration')
                        ? TodoDuration.fromDuration(
                            Duration(microseconds: data['duration'] as int),
                          )
                        : null,
                  ))
              .toSet(),
        ));
      } else {
        return Err(IncompleteData(
          src,
          helpMsg: 'Todo did not contain required fields',
          requiredFields: const <String>[
            'id',
            'creationDate',
            'title',
            'isImportant',
            'isCompleted',
            'hadInitialSync',
            'lastModified',
          ],
        ));
      }
    });

/// Takes in an encoded json-table and returns a [ParsedTodo].
///
/// A todo **must** have the following fields:
///
/// - `id`
/// - `creationDate`
/// - `title`
/// - `isImportant`
/// - `isCompleted`
/// - `hadInitialSync`
/// - `lastModified`
///
/// If any or all of those are missing an `Err(IncompleteData())` will be
/// returned. If the [source] string or any of the fields can't be parsed an
/// `Err(CorruptData())` will be returned instead.
///
/// Otherwise returns `Ok(Todo())`.
ParsedTodo parseTodo(String source, {bool withId = true}) => _parseTodo(
      source,
      Guard.guard<Json>(() => todoJsonDecode(source)).mapErr(
        (err) => CorruptData(source, err),
      ),
      withId,
    );

/// Takes in a json encoded string and parses out all the contained todos.
///
/// If the json data does not have an array of tables called `todos` [None] will
/// be returned. Otherwise an iterable containing [ParsedTodo]s will be
/// returned. For more information on how the individual todos are parsed take a
/// look at [parseTodo].
Option<Iterable<ParsedTodo>> parseTodos(String source, {bool withId = true}) {
  final json = Guard.guard<Json>(() => todoJsonDecode(source)).mapErr(
    (err) => CorruptData(source, err),
  );

  if (json.isOkAnd((t) => t.containsKey('todos'))) {
    final todos = (json.unwrap()['todos'] as List<dynamic>).cast<Json>();
    return Some(todos.map((todo) => _parseTodo(todo, Ok(todo), withId)));
  } else {
    return const None();
  }
}

extension TodoToTomlMap on Todo {
  /// Converts a [Todo] to a json compliant map.
  ///
  /// This is used internally in [storeTodo] and [storeTodos].
  Json toMap() {
    final stored = {
      'id': id,
      'creationDate': creationDate,
      'title': title,
      'description': description,
      'isImportant': isImportant,
      'isCompleted': isCompleted,
      'lastModified': lastModified,
      'relevantDate': relevantDate,
      'relevantTime': relevantTime,
      'hadInitialSync': hadInitialSync,
      'listId': listId,
      'hasRecurred': hasRecurred,
      'completedDate': completedDate,
      'rRule': rRuleString,
      'progress': progress,
      'notifications': notificationMetadata
          ?.map((notif) => {
                'id': notif.id.value,
                'scheduledDate': notif.scheduledDate,
                'isRelative': notif.isRelative,
                if (notif.duration != null)
                  'duration': notif.duration!.inMicroseconds,
              })
          .toList(),
    };

    stored.removeWhere((_, value) => value == null);
    return stored;
  }
}

String storeTodo(Todo todo) => todoJsonEncode(todo.toMap());

String storeTodos(Iterable<Todo> todos) =>
    todoJsonEncode({'todos': todos.map((t) => t.toMap()).toList()});

import 'package:result/result.dart';
import '/types/malformed_data.dart';

export '/types/malformed_data.dart';

typedef Json = Map<String, dynamic>;
typedef ParsedJson = Result<Json, MalformedData<Object>>;

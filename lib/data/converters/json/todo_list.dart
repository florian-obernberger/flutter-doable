import 'dart:convert';

import 'package:result/result.dart';

import '/data/todo_list.dart';
import '/data/hive_icon.dart';

import 'types.dart';

typedef ParsedTodoList = Result<TodoList, MalformedData<Object>>;

Json _jsonDecode(String source) => jsonDecode(source,
    reviver: (key, value) => switch (key) {
          'creationDate' || 'lastModified' => switch (value) {
              String() => DateTime.tryParse(value),
              _ => value,
            },
          _ => value,
        });

String _jsonEncode(Json source) => JsonEncoder.withIndent(
      '  ',
      (value) => switch (value) {
        DateTime() => value.toIso8601String(),
        HiveIcon() => value.toMap(),
        _ => value
      },
    ).convert(source);

ParsedTodoList _parseTodoList(Object src, ParsedJson res, bool withId) => res
        .mapErr<MalformedData<Object>>((err) => CorruptData(src, err))
        .andThen((list) {
      if (list.containsKey('icon') && !list.containsKey('hiveIcon')) {
        list['hiveIcon'] = list['icon'];
      }

      if (list.containsKey('selectedIcon') &&
          !list.containsKey('selectedHiveIcon')) {
        list['selectedHiveIcon'] = list['selectedIcon'];
      }

      if (list
          case {
            'id': String _,
            'name': String _,
            'hiveIcon': Map _,
            'selectedHiveIcon': Map _,
            'lastModified': DateTime _,
          }) {
        return Ok(TodoList.fromMap(list));
      } else {
        return Err(IncompleteData(
          src,
          helpMsg: 'TodoList did not contain required fields',
          requiredFields: const <String>[
            'id',
            'name',
            'hiveIcon',
            'selectedHiveIcon',
            'lastModified',
          ],
        ));
      }
    });

/// Takes in an encoded json-table and returns a [ParsedTodoList].
///
/// A todo list **must** have the following fields:
///
/// - `name`
/// - `hiveIcon`
/// - `selectedHiveIcon`
/// - `creationDate`
/// - `lastModified`
/// - `hadInitialSync`
///
/// If any or all of those are missing an `Err(IncompleteData())` will be
/// returned. If the [source] string or any of the fields can't be parsed an
/// `Err(CorruptData())` will be returned instead.
///
/// Otherwise returns `Ok(TodoList())`.
ParsedTodoList parseTodoList(String source, {bool withId = true}) =>
    _parseTodoList(
      source,
      Guard.guard(() => _jsonDecode(source)).mapErr(
        (err) => CorruptData(source, err),
      ),
      withId,
    );

/// Takes in a json encoded string and parses out all the contained todo lists.
///
/// If the json data does not have an array of tables called `todos` [None] will
/// be returned. Otherwise an iterable containing [ParsedTodoList]s will be
/// returned. For more information on how the individual todos are parsed take a
/// look at [parseTodoList].
Option<Iterable<ParsedTodoList>> parseTodoLists(
  String source, {
  bool withId = true,
}) {
  final json = Guard.guard(() => _jsonDecode(source)).mapErr(
    (err) => CorruptData(source, err),
  );

  if (json.isOkAnd((l) => l.containsKey('lists'))) {
    final lists = (json.unwrap()['lists'] as List<dynamic>).cast<Json>();
    return Some(lists.map((list) => _parseTodoList(list, Ok(list), withId)));
  } else {
    return const None();
  }
}

String storeTodoList(TodoList list) => _jsonEncode(list.toMap());

String storeTodoLists(Iterable<TodoList> lists) =>
    _jsonEncode({'lists': lists.map((l) => l.toMap()).toList()});

import 'package:result/result.dart';
import 'package:toml/toml.dart';

import '/data/todo_list.dart' show TodoList;
import '/data/hive_icon.dart';

import 'types.dart';
import 'helpers.dart';

typedef ParsedTodoList = Result<TodoList, MalformedData<Object>>;

/// A function that simply returns the value.
T _p<T>(T v) => v;

ParsedTodoList _parseTodoList(Object src, ParsedToml res, bool withId) => res
        .mapErr<MalformedData<Object>>((err) => CorruptData(src, err))
        .andThen((list) {
      if (list.containsKey('icon') && !list.containsKey('hiveIcon')) {
        list['hiveIcon'] = list['icon'];
      }

      if (list.containsKey('selectedIcon') &&
          !list.containsKey('selectedHiveIcon')) {
        list['selectedHiveIcon'] = list['selectedIcon'];
      }

      if (list
          case {
            'id': String id,
            'name': String name,
            'hiveIcon': Toml hiveIcon,
            'selectedHiveIcon': Toml selectedHiveIcon,
            'lastModified': Object modified,
          }) {
        final time = parseDateTime(modified);
        if (time.isNone()) {
          return Err(CorruptData(time, const FormatException()));
        }

        final icon = HiveIcon.fromMap(hiveIcon);
        final selectedIcon = HiveIcon.fromMap(selectedHiveIcon);

        return Ok(TodoList(
          id: withId ? id : null,
          name: name,
          hiveIcon: icon,
          selectedHiveIcon: selectedIcon,
          creationDate: parseDateTime(list['creationDate']).mapOr(
            _p,
            null,
          ),
          lastModified: time.unwrap(),
          hadInitialSync: list['hadInitialSync'] as bool?,
        ));
      } else {
        return Err(IncompleteData(
          src,
          helpMsg: 'TodoList did not contain required fields',
          requiredFields: const <String>[
            'id',
            'name',
            'hiveIcon',
            'selectedHiveIcon',
            'lastModified',
          ],
        ));
      }
    });

/// Takes in an encoded toml-table and returns a [ParsedTodoList].
///
/// A todo list **must** have the following fields:
///
/// - `name`
/// - `hiveIcon`
/// - `selectedHiveIcon`
/// - `creationDate`
/// - `lastModified`
/// - `hadInitialSync`
///
/// If any or all of those are missing an `Err(IncompleteData())` will be
/// returned. If the [source] string or any of the fields can't be parsed an
/// `Err(CorruptData())` will be returned instead.
///
/// Otherwise returns `Ok(TodoList())`.
ParsedTodoList parseTodoList(String source, {bool withId = true}) =>
    _parseTodoList(
      source,
      Guard.guard(() => TomlDocument.parse(source).toMap()).mapErr(
        (err) => CorruptData(source, err),
      ),
      withId,
    );

/// Takes in a toml encoded string and parses out all the contained todo lists.
///
/// If the toml data does not have an array of tables called `todos` [None] will
/// be returned. Otherwise an iterable containing [ParsedTodoList]s will be
/// returned. For more information on how the individual todos are parsed take a
/// look at [parseTodoList].
Option<Iterable<ParsedTodoList>> parseTodoLists(String source,
    {bool withId = true}) {
  final toml = Guard.guard(() => TomlDocument.parse(source).toMap()).mapErr(
    (err) => CorruptData(source, err),
  );

  if (toml.isOkAnd((l) => l.containsKey('lists'))) {
    final lists = (toml.unwrap()['lists'] as List<dynamic>).cast<Toml>();
    return Some(lists.map((list) => _parseTodoList(list, Ok(list), withId)));
  } else {
    return const None();
  }
}

extension TodoListToToml on TodoList {
  @Deprecated('Please use json instead')
  Toml toTomlMap() => toMap();
}

@Deprecated('Please use json/storeTodo instead')
String storeTodoList(TodoList list) =>
    TomlDocument.fromMap(list.toMap()).toString();

@Deprecated('Please use json/storeTodo instead')
String storeTodoLists(Iterable<TodoList> lists) =>
    TomlDocument.fromMap({'lists': lists.map((l) => l.toMap())}).toString();

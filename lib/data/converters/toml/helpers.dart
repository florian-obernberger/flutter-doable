import 'package:flutter/material.dart';
import 'package:result/result.dart';
import 'package:toml/toml.dart';

Option<DateTime> parseDateTime<T>(Object? date) => switch (date) {
      TomlOffsetDateTime() => Some(date.toUtcDateTime()),
      String() => Some(DateTime.parse(date)),
      _ => const None(),
    };

Option<TimeOfDay> parseTimeOfDay(Object? time) {
  if (time is! String) return const None();

  final [hour, minute, ..._] = time.split(':').map(int.tryParse).toList();
  if (hour == null || minute == null) return const None();
  return Some(TimeOfDay(hour: hour, minute: minute));
}

String storeTimeOfDay(TimeOfDay time) =>
    '${time.hour.toString().padLeft(2, '0')}:'
    '${time.minute.toString().padLeft(2, '0')}:'
    '00';

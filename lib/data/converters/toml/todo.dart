import 'package:flutter/material.dart';
import 'package:result/result.dart';
import 'package:toml/toml.dart';

import '/data/todo.dart';

import 'types.dart';
import 'helpers.dart';

typedef ParsedTodo = Result<Todo, MalformedData<Object>>;

/// A function that simply returns the value.
T _p<T>(T v) => v;

ParsedTodo _parseTodo(Object src, ParsedToml res, bool withId) => res
        .mapErr<MalformedData<Object>>((err) => CorruptData(src, err))
        .andThen((todo) {
      if (todo
          case {
            'id': String id,
            'creationDate': Object creation,
            'lastModified': Object modified,
            'title': String title,
            'isImportant': bool isImportant,
            'isCompleted': bool isCompleted,
            'hadInitialSync': bool hadInitialSync,
          }) {
        final times = parseDateTime(creation).zip(parseDateTime(modified));
        if (times.isNone()) {
          return Err(CorruptData(
            (creation, modified),
            const FormatException(),
          ));
        }

        final (creationDate, lastModified) = times.unwrap();

        return Ok(Todo(
          id: withId ? id : null,
          creationDate: creationDate,
          title: title,
          isImportant: isImportant,
          isCompleted: isCompleted,
          hadInitialSync: hadInitialSync,
          hasRecurred: todo['hasRecurred'] as bool?,
          description: todo['description'] as String?,
          listId: todo['listId'] as String?,
          lastModified: lastModified,
          completedDate: parseDateTime(todo['completedDate']).mapOr(_p, null),
          relevantDate: parseDateTime(todo['relevantDate']).mapOr(_p, null),
          relevantTime: parseTimeOfDay(todo['relevantTime']).mapOr(_p, null),
          rRuleString: todo['rRule'] as String?,
        ));
      } else {
        return Err(IncompleteData(
          src,
          helpMsg: 'Todo did not contain required fields',
          requiredFields: const <String>[
            'id',
            'creationDate',
            'title',
            'isImportant',
            'isCompleted',
            'hadInitialSync',
            'lastModified',
          ],
        ));
      }
    });

/// Takes in an encoded toml-table and returns a [ParsedTodo].
///
/// A todo **must** have the following fields:
///
/// - `id`
/// - `creationDate`
/// - `title`
/// - `isImportant`
/// - `isCompleted`
/// - `hadInitialSync`
/// - `lastModified`
///
/// If any or all of those are missing an `Err(IncompleteData())` will be
/// returned. If the [source] string or any of the fields can't be parsed an
/// `Err(CorruptData())` will be returned instead.
///
/// Otherwise returns `Ok(Todo())`.
ParsedTodo parseTodo(String source, {bool withId = true}) => _parseTodo(
      source,
      Guard.guard<Toml>(() => TomlDocument.parse(source).toMap()).mapErr(
        (err) => CorruptData(source, err),
      ),
      withId,
    );

/// Takes in a toml encoded string and parses out all the contained todos.
///
/// The toml must follow the following structure:
///
/// ```toml
/// [[todos]]
/// id = 'a5497520-02f2-11ee-8fd8-83cded5a3bfc'
/// creationDate = '2023-06-04T18:12:47.346'
/// title = 'example'
/// isImportant = false
/// isCompleted = false
/// lastModified = '2023-06-19T12:13:57.843'
/// relevantDate = '2023-06-27T00:00:00.000'
/// hadInitialSync = true
/// ```
///
/// If the toml data does not have an array of tables called `todos` [None] will
/// be returned. Otherwise an iterable containing [ParsedTodo]s will be
/// returned. For more information on how the individual todos are parsed take a
/// look at [parseTodo].
Option<Iterable<ParsedTodo>> parseTodos(String source, {bool withId = true}) {
  final toml =
      Guard.guard<Toml>(() => TomlDocument.parse(source).toMap()).mapErr(
    (err) => CorruptData(source, err),
  );

  if (toml.isOkAnd((t) => t.containsKey('todos'))) {
    final todos = (toml.unwrap()['todos'] as List).cast<Toml>();
    return Some(todos.map((todo) => _parseTodo(todo, Ok(todo), withId)));
  } else {
    return const None();
  }
}

extension _StoreTimeOfDayExtension on TimeOfDay {
  String toTomlString() => storeTimeOfDay(this);
}

extension TodoToTomlMap on Todo {
  /// Converts a [Todo] to a toml compliant map.
  ///
  /// This is used internally in [storeTodo] and [storeTodos].
  @Deprecated('Please use json instead')
  Toml toTomlMap() {
    final stored = {
      'id': id,
      'creationDate': creationDate.toIso8601String(),
      'title': title,
      'description': description,
      'isImportant': isImportant,
      'isCompleted': isCompleted,
      'lastModified': lastModified.toIso8601String(),
      'relevantDate': relevantDate?.toIso8601String(),
      'relevantTime': relevantTime?.toTomlString(),
      'hadInitialSync': hadInitialSync,
      'listId': listId,
      'hasRecurred': hasRecurred,
      'completedDate': completedDate?.toIso8601String(),
      'rRule': rRuleString,
    };

    stored.removeWhere((_, value) => value == null);
    return stored;
  }
}

@Deprecated('Please use json/storeTodo instead')
String storeTodo(Todo todo) =>
    TomlDocument.fromMap(todo.toTomlMap()).toString();

@Deprecated('Please use json/storeTodos instead')
String storeTodos(Iterable<Todo> todos) =>
    TomlDocument.fromMap({'todos': todos.map((t) => t.toTomlMap())}).toString();

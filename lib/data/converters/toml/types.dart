import 'package:result/result.dart';
import '/types/malformed_data.dart';

export '/types/malformed_data.dart';

typedef Toml = Map<String, dynamic>;
typedef ParsedToml = Result<Toml, MalformedData<Object>>;

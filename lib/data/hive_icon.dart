import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';

import 'mappable.dart';

part 'hive_icon.g.dart';

@HiveType(typeId: 2)
class HiveIcon with EquatableMixin implements Mappable {
  const HiveIcon(
    this.codePoint, {
    this.fontFamily,
    this.fontPackage,
    this.matchTextDirection = false,
  });

  factory HiveIcon.fromIcon(IconData icon) => HiveIcon(
        icon.codePoint,
        fontFamily: icon.fontFamily,
        fontPackage: icon.fontPackage,
        matchTextDirection: icon.matchTextDirection,
      );

  IconData get icon => IconData(
        codePoint,
        fontFamily: fontFamily,
        fontPackage: fontPackage,
      );

  @HiveField(0)
  final int codePoint;

  @HiveField(1)
  final String? fontFamily;

  @HiveField(2)
  final String? fontPackage;

  @HiveField(3)
  final bool matchTextDirection;

  factory HiveIcon.fromMap(Map<String, dynamic> map) => HiveIcon(
        map['codePoint'] as int,
        fontFamily: map.get('fontFamily') as String?,
        fontPackage: map.get('fontPackage') as String?,
        matchTextDirection: map['matchTextDirection'],
      );

  @override
  Map<String, dynamic> toMap() => {
        'codePoint': codePoint,
        if (fontFamily != null) 'fontFamily': fontFamily,
        if (fontPackage != null) 'fontPackage': fontPackage,
        'matchTextDirection': matchTextDirection,
      };

  @override
  List<Object?> get props => [
        codePoint,
        fontFamily,
        fontPackage,
        matchTextDirection,
      ];
}

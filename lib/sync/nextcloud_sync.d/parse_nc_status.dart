import 'package:result/result.dart';

import '../base_sync.dart';

import 'api.dart';

Result<(), SyncError> parseNcStatus(NextcloudApiStatus res) => switch (res) {
      NextcloudApiStatus.ok ||
      NextcloudApiStatus.created ||
      NextcloudApiStatus.savedChanges =>
        const Ok(()),
      NextcloudApiStatus.unknownError => const Err(SyncError.unknownError),
      NextcloudApiStatus.noAuthorization ||
      NextcloudApiStatus.missingPermission =>
        const Err(SyncError.wrongUserOrPassword),
      NextcloudApiStatus.notFound => const Err(SyncError.doesNotExist),
      NextcloudApiStatus.maintenance => const Err(SyncError.maintenance),
      NextcloudApiStatus.unavailable => const Err(SyncError.unavailable),
      NextcloudApiStatus.fileLocked =>
        const Ok(()), // Let's hope this works because yes.
      _ => const Err(SyncError.unknownError),
    };

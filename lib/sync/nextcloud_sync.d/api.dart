import 'dart:convert';
import 'dart:typed_data';

import 'package:result/result.dart';
import 'package:path/path.dart' as path;
import 'package:http/http.dart' as http;

import '/types/fetch_exception.dart';
import '/classes/logger.dart';

const _loggerTag = 'NC API';

enum NextcloudApiStatus {
  /// Everything went oki-doki.
  ok(200),

  /// Created the file / folder.
  created(201),

  /// No content aka saved the changes successfully.
  savedChanges(204),

  /// There was an error with the authorization. Either user or password was
  /// incorrect.
  noAuthorization(401),

  /// The specified file / folder does not exist.
  notFound(404),

  /// The user does not have permission to write to that location.
  missingPermission(403),

  /// The folder already exists.
  folderAlreadyExists(405),

  /// The server is not available.
  unavailable(503),

  /// The server is in maintenance mode.
  maintenance(503),

  /// The file is locked and can't currently be written to.
  fileLocked(423),

  /// This server is not a nextcloud instance.
  notNextcloud(-1),

  /// Unknown error occurred.
  unknownError(-1);

  const NextcloudApiStatus(this.statusCode);
  final int statusCode;
}

class NextcloudApiResponse {
  const NextcloudApiResponse({
    required this.status,
    required this.bodyBytes,
    required this.body,
  });

  final Uint8List bodyBytes;
  final String body;
  final NextcloudApiStatus status;
}

typedef ResponseResult = FutureResult<NextcloudApiResponse, FetchException>;
typedef StatusResult = FutureResult<NextcloudApiStatus, FetchException>;

class NextcloudApi {
  NextcloudApi({
    required String baseUrl,
    required this.password,
    required this.user,
  })  : baseUrl = baseUrl.endsWith('/')
            ? baseUrl.substring(0, baseUrl.length - 1)
            : baseUrl,
        _webDavUri = '$baseUrl/remote.php/dav/files/$user',
        authHeaders = {
          'Authorization':
              "Basic ${base64.encode(utf8.encode('$user:$password'))}"
        };

  final String baseUrl;
  final String user;
  final String password;

  final Map<String, String> authHeaders;

  final String _webDavUri;

  Uri _webDavJoin(String filePath) => Uri.parse(
        path.join(_webDavUri, filePath),
      );

  static FutureResult<http.Response, FetchException> _get(
    Uri url, {
    required String helpMsg,
    Map<String, String>? headers,
  }) async {
    try {
      return Ok(await http.get(url, headers: headers));
    } on http.ClientException catch (e, s) {
      logger.e(message: helpMsg, e, stackTrace: s);

      return Err(FetchException(
        url,
        originalException: e,
        helpMessage: helpMsg,
      ));
    } on Exception catch (e, s) {
      logger.e(message: 'Unexpected exception occurred', e, stackTrace: s);

      return Err(FetchException(
        url,
        helpMessage: 'Unexpected exception occurred',
        originalException: e,
      ));
    }
  }

  static FutureResult<http.Response, FetchException> _put(
    Uri url, {
    required String helpMsg,
    Object? body,
    Encoding? encoding,
    Map<String, String>? headers,
  }) async {
    try {
      return Ok(await http.put(
        url,
        headers: headers,
        body: body,
        encoding: encoding,
      ));
    } on http.ClientException catch (e, s) {
      logger.e(message: helpMsg, e, stackTrace: s);

      return Err(FetchException(
        url,
        helpMessage: helpMsg,
        originalException: e,
      ));
    } on Exception catch (e, s) {
      logger.e(message: 'Unexpected exception occurred', e, stackTrace: s);

      return Err(FetchException(
        url,
        helpMessage: 'Unexpected exception occurred',
        originalException: e,
      ));
    }
  }

  static FutureResult<bool, FetchException> isNextcloudServer(
    String baseUrl,
  ) async {
    final res = await _get(
      Uri.parse(path.join(baseUrl, 'index.php/login/flow')),
      helpMsg: 'Could not determine if it is a Nextcloud Server',
      headers: {'OCS-APIRequest': 'true'},
    );

    return switch (res) {
      Ok() => Ok(res.value.statusCode == 200),
      Err(error: http.ClientException()) => const Ok(false),
      Err() => Err(res.error),
    };
  }

  StatusResult storeFile(String filePath, String content) => _put(
        _webDavJoin(filePath),
        helpMsg: 'Could not store file',
        headers: authHeaders,
        body: content,
        encoding: utf8,
      ).map(_getStatus);

  ResponseResult loadFile(String filePath) => _get(
        _webDavJoin(filePath),
        helpMsg: 'Could not load file',
        headers: authHeaders,
      ).map(
        (value) => NextcloudApiResponse(
          status: _getStatus(value),
          bodyBytes: value.bodyBytes,
          body: utf8.decode(value.bodyBytes),
        ),
      );

  ResponseResult createDirectory(String directory) async {
    final url = _webDavJoin(directory);
    final helpMsg = 'Tried creating directory $directory';

    try {
      final request = http.Request('MKCOL', url);
      request.headers.addAll(authHeaders);
      final streamedResponse = await http.Client().send(request);
      final response = await http.Response.fromStream(streamedResponse);

      return Ok(NextcloudApiResponse(
        // Map already exists to `Ok`.
        status: response.statusCode == 405
            ? NextcloudApiStatus.ok
            : _getStatus(response),
        bodyBytes: response.bodyBytes,
        body: utf8.decode(response.bodyBytes),
      ));
    } on http.ClientException catch (e, s) {
      logger.e(message: helpMsg, e, stackTrace: s);

      return Err(FetchException(
        url,
        originalException: e,
        helpMessage: helpMsg,
      ));
    } on Exception catch (e, s) {
      logger.e(message: 'Unexpected exception occurred', e, stackTrace: s);

      return Err(FetchException(
        url,
        helpMessage: 'Unexpected exception occurred',
        originalException: e,
      ));
    }
  }

  String getAvatarUrl([int size = 512]) =>
      path.join(baseUrl, 'index.php/avatar', user, '$size');

  FutureResult<Uint8List, FetchException> getAvatar([int size = 512]) => _get(
        Uri.parse(getAvatarUrl(size)),
        helpMsg: 'Could not get avatar',
        headers: authHeaders,
      ).map((value) => value.bodyBytes);

  FutureResult<(), FetchException> delete(String path) async {
    final url = _webDavJoin(path);
    final helpMsg = 'Tried deleting $path';

    try {
      await http.delete(url, headers: authHeaders);
      return const Ok(());
    } on http.ClientException catch (e, s) {
      logger.e(message: helpMsg, e, stackTrace: s);

      return Err(FetchException(
        url,
        originalException: e,
        helpMessage: helpMsg,
      ));
    } on Exception catch (e, s) {
      logger.e(message: 'Unexpected exception occurred', e, stackTrace: s);

      return Err(FetchException(
        url,
        helpMessage: 'Unexpected exception occurred',
        originalException: e,
      ));
    }
  }

  FutureResult<(), FetchException> logOut() async {
    final url = Uri.parse(path.join(baseUrl, 'ocs/v2.php/core/apppassword'));

    try {
      await http.delete(
        url,
        headers: {...authHeaders, 'OCS-APIRequest': 'true'},
      );

      return const Ok(());
    } on http.ClientException catch (e, s) {
      logger.e(message: 'Could not delete login data', e, stackTrace: s);

      return Err(FetchException(
        url,
        helpMessage: 'Could not delete login data',
        originalException: e,
      ));
    } on Exception catch (e, s) {
      logger.e(message: 'Unexpected exception occurred', e, stackTrace: s);

      return Err(FetchException(
        url,
        helpMessage: 'Unexpected exception occurred',
        originalException: e,
      ));
    }
  }

  NextcloudApiStatus _getStatus(http.Response res) {
    switch (res.statusCode) {
      case 200:
        return NextcloudApiStatus.ok;
      case 201:
        return NextcloudApiStatus.created;
      case 204:
        return NextcloudApiStatus.savedChanges;
      case 401:
        logger.e(
          message: 'No Authorization',
          tag: _loggerTag,
          res.body,
        );
        return NextcloudApiStatus.noAuthorization;
      case 404:
        logger.e(message: 'Not found', tag: _loggerTag, res.body);
        return NextcloudApiStatus.notFound;
      case 403:
        logger.e(
          message: 'Missing permission',
          tag: _loggerTag,
          res.body,
        );
        return NextcloudApiStatus.missingPermission;
      case 423:
        return NextcloudApiStatus.fileLocked;
      case 503:
        if (res.headers.containsKey('x-nextcloud-maintenance-mode') &&
            res.headers['x-nextcloud-maintenance-mode'] == '1') {
          logger.i(
            'Maintenance mode',
            tag: _loggerTag,
            error: res.body,
          );
          return NextcloudApiStatus.maintenance;
        }

        logger.e(
          message: 'Unavailable',
          tag: _loggerTag,
          res.body,
        );
        return NextcloudApiStatus.unavailable;
      default:
        logger.e(
          res.body,
          message: 'Unknown Error',
          tag: _loggerTag,
          more: {
            'statusCode': res.statusCode.toString(),
            ...res.headers,
          },
        );
        return NextcloudApiStatus.unknownError;
    }
  }
}

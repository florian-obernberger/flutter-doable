import 'dart:convert';
import 'dart:typed_data';

import 'package:result/result.dart';
import 'package:http/http.dart' as http;
import 'package:path/path.dart' as path;

import '/data/todo.dart';
import '/data/converters/toml/todo.dart';
import '/classes/logger.dart';

import 'base_sync.dart';

const _loggerTag = 'WebDAVSync';

@Deprecated('Transition to Remote')
class WebDAVSync extends BaseSync<Todo> {
  WebDAVSync({
    required this.baseUrl,
    required this.user,
    required this.password,
  }) : _authHeaders = {
          'Authorization':
              "Basic ${base64.encode(utf8.encode('$user:$password'))}"
        };

  final String baseUrl;
  final String user;
  final String password;

  final Map<String, String> _authHeaders;

  @override
  FutureResult<(), SyncError> logOut() async {
    return const Ok(());
  }

  @override
  FutureResult<Iterable<Todo>, SyncError> pull() async {
    if (!shouldMakeRequest) {
      return const Err(SyncError.tooManyConnectionTimeOuts);
    }

    late final Result<http.Response, SyncError> fetchRes;
    try {
      fetchRes = Ok(await http.get(
        Uri.parse(path.join(baseUrl, syncFileName)),
        headers: _authHeaders,
      ));
    } on http.ClientException catch (e, s) {
      logger.e(
        e,
        message: 'Tried fetching $baseUrl, but failed',
        tag: _loggerTag,
        stackTrace: s,
      );
      fetchRes = _handleClientException(
        'Tried fetching $baseUrl, but failed',
        e,
        s,
      );
    }

    return fetchRes.andThen((res) => mapStatusCode<List<Todo>>(
          res,
          () => deserializeTodos(res.bodyBytes),
        ));
  }

  @override
  FutureResult<(), SyncError> push(Iterable<Todo> values) async {
    if (!shouldMakeRequest) {
      return const Err(SyncError.tooManyConnectionTimeOuts);
    }

    final content = storeTodos(values);

    late final Result<http.Response, SyncError> storeRes;
    try {
      storeRes = Ok(await http.put(
        Uri.parse(path.join(baseUrl, syncFileName)),
        headers: _authHeaders,
        body: content,
        encoding: utf8,
      ));
    } on http.ClientException catch (e, s) {
      storeRes = _handleClientException(
        'Tried storing $baseUrl, but failed',
        e,
        s,
      );
    }

    return storeRes.andThen(
      (res) => mapStatusCode<()>(res, () => const Ok(())),
    );
  }

  Err<T, SyncError> _handleClientException<T>(
    String message,
    http.ClientException exception,
    StackTrace stackTrace,
  ) {
    logger.e(
      exception,
      message: message,
      stackTrace: stackTrace,
      tag: _loggerTag,
    );
    if (exception.message == 'Connection timed out') {
      return Err(onTimeOut(SyncError.connectionTimedOut));
    }

    return const Err(SyncError.noInternetConnection);
  }

  Result<T, SyncError> mapStatusCode<T>(
    http.Response res,
    Result<T, SyncError> Function() onOk,
  ) {
    if (res.statusCode == 200 ||
        res.statusCode == 204 ||
        res.statusCode == 201) {
      return onOk();
    }

    if (res.statusCode == 401) {
      logger.i(
        'User specified wrong user or password.',
        tag: _loggerTag,
        more: res.headers,
      );
      return const Err(SyncError.wrongUserOrPassword);
    }

    if (res.statusCode == 403) {
      logger.i(
        'User does not have permission for this action.',
        tag: _loggerTag,
        more: res.headers,
      );
      return const Err(SyncError.wrongUserOrPassword);
    }

    if (res.statusCode == 404) {
      logger.i(
        "Couldn't find resource $baseUrl",
        tag: _loggerTag,
        more: res.headers,
      );
      return const Err(SyncError.doesNotExist);
    }

    if (res.statusCode == 503) {
      return const Err(SyncError.unavailable);
    }

    logger.w(
      'Could not match status code ${res.statusCode}.',
      tag: _loggerTag,
      more: res.headers,
    );
    return const Err(SyncError.unknownError);
  }

  Result<List<Todo>, SyncError> deserializeTodos(Uint8List bytes) {
    return parseTodos(utf8.decode(bytes))
        .map((todos) => todos
            .where((todo) => todo.inspectErr((err) {
                  logger.e(err, message: "Couldn't decode Todo");
                }).isOk())
            .map((todo) => todo.unwrap()))
        .okOr(SyncError.corruptedFile)
        .inspectErr((err) {
      logger.e(err, message: "Couldn't parse $syncFileName");
    }).map((todos) => todos.toList());
  }
}

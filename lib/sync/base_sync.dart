import 'package:meta/meta.dart';
import 'package:result/result.dart';

import '/classes/syncable.dart';
import '/state/time_out.dart';

const syncFileName = 'todos.doable.toml';
const syncTodoListsFileName = 'todo_lists.doable.toml';

enum SyncError {
  wrongUserOrPassword,
  wrongUrl,
  doesNotExist,
  corruptedFile,
  maintenance,
  unavailable,
  unknownError,
  noInternetConnection,
  connectionTimedOut,
  tooManyConnectionTimeOuts,
}

enum Sync {
  updated,
  unchanged;

  operator +(Sync other) =>
      switch (this) { updated => other, unchanged => unchanged };
}

typedef SyncResult = Result<Sync, SyncError>;

enum SyncType { nextcloud, webDAV, localStorage }

@Deprecated('Transition to Remote')
abstract class BaseSync<T extends Syncable<T>> {
  const BaseSync();

  /// Stores the given Todos on the remote.
  FutureResult<(), SyncError> push(Iterable<T> values);

  /// Retrieves all stored Todos.
  FutureResult<Iterable<T>, SyncError> pull();

  /// Log out of synced account.
  FutureResult<(), SyncError> logOut();

  bool get shouldMakeRequest => !timedOutConnectionStore.hitTimeOutLimit(
        runtimeType.toString(),
      );

  @mustCallSuper
  SyncError onTimeOut(SyncError error) {
    if (error == SyncError.connectionTimedOut) {
      timedOutConnectionStore.timedOut(runtimeType.toString());
      if (!shouldMakeRequest) return SyncError.tooManyConnectionTimeOuts;
    }

    return error;
  }
}

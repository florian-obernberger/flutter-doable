import 'dart:convert';
import 'dart:typed_data';

import 'package:path/path.dart' as path;
import 'package:result/result.dart';

import '/data/todo_list.dart';
import '/data/converters/toml/todo_list.dart';
import '/classes/logger.dart';
import '/types/fetch_exception.dart';

import '../base_sync.dart';

import '../nextcloud_sync.d/api.dart';
import '../nextcloud_sync.d/parse_nc_status.dart';

@Deprecated('Transition to Remote')
class TodoListNextcloudSync extends BaseSync<TodoList> {
  TodoListNextcloudSync({
    required this.baseUrl,
    required this.folder,
    required this.user,
    required this.password,
  }) : _api = NextcloudApi(baseUrl: baseUrl, password: password, user: user);

  factory TodoListNextcloudSync.fromQr(String qrContent, [String? folder]) {
    final decoded = Uri.decodeFull(qrContent);

    final fields = Map.fromEntries(
      decoded.substring(11).split('&').map<MapEntry<String, String>>(
        (field) {
          final parts = field.split(':');
          final key = parts.first;
          parts.removeAt(0);
          final value = parts.join(':');
          return MapEntry(key, value);
        },
      ),
    );

    return TodoListNextcloudSync(
      baseUrl: fields['server']!,
      folder: folder ?? '',
      user: fields['user']!,
      password: fields['password']!,
    );
  }

  final String baseUrl;
  String folder;
  final String user;
  final String password;

  final NextcloudApi _api;

  @override
  FutureResult<Iterable<TodoList>, SyncError> pull() async {
    if (!shouldMakeRequest) {
      return const Err(SyncError.tooManyConnectionTimeOuts);
    }

    final loadRes =
        await _api.loadFile(path.join(folder, syncTodoListsFileName));

    if (loadRes.isErr()) {
      return loadRes.map((value) => <TodoList>[]).mapErr(_decodeFetchException);
    }

    final res = loadRes.unwrap();

    if (res.status == NextcloudApiStatus.ok ||
        res.status == NextcloudApiStatus.created ||
        res.status == NextcloudApiStatus.savedChanges) {
      final source = utf8.decode(res.bodyBytes);
      if (source.isEmpty) {
        logger.i('There are no Todos stored in todos.doable.toml');
        return const Ok(<TodoList>[]);
      }

      return parseTodoLists(source)
          .map((todos) => todos
              .where((todo) => todo.inspectErr((err) {
                    logger.e(err, message: "Couldn't decode Todo List");
                  }).isOk())
              .map((todo) => todo.unwrap()))
          .okOr(SyncError.corruptedFile)
          .inspectErr((err) {
        logger.e(err, message: "Couldn't parse $syncFileName");
      });
    }

    return parseNcStatus(res.status).map<List<TodoList>>((_) => const []);
  }

  @override
  FutureResult<(), SyncError> push(Iterable<TodoList> values) async {
    if (!shouldMakeRequest) {
      return const Err(SyncError.tooManyConnectionTimeOuts);
    }

    final content = storeTodoLists(values);

    final storeRes =
        await _api.storeFile(path.join(folder, syncTodoListsFileName), content);

    if (storeRes.isErr()) {
      return storeRes.map((value) => ()).mapErr(_decodeFetchException);
    }

    return parseNcStatus(storeRes.unwrap());
  }

  FutureResult<(), SyncError> deleteBackupFile() async {
    return await _api
        .delete(path.join(folder, syncTodoListsFileName))
        .mapErr(_decodeFetchException<()>);
  }

  @override
  FutureResult<(), SyncError> logOut() =>
      _api.logOut().mapErr(_decodeFetchException);

  Map<String, String> get authHeaders => _api.authHeaders;

  String avatarUrl([int size = 512]) => _api.getAvatarUrl(size);

  FutureResult<Uint8List, SyncError> avatar([int size = 512]) =>
      _api.getAvatar(size).mapErr(_decodeFetchException);

  SyncError _decodeFetchException<S>(FetchException error) {
    if (error.isHttpClientException) {
      final exception = error.asHttpClientException();
      if (exception.message == 'Connection timed out') {
        return SyncError.connectionTimedOut;
      }

      return SyncError.noInternetConnection;
    } else {
      return SyncError.unknownError;
    }
  }
}

import 'dart:convert';
import 'dart:typed_data';

import 'package:result/result.dart';
import 'package:http/http.dart' as http;
import 'package:path/path.dart' as path;

import '/data/todo_list.dart';
import '/data/converters/toml/todo_list.dart';
import '/classes/logger.dart';

import '../base_sync.dart';

const _loggerTag = 'WebDAVSyncList';

@Deprecated('Transition to Remote')
class TodoListWebDAVSync extends BaseSync<TodoList> {
  TodoListWebDAVSync({
    required this.baseUrl,
    required this.user,
    required this.password,
  }) : _authHeaders = {
          'Authorization':
              "Basic ${base64.encode(utf8.encode('$user:$password'))}"
        };

  final String baseUrl;
  final String user;
  final String password;

  final Map<String, String> _authHeaders;

  @override
  FutureResult<(), SyncError> logOut() async {
    return const Ok(());
  }

  @override
  FutureResult<Iterable<TodoList>, SyncError> pull() async {
    if (!shouldMakeRequest) {
      return const Err(SyncError.tooManyConnectionTimeOuts);
    }

    late final Result<http.Response, SyncError> fetchRes;
    try {
      fetchRes = Ok(await http.get(
        Uri.parse(path.join(baseUrl, syncTodoListsFileName)),
        headers: _authHeaders,
      ));
    } on http.ClientException catch (e, s) {
      logger.e(
        e,
        message: 'Tried fetching $baseUrl, but failed',
        tag: _loggerTag,
        stackTrace: s,
      );
      fetchRes = const Err(SyncError.noInternetConnection);
    }

    return fetchRes.andThen(
      (res) => mapStatusCode(res, () => deserializeTodoLists(res.bodyBytes)),
    );
  }

  @override
  FutureResult<(), SyncError> push(Iterable<TodoList> values) async {
    if (!shouldMakeRequest) {
      return const Err(SyncError.tooManyConnectionTimeOuts);
    }

    final content = storeTodoLists(values);

    late final Result<http.Response, SyncError> storeRes;
    try {
      storeRes = Ok(await http.put(
        Uri.parse(path.join(baseUrl, syncTodoListsFileName)),
        headers: _authHeaders,
        body: content,
        encoding: utf8,
      ));
    } on http.ClientException catch (e, s) {
      logger.e(
        e,
        message: 'Tried storing $baseUrl, but failed',
        tag: _loggerTag,
        stackTrace: s,
      );
      storeRes = const Err(SyncError.noInternetConnection);
    }

    return storeRes.andThen((res) => mapStatusCode(res, () => const Ok(())));
  }

  Result<T, SyncError> mapStatusCode<T>(
    http.Response res,
    Result<T, SyncError> Function() onOk,
  ) {
    if (res.statusCode == 200 ||
        res.statusCode == 204 ||
        res.statusCode == 201) {
      return onOk();
    }

    if (res.statusCode == 401) {
      logger.i(
        'User specified wrong user or password.',
        tag: _loggerTag,
        more: res.headers,
      );
      return const Err(SyncError.wrongUserOrPassword);
    }

    if (res.statusCode == 403) {
      logger.i(
        'User does not have permission for this action.',
        tag: _loggerTag,
        more: res.headers,
      );
      return const Err(SyncError.wrongUserOrPassword);
    }

    if (res.statusCode == 404) {
      logger.i(
        "Couldn't find resource $baseUrl",
        tag: _loggerTag,
        more: res.headers,
      );
      return const Err(SyncError.doesNotExist);
    }

    if (res.statusCode == 503) {
      return const Err(SyncError.unavailable);
    }

    logger.w(
      'Could not match status code ${res.statusCode}.',
      tag: _loggerTag,
      more: res.headers,
    );
    return const Err(SyncError.unknownError);
  }

  Result<List<TodoList>, SyncError> deserializeTodoLists(Uint8List bytes) {
    return parseTodoLists(utf8.decode(bytes))
        .map((todos) => todos
            .where((todo) => todo.inspectErr((err) {
                  logger.e(err, message: "Couldn't decode Todo List");
                }).isOk())
            .map((todo) => todo.unwrap()))
        .okOr(SyncError.corruptedFile)
        .inspectErr((err) {
      logger.e(err, message: "Couldn't parse $syncFileName");
    }).map((todos) => todos.toList());
  }
}

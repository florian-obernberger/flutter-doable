import 'dart:io';

import 'package:result/result.dart';
import 'package:path/path.dart' as path;

import '/data/todo.dart';
import '/data/converters/toml/todo.dart';
import '/classes/logger.dart';

import 'base_sync.dart';

@Deprecated('Transition to Remote')
class LocalStorageSync extends BaseSync<Todo> {
  const LocalStorageSync(this.directory);

  final Directory directory;

  File get file {
    final file = File(path.join(directory.path, syncFileName));
    if (!file.existsSync()) file.createSync(recursive: true);
    return file;
  }

  @override
  FutureResult<(), SyncError> logOut() async {
    try {
      await file.delete();
      return const Ok(());
    } on Exception catch (e, s) {
      logger.e(message: "[ls] couldn't delete file", e, stackTrace: s);
      return const Err(SyncError.unknownError);
    }
  }

  @override
  FutureResult<Iterable<Todo>, SyncError> pull() async {
    final source = await file.readAsString();

    return parseTodos(source)
        .map((todos) => todos
            .where((todo) => todo.inspectErr((err) {
                  logger.e(err, message: "Couldn't decode Todo");
                }).isOk())
            .map((todo) => todo.unwrap()))
        .okOr(SyncError.corruptedFile)
        .inspectErr((err) {
      logger.e(err, message: "Couldn't parse $syncFileName");
    });
  }

  @override
  FutureResult<(), SyncError> push(Iterable<Todo> values) async {
    try {
      await file.writeAsString(storeTodos(values));
      return const Ok(());
    } on Exception catch (e, s) {
      logger.e(message: '[ls] could not write file', e, stackTrace: s);
      return const Err(SyncError.corruptedFile);
    }
  }
}

import 'package:flutter/material.dart';

import '/classes/motion.dart';

typedef AsyncDataCallback<T> = Widget Function(BuildContext context, T data);
typedef AsyncErrorCallback = Widget Function(
  BuildContext context,
  Object error,
);
typedef AsyncLoadingCallback = Widget Function(BuildContext context);
typedef AsyncOrElseCallback = Widget Function(BuildContext context);

extension AsyncBuilderExtension<T> on Future<T> {
  AsyncBuilder<T> map({
    required AsyncDataCallback<T> data,
    required AsyncErrorCallback error,
    required AsyncLoadingCallback loading,
    Duration duration = Motion.medium1,
    Duration reverseDuration = Motion.short4,
    Curve switchInCurve = Easing.standardDecelerate,
    Curve switchOutCurve = Easing.standardAccelerate,
  }) =>
      AsyncBuilder(
        future: this,
        data: data,
        error: error,
        loading: loading,
        duration: duration,
        reverseDuration: reverseDuration,
        switchInCurve: switchInCurve,
        switchOutCurve: switchOutCurve,
      );

  AsyncBuilder<T> mapDataOrElse({
    required AsyncDataCallback<T> data,
    required AsyncOrElseCallback orElse,
    Duration duration = Motion.medium1,
    Duration reverseDuration = Motion.short4,
    Curve switchInCurve = Easing.standardDecelerate,
    Curve switchOutCurve = Easing.standardAccelerate,
  }) =>
      AsyncBuilder.dataOrElse(
        future: this,
        data: data,
        orElse: orElse,
        duration: duration,
        reverseDuration: reverseDuration,
        switchInCurve: switchInCurve,
        switchOutCurve: switchOutCurve,
      );
}

class AsyncBuilder<T> extends StatelessWidget {
  const AsyncBuilder({
    required this.future,
    required this.data,
    required this.error,
    required this.loading,
    this.duration = Motion.medium1,
    this.reverseDuration = Motion.short4,
    this.switchInCurve = Easing.standardDecelerate,
    this.switchOutCurve = Easing.standardAccelerate,
    super.key,
  });

  factory AsyncBuilder.dataOrElse({
    Key? key,
    required Future<T> future,
    required AsyncDataCallback<T> data,
    required AsyncOrElseCallback orElse,
    Duration duration = Motion.medium1,
    Duration reverseDuration = Motion.short4,
    Curve switchInCurve = Easing.standardDecelerate,
    Curve switchOutCurve = Easing.standardAccelerate,
  }) =>
      AsyncBuilder(
        key: key,
        future: future,
        data: data,
        loading: orElse,
        error: (context, _) => orElse(context),
        duration: duration,
        reverseDuration: reverseDuration,
        switchInCurve: switchInCurve,
        switchOutCurve: switchOutCurve,
      );

  /// Th [Future] containing the potential data that will be passed to [data].
  final Future<T> future;

  /// Gets called when the future resolved without an error.
  final AsyncDataCallback<T> data;

  /// Gets called when the future resolved with an error.
  final AsyncErrorCallback error;

  /// Gets called when the future is not yet resolved.
  final AsyncLoadingCallback loading;

  /// The animation curve to use when transitioning in a new state.
  ///
  /// This curve is applied to the given state when that property is set to a
  /// new state.
  final Curve switchInCurve;

  /// The animation curve to use when transitioning a previous state out.
  ///
  /// This curve is applied to the state when the child is faded in
  /// (or when the widget is created, for the first state).
  final Curve switchOutCurve;

  /// The duration of the transition from the old state to the new one.
  final Duration duration;

  /// The duration of the transition from the new state to the old one.
  final Duration reverseDuration;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<T>(
      future: future,
      builder: (context, snapshot) {
        final Widget content;

        if (snapshot.connectionState != ConnectionState.done) {
          content = SizedBox(
            key: UniqueKey(),
            child: loading(context),
          );
        } else if (snapshot.hasData) {
          content = SizedBox(
            key: UniqueKey(),
            child: data(context, snapshot.data as T),
          );
        } else if (snapshot.hasError) {
          content = SizedBox(
            key: UniqueKey(),
            child: error(context, snapshot.error!),
          );
        } else {
          content = SizedBox(
            key: UniqueKey(),
            child: loading(context),
          );
        }

        return AnimatedSwitcher(
          key: content.key,
          duration: duration,
          reverseDuration: reverseDuration,
          switchInCurve: switchInCurve,
          switchOutCurve: switchOutCurve,
          child: content,
        );
      },
    );
  }
}

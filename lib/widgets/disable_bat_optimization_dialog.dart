import 'package:flutter/material.dart';
import 'package:material_symbols_icons/symbols.dart';

import '/strings/strings.dart';

class DisableBatOptimizationDialog extends StatelessWidget {
  const DisableBatOptimizationDialog({super.key});

  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context)!;
    final scheme = Theme.of(context).colorScheme;

    return AlertDialog(
      icon: Icon(Symbols.battery_alert, color: scheme.error),
      title: Text(strings.batteryOptimization),
      content: Text(strings.batteryOptimizationDescription),
      actions: <Widget>[
        TextButton(
          style: TextButton.styleFrom(foregroundColor: scheme.error),
          onPressed: () => Navigator.of(context).pop(false),
          child: Text(strings.cancel),
        ),
        FilledButton(
          style: FilledButton.styleFrom(
            backgroundColor: scheme.error,
            foregroundColor: scheme.onError,
          ),
          onPressed: () => Navigator.of(context).pop(true),
          child: Text(strings.disable),
        ),
      ],
    );
  }
}

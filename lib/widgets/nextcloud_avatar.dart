import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:result/result.dart';

import '/state/secure_storage.dart';
import '/classes/logger.dart';
import '/widgets/async_builder.dart';

enum AvatarShape {
  circle(CircleAvatarClipper()),
  circle8(Circle8AvatarClipper()),
  circle12(Circle12AvatarClipper());

  const AvatarShape(this.clipper);
  final CustomClipper<Path> clipper;
}

final _avatarUrlProvider = FutureProvider((ref) async {
  final neon = await ref.read(secureStorageDbProvider).readNextcloudRemote();
  final res = await neon.fetchAvatarUrl().inspectErr((error) => logger.ah(
        error,
        message: 'Could not fetch avatar url',
        tag: 'NextcloudAvatar',
      ));

  return switch (res) {
    Ok(value: final val) => val,
    Err() => null,
  };
});

class NextcloudAvatar extends ConsumerStatefulWidget {
  const NextcloudAvatar({
    this.imageSize = 30,
    this.iconSize = 24,
    this.shape = AvatarShape.circle8,
    super.key,
  });

  final double imageSize;
  final double iconSize;
  final AvatarShape shape;

  static Future<bool> clearCache(WidgetRef ref) async {
    final res = ref.read(_avatarUrlProvider);

    return switch (res.value) {
      (url: final url, headers: final _) => CachedNetworkImage.evictFromCache(
          url.toString(),
        ),
      _ => Future.value(false),
    };
  }

  @override
  ConsumerState<ConsumerStatefulWidget> createState() =>
      _NextcloudAvatarState();
}

class _NextcloudAvatarState extends ConsumerState<NextcloudAvatar> {
  late final nameFuture = ref
      .read(secureStorageDbProvider)
      .readNextcloudRemote()
      .then((neon) => neon.user);

  @override
  Widget build(BuildContext context) {
    final scheme = Theme.of(context).colorScheme;
    final text = Theme.of(context).textTheme;

    final placeholder = ClipPath(
      clipBehavior: Clip.antiAlias,
      clipper: widget.shape.clipper,
      child: Container(
        width: widget.imageSize,
        height: widget.imageSize,
        color: scheme.tertiaryContainer,
        child: nameFuture.mapDataOrElse(
          data: (context, name) => Text(
            name.characters.first.toUpperCase(),
            style: text.bodyLarge!.copyWith(
              color: scheme.onTertiaryContainer,
              fontSize: widget.imageSize * 0.5,
              height: 1,
              fontWeight: FontWeight.w500,
            ),
            textAlign: TextAlign.center,
          ),
          orElse: (_) => const SizedBox.shrink(),
        ),
      ),
    );

    return ref.watch(_avatarUrlProvider).map(
          loading: (_) => placeholder,
          error: (_) => placeholder,
          data: (data) => switch (data.value) {
            null when data.hasValue => placeholder,
            null => placeholder,
            (url: final url, headers: final headers) => CachedNetworkImage(
                imageUrl: url.toString(),
                httpHeaders: headers,
                width: widget.imageSize,
                height: widget.imageSize,
                alignment: Alignment.center,
                fit: BoxFit.cover,
                errorWidget: (context, _, __) => placeholder,
                imageBuilder: (context, imageProvider) {
                  return ClipPath(
                    clipBehavior: Clip.antiAliasWithSaveLayer,
                    clipper: widget.shape.clipper,
                    child: Image(
                      image: imageProvider,
                      height: widget.imageSize,
                      width: widget.imageSize,
                    ),
                  );
                },
                placeholder: (context, _) => placeholder,
              )
          },
        );
  }
}

class Circle8AvatarClipper extends CustomClipper<Path> {
  const Circle8AvatarClipper();

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) => false;

  @override
  Path getClip(Size size) {
    final path = Path();
    path.lineTo(size.width / 2, 0);
    path.cubicTo(size.width * 0.44, 0, size.width * 0.39, size.height * 0.06,
        size.width / 3, size.height * 0.08);
    path.cubicTo(size.width * 0.27, size.height * 0.11, size.width * 0.19,
        size.height * 0.1, size.width * 0.15, size.height * 0.15);
    path.cubicTo(size.width * 0.1, size.height * 0.19, size.width * 0.11,
        size.height * 0.27, size.width * 0.08, size.height / 3);
    path.cubicTo(size.width * 0.06, size.height * 0.39, 0, size.height * 0.44,
        0, size.height / 2);
    path.cubicTo(0, size.height * 0.56, size.width * 0.06, size.height * 0.61,
        size.width * 0.08, size.height * 0.67);
    path.cubicTo(size.width * 0.11, size.height * 0.73, size.width * 0.1,
        size.height * 0.81, size.width * 0.15, size.height * 0.85);
    path.cubicTo(size.width * 0.19, size.height * 0.9, size.width * 0.27,
        size.height * 0.89, size.width / 3, size.height * 0.92);
    path.cubicTo(size.width * 0.39, size.height * 0.94, size.width * 0.44,
        size.height, size.width / 2, size.height);
    path.cubicTo(size.width * 0.56, size.height, size.width * 0.61,
        size.height * 0.94, size.width * 0.67, size.height * 0.92);
    path.cubicTo(size.width * 0.73, size.height * 0.89, size.width * 0.81,
        size.height * 0.9, size.width * 0.85, size.height * 0.85);
    path.cubicTo(size.width * 0.9, size.height * 0.81, size.width * 0.89,
        size.height * 0.73, size.width * 0.92, size.height * 0.67);
    path.cubicTo(size.width * 0.94, size.height * 0.61, size.width,
        size.height * 0.56, size.width, size.height / 2);
    path.cubicTo(size.width, size.height * 0.44, size.width * 0.94,
        size.height * 0.39, size.width * 0.92, size.height / 3);
    path.cubicTo(size.width * 0.89, size.height * 0.27, size.width * 0.9,
        size.height * 0.19, size.width * 0.85, size.height * 0.15);
    path.cubicTo(size.width * 0.81, size.height * 0.1, size.width * 0.73,
        size.height * 0.11, size.width * 0.67, size.height * 0.08);
    path.cubicTo(size.width * 0.61, size.height * 0.06, size.width * 0.56, 0,
        size.width / 2, 0);
    path.cubicTo(size.width / 2, 0, size.width / 2, 0, size.width / 2, 0);

    return path;
  }
}

class Circle12AvatarClipper extends CustomClipper<Path> {
  const Circle12AvatarClipper();

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) => false;

  @override
  Path getClip(Size size) {
    final path = Path();
    path.lineTo(size.width / 2, 0);
    path.cubicTo(size.width * 0.46, 0, size.width * 0.42, size.height * 0.05,
        size.width * 0.38, size.height * 0.06);
    path.cubicTo(size.width * 0.34, size.height * 0.07, size.width * 0.29,
        size.height * 0.05, size.width / 4, size.height * 0.07);
    path.cubicTo(size.width / 5, size.height * 0.09, size.width / 5,
        size.height * 0.15, size.width * 0.18, size.height * 0.18);
    path.cubicTo(size.width * 0.15, size.height / 5, size.width * 0.09,
        size.height / 5, size.width * 0.07, size.height / 4);
    path.cubicTo(size.width * 0.05, size.height * 0.29, size.width * 0.07,
        size.height * 0.34, size.width * 0.06, size.height * 0.38);
    path.cubicTo(size.width * 0.05, size.height * 0.42, 0, size.height * 0.46,
        0, size.height / 2);
    path.cubicTo(0, size.height * 0.54, size.width * 0.05, size.height * 0.58,
        size.width * 0.06, size.height * 0.62);
    path.cubicTo(size.width * 0.07, size.height * 0.66, size.width * 0.05,
        size.height * 0.71, size.width * 0.07, size.height * 0.75);
    path.cubicTo(size.width * 0.09, size.height * 0.79, size.width * 0.15,
        size.height * 0.79, size.width * 0.18, size.height * 0.82);
    path.cubicTo(size.width / 5, size.height * 0.85, size.width / 5,
        size.height * 0.91, size.width / 4, size.height * 0.93);
    path.cubicTo(size.width * 0.29, size.height * 0.95, size.width * 0.34,
        size.height * 0.93, size.width * 0.38, size.height * 0.94);
    path.cubicTo(size.width * 0.42, size.height * 0.95, size.width * 0.46,
        size.height, size.width / 2, size.height);
    path.cubicTo(size.width * 0.54, size.height, size.width * 0.58,
        size.height * 0.95, size.width * 0.62, size.height * 0.94);
    path.cubicTo(size.width * 0.66, size.height * 0.93, size.width * 0.71,
        size.height * 0.95, size.width * 0.75, size.height * 0.93);
    path.cubicTo(size.width * 0.79, size.height * 0.91, size.width * 0.79,
        size.height * 0.85, size.width * 0.82, size.height * 0.82);
    path.cubicTo(size.width * 0.85, size.height * 0.79, size.width * 0.91,
        size.height * 0.79, size.width * 0.93, size.height * 0.75);
    path.cubicTo(size.width * 0.95, size.height * 0.71, size.width * 0.93,
        size.height * 0.66, size.width * 0.94, size.height * 0.62);
    path.cubicTo(size.width * 0.95, size.height * 0.58, size.width,
        size.height * 0.54, size.width, size.height / 2);
    path.cubicTo(size.width, size.height * 0.46, size.width * 0.95,
        size.height * 0.42, size.width * 0.94, size.height * 0.38);
    path.cubicTo(size.width * 0.93, size.height * 0.34, size.width * 0.95,
        size.height * 0.29, size.width * 0.93, size.height / 4);
    path.cubicTo(size.width * 0.91, size.height / 5, size.width * 0.85,
        size.height / 5, size.width * 0.82, size.height * 0.18);
    path.cubicTo(size.width * 0.79, size.height * 0.15, size.width * 0.79,
        size.height * 0.09, size.width * 0.75, size.height * 0.07);
    path.cubicTo(size.width * 0.71, size.height * 0.05, size.width * 0.66,
        size.height * 0.07, size.width * 0.62, size.height * 0.06);
    path.cubicTo(size.width * 0.58, size.height * 0.05, size.width * 0.54, 0,
        size.width / 2, 0);
    path.cubicTo(size.width / 2, 0, size.width / 2, 0, size.width / 2, 0);

    return path;
  }
}

class CircleAvatarClipper extends CustomClipper<Path> {
  const CircleAvatarClipper();

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) => false;

  @override
  Path getClip(Size size) {
    final path = Path();
    path.lineTo(size.width, size.height / 2);
    path.cubicTo(size.width, size.height * 0.78, size.width * 0.78, size.height,
        size.width / 2, size.height);
    path.cubicTo(size.width * 0.22, size.height, 0, size.height * 0.78, 0,
        size.height / 2);
    path.cubicTo(
        0, size.height * 0.22, size.width * 0.22, 0, size.width / 2, 0);
    path.cubicTo(size.width * 0.78, 0, size.width, size.height * 0.22,
        size.width, size.height / 2);
    return path;
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:result/result.dart';

import '/strings/strings.dart';
import '/state/secure_storage.dart';
import '/state/todo_db.dart';
import '/state/todo_list_db.dart';
import '/sync/base_sync.dart';

@Deprecated('Transition to Remote')
class FixCorruptedDialog extends ConsumerStatefulWidget {
  const FixCorruptedDialog(this.onError, {super.key});

  final void Function(BuildContext context, SyncError error) onError;

  @override
  ConsumerState<FixCorruptedDialog> createState() => _FixCorruptedDialogState();
}

@Deprecated('Transition to Remote')
class _FixCorruptedDialogState extends ConsumerState<FixCorruptedDialog> {
  bool isFixing = false;

  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context)!;
    final scheme = Theme.of(context).colorScheme;

    return PopScope(
      canPop: false,
      child: AlertDialog(
        title: Text(strings.whatWentWrong),
        content: Text(strings.fixCorruptBackupFile),
        actions: [
          OutlinedButton(
            onPressed: isFixing ? null : Navigator.of(context).pop,
            child: Text(strings.cancel),
          ),
          FilledButton(
            style: FilledButton.styleFrom(
              backgroundColor: scheme.error,
              foregroundColor: scheme.onError,
            ),
            onPressed: () {
              if (isFixing) return;

              setState(() => isFixing = true);
              fixCorruption().then((res) {
                Navigator.of(context).pop();
                switch (res) {
                  case Err():
                    widget.onError(context, res.error);
                  case Ok():
                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(
                        duration: const Duration(seconds: 2),
                        dismissDirection: DismissDirection.vertical,
                        margin: const EdgeInsets.fromLTRB(24, 8, 24, 18),
                        content: Text(strings.syncSuccessful),
                      ),
                    );
                }
              });
            },
            child: Stack(
              alignment: Alignment.center,
              children: <Widget>[
                if (isFixing)
                  SizedBox(
                    height: 24,
                    width: 24,
                    child: CircularProgressIndicator(
                      strokeWidth: 2.5,
                      color: scheme.onError,
                      backgroundColor: Colors.transparent,
                    ),
                  ),
                Visibility(
                  visible: !isFixing,
                  maintainSize: true,
                  maintainAnimation: true,
                  maintainState: true,
                  child: Text(strings.doContinue),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  FutureResult<Sync, SyncError> fixCorruption() async {
    final secureStorage = ref.read(secureStorageDbProvider);
    final todoDb = ref.read(todoDatabaseProvider);
    final todoListDb = ref.read(todoListDatabaseProvider);

    final nc = await secureStorage.readNextcloudSync();
    final ncList = await secureStorage.readTodoListNextcloudSync();

    return nc
        .deleteBackupFile()
        .andThen((_) => ncList.deleteBackupFile())
        .map((_) => Sync.unchanged)
        .andThen((_) => todoDb.syncTodos())
        .andThen((_) => todoListDb.syncLists());
  }
}

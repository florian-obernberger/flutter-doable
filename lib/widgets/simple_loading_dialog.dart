import 'package:flutter/material.dart';
import 'package:gap/gap.dart';

class SimpleLoadingDialog extends StatelessWidget {
  const SimpleLoadingDialog(
    this.message, {
    this.progress,
    this.dismissible = false,
    super.key,
  });

  /// The message that is show while loading. Usually ends with an ellipsis.
  final String message;

  /// A value between 0 and 1 that indicates how far along the progress is.
  final double? progress;

  final bool dismissible;

  @override
  Widget build(BuildContext context) {
    final scheme = Theme.of(context).colorScheme;
    final text = Theme.of(context).textTheme;

    return PopScope(
      canPop: dismissible,
      child: AlertDialog(
        contentPadding: const EdgeInsets.fromLTRB(36, 24, 18, 24),
        contentTextStyle: text.bodyMedium?.copyWith(
          color: scheme.onSurfaceVariant,
        ),
        content: ConstrainedBox(
          constraints: const BoxConstraints(maxWidth: 640),
          child: Row(
            children: <Widget>[
              CircularProgressIndicator(
                value: progress,
                backgroundColor: Colors.transparent,
              ),
              const Gap(24),
              Expanded(child: SizedBox(width: 10000, child: Text(message))),
            ],
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:gap/gap.dart';

class RichTooltip extends StatelessWidget {
  RichTooltip({
    super.key,
    this.subhead,
    required this.supportingText,
    this.child,
    this.actions,
    this.preferBelow,
    this.triggerMode,
  }) : assert(actions == null || actions.isNotEmpty);

  final String? subhead;
  final String supportingText;
  final Widget? child;
  final List<Widget>? actions;
  final bool? preferBelow;
  final TooltipTriggerMode? triggerMode;

  @override
  Widget build(BuildContext context) {
    final scheme = Theme.of(context).colorScheme;
    final text = Theme.of(context).textTheme;

    return Tooltip(
      triggerMode: triggerMode,
      margin: EdgeInsets.zero,
      preferBelow: preferBelow,
      showDuration: const Duration(minutes: 1),
      decoration: const BoxDecoration(),
      richMessage: WidgetSpan(
        child: ConstrainedBox(
          constraints: const BoxConstraints(maxWidth: 312),
          child: Card(
            color: scheme.surfaceContainer,
            elevation: 2,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(12),
            ),
            child: Padding(
              padding: EdgeInsetsDirectional.fromSTEB(
                16,
                12,
                16,
                (actions?.isEmpty ?? true) ? 12 : 8,
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  if (subhead != null) ...[
                    Text(
                      subhead!,
                      style: text.titleSmall?.copyWith(
                        color: scheme.onSurfaceVariant,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    const Gap(4)
                  ],
                  Text(
                    supportingText,
                    style: text.bodyMedium?.copyWith(
                      color: scheme.onSurfaceVariant,
                    ),
                  ),
                  if (actions != null) ...[
                    const Gap(12),
                    ...actions!,
                  ]
                ],
              ),
            ),
          ),
        ),
      ),
      child: child,
    );
  }
}

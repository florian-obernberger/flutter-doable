import 'package:flutter/material.dart';
import 'package:gap/gap.dart';
import 'package:go_router/go_router.dart';
import 'package:numberpicker/numberpicker.dart';

import '/strings/strings.dart';
import '/util/animated_dialog.dart';

Future<Duration?> showAnimatedDurationPicker({
  required BuildContext context,
  required String title,
  List<DurationPart> parts = DurationPart._default,
  Duration? initialDuration,
}) {
  return showAnimatedDialog<Duration>(
    context: context,
    builder: (context) => DurationPicker(
      initialDuration: initialDuration,
      title: title,
      parts: parts,
    ),
    animationType: DialogTransitionType.fadeScale,
  );
}

enum DurationPart {
  day(364),
  hour(23),
  minute(59),
  second(59);

  const DurationPart(this.maxValue);

  final int maxValue;

  static const _default = [hour, minute, second];
}

class DurationPicker extends StatefulWidget {
  const DurationPicker({
    required this.title,
    this.initialDuration,
    this.parts = DurationPart._default,
    super.key,
  });

  final String title;
  final Duration? initialDuration;
  final List<DurationPart> parts;

  @override
  State<DurationPicker> createState() => _DurationPickerState();
}

class _DurationPickerState extends State<DurationPicker> {
  final current = {
    DurationPart.day: 0,
    DurationPart.hour: 0,
    DurationPart.minute: 15,
    DurationPart.second: 0,
  };

  @override
  void initState() {
    super.initState();

    if (widget.initialDuration != null) {
      final duration = widget.initialDuration!.inMicroseconds;

      current[DurationPart.day] = duration ~/ Duration.microsecondsPerDay;
      current[DurationPart.hour] = (duration % Duration.microsecondsPerDay) ~/
          Duration.microsecondsPerHour;
      current[DurationPart.minute] =
          (duration % Duration.microsecondsPerHour) ~/
              Duration.microsecondsPerMinute;
      current[DurationPart.second] =
          (duration % Duration.microsecondsPerMinute) ~/
              Duration.microsecondsPerSecond;
    }
  }

  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context)!;

    return AlertDialog(
      title: Text(widget.title),
      content: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          for (int i = 0; i < widget.parts.length; i++) ...[
            buildPart(widget.parts[i], isLast: i == widget.parts.length - 1)
          ],
        ],
      ),
      actions: <Widget>[
        TextButton(
          onPressed: context.pop,
          child: Text(strings.cancel),
        ),
        TextButton(
          onPressed: () => context.pop(Duration(
            days: current[DurationPart.day]!,
            hours: current[DurationPart.hour]!,
            minutes: current[DurationPart.minute]!,
            seconds: current[DurationPart.second]!,
          )),
          child: Text(strings.ok),
        ),
      ],
    );
  }

  Widget buildPart(DurationPart part, {required bool isLast}) {
    final strings = Strings.of(context)!;

    final scheme = Theme.of(context).colorScheme;
    final text = Theme.of(context).textTheme;

    final style = text.titleMedium!.copyWith(
      color: scheme.primary.withOpacity(0.38),
    );
    final selectedStyle = text.titleLarge!.copyWith(
      color: scheme.primary,
      fontWeight: FontWeight.w500,
    );
    final headerStyle = text.labelMedium!.copyWith(
      color: scheme.onSurfaceVariant,
    );

    final header = switch (part) {
      DurationPart.day => strings.days,
      DurationPart.hour => strings.hours,
      DurationPart.minute => strings.minutes,
      DurationPart.second => strings.seconds,
    };

    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsetsDirectional.only(start: 4),
          child: Text(header, style: headerStyle),
        ),
        const Gap(8),
        Row(
          children: <Widget>[
            NumberPicker(
              textStyle: style,
              selectedTextStyle: selectedStyle,
              itemWidth: 52,
              itemHeight: selectedStyle.height! * selectedStyle.fontSize! * 1.5,
              infiniteLoop: true,
              minValue: 0,
              maxValue: part.maxValue,
              value: current[part]!,
              onChanged: (value) => setState(() => current[part] = value),
            ),
            if (!isLast)
              Padding(
                padding: const EdgeInsets.only(bottom: 4),
                child: Text(':', style: selectedStyle),
              )
          ],
        ),
      ],
    );
  }
}

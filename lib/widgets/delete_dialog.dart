import 'package:flutter/material.dart';

import '/strings/strings.dart';
import '/util/animated_dialog.dart';

/// Shows an animated Dialog that asks they user if they are sure they want to
/// delete whatever you ask them to delete.
Future<bool?> showDeleteDialog({
  required BuildContext context,
  String? title,
  required String description,
  bool discard = false,
}) async =>
    showAnimatedDialog<bool>(
      context: context,
      builder: (context) => DeleteDialog(
        description: description,
        title: title,
        discard: discard,
      ),
    );

class DeleteDialog extends StatelessWidget {
  const DeleteDialog({
    this.title,
    required this.description,
    this.discard = false,
    super.key,
  });

  /// The title of the Dialog. Defaults to [Strings.of(context).attention].
  final String? title;

  /// The description of the Dialog.
  final String description;

  /// Weather to show "continue" or "discard".
  final bool discard;

  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context)!;

    return AlertDialog(
      title: Text(title ?? strings.attention),
      content: ConstrainedBox(
        constraints: const BoxConstraints(minWidth: 280, maxWidth: 560),
        child: Text(description),
      ),
      actions: [
        TextButton(
          onPressed: Navigator.of(context).pop,
          child: Text(strings.cancel),
        ),
        TextButton(
          onPressed: () => Navigator.of(context).pop(true),
          child: Text(discard ? strings.discard : strings.doContinue),
        ),
      ],
    );
  }
}

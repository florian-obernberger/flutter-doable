import 'package:custom_text/custom_text.dart';
import 'package:flutter/material.dart';
import 'package:gap/gap.dart';
import 'package:material_symbols_icons/symbols.dart';

typedef LineBuilder = Widget Function(String line, TextStyle style);

class MdTaskListMatcher extends TextMatcher {
  const MdTaskListMatcher() : super(r'(\n|^)((  )*)[-\+\*] \[([ xX])\] (.*)');
}

class MdTaskListDefinition extends SpanDefinition {
  MdTaskListDefinition({
    required TextStyle style,
    required LineBuilder lineBuilder,
    TextStyle? completedStyle,
  }) : super(
          matcher: const MdTaskListMatcher(),
          builder: (text, groups) =>
              _builder(text, groups, style, completedStyle, lineBuilder),
        );

  static InlineSpan _builder(
    String text,
    List<String?> groups,
    TextStyle style,
    TextStyle? completedStyle,
    LineBuilder lineBuilder,
  ) {
    final isChecked = !(groups[3]! == ' ');

    final indent = (groups[1]?.length ?? 0) ~/ 2;

    return TextSpan(children: [
      TextSpan(
        text: groups[0]!,
        style: style.copyWith(decoration: TextDecoration.none),
      ),
      WidgetSpan(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Gap(16.0 * indent),
            Padding(
              padding: const EdgeInsets.only(top: 2),
              child: Icon(
                isChecked ? Symbols.check : Symbols.circle,
                size: _maybeMultiply(style.fontSize, 1.2),
                color: isChecked
                    ? completedStyle?.decorationColor ?? style.color
                    : style.color,
              ),
            ),
            const Gap(8),
            Expanded(
              child: lineBuilder(
                groups[4]!,
                (isChecked ? completedStyle : null) ?? style,
              ),
            ),
          ],
        ),
      ),
    ]);
  }
}

double? _maybeMultiply(double? value1, double? value2) {
  if (value1 == null && value2 == null) return null;
  return (value1 ?? 1) * (value2 ?? 1);
}

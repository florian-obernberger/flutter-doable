import 'package:custom_text/custom_text.dart';
import 'package:flutter/material.dart';
import 'package:gap/gap.dart';
import 'package:flutter_material_design_icons/flutter_material_design_icons.dart';
import 'package:material_symbols_icons/symbols.dart';
import 'package:simple_icons/simple_icons.dart';

class MdCodeBlockMatcher extends TextMatcher {
  const MdCodeBlockMatcher() : super(r'```(\S*?)\n([\s\S]*?)```');
}

class MdCodeBlockDefinition extends SpanDefinition {
  MdCodeBlockDefinition({
    required TextStyle style,
    required Color background,
    void Function(String)? onLongPress,
  }) : super(
          matcher: const MdCodeBlockMatcher(),
          builder: (text, groups) => _builder(
            text,
            groups,
            style,
            background,
            onLongPress,
          ),
        );

  static InlineSpan _builder(
    String text,
    List<String?> groups,
    TextStyle style,
    Color background,
    void Function(String)? onLongPress,
  ) {
    return TextSpan(children: [
      WidgetSpan(
        child: GestureDetector(
          onLongPress:
              onLongPress == null ? null : () => onLongPress(groups[1]!),
          child: Container(
            decoration: BoxDecoration(
              color: background,
              borderRadius: BorderRadius.circular(8),
            ),
            width: double.infinity,
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 10),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Icon(
                        getIcon(groups[0]!),
                        color: style.color,
                        size: maybeMultiply(style.fontSize, 0.9),
                      ),
                      const Gap(4),
                      Text(groups[0]!, style: style),
                    ],
                  ),
                  Divider(color: style.color),
                  Text(groups[1]!.trim(), style: style),
                ],
              ),
            ),
          ),
        ),
        // baseline: TextBaseline.alphabetic,
        // alignment: PlaceholderAlignment.baseline,
      ),
    ]);
  }

  static IconData getIcon(String language) {
    switch (language.toLowerCase()) {
      case 'python' || 'py':
        return SimpleIcons.python;
      case 'go':
        return SimpleIcons.go;
      case 'javascript' || 'js':
        return SimpleIcons.javascript;
      case 'typescript' || 'ts':
        return SimpleIcons.typescript;
      case 'react' || ' jsx' || 'tsx':
        return SimpleIcons.react;
      case 'dart':
        return SimpleIcons.dart;
      case 'flutter':
        return SimpleIcons.flutter;
      case 'kotlin':
        return SimpleIcons.kotlin;
      case 'haskell':
        return SimpleIcons.haskell;
      case 'lua':
        return SimpleIcons.lua;
      case 'c':
        return SimpleIcons.c;
      case 'csharp':
        return SimpleIcons.csharp;
      case 'cpp' || 'cplusplus':
        return SimpleIcons.cplusplus;
      case 'rust':
        return SimpleIcons.rust;
      case 'swift':
        return SimpleIcons.swift;
      case 'elixir':
        return SimpleIcons.elixir;
      case 'julia':
        return SimpleIcons.julia;
      case 'bash':
        return SimpleIcons.gnubash;
      case 'php':
        return SimpleIcons.php;
      case 'r':
        return SimpleIcons.r;
      case 'ruby':
        return SimpleIcons.ruby;
      case 'perl':
        return SimpleIcons.perl;
      case 'java':
        return MdiIcons.languageJava;
      case 'css':
        return SimpleIcons.css3;
      case 'md':
        return SimpleIcons.markdown;
      case 'markdown' || 'md':
        return SimpleIcons.markdown;
      case 'latex':
        return SimpleIcons.latex;
      default:
        return Symbols.code;
    }
  }
}

double? maybeMultiply(double? value1, double? value2) {
  if (value1 == null && value2 == null) return null;
  return (value1 ?? 1) * (value2 ?? 1);
}

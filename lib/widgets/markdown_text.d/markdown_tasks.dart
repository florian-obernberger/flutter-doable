import 'package:custom_text/custom_text.dart';
import 'package:flutter/material.dart';
import 'package:material_symbols_icons/symbols.dart';

class MdTasksMatcher extends TextMatcher {
  const MdTasksMatcher() : super(r'(☐|✓)(.*?)');
}

class MdTasksDefinition extends SpanDefinition {
  MdTasksDefinition({required TextStyle style, TextStyle? completedStyle})
      : super(
          matcher: const MdTasksMatcher(),
          builder: (text, groups) =>
              _builder(text, groups, style, completedStyle),
        );

  static InlineSpan _builder(
    String text,
    List<String?> groups,
    TextStyle style,
    TextStyle? completedStyle,
  ) {
    final ndStyle = style.copyWith(decoration: TextDecoration.none);
    final isChecked = groups[0]! == '✓';

    return WidgetSpan(
      child: Icon(
        isChecked ? Symbols.check : Symbols.circle,
        size: maybeMultiply(style.fontSize, 1.1),
        color: isChecked
            ? completedStyle?.decorationColor ?? style.color
            : style.color,
      ),
      alignment: PlaceholderAlignment.top,
      style: ndStyle,
    );
  }
}

double? maybeMultiply(double? value1, double? value2) {
  if (value1 == null && value2 == null) return null;
  return (value1 ?? 1) * (value2 ?? 1);
}

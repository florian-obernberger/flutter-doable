import 'package:custom_text/custom_text.dart';
import 'package:flutter/material.dart';

import '/strings/strings.dart';

typedef LineBuilder = Widget Function(String line, TextStyle style);

class MdQuoteMatcher extends TextMatcher {
  const MdQuoteMatcher() : super(r'"(.*?)"');
}

class MdQuoteDefinition extends SpanDefinition {
  MdQuoteDefinition({
    required TextStyle style,
    required Strings strings,
    required LineBuilder lineBuilder,
  }) : super(
          matcher: const MdQuoteMatcher(),
          builder: (text, groups) => _builder(
            text,
            groups,
            style,
            strings,
            lineBuilder,
          ),
        );

  static InlineSpan _builder(
    String text,
    List<String?> groups,
    TextStyle style,
    Strings strings,
    LineBuilder lineBuilder,
  ) {
    return WidgetSpan(
      baseline: TextBaseline.alphabetic,
      alignment: PlaceholderAlignment.baseline,
      child: lineBuilder(
        '${strings.quoteDoubleLeft}${groups[0]!}${strings.quoteDoubleRight}',
        style,
      ),
    );
  }
}

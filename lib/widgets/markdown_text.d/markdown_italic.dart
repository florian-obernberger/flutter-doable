import 'package:custom_text/custom_text.dart';

class MdItalicMatcher extends TextMatcher {
  const MdItalicMatcher() : super(r'(\*|_)(.*?)(\*|_)');
}

class MdItalicDefinition extends SelectiveDefinition {
  const MdItalicDefinition({
    super.matchStyle,
    super.tapStyle,
    super.hoverStyle,
    super.onTap,
    super.onLongPress,
    super.mouseCursor,
  }) : super(
          matcher: const MdItalicMatcher(),
          labelSelector: _labelSelector,
          tapSelector: _tapSelector,
        );

  static String _labelSelector(List<String?> groups) => groups[1]!;
  static String _tapSelector(List<String?> groups) => groups[1]!;
}

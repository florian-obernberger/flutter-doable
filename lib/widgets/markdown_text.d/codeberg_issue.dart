import 'package:custom_text/custom_text.dart';

class CodebergIssueMatcher extends TextMatcher {
  const CodebergIssueMatcher() : super(r'#(\d+)');
}

class CodebergIssueDefinition extends SelectiveDefinition {
  const CodebergIssueDefinition({
    super.matchStyle,
    super.tapStyle,
    super.hoverStyle,
    super.onTap,
    super.onLongPress,
    super.mouseCursor,
  }) : super(
          matcher: const CodebergIssueMatcher(),
          labelSelector: _labelSelector,
          tapSelector: _tapSelector,
        );

  static String _labelSelector(List<String?> groups) => '#${groups[0]!}';
  static String _tapSelector(List<String?> groups) =>
      'https://codeberg.org/florian-obernberger/flutter-doable/issues/${groups[0]!}';
}

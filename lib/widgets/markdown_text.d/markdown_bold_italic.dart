import 'package:custom_text/custom_text.dart';

class MdBoldItalicMatcher extends TextMatcher {
  const MdBoldItalicMatcher()
      : super(r'(\*\*\*|_\*\*|\*\*_)(.*?)(\*\*\*|_\*\*|\*\*_)');
}

class MdBoldItalicDefinition extends SelectiveDefinition {
  const MdBoldItalicDefinition({
    super.matchStyle,
    super.tapStyle,
    super.hoverStyle,
    super.onTap,
    super.onLongPress,
    super.mouseCursor,
  }) : super(
          matcher: const MdBoldItalicMatcher(),
          labelSelector: _labelSelector,
          tapSelector: _tapSelector,
        );

  static String _labelSelector(List<String?> groups) => groups[1]!;
  static String _tapSelector(List<String?> groups) => groups[1]!;
}

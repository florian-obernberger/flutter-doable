import 'package:custom_text/custom_text.dart';

class MdBoldUnderlineMatcher extends TextMatcher {
  const MdBoldUnderlineMatcher()
      : super(r'(\*\*__|__\*\*)(.*?)(\*\*__|__\*\*)');
}

class MdBoldUnderlineDefinition extends SelectiveDefinition {
  const MdBoldUnderlineDefinition({
    super.matchStyle,
    super.tapStyle,
    super.hoverStyle,
    super.onTap,
    super.onLongPress,
    super.mouseCursor,
  }) : super(
          matcher: const MdBoldUnderlineMatcher(),
          labelSelector: _labelSelector,
          tapSelector: _tapSelector,
        );

  static String _labelSelector(List<String?> groups) => groups[1]!;
  static String _tapSelector(List<String?> groups) => groups[1]!;
}

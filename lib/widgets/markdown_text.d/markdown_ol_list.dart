import 'package:custom_text/custom_text.dart';
import 'package:flutter/material.dart';
import 'package:gap/gap.dart';

typedef LineBuilder = Widget Function(String line, TextStyle style);

class MdOlListMatcher extends TextMatcher {
  const MdOlListMatcher() : super(r'(\n|^)((  )*)([0-9]+\.) (.*)');
}

class MdOlListDefinition extends SpanDefinition {
  MdOlListDefinition({
    required TextStyle style,
    required LineBuilder lineBuilder,
  }) : super(
          matcher: const MdOlListMatcher(),
          builder: (text, groups) => _builder(text, groups, style, lineBuilder),
        );

  static InlineSpan _builder(
    String text,
    List<String?> groups,
    TextStyle style,
    LineBuilder lineBuilder,
  ) {
    final indent = (groups[1]?.length ?? 0) ~/ 2;

    return TextSpan(children: [
      TextSpan(
        text: groups[0]!,
        style: style.copyWith(decoration: TextDecoration.none),
      ),
      WidgetSpan(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Gap(16.0 * indent),
            Text(groups[3]!,
                style: style.copyWith(decoration: TextDecoration.none)),
            const Gap(8),
            Expanded(child: lineBuilder(groups[4]!, style)),
          ],
        ),
      ),
    ]);
  }
}

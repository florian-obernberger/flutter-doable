import 'package:custom_text/custom_text.dart';

class MdUnderlineMatcher extends TextMatcher {
  const MdUnderlineMatcher() : super(r'__(.*?)__');
}

class MdUnderlineDefinition extends SelectiveDefinition {
  const MdUnderlineDefinition({
    super.matchStyle,
    super.tapStyle,
    super.hoverStyle,
    super.onTap,
    super.onLongPress,
    super.mouseCursor,
  }) : super(
          matcher: const MdUnderlineMatcher(),
          labelSelector: _labelSelector,
          tapSelector: _tapSelector,
        );

  static String _labelSelector(List<String?> groups) => groups[0]!;
  static String _tapSelector(List<String?> groups) => groups[0]!;
}

import 'package:custom_text/custom_text.dart';

class MdRawLinkMatcher extends TextMatcher {
  const MdRawLinkMatcher() : super(r'<(.*?)>');
}

class MdRawLinkDefinition extends SelectiveDefinition {
  const MdRawLinkDefinition({
    super.matchStyle,
    super.tapStyle,
    super.hoverStyle,
    super.onTap,
    super.onLongPress,
    super.mouseCursor,
  }) : super(
          matcher: const MdRawLinkMatcher(),
          labelSelector: _labelSelector,
          tapSelector: _tapSelector,
        );

  static String _labelSelector(List<String?> groups) => groups[0]!;
  static String _tapSelector(List<String?> groups) => groups[0]!;
}

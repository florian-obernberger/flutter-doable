import 'package:custom_text/custom_text.dart';

class MdStrikeMatcher extends TextMatcher {
  const MdStrikeMatcher() : super(r'(~~)(.*?)(~~)');
}

class MdStrikeDefinition extends SelectiveDefinition {
  const MdStrikeDefinition({
    super.matchStyle,
    super.tapStyle,
    super.hoverStyle,
    super.onTap,
    super.onLongPress,
    super.mouseCursor,
  }) : super(
          matcher: const MdStrikeMatcher(),
          labelSelector: _labelSelector,
          tapSelector: _tapSelector,
        );

  static String _labelSelector(List<String?> groups) => groups[1]!;
  static String _tapSelector(List<String?> groups) => groups[1]!;
}

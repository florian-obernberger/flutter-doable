import 'package:custom_text/custom_text.dart';
import 'package:flutter/material.dart';

typedef LineBuilder = Widget Function(String line, TextStyle style);

class MdHrMatcher extends TextMatcher {
  const MdHrMatcher() : super(r'(\n|^)(---|___)(\n|$)');
}

class MdHrDefinition extends SpanDefinition {
  MdHrDefinition({required TextStyle style})
      : super(
          matcher: const MdHrMatcher(),
          builder: (text, groups) => _builder(text, groups, style),
        );

  static InlineSpan _builder(
    String text,
    List<String?> groups,
    TextStyle style,
  ) {
    return TextSpan(children: [
      WidgetSpan(
        style: style,
        child: const Divider(),
      ),
    ]);
  }
}

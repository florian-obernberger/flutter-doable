import 'package:custom_text/custom_text.dart';
import 'package:flutter/material.dart';
import 'package:material_symbols_icons/symbols.dart';

typedef LineBuilder = Widget Function(String line, TextStyle style);

class MdTaskTagMatcher extends TextMatcher {
  const MdTaskTagMatcher() : super(r'<::>(☐|✓)(.*?)</::>');
}

class MdTaskTagDefinition extends SpanDefinition {
  MdTaskTagDefinition({
    required TextStyle style,
    TextStyle? completedStyle,
    required LineBuilder lineBuilder,
  }) : super(
          matcher: const MdTaskTagMatcher(),
          builder: (text, groups) =>
              _builder(text, groups, style, completedStyle, lineBuilder),
        );

  static InlineSpan _builder(
    String text,
    List<String?> groups,
    TextStyle style,
    TextStyle? completedStyle,
    LineBuilder lineBuilder,
  ) {
    final ndStyle = style.copyWith(decoration: TextDecoration.none);
    final isChecked = groups[0]! == '✓';

    return TextSpan(children: <InlineSpan>[
      WidgetSpan(
        child: Icon(
          isChecked ? Symbols.check : Symbols.circle,
          size: maybeMultiply(style.fontSize, 1.1),
          color: isChecked
              ? completedStyle?.decorationColor ?? style.color
              : style.color,
        ),
        alignment: PlaceholderAlignment.top,
        style: ndStyle,
      ),
      WidgetSpan(
        baseline: TextBaseline.alphabetic,
        alignment: PlaceholderAlignment.baseline,
        child: lineBuilder(
          groups[1]!,
          (isChecked ? completedStyle : null) ?? style,
        ),
      ),
    ]);
  }
}

double? maybeMultiply(double? value1, double? value2) {
  if (value1 == null && value2 == null) return null;
  return (value1 ?? 1) * (value2 ?? 1);
}

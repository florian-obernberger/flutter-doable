import 'package:custom_text/custom_text.dart';

class MdBoldItalicUnderlineMatcher extends TextMatcher {
  const MdBoldItalicUnderlineMatcher()
      : super(
          r'(\*\*__\*|\*__\*\*|__\*\*\*|\*\*__\*|\*__\*\*|___\*\*|__\*\*_|___\*\*|_\*\*__|\*\*___)'
          r'(.*?)'
          r'(\*\*__\*|\*__\*\*|__\*\*\*|\*\*__\*|\*__\*\*|___\*\*|__\*\*_|___\*\*|_\*\*__|\*\*___)',
        );
}

class MdBoldItalicUnderlineDefinition extends SelectiveDefinition {
  const MdBoldItalicUnderlineDefinition({
    super.matchStyle,
    super.tapStyle,
    super.hoverStyle,
    super.onTap,
    super.onLongPress,
    super.mouseCursor,
  }) : super(
          matcher: const MdBoldItalicUnderlineMatcher(),
          labelSelector: _labelSelector,
          tapSelector: _tapSelector,
        );

  static String _labelSelector(List<String?> groups) => groups[1]!;
  static String _tapSelector(List<String?> groups) => groups[1]!;
}

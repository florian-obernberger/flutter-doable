import 'package:custom_text/custom_text.dart';

class MdLinkMatcher extends TextMatcher {
  const MdLinkMatcher() : super(r'\[(.+?)\]\((.*?)\)');
}

class MdLinkDefinition extends SelectiveDefinition {
  const MdLinkDefinition({
    super.matchStyle,
    super.tapStyle,
    super.hoverStyle,
    super.onTap,
    super.onLongPress,
    super.mouseCursor,
  }) : super(
          matcher: const MdLinkMatcher(),
          labelSelector: _labelSelector,
          tapSelector: _tapSelector,
        );

  static String _labelSelector(List<String?> groups) => groups[0]!;
  static String _tapSelector(List<String?> groups) => groups[1]!;
}

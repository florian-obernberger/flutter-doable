import 'package:custom_text/custom_text.dart';

class MdItalicUnderlineMatcher extends TextMatcher {
  const MdItalicUnderlineMatcher()
      : super(r'(\*__|__\*|___)(.*?)(\*__|__\*|___)');
}

class MdItalicUnderlineDefinition extends SelectiveDefinition {
  const MdItalicUnderlineDefinition({
    super.matchStyle,
    super.tapStyle,
    super.hoverStyle,
    super.onTap,
    super.onLongPress,
    super.mouseCursor,
  }) : super(
          matcher: const MdItalicUnderlineMatcher(),
          labelSelector: _labelSelector,
          tapSelector: _tapSelector,
        );

  static String _labelSelector(List<String?> groups) => groups[1]!;
  static String _tapSelector(List<String?> groups) => groups[1]!;
}

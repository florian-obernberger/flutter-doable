import 'package:custom_text/custom_text.dart';
import 'package:flutter/material.dart';

class MdInlineCodeMatcher extends TextMatcher {
  const MdInlineCodeMatcher() : super(r'(`)(.*?)(`)');
}

class MdInlineCodeDefinition extends SpanDefinition {
  MdInlineCodeDefinition({
    required TextStyle style,
    required Color background,
    int? maxLines,
    bool? softWrap,
    TextOverflow? overflow = TextOverflow.fade,
    void Function(String)? onLongPress,
  }) : super(
          matcher: const MdInlineCodeMatcher(),
          builder: (text, groups) => _builder(
            text,
            groups,
            style,
            background,
            maxLines,
            softWrap,
            overflow,
            onLongPress,
          ),
        );

  static InlineSpan _builder(
    String text,
    List<String?> groups,
    TextStyle style,
    Color background,
    int? maxLines,
    bool? softWrap,
    TextOverflow? overflow,
    void Function(String)? onLongPress,
  ) {
    return TextSpan(children: [
      WidgetSpan(
        child: GestureDetector(
          onLongPress:
              onLongPress == null ? null : () => onLongPress(groups[1]!),
          child: Container(
            decoration: BoxDecoration(
              color: background,
              borderRadius: BorderRadius.circular(4),
            ),
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 6),
              child: Text(
                groups[1]!,
                style: style,
                maxLines: maxLines,
                overflow: overflow,
                softWrap: softWrap,
              ),
            ),
          ),
        ),
        baseline: TextBaseline.alphabetic,
        alignment: PlaceholderAlignment.baseline,
      ),
    ]);
  }
}

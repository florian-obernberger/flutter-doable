import 'package:custom_text/custom_text.dart';

class MdHighlightMatcher extends TextMatcher {
  const MdHighlightMatcher() : super(r'(==)(.*?)(==)');
}

class MdHighlightDefinition extends SelectiveDefinition {
  const MdHighlightDefinition({
    super.matchStyle,
    super.tapStyle,
    super.hoverStyle,
    super.onTap,
    super.onLongPress,
    super.mouseCursor,
  }) : super(
          matcher: const MdHighlightMatcher(),
          labelSelector: _labelSelector,
          tapSelector: _tapSelector,
        );

  static String _labelSelector(List<String?> groups) => '\t${groups[1]!}\t';
  static String _tapSelector(List<String?> groups) => groups[1]!;
}

import 'package:custom_text/custom_text.dart';
import 'package:flutter/material.dart';
import 'package:gap/gap.dart';

typedef LineBuilder = Widget Function(String line, TextStyle style);

class MdUlListMatcher extends TextMatcher {
  const MdUlListMatcher() : super(r'(\n|^)((  )*)[-\+\*] (.*)');
}

class MdUlListDefinition extends SpanDefinition {
  MdUlListDefinition({
    required TextStyle style,
    required LineBuilder lineBuilder,
  }) : super(
          matcher: const MdUlListMatcher(),
          builder: (text, groups) => _builder(text, groups, style, lineBuilder),
        );

  static InlineSpan _builder(
    String text,
    List<String?> groups,
    TextStyle style,
    LineBuilder lineBuilder,
  ) {
    final indent = (groups[1]?.length ?? 0) ~/ 2;
    final bullet = switch (indent % 3) {
      0 => '•', // U+2022
      1 => '⁃', // U+2043
      2 => '◦', // U+25E6
      _ => '', // cannot happen because modulo 3
    };

    return TextSpan(children: [
      TextSpan(
        text: groups[0]!,
        style: style.copyWith(decoration: TextDecoration.none),
      ),
      WidgetSpan(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Gap(16.0 * indent),
            Text(bullet,
                style: style.copyWith(decoration: TextDecoration.none)),
            const Gap(8),
            Expanded(child: lineBuilder(groups[3]!, style)),
          ],
        ),
      ),
    ]);
  }
}

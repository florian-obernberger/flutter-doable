import 'package:custom_text/custom_text.dart';

class MdBoldMatcher extends TextMatcher {
  const MdBoldMatcher() : super(r'\*\*(.*?)\*\*');
}

class MdBoldDefinition extends SelectiveDefinition {
  const MdBoldDefinition({
    super.matchStyle,
    super.tapStyle,
    super.hoverStyle,
    super.onTap,
    super.onLongPress,
    super.mouseCursor,
  }) : super(
          matcher: const MdBoldMatcher(),
          labelSelector: _labelSelector,
          tapSelector: _tapSelector,
        );

  static String _labelSelector(List<String?> groups) => groups[0]!;
  static String _tapSelector(List<String?> groups) => groups[0]!;
}

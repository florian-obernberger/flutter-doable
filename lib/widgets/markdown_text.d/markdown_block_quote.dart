import 'package:custom_text/custom_text.dart';
import 'package:flutter/material.dart';

typedef LineBuilder = Widget Function(String line, TextStyle style);

class MdBlockQuoteMatcher extends TextMatcher {
  const MdBlockQuoteMatcher() : super(r'((?:^> .*?(\n|$))+)');
}

class MdBlockQuoteDefinition extends SpanDefinition {
  MdBlockQuoteDefinition({
    required TextStyle style,
    required LineBuilder lineBuilder,
  }) : super(
          matcher: const MdBlockQuoteMatcher(),
          builder: (text, groups) => _builder(text, groups, style, lineBuilder),
        );

  static InlineSpan _builder(
    String text,
    List<String?> groups,
    TextStyle style,
    LineBuilder lineBuilder,
  ) {
    final content = groups[0]!.replaceAll(RegExp(r'(^|\n)> '), '').trim();

    return WidgetSpan(
      style: style,
      child: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          border: BorderDirectional(
            start: BorderSide(color: style.color!, width: 4),
          ),
        ),
        child: Padding(
          padding: const EdgeInsetsDirectional.only(start: 8),
          child: lineBuilder(content, style),
        ),
      ),
      alignment: PlaceholderAlignment.middle,
      baseline: TextBaseline.alphabetic,
    );
  }
}

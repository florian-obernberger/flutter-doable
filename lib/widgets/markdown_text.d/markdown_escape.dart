import 'package:custom_text/custom_text.dart';

class MdEscapeMatcher extends TextMatcher {
  const MdEscapeMatcher() : super(r'\\(.)');
}

class MdEscapeDefinition extends SelectiveDefinition {
  const MdEscapeDefinition({
    super.matchStyle,
    super.tapStyle,
    super.hoverStyle,
    super.onTap,
    super.onLongPress,
    super.mouseCursor,
  }) : super(
          matcher: const MdEscapeMatcher(),
          labelSelector: _labelSelector,
          tapSelector: _tapSelector,
        );

  static String _labelSelector(List<String?> groups) => groups.first!;
  static String _tapSelector(List<String?> groups) => groups.first!;
}

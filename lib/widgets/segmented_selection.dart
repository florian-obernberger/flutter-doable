import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '/util/group_shape.dart';

import 'circle.dart';

class SegmentedItem<T> {
  const SegmentedItem({
    required this.value,
    required this.title,
    this.description,
    required this.icon,
  });

  final T value;
  final String title;
  final String? description;
  final IconData icon;

  static List<SegmentedItem<T>> fromValues<T>({
    required List<T> values,
    required String Function(T value) toTitle,
    String Function(T value)? toDescription,
    required IconData Function(T value) toIcon,
  }) {
    return <SegmentedItem<T>>[
      for (final value in values)
        SegmentedItem(
          value: value,
          title: toTitle(value),
          description: toDescription?.call(value),
          icon: toIcon(value),
        ),
    ];
  }
}

class SegmentedSelection<T> extends StatelessWidget {
  const SegmentedSelection({
    this.title,
    required this.items,
    required this.selected,
    required this.onSelect,
    super.key,
  }) : assert(items.length >= 2);

  final String? title;
  final List<SegmentedItem<T>> items;
  final T selected;
  final ValueChanged<T> onSelect;

  @override
  Widget build(BuildContext context) {
    final first = this.items.first;
    final last = this.items.last;
    final items = this.items.sublist(1, this.items.length - 1);

    final ThemeData(colorScheme: scheme, textTheme: text) = Theme.of(context);

    return Padding(
      padding: const EdgeInsets.all(12),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          if (title != null)
            Padding(
              padding: const EdgeInsets.fromLTRB(8, 4, 8, 8),
              child: Text(
                title!,
                style: text.bodyLarge?.copyWith(
                  color: scheme.onSurface,
                ),
              ),
            ),
          _buildCard(
            context,
            first,
            shape: _Shapes.startShape,
            margin: _Shapes.startPadding,
          ),
          for (final item in items)
            _buildCard(
              context,
              item,
              shape: _Shapes.middleShape,
              margin: _Shapes.middlePadding,
            ),
          _buildCard(
            context,
            last,
            shape: _Shapes.endShape,
            margin: _Shapes.endPadding,
          ),
        ],
      ),
    );
  }

  Widget _buildCard(
    BuildContext context,
    SegmentedItem<T> item, {
    required ShapeBorder shape,
    required EdgeInsetsGeometry margin,
  }) {
    final scheme = Theme.of(context).colorScheme;
    final text = Theme.of(context).textTheme;

    final selected = this.selected == item.value;

    final titleStyle = text.titleMedium?.copyWith(
      color: selected ? scheme.onPrimaryContainer : scheme.onSurface,
    );

    final subtitleStyle = text.bodyMedium?.copyWith(
      color: selected ? scheme.onPrimaryContainer : scheme.onSurfaceVariant,
    );

    final iconForeground = selected ? scheme.onPrimary : scheme.onSecondary;
    final iconBackground = selected ? scheme.primary : scheme.secondary;

    return Card.filled(
      clipBehavior: Clip.antiAliasWithSaveLayer,
      color: selected ? scheme.primaryContainer : scheme.surfaceContainer,
      shape: shape,
      margin: margin,
      child: InkWell(
        onTap: () {
          HapticFeedback.heavyImpact();
          if (!selected) onSelect(item.value);
        },
        child: ListTile(
          contentPadding: const EdgeInsets.fromLTRB(12, 8, 12, 8),
          title: Text(item.title),
          titleTextStyle: titleStyle,
          subtitle: item.description != null ? Text(item.description!) : null,
          subtitleTextStyle: subtitleStyle,
          leading: Circle(
            diameter: 40,
            color: iconBackground,
            child: Icon(
              color: iconForeground,
              fill: selected ? 1 : 0,
              weight: 600,
              item.icon,
            ),
          ),
        ),
      ),
    );
  }
}

class _Shapes {
  static const double rGroup = 8;
  static const double rSingle = 16;

  static const double mGroup = 2;
  static const double mSingle = 4;
  static const double mSides = 4;

  static const startPadding = GroupEdgeInsets(
    top: mSingle,
    bottom: mGroup,
    sides: mSides,
  );

  static const startShape = RoundedRectangleBorder(
    borderRadius: GroupBorderRadius(
      top: Radius.circular(rSingle),
      bottom: Radius.circular(rGroup),
    ),
  );

  static const middlePadding = GroupEdgeInsets(
    top: mGroup,
    bottom: mGroup,
    sides: mSides,
  );
  static const middleShape = RoundedRectangleBorder(
    borderRadius: GroupBorderRadius(
      top: Radius.circular(rGroup),
      bottom: Radius.circular(rGroup),
    ),
  );

  static const endPadding = GroupEdgeInsets(
    top: mGroup,
    bottom: mSingle,
    sides: mSides,
  );
  static const endShape = RoundedRectangleBorder(
    borderRadius: GroupBorderRadius(
      top: Radius.circular(rGroup),
      bottom: Radius.circular(rSingle),
    ),
  );
}

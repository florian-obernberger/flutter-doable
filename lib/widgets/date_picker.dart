import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:material_symbols_icons/symbols.dart';

import '/strings/strings.dart';
import '/data/todo.dart';
import '/util/animated_dialog.dart';

import 'date_time_picker.dart';
import 'todo_chips.dart';

Future<DateTime?> showAnimatedDatePicker({
  required BuildContext context,
  required DateTime initialDate,
  required DateTime firstDate,
  required DateTime lastDate,
  DateTime? currentDate,
  DatePickerEntryMode initialEntryMode = DatePickerEntryMode.calendar,
  SelectableDayPredicate? selectableDayPredicate,
  String? helpText,
  String? cancelText,
  String? confirmText,
  Locale? locale,
  bool useRootNavigator = true,
  RouteSettings? routeSettings,
  TextDirection? textDirection,
  TransitionBuilder? builder,
  DatePickerMode initialDatePickerMode = DatePickerMode.day,
  String? errorFormatText,
  String? errorInvalidText,
  String? fieldHintText,
  String? fieldLabelText,
  TextInputType? keyboardType,
  Offset? anchorPoint,
}) async {
  initialDate = DateUtils.dateOnly(initialDate);
  firstDate = DateUtils.dateOnly(firstDate);
  lastDate = DateUtils.dateOnly(lastDate);
  assert(
    !lastDate.isBefore(firstDate),
    'lastDate $lastDate must be on or after firstDate $firstDate.',
  );
  assert(
    !initialDate.isBefore(firstDate),
    'initialDate $initialDate must be on or after firstDate $firstDate.',
  );
  assert(
    !initialDate.isAfter(lastDate),
    'initialDate $initialDate must be on or before lastDate $lastDate.',
  );
  assert(
    selectableDayPredicate == null || selectableDayPredicate(initialDate),
    'Provided initialDate $initialDate must satisfy provided selectableDayPredicate.',
  );
  assert(debugCheckHasMaterialLocalizations(context));

  Widget dialog = DatePickerDialog(
    initialDate: initialDate,
    firstDate: firstDate,
    lastDate: lastDate,
    currentDate: currentDate,
    initialEntryMode: initialEntryMode,
    selectableDayPredicate: selectableDayPredicate,
    helpText: helpText,
    cancelText: cancelText,
    confirmText: confirmText,
    initialCalendarMode: initialDatePickerMode,
    errorFormatText: errorFormatText,
    errorInvalidText: errorInvalidText,
    fieldHintText: fieldHintText,
    fieldLabelText: fieldLabelText,
    keyboardType: keyboardType,
  );

  if (textDirection != null) {
    dialog = Directionality(
      textDirection: textDirection,
      child: dialog,
    );
  }

  if (locale != null) {
    dialog = Localizations.override(
      context: context,
      locale: locale,
      child: dialog,
    );
  }

  return showAnimatedDialog<DateTime>(
    context: context,
    animationType: DialogTransitionType.fadeScale,
    builder: (BuildContext context) {
      return builder == null ? dialog : builder(context, dialog);
    },
  );
}

typedef TodoDatePickerCallback = void Function(
    DateTime selectedDate, TimeOfDay? selectedTime);
typedef TodoDatePickerClearCallback = void Function();

class TodoDatePicker extends StatelessWidget {
  const TodoDatePicker({
    required this.todo,
    required this.onSelected,
    required this.onCleared,
    this.chipOnRight = false,
    this.showChip = true,
    this.giveHapticFeedback = true,
    this.buttonPadding,
    this.chipBackgroundColor,
    super.key,
  });

  final Todo todo;
  final TodoDatePickerCallback onSelected;
  final TodoDatePickerClearCallback onCleared;
  final bool chipOnRight;
  final bool showChip;
  final bool giveHapticFeedback;
  final EdgeInsets? buttonPadding;
  final Color? chipBackgroundColor;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final scheme = theme.colorScheme;

    final hasDate = todo.relevantDate != null;

    Widget? chip;

    if (hasDate) {
      chip = DueDateChip.outlined(
        todo.relevantDateTime!,
        onTap: () => selectDateTime(context),
        showTime: todo.relevantTime != null,
        onDelete: onCleared,
      );
    }

    return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        if (showChip && hasDate && !chipOnRight) chip!,
        GestureDetector(
          onLongPress: hasDate
              ? null
              : () {
                  HapticFeedback.mediumImpact();
                  onSelected(DateTime.now(), null);
                },
          child: IconButton(
            padding: buttonPadding,
            icon: Icon(
              Symbols.calendar_clock,
              weight: 500,
              color: scheme.onSurfaceVariant,
            ),
            selectedIcon: Icon(
              Symbols.calendar_clock,
              weight: 500,
              fill: 1,
              color: DueDateChip.colors(scheme, outlined: false).background,
            ),
            isSelected: todo.relevantDate != null,
            onPressed: () => selectDateTime(context),
          ),
        ),
        if (showChip && hasDate && chipOnRight) chip!,
      ],
    );
  }

  TimeOfDay defaultTimeOfDay(DateTime dateTime) {
    final currentHour = dateTime.hour;
    int nextHour = currentHour + 1;
    if (nextHour >= TimeOfDay.hoursPerDay) {
      nextHour -= TimeOfDay.hoursPerDay;
    }

    final (hour, minute) = switch (dateTime.minute) {
      < 30 => (currentHour, 30),
      _ => (nextHour, 0),
    };

    return TimeOfDay(hour: hour, minute: minute);
  }

  Future<void> selectDateTime(BuildContext context) async {
    final strings = Strings.of(context)!;
    final scheme = Theme.of(context).colorScheme;

    final now = DateTime.now();

    if (giveHapticFeedback) unawaited(HapticFeedback.mediumImpact());

    final result = await showAnimatedDateTimePicker(
      context: context,
      initialDate: todo.relevantDate ?? now,
      timeInitialTime: todo.relevantTime,
      timeDefaultTime: defaultTimeOfDay(now),
      firstDate: DateTime(now.year - 100),
      lastDate: DateTime(now.year + 100),
      builder: (context, child) => Theme(
        data: Theme.of(context).copyWith(
          splashFactory: NoSplash.splashFactory,
          colorScheme: scheme.copyWith(
            surface: ElevationOverlay.applySurfaceTint(
              scheme.surface,
              scheme.surfaceTint,
              3,
            ),
          ),
        ),
        child: child,
      ),
      cancelText: strings.cancel,
      confirmText: strings.ok,
      helpText: strings.dueDate,
    );

    if (result == null) return;
    onSelected(result.$1, result.$2);
  }
}

import 'package:flutter/material.dart';
import 'package:material_symbols_icons/symbols.dart';

import '/strings/strings.dart';

class SaveButton extends StatelessWidget {
  const SaveButton({
    this.onSave,
    this.showIcon = false,
    this.isFilled = true,
    this.label,
    super.key,
  });

  final VoidCallback? onSave;
  final bool showIcon;
  final bool isFilled;
  final String? label;

  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context)!;

    final label = Text(this.label ?? strings.save);

    return switch ((showIcon, isFilled)) {
      (true, true) => FilledButton.icon(
          onPressed: onSave,
          icon: const Icon(Symbols.save),
          label: label,
        ),
      (false, true) => FilledButton(
          onPressed: onSave,
          child: label,
        ),
      (true, false) => TextButton.icon(
          onPressed: onSave,
          icon: const Icon(Symbols.save),
          label: label,
        ),
      (false, false) => TextButton(
          onPressed: onSave,
          child: label,
        )
    };
  }
}

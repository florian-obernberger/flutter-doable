import 'package:flutter/material.dart';
import 'package:material_symbols_icons/symbols.dart';

import '/strings/strings.dart';
import '/state/settings.dart';
import '/util/formatters/format_date.dart';

abstract base class _DateTimeChipBase extends StatelessWidget {
  const _DateTimeChipBase({
    this.onClear,
    this.backgroundColor,
    this.foregroundColor,
    required this.selectTime,
    super.key,
  });

  final VoidCallback? onClear;
  final Color? backgroundColor;
  final Color? foregroundColor;
  final VoidCallback? selectTime;

  String? _locale(BuildContext context) =>
      Localizations.localeOf(context).countryCode;

  Widget _buildChip(BuildContext context, String formatted) {
    final scheme = Theme.of(context).colorScheme;
    final text = Theme.of(context).textTheme;

    return InputChip(
      visualDensity: VisualDensity.compact,
      backgroundColor: backgroundColor,
      elevation: 0,
      deleteIcon: const Icon(Symbols.clear, size: 18),
      deleteIconColor: scheme.onSurfaceVariant,
      onPressed: selectTime,
      onDeleted: onClear,
      label: Text(formatted),
      labelStyle: text.labelLarge?.copyWith(
        color: foregroundColor ?? scheme.onSurfaceVariant,
      ),
    );
  }
}

final class TimeChip extends _DateTimeChipBase {
  const TimeChip({
    required this.time,
    required super.selectTime,
    super.onClear,
    super.backgroundColor,
    super.foregroundColor,
    super.key,
  });

  final TimeOfDay time;

  @override
  Widget build(BuildContext context) => _buildChip(
        context,
        formatTimeOfDay(Settings.of(context).dateTime, time),
      );
}

final class DateTimeChip extends _DateTimeChipBase {
  const DateTimeChip({
    required this.dateTime,
    required super.selectTime,
    this.showTime = false,
    this.separator = ', ',
    this.useNames = true,
    super.onClear,
    super.backgroundColor,
    super.foregroundColor,
    super.key,
  });

  final DateTime dateTime;
  final bool showTime;
  final String separator;
  final bool useNames;

  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context)!;
    final locale = _locale(context);

    final prefs = Settings.of(context).dateTime;

    final formatted = showTime
        ? formatDateTime(
            prefs,
            strings,
            dateTime,
            locale: locale,
            separator: separator,
            useNames: useNames,
          )
        : formatDate(prefs, strings, dateTime, locale, useNames);

    return _buildChip(context, formatted);
  }
}

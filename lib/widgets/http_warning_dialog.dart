import 'package:flutter/material.dart';
import 'package:material_symbols_icons/symbols.dart';

import '/strings/strings.dart';

class HttpWarningDialog extends StatelessWidget {
  const HttpWarningDialog({super.key});

  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context)!;
    final scheme = Theme.of(context).colorScheme;

    return AlertDialog(
      icon: Icon(Symbols.warning_amber, color: scheme.error),
      title: Text(strings.httpWarning),
      content: Text(strings.httpWarningDescription),
      actions: <Widget>[
        TextButton(
          style: TextButton.styleFrom(foregroundColor: scheme.error),
          onPressed: () => Navigator.of(context).pop(false),
          child: Text(strings.cancel),
        ),
        FilledButton(
          style: FilledButton.styleFrom(
            backgroundColor: scheme.error,
            foregroundColor: scheme.onError,
          ),
          onPressed: () => Navigator.of(context).pop(true),
          child: Text(strings.doContinue),
        ),
      ],
    );
  }
}

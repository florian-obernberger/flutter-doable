// cSpell:disable

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gap/gap.dart';
import 'package:flutter_material_design_icons/flutter_material_design_icons.dart';
import 'package:material_symbols_icons/symbols.dart';
import 'package:simple_icons/simple_icons.dart';

import '/strings/strings.dart';
import '/state/settings.dart';
import '/util/launch.dart';
import '/classes/system_info.dart';
import '/util/looks_like_mastodon.dart';

class Link extends StatelessWidget {
  const Link({
    required String url,
    this.text,
    required this.baseStyle,
    this.maxLines,
    this.showIcon = false,
    this.textOverflow,
    this.isClickable = true,
    this.softWrap,
    super.key,
  }) : _url = url;

  final String _url;
  String get url => Uri.parse(_url).host.isEmpty ? 'https://$_url' : _url;
  final String? text;
  final TextStyle baseStyle;
  final int? maxLines;
  final bool showIcon;
  final TextOverflow? textOverflow;
  final bool isClickable;
  final bool? softWrap;

  @override
  Widget build(BuildContext context) {
    final scheme = Theme.of(context).colorScheme;

    final color = isClickable
        ? scheme.primary
        : Color.alphaBlend(
            scheme.primary.withOpacity(0.82),
            scheme.onSurfaceVariant,
          );

    final linkStyle = baseStyle.copyWith(
      color: color,
      decoration: TextDecoration.underline,
      decorationColor: color,
    );

    Widget child = Text(
      text ?? url,
      style: linkStyle,
      maxLines: maxLines,
      overflow: textOverflow ?? TextOverflow.fade,
      textAlign: TextAlign.left,
      softWrap: softWrap ?? false,
    );

    if (showIcon) {
      child = Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Icon(
            getIcon(),
            size: linkStyle.fontSize, // 18,
            color: color,
          ),
          const Gap(4),
          Flexible(child: child),
        ],
      );
    }

    return GestureDetector(
      onTap: isClickable
          ? () => launchUrl(
                url,
                inAppBrowser: Settings.of(context).general.inAppBrowser.value,
              )
          : null,
      onLongPress: isClickable ? () => onLongPress(context, url) : null,
      child: child,
    );
  }

  void onLongPress(BuildContext context, String content) {
    HapticFeedback.mediumImpact();
    content = content.replaceFirst('mailto:', '').replaceFirst('tel:', '');
    Clipboard.setData(ClipboardData(text: content));

    if (!SystemInfo.instance.hasClipboardPopup) {
      final strings = Strings.of(context)!;

      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          duration: const Duration(seconds: 2),
          dismissDirection: DismissDirection.vertical,
          margin: const EdgeInsets.fromLTRB(24, 8, 24, 18),
          content: Text(strings.copiedToClipboard),
        ),
      );
    }
  }

  IconData getIcon() => icon(url);

  static IconData icon(String url) {
    final host = Uri.parse(url).host;
    final base = host
        .split('.')
        .reversed
        .take(2)
        .toList()
        .reversed
        .join('.')
        .toLowerCase();

    return switch (host) {
      'drive.google.com' => SimpleIcons.googledrive,
      'docs.google.com' => SimpleIcons.googledocs,
      'sheets.google.com' => SimpleIcons.googlesheets,
      'slides.google.com' => SimpleIcons.googleslides,
      _ => switch (base) {
          'twitter.com' => SimpleIcons.twitter,
          'x.com' => SimpleIcons.x,
          'instagram.com' => SimpleIcons.instagram,
          'whatsapp.com' => SimpleIcons.whatsapp,
          'facebook.com' => SimpleIcons.facebook,
          'youtube.com' || 'youtu.be' => SimpleIcons.youtube,
          'twitch.com' => SimpleIcons.twitch,
          'spotify.com' => SimpleIcons.spotify,
          'snapchat.com' => SimpleIcons.snapchat,
          'sharepoint.com' || 'onedrive.com' => SimpleIcons.microsoftonedrive,
          'discord.gg' || 'discord.com' => SimpleIcons.discord,
          'tiktok.com' || 'tiktok.vm' => SimpleIcons.tiktok,
          'github.com' => SimpleIcons.github,
          'codeberg.org' => SimpleIcons.codeberg,
          'gitlab.com' => SimpleIcons.gitlab,
          'reddit.com' => SimpleIcons.reddit,
          'google.com' => SimpleIcons.google,
          'sr.ht' => SimpleIcons.sourcehut,
          // TODO: uncomment once it's available
          // 'bsky.app' || 'bsky.social' => SimpleIcons.bluesky,
          _ when looksLikeMastodonUrl(url) => SimpleIcons.mastodon,
          _ => MdiIcons.linkVariant,
        },
    };
  }
}

class Mail extends Link {
  const Mail({
    super.key,
    required String email,
    required super.baseStyle,
    super.maxLines,
    super.showIcon = true,
    super.textOverflow,
    super.isClickable,
  }) : super(
          url: 'mailto:$email',
          text: email,
        );

  @override
  String get url => _url;

  @override
  IconData getIcon() => Symbols.email;
}

class Tel extends Link {
  const Tel({
    super.key,
    required String number,
    required super.baseStyle,
    super.maxLines,
    super.showIcon = true,
    super.textOverflow,
    super.isClickable,
  }) : super(
          url: 'tel:$number',
          text: number,
        );

  @override
  String get url => _url;

  @override
  IconData getIcon() => Symbols.phone;
}

Link autoLink({
  required String link,
  required TextStyle baseStyle,
  int maxLines = 1,
  required bool showIcon,
  Key? key,
}) {
  if (link.startsWith('mailto:')) {
    return Mail(
      email: link.replaceFirst('mailto:', ''),
      baseStyle: baseStyle,
      showIcon: showIcon,
      maxLines: maxLines,
      key: key,
    );
  }

  if (link.startsWith('tel:')) {
    return Tel(
      number: link.replaceFirst('tel:', ''),
      baseStyle: baseStyle,
      showIcon: showIcon,
      maxLines: maxLines,
      key: key,
    );
  }

  return Link(
    url: link,
    baseStyle: baseStyle,
    showIcon: showIcon,
    maxLines: maxLines,
    key: key,
  );
}

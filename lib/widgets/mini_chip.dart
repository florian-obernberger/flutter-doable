import 'package:flutter/material.dart';
import 'package:gap/gap.dart';
import 'package:material_symbols_icons/symbols.dart';

enum _MiniChipType { text, widget, icon, avatar }

class MiniChip extends StatelessWidget {
  /// Sspace around the [MiniChip].
  final EdgeInsetsGeometry? margin;

  /// Constraints of the [MiniChip], excluding the [margin].
  final BoxConstraints? constraints;

  /// Textual content of the [MiniChip].
  final String? text;

  /// Leading icon / content for in [MiniChip.widget].
  final Widget? icon;

  /// Image avatar.
  final ImageProvider? avatar;

  /// Background color, can be [Colors.transparent].
  final Color? background;

  /// Foreground color.
  final Color? foreground;

  /// Border around the [MiniChip].
  final BorderSide? border;

  /// Weather the shape of the [MiniChip] should be fully
  /// rounded or not.
  final bool rounded;

  /// The color of the deletion icon visible when [onDelete]
  /// is not `null`.
  final Color? deleteIconColor;

  /// If this is non-`null` a delete icon will be shown;
  /// this is the callback for this icon.
  final VoidCallback? onDelete;

  /// On tap call back. If this is `null` this will **not**
  /// be disabled.
  final GestureTapCallback? onTap;

  final _MiniChipType _type;

  const MiniChip({
    required String this.text,
    this.onTap,
    this.margin,
    this.constraints,
    this.background,
    this.foreground,
    this.border,
    this.rounded = false,
    this.deleteIconColor,
    this.onDelete,
    super.key,
  })  : icon = null,
        avatar = null,
        _type = _MiniChipType.text;

  const MiniChip.widget({
    required Widget content,
    this.onTap,
    this.margin,
    this.constraints,
    this.background,
    this.foreground,
    this.border,
    this.rounded = false,
    this.deleteIconColor,
    this.onDelete,
    super.key,
  })  : text = null,
        avatar = null,
        icon = content,
        _type = _MiniChipType.widget;

  const MiniChip.icon({
    required String this.text,
    required Widget this.icon,
    this.onTap,
    this.margin,
    this.constraints,
    this.background,
    this.foreground,
    this.border,
    this.rounded = false,
    this.deleteIconColor,
    this.onDelete,
    super.key,
  })  : avatar = null,
        _type = _MiniChipType.icon;

  const MiniChip.avatar({
    required String this.text,
    required ImageProvider this.avatar,
    this.onTap,
    this.rounded = true,
    this.margin,
    this.constraints,
    this.background,
    this.foreground,
    this.border,
    this.deleteIconColor,
    this.onDelete,
    super.key,
  })  : icon = null,
        _type = _MiniChipType.avatar;

  @override
  Widget build(BuildContext context) {
    final ThemeData(
      colorScheme: scheme,
      textTheme: text,
      iconTheme: iconTheme,
      splashFactory: splashFactory
    ) = Theme.of(context);

    final background = this.background ?? scheme.tertiaryContainer;
    final foreground = this.foreground ?? scheme.onTertiaryContainer;

    final style = text.labelMedium?.copyWith(color: foreground);

    return Padding(
      padding: margin ?? EdgeInsets.zero,
      child: ConstrainedBox(
        constraints: (constraints ?? const BoxConstraints()).copyWith(
          minHeight: 24,
        ),
        child: ConstrainedBox(
          constraints: constraints ?? const BoxConstraints(),
          child: Material(
            clipBehavior: Clip.antiAlias,
            color: background,
            shape: RoundedRectangleBorder(
              borderRadius: rounded
                  ? const BorderRadius.all(Radius.circular(64))
                  : const BorderRadius.all(Radius.circular(8)),
              side: border ?? BorderSide.none,
            ),
            child: InkWell(
              onTap: onTap,
              child: Padding(
                padding: _type == _MiniChipType.avatar
                    ? const EdgeInsetsDirectional.fromSTEB(4, 4, 8, 4)
                    : const EdgeInsets.symmetric(vertical: 6, horizontal: 8),
                child: IconTheme(
                  data: iconTheme.copyWith(color: foreground, size: 16),
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Flexible(
                        child: switch (_type) {
                          _MiniChipType.text => Text(
                              this.text!,
                              style: style,
                              maxLines: 1,
                              softWrap: false,
                              overflow: TextOverflow.fade,
                            ),
                          _MiniChipType.widget => icon!,
                          _MiniChipType.icon => Row(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                icon!,
                                const Gap(4),
                                Text(
                                  this.text!,
                                  style: style,
                                  maxLines: 1,
                                  softWrap: false,
                                  overflow: TextOverflow.fade,
                                ),
                                const Gap(4),
                              ],
                            ),
                          _MiniChipType.avatar => Row(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                CircleAvatar(
                                    radius: 10, foregroundImage: avatar!),
                                const Gap(4),
                                Text(
                                  this.text!,
                                  style: style,
                                  maxLines: 1,
                                  softWrap: false,
                                  overflow: TextOverflow.fade,
                                ),
                                const Gap(4),
                              ],
                            )
                        },
                      ),
                      if (onDelete != null)
                        Semantics(
                          button: true,
                          container: true,
                          child: Tooltip(
                            message: MaterialLocalizations.of(context)
                                .deleteButtonTooltip,
                            child: InkWell(
                              radius: 14,
                              onTap: onDelete,
                              customBorder: const CircleBorder(),
                              splashFactory: splashFactory,
                              child: Icon(
                                Symbols.close,
                                color: deleteIconColor ?? foreground,
                              ),
                            ),
                          ),
                        )
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

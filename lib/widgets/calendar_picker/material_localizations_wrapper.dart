import 'package:flutter/material.dart';

import '/state/settings.dart';

class StartOfTheWeekMaterialLocalizations implements MaterialLocalizations {
  final Settings _settings;
  final MaterialLocalizations _wrapped;

  const StartOfTheWeekMaterialLocalizations._(this._settings, this._wrapped);

  static StartOfTheWeekMaterialLocalizations of(BuildContext context) {
    final wrapped = MaterialLocalizations.of(context);
    final settings = Settings.of(context);

    return StartOfTheWeekMaterialLocalizations._(settings, wrapped);
  }

  @override
  String aboutListTileTitle(String applicationName) =>
      _wrapped.aboutListTileTitle(applicationName);

  @override
  String get alertDialogLabel => _wrapped.alertDialogLabel;

  @override
  String get anteMeridiemAbbreviation => _wrapped.anteMeridiemAbbreviation;

  @override
  String get backButtonTooltip => _wrapped.backButtonTooltip;

  @override
  String get bottomSheetLabel => _wrapped.bottomSheetLabel;

  @override
  String get calendarModeButtonLabel => _wrapped.calendarModeButtonLabel;

  @override
  String get cancelButtonLabel => _wrapped.cancelButtonLabel;

  @override
  String get closeButtonLabel => _wrapped.closeButtonLabel;

  @override
  String get closeButtonTooltip => _wrapped.closeButtonTooltip;

  @override
  String get clearButtonTooltip => _wrapped.clearButtonTooltip;

  @override
  String get collapsedHint => _wrapped.collapsedHint;

  @override
  String get collapsedIconTapHint => _wrapped.collapsedIconTapHint;

  @override
  String get continueButtonLabel => _wrapped.continueButtonLabel;

  @override
  String get copyButtonLabel => _wrapped.copyButtonLabel;

  @override
  String get currentDateLabel => _wrapped.currentDateLabel;

  @override
  String get cutButtonLabel => _wrapped.cutButtonLabel;

  @override
  String get dateHelpText => _wrapped.dateHelpText;

  @override
  String get dateInputLabel => _wrapped.dateInputLabel;

  @override
  String get dateOutOfRangeLabel => _wrapped.dateOutOfRangeLabel;

  @override
  String get datePickerHelpText => _wrapped.datePickerHelpText;

  @override
  String dateRangeEndDateSemanticLabel(String formattedDate) =>
      _wrapped.dateRangeEndDateSemanticLabel(formattedDate);

  @override
  String get dateRangeEndLabel => _wrapped.dateRangeEndLabel;

  @override
  String get dateRangePickerHelpText => _wrapped.dateRangePickerHelpText;

  @override
  String dateRangeStartDateSemanticLabel(String formattedDate) =>
      _wrapped.dateRangeStartDateSemanticLabel(formattedDate);

  @override
  String get dateRangeStartLabel => _wrapped.dateRangeStartLabel;

  @override
  String get dateSeparator => _wrapped.dateSeparator;

  @override
  String get deleteButtonTooltip => _wrapped.deleteButtonTooltip;

  @override
  String get dialModeButtonLabel => _wrapped.dialModeButtonLabel;

  @override
  String get dialogLabel => _wrapped.dialogLabel;

  @override
  String get drawerLabel => _wrapped.drawerLabel;

  @override
  String get expandedHint => _wrapped.expandedHint;

  @override
  String get expandedIconTapHint => _wrapped.expandedIconTapHint;

  @override
  String get expansionTileCollapsedHint => _wrapped.expansionTileCollapsedHint;

  @override
  String get expansionTileCollapsedTapHint =>
      _wrapped.expansionTileCollapsedTapHint;

  @override
  String get expansionTileExpandedHint => _wrapped.expansionTileExpandedHint;

  @override
  String get expansionTileExpandedTapHint =>
      _wrapped.expansionTileExpandedTapHint;

  @override
  int get firstDayOfWeekIndex =>
      _settings.general.startOfTheWeek.value?.index ??
      _wrapped.firstDayOfWeekIndex;

  @override
  String get firstPageTooltip => _wrapped.firstPageTooltip;

  @override
  String formatCompactDate(DateTime date) => _wrapped.formatCompactDate(date);

  @override
  String formatDecimal(int number) => _wrapped.formatDecimal(number);

  @override
  String formatFullDate(DateTime date) => _wrapped.formatFullDate(date);

  @override
  String formatHour(
    TimeOfDay timeOfDay, {
    bool alwaysUse24HourFormat = false,
  }) =>
      _wrapped.formatHour(
        timeOfDay,
        alwaysUse24HourFormat: alwaysUse24HourFormat,
      );

  @override
  String formatMediumDate(DateTime date) => _wrapped.formatMediumDate(date);

  @override
  String formatMinute(TimeOfDay timeOfDay) => _wrapped.formatMinute(timeOfDay);

  @override
  String formatMonthYear(DateTime date) => _wrapped.formatMonthYear(date);

  @override
  String formatShortDate(DateTime date) => _wrapped.formatShortDate(date);

  @override
  String formatShortMonthDay(DateTime date) =>
      _wrapped.formatShortMonthDay(date);

  @override
  String formatTimeOfDay(
    TimeOfDay timeOfDay, {
    bool alwaysUse24HourFormat = false,
  }) =>
      _wrapped.formatTimeOfDay(
        timeOfDay,
        alwaysUse24HourFormat: alwaysUse24HourFormat,
      );

  @override
  String formatYear(DateTime date) => _wrapped.formatYear(date);

  @override
  String get hideAccountsLabel => _wrapped.hideAccountsLabel;

  @override
  String get inputDateModeButtonLabel => _wrapped.inputDateModeButtonLabel;

  @override
  String get inputTimeModeButtonLabel => _wrapped.inputTimeModeButtonLabel;

  @override
  String get invalidDateFormatLabel => _wrapped.invalidDateFormatLabel;

  @override
  String get invalidDateRangeLabel => _wrapped.invalidDateRangeLabel;

  @override
  String get invalidTimeLabel => _wrapped.invalidTimeLabel;

  @override
  String get keyboardKeyAlt => _wrapped.keyboardKeyAlt;

  @override
  String get keyboardKeyAltGraph => _wrapped.keyboardKeyAltGraph;

  @override
  String get keyboardKeyBackspace => _wrapped.keyboardKeyBackspace;

  @override
  String get keyboardKeyCapsLock => _wrapped.keyboardKeyCapsLock;

  @override
  String get keyboardKeyChannelDown => _wrapped.keyboardKeyChannelDown;

  @override
  String get keyboardKeyChannelUp => _wrapped.keyboardKeyChannelUp;

  @override
  String get keyboardKeyControl => _wrapped.keyboardKeyControl;

  @override
  String get keyboardKeyDelete => _wrapped.keyboardKeyDelete;

  @override
  String get keyboardKeyEject => _wrapped.keyboardKeyEject;

  @override
  String get keyboardKeyEnd => _wrapped.keyboardKeyEnd;

  @override
  String get keyboardKeyEscape => _wrapped.keyboardKeyEscape;

  @override
  String get keyboardKeyFn => _wrapped.keyboardKeyFn;

  @override
  String get keyboardKeyHome => _wrapped.keyboardKeyHome;

  @override
  String get keyboardKeyInsert => _wrapped.keyboardKeyInsert;

  @override
  String get keyboardKeyMeta => _wrapped.keyboardKeyMeta;

  @override
  String get keyboardKeyMetaMacOs => _wrapped.keyboardKeyMetaMacOs;

  @override
  String get keyboardKeyMetaWindows => _wrapped.keyboardKeyMetaWindows;

  @override
  String get keyboardKeyNumLock => _wrapped.keyboardKeyNumLock;

  @override
  String get keyboardKeyNumpad0 => _wrapped.keyboardKeyNumpad0;

  @override
  String get keyboardKeyNumpad1 => _wrapped.keyboardKeyNumpad1;

  @override
  String get keyboardKeyNumpad2 => _wrapped.keyboardKeyNumpad2;

  @override
  String get keyboardKeyNumpad3 => _wrapped.keyboardKeyNumpad3;

  @override
  String get keyboardKeyNumpad4 => _wrapped.keyboardKeyNumpad4;

  @override
  String get keyboardKeyNumpad5 => _wrapped.keyboardKeyNumpad5;

  @override
  String get keyboardKeyNumpad6 => _wrapped.keyboardKeyNumpad6;

  @override
  String get keyboardKeyNumpad7 => _wrapped.keyboardKeyNumpad7;

  @override
  String get keyboardKeyNumpad8 => _wrapped.keyboardKeyNumpad8;

  @override
  String get keyboardKeyNumpad9 => _wrapped.keyboardKeyNumpad9;

  @override
  String get keyboardKeyNumpadAdd => _wrapped.keyboardKeyNumpadAdd;

  @override
  String get keyboardKeyNumpadComma => _wrapped.keyboardKeyNumpadComma;

  @override
  String get keyboardKeyNumpadDecimal => _wrapped.keyboardKeyNumpadDecimal;

  @override
  String get keyboardKeyNumpadDivide => _wrapped.keyboardKeyNumpadDivide;

  @override
  String get keyboardKeyNumpadEnter => _wrapped.keyboardKeyNumpadEnter;

  @override
  String get keyboardKeyNumpadEqual => _wrapped.keyboardKeyNumpadEqual;

  @override
  String get keyboardKeyNumpadMultiply => _wrapped.keyboardKeyNumpadMultiply;

  @override
  String get keyboardKeyNumpadParenLeft => _wrapped.keyboardKeyNumpadParenLeft;

  @override
  String get keyboardKeyNumpadParenRight =>
      _wrapped.keyboardKeyNumpadParenRight;

  @override
  String get keyboardKeyNumpadSubtract => _wrapped.keyboardKeyNumpadSubtract;

  @override
  String get keyboardKeyPageDown => _wrapped.keyboardKeyPageDown;

  @override
  String get keyboardKeyPageUp => _wrapped.keyboardKeyPageUp;

  @override
  String get keyboardKeyPower => _wrapped.keyboardKeyPower;

  @override
  String get keyboardKeyPowerOff => _wrapped.keyboardKeyPowerOff;

  @override
  String get keyboardKeyPrintScreen => _wrapped.keyboardKeyPrintScreen;

  @override
  String get keyboardKeyScrollLock => _wrapped.keyboardKeyScrollLock;

  @override
  String get keyboardKeySelect => _wrapped.keyboardKeySelect;

  @override
  String get keyboardKeyShift => _wrapped.keyboardKeyShift;

  @override
  String get keyboardKeySpace => _wrapped.keyboardKeySpace;

  @override
  String get lastPageTooltip => _wrapped.lastPageTooltip;

  @override
  String licensesPackageDetailText(int licenseCount) =>
      _wrapped.licensesPackageDetailText(licenseCount);

  @override
  String get licensesPageTitle => _wrapped.licensesPageTitle;

  @override
  String get lookUpButtonLabel => _wrapped.lookUpButtonLabel;

  @override
  String get menuBarMenuLabel => _wrapped.menuBarMenuLabel;

  @override
  String get menuDismissLabel => _wrapped.menuDismissLabel;

  @override
  String get modalBarrierDismissLabel => _wrapped.modalBarrierDismissLabel;

  @override
  String get moreButtonTooltip => _wrapped.moreButtonTooltip;

  @override
  List<String> get narrowWeekdays => _wrapped.narrowWeekdays;

  @override
  String get nextMonthTooltip => _wrapped.nextMonthTooltip;

  @override
  String get nextPageTooltip => _wrapped.nextPageTooltip;

  @override
  String get okButtonLabel => _wrapped.okButtonLabel;

  @override
  String get openAppDrawerTooltip => _wrapped.openAppDrawerTooltip;

  @override
  String pageRowsInfoTitle(
    int firstRow,
    int lastRow,
    int rowCount,
    bool rowCountIsApproximate,
  ) =>
      _wrapped.pageRowsInfoTitle(
        firstRow,
        lastRow,
        rowCount,
        rowCountIsApproximate,
      );

  @override
  DateTime? parseCompactDate(String? inputString) =>
      _wrapped.parseCompactDate(inputString);

  @override
  String get pasteButtonLabel => _wrapped.pasteButtonLabel;

  @override
  String get popupMenuLabel => _wrapped.popupMenuLabel;

  @override
  String get postMeridiemAbbreviation => _wrapped.postMeridiemAbbreviation;

  @override
  String get previousMonthTooltip => _wrapped.previousMonthTooltip;

  @override
  String get previousPageTooltip => _wrapped.previousPageTooltip;

  @override
  String get refreshIndicatorSemanticLabel =>
      _wrapped.refreshIndicatorSemanticLabel;

  @override
  String remainingTextFieldCharacterCount(int remaining) =>
      _wrapped.remainingTextFieldCharacterCount(remaining);

  @Deprecated(
    'Use the reorderItemToStart from WidgetsLocalizations instead. '
    'This feature was deprecated after v3.10.0-2.0.pre.',
  )
  @override
  String get reorderItemDown => _wrapped.reorderItemDown;

  @Deprecated(
    'Use the reorderItemToEnd from WidgetsLocalizations instead. '
    'This feature was deprecated after v3.10.0-2.0.pre.',
  )
  @override
  String get reorderItemLeft => _wrapped.reorderItemLeft;

  @Deprecated(
    'Use the reorderItemUp from WidgetsLocalizations instead. '
    'This feature was deprecated after v3.10.0-2.0.pre.',
  )
  @override
  String get reorderItemRight => _wrapped.reorderItemRight;

  @Deprecated(
    'Use the reorderItemDown from WidgetsLocalizations instead. '
    'This feature was deprecated after v3.10.0-2.0.pre.',
  )
  @override
  String get reorderItemToEnd => _wrapped.reorderItemToEnd;

  @Deprecated(
    'Use the reorderItemLeft from WidgetsLocalizations instead. '
    'This feature was deprecated after v3.10.0-2.0.pre.',
  )
  @override
  String get reorderItemToStart => _wrapped.reorderItemToStart;

  @Deprecated(
    'Use the reorderItemRight from WidgetsLocalizations instead. '
    'This feature was deprecated after v3.10.0-2.0.pre.',
  )
  @override
  String get reorderItemUp => _wrapped.reorderItemUp;

  @override
  String get rowsPerPageTitle => _wrapped.rowsPerPageTitle;

  @override
  String get saveButtonLabel => _wrapped.saveButtonLabel;

  @override
  String get scanTextButtonLabel => _wrapped.scanTextButtonLabel;

  @override
  String get scrimLabel => _wrapped.scrimLabel;

  @override
  String scrimOnTapHint(String modalRouteContentName) =>
      _wrapped.scrimOnTapHint(modalRouteContentName);

  @override
  ScriptCategory get scriptCategory => _wrapped.scriptCategory;

  @override
  String get searchFieldLabel => _wrapped.searchFieldLabel;

  @override
  String get searchWebButtonLabel => _wrapped.searchWebButtonLabel;

  @override
  String get selectAllButtonLabel => _wrapped.selectAllButtonLabel;

  @override
  String get selectYearSemanticsLabel => _wrapped.selectYearSemanticsLabel;

  @override
  String get selectedDateLabel => _wrapped.selectedDateLabel;

  @override
  String selectedRowCountTitle(int selectedRowCount) =>
      _wrapped.selectedRowCountTitle(selectedRowCount);

  @override
  String get shareButtonLabel => _wrapped.shareButtonLabel;

  @override
  String get showAccountsLabel => _wrapped.showAccountsLabel;

  @override
  String get showMenuTooltip => _wrapped.showMenuTooltip;

  @override
  String get signedInLabel => _wrapped.signedInLabel;

  @override
  String tabLabel({required int tabIndex, required int tabCount}) =>
      _wrapped.tabLabel(tabIndex: tabIndex, tabCount: tabCount);

  @override
  TimeOfDayFormat timeOfDayFormat({bool alwaysUse24HourFormat = false}) =>
      _wrapped.timeOfDayFormat(alwaysUse24HourFormat: alwaysUse24HourFormat);

  @override
  String get timePickerDialHelpText => _wrapped.timePickerDialHelpText;

  @override
  String get timePickerHourLabel => _wrapped.timePickerHourLabel;

  @override
  String get timePickerHourModeAnnouncement =>
      _wrapped.timePickerHourModeAnnouncement;

  @override
  String get timePickerInputHelpText => _wrapped.timePickerInputHelpText;

  @override
  String get timePickerMinuteLabel => _wrapped.timePickerMinuteLabel;

  @override
  String get timePickerMinuteModeAnnouncement =>
      _wrapped.timePickerMinuteModeAnnouncement;

  @override
  String get unspecifiedDate => _wrapped.unspecifiedDate;

  @override
  String get unspecifiedDateRange => _wrapped.unspecifiedDateRange;

  @override
  String get viewLicensesButtonLabel => _wrapped.viewLicensesButtonLabel;
}

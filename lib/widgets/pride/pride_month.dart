enum PrideMonth {
  queer(DateTime.june),
  disability(DateTime.july);

  const PrideMonth(this.month);

  final int month;

  static PrideMonth? get currentMonth => values
      .cast<PrideMonth?>()
      .firstWhere((month) => month!.isMonth, orElse: () => null);

  bool get isMonth => DateTime.now().month == month;
}

import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '/resources/resources.dart';
import '/state/settings.dart';

import 'pride_month.dart';

class PrideBanner extends StatelessWidget {
  final PrideMonth month;
  final EdgeInsetsGeometry? padding;

  const PrideBanner({required this.month, this.padding, super.key});

  static PrideBanner? currentOf(
    BuildContext context, {
    EdgeInsetsGeometry? padding,
  }) {
    final settings = Settings.of(context);
    if (!settings.accessability.enablePrideFlags.value) return null;

    for (final month in PrideMonth.values) {
      if (month.isMonth) return PrideBanner(month: month, padding: padding);
    }

    final constant = settings.accessability.constantPrideFlag.value;
    if (constant != null) return PrideBanner(month: constant, padding: padding);

    return null;
  }

  @override
  Widget build(BuildContext context) {
    final scheme = Theme.of(context).colorScheme;

    return Padding(
      padding: padding ?? EdgeInsets.zero,
      child: Stack(
        children: <Widget>[
          SvgPicture.asset(
            switch (month) {
              PrideMonth.queer => Pride.prideBanner,
              PrideMonth.disability => Pride.disabilityPrideBanner,
            },
            fit: BoxFit.fill,
            height: double.infinity,
          ),
          Container(
            width: double.infinity,
            height: double.infinity,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                stops: const [0.25, 0.85, 1],
                transform: const GradientRotation(0.5 * pi),
                colors: [
                  scheme.surfaceDim.withOpacity(0.5),
                  scheme.surfaceDim.withOpacity(0.8),
                  scheme.surfaceDim.withOpacity(1),
                ],
              ),
              backgroundBlendMode: switch (scheme.brightness) {
                Brightness.light => BlendMode.lighten,
                Brightness.dark => BlendMode.darken,
              },
            ),
          ),
        ],
      ),
    );
  }
}

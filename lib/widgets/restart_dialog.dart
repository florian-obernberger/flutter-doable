import 'package:flutter/material.dart';
import 'package:restart_app/restart_app.dart';

import '/strings/strings.dart';

class RestartAppDialog extends StatelessWidget {
  const RestartAppDialog({super.key});

  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context)!;

    return AlertDialog(
      title: Text(strings.restartRequired),
      content: Text(strings.restartRequiredForSetting),
      actions: [
        TextButton(
          onPressed: Navigator.of(context).pop,
          child: Text(strings.later),
        ),
        TextButton(
          onPressed: Restart.restartApp,
          child: Text(strings.doContinue),
        ),
      ],
    );
  }
}

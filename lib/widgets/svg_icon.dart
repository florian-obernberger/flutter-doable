import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SvgIcon extends StatelessWidget {
  const SvgIcon({
    required this.icon,
    this.size,
    this.color,
    this.matchTextDirection = false,
    super.key,
  });

  final String icon;
  final double? size;
  final Color? color;
  final bool matchTextDirection;

  @override
  Widget build(BuildContext context) {
    IconThemeData theme = IconTheme.of(context);
    final scheme = Theme.of(context).colorScheme;

    return SvgPicture.asset(
      icon,
      width: size ?? theme.size,
      height: size ?? theme.size,
      colorFilter: ColorFilter.mode(
        color ?? theme.color ?? scheme.onSurface,
        BlendMode.srcIn,
      ),
      matchTextDirection: matchTextDirection,
    );
  }
}

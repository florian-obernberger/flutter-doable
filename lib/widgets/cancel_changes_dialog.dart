import 'package:flutter/material.dart';

import '/strings/strings.dart';

class CancelChangesDialog extends StatelessWidget {
  final String title, description;

  const CancelChangesDialog({
    required this.title,
    required this.description,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context)!;

    return AlertDialog(
      title: Text(title),
      content: Text(description),
      actions: [
        TextButton(
          onPressed: () => Navigator.of(context).pop(true),
          child: Text(strings.discard),
        ),
        TextButton(
          onPressed: Navigator.of(context).pop,
          child: Text(strings.keepEditing),
        ),
      ],
    );
  }
}

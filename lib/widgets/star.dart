import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:like_button/like_button.dart';
import 'package:material_symbols_icons/symbols.dart';

import '/strings/strings.dart';
import '/state/settings.dart';

const _harmonized = false;

BubblesColor bubblesColor(
  Brightness brightness, {
  bool harmonized = _harmonized,
}) {
  if (brightness == Brightness.light) {
    if (harmonized) {
      return const BubblesColor(
        dotPrimaryColor: Color(0xFF845400),
        dotSecondaryColor: Color(0xFF944b00),
        dotThirdColor: Color(0xFFa73a00),
        dotLastColor: Color(0xFFad3200),
      );
    } else {
      return const BubblesColor(
        dotPrimaryColor: Color(0xFF785900),
        dotSecondaryColor: Color(0xFF8b5000),
        dotThirdColor: Color(0xFFb02f00),
        dotLastColor: Color(0xFFbb1614),
      );
    }
  }

  if (harmonized) {
    return const BubblesColor(
      dotPrimaryColor: Color(0xFFffb95b),
      dotSecondaryColor: Color(0xFFffb783),
      dotThirdColor: Color(0xFFffb599),
      dotLastColor: Color(0xFFffb59e),
    );
  } else {
    return const BubblesColor(
      dotPrimaryColor: Color(0xFFfabd00),
      dotSecondaryColor: Color(0xFFffb870),
      dotThirdColor: Color(0xFFffb5a0),
      dotLastColor: Color(0xFFffb4a9),
    );
  }
}

Color starColor(Brightness brightness, {bool harmonized = _harmonized}) =>
    bubblesColor(brightness, harmonized: harmonized).dotPrimaryColor;

CircleColor circleColor(
  Brightness brightness, {
  bool harmonized = _harmonized,
}) {
  final bubbles = bubblesColor(brightness, harmonized: harmonized);
  return CircleColor(
    start: bubbles.dotThirdColor!,
    end: bubbles.dotPrimaryColor,
  );
}

class StarButton extends StatelessWidget {
  const StarButton({
    required this.isStarred,
    required this.onTap,
    this.iconSize,
    this.unselectedColor,
    this.buttonSize,
    this.giveHapticFeedback = true,
    this.padding,
    super.key,
  });

  final double? iconSize;
  final double? buttonSize;
  final Color? unselectedColor;
  final bool isStarred;
  final bool giveHapticFeedback;
  final LikeButtonTapCallback? onTap;
  final EdgeInsets? padding;

  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context)!;
    final scheme = Theme.of(context).colorScheme;

    final size = buttonSize ?? Theme.of(context).iconTheme.size ?? 24;
    // ignore: avoid_positional_boolean_parameters
    Widget iconBuilder(bool isLiked) => Icon(
          Symbols.grade,
          fill: isLiked ? 1 : 0,
          color: isLiked
              ? starColor(scheme.brightness)
              : unselectedColor ?? scheme.outline,
          size: iconSize,
        );

    // ignore: avoid_positional_boolean_parameters
    final Future<bool?> Function(bool isStarred)? onTap = this.onTap != null
        ? (isStarred) {
            if (giveHapticFeedback && !isStarred) {
              HapticFeedback.mediumImpact();
            }

            return this.onTap!.call(isStarred);
          }
        : null;

    return ValueListenableBuilder(
      valueListenable: Settings.of(context).accessability.reduceMotion,
      builder: (context, reduceMotion, _) {
        if (reduceMotion == true) {
          return IconButton(
            onPressed: onTap == null ? null : () => onTap(isStarred),
            icon: iconBuilder(isStarred),
          );
        }

        return Tooltip(
          message: strings.markAsStarred,
          child: LikeButton(
            size: size,
            padding: padding,
            isLiked: isStarred,
            bubblesColor: bubblesColor(scheme.brightness),
            circleColor: circleColor(scheme.brightness),
            likeBuilder: iconBuilder,
            onTap: onTap,
          ),
        );
      },
    );
  }
}

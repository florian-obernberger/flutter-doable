import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:material_symbols_icons/symbols.dart';

class BackButton extends StatelessWidget {
  const BackButton({this.onBack, this.color, this.tooltip, super.key});

  final VoidCallback? onBack;
  final Color? color;
  final String? tooltip;

  @override
  Widget build(BuildContext context) {
    final scheme = Theme.of(context).colorScheme;
    final mStrings = MaterialLocalizations.of(context);

    return IconButton(
      icon: const Icon(Symbols.arrow_back),
      onPressed: onBack ?? context.pop,
      color: color ?? scheme.onSurface,
      tooltip: tooltip ?? mStrings.backButtonTooltip,
    );
  }
}

import 'dart:io';
import 'dart:math' as math;

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:share_plus/share_plus.dart';

import '/strings/strings.dart';
import '/types/markdown_edit_action.dart';
import '/util/extensions/iterable.dart';

extension ReplaceText on EditableTextState {
  /// Paste the given [text].
  void replaceText(SelectionChangedCause cause, String text) {
    if (widget.readOnly) {
      return;
    }
    final TextSelection selection = textEditingValue.selection;
    if (!selection.isValid) {
      return;
    }

    // After the paste, the cursor should be collapsed and located after the
    // pasted content.
    final int lastSelectionIndex =
        math.max(selection.baseOffset, selection.extentOffset);
    final TextEditingValue collapsedTextEditingValue =
        textEditingValue.copyWith(
      selection: TextSelection.collapsed(offset: lastSelectionIndex),
    );

    userUpdateTextEditingValue(
      collapsedTextEditingValue.replaced(selection, text),
      cause,
    );
    if (cause == SelectionChangedCause.toolbar) {
      // Schedule a call to bringIntoView() after renderEditable updates.
      SchedulerBinding.instance.addPostFrameCallback((_) {
        if (mounted) {
          bringIntoView(textEditingValue.selection.extent);
        }
      });
      hideToolbar();
    }
  }
}

class MarkdownContextMenuButtonItem extends ContextMenuButtonItem {
  const MarkdownContextMenuButtonItem({
    required super.onPressed,
    required this.action,
    super.label,
  });

  final MarkdownEditAction action;

  TextStyle style(TextStyle baseStyle, ColorScheme scheme) => switch (action) {
        MarkdownEditAction.bold =>
          baseStyle.copyWith(fontWeight: FontWeight.bold),
        MarkdownEditAction.italic =>
          baseStyle.copyWith(fontStyle: FontStyle.italic),
        MarkdownEditAction.monospace =>
          baseStyle.copyWith(fontFamily: 'JetbrainsMono'),
        MarkdownEditAction.highlight => baseStyle.copyWith(
            backgroundColor: scheme.primary,
            color: scheme.onPrimary,
          ),
      };
}

class TextSelectionMenu extends StatelessWidget {
  const TextSelectionMenu({
    required this.editableTextState,
    required this.anchors,
    required this.textEditingValue,
    required this.contextMenuButtonItems,
    required this.copyEnabled,
    this.markdownEnabled = false,
    super.key,
  });

  final EditableTextState editableTextState;
  final TextSelectionToolbarAnchors anchors;
  final TextEditingValue textEditingValue;
  final List<ContextMenuButtonItem> contextMenuButtonItems;
  final bool markdownEnabled;
  final bool copyEnabled;

  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context)!;
    final scheme = Theme.of(context).colorScheme;
    final text = Theme.of(context).textTheme;

    final buttonItems = contextMenuButtonItems;

    if (copyEnabled && !buttonItems.any((btn) => btn.label == strings.share)) {
      final selection = textEditingValue.selection;
      final text = textEditingValue.text;

      final index = text.length == selection.textInside(text).length
          ? buttonItems.length
          : buttonItems.length - 1;

      buttonItems.insertAll(
        index,
        [
          if (markdownEnabled) ...[
            for (final action in MarkdownEditAction.values)
              MarkdownContextMenuButtonItem(
                label: action.localize(strings),
                onPressed: () => editMarkdown(action),
                action: action,
              ),
          ],
          ContextMenuButtonItem(
            type: ContextMenuButtonType.share,
            label: strings.share,
            onPressed: () {
              if (selection.isCollapsed) return;
              Share.share(selection.textInside(text));
            },
          ),
        ],
      );
    }

    if (!Platform.isAndroid) {
      return AdaptiveTextSelectionToolbar.buttonItems(
        anchors: anchors,
        buttonItems: buttonItems,
      );
    }

    final isDarkMode = Theme.of(context).brightness == Brightness.dark;
    final backgroundColor =
        isDarkMode ? const Color(0xff424242) : const Color(0xffffffff);
    final foregroundColor =
        isDarkMode ? const Color(0xffffffff) : const Color(0xff000000);

    final List<Widget> buttons = <Widget>[];
    for (final (i, buttonItem) in buttonItems.enumerate()) {
      final baseStyle = text.labelLarge!.copyWith(color: foregroundColor);

      final style = switch (buttonItem) {
        MarkdownContextMenuButtonItem() => buttonItem.style(baseStyle, scheme),
        _ => baseStyle,
      };

      buttons.add(TextSelectionToolbarTextButton(
        padding: TextSelectionToolbarTextButton.getPadding(
          i,
          buttonItems.length,
        ),
        onPressed: buttonItem.onPressed,
        alignment: Alignment.center,
        child: Text(
          AdaptiveTextSelectionToolbar.getButtonLabel(context, buttonItem),
          style: style,
        ),
      ));
    }

    return TextSelectionToolbar(
      toolbarBuilder: (context, child) => Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(24),
          color: backgroundColor,
          boxShadow: const [BoxShadow(blurRadius: 2, color: Colors.black26)],
        ),
        clipBehavior: Clip.hardEdge,
        child: child,
      ),
      anchorAbove: anchors.primaryAnchor,
      anchorBelow: anchors.secondaryAnchor == null
          ? anchors.primaryAnchor
          : anchors.secondaryAnchor!,
      children: buttons,
    );
  }

  void editMarkdown(MarkdownEditAction action) {
    final selection = textEditingValue.selection;
    final start = selection.start;
    final end = selection.end;
    final text = textEditingValue.text.substring(start, end);

    switch (action) {
      case MarkdownEditAction.bold:
        editableTextState.replaceText(
            SelectionChangedCause.toolbar, '**$text**');
      case MarkdownEditAction.italic:
        editableTextState.replaceText(SelectionChangedCause.toolbar, '*$text*');
      case MarkdownEditAction.highlight:
        editableTextState.replaceText(
            SelectionChangedCause.toolbar, '==$text==');
      case MarkdownEditAction.monospace:
        editableTextState.replaceText(SelectionChangedCause.toolbar, '`$text`');
    }
  }
}

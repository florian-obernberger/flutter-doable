import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class LinProgressIndicator extends ProgressIndicator {
  static const kIndeterminateLinearDuration = 1800;

  const LinProgressIndicator({
    super.key,
    super.value,
    super.backgroundColor,
    super.color,
    super.valueColor,
    this.minHeight,
    super.semanticsLabel,
    super.semanticsValue,
    this.borderRadius = const BorderRadius.all(Radius.circular(28)),
  }) : assert(minHeight == null || minHeight > 0);

  final double? minHeight;

  final BorderRadiusGeometry borderRadius;

  @override
  State<LinProgressIndicator> createState() => _LinProgressIndicatorState();

  Widget _buildSemanticsWrapper({
    required BuildContext context,
    required Widget child,
  }) {
    String? expandedSemanticsValue = semanticsValue;
    if (value != null) {
      expandedSemanticsValue ??= '${(value! * 100).round()}%';
    }
    return Semantics(
      label: semanticsLabel,
      value: expandedSemanticsValue,
      child: child,
    );
  }

  Color _getValueColor(BuildContext context, {Color? defaultColor}) {
    return valueColor?.value ??
        color ??
        ProgressIndicatorTheme.of(context).color ??
        defaultColor ??
        Theme.of(context).colorScheme.primary;
  }
}

class _LinProgressIndicatorState extends State<LinProgressIndicator>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      duration: const Duration(
          milliseconds: LinProgressIndicator.kIndeterminateLinearDuration),
      vsync: this,
    );
    if (widget.value == null) {
      _controller.repeat();
    }
  }

  @override
  void didUpdateWidget(LinProgressIndicator oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.value == null && !_controller.isAnimating) {
      _controller.repeat();
    } else if (widget.value != null && _controller.isAnimating) {
      _controller.stop();
    }
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  Widget _buildIndicator(
    BuildContext context,
    double animationValue,
    TextDirection textDirection,
  ) {
    final defaults = _LinProgressIndicatorDefaultsM3(context);

    final indicatorTheme = ProgressIndicatorTheme.of(context);
    final trackColor = widget.backgroundColor ??
        indicatorTheme.linearTrackColor ??
        defaults.linearTrackColor;

    final minHeight = widget.minHeight ??
        indicatorTheme.linearMinHeight ??
        defaults.linearMinHeight;

    return widget._buildSemanticsWrapper(
      context: context,
      child: Container(
        // Clip is only needed with indeterminate progress indicators
        clipBehavior:
            (widget.borderRadius != BorderRadius.zero && widget.value == null)
                ? Clip.antiAlias
                : Clip.none,
        decoration: ShapeDecoration(
          shape: RoundedRectangleBorder(borderRadius: widget.borderRadius),
        ),
        constraints: BoxConstraints(
          minWidth: double.infinity,
          minHeight: minHeight,
        ),
        child: CustomPaint(
          painter: _LinProgressIndicatorPainter(
            backgroundColor: trackColor,
            valueColor:
                widget._getValueColor(context, defaultColor: defaults.color),
            value: widget.value,
            animationValue: animationValue,
            textDirection: textDirection,
            indicatorBorderRadius: widget.borderRadius,
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final TextDirection textDirection = Directionality.of(context);

    final Widget child;

    if (widget.value != null) {
      child = _buildIndicator(context, _controller.value, textDirection);
    } else {
      child = AnimatedBuilder(
        animation: _controller.view,
        builder: (BuildContext context, Widget? child) {
          return _buildIndicator(context, _controller.value, textDirection);
        },
      );
    }

    return Padding(
      padding: const EdgeInsetsDirectional.symmetric(horizontal: 4),
      child: child,
    );
  }
}

class _LinProgressIndicatorPainter extends CustomPainter {
  const _LinProgressIndicatorPainter({
    required this.backgroundColor,
    required this.valueColor,
    this.value,
    required this.animationValue,
    required this.textDirection,
    required this.indicatorBorderRadius,
  });

  final Color backgroundColor;
  final Color valueColor;
  final double? value;
  final double animationValue;
  final TextDirection textDirection;
  final BorderRadiusGeometry indicatorBorderRadius;

  // The indeterminate progress animation displays two lines whose leading (head)
  // and trailing (tail) endpoints are defined by the following four curves.
  static const Curve line1Head = Interval(
    0.0,
    750.0 / LinProgressIndicator.kIndeterminateLinearDuration,
    curve: Cubic(0.2, 0.0, 0.8, 1.0),
  );
  static const Curve line1Tail = Interval(
    333.0 / LinProgressIndicator.kIndeterminateLinearDuration,
    (333.0 + 750.0) / LinProgressIndicator.kIndeterminateLinearDuration,
    curve: Cubic(0.4, 0.0, 1.0, 1.0),
  );
  static const Curve line2Head = Interval(
    1000.0 / LinProgressIndicator.kIndeterminateLinearDuration,
    (1000.0 + 567.0) / LinProgressIndicator.kIndeterminateLinearDuration,
    curve: Cubic(0.0, 0.0, 0.65, 1.0),
  );
  static const Curve line2Tail = Interval(
    1267.0 / LinProgressIndicator.kIndeterminateLinearDuration,
    (1267.0 + 533.0) / LinProgressIndicator.kIndeterminateLinearDuration,
    curve: Cubic(0.10, 0.0, 0.45, 1.0),
  );

  @override
  void paint(Canvas canvas, Size size) {
    void drawBar(double x, double width, Color color) {
      if (width <= 0.0) {
        return;
      }

      final paint = Paint()
        ..color = color
        ..style = PaintingStyle.fill;

      final double left = switch (textDirection) {
        TextDirection.rtl => size.width - width - x,
        TextDirection.ltr => x,
      };

      final Rect rect = Offset(left, 0.0) & Size(width, size.height);
      if (indicatorBorderRadius != BorderRadius.zero) {
        final RRect rrect =
            indicatorBorderRadius.resolve(textDirection).toRRect(rect);
        canvas.drawRRect(rrect, paint);
      } else {
        canvas.drawRect(rect, paint);
      }
    }

    if (value != null) {
      final width = clampDouble(value!, 0.0, 1.0) * size.width;
      drawBar(0.0, width, valueColor);

      final xTrack = width + 4;
      final widthTrack = size.width - width - 4;
      drawBar(xTrack, widthTrack, backgroundColor);
      drawBar(xTrack + widthTrack - 4, 4, valueColor);
    } else {
      final double x1 = size.width * line1Tail.transform(animationValue);
      final double width1 =
          size.width * line1Head.transform(animationValue) - x1;

      final double x2 = size.width * line2Tail.transform(animationValue);
      final double width2 =
          size.width * line2Head.transform(animationValue) - x2;

      drawBar(x1, width1, valueColor);
      drawBar(x2, width2, valueColor);

      if (x1 > 0.0) {
        if (width2 == 0.0) {
          drawBar(0.0, x1 - 4, backgroundColor);
        } else if (width2 > 0.0) {
          drawBar(
            x2 + width2 + 4,
            width1 == 0.0 ? size.width - width2 : x1 - width2 - 8,
            backgroundColor,
          );
        }
      }

      if (width1 > 0.0 && width2 == 0.0) {
        drawBar(x1 + width1 + 4, size.width, backgroundColor);
      } else {
        drawBar(0.0, x2 - 4, backgroundColor);
      }
    }
  }

  @override
  bool shouldRepaint(_LinProgressIndicatorPainter oldPainter) {
    return oldPainter.backgroundColor != backgroundColor ||
        oldPainter.valueColor != valueColor ||
        oldPainter.value != value ||
        oldPainter.animationValue != animationValue ||
        oldPainter.textDirection != textDirection ||
        oldPainter.indicatorBorderRadius != indicatorBorderRadius;
  }
}

class _LinProgressIndicatorDefaultsM3 extends ProgressIndicatorThemeData {
  _LinProgressIndicatorDefaultsM3(this.context);

  final BuildContext context;
  late final ColorScheme _colors = Theme.of(context).colorScheme;

  @override
  Color get color => _colors.primary;

  @override
  Color get linearTrackColor => _colors.secondaryContainer;

  @override
  double get linearMinHeight => 4.0;
}

class LinProgressIndicatorScaleTransition extends MatrixTransition {
  LinProgressIndicatorScaleTransition({
    super.key,
    required LinProgressController controller,
    super.alignment = Alignment.center,
    super.filterQuality,
    super.child,
  }) : super(
          animation: CurvedAnimation(
            curve: Easing.emphasizedDecelerate,
            reverseCurve: Easing.emphasizedAccelerate,
            parent: controller,
          ),
          onTransform: _handleScaleMatrix,
        );

  Animation<double> get scale => animation;

  static Matrix4 _handleScaleMatrix(double value) => Matrix4.diagonal3Values(
        0.99 + 0.01 * value, // horizontal
        value, // vertical
        1.0,
      );
}

class LinProgressController extends AnimationController {
  LinProgressController({required super.vsync})
      : super(value: 1, duration: Durations.short4);

  Future<void> hide() => Future.delayed(
        const Duration(milliseconds: 1350),
        reverse,
      );

  TickerFuture show() => forward();
}

import 'package:flutter/material.dart';

class DetailedSwitchListTile extends ListTile {
  const DetailedSwitchListTile({
    super.title,
    super.subtitle,
    super.isThreeLine,
    super.leading,
    super.style,
    super.enabled,
    required this.value,
    this.disableDetailsIfOff = true,
    required this.onSwitchChanged,
    required this.onShowDetails,
    this.activeColor,
    this.activeTrackColor,
    this.inactiveTrackColor,
    this.inactiveThumbColor,
    this.thumbIcon,
    super.key,
  });

  final bool value;
  final ValueChanged<bool> onSwitchChanged;
  final VoidCallback onShowDetails;
  final bool disableDetailsIfOff;

  final Color? activeColor;
  final Color? activeTrackColor;
  final Color? inactiveTrackColor;
  final Color? inactiveThumbColor;
  final WidgetStateProperty<Icon?>? thumbIcon;

  @override
  Widget build(BuildContext context) {
    final color = Theme.of(context).colorScheme.outline;
    final disabledColor = Theme.of(context).colorScheme.outlineVariant;
    const double indent = 8;

    final switchEnabled = enabled;
    final detailsEnabled = enabled && !(disableDetailsIfOff && !value);

    final control = Row(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment:
          isThreeLine ? CrossAxisAlignment.start : CrossAxisAlignment.center,
      children: <Widget>[
        VerticalDivider(
          width: 16,
          indent: indent,
          endIndent: indent,
          color: enabled ? color : disabledColor,
        ),
        Switch(
          value: value,
          onChanged: switchEnabled ? onSwitchChanged : null,
          activeColor: activeColor,
          thumbIcon: thumbIcon,
          materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
          activeTrackColor: activeTrackColor,
          inactiveTrackColor: inactiveTrackColor,
          inactiveThumbColor: inactiveThumbColor,
        ),
      ],
    );

    return ListTile(
      enabled: detailsEnabled,
      key: key,
      leading: leading,
      title: title,
      subtitle: subtitle,
      trailing: control,
      isThreeLine: isThreeLine,
      style: style,
      onTap: detailsEnabled ? onShowDetails : null,
    );
  }
}

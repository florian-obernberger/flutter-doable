import 'package:flutter/material.dart';

class SelectableTab extends Tab {
  final TabController controller;
  final Widget? activeIcon;
  final int index;

  const SelectableTab({
    super.key,
    required this.controller,
    required this.index,
    super.text,
    required Icon super.icon,
    this.activeIcon,
    super.iconMargin,
    super.height,
    super.child,
  });

  @override
  Widget get icon => super.icon!;

  @override
  Widget build(BuildContext context) {
    return Tab(
      text: text,
      icon: AnimatedBuilder(
        animation: controller.animation!,
        builder: (context, _) {
          return AnimatedSwitcher(
            switchInCurve: Easing.standardDecelerate,
            switchOutCurve: Easing.standardAccelerate,
            duration: controller.animationDuration ~/ 2,
            child: _getIcon(controller.animation!.value),
          );
        },
      ),
      iconMargin: iconMargin,
      height: height,
      child: child,
    );
  }

  Widget _getIcon(double animValue) {
    const activeKey = Key('active-tab-icon');
    const key = Key('inactive-tab-icon');

    final inactive = KeyedSubtree(key: key, child: icon);
    if (activeIcon == null) return inactive;

    final active = KeyedSubtree(key: activeKey, child: activeIcon!);

    final curIndex = animValue.round();
    return curIndex == index ? active : inactive;
  }
}

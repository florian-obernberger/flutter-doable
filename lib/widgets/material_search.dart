import 'package:flutter/material.dart';
import 'package:material_symbols_icons/symbols.dart';

import '/util/build_text_selection_menu.dart';

import 'search.dart' as search;
import 'back_button.dart' as back;

abstract class MaterialSearchDelegate<T> extends search.SearchDelegate<T?> {
  MaterialSearchDelegate({
    super.searchFieldLabel,
    super.searchFieldStyle,
    super.searchFieldDecorationTheme,
    super.keyboardType,
    super.textInputAction,
  });

  @override
  EditableTextContextMenuBuilder? get contextMenuBuilder =>
      buildTextSelectionMenu;

  @override
  ThemeData appBarTheme(BuildContext context) {
    final scheme = Theme.of(context).colorScheme;
    final text = Theme.of(context).textTheme;

    return Theme.of(context).copyWith(
      // textTheme: text.copyWith(
      //   titleLarge: text.bodyLarge?.copyWith(color: scheme.onSurface),
      // ),
      inputDecorationTheme: InputDecorationTheme(
        hintStyle: text.bodyLarge?.copyWith(color: scheme.onSurfaceVariant),
        border: InputBorder.none,
      ),
      appBarTheme: AppBarTheme.of(context).copyWith(
        elevation: 3,
        scrolledUnderElevation: 3,
        toolbarHeight: 72,
        titleTextStyle: text.bodyLarge?.copyWith(color: scheme.onSurface),
      ),
      scaffoldBackgroundColor: ElevationOverlay.applySurfaceTint(
        scheme.surface,
        scheme.surfaceTint,
        3,
      ),
    );
  }

  @override
  PreferredSizeWidget? buildBottom(BuildContext context) {
    final scheme = Theme.of(context).colorScheme;

    return PreferredSize(
      preferredSize: const Size.fromHeight(1),
      child: Divider(height: 1, color: scheme.outline),
    );
  }

  @override
  List<Widget>? buildActions(BuildContext context) {
    if (query.isEmpty) {
      return [];
    }

    return [
      Padding(
        padding: const EdgeInsets.only(right: 8),
        child: IconButton(
          onPressed: () => query = '',
          icon: const Icon(Symbols.clear),
        ),
      )
    ];
  }

  @override
  Widget? buildLeading(BuildContext context) => back.BackButton(
        onBack: () => close(context, null),
      );
}

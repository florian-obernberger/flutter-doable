import 'package:flutter/material.dart' show TimeOfDay;
import 'package:rrule/rrule.dart' as rr;

import '/types/weekday.dart';
import 'ending.dart';
import 'frequency.dart';

export '/types/weekday.dart';
export 'ending.dart';
export 'frequency.dart';

class Recurrence {
  final Ending end;
  final Frequency freq;
  final int interval;

  const Recurrence.daily({
    required this.end,
    required this.interval,
    this.months,
    this.dailyTime,
  })  : freq = Frequency.daily,
        weekdays = null,
        monthlyStartDate = null,
        monthlyType = null;

  const Recurrence.weekly({
    required this.end,
    required this.interval,
    this.months,
    required Set<Weekday> this.weekdays,
  })  : freq = Frequency.weekly,
        dailyTime = null,
        monthlyStartDate = null,
        monthlyType = null;

  const Recurrence.monthly({
    required this.end,
    required this.interval,
    this.months,
    required DateTime this.monthlyStartDate,
    required MonthlyType this.monthlyType,
  })  : freq = Frequency.monthly,
        dailyTime = null,
        weekdays = null;

  const Recurrence.yearly({
    required this.end,
    required this.interval,
    this.months,
  })  : freq = Frequency.yearly,
        dailyTime = null,
        weekdays = null,
        monthlyStartDate = null,
        monthlyType = null;

  // If freq == Frequency.daily

  final TimeOfDay? dailyTime;

  // If freq == Frequency.weekly

  final Set<Weekday>? weekdays;

  // If freq == Frequency.monthly

  final DateTime? monthlyStartDate;
  final MonthlyType? monthlyType;

  // If freq == Frequency.yearly

  final List<int>? months;

  rr.RecurrenceRule toRule() {
    switch (freq) {
      case Frequency.daily:
        final (hour, minute) = dailyTime == null
            ? const (<int>[], <int>[])
            : ([dailyTime!.hour], [dailyTime!.minute]);

        return rr.RecurrenceRule(
          interval: interval,
          frequency: rr.Frequency.daily,
          byHours: hour,
          byMinutes: minute,
          byMonths: months ?? [],
        );
      case Frequency.weekly:
        return rr.RecurrenceRule(
          interval: interval,
          frequency: rr.Frequency.weekly,
          byWeekDays: weekdays!.map((d) => rr.ByWeekDayEntry(d.day)).toList(),
          byMonths: months ?? [],
        );
      case Frequency.monthly:
        final date = monthlyStartDate!;

        final rule = rr.RecurrenceRule(
          frequency: rr.Frequency.monthly,
          byMinutes: months ?? [],
        );

        return switch (monthlyType!) {
          MonthlyType.onSameDay => rule.copyWith(byWeekDays: [
              rr.ByWeekDayEntry(date.weekday, _calcWeekDayOffset(date))
            ]),
          MonthlyType.onExactDate => rule.copyWith(byMonthDays: [date.day]),
        };
      case Frequency.yearly:
        return rr.RecurrenceRule(
          interval: interval,
          frequency: rr.Frequency.yearly,
          byMinutes: months ?? [],
        );
    }
  }

  int _calcWeekDayOffset(DateTime date) {
    final weekday = date.weekday;
    int offset = 1;
    DateTime tempDate = DateTime(date.year, date.month, 1);

    while (tempDate.weekday != weekday) {
      tempDate = tempDate.add(const Duration(days: 1));
    }

    while (tempDate.day != date.day) {
      tempDate = tempDate.add(const Duration(days: 7));
      offset++;
    }

    return offset;
  }
}

sealed class Ending {
  const Ending();
}

class Never extends Ending {
  const Never();

  @override
  // ignore: hash_and_equals
  bool operator ==(covariant other) => other is Never;
}

class Count extends Ending {
  const Count(this.count);

  final int count;

  @override
  int get hashCode => count.hashCode;

  @override
  bool operator ==(covariant other) => other is Count && other.count == count;
}

class EndDate extends Ending {
  const EndDate(this.date);

  final DateTime date;

  @override
  int get hashCode => date.hashCode;

  @override
  bool operator ==(covariant other) => other is EndDate && other.date == date;
}

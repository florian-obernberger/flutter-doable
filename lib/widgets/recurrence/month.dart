// cSpell: disable

enum Month {
  january(DateTime.january),
  february(DateTime.february),
  march(DateTime.march),
  april(DateTime.april),
  may(DateTime.may),
  june(DateTime.june),
  july(DateTime.july),
  august(DateTime.august),
  september(DateTime.september),
  october(DateTime.october),
  november(DateTime.november),
  december(DateTime.december);

  const Month(this.month);

  final int month;
}

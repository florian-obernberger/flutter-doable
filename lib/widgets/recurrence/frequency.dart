enum Frequency { daily, weekly, monthly, yearly }

enum MonthlyType { onExactDate, onSameDay }

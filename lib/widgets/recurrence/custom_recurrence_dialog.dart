import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

import '/strings/strings.dart';
import '/state/settings.dart';
import '/classes/motion.dart';
import '/widgets/nav_button.dart';
import '/widgets/save_button.dart';

import 'recurrence.dart';

import 'custom_recurrence_dialog.d/frequency_picker.dart';
import 'custom_recurrence_dialog.d/weekday_picker.dart';
import 'custom_recurrence_dialog.d/ending_picker.dart';

class CustomRecurrenceDialog extends StatefulWidget {
  const CustomRecurrenceDialog({required this.date, super.key});

  final DateTime date;

  @override
  State<CustomRecurrenceDialog> createState() => _CustomRecurrenceDialogState();
}

class _CustomRecurrenceDialogState extends State<CustomRecurrenceDialog>
    with SingleTickerProviderStateMixin {
  final notifier = ValueNotifier<(Frequency, Ending, int)>((
    Frequency.weekly,
    const Never(),
    1,
  ));

  final weekdays = <Weekday>{};

  final controller = ScrollController();

  late final Animation<double> weekAnimation;
  late final AnimationController animController;

  Settings? _settings;
  Settings get settings => _settings!;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    if (_settings == null) {
      _settings = Settings.of(context);

      final duration = settings.accessability.reduceMotion.value
          ? Duration.zero
          : Motion.long1;

      animController = AnimationController(
        vsync: this,
        duration: duration,
        value: notifier.value.$1 == Frequency.weekly ? 1 : 0,
      );

      weekAnimation = CurvedAnimation(
        parent: animController,
        curve: Easing.standard,
      );

      notifier.addListener(() {
        if (notifier.value.$1 == Frequency.weekly) {
          animController.forward();
        } else {
          animController.reverse();
        }
      });
    }
  }

  @override
  void dispose() {
    animController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context)!;

    final scheme = Theme.of(context).colorScheme;
    final text = Theme.of(context).textTheme;

    final labelStyle = text.labelLarge!.copyWith(color: scheme.onSurface);

    final double size =
        64 * (settings.accessability.textScaleFactor.value ?? 1);
    const padding = EdgeInsets.all(24);
    const spacer = SizedBox(height: 12);

    return Dialog.fullscreen(
      backgroundColor: scheme.surfaceContainerHigh,
      child: Scaffold(
        backgroundColor: scheme.surfaceContainerHigh,
        appBar: AppBar(
          backgroundColor: scheme.surfaceContainerHigh,
          notificationPredicate: (_) => false,
          leading: NavButton.close(onNav: context.pop),
          title: Text(strings.rrCustomRecurrence),
          actions: <Widget>[
            Padding(
              padding: const EdgeInsetsDirectional.only(end: 16),
              child: SaveButton(onSave: save, isFilled: false),
            )
          ],
        ),
        body: ValueListenableBuilder(
          valueListenable: notifier,
          builder: (context, value, weekPicker) {
            final (freq, ending, interval) = value;

            return ListView(
              physics: const ClampingScrollPhysics(),
              controller: controller,
              children: <Widget>[
                // Frequency
                FrequencyPicker(
                  padding: padding,
                  size: size,
                  spacer: spacer,
                  labelStyle: labelStyle,
                  frequency: freq,
                  interval: interval,
                  updateFrequency: (frequency) => notifier.value = (
                    frequency ?? freq,
                    ending,
                    interval,
                  ),
                ),
                weekPicker!,
                const Divider(),
                EndingPicker(
                  padding: padding,
                  labelStyle: labelStyle,
                  date: widget.date,
                  onChanged: (end) => notifier.value = (
                    freq,
                    end,
                    interval,
                  ),
                  selected: switch (ending) {
                    Never() => EndingType.never,
                    Count() => EndingType.count,
                    EndDate() => EndingType.date,
                  },
                ),
              ],
            );
          },
          child: FadeTransition(
            opacity: weekAnimation,
            child: SizeTransition(
              sizeFactor: weekAnimation,
              axisAlignment: -1,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  const Divider(),
                  StatefulBuilder(
                    builder: (context, state) => WeekdayPicker(
                      labelStyle: labelStyle,
                      padding: padding,
                      spacer: spacer,
                      selected: weekdays,
                      onSelect: (day) => state(() => weekdays.add(day)),
                      onDeselect: (day) => state(
                        () => weekdays.remove(day),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void save() {
    final (freq, end, interval) = notifier.value;

    final rRule = switch (freq) {
      Frequency.daily => Recurrence.daily(
          end: end,
          interval: interval,
          dailyTime: TimeOfDay.fromDateTime(widget.date),
        ),
      Frequency.weekly => Recurrence.weekly(
          end: end,
          interval: interval,
          weekdays: weekdays,
        ),
      Frequency.monthly => Recurrence.monthly(
          end: end,
          interval: interval,
          monthlyStartDate: widget.date,
          monthlyType: MonthlyType.onExactDate,
        ),
      Frequency.yearly => Recurrence.yearly(
          end: end,
          interval: interval,
        ),
    };

    context.pop(rRule);
  }
}

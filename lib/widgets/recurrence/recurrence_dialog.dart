import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:rrule/rrule.dart' as rr;

import '/strings/strings.dart';
import '/classes/motion.dart';
import '/util/animated_dialog.dart';

import 'recurrence.dart';
// import 'custom_recurrence_dialog.dart';

typedef RROption = rr.RecurrenceRule?;

enum RecurrenceType {
  never,
  daily,
  weekly,
  monthly,
  yearly,
  // custom
  ;

  static RecurrenceType? fromFrequency(rr.Frequency? freq) => switch (freq) {
        null => null,
        rr.Frequency.daily => daily,
        rr.Frequency.weekly => weekly,
        rr.Frequency.monthly => monthly,
        rr.Frequency.yearly => yearly,
        rr.Frequency.hourly => daily,
        rr.Frequency.minutely => daily,
        rr.Frequency.secondly => daily,
        rr.Frequency() => null,
      };
}

Future<RROption> showRecurrenceDialog({
  required BuildContext context,
  required DateTime initialDate,
  RecurrenceType? recurrenceType,
  bool barrierDismissible = true,
  Duration duration = Motion.long2,
  Duration? reverseDuration,
  bool useSafeArea = true,
}) async {
  return showAnimatedDialog(
    context: context,
    builder: (context) => RecurrenceDialog(
      date: initialDate,
      recurrence: recurrenceType,
    ),
    barrierDismissible: barrierDismissible,
    duration: duration,
    reverseDuration: reverseDuration,
    animationType: DialogTransitionType.fadeScale,
    useSafeArea: useSafeArea,
  );
}

class RecurrenceDialog extends StatefulWidget {
  const RecurrenceDialog({this.recurrence, required this.date, super.key});

  final RecurrenceType? recurrence;
  final DateTime date;

  @override
  State<RecurrenceDialog> createState() => _RecurrenceDialogState();
}

class _RecurrenceDialogState extends State<RecurrenceDialog> {
  late RecurrenceType recurrence = widget.recurrence ?? RecurrenceType.never;

  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context)!;

    return AlertDialog(
      clipBehavior: Clip.antiAliasWithSaveLayer,
      contentPadding: const EdgeInsets.fromLTRB(0, 16, 0, 24),
      title: Text(strings.rrRecurrence),
      content: Material(
        color: DialogTheme.of(context).backgroundColor,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            for (var rr in RecurrenceType.values)
              RadioListTile<RecurrenceType>(
                tileColor: Colors.transparent,
                value: rr,
                title: Text(text(rr, strings)),
                groupValue: recurrence,
                onChanged: onChanged,
              ),
          ],
        ),
      ),
      actions: [
        TextButton(
          onPressed: Navigator.of(context).pop,
          child: Text(strings.cancel),
        ),
        TextButton(
          onPressed: submit,
          child: Text(strings.ok),
        ),
      ],
    );
  }

  String text(RecurrenceType rr, Strings strings) => switch (rr) {
        RecurrenceType.never => strings.never,
        RecurrenceType.daily => strings.daily,
        RecurrenceType.weekly => strings.weekly,
        RecurrenceType.monthly => strings.monthly,
        RecurrenceType.yearly => strings.yearly,
        // RecurrenceType.custom => strings.rrCustomRecurrence,
      };

  Future<void> submit() async {
    final router = GoRouter.of(context);

    final RROption rRule = switch (recurrence) {
      RecurrenceType.never => null,
      RecurrenceType.daily =>
        const Recurrence.daily(end: Never(), interval: 1).toRule(),
      RecurrenceType.weekly => Recurrence.weekly(
          end: const Never(),
          interval: 1,
          weekdays: {Weekday.fromDateTime(widget.date)},
        ).toRule(),
      RecurrenceType.monthly => Recurrence.monthly(
          end: const Never(),
          interval: 1,
          monthlyStartDate: widget.date,
          monthlyType: MonthlyType.onSameDay,
        ).toRule(),
      RecurrenceType.yearly =>
        const Recurrence.yearly(end: Never(), interval: 1).toRule(),
      // RecurrenceType.custom => await showAnimatedDialog<Recurrence>(
      //     useSafeArea: false,
      //     context: context,
      //     builder: (context) => CustomRecurrenceDialog(date: widget.date),
      //     animationType: DialogTransitionType.slideFromBottomFade,
      //   ).then((rr) => rr == null ? null : Some(rr.toRule())),
    };

    if (mounted && router.canPop()) router.pop(rRule);
  }

  void onChanged(RecurrenceType? rr) {
    if (rr != null) {
      setState(() => recurrence = rr);
    }
  }
}

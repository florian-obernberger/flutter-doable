import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:result/result.dart';
import 'package:rrule/rrule.dart' as rr;
import 'package:numberpicker/numberpicker.dart';

import '/strings/strings.dart';

enum Frequency {
  never,
  minutely,
  hourly,
  daily,
  weekly,
  monthly,
  yearly;

  String localize(BuildContext context) {
    final strings = Strings.of(context)!;
    final localizations = MaterialLocalizations.of(context);

    return switch (this) {
      never => strings.never,
      minutely => localizations.timePickerMinuteLabel,
      hourly => localizations.timePickerHourLabel,
      daily => strings.day,
      weekly => strings.week,
      monthly => strings.month,
      yearly => strings.year,
    };
  }

  static Frequency? fromFrequency(rr.Frequency? freq) => switch (freq) {
        null => null,
        // rr.Frequency.secondly => secondly,
        rr.Frequency.minutely => minutely,
        rr.Frequency.hourly => hourly,
        rr.Frequency.daily => daily,
        rr.Frequency.weekly => weekly,
        rr.Frequency.monthly => monthly,
        rr.Frequency.yearly => yearly,
        rr.Frequency() => null,
      };

  rr.Frequency? toFrequency() => switch (this) {
        never => null,
        // secondly => rr.Frequency.secondly,
        minutely => rr.Frequency.minutely,
        hourly => rr.Frequency.hourly,
        daily => rr.Frequency.daily,
        weekly => rr.Frequency.weekly,
        monthly => rr.Frequency.monthly,
        yearly => rr.Frequency.yearly,
      };

  static List<(Frequency, Frequency)> pairs({
    bool daysOnly = false,
  }) =>
      [
        if (!daysOnly) (minutely, hourly),
        (daily, weekly),
        (monthly, yearly),
      ];
}

class RecurrenceBottomSheet extends StatefulWidget {
  const RecurrenceBottomSheet({
    this.interval,
    this.frequency,
    this.daysOnly = false,
    super.key,
  });

  final int? interval;
  final Frequency? frequency;
  final bool daysOnly;

  @override
  State<RecurrenceBottomSheet> createState() => _RecurrenceBottomSheetState();
}

class _RecurrenceBottomSheetState extends State<RecurrenceBottomSheet> {
  late final intervalNotifier = ValueNotifier(widget.interval ?? 1);
  late final frequencyNotifier = ValueNotifier(
    widget.frequency ?? Frequency.weekly,
  );

  @override
  void dispose() {
    intervalNotifier.dispose();
    frequencyNotifier.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context)!;
    final scheme = Theme.of(context).colorScheme;
    final text = Theme.of(context).textTheme;
    final mediaQuery = MediaQuery.of(context);

    const contentPadding = EdgeInsetsDirectional.only(start: 20, end: 12);
    const disabledOpacity = 0.38;

    return Padding(
      padding: EdgeInsets.only(
        bottom: 16 + mediaQuery.viewPadding.bottom,
      ),
      child: ValueListenableBuilder(
        valueListenable: frequencyNotifier,
        builder: (context, frequency, _) => Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              width: double.infinity,
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Row(
                children: <Widget>[
                  Text(
                    strings.rrRecurrence,
                    style: text.titleMedium?.copyWith(
                      color: scheme.onSurface,
                      fontWeight: FontWeight.bold,
                    ),
                    textAlign: TextAlign.left,
                  ),
                  const Spacer(),
                  FilledButton(
                    onPressed: save,
                    child: Text(strings.ok),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Text(
                strings.interval,
                style: text.labelLarge?.copyWith(
                  color: scheme.onSurfaceVariant,
                ),
              ),
            ),
            ValueListenableBuilder(
              valueListenable: intervalNotifier,
              builder: (context, interval, _) {
                final enabled = frequency != Frequency.never;

                final selectedStyle = text.bodyLarge!.copyWith(
                  color: enabled
                      ? scheme.primary
                      : scheme.onSurface.withOpacity(disabledOpacity),
                  fontWeight: FontWeight.bold,
                );
                final style = text.bodySmall!.copyWith(
                  color: enabled
                      ? scheme.onSurface
                      : scheme.onSurface.withOpacity(disabledOpacity),
                );

                return AbsorbPointer(
                  absorbing: frequency == Frequency.never,
                  child: NumberPicker(
                    axis: Axis.horizontal,
                    haptics: true,
                    itemCount: (mediaQuery.size.width - 40) ~/ 72 + 1,
                    itemWidth: 72,
                    selectedTextStyle: selectedStyle,
                    textStyle: style,
                    minValue: 1,
                    maxValue: 1024,
                    value: interval,
                    onChanged: (value) => intervalNotifier.value = value,
                  ),
                );
              },
            ),
            const Divider(indent: 20, endIndent: 20),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 8),
              child: Text(
                strings.rrRepeatsEvery,
                style: text.labelLarge?.copyWith(
                  color: scheme.onSurfaceVariant,
                ),
              ),
            ),
            for (final (left, right) in Frequency.pairs(
              daysOnly: widget.daysOnly,
            ))
              Row(
                children: <Widget>[
                  Flexible(
                    child: RadioListTile(
                      value: left,
                      contentPadding: contentPadding,
                      groupValue: frequency,
                      onChanged: (_) => frequencyNotifier.value = left,
                      title: Text(left.localize(context)),
                    ),
                  ),
                  Flexible(
                    child: RadioListTile(
                      value: right,
                      contentPadding: contentPadding,
                      groupValue: frequency,
                      onChanged: (_) => frequencyNotifier.value = right,
                      title: Text(right.localize(context)),
                    ),
                  ),
                ],
              ),
          ],
        ),
      ),
    );
  }

  void save() {
    final freq = frequencyNotifier.value;
    final interval = intervalNotifier.value;

    if (freq == Frequency.never) {
      context.pop(const None<rr.RecurrenceRule>());
      return;
    }

    final rRule = rr.RecurrenceRule(
      frequency: freq.toFrequency()!,
      interval: interval,
    );

    context.pop(Some(rRule));
  }
}

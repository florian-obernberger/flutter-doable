// import 'package:flutter/material.dart';

// import '/strings/strings.dart';
// import '/widgets/dropdown.dart' as dd;

// import '../recurrence.dart';

// class MonthPicker extends StatelessWidget {
//   const MonthPicker({
//     required this.padding,
//     required this.selected,
//     required this.onSelect,
//     required this.labelStyle,
//     super.key,
//   });

//   final MonthlyType selected;
//   final void Function(MonthlyType type) onSelect;
//   final EdgeInsetsGeometry padding;
//   final TextStyle labelStyle;

//   @override
//   Widget build(BuildContext context) {
//     final strings = Strings.of(context)!;

//     final scheme = Theme.of(context).colorScheme;
//     final dayStyle = Theme.of(context).textTheme.bodyLarge!;

//     return Padding(
//       padding: padding,
//       child: dd.DropdownButton<MonthlyType>(
//         value: selected,
//         items: [
//           dd.DropdownMenuItem(
//             value: MonthlyType.onExactDate,
//             child: Text(),
//           ),
//           dd.DropdownMenuItem(
//             value: MonthlyType.onSameDay,
//             child: Text(),
//           ),
//         ],
//       ),
//     );
//   }
// }

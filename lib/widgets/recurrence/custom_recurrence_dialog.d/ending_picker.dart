import 'package:flutter/material.dart';
import 'package:gap/gap.dart';
import 'package:go_router/go_router.dart';
import 'package:material_symbols_icons/symbols.dart';
import 'package:numberpicker/numberpicker.dart';

import '/strings/strings.dart';
import '/util/animated_dialog.dart';
import '/widgets/date_picker.dart';
import '/widgets/date_time_chips.dart';

import '../recurrence.dart';

enum EndingType { never, date, count }

class EndingPicker extends StatefulWidget {
  const EndingPicker({
    required this.selected,
    required this.onChanged,
    required this.padding,
    required this.labelStyle,
    required this.date,
    super.key,
  });

  final EndingType selected;
  final void Function(Ending ending) onChanged;
  final EdgeInsetsGeometry padding;
  final TextStyle labelStyle;
  final DateTime date;

  @override
  State<EndingPicker> createState() => _EndingPickerState();
}

class _EndingPickerState extends State<EndingPicker> {
  late var selected = widget.selected;
  int count = 1;
  late DateTime date;

  @override
  void initState() {
    super.initState();

    int month = widget.date.month + 1;
    if (month > DateTime.monthsPerYear) month = DateTime.january;

    date = widget.date.copyWith(month: month);
  }

  @override
  void didUpdateWidget(covariant EndingPicker oldWidget) {
    super.didUpdateWidget(oldWidget);

    if (widget.selected != selected) setState(() => selected = widget.selected);
  }

  @override
  Widget build(BuildContext context) {
    final scheme = Theme.of(context).colorScheme;
    final text = Theme.of(context).textTheme;
    final strings = Strings.of(context)!;

    final style = text.bodyLarge?.copyWith(color: scheme.onSurfaceVariant);
    final selectedStyle = text.bodyLarge?.copyWith(color: scheme.onSurface);

    final isNever = selected == EndingType.never;
    final isDate = selected == EndingType.date;
    final isCount = selected == EndingType.count;

    const divider = Divider(endIndent: 24, indent: 72);

    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsetsDirectional.fromSTEB(24, 24, 24, 12),
          child: Text(strings.rrEnds, style: widget.labelStyle),
        ),
        RadioListTile<EndingType>(
          groupValue: selected,
          value: EndingType.never,
          onChanged: (_) => widget.onChanged(const Never()),
          title: Text(strings.never, style: isNever ? selectedStyle : style),
        ),
        divider,
        RadioListTile<EndingType>(
          groupValue: selected,
          value: EndingType.date,
          onChanged: (_) => widget.onChanged(EndDate(date)),
          title: Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(
                strings.onFollowedByDate,
                style: isDate ? selectedStyle : style,
              ),
              const Gap(8),
              DateTimeChip(
                dateTime: date,
                useNames: false,
                backgroundColor: scheme.surfaceContainerHigh,
                foregroundColor: isDate ? selectedStyle?.color : null,
                selectTime: isDate
                    ? () => showAnimatedDatePicker(
                          context: context,
                          initialDate: date,
                          firstDate: date.copyWith(year: date.year - 100),
                          lastDate: date.copyWith(year: date.year + 100),
                        ).then((d) => setState(() => date = d ?? date))
                    : () => widget.onChanged(EndDate(date)),
              ),
            ],
          ),
        ),
        divider,
        RadioListTile<EndingType>(
          groupValue: selected,
          value: EndingType.count,
          onChanged: (_) => widget.onChanged(Count(count)),
          title: Row(
            children: <Widget>[
              Text(
                strings.rrAfterOccurrences,
                style: isCount ? selectedStyle : style,
              ),
              const Gap(8),
              InputChip(
                visualDensity: VisualDensity.compact,
                backgroundColor: scheme.surfaceContainerHigh,
                elevation: 0,
                deleteIcon: const Icon(Symbols.clear, size: 18),
                deleteIconColor: scheme.onSurfaceVariant,
                onPressed: isCount
                    ? () async {
                        count = await showAnimatedDialog<int>(
                              context: context,
                              builder: (context) => _NumberPickerDialog(count),
                            ) ??
                            count;

                        if (mounted) setState(() {});
                      }
                    : () => widget.onChanged(Count(count)),
                label: Text(count.toString()),
                labelStyle: text.labelLarge?.copyWith(
                  color:
                      isCount ? selectedStyle?.color : scheme.onSurfaceVariant,
                ),
              ),
              const Gap(8),
              if (count == 1)
                Text(
                  strings.occurrence,
                  style: isCount ? selectedStyle : style,
                )
              else
                Text(
                  strings.occurrences,
                  style: isCount ? selectedStyle : style,
                ),
            ],
          ),
        ),
      ],
    );
  }
}

class _NumberPickerDialog extends StatelessWidget {
  const _NumberPickerDialog(this.initialCount);

  final int initialCount;

  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context)!;
    final scheme = Theme.of(context).colorScheme;
    final text = Theme.of(context).textTheme;

    final notifier = ValueNotifier<int>(initialCount);

    return ValueListenableBuilder(
        valueListenable: notifier,
        builder: (context, count, _) {
          return AlertDialog(
            title: Text(strings.occurrences),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                const Divider(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    IconButton(
                      onPressed: count == 1
                          ? null
                          : () => notifier.value = (count - 1).clamp(1, 69420),
                      icon: const Icon(Symbols.remove),
                    ),
                    Expanded(
                      child: NumberPicker(
                        minValue: 1,
                        maxValue: 69420,
                        value: count,
                        haptics: true,
                        onChanged: (val) => notifier.value = val,
                        textStyle: text.bodyMedium!.copyWith(
                          color: scheme.onSurfaceVariant,
                        ),
                      ),
                    ),
                    IconButton(
                      onPressed: () => notifier.value = count + 1,
                      icon: const Icon(Symbols.add),
                    ),
                  ],
                ),
                const Divider(),
              ],
            ),
            actions: <Widget>[
              TextButton(
                onPressed: context.pop,
                child: Text(strings.cancel),
              ),
              TextButton(
                onPressed: () => context.pop(count),
                child: Text(strings.ok),
              ),
            ],
          );
        });
  }
}

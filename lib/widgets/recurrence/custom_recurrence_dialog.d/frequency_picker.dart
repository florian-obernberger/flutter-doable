import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gap/gap.dart';

import '/strings/strings.dart';
import '/util/build_text_selection_menu.dart';

import '../recurrence.dart';

class FrequencyPicker extends StatefulWidget {
  const FrequencyPicker({
    required this.padding,
    required this.size,
    required this.spacer,
    required this.labelStyle,
    required this.frequency,
    required this.updateFrequency,
    required this.interval,
    super.key,
  });

  final EdgeInsetsGeometry padding;
  final double size;
  final Widget spacer;
  final TextStyle labelStyle;
  final Frequency frequency;
  final void Function(Frequency? frequency) updateFrequency;
  final int interval;

  @override
  State<FrequencyPicker> createState() => _FrequencyPickerState();
}

class _FrequencyPickerState extends State<FrequencyPicker> {
  final intervalController = TextEditingController(text: '1');

  bool get isPlural => intervalController.text != '1';

  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context)!;

    final scheme = Theme.of(context).colorScheme;
    final text = Theme.of(context).textTheme;

    final dayStyle = text.bodyLarge;

    return Padding(
      padding: widget.padding,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Text(strings.rrRepeatsEvery, style: widget.labelStyle),
          widget.spacer,
          Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              SizedBox(
                height: widget.size,
                width: widget.size,
                child: TextField(
                  contextMenuBuilder: buildTextSelectionMenu,
                  onChanged: (_) => setState(() {}),
                  onSubmitted: (val) =>
                      val.isEmpty ? intervalController.text = '1' : null,
                  controller: intervalController,
                  textAlign: TextAlign.center,
                  style: dayStyle,
                  keyboardType: TextInputType.number,
                  inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                ),
              ),
              const Gap(12),
              SizedBox(
                height: widget.size,
                child: DropdownMenu<Frequency>(
                  key: ValueKey(isPlural),
                  textStyle: dayStyle?.copyWith(
                    color: scheme.onSurface,
                  ),
                  menuStyle: MenuStyle(
                    backgroundColor: WidgetStatePropertyAll(
                      scheme.surfaceContainerHigh,
                    ),
                  ),
                  initialSelection: widget.frequency,
                  onSelected: widget.updateFrequency,
                  dropdownMenuEntries: [
                    for (final freq in Frequency.values)
                      DropdownMenuEntry(
                        value: freq,
                        label: switch ((freq, isPlural)) {
                          (Frequency.daily, false) => strings.day,
                          (Frequency.daily, true) => strings.days,
                          (Frequency.weekly, false) => strings.week,
                          (Frequency.weekly, true) => strings.weeks,
                          (Frequency.monthly, false) => strings.month,
                          (Frequency.monthly, true) => strings.months,
                          (Frequency.yearly, false) => strings.year,
                          (Frequency.yearly, true) => strings.yearly,
                        },
                      )
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

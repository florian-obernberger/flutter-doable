import 'package:flutter/material.dart';

import '/strings/strings.dart';
import '/classes/motion.dart';

import '../recurrence.dart';

typedef Weekdays = List<(Weekday, String)>;

class WeekdayPicker extends StatelessWidget {
  const WeekdayPicker({
    required this.padding,
    required this.selected,
    required this.onSelect,
    required this.onDeselect,
    required this.spacer,
    required this.labelStyle,
    super.key,
  });

  final Set<Weekday> selected;
  final void Function(Weekday day) onSelect;
  final void Function(Weekday day) onDeselect;
  final EdgeInsetsGeometry padding;
  final Widget spacer;
  final TextStyle labelStyle;

  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context)!;

    final scheme = Theme.of(context).colorScheme;
    final dayStyle = Theme.of(context).textTheme.bodyLarge!;

    final mStrings = MaterialLocalizations.of(context);
    final first = mStrings.firstDayOfWeekIndex;
    final days = mStrings.narrowWeekdays;

    final Weekdays week = [];

    for (int offset = 0; offset <= 6; offset++) {
      var i = first + offset;
      if (i > 6) i -= 7;

      week.add((Weekday.fromIndex(i), days[i]));
    }

    return Padding(
      padding: padding,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Text(strings.rrRepeatsOn, style: labelStyle),
          spacer,
          SingleChildScrollView(
            physics: const ClampingScrollPhysics(),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                for (final (day, name) in week)
                  AnimatedSwitcher(
                    duration: Motion.short3,
                    child: IconButton.outlined(
                      key: ValueKey(selected),
                      isSelected: selected.contains(day),
                      icon: Text(name,
                          style: dayStyle.copyWith(
                            color: scheme.onSurface,
                          )),
                      onPressed: () {
                        if (selected.contains(day)) {
                          onDeselect(day);
                        } else {
                          onSelect(day);
                        }
                      },
                      selectedIcon: Text(name,
                          style: dayStyle.copyWith(
                            color: scheme.onPrimary,
                          )),
                    ),
                  )
              ],
            ),
          ),
        ],
      ),
    );
  }
}

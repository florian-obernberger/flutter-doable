import 'package:flutter/material.dart';
import 'package:custom_text/custom_text.dart';
import 'package:flutter/services.dart';
import 'package:url_launcher/url_launcher_string.dart';

import '/strings/strings.dart';
import '/state/settings.dart';
import '/data/todo.dart';
import '/widgets/link.dart';
import '/classes/system_info.dart';
import '/util/extensions/build_context.dart';

import 'markdown_text.d/markdown_block_quote.dart';
import 'markdown_text.d/markdown_bold.dart';
import 'markdown_text.d/markdown_bold_italic.dart';
import 'markdown_text.d/markdown_bold_underline.dart';
import 'markdown_text.d/markdown_bold_italic_underline.dart';
import 'markdown_text.d/markdown_code_block.dart';
import 'markdown_text.d/markdown_inline_code.dart';
import 'markdown_text.d/markdown_italic.dart';
import 'markdown_text.d/markdown_italic_underline.dart';
import 'markdown_text.d/markdown_link.dart';
import 'markdown_text.d/markdown_raw_link.dart';
import 'markdown_text.d/markdown_ol_list.dart';
import 'markdown_text.d/markdown_quote.dart';
import 'markdown_text.d/markdown_strike.dart';
import 'markdown_text.d/markdown_ul_list.dart';
import 'markdown_text.d/markdown_underline.dart';
import 'markdown_text.d/markdown_task_list.dart';
import 'markdown_text.d/markdown_highlight.dart';
import 'markdown_text.d/markdown_escape.dart';
import 'markdown_text.d/markdown_task_tags.dart';
import 'markdown_text.d/markdown_tasks.dart';
import 'markdown_text.d/markdown_hr.dart';
import 'markdown_text.d/codeberg_issue.dart';

enum MarkdownTapType { url, mail, tel }

class MarkdownText extends StatelessWidget {
  const MarkdownText(
    this.data, {
    this.todo,
    this.useTap = true,
    this.supportMarkdown = true,
    this.isTitle = false,
    this.maxLines,
    this.colorOpacity = 1,
    this.baseStyle,
    this.overflow,
    this.softWrap,
    this.textAlign,
    this.supportCodebergIssues = false,
    this.isLineBuilder = false,
    super.key,
  });

  final String data;
  final Todo? todo;
  final bool useTap;
  final bool supportMarkdown;
  final bool isTitle;
  final int? maxLines;
  final double colorOpacity;
  final TextStyle? baseStyle;
  final TextOverflow? overflow;
  final bool? softWrap;
  final TextAlign? textAlign;
  final bool supportCodebergIssues;
  final bool isLineBuilder;

  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context)!;
    final settings = Settings.of(context);
    final scheme = Theme.of(context).colorScheme;

    final defaultStyle = baseStyle ?? DefaultTextStyle.of(context).style;

    final monoStyle =
        settings.design.monoFontTheme.textTheme.bodyMedium!.copyWith(
      fontSize: defaultStyle.fontSize,
      height: defaultStyle.height,
      fontWeight: defaultStyle.fontWeight,
    );

    final linkStyle = defaultStyle.copyWith(
      decoration: TextDecoration.underline,
      color: scheme.primary,
      decorationColor: scheme.primary,
    );

    final linkTapStyle = defaultStyle.copyWith(
      decoration: TextDecoration.underline,
      backgroundColor: scheme.primaryContainer,
      color: scheme.onPrimaryContainer,
      decorationColor: scheme.onPrimaryContainer,
    );

    return DefaultTextStyle(
      style: defaultStyle,
      maxLines: maxLines,
      child: CustomText(
        data,
        textAlign: textAlign,
        definitions: [
          if (!isTitle) ...[
            SpanDefinition(
              matcher: const EmailMatcher(),
              builder: (text, groups) => linkBuilder(
                context,
                text,
                groups,
                MarkdownTapType.mail,
                defaultStyle,
                defaultStyle.fontSize,
              ),
            ),
            SpanDefinition(
              matcher: const UrlMatcher(),
              builder: (text, groups) => linkBuilder(
                context,
                text,
                groups,
                MarkdownTapType.url,
                defaultStyle,
                defaultStyle.fontSize,
              ),
            ),
            SpanDefinition(
              // matcher: const TelMatcher(
              //   r'\+?(?<!\d)(?:'
              //   r'(?:\+?[1-9]\d{0,4}[- ])?\d{1,4}[- ]?\d{3,4}[- ]?\d{3,4}'
              //   r'|\d{3,5}'
              //   r')(?!\d)',
              // ),
              matcher: const TelMatcher(
                r'((?:\+?\d{1,3}\s?)?(?:\d{3}[-.\s]?)?\(?\d{2,3}\)?[-.\s]?\d{3}[-.\s]?\d{4})',
              ),
              builder: (text, groups) => linkBuilder(
                context,
                text,
                groups,
                MarkdownTapType.tel,
                defaultStyle,
                defaultStyle.fontSize,
              ),
            ),
          ],
          if (supportCodebergIssues)
            CodebergIssueDefinition(
              onTap: useTap
                  ? (content) => onTap(context, MarkdownTapType.url, content)
                  : null,
              onLongPress:
                  useTap ? (content) => onLongPress(context, content) : null,
              matchStyle: linkStyle,
              tapStyle: linkTapStyle,
              hoverStyle: linkTapStyle,
            ),
          if (supportMarkdown) ...[
            if (!isTitle && !isLineBuilder)
              MdCodeBlockDefinition(
                style: monoStyle.copyWith(
                  color: scheme.onSecondaryContainer,
                ),
                onLongPress:
                    useTap ? (content) => onLongPress(context, content) : null,
                background: scheme.secondaryContainer,
              ),
            MdInlineCodeDefinition(
              style: monoStyle.copyWith(
                color: scheme.onSecondaryContainer,
              ),
              onLongPress:
                  useTap ? (content) => onLongPress(context, content) : null,
              background: scheme.secondaryContainer,
              maxLines: maxLines,
              softWrap: softWrap,
              overflow: overflow,
            ),
            MdEscapeDefinition(matchStyle: defaultStyle),
            MdHrDefinition(style: defaultStyle),
            MdTaskTagDefinition(
              style: defaultStyle,
              completedStyle: defaultStyle.copyWith(
                color: scheme.outline.withOpacity(colorOpacity),
                decorationColor: scheme.secondary.withOpacity(colorOpacity),
              ),
              lineBuilder: lineBuilder,
            ),
            MdTasksDefinition(
              style: defaultStyle,
              completedStyle: defaultStyle.copyWith(
                color: scheme.outline.withOpacity(colorOpacity),
                decorationColor: scheme.secondary.withOpacity(colorOpacity),
              ),
            ),
            if (!isTitle && !isLineBuilder)
              MdBlockQuoteDefinition(
                style: defaultStyle.copyWith(color: scheme.tertiary),
                lineBuilder: lineBuilder,
              ),
            MdQuoteDefinition(
              style: defaultStyle,
              strings: strings,
              lineBuilder: lineBuilder,
            ),
            if (!isTitle && !isLineBuilder) ...[
              MdTaskListDefinition(
                style: defaultStyle,
                completedStyle: defaultStyle.copyWith(
                  color: scheme.outline.withOpacity(colorOpacity),
                  decorationColor: scheme.secondary.withOpacity(colorOpacity),
                ),
                lineBuilder: lineBuilder,
              ),
              MdUlListDefinition(style: defaultStyle, lineBuilder: lineBuilder),
              MdOlListDefinition(style: defaultStyle, lineBuilder: lineBuilder),
            ],
            MdBoldItalicUnderlineDefinition(
              matchStyle: defaultStyle.copyWith(
                fontWeight: FontWeight.bold,
                fontStyle: FontStyle.italic,
                decoration: TextDecoration.underline,
              ),
            ),
            MdBoldUnderlineDefinition(
              matchStyle: defaultStyle.copyWith(
                fontWeight: FontWeight.bold,
                decoration: TextDecoration.underline,
              ),
            ),
            MdItalicUnderlineDefinition(
              matchStyle: defaultStyle.copyWith(
                fontStyle: FontStyle.italic,
                decoration: TextDecoration.underline,
              ),
            ),
            MdBoldItalicDefinition(
              matchStyle: defaultStyle.copyWith(
                fontWeight: FontWeight.bold,
                fontStyle: FontStyle.italic,
              ),
            ),
            MdBoldDefinition(
              matchStyle: defaultStyle.copyWith(fontWeight: FontWeight.bold),
            ),
            MdUnderlineDefinition(
              matchStyle: defaultStyle.copyWith(
                decoration: TextDecoration.underline,
              ),
            ),
            MdItalicDefinition(
              matchStyle: defaultStyle.copyWith(fontStyle: FontStyle.italic),
            ),
            MdHighlightDefinition(
              matchStyle: defaultStyle.copyWith(
                backgroundColor: scheme.primaryContainer,
                color: scheme.onPrimaryContainer,
              ),
            ),
            MdStrikeDefinition(
              matchStyle:
                  defaultStyle.copyWith(decoration: TextDecoration.lineThrough),
            ),
            if (!isTitle)
              MdRawLinkDefinition(
                onTap: useTap
                    ? (content) => onTap(context, MarkdownTapType.url, content)
                    : null,
                onLongPress:
                    useTap ? (content) => onLongPress(context, content) : null,
                matchStyle: linkStyle,
                tapStyle: linkTapStyle,
                hoverStyle: linkTapStyle,
              ),
            MdLinkDefinition(
              onTap: useTap
                  ? (content) => onTap(context, MarkdownTapType.url, content)
                  : null,
              onLongPress:
                  useTap ? (content) => onLongPress(context, content) : null,
              matchStyle: linkStyle,
              tapStyle: linkTapStyle,
              hoverStyle: linkTapStyle,
            ),
          ]
        ],
        maxLines: maxLines,
        style: defaultStyle,
        overflow: overflow ?? TextOverflow.fade,
        softWrap: softWrap ?? false,
      ),
    );
  }

  Widget lineBuilder(line, style) => MarkdownText(
        line,
        maxLines: maxLines,
        baseStyle: style,
        supportMarkdown: supportMarkdown,
        isTitle: isTitle,
        colorOpacity: colorOpacity,
        overflow: overflow,
        softWrap: softWrap,
        todo: todo,
        useTap: useTap,
        supportCodebergIssues: supportCodebergIssues,
        isLineBuilder: true,
      );

  void onTap(BuildContext context, MarkdownTapType type, String content) {
    switch (type) {
      case MarkdownTapType.mail:
        final subject = todo == null ? '' : '?subject=${todo!.title}';
        launchUrlString('mailto:$content$subject');
        return;
      case MarkdownTapType.tel:
        launchUrlString('tel:$content');
        return;
      case MarkdownTapType.url:
        context.launchUrl(content);
        return;
    }
  }

  void onLongPress(BuildContext context, String content) {
    HapticFeedback.mediumImpact();
    Clipboard.setData(ClipboardData(text: content));

    if (!SystemInfo.instance.hasClipboardPopup) {
      final strings = Strings.of(context)!;

      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          duration: const Duration(seconds: 2),
          dismissDirection: DismissDirection.vertical,
          margin: const EdgeInsets.fromLTRB(24, 8, 24, 18),
          content: Text(strings.copiedToClipboard),
        ),
      );
    }
  }

  InlineSpan linkBuilder(
    BuildContext context,
    String text,
    List<String?> groups,
    MarkdownTapType type,
    TextStyle defaultStyle,
    double? size,
  ) {
    final Widget child;

    switch (type) {
      case MarkdownTapType.url:
        child = Link(
          url: text,
          text: text.replaceAll(RegExp(r'https?://'), ''),
          baseStyle: defaultStyle,
          maxLines: maxLines,
          showIcon: true,
          isClickable: useTap,
        );
        break;
      case MarkdownTapType.mail:
        child = Mail(
          email: text,
          baseStyle: defaultStyle,
          maxLines: maxLines,
          showIcon: true,
          isClickable: useTap,
        );
        break;
      case MarkdownTapType.tel:
        child = Tel(
          number: text,
          baseStyle: defaultStyle,
          maxLines: maxLines,
          showIcon: true,
          isClickable: useTap,
        );
        break;
    }

    return WidgetSpan(child: child);
  }
}

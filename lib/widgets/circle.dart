import 'package:flutter/material.dart';

class Circle extends Container {
  Circle({
    super.key,
    super.alignment,
    super.padding,
    Color? color,
    BoxDecoration? decoration,
    super.foregroundDecoration,
    double? diameter,
    super.constraints,
    super.margin,
    super.transform,
    super.transformAlignment,
    super.child,
    super.clipBehavior = Clip.none,
  }) : super(
          height: diameter,
          width: diameter,
          decoration: decoration ??
              BoxDecoration(
                color: color,
                shape: BoxShape.circle,
              ),
        );
}

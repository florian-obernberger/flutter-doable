import 'package:flutter/material.dart';

import 'back_button.dart' as back;
import 'close_button.dart' as close;

enum NavButtonType { back, close }

class NavButton extends StatelessWidget {
  const NavButton({
    required this.type,
    this.onNav,
    this.color,
    this.tooltip,
    super.key,
  });

  const NavButton.close({this.onNav, this.color, this.tooltip, super.key})
      : type = NavButtonType.close;

  const NavButton.back({this.onNav, this.color, this.tooltip, super.key})
      : type = NavButtonType.back;

  final NavButtonType type;
  final VoidCallback? onNav;
  final String? tooltip;
  final Color? color;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: switch (type) {
        NavButtonType.back => back.BackButton(
            onBack: onNav,
            tooltip: tooltip,
            color: color,
          ),
        NavButtonType.close => close.CloseButton(
            onClose: onNav,
            tooltip: tooltip,
            color: color,
          ),
      },
    );
  }
}

import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:material_symbols_icons/symbols.dart';
import 'package:rrule/rrule.dart';

import '/strings/strings.dart';
import '/state/settings.dart';
import '/data/todo_list.dart';
import '/widgets/mini_chip.dart';
import '/util/extensions/color.dart';
import '/util/formatters/format_date.dart';

export 'package:rrule/rrule.dart' show Frequency;

typedef TodoChipColors = ({
  Color? foreground,
  Color? background,
  Color? deleteIcon,
});

typedef TodoChipColorsFull = ({
  Color foreground,
  Color background,
  Color deleteIcon,
});

sealed class TodoChip<T> extends StatelessWidget {
  final T data;
  final VoidCallback? onDelete;

  const TodoChip(
    this.data, {
    this.onDelete,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context)!;
    final theme = Theme.of(context);
    final scheme = theme.colorScheme;

    final (:foreground, :background, :deleteIcon) = _colors(scheme);

    final icon = _icon();
    final avatar = _avatar();

    final text = _advancedText(context) ?? _text(strings);

    final constraints = _constraints();
    final border = _border(scheme);

    if (avatar != null) {
      return MiniChip.avatar(
        text: text,
        avatar: avatar,
        onTap: _onTap,
        rounded: false,
        foreground: foreground,
        background: background,
        deleteIconColor: deleteIcon,
        onDelete: onDelete,
        constraints: constraints,
        border: border,
      );
    }

    if (icon != null) {
      return MiniChip.icon(
        text: text,
        icon: icon,
        onTap: _onTap,
        foreground: foreground,
        background: background,
        deleteIconColor: deleteIcon,
        onDelete: onDelete,
        constraints: constraints,
        border: border,
      );
    }

    return MiniChip(
      text: text,
      onTap: _onTap,
      foreground: foreground,
      background: background,
      deleteIconColor: deleteIcon,
      onDelete: onDelete,
      constraints: constraints,
      border: border,
    );
  }

  String _text(Strings strings);
  String? _advancedText(BuildContext context) => null;

  Widget? _icon();
  ImageProvider? _avatar() => null;

  TodoChipColors _colors(ColorScheme scheme);

  BoxConstraints _constraints() => const BoxConstraints();

  BorderSide? _border(ColorScheme scheme) => null;

  GestureTapCallback? get _onTap => null;
}

class ListChip extends TodoChip<TodoList> {
  const ListChip(
    super.data, {
    super.onDelete,
    super.key,
  });

  @override
  String _text(Strings strings) => data.name;

  @override
  Null _icon() => null;

  @override
  TodoChipColors _colors(ColorScheme scheme) {
    final listScheme = data.color?.listSchemeFromBrightness(scheme.brightness);

    return (
      foreground: listScheme?.onPrimary,
      background: listScheme?.primary,
      deleteIcon: listScheme?.onPrimaryFixedVariant,
    );
  }

  @override
  BoxConstraints _constraints() => const BoxConstraints(maxWidth: 96);
}

class RecurrenceChip extends TodoChip<Frequency> {
  const RecurrenceChip(
    super.data, {
    super.onDelete,
    super.key,
  });

  @override
  String _text(Strings strings) => switch (data) {
        Frequency.secondly => strings.secondly,
        Frequency.minutely => strings.minutely,
        Frequency.hourly => strings.hourly,
        Frequency.daily => strings.daily,
        Frequency.weekly => strings.weekly,
        Frequency.monthly => strings.monthly,
        Frequency.yearly => strings.yearly,
        _ => '',
      };

  @override
  Widget _icon() => const Icon(Symbols.repeat, weight: 300);

  @override
  TodoChipColorsFull _colors(ColorScheme scheme) => colors(scheme);

  static TodoChipColorsFull colors(ColorScheme scheme) => (
        foreground: scheme.onTertiary,
        background: scheme.tertiary,
        deleteIcon: scheme.onTertiaryFixedVariant,
      );
}

class ImageChip extends TodoChip<Uint8List?> {
  const ImageChip({
    super.onDelete,
    super.key,
  }) : super(null);

  const ImageChip.avatar(
    Uint8List super.data, {
    super.onDelete,
    super.key,
  });

  @override
  String _text(Strings strings) => strings.image;

  @override
  Widget _icon() => const Icon(Symbols.wallpaper, weight: 300);

  @override
  ImageProvider? _avatar() => data != null ? Image.memory(data!).image : null;

  @override
  TodoChipColorsFull _colors(ColorScheme scheme) => colors(scheme);

  static TodoChipColorsFull colors(ColorScheme scheme) => (
        foreground: scheme.onTertiary,
        background: scheme.tertiary,
        deleteIcon: scheme.onTertiaryFixedVariant,
      );
}

class DueDateChip extends TodoChip<DateTime> {
  static const defaultSeparator = ', ';

  final bool showTime;
  final bool useNames;
  final String separator;

  final bool outlined;

  final GestureTapCallback? onTap;

  const DueDateChip(
    super.date, {
    this.onTap,
    this.showTime = false,
    this.separator = defaultSeparator,
    this.useNames = true,
    super.onDelete,
    super.key,
  }) : outlined = false;

  const DueDateChip.outlined(
    super.date, {
    this.onTap,
    this.showTime = false,
    this.separator = defaultSeparator,
    this.useNames = true,
    super.onDelete,
    super.key,
  }) : outlined = true;

  @override
  String _text(Strings strings) => strings.dueDate;

  @override
  String _advancedText(BuildContext context) {
    final strings = Strings.of(context)!;
    final prefs = Settings.of(context).dateTime;
    final locale = Localizations.localeOf(context).countryCode;

    return showTime
        ? formatDateTime(
            prefs,
            strings,
            data,
            locale: locale,
            separator: separator,
            useNames: useNames,
          )
        : formatDate(prefs, strings, data, locale, useNames);
  }

  @override
  Widget _icon() => const Icon(Symbols.calendar_clock, weight: 300);

  @override
  TodoChipColorsFull _colors(ColorScheme scheme) =>
      colors(scheme, outlined: outlined);

  static TodoChipColorsFull colors(ColorScheme scheme,
          {required bool outlined}) =>
      (
        foreground: outlined ? scheme.onSurface : scheme.onSecondary,
        background: outlined ? Colors.transparent : scheme.secondary,
        deleteIcon:
            outlined ? scheme.onSurfaceVariant : scheme.onSecondaryFixedVariant,
      );

  @override
  BorderSide? _border(ColorScheme scheme) =>
      outlined ? BorderSide(color: scheme.outline) : null;

  @override
  GestureTapCallback? get _onTap => onTap;
}

import 'dart:async';
import 'dart:io';

import 'package:android_system_font/android_system_font.dart';
import 'package:flex_seed_scheme/flex_seed_scheme.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:dynamic_color/dynamic_color.dart';
import 'package:flutter_displaymode/flutter_displaymode.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_timezone/flutter_timezone.dart';
import 'package:go_router/go_router.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hive_flutter/adapters.dart';
import 'package:intl/intl.dart';
import 'package:l10n_esperanto/l10n_esperanto.dart';
import 'package:material_symbols_icons/symbols.dart';
import 'package:timezone/data/latest_all.dart' as tz;
import 'package:timezone/timezone.dart' as tz;
import 'package:path/path.dart' as path;

import '/strings/strings.dart';
import '/state/router.dart';
import '/state/settings.dart';
import '/state/minimal_state.dart';
import '/state/todo_list_db.dart';
import '/state/todo_notif_response.dart';
import '/classes/logger.dart';
import '/data/base_notification.dart';
import '/data/notifications/todo_notification.dart';
import '/data/todo.dart';
import '/data/hive_icon.dart';
import '/data/todo_list.dart';
import '/data/applications_directory.dart';
import '/data/doable_theme_mode.dart';
import '/theme/color_schemes.dart';
import '/theme/slider_theme.dart';
import '/theme/rainbow_colors.dart';
import '/classes/system_info.dart';
import '/util/extensions/color.dart';
import '/util/extensions/date_time.dart';
import '/util/migration/sorting.dart';
import '/util/adapters/tz_date_time.dart';

import 'pages/settings_view.d/backup_and_sync_view.d/import_export.d/import_export.dart';

Future<void> onDidReceiveNotification(
  NotificationResponse response, {
  bool isInitial = false,
}) async {
  await initMinimalHive();
  final logger = await Logger.init(defaultTag: 'Notifications');

  logger.log(LogLevel.debug, message: 'Received response');

  final todoId = response.payload;
  if (todoId == null) {
    logger.i('Received response without todoId');
    return;
  }

  final action = TodoNotificationAction.fromId(response.actionId);

  final notification = action == null
      ? OpenTodoNotifResponse(todoId, response.id!)
      : switch (action) {
          TodoNotificationAction.complete =>
            CompleteTodoNotifResponse(todoId, response.id!),
          TodoNotificationAction.snooze =>
            SnoozeTodoNotifResponse(todoId, response.id!),
        };

  if (isInitial) {
    initialNotification = notification;
  } else {
    todoNotificationsStream.add(notification);
  }
}

@pragma('vm:entry-point')
Future<void> onDidReceiveBackgroundNotificationResponse(
  NotificationResponse response,
) async {
  await initMinimalHive();
  final logger = await Logger.init(defaultTag: 'Notifications');

  logger.log(LogLevel.debug, message: 'Received background response');

  final todoId = response.payload;
  if (todoId == null) {
    logger.i('Received response without todoId');
    return;
  }

  final action = TodoNotificationAction.fromId(response.actionId);
  if (action == null) {
    logger.i('Received response without valid actionId', more: {
      'actionId': response.actionId.toString(),
    });
    return;
  }

  final todoDb = await initMinimalTodoDb();

  final todo = todoDb.get(todoId);
  if (todo == null) {
    logger.i('Received response with non-existing todoId', more: {
      'todoId': todoId,
    });
    return;
  }

  switch (action) {
    case TodoNotificationAction.complete:
      todo.complete();
      todo.notificationMetadata
          ?.map((meta) => meta.id.value)
          .forEach(BaseNotification.cancelId);

      await todoDb.store(todo);
    case TodoNotificationAction.snooze:
      break;
  }

  todoDb.dispose();
  logger.dispose();
  await Hive.close();
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await Hive.initFlutter();
  Hive.registerAdapter(TimeOfDayAdapter());
  Hive.registerAdapter(ColorAdapter());

  // TZDateTime

  Hive.registerAdapter(TimeZoneAdapter());
  Hive.registerAdapter(LocationAdapter());
  Hive.registerAdapter(TZDateTimeAdapter());

  // Notifications

  Hive.registerAdapter(NotifIdAdapter());
  Hive.registerAdapter(TodoNotifMetaAdapter());
  // Hive.registerAdapter(NotifMetaAdapter());

  // Data

  Hive.registerAdapter(TodoAdapter());
  Hive.registerAdapter(HiveIconAdapter());
  Hive.registerAdapter(TodoListAdapter());

  // Logs

  Hive.registerAdapter(LogEntryAdapter());
  Hive.registerAdapter(LogLevelAdapter());

  GoogleFonts.config.allowRuntimeFetching = kDebugMode;

  final settings = Settings();
  await settings.initialize();
  settings.initializePrivateSettings();

  // Sorting migration

  // ignore: deprecated_member_use_from_same_package
  if (!await SortOrderPreferences.isDeleted()) {
    // ignore: deprecated_member_use_from_same_package
    final sortOrder = SortOrderPreferences();
    await sortOrder.initialize();

    final (:groupBy, :sortBy) = migrateSorting(sortOrder);
    settings.sorting.groupBy.value = groupBy;
    settings.sorting.sortBy.value = sortBy;

    // ignore: deprecated_member_use_from_same_package
    await SortOrderPreferences.delete();
  }

  await initApplicationsDirectory();

  await SystemInfo.instance.init();

  // Display mode

  if (settings.accessability.forceHighestRefreshRate.value) {
    await FlutterDisplayMode.setHighRefreshRate();
  }

  // Error management

  await initLogger();

  FlutterError.onError = (details) {
    FlutterError.presentError(details);

    logger.e(
      details.exception,
      tag: 'Flutter',
      stackTrace: details.stack,
      message: 'Uncaught Error',
    );
  };

  // Notifications

  tz.initializeTimeZones();
  tz.setLocalLocation(tz.getLocation(await FlutterTimezone.getLocalTimezone()));

  const initializationSettings = InitializationSettings(
    android: AndroidInitializationSettings('ic_notification_todos'),
  );

  final notifyPlugin = FlutterLocalNotificationsPlugin();

  await notifyPlugin.initialize(
    initializationSettings,
    onDidReceiveNotificationResponse: onDidReceiveNotification,
    onDidReceiveBackgroundNotificationResponse:
        onDidReceiveBackgroundNotificationResponse,
  );

  await notifyPlugin.getNotificationAppLaunchDetails().then((details) {
    if (details == null || !details.didNotificationLaunchApp) return;
    if (details.notificationResponse != null) {
      final response = details.notificationResponse!;

      onDidReceiveNotification(response, isInitial: true);
    }
  });

  runApp(SettingsProvider(
    settings: settings,
    child: const ProviderScope(child: Doable()),
  ));
}

class Doable extends ConsumerStatefulWidget {
  const Doable({super.key});

  @override
  ConsumerState<Doable> createState() => _DoableState();
}

class _DoableState extends ConsumerState<Doable> {
  final androidInfo = SystemInfo.instance;

  Settings? _settings;
  Settings get settings => _settings!;

  late final GoRouter router;

  bool _canUseListColor = false;

  late final Timer _exportTimer;

  bool _systemFontLoaded = false;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    if (_settings == null) {
      _settings = Settings.of(context);
      router = getRouter(settings);

      ref.read(todoListDatabaseProvider).init().then(
          (_) => mounted ? setState(() => _canUseListColor = true) : null);

      _exportTimer = Timer.periodic(const Duration(minutes: 1), autoExport);

      if (settings.design.useSystemFont.value) systemFontListener();

      settings.general.appLanguage.addListener(listener);
      settings.extensions.useListColor.addListener(listener);
      settings.extensions.lastTodoList.addListener(listener);
      settings.design.dynamicColors.addListener(listener);
      settings.design.themeMode.addListener(listener);
      settings.design.customAccentColor.addListener(listener);
      settings.design.customSecondaryAccentColor.addListener(listener);
      settings.design.customTertiaryAccentColor.addListener(listener);
      settings.design.enableCustomColorScheme.addListener(listener);
      settings.design.customColorSchemes.addListener(listener);
      settings.design.fontPairing.addListener(listener);
      settings.design.useSystemFont.addListener(systemFontListener);
      settings.accessability.addListener(listener);
    }
  }

  @override
  void dispose() {
    settings.general.appLanguage.removeListener(listener);
    settings.extensions.useListColor.removeListener(listener);
    settings.extensions.lastTodoList.removeListener(listener);
    settings.design.dynamicColors.removeListener(listener);
    settings.design.themeMode.removeListener(listener);
    settings.design.customAccentColor.removeListener(listener);
    settings.design.customSecondaryAccentColor.removeListener(listener);
    settings.design.customTertiaryAccentColor.removeListener(listener);
    settings.design.enableCustomColorScheme.removeListener(listener);
    settings.design.customColorSchemes.removeListener(listener);
    settings.design.fontPairing.removeListener(listener);
    settings.design.useSystemFont.addListener(systemFontListener);
    settings.accessability.removeListener(listener);

    _exportTimer.cancel();

    super.dispose();
  }

  void listener() => setState(() {});

  static Future<ByteData> _readFileBytes(String path) async {
    var bytes = await File(path).readAsBytes();
    return ByteData.view(bytes.buffer);
  }

  Future<void> systemFontListener() async {
    if (_systemFontLoaded) return setState(() {});

    final fontLoader = FontLoader('SystemFont');

    final fontFilePath = await AndroidSystemFont().getFilePath();
    if (fontFilePath == null) return;

    fontLoader.addFont(_readFileBytes(fontFilePath));
    await fontLoader.load();

    setState(() => _systemFontLoaded = true);

    const loggerTag = 'System font';

    final shouldLog = logger.getTag(loggerTag).latest.isSomeAnd((entry) {
      return entry.logTime <= DateTime.now() - const Duration(days: 3);
    });

    if (shouldLog) {
      logger.i('System font is ${path.basename(fontFilePath)}', tag: loggerTag);
    }
  }

  void autoExport(Timer timer) async {
    final backup = _settings!.backup;
    if (!backup.autoExport) return;

    final now = DateTime.now();
    final last = backup.lastAutoExport.value;

    if (last == null || last.add(backup.autoExportDuration.value) < now) {
      unawaited(exportData(
        ref,
        const {
          ExportData.todos,
          ExportData.lists,
          ExportData.settings,
        },
        directory: _settings!.backup.autoExportPath.value,
      ));

      backup.lastAutoExport.value = now;
    }
  }

  @override
  Widget build(BuildContext context) {
    return DynamicColorBuilder(
      builder: (lightDynamic, darkDynamic) {
        final useSystemColors = settings.design.dynamicColors.value;
        final doableThemeMode = settings.design.themeMode.value;
        final customAccent = settings.design.customAccentColor.value;
        final customSecondaryAccent =
            settings.design.customSecondaryAccentColor.value;
        final customTertiaryAccent =
            settings.design.customTertiaryAccentColor.value;
        final useCustomColorScheme =
            settings.design.enableCustomColorScheme.value;
        final customColorSchemes = settings.design.customColorSchemes.value;

        ColorScheme lightScheme;
        ColorScheme darkScheme;

        if (useSystemColors && androidInfo.supportsDynamicColor) {
          lightScheme = lightDynamic ?? lightColorScheme;
          darkScheme = darkDynamic ?? darkColorScheme;
        } else if (useCustomColorScheme && customColorSchemes != null) {
          lightScheme = customColorSchemes.lightScheme;
          darkScheme = customColorSchemes.darkScheme;
        } else if (customAccent != null) {
          lightScheme = SeedColorScheme.fromSeeds(
            primaryKey: customAccent,
            secondaryKey: customSecondaryAccent,
            tertiaryKey: customTertiaryAccent,
            brightness: Brightness.light,
            tones: const FlexTones.light(),
          );
          darkScheme = SeedColorScheme.fromSeeds(
            primaryKey: customAccent,
            secondaryKey: customSecondaryAccent,
            tertiaryKey: customTertiaryAccent,
            brightness: Brightness.dark,
            tones: const FlexTones.dark(),
          );
        } else {
          lightScheme = lightColorScheme;
          darkScheme = darkColorScheme;
        }

        if (_canUseListColor &&
            settings.extensions.enableLists.value &&
            settings.extensions.useListColor.value &&
            settings.extensions.lastTodoList.value != null) {
          final listColor = ref
              .read(todoListDatabaseProvider)
              .get(settings.extensions.lastTodoList.value!)
              ?.color;

          if (listColor != null) {
            (
              lightScheme: lightScheme,
              darkScheme: darkScheme,
            ) = listColor.listSchemes();
          }
        }

        darkScheme = darkScheme.applyDarkMode(doableThemeMode.darkMode);

        lightScheme = fixSurfaceContainers(lightScheme);
        darkScheme = fixSurfaceContainers(darkScheme);

        final currentLocale = settings.general.appLanguage.value.locale;
        Intl.defaultLocale = currentLocale?.toLanguageTag();

        final mediaQuery = MediaQuery.of(context);

        final bool isDarkMode;
        if (doableThemeMode.themeMode == ThemeMode.system) {
          isDarkMode = mediaQuery.platformBrightness == Brightness.dark;
        } else {
          isDarkMode = doableThemeMode.isDark;
        }

        return MediaQuery(
          data: mediaQuery.copyWith(
            accessibleNavigation: false,
            alwaysUse24HourFormat: settings.dateTime.timeFormat.value.is24hour,
            disableAnimations: settings.accessability.reduceMotion.value,
            textScaler: settings.accessability.textScaler,
          ),
          child: MaterialApp.router(
            color: isDarkMode ? darkScheme.surface : lightScheme.surface,
            routerConfig: router,
            locale: currentLocale,
            themeMode: doableThemeMode.themeMode,
            debugShowCheckedModeBanner: false,
            title: 'Doable',
            onGenerateTitle: (context) => Strings.of(context)!.appTitle,
            localizationsDelegates: const [
              ...Strings.localizationsDelegates,
              MaterialLocalizationsEo.delegate,
              CupertinoLocalizationsEo.delegate,
            ],
            localeListResolutionCallback: (locales, supportedLocalesIter) {
              if (locales == null) return const Locale('en');

              final supportedLocales = supportedLocalesIter.toList();

              for (var locale in locales) {
                if (supportedLocales.any((supLocale) =>
                    supLocale.languageCode == locale.languageCode)) {
                  return Locale(locale.languageCode);
                }
              }

              return const Locale('en');
            },
            supportedLocales: Strings.supportedLocales,
            theme: theme(lightScheme, darkScheme),
            darkTheme: theme(darkScheme, lightScheme),
          ),
        );
      },
    );
  }

  ColorScheme fixSurfaceContainers(ColorScheme scheme) {
    final surface = scheme.surface;
    final tint = scheme.surfaceTint;

    var surfaceContainerLowest = scheme.surfaceContainerLowest;
    var surfaceContainerLow = scheme.surfaceContainerLow;
    var surfaceContainer = scheme.surfaceContainer;
    var surfaceContainerHigh = scheme.surfaceContainerHigh;
    var surfaceContainerHighest = scheme.surfaceContainerHighest;

    if (surfaceContainerLowest == surface) {
      surfaceContainerLowest = switch (scheme.brightness) {
        Brightness.light => Colors.white,
        Brightness.dark => Color.alphaBlend(
            Colors.black45,
            scheme.surfaceDim,
          ),
      };
    }
    if (surfaceContainerLow == surface) {
      surfaceContainerLow = ElevationOverlay.applySurfaceTint(surface, tint, 1);
    }
    if (surfaceContainer == surface) {
      surfaceContainer = ElevationOverlay.applySurfaceTint(surface, tint, 2);
    }
    if (surfaceContainerHigh == surface) {
      surfaceContainerHigh = ElevationOverlay.applySurfaceTint(
        surface,
        tint,
        3,
      );
    }
    if (surfaceContainerHighest == surface) {
      // ignore: deprecated_member_use
      surfaceContainerHighest = scheme.surfaceVariant;
    }

    return scheme.copyWith(
      surfaceContainerLowest: surfaceContainerLowest,
      surfaceContainerLow: surfaceContainerLow,
      surfaceContainer: surfaceContainer,
      surfaceContainerHigh: surfaceContainerHigh,
      surfaceContainerHighest: surfaceContainerHighest,
    );
  }

  ThemeData theme(ColorScheme scheme, ColorScheme inverseScheme) {
    final text = settings.design.primaryFontTheme.textTheme;

    return ThemeData(
      extensions: [
        scheme.brightness == Brightness.light
            ? lightRainbowColors
            : darkRainbowColors
      ],
      fontFamilyFallback: const ['Noto'],
      useMaterial3: true,
      listTileTheme: ListTileThemeData(
        titleTextStyle: text.bodyLarge!.copyWith(color: scheme.onSurface),
        subtitleTextStyle: text.bodyMedium!.copyWith(
          color: scheme.onSurfaceVariant,
        ),
      ),
      bottomSheetTheme: BottomSheetThemeData(
        dragHandleColor: scheme.onSurfaceVariant,
        constraints: const BoxConstraints(maxWidth: 640),
      ),
      colorScheme: scheme,
      textTheme: text,
      cardColor: scheme.surface,
      scaffoldBackgroundColor: scheme.surface,
      splashFactory: InkSparkle.splashFactory,
      dialogBackgroundColor: scheme.surface,
      unselectedWidgetColor: scheme.onSurface,
      canvasColor: scheme.surface,
      tooltipTheme: TooltipThemeData(
        decoration: BoxDecoration(
          color: scheme.inverseSurface,
          borderRadius: BorderRadius.circular(4),
        ),
        textStyle: text.bodySmall!.copyWith(color: scheme.onInverseSurface),
        height: 24,
        showDuration: const Duration(milliseconds: 1500), // 1.5 s
        waitDuration: const Duration(milliseconds: 500),
        padding: const EdgeInsets.symmetric(horizontal: 8),
      ),
      iconTheme: IconThemeData(
        weight: 500,
        opticalSize: 48,
        color: scheme.onSurfaceVariant,
      ),
      appBarTheme: AppBarTheme(
        toolbarHeight: 64,
        titleSpacing: 8,
        scrolledUnderElevation: 2,
        systemOverlayStyle: systemOverlayStyle(scheme),
        actionsIconTheme: IconThemeData(color: scheme.onSurfaceVariant),
        iconTheme: IconThemeData(color: scheme.onSurface),
      ),
      floatingActionButtonTheme: FloatingActionButtonThemeData(
        extendedTextStyle: text.labelLarge!.copyWith(
          fontWeight: FontWeight.w500,
        ),
        foregroundColor: scheme.secondaryContainer,
        backgroundColor: scheme.onSecondaryContainer,
      ),
      snackBarTheme: SnackBarThemeData(
        backgroundColor: scheme.inverseSurface,
        actionTextColor: scheme.inversePrimary,
        contentTextStyle: text.bodyMedium?.copyWith(
          color: scheme.onInverseSurface,
        ),
        behavior: SnackBarBehavior.floating,
        elevation: 1,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
      ),
      dividerTheme: DividerThemeData(
        color: inverseScheme.onSurfaceVariant,
        endIndent: 0,
        indent: 0,
        space: 8,
        thickness: 1,
      ),
      chipTheme: ChipThemeData(
        showCheckmark: false,
        // elevation: 1,
        padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 7),
        brightness: scheme.brightness,
        backgroundColor: scheme.surface,
        surfaceTintColor: scheme.surfaceTint,
        labelStyle: text.labelLarge?.copyWith(color: scheme.onSurface),
        selectedColor: scheme.secondaryContainer,
      ),
      inputDecorationTheme: InputDecorationTheme(
        border: const OutlineInputBorder(),
        labelStyle: text.bodyLarge?.copyWith(
          color: scheme.onSurfaceVariant,
        ),
        floatingLabelStyle: text.bodyMedium,
        filled: false,
      ),
      dialogTheme: DialogTheme(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(28)),
        backgroundColor: scheme.surfaceContainerHigh,
        elevation: 0,
        titleTextStyle: text.headlineSmall?.copyWith(color: scheme.onSurface),
        contentTextStyle: text.bodyMedium?.copyWith(
          color: scheme.onSurfaceVariant,
        ),
      ),
      sliderTheme: m3SliderTheme(scheme, inverseScheme, text),
      datePickerTheme: DatePickerThemeData(
        backgroundColor: scheme.surfaceContainerHigh,
        rangePickerBackgroundColor: scheme.surfaceContainerHigh,
        elevation: 0,
      ),
      bottomAppBarTheme: BottomAppBarTheme(
        color: scheme.surfaceContainer,
        elevation: 0,
      ),
      timePickerTheme: TimePickerThemeData(
        backgroundColor: scheme.surfaceContainerHigh,
        elevation: 0,
        dialBackgroundColor: scheme.surfaceContainerHighest,
        dialTextColor: WidgetStateColor.resolveWith((states) {
          if (states.contains(WidgetState.selected)) return scheme.onPrimary;
          return scheme.onSurface;
        }),
      ),
      filledButtonTheme: const FilledButtonThemeData(
        style: ButtonStyle(iconSize: WidgetStatePropertyAll(18)),
      ),
      outlinedButtonTheme: const OutlinedButtonThemeData(
        style: ButtonStyle(iconSize: WidgetStatePropertyAll(18)),
      ),
      elevatedButtonTheme: const ElevatedButtonThemeData(
        style: ButtonStyle(iconSize: WidgetStatePropertyAll(18)),
      ),
      textButtonTheme: const TextButtonThemeData(
        style: ButtonStyle(iconSize: WidgetStatePropertyAll(18)),
      ),
      popupMenuTheme: PopupMenuThemeData(
        color: ElevationOverlay.applySurfaceTint(
          scheme.surface,
          scheme.surfaceTint,
          1,
        ),
        textStyle: text.labelLarge?.copyWith(color: scheme.onSurface),
        elevation: 2,
        position: PopupMenuPosition.under,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12),
          side: BorderSide(color: scheme.outlineVariant),
        ),
      ),
      switchTheme: SwitchThemeData(
        thumbIcon: WidgetStateProperty.resolveWith<Icon?>((states) {
          if (states.contains(WidgetState.selected)) {
            return const Icon(Symbols.check);
          } else {
            return const Icon(Symbols.close);
          }
        }),
      ),
      segmentedButtonTheme: SegmentedButtonThemeData(
        style: ButtonStyle(
          backgroundColor: WidgetStateProperty.resolveWith((states) {
            if (states.contains(WidgetState.selected)) {
              return scheme.secondaryContainer;
            } else {
              return Colors.transparent;
            }
          }),
          foregroundColor: WidgetStateProperty.resolveWith((states) {
            if (states.contains(WidgetState.selected)) {
              return scheme.onSecondaryContainer;
            } else {
              return scheme.onSurface;
            }
          }),
          minimumSize: const WidgetStatePropertyAll(Size(48, 40)),
        ),
      ),
      progressIndicatorTheme: ProgressIndicatorThemeData(
        linearMinHeight: 4,
        linearTrackColor: scheme.surfaceContainerHighest,
        circularTrackColor: scheme.surfaceContainerHighest,
        color: scheme.primary,
      ),
      scrollbarTheme: ScrollbarThemeData(
        thickness: WidgetStateProperty.all(4),
        radius: const Radius.circular(24),
        mainAxisMargin: 16,
        crossAxisMargin: 6,
      ),
    );
  }

  SystemUiOverlayStyle systemOverlayStyle(ColorScheme scheme) {
    final oreoLight = SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
      statusBarIconBrightness: Brightness.dark,
      systemNavigationBarColor: scheme.surface,
      systemNavigationBarIconBrightness: Brightness.dark,
    );

    final oreoDark = SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
      statusBarIconBrightness: Brightness.light,
      systemNavigationBarColor: scheme.surface,
      systemNavigationBarIconBrightness: Brightness.light,
    );

    const light = SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
      statusBarIconBrightness: Brightness.dark,
      systemNavigationBarColor: Colors.transparent,
      systemNavigationBarIconBrightness: Brightness.dark,
    );

    const dark = SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
      statusBarIconBrightness: Brightness.light,
      systemNavigationBarColor: Colors.transparent,
      systemNavigationBarIconBrightness: Brightness.light,
    );

    return switch (scheme.brightness) {
      Brightness.light when androidInfo.supportsDarkMode => light,
      Brightness.dark when androidInfo.supportsDarkMode => dark,
      Brightness.light => oreoLight,
      Brightness.dark => oreoDark,
    };
  }
}

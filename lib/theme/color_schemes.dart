// ignore_for_file: deprecated_member_use

import 'package:flutter/material.dart';

const lightColorScheme = ColorScheme(
  brightness: Brightness.light,
  primary: Color(0xff904a4b),
  surfaceTint: Color(0xff904a4b),
  onPrimary: Color(0xffffffff),
  primaryContainer: Color(0xffffdad9),
  onPrimaryContainer: Color(0xff3b080d),
  secondary: Color(0xff775656),
  onSecondary: Color(0xffffffff),
  secondaryContainer: Color(0xffffdad9),
  onSecondaryContainer: Color(0xff2c1515),
  tertiary: Color(0xff755a2f),
  onTertiary: Color(0xffffffff),
  tertiaryContainer: Color(0xffffdeae),
  onTertiaryContainer: Color(0xff281900),
  error: Color(0xffba1a1a),
  onError: Color(0xffffffff),
  errorContainer: Color(0xffffdad6),
  onErrorContainer: Color(0xff410002),
  background: Color(0xfffff8f7),
  onBackground: Color(0xff221919),
  surface: Color(0xfffff8f7),
  onSurface: Color(0xff221919),
  surfaceVariant: Color(0xfff4dddc),
  onSurfaceVariant: Color(0xff524343),
  outline: Color(0xff857372),
  outlineVariant: Color(0xffd7c1c1),
  shadow: Color(0xff000000),
  scrim: Color(0xff000000),
  inverseSurface: Color(0xff382e2e),
  onInverseSurface: Color(0xffffedec),
  inversePrimary: Color(0xffffb3b2),
  primaryFixed: Color(0xffffdad9),
  onPrimaryFixed: Color(0xff3b080d),
  primaryFixedDim: Color(0xffffb3b2),
  onPrimaryFixedVariant: Color(0xff733335),
  secondaryFixed: Color(0xffffdad9),
  onSecondaryFixed: Color(0xff2c1515),
  secondaryFixedDim: Color(0xffe6bdbc),
  onSecondaryFixedVariant: Color(0xff5d3f3f),
  tertiaryFixed: Color(0xffffdeae),
  onTertiaryFixed: Color(0xff281900),
  tertiaryFixedDim: Color(0xffe4c18d),
  onTertiaryFixedVariant: Color(0xff5b431a),
  surfaceDim: Color(0xffe8d6d5),
  surfaceBright: Color(0xfffff8f7),
  surfaceContainerLowest: Color(0xffffffff),
  surfaceContainerLow: Color(0xfffff0ef),
  surfaceContainer: Color(0xfffceae9),
  surfaceContainerHigh: Color(0xfff6e4e3),
  surfaceContainerHighest: Color(0xfff0dede),
);

const darkColorScheme = ColorScheme(
  brightness: Brightness.dark,
  primary: Color(0xfff7bb71),
  surfaceTint: Color(0xfff7bb71),
  onPrimary: Color(0xff472a00),
  primaryContainer: Color(0xff653e00),
  onPrimaryContainer: Color(0xffffddb7),
  secondary: Color(0xfff3bd6e),
  onSecondary: Color(0xff442b00),
  secondaryContainer: Color(0xff624000),
  onSecondaryContainer: Color(0xffffddb1),
  tertiary: Color(0xffffb5a0),
  onTertiary: Color(0xff561f0f),
  tertiaryContainer: Color(0xff723523),
  onTertiaryContainer: Color(0xffffdbd1),
  error: Color(0xffffb4ab),
  onError: Color(0xff690005),
  errorContainer: Color(0xff93000a),
  onErrorContainer: Color(0xffffdad6),
  background: Color(0xff18120c),
  onBackground: Color(0xffeee0d4),
  surface: Color(0xff18120c),
  onSurface: Color(0xffeee0d4),
  surfaceVariant: Color(0xff504539),
  onSurfaceVariant: Color(0xffd4c4b5),
  outline: Color(0xff9c8e80),
  outlineVariant: Color(0xff504539),
  shadow: Color(0xff000000),
  scrim: Color(0xff000000),
  inverseSurface: Color(0xffeee0d4),
  onInverseSurface: Color(0xff372f27),
  inversePrimary: Color(0xff825513),
  primaryFixed: Color(0xffffddb7),
  onPrimaryFixed: Color(0xff2a1700),
  primaryFixedDim: Color(0xfff7bb71),
  onPrimaryFixedVariant: Color(0xff653e00),
  secondaryFixed: Color(0xffffddb1),
  onSecondaryFixed: Color(0xff291800),
  secondaryFixedDim: Color(0xfff3bd6e),
  onSecondaryFixedVariant: Color(0xff624000),
  tertiaryFixed: Color(0xffffdbd1),
  onTertiaryFixed: Color(0xff3a0b01),
  tertiaryFixedDim: Color(0xffffb5a0),
  onTertiaryFixedVariant: Color(0xff723523),
  surfaceDim: Color(0xff18120c),
  surfaceBright: Color(0xff403830),
  surfaceContainerLowest: Color(0xff130d07),
  surfaceContainerLow: Color(0xff211a13),
  surfaceContainer: Color(0xff251e17),
  surfaceContainerHigh: Color(0xff302921),
  surfaceContainerHighest: Color(0xff3b332b),
);

const monochromeLightScheme = ColorScheme(
  brightness: Brightness.light,
  primary: Color(0xFF5E5E5E),
  onPrimary: Color(0xFFFFFFFF),
  primaryContainer: Color(0xFFE2E2E2),
  onPrimaryContainer: Color(0xFF1B1B1B),
  secondary: Color(0xFF5E5E5E),
  onSecondary: Color(0xFFFFFFFF),
  secondaryContainer: Color(0xFFE2E2E2),
  onSecondaryContainer: Color(0xFF1B1B1B),
  tertiary: Color(0xFF5E5E5E),
  onTertiary: Color(0xFFFFFFFF),
  tertiaryContainer: Color(0xFFE2E2E2),
  onTertiaryContainer: Color(0xFF1B1B1B),
  error: Color(0xFFBA1A1A),
  errorContainer: Color(0xFFFFDAD6),
  onError: Color(0xFFFFFFFF),
  onErrorContainer: Color(0xFF410002),
  surface: Color(0xFFFCFCFC),
  onSurface: Color(0xFF1B1B1B),
  surfaceContainerHighest: Color(0xFFE2E2E2),
  onSurfaceVariant: Color(0xFF474747),
  outline: Color(0xFF767676),
  onInverseSurface: Color(0xFFF1F1F1),
  inverseSurface: Color(0xFF303030),
  inversePrimary: Color(0xFFC6C6C6),
  shadow: Color(0xFF000000),
  surfaceTint: Color(0xFF5E5E5E),
  outlineVariant: Color(0xFFC6C6C6),
  scrim: Color(0xFF000000),
);

const monochromeDarkScheme = ColorScheme(
  brightness: Brightness.dark,
  primary: Color(0xFFC6C6C6),
  onPrimary: Color(0xFF303030),
  primaryContainer: Color(0xFF474747),
  onPrimaryContainer: Color(0xFFE2E2E2),
  secondary: Color(0xFFC6C6C6),
  onSecondary: Color(0xFF303030),
  secondaryContainer: Color(0xFF474747),
  onSecondaryContainer: Color(0xFFE2E2E2),
  tertiary: Color(0xFFC6C6C6),
  onTertiary: Color(0xFF303030),
  tertiaryContainer: Color(0xFF474747),
  onTertiaryContainer: Color(0xFFE2E2E2),
  error: Color(0xFFFFB4AB),
  errorContainer: Color(0xFF93000A),
  onError: Color(0xFF690005),
  onErrorContainer: Color(0xFFFFB4AB),
  surface: Color(0xFF1B1B1B),
  onSurface: Color(0xFFE2E2E2),
  surfaceContainerHighest: Color(0xFF474747),
  onSurfaceVariant: Color(0xFFC6C6C6),
  outline: Color(0xFF919191),
  onInverseSurface: Color(0xFF303030),
  inverseSurface: Color(0xFFE2E2E2),
  inversePrimary: Color(0xFF5E5E5E),
  shadow: Color(0xFF000000),
  surfaceTint: Color(0xFFC6C6C6),
  outlineVariant: Color(0xFF474747),
  scrim: Color(0xFF000000),
);

import 'dart:ui' as ui;
import 'dart:math' as math;

import 'package:flutter/material.dart';

SliderThemeData m3SliderTheme(
  ColorScheme scheme,
  ColorScheme inverse,
  TextTheme text,
) {
  const disabledActiveOpacity = 0.38;
  const disabledInactiveOpacity = 0.12;

  return SliderThemeData(
    activeTrackColor: scheme.primary,
    inactiveTrackColor: scheme.secondaryContainer,
    inactiveTickMarkColor: scheme.primary,
    activeTickMarkColor: scheme.secondaryContainer,
    disabledActiveTrackColor: scheme.onSurface.withOpacity(
      disabledActiveOpacity,
    ),
    disabledInactiveTrackColor: scheme.onSurfaceVariant.withOpacity(
      disabledInactiveOpacity,
    ),
    disabledThumbColor: scheme.onSurface.withOpacity(disabledActiveOpacity),
    trackHeight: 16,
    thumbColor: scheme.primary,
    trackShape: _M3TrackShape(),
    thumbShape: _M3HandleShape(),
    tickMarkShape: _M3StopMarkShape(),
    valueIndicatorColor: scheme.inverseSurface,
    showValueIndicator: ShowValueIndicator.always,
    valueIndicatorTextStyle: text.labelMedium!.copyWith(
      color: scheme.onInverseSurface,
    ),
    valueIndicatorShape: _M3LabelShape(),
  );
}

class _M3TrackShape extends SliderTrackShape with BaseSliderTrackShape {
  @override
  void paint(
    PaintingContext context,
    ui.Offset offset, {
    required RenderBox parentBox,
    required SliderThemeData sliderTheme,
    required Animation<double> enableAnimation,
    required Offset thumbCenter,
    Offset? secondaryOffset,
    bool isEnabled = false,
    bool isDiscrete = false,
    required ui.TextDirection textDirection,
    double additionalActiveTrackHeight = 2,
  }) {
    // Assign the track segment paints, which are leading: active and
    // trailing: inactive.
    final ColorTween activeTrackColorTween = ColorTween(
        begin: sliderTheme.disabledActiveTrackColor,
        end: sliderTheme.activeTrackColor);
    final ColorTween inactiveTrackColorTween = ColorTween(
        begin: sliderTheme.disabledInactiveTrackColor,
        end: sliderTheme.inactiveTrackColor);
    final Paint activePaint = Paint()
      ..color = activeTrackColorTween.evaluate(enableAnimation)!;
    final Paint inactivePaint = Paint()
      ..color = inactiveTrackColorTween.evaluate(enableAnimation)!;
    final Paint leftTrackPaint;
    final Paint rightTrackPaint;
    switch (textDirection) {
      case ui.TextDirection.ltr:
        leftTrackPaint = activePaint;
        rightTrackPaint = inactivePaint;
      case ui.TextDirection.rtl:
        leftTrackPaint = inactivePaint;
        rightTrackPaint = activePaint;
    }

    final Rect trackRect = getPreferredRect(
      parentBox: parentBox,
      offset: offset,
      sliderTheme: sliderTheme,
      isEnabled: isEnabled,
      isDiscrete: isDiscrete,
    );
    final Radius trackRadius = Radius.circular(trackRect.height / 2);
    final Radius activeTrackRadius =
        Radius.circular((trackRect.height + additionalActiveTrackHeight) / 2);
    const trailingTrackRadius = Radius.circular(2);

    context.canvas.drawRRect(
      RRect.fromLTRBAndCorners(
        trackRect.left,
        (textDirection == ui.TextDirection.ltr)
            ? trackRect.top - (additionalActiveTrackHeight / 2)
            : trackRect.top,
        (thumbCenter.dx - 8).clamp(trackRect.left, trackRect.right),
        (textDirection == ui.TextDirection.ltr)
            ? trackRect.bottom + (additionalActiveTrackHeight / 2)
            : trackRect.bottom,
        topLeft: (textDirection == ui.TextDirection.ltr)
            ? activeTrackRadius
            : trackRadius,
        bottomLeft: (textDirection == ui.TextDirection.ltr)
            ? activeTrackRadius
            : trackRadius,
        topRight: trailingTrackRadius,
        bottomRight: trailingTrackRadius,
      ),
      leftTrackPaint,
    );

    context.canvas.drawRRect(
      RRect.fromLTRBAndCorners(
        (thumbCenter.dx + 8).clamp(trackRect.left, trackRect.right),
        (textDirection == ui.TextDirection.rtl)
            ? trackRect.top - (additionalActiveTrackHeight / 2)
            : trackRect.top,
        trackRect.right,
        (textDirection == ui.TextDirection.rtl)
            ? trackRect.bottom + (additionalActiveTrackHeight / 2)
            : trackRect.bottom,
        topRight: (textDirection == ui.TextDirection.rtl)
            ? activeTrackRadius
            : trackRadius,
        bottomRight: (textDirection == ui.TextDirection.rtl)
            ? activeTrackRadius
            : trackRadius,
        topLeft: trailingTrackRadius,
        bottomLeft: trailingTrackRadius,
      ),
      rightTrackPaint,
    );
  }
}

class _M3HandleShape extends SliderComponentShape {
  static const _handleHeight = 44;
  static const _handleWidth = 4.0;
  // static const _pressedHandleWidth = 2.0;

  @override
  Size getPreferredSize(bool isEnabled, bool isDiscrete) => const Size(4, 44);

  @override
  void paint(
    PaintingContext context,
    Offset center, {
    required Animation<double> activationAnimation,
    required Animation<double> enableAnimation,
    required bool isDiscrete,
    required TextPainter labelPainter,
    required RenderBox parentBox,
    required SliderThemeData sliderTheme,
    required ui.TextDirection textDirection,
    required double value,
    required double textScaleFactor,
    required Size sizeWithOverflow,
  }) {
    final color = ColorTween(
          begin: sliderTheme.disabledThumbColor,
          end: sliderTheme.thumbColor!,
        ).evaluate(enableAnimation) ??
        sliderTheme.thumbColor!;

    context.canvas.drawLine(
      center.translate(0, (_handleHeight / 2)),
      center.translate(0, -(_handleHeight / 2)),
      Paint()
        ..color = color
        // ..strokeWidth = ui.lerpDouble(
        //   _handleWidth,
        //   _pressedHandleWidth,
        //   CurvedAnimation(
        //     parent: activationAnimation,
        //     curve: Easing.standard,
        //   ).value,
        // )!
        ..strokeWidth = _handleWidth
        ..strokeCap = StrokeCap.round,
    );
  }
}

class _M3LabelShape extends SliderComponentShape {
  static const _minLabelWidth = 48;
  static const _minLabelHeight = 44;
  static const _labelPaddingHorizontal = 16;
  static const _labelPaddingVertical = 12;

  static const _handleHeight = 44.0;
  static const _handleLabelGap = 4.0;

  @override
  ui.Size getPreferredSize(
    bool isEnabled,
    bool isDiscrete, {
    TextPainter? labelPainter,
    double? textScaleFactor,
  }) {
    assert(labelPainter != null);
    assert(textScaleFactor != null);

    final width = math.max(_minLabelWidth * textScaleFactor!,
        labelPainter!.width + _labelPaddingHorizontal * 2 * textScaleFactor);

    final height = math.max(_minLabelHeight * textScaleFactor,
        (labelPainter.height + 2 * _labelPaddingVertical) * textScaleFactor);

    return Size(width, height);
  }

  @override
  void paint(
    PaintingContext context,
    ui.Offset center, {
    required Animation<double> activationAnimation,
    required Animation<double> enableAnimation,
    required bool isDiscrete,
    required TextPainter labelPainter,
    required RenderBox parentBox,
    required SliderThemeData sliderTheme,
    required ui.TextDirection textDirection,
    required double value,
    required double textScaleFactor,
    required ui.Size sizeWithOverflow,
  }) {
    final animation = CurvedAnimation(
      parent: activationAnimation,
      curve: Easing.standard,
    ).value;

    final size = getPreferredSize(
          true,
          isDiscrete,
          labelPainter: labelPainter,
          textScaleFactor: textScaleFactor,
        ) *
        animation;

    final labelOffset = (_handleHeight + _handleLabelGap) * animation +
        (_handleHeight) * (1 - animation);

    context.canvas.drawRRect(
      RRect.fromRectAndRadius(
        Rect.fromCenter(
          center: center.translate(0, -labelOffset),
          width: size.width,
          height: size.height,
        ),
        const Radius.circular(100),
      ),
      Paint()..color = sliderTheme.valueIndicatorColor!,
    );

    /* 
      The most left of the label is in the center. I need to align the middle
      of the label to the center.
    */

    final labelCenter = labelPainter.width / 2;
    final horizontalOffset = labelPainter.width - labelCenter;

    final verticalOffset =
        _handleHeight + (_handleLabelGap / 2 + size.height / 4) * animation;

    labelPainter.paint(
      context.canvas,
      center.translate(-horizontalOffset, -verticalOffset),
    );
  }
}

class _M3StopMarkShape extends SliderTickMarkShape {
  @override
  Size getPreferredSize({
    required SliderThemeData sliderTheme,
    bool isEnabled = false,
  }) =>
      const Size(4, 4);

  @override
  void paint(
    PaintingContext context,
    ui.Offset center, {
    required RenderBox parentBox,
    required SliderThemeData sliderTheme,
    required Animation<double> enableAnimation,
    required ui.Offset thumbCenter,
    required bool isEnabled,
    required ui.TextDirection textDirection,
  }) {
    if ((center.dx - thumbCenter.dx).abs() < 10) return;

    Color? begin;
    Color? end;
    switch (textDirection) {
      case ui.TextDirection.ltr:
        final bool isTickMarkRightOfThumb = center.dx > thumbCenter.dx;
        begin = isTickMarkRightOfThumb
            ? sliderTheme.disabledInactiveTickMarkColor
            : sliderTheme.disabledActiveTickMarkColor;
        end = isTickMarkRightOfThumb
            ? sliderTheme.inactiveTickMarkColor
            : sliderTheme.activeTickMarkColor;
      case ui.TextDirection.rtl:
        final bool isTickMarkLeftOfThumb = center.dx < thumbCenter.dx;
        begin = isTickMarkLeftOfThumb
            ? sliderTheme.disabledInactiveTickMarkColor
            : sliderTheme.disabledActiveTickMarkColor;
        end = isTickMarkLeftOfThumb
            ? sliderTheme.inactiveTickMarkColor
            : sliderTheme.activeTickMarkColor;
    }
    final Paint paint = Paint()
      ..color = ColorTween(begin: begin, end: end).evaluate(enableAnimation)!;

    final double tickMarkRadius = getPreferredSize(
          isEnabled: isEnabled,
          sliderTheme: sliderTheme,
        ).width /
        2;

    parentBox.size.width;

    if (tickMarkRadius > 0) {
      context.canvas.drawCircle(center, tickMarkRadius, paint);
    }
  }
}

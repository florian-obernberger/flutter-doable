import 'package:dynamic_color/dynamic_color.dart';
import 'package:flutter/material.dart';

const blue = Color(0xFF3584E4);
const green = Color(0xFF33D17A);
const yellow = Color(0xFFF6D32D);
const orange = Color(0xFFFF7800);
const red = Color(0xFFE01B24);
const purple = Color(0xFF9141AC);
const brown = Color(0xFF986A44);

RainbowColors lightRainbowColors = const RainbowColors(
  sourceBlue: Color(0xFF3584E4),
  blue: Color(0xFF005EB2),
  onBlue: Color(0xFFFFFFFF),
  blueContainer: Color(0xFFD5E3FF),
  onBlueContainer: Color(0xFF001B3C),
  sourceGreen: Color(0xFF33D17A),
  green: Color(0xFF006D3A),
  onGreen: Color(0xFFFFFFFF),
  greenContainer: Color(0xFF69FEA1),
  onGreenContainer: Color(0xFF00210D),
  sourceYellow: Color(0xFFF6D32D),
  yellow: Color(0xFF6F5D00),
  onYellow: Color(0xFFFFFFFF),
  yellowContainer: Color(0xFFFFE167),
  onYellowContainer: Color(0xFF221B00),
  sourceOrange: Color(0xFFFF7800),
  orange: Color(0xFF9A4600),
  onOrange: Color(0xFFFFFFFF),
  orangeContainer: Color(0xFFFFDBC9),
  onOrangeContainer: Color(0xFF321200),
  sourceRed: Color(0xFFE01B24),
  red: Color(0xFFC00016),
  onRed: Color(0xFFFFFFFF),
  redContainer: Color(0xFFFFDAD6),
  onRedContainer: Color(0xFF410003),
  sourcePurple: Color(0xFF9141AC),
  purple: Color(0xFF8A3BA5),
  onPurple: Color(0xFFFFFFFF),
  purpleContainer: Color(0xFFFAD7FF),
  onPurpleContainer: Color(0xFF330044),
  sourceBrown: Color(0xFF986A44),
  brown: Color(0xFF904D00),
  onBrown: Color(0xFFFFFFFF),
  brownContainer: Color(0xFFFFDCC3),
  onBrownContainer: Color(0xFF2F1500),
);

RainbowColors darkRainbowColors = const RainbowColors(
  sourceBlue: Color(0xFF3584E4),
  blue: Color(0xFFA7C8FF),
  onBlue: Color(0xFF003061),
  blueContainer: Color(0xFF004788),
  onBlueContainer: Color(0xFFD5E3FF),
  sourceGreen: Color(0xFF33D17A),
  green: Color(0xFF48E087),
  onGreen: Color(0xFF00391B),
  greenContainer: Color(0xFF00522A),
  onGreenContainer: Color(0xFF69FEA1),
  sourceYellow: Color(0xFFF6D32D),
  yellow: Color(0xFFE6C41A),
  onYellow: Color(0xFF3A3000),
  yellowContainer: Color(0xFF544600),
  onYellowContainer: Color(0xFFFFE167),
  sourceOrange: Color(0xFFFF7800),
  orange: Color(0xFFFFB68C),
  onOrange: Color(0xFF532200),
  orangeContainer: Color(0xFF753400),
  onOrangeContainer: Color(0xFFFFDBC9),
  sourceRed: Color(0xFFE01B24),
  red: Color(0xFFFFB4AC),
  onRed: Color(0xFF690007),
  redContainer: Color(0xFF93000E),
  onRedContainer: Color(0xFFFFDAD6),
  sourcePurple: Color(0xFF9141AC),
  purple: Color(0xFFEFB0FF),
  onPurple: Color(0xFF54006E),
  purpleContainer: Color(0xFF701E8B),
  onPurpleContainer: Color(0xFFFAD7FF),
  sourceBrown: Color(0xFF986A44),
  brown: Color(0xFFFFB77D),
  onBrown: Color(0xFF4D2600),
  brownContainer: Color(0xFF6E3900),
  onBrownContainer: Color(0xFFFFDCC3),
);

/// Defines a set of custom colors, each comprised of 4 complementary tones.
///
/// See also:
///   * <https://m3.material.io/styles/color/the-color-system/custom-colors>
@immutable
class RainbowColors extends ThemeExtension<RainbowColors> {
  const RainbowColors({
    required this.sourceBlue,
    required this.blue,
    required this.onBlue,
    required this.blueContainer,
    required this.onBlueContainer,
    required this.sourceGreen,
    required this.green,
    required this.onGreen,
    required this.greenContainer,
    required this.onGreenContainer,
    required this.sourceYellow,
    required this.yellow,
    required this.onYellow,
    required this.yellowContainer,
    required this.onYellowContainer,
    required this.sourceOrange,
    required this.orange,
    required this.onOrange,
    required this.orangeContainer,
    required this.onOrangeContainer,
    required this.sourceRed,
    required this.red,
    required this.onRed,
    required this.redContainer,
    required this.onRedContainer,
    required this.sourcePurple,
    required this.purple,
    required this.onPurple,
    required this.purpleContainer,
    required this.onPurpleContainer,
    required this.sourceBrown,
    required this.brown,
    required this.onBrown,
    required this.brownContainer,
    required this.onBrownContainer,
  });

  final Color sourceBlue;
  final Color blue;
  final Color onBlue;
  final Color blueContainer;
  final Color onBlueContainer;
  final Color sourceGreen;
  final Color green;
  final Color onGreen;
  final Color greenContainer;
  final Color onGreenContainer;
  final Color sourceYellow;
  final Color yellow;
  final Color onYellow;
  final Color yellowContainer;
  final Color onYellowContainer;
  final Color sourceOrange;
  final Color orange;
  final Color onOrange;
  final Color orangeContainer;
  final Color onOrangeContainer;
  final Color sourceRed;
  final Color red;
  final Color onRed;
  final Color redContainer;
  final Color onRedContainer;
  final Color sourcePurple;
  final Color purple;
  final Color onPurple;
  final Color purpleContainer;
  final Color onPurpleContainer;
  final Color sourceBrown;
  final Color brown;
  final Color onBrown;
  final Color brownContainer;
  final Color onBrownContainer;

  @override
  RainbowColors copyWith({
    Color? sourceBlue,
    Color? blue,
    Color? onBlue,
    Color? blueContainer,
    Color? onBlueContainer,
    Color? sourceGreen,
    Color? green,
    Color? onGreen,
    Color? greenContainer,
    Color? onGreenContainer,
    Color? sourceYellow,
    Color? yellow,
    Color? onYellow,
    Color? yellowContainer,
    Color? onYellowContainer,
    Color? sourceOrange,
    Color? orange,
    Color? onOrange,
    Color? orangeContainer,
    Color? onOrangeContainer,
    Color? sourceRed,
    Color? red,
    Color? onRed,
    Color? redContainer,
    Color? onRedContainer,
    Color? sourcePurple,
    Color? purple,
    Color? onPurple,
    Color? purpleContainer,
    Color? onPurpleContainer,
    Color? sourceBrown,
    Color? brown,
    Color? onBrown,
    Color? brownContainer,
    Color? onBrownContainer,
  }) {
    return RainbowColors(
      sourceBlue: sourceBlue ?? this.sourceBlue,
      blue: blue ?? this.blue,
      onBlue: onBlue ?? this.onBlue,
      blueContainer: blueContainer ?? this.blueContainer,
      onBlueContainer: onBlueContainer ?? this.onBlueContainer,
      sourceGreen: sourceGreen ?? this.sourceGreen,
      green: green ?? this.green,
      onGreen: onGreen ?? this.onGreen,
      greenContainer: greenContainer ?? this.greenContainer,
      onGreenContainer: onGreenContainer ?? this.onGreenContainer,
      sourceYellow: sourceYellow ?? this.sourceYellow,
      yellow: yellow ?? this.yellow,
      onYellow: onYellow ?? this.onYellow,
      yellowContainer: yellowContainer ?? this.yellowContainer,
      onYellowContainer: onYellowContainer ?? this.onYellowContainer,
      sourceOrange: sourceOrange ?? this.sourceOrange,
      orange: orange ?? this.orange,
      onOrange: onOrange ?? this.onOrange,
      orangeContainer: orangeContainer ?? this.orangeContainer,
      onOrangeContainer: onOrangeContainer ?? this.onOrangeContainer,
      sourceRed: sourceRed ?? this.sourceRed,
      red: red ?? this.red,
      onRed: onRed ?? this.onRed,
      redContainer: redContainer ?? this.redContainer,
      onRedContainer: onRedContainer ?? this.onRedContainer,
      sourcePurple: sourcePurple ?? this.sourcePurple,
      purple: purple ?? this.purple,
      onPurple: onPurple ?? this.onPurple,
      purpleContainer: purpleContainer ?? this.purpleContainer,
      onPurpleContainer: onPurpleContainer ?? this.onPurpleContainer,
      sourceBrown: sourceBrown ?? this.sourceBrown,
      brown: brown ?? this.brown,
      onBrown: onBrown ?? this.onBrown,
      brownContainer: brownContainer ?? this.brownContainer,
      onBrownContainer: onBrownContainer ?? this.onBrownContainer,
    );
  }

  @override
  RainbowColors lerp(ThemeExtension<RainbowColors>? other, double t) {
    if (other is! RainbowColors) {
      return this;
    }
    return RainbowColors(
      sourceBlue: Color.lerp(sourceBlue, other.sourceBlue, t) ?? sourceBlue,
      blue: Color.lerp(blue, other.blue, t) ?? blue,
      onBlue: Color.lerp(onBlue, other.onBlue, t) ?? onBlue,
      blueContainer:
          Color.lerp(blueContainer, other.blueContainer, t) ?? blueContainer,
      onBlueContainer: Color.lerp(onBlueContainer, other.onBlueContainer, t) ??
          onBlueContainer,
      sourceGreen: Color.lerp(sourceGreen, other.sourceGreen, t) ?? sourceGreen,
      green: Color.lerp(green, other.green, t) ?? green,
      onGreen: Color.lerp(onGreen, other.onGreen, t) ?? onGreen,
      greenContainer:
          Color.lerp(greenContainer, other.greenContainer, t) ?? greenContainer,
      onGreenContainer:
          Color.lerp(onGreenContainer, other.onGreenContainer, t) ??
              onGreenContainer,
      sourceYellow:
          Color.lerp(sourceYellow, other.sourceYellow, t) ?? sourceYellow,
      yellow: Color.lerp(yellow, other.yellow, t) ?? yellow,
      onYellow: Color.lerp(onYellow, other.onYellow, t) ?? onYellow,
      yellowContainer: Color.lerp(yellowContainer, other.yellowContainer, t) ??
          yellowContainer,
      onYellowContainer:
          Color.lerp(onYellowContainer, other.onYellowContainer, t) ??
              onYellowContainer,
      sourceOrange:
          Color.lerp(sourceOrange, other.sourceOrange, t) ?? sourceOrange,
      orange: Color.lerp(orange, other.orange, t) ?? orange,
      onOrange: Color.lerp(onOrange, other.onOrange, t) ?? onOrange,
      orangeContainer: Color.lerp(orangeContainer, other.orangeContainer, t) ??
          orangeContainer,
      onOrangeContainer:
          Color.lerp(onOrangeContainer, other.onOrangeContainer, t) ??
              onOrangeContainer,
      sourceRed: Color.lerp(sourceRed, other.sourceRed, t) ?? sourceRed,
      red: Color.lerp(red, other.red, t) ?? red,
      onRed: Color.lerp(onRed, other.onRed, t) ?? onRed,
      redContainer:
          Color.lerp(redContainer, other.redContainer, t) ?? redContainer,
      onRedContainer:
          Color.lerp(onRedContainer, other.onRedContainer, t) ?? onRedContainer,
      sourcePurple:
          Color.lerp(sourcePurple, other.sourcePurple, t) ?? sourcePurple,
      purple: Color.lerp(purple, other.purple, t) ?? purple,
      onPurple: Color.lerp(onPurple, other.onPurple, t) ?? onPurple,
      purpleContainer: Color.lerp(purpleContainer, other.purpleContainer, t) ??
          purpleContainer,
      onPurpleContainer:
          Color.lerp(onPurpleContainer, other.onPurpleContainer, t) ??
              onPurpleContainer,
      sourceBrown: Color.lerp(sourceBrown, other.sourceBrown, t) ?? sourceBrown,
      brown: Color.lerp(brown, other.brown, t) ?? brown,
      onBrown: Color.lerp(onBrown, other.onBrown, t) ?? onBrown,
      brownContainer:
          Color.lerp(brownContainer, other.brownContainer, t) ?? brownContainer,
      onBrownContainer:
          Color.lerp(onBrownContainer, other.onBrownContainer, t) ??
              onBrownContainer,
    );
  }

  /// Returns an instance of [RainbowColors] in which the following custom
  /// colors are harmonized with [dynamic]'s [ColorScheme.primary].
  ///   * [RainbowColors.sourceBlue]
  ///   * [RainbowColors.blue]
  ///   * [RainbowColors.onBlue]
  ///   * [RainbowColors.blueContainer]
  ///   * [RainbowColors.onBlueContainer]
  ///   * [RainbowColors.sourceGreen]
  ///   * [RainbowColors.green]
  ///   * [RainbowColors.onGreen]
  ///   * [RainbowColors.greenContainer]
  ///   * [RainbowColors.onGreenContainer]
  ///   * [RainbowColors.sourceYellow]
  ///   * [RainbowColors.yellow]
  ///   * [RainbowColors.onYellow]
  ///   * [RainbowColors.yellowContainer]
  ///   * [RainbowColors.onYellowContainer]
  ///   * [RainbowColors.sourceOrange]
  ///   * [RainbowColors.orange]
  ///   * [RainbowColors.onOrange]
  ///   * [RainbowColors.orangeContainer]
  ///   * [RainbowColors.onOrangeContainer]
  ///   * [RainbowColors.sourceRed]
  ///   * [RainbowColors.red]
  ///   * [RainbowColors.onRed]
  ///   * [RainbowColors.redContainer]
  ///   * [RainbowColors.onRedContainer]
  ///   * [RainbowColors.sourcePurple]
  ///   * [RainbowColors.purple]
  ///   * [RainbowColors.onPurple]
  ///   * [RainbowColors.purpleContainer]
  ///   * [RainbowColors.onPurpleContainer]
  ///   * [RainbowColors.sourceBrown]
  ///   * [RainbowColors.brown]
  ///   * [RainbowColors.onBrown]
  ///   * [RainbowColors.brownContainer]
  ///   * [RainbowColors.onBrownContainer]
  ///
  /// See also:
  ///   * <https://m3.material.io/styles/color/the-color-system/custom-colors#harmonization>
  RainbowColors harmonized(ColorScheme dynamic) {
    return copyWith(
      sourceBlue: sourceBlue.harmonizeWith(dynamic.primary),
      blue: blue.harmonizeWith(dynamic.primary),
      onBlue: onBlue.harmonizeWith(dynamic.primary),
      blueContainer: blueContainer.harmonizeWith(dynamic.primary),
      onBlueContainer: onBlueContainer.harmonizeWith(dynamic.primary),
      sourceGreen: sourceGreen.harmonizeWith(dynamic.primary),
      green: green.harmonizeWith(dynamic.primary),
      onGreen: onGreen.harmonizeWith(dynamic.primary),
      greenContainer: greenContainer.harmonizeWith(dynamic.primary),
      onGreenContainer: onGreenContainer.harmonizeWith(dynamic.primary),
      sourceYellow: sourceYellow.harmonizeWith(dynamic.primary),
      yellow: yellow.harmonizeWith(dynamic.primary),
      onYellow: onYellow.harmonizeWith(dynamic.primary),
      yellowContainer: yellowContainer.harmonizeWith(dynamic.primary),
      onYellowContainer: onYellowContainer.harmonizeWith(dynamic.primary),
      sourceOrange: sourceOrange.harmonizeWith(dynamic.primary),
      orange: orange.harmonizeWith(dynamic.primary),
      onOrange: onOrange.harmonizeWith(dynamic.primary),
      orangeContainer: orangeContainer.harmonizeWith(dynamic.primary),
      onOrangeContainer: onOrangeContainer.harmonizeWith(dynamic.primary),
      sourceRed: sourceRed.harmonizeWith(dynamic.primary),
      red: red.harmonizeWith(dynamic.primary),
      onRed: onRed.harmonizeWith(dynamic.primary),
      redContainer: redContainer.harmonizeWith(dynamic.primary),
      onRedContainer: onRedContainer.harmonizeWith(dynamic.primary),
      sourcePurple: sourcePurple.harmonizeWith(dynamic.primary),
      purple: purple.harmonizeWith(dynamic.primary),
      onPurple: onPurple.harmonizeWith(dynamic.primary),
      purpleContainer: purpleContainer.harmonizeWith(dynamic.primary),
      onPurpleContainer: onPurpleContainer.harmonizeWith(dynamic.primary),
      sourceBrown: sourceBrown.harmonizeWith(dynamic.primary),
      brown: brown.harmonizeWith(dynamic.primary),
      onBrown: onBrown.harmonizeWith(dynamic.primary),
      brownContainer: brownContainer.harmonizeWith(dynamic.primary),
      onBrownContainer: onBrownContainer.harmonizeWith(dynamic.primary),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

typedef FontThemeFunction = TextTheme Function();

const _fallback = ['Noto'];
const _fallbackMono = ['NotoMono'];

TextTheme _addFallback(TextTheme textTheme) {
  return TextTheme(
    displayLarge:
        textTheme.displayLarge?.copyWith(fontFamilyFallback: _fallback),
    displayMedium:
        textTheme.displayMedium?.copyWith(fontFamilyFallback: _fallback),
    displaySmall:
        textTheme.displaySmall?.copyWith(fontFamilyFallback: _fallback),
    headlineLarge:
        textTheme.headlineLarge?.copyWith(fontFamilyFallback: _fallback),
    headlineMedium:
        textTheme.headlineMedium?.copyWith(fontFamilyFallback: _fallback),
    headlineSmall:
        textTheme.headlineSmall?.copyWith(fontFamilyFallback: _fallback),
    titleLarge: textTheme.titleLarge?.copyWith(fontFamilyFallback: _fallback),
    titleMedium: textTheme.titleMedium?.copyWith(fontFamilyFallback: _fallback),
    titleSmall: textTheme.titleSmall?.copyWith(fontFamilyFallback: _fallback),
    labelLarge: textTheme.labelLarge?.copyWith(fontFamilyFallback: _fallback),
    labelMedium: textTheme.labelMedium?.copyWith(fontFamilyFallback: _fallback),
    labelSmall: textTheme.labelSmall?.copyWith(fontFamilyFallback: _fallback),
    bodyLarge: textTheme.bodyLarge?.copyWith(fontFamilyFallback: _fallback),
    bodyMedium: textTheme.bodyMedium?.copyWith(fontFamilyFallback: _fallback),
    bodySmall: textTheme.bodySmall?.copyWith(fontFamilyFallback: _fallback),
  );
}

enum FontPairing {
  vanilla('Vanilla', FontTheme.outfit, FontTheme.pacifico),
  doable('Doable', FontTheme.hankenGrotesk, FontTheme.neucha),
  shantell('Shantell', FontTheme.lato, FontTheme.shantellSans),
  retro('Retro', FontTheme.roboto, FontTheme.japan),
  rusty('Rusty', FontTheme.lora, FontTheme.caveat),
  elegant('Elegant', FontTheme.manrope, FontTheme.neucha);

  const FontPairing(
    this.displayName,
    this.primary,
    this.accent, [
    // ignore: unused_element
    this.mono = FontTheme.jetbrainsMono,
  ]);

  final String displayName;
  final FontTheme primary;
  final FontTheme accent;
  final FontTheme mono;
}

enum FontThemeType {
  primary,
  accent,
  textAccent,
  mono;

  bool get isPrimary => this == primary;
  bool get isAccent => this == accent || this == textAccent;
  bool get isTextAccent => this == textAccent;
  bool get isMono => this == mono;
}

enum FontTheme {
  // Primary
  outfit('Outfit', FontThemeType.primary, _outfit),
  roboto('Roboto', FontThemeType.primary, _roboto),
  lora('Lora', FontThemeType.primary, _lora),
  lato('Lato', FontThemeType.primary, _lato),
  manrope('Manrope', FontThemeType.primary, _manrope),
  hankenGrotesk('Hanken Grotesk', FontThemeType.primary, _hankenGrotesk),
  systemFont('System Font', FontThemeType.primary, _systemFont),

  // Accent
  japan('Japan', FontThemeType.accent, _japan),
  neucha('Neucha', FontThemeType.accent, _neucha),
  pacifico('Pacifico', FontThemeType.accent, _pacifico),
  caveat('Caveat', FontThemeType.accent, _caveat),
  shantellSans('Shantell Sans', FontThemeType.accent, _shantellSans),

  // Mono
  jetbrainsMono('JetbrainsMono', FontThemeType.mono, _jetbrainsMono);

  const FontTheme(this.displayName, this.type, this._textTheme);

  final String displayName;
  final FontThemeType type;
  final FontThemeFunction _textTheme;

  TextTheme get textTheme => _textTheme();

  static List<FontTheme> get primaries =>
      values.where((ft) => ft.type == FontThemeType.primary).toList();
  static List<FontTheme> get accents =>
      values.where((ft) => ft.type == FontThemeType.accent).toList();
  static List<FontTheme> get monos =>
      values.where((ft) => ft.type == FontThemeType.mono).toList();
}

// Primary TextThemes Functions

TextTheme _roboto() => _robotoTextTheme;
TextTheme _outfit() => _outfitTextTheme;
TextTheme _lora() => _loraTextTheme;
TextTheme _lato() => _latoTextTheme;
TextTheme _manrope() => _manropeTextTheme;
TextTheme _hankenGrotesk() => _hankenGroteskTextTheme;
TextTheme _systemFont() => _systemTextTheme;

// Accent TextThemes Functions

TextTheme _japan() => _japanTextTheme;
TextTheme _neucha() => _neuchaTextTheme;
TextTheme _pacifico() => _pacificoTextTheme;
TextTheme _caveat() => _caveatTextTheme;
TextTheme _shantellSans() => _shantellSansTextTheme;

// Monospace TextThemes Functions

TextTheme _jetbrainsMono() => _jetbrainsMonoTextTheme;

// #region Primary TextThemes

final _robotoTextTheme = _addFallback(TextTheme(
  displayLarge: GoogleFonts.robotoFlex(
    height: 64 / 57,
    fontSize: 57,
    fontWeight: FontWeight.w400,
  ),
  displayMedium: GoogleFonts.robotoFlex(
    height: 52 / 45,
    fontSize: 45,
    fontWeight: FontWeight.w400,
  ),
  displaySmall: GoogleFonts.robotoFlex(
    height: 44 / 36,
    fontSize: 36,
    fontWeight: FontWeight.w400,
  ),
  headlineLarge: GoogleFonts.robotoFlex(
    height: 40 / 32,
    fontSize: 32,
    fontWeight: FontWeight.w400,
  ),
  headlineMedium: GoogleFonts.robotoFlex(
    height: 36 / 28,
    fontSize: 28,
    fontWeight: FontWeight.w400,
  ),
  headlineSmall: GoogleFonts.robotoFlex(
    height: 32 / 24,
    fontSize: 24,
    fontWeight: FontWeight.w400,
  ),
  titleLarge: GoogleFonts.robotoFlex(
    height: 28 / 22,
    fontSize: 22,
    fontWeight: FontWeight.w400,
  ),
  titleMedium: GoogleFonts.robotoFlex(
    height: 24 / 16,
    fontSize: 16,
    fontWeight: FontWeight.w500,
  ),
  titleSmall: GoogleFonts.robotoFlex(
    height: 20 / 14,
    fontSize: 14,
    fontWeight: FontWeight.w500,
  ),
  labelLarge: GoogleFonts.robotoFlex(
    height: 20 / 14,
    fontSize: 14,
    fontWeight: FontWeight.w500,
  ),
  labelMedium: GoogleFonts.robotoFlex(
    height: 16 / 12,
    fontSize: 12,
    fontWeight: FontWeight.w500,
  ),
  labelSmall: GoogleFonts.robotoFlex(
    height: 6 / 11,
    fontSize: 11,
    fontWeight: FontWeight.w500,
  ),
  bodyLarge: GoogleFonts.robotoFlex(
    height: 24 / 16,
    fontSize: 16,
    fontWeight: FontWeight.w500,
  ),
  bodyMedium: GoogleFonts.robotoFlex(
    height: 20 / 14,
    fontSize: 14,
    fontWeight: FontWeight.w500,
  ),
  bodySmall: GoogleFonts.robotoFlex(
    height: 16 / 12,
    fontSize: 12,
    fontWeight: FontWeight.w500,
  ),
));

final _loraTextTheme = _addFallback(
  GoogleFonts.loraTextTheme(_robotoTextTheme),
);
final _latoTextTheme = _addFallback(
  GoogleFonts.latoTextTheme(_robotoTextTheme),
);
final _outfitTextTheme = _addFallback(
  GoogleFonts.outfitTextTheme(_hankenGroteskTextTheme),
);
final _manropeTextTheme = _addFallback(
  GoogleFonts.manropeTextTheme(_robotoTextTheme),
);
const _hankenGroteskTextTheme = TextTheme(
  displayLarge: TextStyle(
    fontFamily: 'HankenGrotesk',
    fontFamilyFallback: _fallback,
    height: 64 / 57,
    fontSize: 57,
    fontWeight: FontWeight.w300,
  ),
  displayMedium: TextStyle(
    fontFamily: 'HankenGrotesk',
    fontFamilyFallback: _fallback,
    height: 52 / 45,
    fontSize: 45,
    fontWeight: FontWeight.w300,
  ),
  displaySmall: TextStyle(
    fontFamily: 'HankenGrotesk',
    fontFamilyFallback: _fallback,
    height: 44 / 36,
    fontSize: 36,
    fontWeight: FontWeight.w300,
  ),
  headlineLarge: TextStyle(
    fontFamily: 'HankenGrotesk',
    fontFamilyFallback: _fallback,
    height: 40 / 32,
    fontSize: 32,
    fontWeight: FontWeight.w300,
  ),
  headlineMedium: TextStyle(
    fontFamily: 'HankenGrotesk',
    fontFamilyFallback: _fallback,
    height: 36 / 28,
    fontSize: 28,
    fontWeight: FontWeight.w300,
  ),
  headlineSmall: TextStyle(
    fontFamily: 'HankenGrotesk',
    fontFamilyFallback: _fallback,
    height: 32 / 24,
    fontSize: 24,
    fontWeight: FontWeight.w300,
  ),
  titleLarge: TextStyle(
    fontFamily: 'HankenGrotesk',
    fontFamilyFallback: _fallback,
    height: 28 / 22,
    fontSize: 22,
    fontWeight: FontWeight.w300,
  ),
  titleMedium: TextStyle(
    fontFamily: 'HankenGrotesk',
    fontFamilyFallback: _fallback,
    height: 24 / 16,
    fontSize: 16,
    fontWeight: FontWeight.w400,
  ),
  titleSmall: TextStyle(
    fontFamily: 'HankenGrotesk',
    fontFamilyFallback: _fallback,
    height: 20 / 14,
    fontSize: 14,
    fontWeight: FontWeight.w400,
  ),
  labelLarge: TextStyle(
    fontFamily: 'HankenGrotesk',
    fontFamilyFallback: _fallback,
    height: 20 / 14,
    fontSize: 14,
    fontWeight: FontWeight.w400,
  ),
  labelMedium: TextStyle(
    fontFamily: 'HankenGrotesk',
    fontFamilyFallback: _fallback,
    height: 16 / 12,
    fontSize: 12,
    fontWeight: FontWeight.w400,
  ),
  labelSmall: TextStyle(
    fontFamily: 'HankenGrotesk',
    fontFamilyFallback: _fallback,
    height: 6 / 11,
    fontSize: 11,
    fontWeight: FontWeight.w400,
  ),
  bodyLarge: TextStyle(
    fontFamily: 'HankenGrotesk',
    fontFamilyFallback: _fallback,
    height: 24 / 16,
    fontSize: 16,
    fontWeight: FontWeight.w400,
  ),
  bodyMedium: TextStyle(
    fontFamily: 'HankenGrotesk',
    fontFamilyFallback: _fallback,
    height: 20 / 14,
    fontSize: 14,
    fontWeight: FontWeight.w400,
  ),
  bodySmall: TextStyle(
    fontFamily: 'HankenGrotesk',
    fontFamilyFallback: _fallback,
    height: 16 / 12,
    fontSize: 12,
    fontWeight: FontWeight.w400,
  ),
);

const _systemTextTheme = TextTheme(
  displayLarge: TextStyle(
    fontFamily: 'SystemFont',
    fontFamilyFallback: _fallback,
    height: 64 / 57,
    fontSize: 57,
    fontWeight: FontWeight.w400,
  ),
  displayMedium: TextStyle(
    fontFamily: 'SystemFont',
    fontFamilyFallback: _fallback,
    height: 52 / 45,
    fontSize: 45,
    fontWeight: FontWeight.w400,
  ),
  displaySmall: TextStyle(
    fontFamily: 'SystemFont',
    fontFamilyFallback: _fallback,
    height: 44 / 36,
    fontSize: 36,
    fontWeight: FontWeight.w400,
  ),
  headlineLarge: TextStyle(
    fontFamily: 'SystemFont',
    fontFamilyFallback: _fallback,
    height: 40 / 32,
    fontSize: 32,
    fontWeight: FontWeight.w400,
  ),
  headlineMedium: TextStyle(
    fontFamily: 'SystemFont',
    fontFamilyFallback: _fallback,
    height: 36 / 28,
    fontSize: 28,
    fontWeight: FontWeight.w400,
  ),
  headlineSmall: TextStyle(
    fontFamily: 'SystemFont',
    fontFamilyFallback: _fallback,
    height: 32 / 24,
    fontSize: 24,
    fontWeight: FontWeight.w400,
  ),
  titleLarge: TextStyle(
    fontFamily: 'SystemFont',
    fontFamilyFallback: _fallback,
    height: 28 / 22,
    fontSize: 22,
    fontWeight: FontWeight.w400,
  ),
  titleMedium: TextStyle(
    fontFamily: 'SystemFont',
    fontFamilyFallback: _fallback,
    height: 24 / 16,
    fontSize: 16,
    fontWeight: FontWeight.w500,
  ),
  titleSmall: TextStyle(
    fontFamily: 'SystemFont',
    fontFamilyFallback: _fallback,
    height: 20 / 14,
    fontSize: 14,
    fontWeight: FontWeight.w500,
  ),
  labelLarge: TextStyle(
    fontFamily: 'SystemFont',
    fontFamilyFallback: _fallback,
    height: 20 / 14,
    fontSize: 14,
    fontWeight: FontWeight.w500,
  ),
  labelMedium: TextStyle(
    fontFamily: 'SystemFont',
    fontFamilyFallback: _fallback,
    height: 16 / 12,
    fontSize: 12,
    fontWeight: FontWeight.w500,
  ),
  labelSmall: TextStyle(
    fontFamily: 'SystemFont',
    fontFamilyFallback: _fallback,
    height: 6 / 11,
    fontSize: 11,
    fontWeight: FontWeight.w500,
  ),
  bodyLarge: TextStyle(
    fontFamily: 'SystemFont',
    fontFamilyFallback: _fallback,
    height: 24 / 16,
    fontSize: 16,
    fontWeight: FontWeight.w500,
  ),
  bodyMedium: TextStyle(
    fontFamily: 'SystemFont',
    fontFamilyFallback: _fallback,
    height: 20 / 14,
    fontSize: 14,
    fontWeight: FontWeight.w500,
  ),
  bodySmall: TextStyle(
    fontFamily: 'SystemFont',
    fontFamilyFallback: _fallback,
    height: 16 / 12,
    fontSize: 12,
    fontWeight: FontWeight.w500,
  ),
);

// #endregion

// #region Accent TextThemes

final _neuchaTextTheme = _addFallback(
  GoogleFonts.neuchaTextTheme(_robotoTextTheme),
);
final _pacificoTextTheme = _addFallback(
  GoogleFonts.pacificoTextTheme(_robotoTextTheme),
);
final _caveatTextTheme = _addFallback(
  GoogleFonts.caveatTextTheme(_robotoTextTheme),
);
final _shantellSansTextTheme = _addFallback(
  GoogleFonts.shantellSansTextTheme(_robotoTextTheme),
);

const _japanTextTheme = TextTheme(
  displayLarge: TextStyle(
    fontFamily: 'Japan',
    fontFamilyFallback: _fallback,
    height: 64 / 57,
    fontSize: 57,
    fontWeight: FontWeight.w400,
  ),
  displayMedium: TextStyle(
    fontFamily: 'Japan',
    fontFamilyFallback: _fallback,
    height: 52 / 45,
    fontSize: 45,
    fontWeight: FontWeight.w400,
  ),
  displaySmall: TextStyle(
    fontFamily: 'Japan',
    fontFamilyFallback: _fallback,
    height: 44 / 36,
    fontSize: 36,
    fontWeight: FontWeight.w400,
  ),
  headlineLarge: TextStyle(
    fontFamily: 'Japan',
    fontFamilyFallback: _fallback,
    height: 40 / 32,
    fontSize: 32,
    fontWeight: FontWeight.w400,
  ),
  headlineMedium: TextStyle(
    fontFamily: 'Japan',
    fontFamilyFallback: _fallback,
    height: 36 / 28,
    fontSize: 28,
    fontWeight: FontWeight.w400,
  ),
  headlineSmall: TextStyle(
    fontFamily: 'Japan',
    fontFamilyFallback: _fallback,
    height: 32 / 24,
    fontSize: 24,
    fontWeight: FontWeight.w400,
  ),
  titleLarge: TextStyle(
    fontFamily: 'Japan',
    fontFamilyFallback: _fallback,
    height: 28 / 22,
    fontSize: 22,
    fontWeight: FontWeight.w400,
  ),
  titleMedium: TextStyle(
    fontFamily: 'Japan',
    fontFamilyFallback: _fallback,
    height: 24 / 16,
    fontSize: 16,
    fontWeight: FontWeight.w500,
  ),
  titleSmall: TextStyle(
    fontFamily: 'Japan',
    fontFamilyFallback: _fallback,
    height: 20 / 14,
    fontSize: 14,
    fontWeight: FontWeight.w500,
  ),
  labelLarge: TextStyle(
    fontFamily: 'Japan',
    fontFamilyFallback: _fallback,
    height: 20 / 14,
    fontSize: 14,
    fontWeight: FontWeight.w500,
  ),
  labelMedium: TextStyle(
    fontFamily: 'Japan',
    fontFamilyFallback: _fallback,
    height: 16 / 12,
    fontSize: 12,
    fontWeight: FontWeight.w500,
  ),
  labelSmall: TextStyle(
    fontFamily: 'Japan',
    fontFamilyFallback: _fallback,
    height: 6 / 11,
    fontSize: 11,
    fontWeight: FontWeight.w500,
  ),
  bodyLarge: TextStyle(
    fontFamily: 'Japan',
    fontFamilyFallback: _fallback,
    height: 24 / 16,
    fontSize: 16,
    fontWeight: FontWeight.w500,
  ),
  bodyMedium: TextStyle(
    fontFamily: 'Japan',
    fontFamilyFallback: _fallback,
    height: 20 / 14,
    fontSize: 14,
    fontWeight: FontWeight.w500,
  ),
  bodySmall: TextStyle(
    fontFamily: 'Japan',
    fontFamilyFallback: _fallback,
    height: 16 / 12,
    fontSize: 12,
    fontWeight: FontWeight.w500,
  ),
);

// #endregion

// #region Monospace TextThemes

const _jetbrainsMonoTextTheme = TextTheme(
  displayLarge: TextStyle(
    fontFamily: 'JetBrainsMono',
    fontFamilyFallback: _fallbackMono,
    height: 64 / 57,
    fontSize: 57,
    fontWeight: FontWeight.w400,
  ),
  displayMedium: TextStyle(
    fontFamily: 'JetBrainsMono',
    fontFamilyFallback: _fallbackMono,
    height: 52 / 45,
    fontSize: 45,
    fontWeight: FontWeight.w400,
  ),
  displaySmall: TextStyle(
    fontFamily: 'JetBrainsMono',
    fontFamilyFallback: _fallbackMono,
    height: 44 / 36,
    fontSize: 36,
    fontWeight: FontWeight.w400,
  ),
  headlineLarge: TextStyle(
    fontFamily: 'JetBrainsMono',
    fontFamilyFallback: _fallbackMono,
    height: 40 / 32,
    fontSize: 32,
    fontWeight: FontWeight.w400,
  ),
  headlineMedium: TextStyle(
    fontFamily: 'JetBrainsMono',
    fontFamilyFallback: _fallbackMono,
    height: 36 / 28,
    fontSize: 28,
    fontWeight: FontWeight.w400,
  ),
  headlineSmall: TextStyle(
    fontFamily: 'JetBrainsMono',
    fontFamilyFallback: _fallbackMono,
    height: 32 / 24,
    fontSize: 24,
    fontWeight: FontWeight.w400,
  ),
  titleLarge: TextStyle(
    fontFamily: 'JetBrainsMono',
    fontFamilyFallback: _fallbackMono,
    height: 28 / 22,
    fontSize: 22,
    fontWeight: FontWeight.w400,
  ),
  titleMedium: TextStyle(
    fontFamily: 'JetBrainsMono',
    fontFamilyFallback: _fallbackMono,
    height: 24 / 16,
    fontSize: 16,
    fontWeight: FontWeight.w500,
  ),
  titleSmall: TextStyle(
    fontFamily: 'JetBrainsMono',
    fontFamilyFallback: _fallbackMono,
    height: 20 / 14,
    fontSize: 14,
    fontWeight: FontWeight.w500,
  ),
  labelLarge: TextStyle(
    fontFamily: 'JetBrainsMono',
    fontFamilyFallback: _fallbackMono,
    height: 20 / 14,
    fontSize: 14,
    fontWeight: FontWeight.w500,
  ),
  labelMedium: TextStyle(
    fontFamily: 'JetBrainsMono',
    fontFamilyFallback: _fallbackMono,
    height: 16 / 12,
    fontSize: 12,
    fontWeight: FontWeight.w500,
  ),
  labelSmall: TextStyle(
    fontFamily: 'JetBrainsMono',
    fontFamilyFallback: _fallbackMono,
    height: 6 / 11,
    fontSize: 11,
    fontWeight: FontWeight.w500,
  ),
  bodyLarge: TextStyle(
    fontFamily: 'JetBrainsMono',
    fontFamilyFallback: _fallbackMono,
    height: 24 / 16,
    fontSize: 16,
    fontWeight: FontWeight.w500,
  ),
  bodyMedium: TextStyle(
    fontFamily: 'JetBrainsMono',
    fontFamilyFallback: _fallbackMono,
    height: 20 / 14,
    fontSize: 14,
    fontWeight: FontWeight.w500,
  ),
  bodySmall: TextStyle(
    fontFamily: 'JetBrainsMono',
    fontFamilyFallback: _fallbackMono,
    height: 16 / 12,
    fontSize: 12,
    fontWeight: FontWeight.w500,
  ),
);

// #endregion

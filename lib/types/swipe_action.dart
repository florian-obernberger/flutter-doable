import 'package:flutter/material.dart';
import 'package:flutter_material_design_icons/flutter_material_design_icons.dart';
import 'package:material_symbols_icons/symbols.dart';

import '/strings/strings.dart';
import '/resources/resources.dart';
import '/data/todo.dart';
import '/widgets/svg_icon.dart';

enum SwipeAction {
  star(negatable: true, hidden: false),
  complete(negatable: true, hidden: true),
  // expand(negatable: true, hidden: false),
  delete(negatable: false, hidden: true);

  const SwipeAction({required this.negatable, required this.hidden});

  final bool negatable;
  final bool hidden;

  ({Color background, Color foreground}) _colors(BuildContext context) {
    final ThemeData(
      colorScheme: scheme,
      brightness: brightness,
      floatingActionButtonTheme: fab
    ) = Theme.of(context);

    final dark = brightness == Brightness.dark;

    return switch (this) {
      star when dark => const (
          background: Color(0xFFffb95b),
          foreground: Color(0xFF462a00)
        ),
      star => const (background: Color(0xFF785900), foreground: Colors.white),
      complete => (
          background: fab.backgroundColor!,
          foreground: fab.foregroundColor!
        ),
      // expand => (
      //     background: scheme.tertiaryContainer,
      //     foreground: scheme.onTertiaryContainer
      //   ),
      delete => (background: scheme.error, foreground: scheme.onError),
    };
  }

  Color color(BuildContext context) => _colors(context).background;

  Widget icon(BuildContext context, {double? size, bool negated = false}) {
    assert(
      !negated || negated == negatable,
      'Cannot negate non-negatable actions',
    );

    final color = _colors(context).foreground;

    return Tooltip(
      message: title(Strings.of(context)!),
      child: switch (this) {
        star when negated => Icon(
            MdiIcons.starOffOutline,
            color: color,
            size: size,
          ),
        star => Icon(MdiIcons.starOutline, color: color, size: size),
        complete when negated => SvgIcon(
            icon: SvgIcons.undoDoneW500,
            color: color,
            size: size,
          ),
        complete => Icon(Symbols.done, weight: 500, color: color, size: size),
        // expand => Icon(Symbols.expand_content, color: color, size: size),
        delete => Icon(Symbols.delete, weight: 500, color: color, size: size),
      },
    );
  }

  String title(Strings strings) => switch (this) {
        star => strings.swipeActionStar,
        complete => strings.swipeActionComplete,
        // expand => strings.swipeActionExpand,
        delete => strings.swipeActionDelete,
      };

  bool shouldNegate(Todo todo) => switch (this) {
        star => todo.isImportant,
        complete => todo.isCompleted,
        delete /* || expand */ => false,
      };
}

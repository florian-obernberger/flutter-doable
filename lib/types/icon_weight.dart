extension type const IconWeight._(double _weight) implements double {
  /// Thin, the least thick.
  static const IconWeight w100 = IconWeight._(100);

  /// Extra-light.
  static const IconWeight w200 = IconWeight._(200);

  /// Light.
  static const IconWeight w300 = IconWeight._(300);

  /// Normal / regular / plain.
  static const IconWeight w400 = IconWeight._(400);

  /// Medium.
  static const IconWeight w500 = IconWeight._(500);

  /// Semi-bold.
  static const IconWeight w600 = IconWeight._(600);

  /// Bold.
  static const IconWeight w700 = IconWeight._(700);

  /// Extra-bold.
  static const IconWeight w800 = IconWeight._(800);

  /// Black, the most thick.
  static const IconWeight w900 = IconWeight._(900);

  /// The default icon weight.
  static const IconWeight normal = w400;

  /// A commonly used icon weight that is heavier than normal.
  static const IconWeight bold = w700;

  static List<IconWeight> get values => [
        w100,
        w200,
        w300,
        w400,
        w500,
        w600,
        w700,
        w800,
        w900,
      ];
}

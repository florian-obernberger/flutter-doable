import 'package:flutter/material.dart';
import 'package:intl/intl.dart' as intl;

const _internationalDate = 'y-M-d';
const _americanDate = 'M/d/y';
const _europeanDate = 'd.M.y';
const _europeanWithMonthShortDate = 'd. MMM y';
const _europeanWithMonthAndYearShortDate = 'd. MMM yy';
const _twitterDate = 'dd MMM yy';

enum DateFormat {
  international(_internationalDate),
  american(_americanDate),
  european(_europeanDate),
  europeanWithMonthShort(_europeanWithMonthShortDate),
  europeanWithMonthAndYearShort(_europeanWithMonthAndYearShortDate),
  twitter(_twitterDate);

  const DateFormat(this._format);
  final String _format;

  factory DateFormat.fromString(String string) => switch (string) {
        _internationalDate => DateFormat.international,
        _americanDate => DateFormat.american,
        _europeanDate => DateFormat.european,
        _twitterDate => DateFormat.twitter,
        _europeanWithMonthShortDate => DateFormat.europeanWithMonthShort,
        _europeanWithMonthAndYearShortDate =>
          DateFormat.europeanWithMonthAndYearShort,
        _ => throw UnimplementedError(
            'This is not a valid DateFormat: $string',
          ),
      };

  @override
  String toString() => _format;

  String format(DateTime date, [String? locale]) =>
      intl.DateFormat(_format, locale).format(date);

  String formatWithTime(DateTime date, TimeFormat timeFormat,
          [String? locale, String separator = ' ']) =>
      intl.DateFormat(_format, locale)
          .addPattern(timeFormat._format, separator)
          .format(date);
}

const _americanTime = 'h:mm a';
const _americanModern = 'h:mm';
const _europeanTime = 'H:mm';
const _militaryTime = 'HH:mm';

enum TimeFormat {
  american(_americanTime, false),
  americanModern(_americanModern, false),
  european(_europeanTime, true),
  military(_militaryTime, true);

  // ignore: avoid_positional_boolean_parameters
  const TimeFormat(this._format, this.is24hour);
  final String _format;
  final bool is24hour;

  factory TimeFormat.fromString(String string) => switch (string) {
        _americanTime => TimeFormat.american,
        _americanModern => TimeFormat.americanModern,
        _europeanTime => TimeFormat.european,
        _militaryTime => TimeFormat.military,
        _ => throw UnimplementedError(
            'This is not a valid TimeFormat: $string',
          ),
      };

  @override
  String toString() => _format;

  String format(DateTime date, [String? locale]) =>
      intl.DateFormat(_format, locale).format(date);

  String formatTimeOfDay(TimeOfDay time, [String? locale]) => format(
        DateTime.now().copyWith(hour: time.hour, minute: time.minute),
        locale,
      );
}

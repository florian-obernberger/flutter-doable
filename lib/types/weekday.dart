import 'package:intl/intl.dart';

enum Weekday {
  sun(DateTime.sunday),
  mon(DateTime.monday),
  tue(DateTime.tuesday),
  wed(DateTime.wednesday),
  thu(DateTime.thursday),
  fri(DateTime.friday),
  sat(DateTime.saturday);

  const Weekday(this.day);
  factory Weekday.fromIndex(int pos) => switch (pos) {
        0 => sun,
        1 => mon,
        2 => tue,
        3 => wed,
        4 => thu,
        5 => fri,
        6 => sat,
        _ => throw UnimplementedError()
      };

  factory Weekday.fromDateTime(DateTime date) => switch (date.weekday) {
        DateTime.sunday => sun,
        DateTime.monday => mon,
        DateTime.tuesday => tue,
        DateTime.wednesday => wed,
        DateTime.thursday => thu,
        DateTime.friday => fri,
        DateTime.saturday => sat,
        _ => throw UnimplementedError(),
      };

  final int day;

  String format([String? locale]) => DateFormat.EEEE(locale).format(
        DateTime(2024, 1, day),
      );
}

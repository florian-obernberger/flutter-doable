/// A class representing malformed data. For example a json or toml string
/// that did not look like it was expected to.
sealed class MalformedData<T> {
  const MalformedData(this.data);

  final T data;

  @override
  String toString() => 'MalformedData: $data';
}

class CorruptData<T> extends MalformedData<T> {
  const CorruptData(super.data, this.exception);

  final Object exception;

  @override
  String toString() => 'CorruptData: $data\n$exception)';
}

class IncompleteData<T> extends MalformedData<T> {
  const IncompleteData(super.data, {this.helpMsg, this.requiredFields});

  final String? helpMsg;
  final List<String>? requiredFields;

  @override
  String toString() {
    final help = helpMsg == null ? '' : '($helpMsg) ';
    final fields = requiredFields == null
        ? ''
        : "\nrequiredFields=${requiredFields!.join(', ')}";

    return "IncompleteData: $help$data$fields";
  }
}

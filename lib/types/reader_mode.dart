import '/strings/strings.dart';

enum ReaderMode {
  saved,
  edit,
  reader;

  factory ReaderMode.fromString(String source) => switch (source) {
        'edit' => edit,
        'reader' => reader,
        _ => saved,
      };

  // ignore: avoid_positional_boolean_parameters
  factory ReaderMode.fromBool(bool? source) => switch (source) {
        false => edit,
        true => reader,
        null => saved,
      };

  /// Returns true if this is [reader], false if this is [edit] and `null` if
  /// this is [saved].
  bool? get isReader => switch (this) {
        edit => false,
        reader => true,
        saved => null,
      };

  String localizeTitle(Strings strings) => switch (this) {
        edit => strings.editTodo,
        reader => strings.previewTodo,
        saved => strings.lastUsed,
      };
}

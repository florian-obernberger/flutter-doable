import 'package:flutter/material.dart';
import 'package:timezone/timezone.dart' as tz;

import '/strings/strings.dart';
import '/data/todo.dart';

enum NotificationPreference {
  /// Never send notifications
  never,

  /// Only send notifications for Todos that have both a relevant datetime and a
  /// relevant timeofday.
  timeOnly,

  /// Send notifications for every Todo that has a relevant datetime
  all;

  String localizeTitle(Strings strings) => switch (this) {
        never => strings.never,
        all => strings.notifyAll,
        timeOnly => strings.notifyTimeOnly,
      };

  String localizeDescription(Strings strings) => switch (this) {
        never => strings.notifyNeverDescription,
        all => strings.notifyAllDescription,
        timeOnly => strings.notifyTimeOnlyDescription,
      };

  bool shouldNotify(Todo todo) => switch (this) {
        never => false,
        all => todo.relevantDate != null,
        timeOnly => todo.relevantDate != null &&
            todo.relevantTime != null &&
            todo.relevantDateTime!.isAfter(
              DateTime.now().add(const Duration(seconds: 5)),
            )
      };

  tz.TZDateTime? getScheduledDate(
    Todo todo,
    TimeOfDay defaultNotificationTime,
  ) {
    if (todo.relevantDate == null) return null;
    if (todo.relevantTime == null) {
      return tz.TZDateTime.from(
        todo.relevantDate!.copyWith(
          hour: defaultNotificationTime.hour,
          minute: defaultNotificationTime.minute,
          second: 0,
        ),
        tz.local,
      );
    }

    return tz.TZDateTime.from(todo.relevantDateTime!, tz.local);
  }
}

import 'package:format/format.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:result/anyhow.dart';

class Version implements Formatable {
  const Version(this.major, this.minor, this.patch);

  /// Parse a version from X.X.X
  static Result<Version> fromString(String source) {
    try {
      final [major, minor, patch] = source.split('.').map(int.parse).toList();
      return Ok(Version(major, minor, patch));
    } catch (e) {
      return bail(e);
    }
  }

  static Future<Version> current() async {
    final info = await PackageInfo.fromPlatform();
    return fromString(info.version).expect('Current version should be valid');
  }

  factory Version.fromPackageInfo(PackageInfo info) =>
      fromString(info.version).expect('Current version should be valid');

  final int major;
  final int minor;
  final int patch;

  @override
  String toString() => '{}'.format(this);

  String get tag => '{:#}'.format(this);

  @override
  String format(FormatOptions options) {
    if (options.debug) {
      return 'Version(major: $major, minor: $minor, patch: $patch)';
    }

    final src = '${options.alternate ? 'v' : ''}$major.$minor.$patch';
    return options.helper.fill(src);
  }

  @override
  bool operator ==(Object other) => switch (other) {
        Version() =>
          other.major == major && other.minor == minor && other.patch == patch,
        _ => false,
      };

  @override
  int get hashCode => Object.hash(major, minor, patch);

  // this < other
  // this.<(other)
  bool operator <(Version other) =>
      major < other.major ||
      (major == other.major && minor < other.minor) ||
      (major == other.major && minor == other.minor && patch < other.patch);

  bool operator >(Version other) =>
      major > other.major ||
      (major == other.major && minor > other.minor) ||
      (major == other.major && minor == other.minor && patch > other.patch);

  bool operator <=(Version other) => other == this || this < other;
  bool operator >=(Version other) => other == this || this > other;
}

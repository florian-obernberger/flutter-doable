import '/strings/strings.dart';

enum MarkdownEditAction {
  bold,
  italic,
  monospace,
  highlight;

  String localize(Strings strings) => switch (this) {
        bold => strings.markdownBold,
        italic => strings.markdownItalic,
        monospace => strings.markdownMonospace,
        highlight => strings.markdownHighlight,
      };
}

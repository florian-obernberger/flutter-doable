@Deprecated('Use SortingGroup and SortingMetric instead')
enum SortableElement {
  starred,
  today,
  overdue,
  relevantDate,
  noRelevantDate;

  static List<SortableElement> fromString(String value) => value
      .split('')
      .map(int.parse)
      .map((id) => SortableElement.values[id])
      .toList();
}

import 'package:http/http.dart';

class FetchException implements Exception {
  const FetchException(
    this.url, {
    required this.helpMessage,
    required this.originalException,
  });

  final Uri url;
  final String helpMessage;
  final Exception originalException;

  @override
  String toString() {
    return 'Error while trying to fetch `$url`. $helpMessage.\n$originalException';
  }

  bool get isHttpClientException => originalException is ClientException;
  ClientException asHttpClientException() =>
      originalException as ClientException;
}

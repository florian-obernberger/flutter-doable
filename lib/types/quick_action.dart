import '/strings/strings.dart';

enum QuickAction {
  newTodo('@mipmap/ic_shortcut_new'),
  newStarredTodo('@mipmap/ic_shortcut_star_new');

  const QuickAction(this.icon);
  final String icon;

  String get type => name;

  String localizedTitle(Strings strings) => switch (this) {
        newTodo => strings.newTodo,
        newStarredTodo => strings.newStarredTodo,
      };
}

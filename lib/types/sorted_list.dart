import 'dart:collection';

import 'package:flutter/material.dart';

final class SortedList<E> extends ListBase<E> {
  final Comparator<E> _comparator;
  final List<E> _list;

  SortedList(Comparator<E> comparator)
      : _comparator = comparator,
        _list = <E>[];

  SortedList.fromList(Comparator<E> comparator, List<E> list)
      : _comparator = comparator,
        _list = list {
    _list.sort(_comparator);
  }

  @override
  void add(E element) {
    _list.add(element);
    sort();
  }

  @override
  void addAll(Iterable<E> iterable) {
    _list.addAll(iterable);
    sort();
  }

  @override
  int get length => _list.length;

  @override
  set length(int newLength) => _list.length = newLength;

  @override
  List<E> operator +(List<E> other) => (_list + other)..sort(_comparator);

  @override
  void sort([Comparator<E>? compare]) => _list.sort(compare ?? _comparator);

  @protected
  @override
  void insert(int index, E element) {
    _list.insert(index, element);
    sort();
  }

  @protected
  @override
  void insertAll(int index, Iterable<E> iterable) {
    _list.insertAll(index, iterable);
    sort();
  }

  @protected
  @override
  void operator []=(int index, E value) {
    _list[index] = value;
    sort();
  }

  @override
  E operator [](int index) => _list[index];

  @protected
  @override
  void setAll(int index, Iterable<E> iterable) {
    _list.setAll(index, iterable);
    sort();
  }

  @protected
  @override
  void setRange(int start, int end, Iterable<E> iterable, [int skipCount = 0]) {
    _list.setRange(start, end, iterable);
    sort();
  }

  @override
  Set<E> toSet() => _list.toSet();
}

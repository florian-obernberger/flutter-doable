import 'package:format/format.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:result/anyhow.dart';

enum Abi implements Formatable {
  universal('universal'),
  armeabi('armeabi-v7a'),
  arm64('arm64-v8a'),
  x86('x86'),
  x86_64('x86_64');

  const Abi(this.name);

  static Result<Abi> fromIndex(Object source) => switch (source) {
        0 || '0' => const Ok(universal),
        1 || '1' => const Ok(armeabi),
        2 || '2' => const Ok(arm64),
        3 || '3' => const Ok(x86),
        4 || '4' => const Ok(x86_64),
        _ => bail<Abi>(FormatException('Not a valid abi index', source, 0))
      };

  static Future<Abi> current() async {
    final info = await PackageInfo.fromPlatform();
    return fromIndex(info.buildNumber.split('').last).unwrapOr(universal);
  }

  final String name;

  @override
  String format(FormatOptions options) {
    if (options.debug) return 'Abi(name: $name)';

    return options.helper.fill(name);
  }
}

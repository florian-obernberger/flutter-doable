/// Instance of [UnreachableError]. You can throw this to indicate that this
/// specific portion of code should never be able to be reached.
final unreachable = UnreachableError._unreachable();

class UnreachableError extends Error {
  UnreachableError([this.message]) : _isUnreachable = false;

  UnreachableError._unreachable()
      : message = null,
        _isUnreachable = true;

  final String? message;
  final bool _isUnreachable;

  @override
  String toString() {
    if (_isUnreachable) return 'unreachable';

    var message = this.message;
    return (message != null)
        ? 'UnreachableError: $message'
        : 'UnreachableError';
  }
}

import 'package:flutter/material.dart';

import '/strings/strings.dart';
import '/strings/strings_zh.dart';
import '/strings/strings_da.dart';
import '/strings/strings_nl.dart';
import '/strings/strings_en.dart';
import '/strings/strings_eo.dart';
import '/strings/strings_de.dart';
import '/strings/strings_id.dart';
import '/strings/strings_it.dart';
import '/strings/strings_fa.dart';
import '/strings/strings_pl.dart';
import '/strings/strings_ro.dart';
import '/strings/strings_ru.dart';
import '/strings/strings_es.dart';
import '/strings/strings_uk.dart';
import '/state/settings.dart';

enum AppLanguage {
  chinese(Locale('zh')),
  danish(Locale('da')),
  dutch(Locale('nl')),
  english(Locale('en')),
  esperanto(Locale('eo')),
  german(Locale('de')),
  indonesian(Locale('id')),
  italian(Locale('it')),
  persian(Locale('fa')),
  polish(Locale('pl')),
  romanian(Locale('ro')),
  russian(Locale('ru')),
  spanish(Locale('es')),
  ukrainian(Locale('uk')),

  system(null);

  const AppLanguage(this.locale);

  final Locale? locale;

  Strings get _strings => switch (this) {
        chinese => StringsZh(),
        danish => StringsDa(),
        dutch => StringsNl(),
        english => StringsEn(),
        esperanto => StringsEo(),
        german => StringsDe(),
        indonesian => StringsId(),
        italian => StringsIt(),
        persian => StringsFa(),
        polish => StringsPl(),
        romanian => StringsRo(),
        russian => StringsRu(),
        spanish => StringsEs(),
        ukrainian => StringsUk(),
        system => throw UnimplementedError(),
      };

  String nativeName(BuildContext context) {
    if (this == system) {
      return Strings.of(context)!.system;
    }

    return _strings.languageNameInNativeLanguage;
  }

  String englishName(BuildContext context) {
    if (this == system) {
      return Strings.of(context)!.system;
    }

    return _strings.languageNameInEnglish;
  }

  String localizedFor(BuildContext context) {
    final direction = Directionality.of(context);

    if (this == system) {
      return Strings.of(context)!.system;
    }

    return switch (direction) {
      TextDirection.ltr =>
        '${_strings.languageNameInNativeLanguage} (${_strings.languageNameInEnglish})',
      TextDirection.rtl =>
        '(${_strings.languageNameInEnglish}) ${_strings.languageNameInNativeLanguage}'
    };
  }

  factory AppLanguage.of(BuildContext context) {
    final lang = Settings.of(context).general.appLanguage.value;
    if (lang != system) return lang;

    final languageCode = Localizations.localeOf(context).languageCode;
    try {
      return values.firstWhere((l) => l.locale?.languageCode == languageCode);
    } on StateError catch (_) {
      return system;
    }
  }
}

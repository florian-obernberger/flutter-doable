import 'dart:async';
import 'dart:io';

import 'package:crypto/crypto.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:gap/gap.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:material_symbols_icons/symbols.dart';
import 'package:rrule/rrule.dart';
import 'package:share_plus/share_plus.dart';
import 'package:result/result.dart';

import '/strings/strings.dart';
import '/resources/resources.dart';
import '/widgets/star.dart';
import '/widgets/date_picker.dart';
import '/widgets/save_button.dart';
import '/widgets/svg_icon.dart';
import '/widgets/nav_button.dart';
import '/widgets/markdown_text.dart';
import '/widgets/cancel_changes_dialog.dart';
import '/widgets/recurrence/recurrence_bottom_sheet.dart' as rbs;
import '/state/todo_db.dart';
import '/state/todo_list_db.dart';
import '/state/settings.dart';
import '/data/todo.dart';
import '/data/todo_list.dart';
import '/data/applications_directory.dart';
import '/data/notifications/todo_notification.dart';
import '/types/reader_mode.dart';
import '/types/window_breakpoint.dart';
import '/util/animated_dialog.dart';
import '/util/formatters/format_text.dart';
import '/util/build_text_selection_menu.dart';

import 'todo_detail_view.d/todo_image.dart';
import 'todo_detail_view.d/notification_tile.dart';
import 'todo_view.d/snackbar_with_undo.dart';
import 'todo_view.d/todo_list_bottom_sheet.dart';

class TodoDetailsView extends ConsumerStatefulWidget {
  const TodoDetailsView({
    required this.todoId,
    required this.wasNotification,
    super.key,
  });

  final String todoId;
  final bool wasNotification;

  @override
  ConsumerState<ConsumerStatefulWidget> createState() =>
      _TodoDetailsViewState();
}

class _TodoDetailsViewState extends ConsumerState<TodoDetailsView>
    with SingleTickerProviderStateMixin {
  final _formKey = GlobalKey<FormState>();
  late final Todo todo;
  late final Todo oldTodo;

  late final TextEditingController titleController;
  late final TextEditingController descriptionController;

  Set<String> expandedTodos = {};
  Set<String> selectedTodos = {};

  bool get isInSelection => selectedTodos.isNotEmpty;

  RruleL10n? rRuleL10n;

  // Image

  Uint8List? image;

  // Reader mode

  bool? _readerMode;

  bool get readerMode {
    _readerMode ??= widget.wasNotification &&
            settings.extensions.alwaysOpenNotificationsInReaderMode.value ||
        settings.extensions.currentReaderMode.isReader!;

    return _readerMode!;
  }

  set readerMode(bool val) {
    settings.extensions.currentReaderMode = ReaderMode.fromBool(val);
    _readerMode = val;
  }

  Settings? _settings;
  Settings get settings => _settings!;

  @override
  void initState() {
    super.initState();

    todo = ref.read(todoDatabaseProvider).get(widget.todoId)!.clone();
    oldTodo = todo.clone();
    titleController = TextEditingController(text: todo.title);
    descriptionController = TextEditingController(text: todo.description);

    initRRuleL10n();
    initImage();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    _settings ??= Settings.of(context);
  }

  Future<void> initImage() async {
    if (todo.image == null) return;

    image = await todoImagesDirectory.image(todo.id).readAsBytes();
    setState(() {});
    // Ensuring the deletion of the old naming convention
    if (!todo.image!.endsWith(ImageDirectory.imagePath(todo.id))) {
      await File(todo.image!).deleteIfExists();
    }
  }

  Future<void> initRRuleL10n() async {
    rRuleL10n = await RruleL10nEn.create();
    if (mounted) setState(() {});
  }

  @override
  void dispose() {
    super.dispose();

    titleController.dispose();
    descriptionController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context)!;
    final theme = Theme.of(context);

    ref.watch(todoDatabaseProvider);

    final opacity = todo.isCompleted ? 0.8 : 1.0;
    final fabBackground = theme.floatingActionButtonTheme.backgroundColor!;
    final fabForeground = theme.floatingActionButtonTheme.foregroundColor!;

    const elementPadding = EdgeInsets.only(top: 8);

    final contentConfiguration = settings.extensions.enableImages.value
        ? ContentInsertionConfiguration(onContentInserted: (content) {
            if (content.hasData) setState(() => image = content.data!);
          })
        : null;

    return PopScope(
      canPop: false,
      onPopInvoked: (didPop) async {
        if (didPop) return;

        if (isInSelection) {
          clearSelection();
        } else {
          final close = await handleClose();
          if (close && context.mounted) {
            final navigator = Navigator.of(context);
            if (navigator.canPop()) navigator.pop();
          }
        }
      },
      child: Scaffold(
        appBar: buildAppBar(strings),
        floatingActionButtonLocation: FloatingActionButtonLocation.endContained,
        floatingActionButton: TooltipTheme(
          data: TooltipTheme.of(context).copyWith(preferBelow: false),
          child: FloatingActionButton(
            elevation: 0,
            onPressed: onFabPressed,
            backgroundColor: fabBackground.withOpacity(opacity),
            foregroundColor: fabForeground.withOpacity(opacity),
            tooltip: todo.isCompleted
                ? strings.markUncompleted
                : strings.markCompleted,
            child: todo.isCompleted
                ? const SvgIcon(icon: SvgIcons.undoDoneW500)
                : const Icon(Symbols.done, weight: 500),
          ),
        ),
        bottomNavigationBar: buildBottomAppBar(),
        body: GestureDetector(
          onTap:
              isInSelection ? clearSelection : FocusScope.of(context).unfocus,
          child: Form(
            key: _formKey,
            child: ListView(
              padding: const EdgeInsets.all(16),
              physics: const ClampingScrollPhysics(),
              children: <Widget>[
                buildTitle(contentConfiguration),
                const Gap(16),
                buildDescription(contentConfiguration),
                if (settings.extensions.enableLists.value)
                  Padding(
                    padding: elementPadding,
                    child: buildListSelector(),
                  ),
                if (settings.extensions.enableRecurringTasks.value)
                  Visibility(
                    visible: !readerMode || todo.rRuleString != null,
                    child: Padding(
                      padding: elementPadding,
                      child: buildRecurrenceSelector(),
                    ),
                  ),
                if (settings.extensions.enableNotifications.value)
                  Visibility(
                    visible: !readerMode || todo.notificationMetadata != null,
                    child: Padding(
                      padding: elementPadding,
                      child: buildNotifications(),
                    ),
                  ),
                if (settings.extensions.enableImages.value)
                  Visibility(
                    visible: !readerMode || todo.image != null,
                    child: Padding(
                      padding: elementPadding,
                      child: buildImagePicker(),
                    ),
                  ),
                if (settings.extensions.enableProgressBar.value)
                  Visibility(
                    visible: !readerMode || todo.progress != null,
                    child: Padding(
                      padding: elementPadding,
                      child: buildProgressBar(),
                    ),
                  ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget buildTitle(ContentInsertionConfiguration? contentConfiguration) {
    final strings = Strings.of(context)!;
    final scheme = Theme.of(context).colorScheme;
    final text = Theme.of(context).textTheme;

    final titleTextStyle = text.headlineLarge;

    final reader = Padding(
      padding: const EdgeInsets.only(top: 12),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          MarkdownText(
            todo.title,
            maxLines: 4,
            softWrap: true,
            isTitle: true,
            baseStyle: titleTextStyle,
            overflow: TextOverflow.ellipsis,
          ),
          const Gap(8),
          const Divider(),
        ],
      ),
    );

    final edit = TextFormField(
      contextMenuBuilder: buildTextSelectionMenu,
      contentInsertionConfiguration: contentConfiguration,
      minLines: 1,
      maxLines: 4,
      controller: titleController,
      validator: (title) =>
          title == null || title.isEmpty ? strings.titleMayNotBeEmpty : null,
      onChanged: (title) => setState(() => todo.title = title),
      onTapOutside: (_) => FocusScope.of(context).unfocus,
      keyboardType: TextInputType.text,
      autocorrect: true,
      autovalidateMode: AutovalidateMode.onUserInteraction,
      style: titleTextStyle?.copyWith(color: scheme.onSurface),
      decoration: InputDecoration(
        hintText: strings.markRequired(strings.title),
        hintStyle: titleTextStyle?.copyWith(
          color: scheme.onSurfaceVariant,
        ),
        helperText: strings.required,
        suffixIcon: todo.title.isNotEmpty
            ? IconButton(
                tooltip: strings.clearInputField,
                icon: const Icon(Symbols.cancel),
                onPressed: () => setState(() {
                  todo.title = '';
                  titleController.clear();
                }),
              )
            : null,
        suffixIconColor: scheme.onSurfaceVariant,
        border: const UnderlineInputBorder(),
      ),
    );

    return animateSwitch(edit: edit, reader: reader);
  }

  Widget buildDescription(ContentInsertionConfiguration? contentConfiguration) {
    final strings = Strings.of(context)!;
    final scheme = Theme.of(context).colorScheme;
    final text = Theme.of(context).textTheme;

    final textFormField = TextFormField(
      contextMenuBuilder: (context, editableTextState) =>
          buildTextSelectionMenu(
        context,
        editableTextState,
        markdownEnabled: settings.extensions.enableMarkdown.value,
      ),
      contentInsertionConfiguration: contentConfiguration,
      minLines: 1,
      maxLines: 8,
      controller: descriptionController,
      onChanged: (desc) => setState(
        () {
          if (desc.isEmpty) {
            todo.description = null;
            return;
          }
          todo.description = formatText(
            desc,
            trimText: settings.general.trimTodoTexts.value,
          );
        },
      ),
      textInputAction: TextInputAction.newline,
      autocorrect: true,
      style: text.bodyLarge?.copyWith(
        color: scheme.onSurface,
      ),
      decoration: getDecoration(
        scheme,
        text,
        strings.description,
        todo.description != null && todo.description!.isNotEmpty
            ? IconButton(
                tooltip: strings.clearInputField,
                icon: const Icon(Symbols.cancel),
                onPressed: () => setState(() {
                  todo.description = null;
                  descriptionController.clear();
                }),
              )
            : null,
      ),
    );

    final description = InputDecorator(
      decoration: getDecoration(
        scheme,
        text,
        strings.description,
      ),
      isEmpty: todo.description == null,
      child: MarkdownText(
        todo.description ?? '',
        todo: todo,
        supportMarkdown: settings.extensions.enableMarkdown.value,
        useTap: true,
        maxLines: null,
        softWrap: true,
        baseStyle: text.bodyLarge!.copyWith(
          color: todo.description == null
              ? scheme.onSurfaceVariant
              : scheme.onSurface,
          fontSize: text.bodyLarge!.fontSize!,
        ),
      ),
    );

    return Card(
      margin: EdgeInsets.zero,
      child: animateSwitch(
        edit: textFormField,
        reader: description,
        readerPadding: const EdgeInsets.fromLTRB(16, 4, 16, 4),
        editPadding: const EdgeInsets.fromLTRB(16, 4, 8, 4),
      ),
    );
  }

  Map<int, String> getOrder() =>
      settings.extensions.todoListsOrder.value!.asMap();

  Widget buildListSelector() {
    final strings = Strings.of(context)!;
    final scheme = Theme.of(context).colorScheme;
    final text = Theme.of(context).textTheme;

    final Widget content;

    if (todo.listId != null) {
      content = Text(
        ref.read(todoListDatabaseProvider).get(todo.listId!)?.name ??
            strings.noList,
      );
    } else {
      content = Text(strings.noList);
    }

    return Card(
      margin: EdgeInsets.zero,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      child: InkWell(
        onTap: readerMode
            ? null
            : () => showModalSelection<TodoList>(
                  builder: (context) => const TodoListBottomSheet(),
                  handler: (list) => switch (list) {
                    null => null,
                    None() => setState(() => todo.listId = null),
                    Some(value: TodoList(id: final id)) =>
                      setState(() => todo.listId = id),
                  },
                ),
        child: Padding(
          padding: const EdgeInsets.fromLTRB(16, 4, 8, 4),
          child: InputDecorator(
            decoration: getDecoration(scheme, text, strings.list),
            child: DefaultTextStyle(
              style: text.bodyLarge!.copyWith(color: scheme.onSurface),
              child: Container(
                height: 28,
                alignment: Alignment.centerLeft,
                child: content,
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget buildRecurrenceSelector() {
    final strings = Strings.of(context)!;
    final scheme = Theme.of(context).colorScheme;
    final text = Theme.of(context).textTheme;

    final Widget content;
    RecurrenceRule? rRule;

    if (todo.rRuleString != null && rRuleL10n != null) {
      rRule = todo.rRule!;
      if (rRule.canFullyConvertToText) {
        content = Text(rRule.toText(l10n: rRuleL10n!));
      } else {
        content = Text(rRule.toString());
      }
    } else {
      content = Text(strings.never);
    }

    final enabled = todo.relevantDate != null;

    final opacity = enabled ? 1.0 : 0.3;

    return Card(
      margin: EdgeInsets.zero,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      child: InkWell(
        onTap: enabled && !readerMode
            ? () => showModalSelection<RecurrenceRule>(
                  builder: (context) => rbs.RecurrenceBottomSheet(
                    frequency: rbs.Frequency.fromFrequency(rRule?.frequency),
                    interval: rRule?.interval,
                    daysOnly: todo.relevantTime == null,
                  ),
                  handler: (rRule) => switch (rRule) {
                    null => null,
                    None() => setState(() => todo
                      ..rRuleString = null
                      ..hasRecurred = false),
                    Some(value: final rule) => setState(() => todo
                      ..rRuleString = rule.toString()
                      ..hasRecurred = false),
                  },
                )
            : null,
        child: Padding(
          padding: const EdgeInsets.fromLTRB(16, 4, 8, 4),
          child: InputDecorator(
            decoration: getDecoration(
              scheme,
              text,
              strings.rrRepeats,
              todo.rRuleString != null
                  ? IconButton(
                      tooltip: strings.never,
                      icon: const Icon(Symbols.cancel),
                      onPressed: () => setState(() => todo
                        ..rRuleString = null
                        ..hasRecurred = false),
                    )
                  : null,
              opacity,
            ),
            child: DefaultTextStyle(
              style: text.bodyLarge!.copyWith(
                color: scheme.onSurface.withOpacity(opacity),
              ),
              child: Container(
                height: 28,
                alignment: Alignment.centerLeft,
                child: content,
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget buildNotifications() => NotificationTile(
        todo: todo,
        readerMode: readerMode,
        onUpdate: (notifications) async {
          todo.notificationMetadata =
              notifications.isNotEmpty ? notifications : null;
        },
      );

  Widget buildImagePicker() {
    final strings = Strings.of(context)!;
    final scheme = Theme.of(context).colorScheme;
    final text = Theme.of(context).textTheme;

    final style = text.bodyLarge?.copyWith(color: scheme.onSurface);
    final readerMode = this.readerMode;
    final image = this.image;

    Widget child;

    if (image != null) {
      child = Column(
        children: <Widget>[
          SizedBox(
            width: double.infinity,
            child: TodoImage.buffer(
              image: image,
              onTap: readerMode ? null : pickImage,
            ),
          ),
          Padding(
            padding: const EdgeInsetsDirectional.fromSTEB(16, 16, 8, 16),
            child: Row(
              children: <Widget>[
                Text(strings.image, style: style),
                const Spacer(),
                IconButton(
                  onPressed: () => Share.shareXFiles(
                    [XFile.fromData(image, mimeType: 'image/png')],
                    text: todo.title,
                  ),
                  tooltip: strings.share,
                  icon: const Icon(Symbols.share),
                ),
                if (!readerMode)
                  IconButton(
                    tooltip: strings.removeImage,
                    icon: const Icon(Symbols.cancel),
                    onPressed: () => setState(() => this.image = null),
                  ),
              ],
            ),
          ),
        ],
      );
    } else {
      child = Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Material(
            color: scheme.primaryContainer,
            child: InkWell(
              onTap: readerMode ? null : pickImage,
              child: Container(
                color: Colors.transparent,
                alignment: Alignment.center,
                padding: const EdgeInsets.symmetric(vertical: 48),
                child: Icon(
                  Symbols.wallpaper,
                  weight: 300,
                  opticalSize: 24,
                  color: scheme.onPrimaryContainer,
                  size: 64,
                ),
              ),
            ),
          ),
          if (!readerMode)
            Padding(
              padding: const EdgeInsets.all(16),
              child: Text(strings.addImage, style: style),
            ),
        ],
      );
    }

    return Card(
      margin: EdgeInsets.zero,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      child: animateSwitch(edit: child, reader: child),
    );
  }

  Widget buildProgressBar() {
    final strings = Strings.of(context)!;
    final scheme = Theme.of(context).colorScheme;
    final text = Theme.of(context).textTheme;

    return Card(
      margin: EdgeInsets.zero,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      child: Padding(
        padding: const EdgeInsets.fromLTRB(0, 4, 0, 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.fromLTRB(16, 12, 0, 0),
              child: Text(
                strings.progress,
                style: text.titleMedium!
                    .copyWith(height: 1)
                    .merge(text.bodySmall?.copyWith(
                      color: scheme.onSurfaceVariant,
                      fontSize: text.bodySmall!.fontSize! * 0.8,
                    )),
              ),
            ),
            const Gap(16),
            ListTile(
              contentPadding: const EdgeInsets.symmetric(horizontal: 8),
              leading: IconButton(
                onPressed:
                    todo.progress == null || todo.progress == 0 || readerMode
                        ? null
                        : () => incrementProgress(-10),
                color: scheme.onSurface,
                tooltip: '-10%',
                icon: const Icon(Symbols.remove),
              ),
              trailing: IconButton(
                onPressed: todo.progress == 1 || readerMode
                    ? null
                    : () => incrementProgress(10),
                color: scheme.onSurface,
                tooltip: '+10%',
                icon: const Icon(Symbols.add),
              ),
              horizontalTitleGap: -12,
              title: Slider(
                min: 0,
                max: 1,
                // divisions: 10,
                value: todo.progress ?? 0,
                label: NumberFormat.percentPattern().format(todo.progress ?? 0),
                onChanged: (value) {
                  if (readerMode) return;
                  setState(() => todo.progress = value);
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  void incrementProgress(int value) {
    int progress;

    if (todo.progress == null) {
      progress = value;
    } else {
      progress = (todo.progress! * 100).toInt() + value;
    }

    setState(() => todo.progress = progress.clamp(0, 100) / 100);
  }

  Future<void> pickImage() async {
    final imagePicker = ImagePicker();
    final imageFile = await imagePicker.pickImage(source: ImageSource.gallery);
    if (imageFile == null) return;

    final imageData = await imageFile.readAsBytes();
    if (image == null ||
        image!.lengthInBytes != imageData.lengthInBytes ||
        image! != imageData) {
      todo.image = sha256.convert(imageData).toString();
      setState(() => image = imageData);
    }
  }

  InputDecoration getDecoration(
    ColorScheme scheme,
    TextTheme text,
    String label, [
    Widget? suffixIcon,
    double opacity = 1,
    EdgeInsetsGeometry? contentPadding,
  ]) =>
      InputDecoration(
        labelText: label,
        labelStyle: text.bodyLarge?.copyWith(
          color: scheme.onSurfaceVariant.withOpacity(opacity),
        ),
        floatingLabelStyle: text.bodySmall?.copyWith(
          color: scheme.onSurfaceVariant.withOpacity(opacity),
        ),
        suffixIconColor: scheme.onSurfaceVariant,
        suffixIcon: suffixIcon,
        border: InputBorder.none,
        contentPadding: contentPadding,
      );

  void clearSelection() => setState(selectedTodos.clear);

  Widget animateSwitch({
    required Widget edit,
    required Widget reader,
    EdgeInsetsGeometry? editPadding,
    EdgeInsetsGeometry? readerPadding,
  }) {
    final readerMode = this.readerMode;

    return AnimatedCrossFade(
      duration: Durations.short4,
      crossFadeState:
          readerMode ? CrossFadeState.showSecond : CrossFadeState.showFirst,
      firstChild: Padding(
        padding: editPadding ?? EdgeInsets.zero,
        child: edit,
      ),
      secondChild: Padding(
        padding: readerPadding ?? EdgeInsets.zero,
        child: reader,
      ),
    );
  }

  AppBar buildAppBar(Strings strings) {
    final breakpoint = WindowBreakpoint.fromMediaQuery(MediaQuery.of(context));

    return AppBar(
      leading: NavButton.close(
        onNav: () => handleClose().then(
          (close) {
            if (mounted && close) Navigator.of(context).pop();
          },
        ),
      ),
      centerTitle: true,
      title: settings.extensions.enableReaderModer.value
          ? SegmentedButton<bool>(
              multiSelectionEnabled: false,
              selected: {readerMode},
              onSelectionChanged: (selected) {
                HapticFeedback.selectionClick();
                setState(() => readerMode = selected.first);
              },
              segments: <ButtonSegment<bool>>[
                ButtonSegment(
                  value: false,
                  tooltip: breakpoint.isCompact ? strings.editTodo : null,
                  label: breakpoint.isCompact ? null : Text(strings.editTodo),
                  icon: Icon(Symbols.edit, fill: readerMode ? 0 : 1),
                ),
                ButtonSegment(
                  value: true,
                  tooltip: breakpoint.isCompact ? strings.previewTodo : null,
                  label:
                      breakpoint.isCompact ? null : Text(strings.previewTodo),
                  icon: Icon(Symbols.notes, fill: readerMode ? 1 : 0),
                ),
              ],
            )
          : null,
      actions: <Widget>[
        SaveButton(onSave: saveFunction),
        const Gap(16),
      ],
    );
  }

  void onFabPressed() {
    final strings = Strings.of(context)!;

    HapticFeedback.mediumImpact();
    setState(() {
      if (todo.isCompleted) {
        todo.uncomplete();
      } else {
        final copy = todo.clone();
        final db = ref.read(todoDatabaseProvider);

        todo.complete();
        if (settings.general.closeDetailsOnComplete.value &&
            saveFunction != null) {
          showSnackBarWithUndo(
            context: context,
            duration: const Duration(seconds: 2),
            content: Text(strings.completedTodo),
            onUndo: () {
              todo.uncomplete();
              db.store(copy);
              db.scheduleNotifications(ref, copy);
            },
          );
          saveFunction!();
        }
      }
    });
  }

  VoidCallback? get saveFunction => _formKey.currentState?.validate() ?? true
      ? () async {
          final todoDb = ref.read(todoDatabaseProvider);

          final image = this.image;
          final file = todoImagesDirectory.image(todo.id);
          if (image != null) {
            todo.image = sha256.convert(image).toString();

            await file.create(recursive: true);
            await file.writeAsBytes(image);
            if (todo.image != oldTodo.image) await FileImage(file).evict();
          } else {
            await file.deleteIfExists();
            todo.image = null;
          }

          unawaited(Future.wait([
            todoDb.store(todo),
            todoDb.scheduleNotifications(ref, todo),
          ]));

          if (mounted) Navigator.of(context).pop();
        }
      : null;

  Widget buildBottomAppBar() {
    final scheme = Theme.of(context).colorScheme;

    return TooltipTheme(
      data: TooltipTheme.of(context).copyWith(preferBelow: false),
      child: BottomAppBar(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            StarButton(
              unselectedColor: scheme.onSurfaceVariant,
              isStarred: todo.isImportant,
              onTap: (isImportant) {
                setState(() => todo.isImportant = !isImportant);
                return Future.value(!isImportant);
              },
            ),
            TodoDatePicker(
              todo: todo,
              onCleared: () => setState(() {
                removeRelevantNotificationsFromTodo(todo);

                todo
                  ..relevantDate = null
                  ..relevantTime = null;
              }),
              onSelected: (selectedDate, selectedTime) {
                setState(() {
                  removeRelevantNotificationsFromTodo(todo);

                  todo
                    ..relevantDate = selectedDate
                    ..relevantTime = selectedTime;

                  addRelevantNotificationsToTodo(todo, settings.extensions);
                });
              },
              chipOnRight: true,
              chipBackgroundColor: scheme.surfaceContainer,
            ),
          ],
        ),
      ),
    );
  }

  bool get hasEdits => todo != oldTodo;

  Future<bool> handleClose() async {
    if (hasEdits) {
      final strings = Strings.of(context)!;

      return showAnimatedDialog<bool>(
        context: context,
        builder: (context) => CancelChangesDialog(
          title: strings.aboutToCancelChanges,
          description: strings.aboutToCancelChangesDescription,
        ),
      ).then((value) => value == true);
    }

    return true;
  }

  Future<void> showModalSelection<T>({
    required WidgetBuilder builder,
    required void Function(Option<T>? op) handler,
  }) =>
      showModalBottomSheet<Option<T>>(
        context: context,
        builder: builder,
        isDismissible: true,
        isScrollControlled: true,
        enableDrag: true,
        useSafeArea: true,
        showDragHandle: true,
        constraints: const BoxConstraints(maxWidth: 640),
      ).then(handler);

  void onDoubleTap(Todo todo) {
    if (expandedTodos.contains(todo.id)) {
      setState(() => expandedTodos.remove(todo.id));
    } else {
      setState(() => expandedTodos.add(todo.id));
    }
  }

  void onTodoLongPress(Todo todo) {
    if (selectedTodos.contains(todo.id)) {
      setState(() => selectedTodos.remove(todo.id));
    } else {
      setState(() => selectedTodos.add(todo.id));
    }
  }
}

import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:gap/gap.dart';
import 'package:material_symbols_icons/symbols.dart';
import 'package:notified_preferences/notified_preferences.dart';

import '/strings/strings.dart';
import '/data/todo_list.dart';
import '/widgets/nav_button.dart';
import '/state/todo_list_db.dart';
import '/state/todo_db.dart';
import '/state/settings.dart';
import '/util/animated_dialog.dart';

import 'todo_view.d/delete_list_dialog.dart';
import 'todo_view.d/list_dialog.dart';
import 'todo_list_settings_view.d/list_card.dart';

class TodoListsSettingsView extends ConsumerStatefulWidget {
  const TodoListsSettingsView({this.standalone = true, super.key});

  final bool standalone;

  @override
  ConsumerState<TodoListsSettingsView> createState() =>
      _TodoListsSettingsViewState();
}

class _TodoListsSettingsViewState extends ConsumerState<TodoListsSettingsView> {
  PreferenceNotifier<List<String>?>? _listOrder;
  PreferenceNotifier<List<String>?> get listOrder => _listOrder!;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    _listOrder ??= Settings.of(context).extensions.todoListsOrder;
  }

  @override
  void dispose() {
    listOrder.removeListener(listener);

    super.dispose();
  }

  void listener() => setState(() {});

  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context)!;
    final settings = Settings.of(context);
    final theme = Theme.of(context);
    final ThemeData(colorScheme: scheme, textTheme: text) = theme;

    final db = ref.watch(todoListDatabaseProvider);

    final Widget child;
    if (db.values().isEmpty) {
      child = Center(
        child: Padding(
          padding: const EdgeInsets.only(bottom: 92),
          child: Center(
            child: Text(
              strings.noTodoLists,
              style: text.titleMedium?.copyWith(
                color: scheme.onSurfaceVariant,
              ),
            ),
          ),
        ),
      );
    } else {
      final header = Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const Gap(4),
          ValueListenableBuilder(
              valueListenable: settings.extensions.useListColor,
              builder: (context, useListColor, _) {
                return SwitchListTile(
                  title: Text(strings.listColorAsAccent),
                  subtitle: Text(
                    strings.listColorAsAccentDescription,
                  ),
                  isThreeLine: true,
                  value: useListColor,
                  onChanged: (value) =>
                      settings.extensions.useListColor.value = value,
                );
              }),
          const Divider(),
          const Gap(10),
        ],
      );

      final children = getChildren(db);
      final builder = ListCard.recordsBuilder(
        editList: editList,
        deleteList: deleteList,
        hideList: hideList,
      );
      final animatedBuilder = ListCard.builder(
        editList: editList,
        deleteList: deleteList,
        hideList: hideList,
      );

      child = ReorderableListView(
        padding: EdgeInsets.only(
          bottom: MediaQuery.of(context).padding.bottom + 80 + 12,
        ),
        physics: const ClampingScrollPhysics(),
        onReorderStart: (_) => HapticFeedback.vibrate(),
        buildDefaultDragHandles: false,
        onReorder: onReorder,
        proxyDecorator: (child, index, animation) {
          final list = children[index].$1;

          return AnimatedBuilder(
            animation: animation,
            builder: (context, child) {
              final value = lerpDouble(
                1,
                4,
                Curves.easeInOutCubicEmphasized.transform(animation.value),
              )!;

              return Padding(
                padding: EdgeInsets.all(value),
                child: animatedBuilder(
                  list,
                  index,
                  elevation: value,
                ),
              );
            },
          );
        },
        header: header,
        children: children.map(builder).toList(),
      );
    }

    return Scaffold(
      appBar: widget.standalone
          ? AppBar(
              title: Text(strings.lists),
              leading: const NavButton.close(),
            )
          : null,
      floatingActionButton: TooltipTheme(
        data: TooltipTheme.of(context).copyWith(preferBelow: false),
        child: FloatingActionButton(
          onPressed: newList,
          tooltip: strings.newTodoList,
          child: const Icon(Symbols.add, weight: 300, size: 24),
        ),
      ),
      body: child,
    );
  }

  List<(TodoList, int)> getChildren(TodoListDatabase db) {
    final order = listOrder.value!.asMap();
    return [for (var idx in order.keys) (db.get(order[idx]!)!, idx)];
  }

  Future<void> editList(TodoList list) async {
    await showAnimatedDialog<TodoList>(
      context: context,
      animationType: DialogTransitionType.fadeScale,
      builder: (context) => TodoListDialog.edit(todoList: list),
    ).then((newList) {
      if (newList == null) return;
      ref.read(todoListDatabaseProvider).store(newList);
    });
  }

  Future<void> deleteList(TodoList list) async {
    await showAnimatedDialog<bool>(
      context: context,
      builder: (context) => DeleteTodoListDialog(listName: list.name),
    ).then((delete) {
      if (delete == true) {
        final todoDb = ref.read(todoDatabaseProvider);
        final listsDb = ref.read(todoListDatabaseProvider);

        final todoIds = todoDb
            .values()
            .where((todo) => todo.listId == list.id)
            .map((todo) => todo.id)
            .toList();

        listOrder.value = listOrder.value!.toList()..remove(list.id);
        if (mounted) setState(() {});
        listsDb.delete(list.id);
        todoDb.deleteAll(todoIds);
      }
    });
  }

  void hideList(TodoList list, {required bool isHidden}) => ref
      .read(todoListDatabaseProvider)
      .store(list.copyWith(isHidden: isHidden));

  Future<void> newList() {
    return showAnimatedDialog<TodoList>(
      context: context,
      animationType: DialogTransitionType.fadeScale,
      builder: (context) => const TodoListDialog(),
    ).then<void>((newList) {
      if (newList == null) return;
      listOrder.value?.add(newList.id);
      listOrder.value ??= [newList.id];
      ref.read(todoListDatabaseProvider).store(newList);
    });
  }

  void onReorder(int oldIndex, int newIndex) {
    setState(() {
      if (oldIndex < newIndex) {
        newIndex -= 1;
      }

      var list = listOrder.value!.toList();
      final item = list.removeAt(oldIndex);
      list.insert(newIndex, item);
      listOrder.value = list;
    });
  }
}

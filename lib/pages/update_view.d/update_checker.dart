import 'dart:convert';
import 'dart:io';

import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:result/anyhow.dart';

import '/types/abi.dart';
import '/types/version.dart';
import '/util/get_localized_changelog.dart';

class Update {
  static const latestRelease =
      'https://codeberg.org/florian-obernberger/flutter-doable/releases/latest';

  static final _separator = '=' * 80;

  const Update({
    required this.abi,
    required this.version,
    required String changelogs,
  }) : _changelogs = changelogs;

  final Abi abi;
  final Version version;
  final String _changelogs;

  String get fileName => 'doable-$version-${abi.name}.apk';

  String get downloadUrl =>
      'https://codeberg.org/florian-obernberger/flutter-doable/releases/download/latest/$fileName';

  String get signatureUrl => '$downloadUrl.asc';

  String changelog(BuildContext context) => _changelogs
      .split(_separator)
      .asMap()[localizedChangelogPosition(context)]!
      .trim();
}

FutureResult<Update?> checkForUpdate([Version? updateSkip]) async {
  final currentVersion = await Version.current();

  // eg: /florian-obernberger/flutter-doable/releases/tag/v1.12.0
  final locationRes = await getLatestRelease();
  if (locationRes case Err(error: final err)) return Err(err);

  final location = locationRes.unwrap();
  final versionStr = location.split('/').last.replaceFirst('v', '');
  final versionRes = Version.fromString(versionStr);
  if (versionRes case Err(error: final err)) return Err(err);

  final version = versionRes.unwrap();
  if (version <= currentVersion) return const Ok(null);
  if (updateSkip != null && version <= updateSkip) return const Ok(null);

  final changelogsRes = await getChangelog(version);
  if (changelogsRes case Err(error: final err)) return Err(err);

  var changelogs = changelogsRes.unwrap();
  if (changelogs.contains('Not found.')) {
    changelogs = await rootBundle.loadString('assets/info/whats_new.txt');
  }
  return Ok(Update(
    abi: await Abi.current(),
    version: version,
    changelogs: changelogs,
  ));
}

FutureResult<String> getLatestRelease() async {
  try {
    final request = await HttpClient().getUrl(Uri.parse(Update.latestRelease));
    request.followRedirects = false;
    final response = await request.close();

    final location = response.headers.value(HttpHeaders.locationHeader);
    return switch (location) {
      null => bail(Exception('Could not get location')),
      String() => Ok(location),
    };
  } on Exception catch (e) {
    return bail<String>(e).context('Could not get latest release');
  }
}

FutureResult<String> getChangelog(Version version) async {
  try {
    final response = await http.get(Uri.parse(
      'https://codeberg.org/florian-obernberger/flutter-doable/raw/tag/${version.tag}/assets/info/whats_new.txt',
    ));

    return Ok(utf8.decode(response.bodyBytes));
  } on Exception catch (e) {
    return bail<String>(e).context('Could not get changelog');
  }
}

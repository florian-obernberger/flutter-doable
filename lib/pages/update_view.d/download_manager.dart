import 'dart:async';
import 'dart:isolate';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart';
import 'package:dart_pg/dart_pg.dart';
import 'package:result/anyhow.dart';

import '/resources/resources.dart';
import '/classes/logger.dart';
import '/classes/past_value_notifier.dart';

export '/resources/resources.dart' show SigningKeys;

const _loggerTag = 'DownloadManager';

enum DownloadState { idle, downloading, verifying, unsigned, error, done }

class DownloadManager {
  DownloadManager({
    required this.url,
    this.signatureUrl,
    this.onDone,
    this.publicKey = SigningKeys.florianObernbergerPubkey,
    this.publicKeyIsAsset = true,
  })  : progressNotifier = ValueNotifier<double?>(0),
        stateNotifier = PastValueNotifier<DownloadState>(DownloadState.idle);

  final String url;
  final String? signatureUrl;
  final String publicKey;
  final bool publicKeyIsAsset;
  final void Function(List<int> data)? onDone;
  final ValueNotifier<double?> progressNotifier;
  final PastValueNotifier<DownloadState> stateNotifier;

  StreamSubscription<List<int>>? _subscription;
  final _bytes = <int>[];

  Future<void> start() async {
    final client = Client();
    progressNotifier.value = null;

    final request = Request('GET', Uri.parse(url));
    final StreamedResponse response;

    try {
      response = await client.send(request);
    } on Object catch (e, stack) {
      logger.ah(
        Error(e).context('Tried downloading $url but failed.'),
        tag: _loggerTag,
        stack: stack,
      );
      stateNotifier.value = DownloadState.error;
      return;
    }

    final contentLength = response.contentLength;
    // final contentLength = double.parse(response.headers['x-decompressed-content-length']);

    progressNotifier.value = 0;
    stateNotifier.value = DownloadState.downloading;

    _subscription ??= response.stream.listen(
      (newBytes) {
        _bytes.addAll(newBytes);
        final downloadedLength = _bytes.length;
        progressNotifier.value = downloadedLength / contentLength!;
      },
      onDone: () async {
        progressNotifier.value = 1;

        // check public key

        if (signatureUrl != null) {
          stateNotifier.value = DownloadState.verifying;

          final message =
              await OpenPGP.createBinaryMessage(Uint8List.fromList(_bytes));

          final signature = await client
              .betterGet(Uri.parse(signatureUrl!))
              .context('Could not get signature url from $signatureUrl')
              .map((res) => OpenPGP.readSignature(res.body));

          if (signature case Err(:final error)) {
            logger.ah(error, level: LogLevel.debug, tag: _loggerTag);
            stateNotifier.value = DownloadState.error;
            return;
          }

          final PublicKey publicKey;

          if (publicKeyIsAsset) {
            publicKey = await rootBundle
                .loadString(this.publicKey)
                .then(OpenPGP.readPublicKey);
          } else {
            final res = await client
                .betterGet(Uri.parse(this.publicKey))
                .context('Could not fetch public key from ${this.publicKey}')
                .map((res) => OpenPGP.readPublicKey(res.body));

            if (res case Err(:final error)) {
              logger.ah(error, level: LogLevel.debug, tag: _loggerTag);
              stateNotifier.value = DownloadState.error;
              return;
            }

            publicKey = res.unwrap();
          }

          final verified = await Isolate.run(_verify(
            message: message,
            signature: signature.unwrap(),
            publicKey: publicKey,
          ));

          if (!verified) {
            logger.ah(
              Error('Signature for $url did not match'),
              tag: _loggerTag,
              level: LogLevel.warn,
            );
            stateNotifier.value = DownloadState.unsigned;
            return;
          }
        }

        stateNotifier.value = DownloadState.done;
      },
      onError: (e) {
        stateNotifier.value = DownloadState.error;
        if (e is! Object) return;

        logger.ah(
          Error(e).context('Could not download url $url'),
          tag: _loggerTag,
        );
      },
      cancelOnError: true,
    );
  }

  Future<bool> Function() _verify({
    required Message message,
    required Signature signature,
    required PublicKey publicKey,
  }) =>
      () async {
        try {
          final verifications = await message.verifySignature(
            signature,
            [publicKey],
          ).then((message) => message.verifications);

          if (verifications.isEmpty) return false;
          for (final verification in verifications) {
            if (!verification.verified) return false;
          }

          return true;
        } on Exception catch (_) {
          return true;
        }
      };

  List<int>? get data {
    return switch (stateNotifier.value) {
      DownloadState.done || DownloadState.unsigned => _bytes,
      DownloadState.error ||
      DownloadState.downloading ||
      DownloadState.verifying ||
      DownloadState.idle =>
        null,
    };
  }

  Uint8List? get dataBytes {
    final data = this.data;
    if (data == null) return null;
    return Uint8List.fromList(data);
  }

  Future<void> cancel() async {
    stateNotifier.value = DownloadState.idle;
    await _subscription?.cancel();
    _subscription = null;
    _bytes.clear();
  }

  void dispose() {
    _subscription?.cancel();
    stateNotifier.dispose();
    progressNotifier.dispose();
  }
}

extension _BetterGet on Client {
  FutureResult<Response> betterGet(
    Uri url, {
    Map<String, String>? headers,
  }) async {
    try {
      final res = await get(url, headers: headers);
      return Ok(res);
    } on Exception catch (e) {
      return bail(e);
    }
  }
}

import 'package:flutter/material.dart';

import '/strings/strings.dart';
import '/widgets/simple_loading_dialog.dart';

class VerifyingDialog extends StatefulWidget {
  const VerifyingDialog({super.key});

  @override
  State<VerifyingDialog> createState() => _VerifyingDialogState();
}

class _VerifyingDialogState extends State<VerifyingDialog> {
  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context)!;

    return SimpleLoadingDialog(strings.apkVerifyingSignature);
  }
}

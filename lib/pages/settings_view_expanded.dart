import 'package:flutter/material.dart';
import 'package:gap/gap.dart';

import '/strings/strings.dart';
import '/state/settings.dart';
import '/widgets/search.dart' as search;
import '/widgets/back_button.dart' as back;
import '/classes/motion.dart';
import '/resources/oss_licenses.dart';

import 'settings_view.d/settings.dart';
import 'settings_view.d/item.dart';

import 'settings_view.d/general_view.dart';
import 'settings_view.d/design_view.dart';
import 'settings_view.d/sorting_view.dart';
import 'settings_view.d/licenses_view.dart';
import 'settings_view.d/backup_and_sync_view.dart';
import 'settings_view.d/about_view.dart';
import 'settings_view.d/contributor_view.dart';
import 'settings_view.d/extension_view.dart';
import 'settings_view.d/log_view.dart';
import 'settings_view.d/license_detail_view.dart';
import 'settings_view.d/licenses_view.d/search.dart';

class SettingsViewExpanded extends StatefulWidget {
  const SettingsViewExpanded({super.key});

  @override
  State<SettingsViewExpanded> createState() => _SettingsViewExpandedState();
}

class _SettingsViewExpandedState extends State<SettingsViewExpanded> {
  SettingsPage currentPage = SettingsPage.general;
  Package? currentLicense;
  bool showLicensesSearch = false;

  Settings? _settings;
  Settings get settings => _settings!;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    if (_settings == null) {
      _settings = Settings.of(context);

      settings.extensions
        ..enableMarkdown.addListener(listener)
        ..enableNotifications.addListener(listener);
    }
  }

  @override
  void dispose() {
    settings.extensions
      ..enableMarkdown.removeListener(listener)
      ..enableNotifications.removeListener(listener);

    super.dispose();
  }

  void listener() => setState(() {});

  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context)!;
    final scheme = Theme.of(context).colorScheme;
    final text = Theme.of(context).textTheme;

    final decoration = BoxDecoration(borderRadius: BorderRadius.circular(28));

    final licensesSearchPage = search.SearchPage(
      delegate: LicensesSearchDelegate(
          packages: packages,
          onSelected: onLicenseSelected,
          onClose: (_, __) {
            setState(() => showLicensesSearch = false);
          }),
    );

    return Material(
      color: ElevationOverlay.applySurfaceTint(
        scheme.surface,
        scheme.surfaceTint,
        1,
      ),
      child: SafeArea(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.fromLTRB(24, 24, 12, 24),
              child: Container(
                width: 360,
                decoration: decoration,
                clipBehavior: Clip.antiAliasWithSaveLayer,
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: 64,
                      child: Row(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.all(8),
                            child: CircleAvatar(
                              backgroundColor: scheme.surface,
                              radius: 24,
                              child: const back.BackButton(),
                            ),
                          ),
                          const Gap(16),
                          Expanded(
                            child: Text(
                              strings.settings,
                              style: text.headlineSmall?.copyWith(
                                color: scheme.onSurface,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      child: ListView(
                        padding: const EdgeInsets.all(8),
                        physics: const ClampingScrollPhysics(),
                        shrinkWrap: true,
                        children: <Widget>[
                          for (var page in SettingsPage.pages) buildItem(page)
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(12, 24, 24, 24),
                child: Container(
                  decoration: decoration.copyWith(color: scheme.surface),
                  clipBehavior: Clip.antiAliasWithSaveLayer,
                  child: AnimatedSwitcher(
                    duration: Motion.long1,
                    child: showLicensesSearch
                        ? licensesSearchPage
                        : currentPage == SettingsPage.licenses &&
                                currentLicense != null
                            ? LicenseDetailView(
                                currentLicense!,
                                isDialog: true,
                                onBack: () => setState(
                                  () => currentLicense = null,
                                ),
                              )
                            : switch (currentPage) {
                                SettingsPage.general =>
                                  const SettingsGeneralView(showAppBar: false),
                                SettingsPage.sorting =>
                                  const SettingsSortView(showAppBar: false),
                                SettingsPage.design =>
                                  const SettingsDesignView(showAppBar: false),
                                SettingsPage.features =>
                                  const SettingsExtensionView(
                                      showAppBar: false),
                                SettingsPage.backupAndSync =>
                                  const SettingsBackupAndSyncView(
                                      showAppBar: false),
                                SettingsPage.about =>
                                  const SettingsAboutView(showAppBar: false),
                                SettingsPage.contributors =>
                                  const SettingsContributorView(),
                                SettingsPage.licenses => SettingsLicensesView(
                                    showAppBar: false,
                                    onSearch: () => setState(
                                        () => showLicensesSearch = true),
                                    onSelected: onLicenseSelected,
                                  ),
                                SettingsPage.licenseDetail =>
                                  throw UnimplementedError(
                                    'This should never happen!',
                                  ),
                                SettingsPage.logs => const SettingsLogView(),
                              },
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void onLicenseSelected(package) => setState(
        () {
          showLicensesSearch = false;
          currentLicense = package;
        },
      );

  Widget buildItem(SettingsPage page) {
    final strings = Strings.of(context)!;
    final theme = Theme.of(context);
    final scheme = theme.colorScheme;

    final backgroundColor = scheme.primaryContainer;

    final shape = RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(16),
    );

    const margin = EdgeInsets.zero;

    final child = SettingsItem(
      title: page.title(strings),
      description: page.description(strings),
      onTap: () {
        if (currentPage == page) return;
        setState(() {
          currentLicense = null;
          currentPage = page;
        });
      },
      mode: SettingsItemMode.custom,
    );

    if (page != currentPage) {
      return Card.filled(
        shape: shape,
        margin: margin,
        color: Colors.transparent,
        clipBehavior: Clip.hardEdge,
        child: child,
      );
    }

    return Card.filled(
      margin: margin,
      shape: shape,
      clipBehavior: Clip.hardEdge,
      color: backgroundColor,
      child: child,
    );
  }
}

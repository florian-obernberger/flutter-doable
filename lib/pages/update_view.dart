import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_material_design_icons/flutter_material_design_icons.dart';
import 'package:gap/gap.dart';
import 'package:go_router/go_router.dart';
import 'package:material_symbols_icons/material_symbols_icons.dart';
import 'package:material_symbols_icons/symbols.dart';
import 'package:path_provider/path_provider.dart';
import 'package:pick_or_save/pick_or_save.dart';
import 'package:simple_icons/simple_icons.dart';

import '/strings/strings.dart';
import '/state/settings.dart';
import '/data/applications_directory.dart';
import '/widgets/markdown_text.dart';
import '/widgets/nav_button.dart';
import '/util/extensions/build_context.dart';
import '/util/animated_dialog.dart';

import 'settings_view.d/about_view.d/about_card.dart';
import 'settings_view.d/settings.dart';

import 'update_view.d/update_checker.dart';
import 'update_view.d/download_manager.dart';
import 'update_view.d/verifying_dialog.dart';

class UpdateView extends StatefulWidget {
  const UpdateView(this.update, {super.key});

  final Update update;

  @override
  State<UpdateView> createState() => _UpdateViewState();
}

class _UpdateViewState extends State<UpdateView> {
  late final manager = DownloadManager(
    url: widget.update.downloadUrl,
    signatureUrl: widget.update.signatureUrl,
  );

  @override
  void initState() {
    super.initState();

    manager.stateNotifier.addListener(() async {
      if (!mounted) return;

      final state = manager.stateNotifier.value;
      final scheme = Theme.of(context).colorScheme;
      final strings = Strings.of(context)!;

      switch (state) {
        case DownloadState.verifying:
          unawaited(showAnimatedDialog(
            context: context,
            builder: (context) => const VerifyingDialog(),
            barrierDismissible: false,
          ));
        case DownloadState.error:
          if (manager.stateNotifier.previous == DownloadState.verifying) {
            context.pop();
          }

          showSnackBar(strings.couldNotDownloadUpdate);
        case DownloadState.unsigned:
          if (manager.stateNotifier.previous == DownloadState.verifying) {
            context.pop();
          }

          final shouldSave = await showAnimatedDialog<bool>(
            context: context,
            builder: (context) => AlertDialog(
              icon: Icon(Symbols.warning_amber, fill: 0, color: scheme.error),
              title: Text(strings.apkSignatureInvalid),
              content: Text(strings.apkSignatureInvalidDescription),
              actions: [
                TextButton(
                  style: TextButton.styleFrom(foregroundColor: scheme.error),
                  onPressed: Navigator.of(context).pop,
                  child: Text(strings.cancel),
                ),
                FilledButton(
                  style: FilledButton.styleFrom(
                    backgroundColor: scheme.error,
                    foregroundColor: scheme.onError,
                  ),
                  onPressed: () => Navigator.of(context).pop(true),
                  child: Text(strings.install),
                ),
              ],
            ),
          );

          if (shouldSave == true) await saveUpdate();
        case DownloadState.done:
          await saveUpdate();
        default:
          break;
      }
    });
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    Settings.of(context).updates.updateSkip.value = widget.update.version;
    checkForUpdate();
  }

  @override
  void dispose() {
    manager.dispose();
    super.dispose();
  }

  void showSnackBar(String content, [SnackBarAction? action]) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        duration: const Duration(seconds: 2),
        dismissDirection: DismissDirection.vertical,
        margin: const EdgeInsets.fromLTRB(24, 8, 24, 18),
        content: Text(content),
        action: action,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context)!;
    final scheme = Theme.of(context).colorScheme;
    final text = Theme.of(context).textTheme;

    final titleStyle = text.headlineSmall!.copyWith(color: scheme.onSurface);
    const divider = Divider(indent: 16, endIndent: 16);
    const contentPadding = EdgeInsetsDirectional.fromSTEB(24, 4, 16, 4);

    final body = ListView(
      padding: const EdgeInsets.all(12).add(EdgeInsets.only(
        bottom: MediaQuery.of(context).padding.bottom,
      )),
      physics: const ClampingScrollPhysics(),
      children: <Widget>[
        AboutCard.elevated(
          title: strings.whatsNew,
          content: MarkdownText(
            widget.update.changelog(context),
            maxLines: 999,
            softWrap: true,
            supportCodebergIssues: true,
          ),
        ),
        Card(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(16, 16, 16, 4),
                child: Text(strings.whatCanYouDo, style: titleStyle),
              ),
              ListTile(
                contentPadding: contentPadding,
                subtitle: Text(strings.updateRelease),
                leading: const Icon(MdiIcons.web),
                onTap: () => context.launchUrl(Update.latestRelease),
              ),
              divider,
              ValueListenableBuilder(
                  valueListenable: manager.stateNotifier,
                  builder: (context, state, _) {
                    return ListTile(
                      contentPadding: contentPadding,
                      subtitle: Text(strings.updateDownload),
                      leading: const Icon(Symbols.download),
                      onTap: switch (state) {
                        DownloadState.idle => manager.start,
                        _ => manager.cancel,
                      },
                    );
                  }),
              divider,
              ListTile(
                contentPadding: contentPadding,
                subtitle: Text(strings.updateFdroid),
                leading: const Icon(SimpleIcons.fdroid),
                onTap: context.pop,
              ),
              divider,
              ListTile(
                contentPadding: contentPadding,
                subtitle: Text(strings.updateDisable),
                leading: const Icon(Symbols.file_download_off),
                onTap: () => context
                  ..go('/')
                  ..push('/settings', extra: SettingsPage.general),
              ),
              const Gap(16),
            ],
          ),
        ),
      ],
    );

    return ValueListenableBuilder(
      valueListenable: manager.stateNotifier,
      builder: (context, state, _) {
        final bottom = switch (state) {
          DownloadState.downloading => PreferredSize(
              preferredSize: const Size.fromHeight(4),
              child: ValueListenableBuilder(
                valueListenable: manager.progressNotifier,
                builder: (context, value, _) =>
                    LinearProgressIndicator(value: value),
              ),
            ),
          _ => null,
        };

        return Scaffold(
          appBar: AppBar(
            title: Text(strings.versionWithNumber(
              widget.update.version.toString(),
            )),
            bottom: bottom,
            leading: NavButton.close(
              onNav: () {
                Settings.of(context).updates.updateSkip.value = null;
                context.pop();
              },
            ),
          ),
          body: body,
        );
      },
    );
  }

  Future<void> saveUpdate() async {
    final data = manager.dataBytes;
    if (data == null) return;

    final tempDir = await getTemporaryDirectory();
    final tempFile = await tempDir.join(widget.update.fileName).create();
    await tempFile.writeAsBytes(data);

    await PickOrSave().fileSaver(
      params: FileSaverParams(
        mimeTypesFilter: const ['application/vnd.android.package-archive'],
        saveFiles: [
          SaveFileInfo(
            filePath: tempFile.path,
            fileName: widget.update.fileName,
          ),
        ],
      ),
    );

    await tempFile.delete();
    await manager.cancel();

    if (mounted) context.pop();
  }
}

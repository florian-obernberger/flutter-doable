import 'package:flutter/material.dart';
import 'package:gap/gap.dart';
import 'package:go_router/go_router.dart';

import '/strings/strings.dart';

class RouterErrorView extends StatelessWidget {
  const RouterErrorView(this.location, {super.key});

  final String location;

  @override
  Widget build(BuildContext context) {
    final scheme = Theme.of(context).colorScheme;
    final text = Theme.of(context).textTheme;

    final strings = Strings.of(context)!;

    return Scaffold(
      appBar: AppBar(
        title: Text(strings.pageNotFound),
        leading: const SizedBox(),
        leadingWidth: 16,
      ),
      body: Padding(
        padding: const EdgeInsets.all(16),
        child: Card(
          child: Padding(
            padding: const EdgeInsets.all(16),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  strings.routingError,
                  style:
                      text.bodyLarge?.copyWith(color: scheme.onSurfaceVariant),
                ),
                const Gap(8),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    OutlinedButton(
                      onPressed: () => context
                        ..go('/')
                        ..push('/feedback'),
                      child: Text(strings.giveFeedback),
                    ),
                    const Gap(8),
                    FilledButton(
                      onPressed: () => context.go('/'),
                      child: Text(strings.goToHome),
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

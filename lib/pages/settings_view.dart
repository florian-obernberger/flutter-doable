import 'package:animations/animations.dart';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:gap/gap.dart';
import 'package:go_router/go_router.dart';

import '/strings/strings.dart';
import '/state/settings.dart';
import '/classes/motion.dart';
import '/widgets/nav_button.dart';
import '/resources/oss_licenses.dart' show Package;

import 'settings_view.d/item.dart';
import 'settings_view.d/settings.dart';
import 'settings_view.d/about_view.dart';
import 'settings_view.d/backup_and_sync_view.dart';
import 'settings_view.d/contributor_view.dart';
import 'settings_view.d/design_view.dart';
import 'settings_view.d/extension_view.dart';
import 'settings_view.d/log_view.dart';
import 'settings_view.d/general_view.dart';
import 'settings_view.d/licenses_view.dart';
import 'settings_view.d/sorting_view.dart';
import 'settings_view.d/license_detail_view.dart';
import 'settings_view.d/licenses_view.d/dev_icon.dart';

final _navigatorKey = GlobalKey<NavigatorState>();

class _SettingsNavigator extends ValueNotifier<SettingsPage?> {
  _SettingsNavigator([SettingsPage? initialPage])
      : _history = <SettingsPage>[],
        _currentPage = initialPage,
        super(null);

  final List<SettingsPage> _history;
  SettingsPage? _currentPage;

  @override
  SettingsPage? get value => _currentPage;

  @override
  set value(SettingsPage? page) {
    if (_currentPage != null) _history.add(_currentPage!);
    _currentPage = page;
    notifyListeners();
  }

  void pop() {
    _currentPage = _history.isEmpty ? null : _history.removeLast();
    notifyListeners();
  }

  Widget title(BuildContext context, Package? package) {
    final strings = Strings.of(context)!;

    if (_currentPage == null) return Text(strings.settings);
    if (_currentPage == SettingsPage.licenseDetail) {
      assert(package != null);

      return Row(
        children: <Widget>[
          if (package!.isDevDependency) ...[
            const DevIcon(size: 32),
            const Gap(4)
          ],
          Expanded(child: Text(package.name.trim())),
        ],
      );
    }

    return Text(_currentPage!.title(strings));
  }
}

class SettingsView extends ConsumerStatefulWidget {
  const SettingsView({super.key, this.initialPage});

  final SettingsPage? initialPage;

  @override
  ConsumerState<SettingsView> createState() => _SettingsViewState();
}

class _SettingsViewState extends ConsumerState<SettingsView> {
  late final currentPage = _SettingsNavigator();
  Package? currentLicense;

  late final Duration transitionDuration;
  Settings? _settings;
  Settings get settings => _settings!;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    if (_settings == null) {
      _settings = Settings.of(context);

      transitionDuration = settings.accessability.reduceMotion.value
          ? Motion.medium2
          : Motion.medium3;
      settings.accessability.enablePrideFlags.addListener(listener);
    }
  }

  @override
  void dispose() {
    settings.accessability.enablePrideFlags.removeListener(listener);

    super.dispose();
  }

  void listener() => setState(() {});

  @override
  Widget build(BuildContext context) {
    final scheme = Theme.of(context).colorScheme;

    return NavigatorPopHandler(
      onPop: handlePop,
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize: const Size.fromHeight(64),
          child: ValueListenableBuilder(
              valueListenable: currentPage,
              child: NavButton(
                type: NavButtonType.back,
                onNav: () => handlePop(),
              ),
              builder: (context, page, _) {
                final isSettings = page == null;

                return AppBar(
                  key: ValueKey(page),
                  title: currentPage.title(context, currentLicense),
                  leading: NavButton(
                    type: isSettings ? NavButtonType.close : NavButtonType.back,
                    color: scheme.onSurface,
                    onNav: handlePop,
                  ),
                );
              }),
        ),
        body: Navigator(
          key: _navigatorKey,
          initialRoute: widget.initialPage?.route ?? '/settings',
          onGenerateRoute: onGenerateRoute,
        ),
      ),
    );
  }

  Future<void> handlePop() async {
    if (currentPage.value == null) {
      context.pop();
    } else {
      _navigatorKey.currentState?.pop();
      currentPage.pop();
    }
  }

  SettingsPage? pageFromName(String? route) =>
      SettingsPage.values.cast<SettingsPage?>().firstWhere(
            (p) => p?.route == route,
            orElse: () => null,
          );

  Route? onGenerateRoute(RouteSettings settings) {
    final route = settings.name;
    if (route == null) return null;

    final settingsPage = pageFromName(route);

    currentPage.value = settingsPage;

    return PageRouteBuilder(
      pageBuilder: (context, animation, secondaryAnimation) =>
          switch (settingsPage) {
        null => const _SettingsSettingsView(),
        SettingsPage.general => const SettingsGeneralView(showAppBar: false),
        SettingsPage.sorting => const SettingsSortView(showAppBar: false),
        SettingsPage.design => const SettingsDesignView(showAppBar: false),
        SettingsPage.features => const SettingsExtensionView(showAppBar: false),
        SettingsPage.backupAndSync =>
          const SettingsBackupAndSyncView(showAppBar: false),
        SettingsPage.about => const SettingsAboutView(showAppBar: false),
        SettingsPage.contributors => const SettingsContributorView(),
        SettingsPage.licenses => SettingsLicensesView(
            showAppBar: false,
            onSelected: (package) {
              currentLicense = package;
              _navigatorKey.currentState?.pushNamed(
                '/settings/licenses/details',
              );
            },
          ),
        SettingsPage.licenseDetail => LicenseDetailView(
            currentLicense ??
                const Package(
                  name: '',
                  description: '',
                  authors: [],
                  version: '',
                  isMarkdown: false,
                  isSdk: false,
                  isDirectDependency: false,
                  isDevDependency: false,
                ),
            showAppBar: false,
            onBack: () => currentLicense = null,
          ),
        SettingsPage.logs => const SettingsLogView(),
      },
      transitionDuration: transitionDuration,
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        if (this.settings.accessability.reduceMotion.value) {
          return FadeTransition(
            opacity: CurveTween(curve: Easing.standard).animate(animation),
            child: child,
          );
        }

        return SharedAxisTransition(
          animation: animation,
          secondaryAnimation: secondaryAnimation,
          transitionType: SharedAxisTransitionType.horizontal,
          child: child,
        );
      },
    );
  }
}

class _SettingsSettingsView extends StatelessWidget {
  const _SettingsSettingsView();

  @override
  Widget build(BuildContext context) {
    final settings = Settings.of(context);
    // final disablePrideFlags = settings.accessability.disablePrideFlags;

    return Scaffold(
      body: ValueListenableBuilder(
          valueListenable: settings.extensions.enableNotifications,
          builder: (context, _, __) {
            return ListView(
              physics: const ClampingScrollPhysics(),
              children: <Widget>[
                for (var page in SettingsPage.pages)
                  SettingsItem.fromSettingsPage(page, context)
              ],
            );
          }),
    );
  }
}

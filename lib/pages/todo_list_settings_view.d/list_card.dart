import 'package:flutter/material.dart';
import 'package:material_symbols_icons/symbols.dart';

import '/strings/strings.dart';
import '/data/todo_list.dart';
import '/resources/resources.dart';
import '/widgets/rich_tooltip.dart';
import '/widgets/svg_icon.dart';

typedef ListCardAction = void Function(TodoList value);
typedef HideListCardAction = void Function(
  TodoList value, {
  required bool isHidden,
});

class ListCard extends StatelessWidget {
  ListCard({
    required this.list,
    required this.index,
    required this.editList,
    required this.deleteList,
    required this.hideList,
    this.elevation,
  }) : super(key: ValueKey(list.id));

  static ListCard Function(TodoList list, int index, {double? elevation})
      builder({
    required ListCardAction editList,
    required ListCardAction deleteList,
    required HideListCardAction hideList,
  }) =>
          (list, index, {double? elevation}) => ListCard(
                list: list,
                index: index,
                editList: editList,
                deleteList: deleteList,
                hideList: hideList,
                elevation: elevation,
              );

  static ListCard Function((TodoList, int), {double? elevation})
      recordsBuilder({
    required ListCardAction editList,
    required ListCardAction deleteList,
    required HideListCardAction hideList,
  }) =>
          (item, {double? elevation}) => ListCard(
                list: item.$1,
                index: item.$2,
                editList: editList,
                deleteList: deleteList,
                hideList: hideList,
                elevation: elevation,
              );

  // Data

  final TodoList list;
  final int index;

  // Callbacks

  final ListCardAction editList;
  final ListCardAction deleteList;
  final HideListCardAction hideList;

  // Animation

  final double? elevation;

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: elevation,
      key: ValueKey(list.id),
      margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 4),
      child: _buildContent(context),
    );
  }

  Widget _buildContent(BuildContext context) {
    final strings = Strings.of(context)!;
    final ThemeData(
      colorScheme: scheme,
      textTheme: text,
    ) = Theme.of(context);

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        ListTile(
          contentPadding: const EdgeInsetsDirectional.fromSTEB(8, 8, 12, 0),
          leading: _buildHider(strings, scheme),
          title: Text(list.name),
          horizontalTitleGap: 4,
          titleTextStyle: text.bodyLarge?.copyWith(
            color: scheme.onSurface,
            fontSize: 18,
          ),
          trailing: Tooltip(
            message: strings.dragToReorder,
            child: Padding(
              padding: const EdgeInsets.all(4),
              child: SizedBox.square(
                dimension: 40,
                child: ReorderableDragStartListener(
                  key: ValueKey(list.id),
                  index: index,
                  child: Icon(
                    Symbols.drag_handle,
                    color: scheme.onSurfaceVariant,
                    size: 24,
                  ),
                ),
              ),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(12, 0, 12, 12),
          child: Row(
            children: <Widget>[
              OutlinedButton.icon(
                onPressed: () => editList(list),
                label: Text(strings.editTodoList),
                icon: const Icon(Symbols.edit),
              ),
              const Spacer(),
              IconButton.filled(
                style: IconButton.styleFrom(
                  backgroundColor: scheme.error,
                  foregroundColor: scheme.onError,
                ),
                tooltip: strings.deleteTodoList,
                onPressed: () => deleteList(list),
                icon: const Icon(Symbols.delete, weight: 500),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget _buildHider(Strings strings, ColorScheme scheme) {
    return RichTooltip(
      subhead: list.isHidden ? strings.showList : strings.hideList,
      supportingText: list.isHidden
          ? strings.showListDescription
          : strings.hideListDescription,
      child: IconButton(
        onPressed: () => hideList(list, isHidden: !list.isHidden),
        icon: SvgIcon(
          icon: SvgIcons.listHiddenW300,
          color: scheme.secondary,
        ),
        selectedIcon: const SvgIcon(icon: SvgIcons.listW300),
        isSelected: !list.isHidden,
      ),
    );
  }
}

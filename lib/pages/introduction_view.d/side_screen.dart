import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:gap/gap.dart';
import 'package:material_symbols_icons/symbols.dart';

import '/strings/strings.dart';

class IntroItem {
  const IntroItem({required this.title, required this.position});

  final String title;
  final int position;
}

class IntroSideSheet extends ConsumerStatefulWidget {
  const IntroSideSheet({
    required this.items,
    required this.currentPosition,
    required this.onFinish,
    required this.onNext,
    required this.onBack,
    required this.onJump,
    super.key,
  });

  final List<IntroItem> items;
  final int currentPosition;

  final VoidCallback onFinish;
  final VoidCallback onNext;
  final VoidCallback onBack;
  final void Function(int newPosition) onJump;

  @override
  ConsumerState<IntroSideSheet> createState() => _ShareBottomSheetState();
}

class _ShareBottomSheetState extends ConsumerState<IntroSideSheet> {
  @override
  Widget build(BuildContext context) {
    final scheme = Theme.of(context).colorScheme;
    final text = Theme.of(context).textTheme;
    final strings = Strings.of(context)!;

    final lastPosition = widget.items.map((i) => i.position).fold(
          0,
          (p, e) => e > p ? e : p,
        );

    return Material(
      child: Drawer(
        width: 256,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadiusDirectional.only(
            topStart: Radius.circular(28),
            bottomStart: Radius.circular(28),
          ),
        ),
        child: SafeArea(
          child: Column(
            children: <Widget>[
              Expanded(
                child: Material(
                  elevation: 1,
                  surfaceTintColor: scheme.surfaceTint,
                  clipBehavior: Clip.hardEdge,
                  child: ListView(
                    semanticChildCount: widget.items.length,
                    padding: const EdgeInsets.all(12),
                    physics: const ClampingScrollPhysics(),
                    children: <Widget>[
                      for (var item in widget.items)
                        ListTile(
                          title: Text(item.title),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(128),
                          ),
                          onTap: () => widget.onJump(item.position),
                          titleTextStyle: text.bodyLarge?.copyWith(
                              color: item.position == widget.currentPosition
                                  ? scheme.onSecondaryContainer
                                  : scheme.onSurfaceVariant),
                          contentPadding:
                              const EdgeInsets.fromLTRB(16, 8, 24, 8),
                          tileColor: item.position == widget.currentPosition
                              ? scheme.secondaryContainer
                              : Colors.transparent,
                        )
                    ],
                  ),
                ),
              ),
              Divider(color: scheme.outline),
              Container(
                alignment: Alignment.centerLeft,
                height: 76,
                padding: const EdgeInsets.symmetric(horizontal: 24),
                child: Row(
                  children: <Widget>[
                    OutlinedButton.icon(
                      style: OutlinedButton.styleFrom(
                        fixedSize: const Size.fromHeight(48),
                      ),
                      onPressed:
                          widget.currentPosition == 0 ? null : widget.onBack,
                      icon: const Icon(Symbols.arrow_back),
                      label: Text(strings.back),
                    ),
                    const Gap(8),
                    if (widget.currentPosition == lastPosition)
                      FilledButton.icon(
                        style: FilledButton.styleFrom(
                          fixedSize: const Size.fromHeight(48),
                        ),
                        onPressed: widget.onFinish,
                        icon: const Icon(Symbols.check),
                        label: Text(strings.done),
                      )
                    else
                      FilledButton.icon(
                        style: FilledButton.styleFrom(
                          fixedSize: const Size.fromHeight(48),
                        ),
                        onPressed: widget.onNext,
                        icon: const Icon(Symbols.arrow_forward),
                        label: Text(strings.next),
                      ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildChip({
    required String label,
    Widget? avatar,
    required bool selected,
    required ValueChanged<bool>? onSelected,
  }) {
    final scheme = Theme.of(context).colorScheme;
    final text = Theme.of(context).textTheme;

    final double opacity = onSelected == null ? 0.12 : 1;

    final backgroundColor = ElevationOverlay.applySurfaceTint(
      scheme.surface,
      scheme.surfaceTint,
      1,
    );

    final selectedColor = onSelected != null
        ? scheme.secondaryContainer
        : scheme.onSurface.withOpacity(opacity);
    return FilterChip(
      selected: selected,
      disabledColor: backgroundColor,
      selectedColor: selectedColor,
      backgroundColor: backgroundColor,
      onSelected: onSelected,
      label: Text(
        label,
        style: text.labelLarge?.copyWith(
          color: selected
              ? onSelected == null
                  ? scheme.onSurface
                  : scheme.onSecondaryContainer
              : scheme.onSurface,
        ),
      ),
      visualDensity: VisualDensity.compact,
      avatar: avatar,
    );
  }
}

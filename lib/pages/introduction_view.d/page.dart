import 'package:flutter/material.dart';

class IntroPage extends StatelessWidget {
  const IntroPage({
    this.title,
    this.titleWidget,
    this.description,
    this.header,
    this.footer,
    required this.accentTextTheme,
    super.key,
  }) : assert((title == null) != (titleWidget == null));

  final String? title;
  final Widget? titleWidget;
  final String? description;
  final Widget? header;
  final Widget? footer;
  final TextTheme accentTextTheme;

  @override
  Widget build(BuildContext context) {
    final scheme = Theme.of(context).colorScheme;
    final text = Theme.of(context).textTheme;

    return Padding(
      padding: const EdgeInsets.all(16),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          if (header != null)
            Padding(
              padding: const EdgeInsets.only(bottom: 16),
              child: header,
            ),
          titleWidget ??
              Text(
                title!,
                style: accentTextTheme.displaySmall?.copyWith(
                  color: scheme.onSurface,
                  fontWeight: FontWeight.normal,
                ),
                textAlign: TextAlign.center,
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
              ),
          if (description != null)
            Padding(
              padding: const EdgeInsets.only(top: 16),
              child: Text(
                description!,
                style: text.bodyLarge?.copyWith(color: scheme.onSurfaceVariant),
                textAlign: TextAlign.center,
              ),
            ),
          if (footer != null)
            Padding(
              padding: const EdgeInsets.only(top: 16),
              child: footer,
            ),
        ],
      ),
    );
  }
}

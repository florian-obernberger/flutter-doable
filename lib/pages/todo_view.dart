import 'dart:async';

import 'package:after_layout/after_layout.dart';
import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_sharing_intent/flutter_sharing_intent.dart';
import 'package:flutter_sharing_intent/model/sharing_file.dart';
import 'package:go_router/go_router.dart';
import 'package:material_symbols_icons/symbols.dart';
import 'package:quick_actions/quick_actions.dart';
import 'package:result/anyhow.dart' as anyhow;
import 'package:result/result.dart';

import '/classes/logger.dart';
import '/classes/motion.dart';
import '/data/todo.dart';
import '/data/base_notification.dart';
import '/data/notifications/todo_notification.dart' show TodoNotification;
import '/data/todo_list.dart';
import '/remote/handle_io_exception.dart';
import '/remote/types.dart';
import '/remote/util/handle_remote_exception.dart';
import '/state/secure_storage.dart';
import '/state/todo_notif_response.dart';
import '/state/settings.dart';
import '/state/todo_db.dart';
import '/state/todo_list_db.dart';
import '/strings/strings.dart';
import '/widgets/lin_progress_indicator.dart';
import '/types/version.dart';
import '/types/quick_action.dart';
import '/util/animated_dialog.dart';
import '/util/extensions/todo.dart';
import '/util/extensions/date_time_range.dart';
import '/util/extensions/go_router.dart';
import '/util/extensions/list.dart';

import 'settings_view.d/settings.dart';
import 'settings_view.d/backup_and_sync_view.d/nextcloud.d/reload_neon_dialog.dart';
import 'settings_view.d/backup_and_sync_view.d/webdav.d/init_webdav_dialog.dart';
import 'update_view.d/update_checker.dart';

import 'todo_view.d/avatar.dart';
import 'todo_view.d/changelog_dialog.dart';
import 'todo_view.d/date_filter_row.dart';
import 'todo_view.d/date_range_chip.dart';
import 'todo_view.d/delete_dialog.dart';
import 'todo_view.d/delete_list_dialog.dart';
import 'todo_view.d/empty_list.dart';
import 'todo_view.d/todo_list_view.dart';
import 'todo_view.d/list_dialog.dart';
import 'todo_view.d/list_drawer.dart';
import 'todo_view.d/selection_state.dart';
import 'todo_view.d/app_bar_state.dart';
import 'todo_view.d/todo_app_bar.dart';
import 'todo_view.d/snackbar_with_undo.dart';
import 'todo_view.d/todo_bottom_sheet.dart';
import 'todo_view.d/todo_detail_bottom_sheet.dart';
import 'todo_view.d/todo_item.dart';
import 'todo_view.d/todo_list_bottom_sheet.dart';
import 'todo_view.d/todo_search.dart';
import 'todo_view.d/todo_view_menu.dart';

class TodoView extends ConsumerStatefulWidget {
  const TodoView({super.key});

  @override
  ConsumerState<TodoView> createState() => _TodoViewState();
}

class _TodoViewState extends ConsumerState<TodoView>
    with TickerProviderStateMixin, AfterLayoutMixin {
  // Keys

  final _listViewKey = UniqueKey();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  // Settings

  Settings? _settings;
  Settings get settings => _settings!;

  // Databases

  late final todoDb = ref.read(todoDatabaseProvider);
  late final listDb = ref.read(todoListDatabaseProvider);

  // Initialization

  int initializedParts = 0;
  bool get isInitialized => initializedParts >= 2;

  // Loading and syncing

  final appBarState = AppBarState();
  late final LinProgressController loadingController;

  // State

  bool isCreatingNewTodo = false;

  final selectionState = SelectionState();

  bool get isInSelection => selectionState.isNotEmpty;

  TodoList? _currentTodoList;

  TodoList? get currentTodoList => _currentTodoList;

  set currentTodoList(TodoList? list) {
    _currentTodoList = list;
    settings.extensions.lastTodoList.value = list?.id;
  }

  // Version update

  Update? versionUpdate;

  bool get hasVersionUpdate => versionUpdate != null;

  // Animations

  late final AnimationController animatedAppBarController;

  // Scheduler

  late final Timer todoScheduler;
  late final Timer settingsScheduler;

  // Date Range Filters

  (DateRangeType, DateTimeRange)? selectedDateFilter;
  ScrollController dateFilterScrollController = ScrollController();

  @override
  void initState() {
    super.initState();

    FlutterSharingIntent.instance.getMediaStream().listen(
      handleSharing,
      onError: (err) {
        logger.e(err, message: 'Error while receiving sharing intent.');
      },
    );

    animatedAppBarController = AnimationController(
      vsync: this,
      duration: Motion.long1,
      reverseDuration: Motion.long1,
    );

    loadingController = LinProgressController(vsync: this);

    if (Platform.isAndroid) {
      // For sharing images coming from outside the app while the app is closed
      FlutterSharingIntent.instance
          .getInitialSharing()
          .then(handleSharing)
          .then((_) => FlutterSharingIntent.instance.reset());
    }

    todoScheduler = Timer.periodic(
      const Duration(seconds: 5),
      updateTodos,
    );

    settingsScheduler = Timer.periodic(
      const Duration(minutes: 3),
      syncSettings,
    );

    todoNotificationsStream.stream.listen((event) async {
      // Making sure they are initialized before using them. Usually this would
      // be handled by the [initializedParts] variable. Notifications might be
      // handled before that.
      await todoDb.init();
      await listDb.init();

      final todo = todoDb.get(event.todoId);
      await BaseNotification.cancelId(event.notifId);
      if (todo == null) {
        logger.i(
          "Couldn't find Todo",
          error: 'Todo with id ${event.todoId} does not exist in the database',
          tag: 'Notifications',
        );

        return;
      }

      switch (event) {
        case OpenTodoNotifResponse():
          showTodoDetails(todo, wasNotification: true);
        case CompleteTodoNotifResponse():
          completeTodo(todo, strings: mounted ? Strings.of(context)! : null);
        case SnoozeTodoNotifResponse(:final notifId):
          if (todo.relevantDate == null) break;
          await settings.initialize();

          final notif = todo.notificationMetadata
              ?.where((meta) => meta.id.value == notifId)
              .firstOrNull;

          if (notif == null) {
            logger.i('Received response without matching notification metadata',
                more: {
                  'id': notifId.toString(),
                });
            break;
          }

          final snoozed = notif.snooze(
            settings.extensions.notificationSnoozeDuration.value,
          );

          await TodoNotification.from(snoozed, ref, todo).schedule();
      }

      setState(() {});
    });

    if (initialNotification != null) {
      todoNotificationsStream.add(initialNotification!);
      initialNotification = null;
    }
  }

  @override
  Future<void> afterFirstLayout(BuildContext context) async {
    final last = Settings.of(context).updates.lastChangelogVersion;
    final current = await Version.current();

    final show = last.value == null || last.value! < current;
    if (!show || !mounted) return;

    if (context.mounted) {
      await showAnimatedDialog(
        context: context,
        builder: (context) => const ChangelogDialog(),
        animationType: DialogTransitionType.fadeScale,
      );
    }

    last.value = current;
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    if (_settings == null) {
      _settings = Settings.of(context);

      initQuickActions();

      listDb.init().then((_) {
        initializedParts++;

        if (!mounted) return;

        final todoList = settings.extensions.lastTodoList.value;
        if (todoList != null) {
          if (!listDb.contains(todoList)) {
            logger.w("Last used TodoList doesn't exist (anymore)."
                ' `settings.extensions.lastTodoList` will be set to null'
                '`settings.extensions.lastTodoList` = $todoList');
            cleanUpListOrder();
          } else {
            currentTodoList = listDb.get(todoList);
          }
        }
      });

      todoDb.init().then((_) async {
        initializedParts++;

        if (!mounted) return;

        if (settings.backup.synchronize) {
          if (!settings.miscellaneous.migratedToRemotes.value) {
            unawaited(migrateToRemotes());
          } else {
            await pullFromSyncs();
          }
        }

        await getVersionUpdate();
        hideLoading();
      });

      settings.miscellaneous.showCompleted.addListener(listener);
      settings.general.highlightToday.addListener(listener);
      settings.general.highlightOverdue.addListener(listener);
      settings.general.appLanguage.addListener(initQuickActions);
      settings.extensions.enableMarkdown.addListener(listener);
      settings.extensions.enableLists.addListener(resetTodoList);
      settings.extensions.enableTodoSearch.addListener(resetTodoList);
      settings.extensions.enableImages.addListener(listener);
      settings.extensions.enableNotifications.addListener(
        notificationsListener,
      );
      settings.extensions.enableSwipeActions.addListener(listener);
      settings.extensions.selectedSwipeActions.addListener(listener);
      settings.extensions.enableProgressBar.addListener(listener);
      settings.extensions.hideCheckWhenCompleteSelected.addListener(listener);
      settings.extensions.todoListsOrder.addListener(
        () => syncSettings(settingsScheduler, force: true),
      );
      settings.extensions.enableDateFilters.addListener(listener);
      settings.updates.autoUpdates.addListener(updateListener);
      settings.updates.updateSkip.addListener(updateListener);
      settings.design.fontPairing.addListener(listener);
      settings.dateTime.addListener(listener);
      settings.backup.nextcloud.addListener(remoteListener);
      settings.backup.webdav.addListener(remoteListener);
      selectionState.addListener(driveAppBarIcon);
      listDb.addListener(todoListListener);
    }
  }

  @override
  void dispose() {
    settings.miscellaneous.showCompleted.removeListener(listener);
    settings.general.highlightToday.removeListener(listener);
    settings.general.highlightOverdue.removeListener(listener);
    settings.general.appLanguage.removeListener(initQuickActions);
    settings.extensions.enableMarkdown.removeListener(listener);
    settings.extensions.enableLists.removeListener(resetTodoList);
    settings.extensions.enableTodoSearch.removeListener(resetTodoList);
    settings.extensions.enableImages.removeListener(listener);
    settings.extensions.enableNotifications.removeListener(
      notificationsListener,
    );
    settings.extensions.enableSwipeActions.removeListener(listener);
    settings.extensions.selectedSwipeActions.removeListener(listener);
    settings.extensions.enableProgressBar.removeListener(listener);
    settings.extensions.hideCheckWhenCompleteSelected.removeListener(listener);
    settings.extensions.todoListsOrder.removeListener(
      () => syncSettings(settingsScheduler, force: true),
    );
    settings.extensions.enableDateFilters.removeListener(listener);
    settings.updates.autoUpdates.removeListener(updateListener);
    settings.updates.updateSkip.removeListener(updateListener);
    settings.design.fontPairing.removeListener(listener);
    settings.dateTime.removeListener(listener);
    settings.backup.nextcloud.removeListener(remoteListener);
    settings.backup.webdav.removeListener(remoteListener);
    selectionState.removeListener(driveAppBarIcon);
    listDb.removeListener(todoListListener);

    animatedAppBarController.dispose();
    loadingController.dispose();

    dateFilterScrollController.dispose();

    todoScheduler.cancel();
    settingsScheduler.cancel();

    super.dispose();
  }

  Future<void> syncSettings(Timer t, {bool force = false}) async {
    if (!mounted) return;

    final secureStorage = ref.read(secureStorageDbProvider);
    bool cancelled = false;

    for (final impl in RemoteImpl.fromSettings(settings)) {
      final remote = switch (impl) {
        RemoteImpl.nextcloud => await secureStorage.readNextcloudRemote(),
        RemoteImpl.webDAV => await secureStorage.readWebdavRemote(),
        _ => throw UnimplementedError(),
      };

      final res = await settings.syncSettings(remote, force: force);
      if (res case Err(:final error)) {
        final exception = error.downcast<RemoteException>();

        cancelled |= exception.isOkAnd(
          (exc) => exc is TooManyConnectionTimeOuts,
        );

        logger.ah(
          error,
          level: exception.map((exc) => exc.logLevel).unwrapOr(LogLevel.wtf),
          message: exception
              .map((exc) => exc.logMessage)
              .unwrapOr('Unknown exception'),
          tag: 'Settings',
        );
      }
    }

    if (cancelled) t.cancel();
  }

  Future<void> migrateToRemotes() async {
    if (!mounted) return;

    final strings = Strings.of(context)!;
    final scheme = Theme.of(context).colorScheme;
    final text = Theme.of(context).textTheme;

    final shouldMigrate = await showAnimatedDialog<bool>(
      context: context,
      barrierDismissible: false,
      builder: (context) => PopScope(
        canPop: false,
        child: AlertDialog(
          contentTextStyle: text.bodyMedium?.copyWith(
            color: scheme.onSurfaceVariant,
          ),
          icon: const Icon(Symbols.sync),
          title: Text(strings.migrateToRemotes),
          content: ConstrainedBox(
            constraints: const BoxConstraints(maxWidth: 640),
            child: Text(strings.migrateToRemotesDescription),
          ),
          actions: <Widget>[
            TextButton(
              onPressed: () => Navigator.of(context).pop(false),
              child: Text(strings.later),
            ),
            FilledButton(
              onPressed: () => Navigator.of(context).pop(true),
              child: Text(strings.doContinue),
            ),
          ],
        ),
      ),
    );

    if (shouldMigrate != true) return;

    if (settings.backup.nextcloud.value && mounted) {
      await showAnimatedDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) => ReloadNeonDialog(
          null,
          message: strings.migratingNextcloudToRemote,
        ),
      );
    }

    if (settings.backup.webdav.value && mounted) {
      await showAnimatedDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) => InitWebdavDialog(
          null,
          message: strings.migratingWebdavToRemote,
        ),
      );
    }

    await pullFromSyncs();
    await syncSettings(settingsScheduler, force: true);

    settings.miscellaneous.migratedToRemotes.value = true;
  }

  Future<void> updateTodos(Timer t) async {
    await todoDb.deleteCompleted();
    await recurrTodos();
    await notificationsListener();
  }

  Future<void> recurrTodos() async {
    if (!settings.extensions.enableRecurringTasks.value) return;

    // Checking all Todos who have the following properties:
    //
    // 1. they have not yet recurred
    // 2. they have a recurrence rule
    // 3. they are either
    //    - completed or
    //    - overdue
    await Future.wait(todoDb
        .values()
        .where((t) =>
            !t.hasRecurred &&
            t.rRuleString != null &&
            (t.isCompleted || t.isOverdue) &&
            !todoDb.containsNextRecurrence(t))
        .map((t) => todoDb.recurrTodo(ref, t)));
  }

  Future<void> handleSharing(List<SharedFile> files) async {
    if (files.isEmpty) return;

    final shared = files.first;
    final type = shared.type;

    String? text;
    Uint8List? image;

    if (type case SharedMediaType.TEXT || SharedMediaType.URL) {
      text = shared.value;
    } else if (type case SharedMediaType.IMAGE when shared.value != null) {
      if (!settings.extensions.enableImages.value) {
        final strings = Strings.of(context)!;

        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          duration: const Duration(seconds: 4),
          dismissDirection: DismissDirection.vertical,
          margin: const EdgeInsets.fromLTRB(24, 8, 24, 18),
          actionOverflowThreshold: 1,
          action: SnackBarAction(
            label: strings.settings,
            onPressed: () => context.push(
              '/settings',
              extra: SettingsPage.features,
            ),
          ),
          content: Text(strings.mustEnableExtensionForImageSharing(
            '${strings.quoteDoubleLeft}${strings.image}${strings.quoteDoubleRight}',
          )),
        ));

        return;
      }

      final file = File(shared.value!);
      image = await file.readAsBytes();
      await file.delete();
    } else {
      logger.i('Received intent for $type', tag: 'Sharing');
    }

    await Future.delayed(
      const Duration(milliseconds: 50),
      () async {
        if (!settings.extensions.enableLists.value && mounted) {
          newTodo(sharedText: text, image: image);
          return;
        }

        unawaited(newTodoWithList(
          sharedText: text,
          image: image,
          forceSelection: true,
        ));
      },
    );
  }

  Future<void> cleanUpListOrder() async {
    settings.extensions.todoListsOrder.value = settings
        .extensions.todoListsOrder.value
        ?.where((list) => listDb.contains(list))
        .toList();

    if (settings.extensions.todoListsOrder.value == null ||
        settings.extensions.todoListsOrder.value!.isEmpty) {
      settings.extensions.todoListsOrder.value =
          listDb.values().map((l) => l.id).toList();
    }

    final orderLen = settings.extensions.todoListsOrder.value!.length;
    final valuesLen = listDb.values().length;

    if (orderLen == valuesLen) return;
    if (orderLen > valuesLen) {
      settings.extensions.todoListsOrder.value!
          .removeWhere((l) => !listDb.contains(l));
    } else {
      final order = settings.extensions.todoListsOrder.value!;
      settings.extensions.todoListsOrder.value!.addAll(
        listDb.values().where((l) => !order.contains(l.id)).map(((l) => l.id)),
      );
    }
  }

  void todoListListener() {
    if (listDb.values().isEmpty) {
      settings.extensions.todoListsOrder.value?.clear();
      setState(() => currentTodoList = null);
      settings.extensions.lastTodoList.value = null;
      return;
    }

    if (currentTodoList != null) {
      if (!listDb.contains(currentTodoList!.id)) {
        setState(() => currentTodoList = null);
      } else {
        setState(() => currentTodoList = listDb.get(currentTodoList!.id));
      }
    }

    cleanUpListOrder();
  }

  void driveAppBarIcon() {
    if (isInSelection) {
      animatedAppBarController.forward();
    } else {
      animatedAppBarController.reverse();
    }
  }

  Future<void> initQuickActions() async {
    if (!Platform.isAndroid) return;

    final strings = Strings.of(context)!;

    const quickActions = QuickActions();
    await quickActions.initialize((type) async {
      await Future.delayed(const Duration(milliseconds: 50), () {
        if (type == QuickAction.newTodo.type) newTodo();
        if (type == QuickAction.newStarredTodo.type) newTodo(isStarred: true);
      });
    });
    await quickActions.setShortcutItems([
      for (var action in QuickAction.values)
        ShortcutItem(
          type: action.type,
          localizedTitle: action.localizedTitle(strings),
          icon: action.icon,
        ),
    ]);
  }

  void updateListener() {
    settings.updates.updateSkip.value = null;
    getVersionUpdate().then(listener);
  }

  void resetTodoList() =>
      mounted ? setState(() => currentTodoList = null) : null;

  Future<void> notificationsListener() => todoDb.scheduleAllNotifications(ref);

  void listener([_]) => mounted ? setState(() {}) : null;

  void remoteListener() async {
    if (settings.backup.synchronize) return;
    final result = await Future.wait([
      todoDb.resetSync().context('Could not reset todo sync'),
      listDb.resetSync().context('Could not reset list sync'),
    ]).merge();

    if (result case anyhow.Err(:final error)) {
      logger.ah(error, level: LogLevel.debug);
    }

    listener();
  }

  void showLoading() {
    appBarState.loading.value = true;
    loadingController.show();
  }

  void hideLoading() {
    appBarState.loading.value = false;
    loadingController.hide();
  }

  Future<void> getVersionUpdate() async {
    if (settings.updates.autoUpdates.value && Platform.isAndroid) {
      switch (await checkForUpdate(settings.updates.updateSkip.value)) {
        case Err(:final error):
          logger.ah(error, level: LogLevel.debug);
        case Ok(:final value):
          versionUpdate = value;
      }
    } else {
      versionUpdate = null;
    }
  }

  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context)!;

    Widget body;

    if (!isInitialized) {
      body = const SizedBox(
        child: SingleChildScrollView(
          physics: NeverScrollableScrollPhysics(),
          child: EmptyList(),
        ),
      );
    } else {
      final todosList = getTodos(ref.watch(todoDatabaseProvider).values());

      body = TodoListView(
        key: _listViewKey,
        todos: todosList,
        currentList: currentTodoList,
        selectionState: selectionState,
        physics: settings.backup.synchronize
            ? const AlwaysScrollableScrollPhysics()
            : const ClampingScrollPhysics(),
        onOpen: showTodoDetails,
        onSelect: onTodoLongPress,
        onEvent: (event, item) {
          switch (event) {
            case TodoItemEvent.complete:
              completeTodo(item, strings: strings);
            case TodoItemEvent.star:
              todoDb.store(item.copyWith(isImportant: !item.isImportant));
            case TodoItemEvent.delete:
              deleteTodos([item]);
          }
        },
      );
    }

    if (settings.backup.synchronize) {
      final scheme = Theme.of(context).colorScheme;

      body = RefreshIndicator(
        backgroundColor: scheme.secondaryContainer,
        color: scheme.primary,
        notificationPredicate: refreshIndicatorNotificationPredicate,
        onRefresh: () async {
          showLoading();
          await pullFromSyncs(showSuccessSnackbar: true, forceSync: true);
          await syncSettings(settingsScheduler);
          hideLoading();
        },
        child: body,
      );
    }

    // final breakpoint = WindowBreakpoint.of(context);
    final expandedDrawer = MediaQuery.of(context).size.width > 1240;

    final drawer = settings.extensions.enableLists.value
        ? TodoListDrawer(
            closeOnSelected: !expandedDrawer,
            onListSelected: onTodoListSelected,
            initialList: currentTodoList,
          )
        : null;

    body = Stack(
      alignment: Alignment.topCenter,
      children: <Widget>[
        body,
        LinProgressIndicatorScaleTransition(
          controller: loadingController,
          filterQuality: FilterQuality.medium,
          child: const LinProgressIndicator(),
        ),
      ],
    );

    return CallbackShortcuts(
      bindings: {
        ...shortcuts,
        const SingleActivator(LogicalKeyboardKey.keyE): () {},
      },
      child: Focus(
        autofocus: true,
        child: Scaffold(
          key: _scaffoldKey,
          drawerEdgeDragWidth: isInSelection ? 0 : 128,
          drawer: isInitialized ? drawer : null,
          endDrawerEnableOpenDragGesture: false,
          appBar: buildAppBar(
            strings,
            noCompleted: isInitialized
                ? !getTodos().any((todo) => todo.isCompleted)
                : true,
          ),
          floatingActionButton: isCreatingNewTodo ? null : buildFab(),
          body: GestureDetector(
            onTap: isInSelection ? clearSelection : null,
            child: body,
          ),
        ),
      ),
    );
  }

  Map<ShortcutActivator, void Function()> get shortcuts => {
        TodoViewMenuDestination.showCompleted.shortcut!: () {
          if (settings.miscellaneous.showCompleted.value) {
            onPopupSelected(TodoViewMenuDestination.hideCompleted);
          } else {
            onPopupSelected(TodoViewMenuDestination.showCompleted);
          }
        },
        TodoViewMenuDestination.settings.shortcut!: () {
          onPopupSelected(TodoViewMenuDestination.settings);
        },
        const SingleActivator(LogicalKeyboardKey.keyN, control: true):
            newTodoWithList,
        const SingleActivator(
          LogicalKeyboardKey.keyN,
          control: true,
          shift: true,
        ): newTodo,
        TodoViewMenuDestination.selectAll.shortcut!: selectAll,
        if (isInSelection) ...{
          const SingleActivator(LogicalKeyboardKey.escape): clearSelection,
          TodoViewMenuDestination.export.shortcut!: () {
            onPopupSelected(TodoViewMenuDestination.export);
          },
          const SingleActivator(LogicalKeyboardKey.delete): deleteSelected,
        },
      };

  bool clearSelection() {
    selectionState.clear();
    return true;
  }

  void completeTodo(Todo todo, {Strings? strings, bool? isCompleted}) {
    final copy = todo.clone();

    if (isCompleted ?? todo.isCompleted) {
      copy.uncomplete();
      copy.notifications(ref)?.scheduleAll();
    } else {
      copy.complete();
      copy.notifications(ref)?.cancelAll();
      if (strings != null) {
        showSnackBarWithUndo(
          context: context,
          duration: const Duration(seconds: 2),
          content: Text(strings.completedTodo),
          onUndo: () {
            copy.uncomplete();
            todoDb
              ..store(copy)
              ..scheduleNotifications(ref, copy);
          },
        );
      }
    }
    todoDb
      ..store(copy)
      ..scheduleNotifications(ref, copy);
    recurrTodos();
  }

  void deleteTodos(List<Todo> todos) {
    final strings = Strings.of(context)!;
    todoDb.deleteAll(todos.map((todo) => todo.id).toList());

    showSnackBarWithUndo(
      context: context,
      duration: const Duration(seconds: 4),
      content: Text(strings.deletedNTodos(todos.length)),
      onUndo: () => todoDb.storeAll(todos
          .map(
              (todo) => todo.clone(newId: true).copyWith(hadInitialSync: false))
          .toList()),
    );
  }

  List<Todo> getTodos([Iterable<Todo>? iterable]) {
    final todos = (iterable ?? todoDb.values()).toList();

    bool Function(Todo) filter;

    if (currentTodoList == null || !settings.extensions.enableLists.value) {
      filter = (todo) {
        if (todo.listId == null) return true;
        return !(listDb.get(todo.listId!)?.isHidden ?? false);
      };
    } else {
      filter = (todo) => todo.listId == currentTodoList!.id;
    }

    if (settings.extensions.enableDateFilters.value &&
        selectedDateFilter != null) {
      final range = selectedDateFilter!.$2;

      return todos
          .where((todo) => filter(todo) && range.containsTodo(todo))
          .toList();
    }

    return todos.where(filter).toList();
  }

  /// Returns if the pull was successful. Returns `null` if the method is
  /// called while already pulling from syncs
  Future<bool?> pullFromSyncs({
    bool showSuccessSnackbar = false,
    bool? forceSync,
  }) async {
    if (appBarState.pullingFromSyncs.value) return null;

    final strings = Strings.of(context)!;
    final messanger = ScaffoldMessenger.of(context);

    appBarState.pullingFromSyncs.value = true;

    final update = await Future.wait([
      todoDb.syncRemotes(),
      listDb.syncRemotes(),
    ]).toResultEager();

    appBarState.pullingFromSyncs.value = false;

    switch (update) {
      case Err(:final error):
        if (mounted) {
          unawaited(handleRemoteException(
            context,
            error.$1,
            remote: error.$2,
            onWipe: onNeonWipe,
          ));
        }

      case Ok():
        settings.backup.lastSync.value = DateTime.now();

        if (showSuccessSnackbar) {
          messanger.showSnackBar(SnackBar(
            duration: const Duration(seconds: 2),
            dismissDirection: DismissDirection.vertical,
            margin: const EdgeInsets.fromLTRB(24, 8, 24, 18),
            content: Text(strings.syncSuccessful),
          ));
        }
    }

    if (!settings.miscellaneous.migratedToRemotes.value) {
      await todoDb
          // ignore: deprecated_member_use_from_same_package
          .syncTodos(forceSync: forceSync)
          // ignore: deprecated_member_use_from_same_package
          .and(listDb.syncLists(forceSync: forceSync));
    }

    return update.isOk();
  }

  void onTodoListSelected(TodoListDrawerEvent event) {
    setState(() => currentTodoList = event.todoList);
  }

  PreferredSizeWidget buildAppBar(
    Strings strings, {
    required bool noCompleted,
  }) {
    final extensions = settings.extensions;
    final scheme = Theme.of(context).colorScheme;

    final todosList = todoDb.initialized ? getTodos(todoDb.values()) : <Todo>[];
    final uncompleted = todosList.uncompleted;
    final completed = todosList.completed;

    final maxSelection = <String>[
      ...uncompleted.map((t) => t.id),
      if (settings.miscellaneous.showCompleted.value)
        ...completed.map((t) => t.id)
    ];

    final dateFilterRow = extensions.enableDateFilters.value
        ? DateFilterRow(
            controller: dateFilterScrollController,
            selected: selectedDateFilter?.$1,
            onSelect: (type, range) => setState(
              () => selectedDateFilter = (type, range),
            ),
            onDeselect: () => setState(() => selectedDateFilter = null),
          )
        : null;

    final searchIcon = extensions.enableTodoSearch.value
        ? OpenContainer<String>(
            transitionDuration: Durations.medium2,
            transitionType: ContainerTransitionType.fadeThrough,
            closedBuilder: (context, open) => IconButton(
              onPressed: open,
              tooltip: strings.searchTodos,
              icon: const Icon(Symbols.search),
            ),
            closedColor: Colors.transparent,
            closedElevation: 0,
            closedShape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(24),
            ),
            middleColor: scheme.surfaceContainer,
            tappable: false,
            onClosed: (todoId) {
              if (todoId != null && mounted) {
                final todo = todoDb.get(todoId);
                if (todo != null) showTodoDetails(todo);
              }
            },
            openBuilder: (context, close) => TodoSearch(
              todos: getTodos().uncompleted.toList(),
              close: close,
              showList: currentTodoList == null,
            ),
            openColor: scheme.surfaceContainerHigh,
            openElevation: 0,
          )
        : null;

    final todoViewMenu = TodoViewMenu(
      disabled: {if (noCompleted) TodoViewMenuDestination.deleteCompleted},
      destinations: <TodoViewMenuDestination>[
        settings.miscellaneous.showCompleted.value
            ? TodoViewMenuDestination.hideCompleted
            : TodoViewMenuDestination.showCompleted,
        TodoViewMenuDestination.deleteCompleted,
        if (currentTodoList != null) ...[
          TodoViewMenuDestination.divider,
          TodoViewMenuDestination.editList,
          TodoViewMenuDestination.deleteList,
        ],
        TodoViewMenuDestination.divider,
        TodoViewMenuDestination.settings,
        TodoViewMenuDestination.feedback,
        if (hasVersionUpdate) ...[
          TodoViewMenuDestination.divider,
          TodoViewMenuDestination.update
        ]
      ],
      onSelected: onPopupSelected,
      icon: hasVersionUpdate
          ? Badge(
              largeSize: 16,
              label: Icon(
                Symbols.upgrade,
                grade: 200,
                size: 12,
                opticalSize: 20,
                color: Theme.of(context).colorScheme.onError,
              ),
              child: const Avatar(),
            )
          : const Avatar(),
    );

    return TodoAppBar(
      appBarState: appBarState,
      appBarIconAnimationController: animatedAppBarController,
      selectionState: selectionState,
      notificationPredicate: todoListNotificationPredicate,
      maxSelection: maxSelection,
      dateFilterRow: dateFilterRow,
      searchIcon: searchIcon,
      todoViewMenu: todoViewMenu,
      currentList: currentTodoList,
      openDrawer: () => _scaffoldKey.currentState?.openDrawer(),
    );
  }

  bool todoListNotificationPredicate(ScrollNotification notification) =>
      !settings.extensions.enableDateFilters.value &&
      notification.context?.findAncestorWidgetOfExactType<SizedBox>()?.key ==
          _listViewKey;

  bool refreshIndicatorNotificationPredicate(ScrollNotification notification) =>
      notification.context?.findAncestorWidgetOfExactType<SizedBox>()?.key ==
      _listViewKey;

  double? get scrollUnderElevation =>
      settings.extensions.enableDateFilters.value ? 0 : null;

  FloatingActionButton buildFab() {
    final strings = Strings.of(context)!;
    final scheme = Theme.of(context).colorScheme;

    return FloatingActionButton.extended(
      backgroundColor: scheme.primary,
      foregroundColor: scheme.onPrimary,
      onPressed: newTodoWithList,
      label: Text(strings.newTodo),
      icon: const Icon(Symbols.add, weight: 300, size: 24),
    );
  }

  Future<Option<TodoList>?> selectTodoList([String? text]) {
    return showModalBottomSheet<Option<TodoList>>(
      context: context,
      isDismissible: true,
      isScrollControlled: true,
      enableDrag: true,
      useSafeArea: true,
      showDragHandle: true,
      constraints: const BoxConstraints(maxWidth: 640),
      builder: (context) => TodoListBottomSheet(
        sharedText: text,
        chooseAList: true,
      ),
    );
  }

  void newTodo({
    bool? isStarred,
    String? sharedText,
    TodoList? selectedList,
    Uint8List? image,
  }) {
    unawaited(_newTodo(
      isStarred: isStarred,
      image: image,
      list: selectedList ?? currentTodoList,
      sharedText: sharedText,
    ));
  }

  Future<void> newTodoWithList({
    bool? isStarred,
    String? sharedText,
    Uint8List? image,
    bool forceSelection = false,
  }) async {
    if (!settings.extensions.enableLists.value || listDb.isEmpty) {
      return newTodo(isStarred: isStarred, sharedText: sharedText);
    }

    Option<TodoList>? list;
    if (forceSelection || currentTodoList == null) {
      list = await selectTodoList(sharedText);
    } else {
      list = Some(currentTodoList!);
    }

    if (list == null || !mounted) return;

    return _newTodo(
      list: switch (list) {
        None() => null,
        Some(:final value) => value,
      },
      isStarred: isStarred,
      sharedText: sharedText,
      image: image,
    );
  }

  Future<void> _newTodo({
    bool? isStarred,
    String? sharedText,
    TodoList? list,
    Uint8List? image,
  }) async {
    if (isCreatingNewTodo) return;

    isCreatingNewTodo = true;

    unawaited(showModalBottomSheet<Todo?>(
      context: context,
      isDismissible: true,
      isScrollControlled: true,
      enableDrag: true,
      showDragHandle: true,
      constraints: const BoxConstraints(maxWidth: 640),
      builder: (context) => TodoBottomSheet(
        isStarred: isStarred,
        listId: list?.id,
        sharedText: sharedText,
        image: image,
      ),
    ).then((todo) {
      if (todo != null) {
        todoDb
          ..store(todo)
          ..scheduleNotifications(ref, todo);
      }
      setState(() => isCreatingNewTodo = false);
    }));
  }

  void selectAll() {
    final todosList = getTodos(todoDb.values());

    final maxSelection = [
      ...todosList.uncompleted,
      if (settings.miscellaneous.showCompleted.value) ...todosList.completed
    ].map((t) => t.id);

    selectionState.addAll(maxSelection);
  }

  void deleteSelected() {
    final strings = Strings.of(context)!;
    final selected = selectionState.selectedList;

    final deletedTodos = todoDb
        .getAll(selected)
        .map((t) => t.clone(newId: true).copyWith(hadInitialSync: false))
        .toList();

    todoDb.deleteAll(selected.toList());

    showSnackBarWithUndo(
      context: context,
      duration: const Duration(seconds: 4),
      content: Text(strings.deletedNTodos(selected.length)),
      onUndo: () async {
        await todoDb.storeAll(deletedTodos);
      },
    );

    clearSelection();
  }

  FutureOr onPopupSelected(TodoViewMenuDestination destination) {
    final strings = Strings.of(context)!;
    final messanger = ScaffoldMessenger.of(context);

    return switch (destination) {
      TodoViewMenuDestination.showCompleted => settings.miscellaneous
          .showCompleted.value = !settings.miscellaneous.showCompleted.value,
      TodoViewMenuDestination.hideCompleted => settings.miscellaneous
          .showCompleted.value = !settings.miscellaneous.showCompleted.value,
      TodoViewMenuDestination.deleteCompleted => showAnimatedDialog<bool>(
          context: context,
          builder: (context) => const DeleteCompletedDialog(),
        ).then<void>((delete) async {
          if (delete != true) return;
          await todoDb.deleteCompleted(
            listId: currentTodoList?.id,
            ignoreDate: true,
          );
          if (mounted) {
            messanger.showSnackBar(SnackBar(
              duration: const Duration(seconds: 2),
              dismissDirection: DismissDirection.vertical,
              margin: const EdgeInsets.fromLTRB(24, 8, 24, 18),
              content: Text(strings.deletedCompleted),
            ));
          }
        }),
      TodoViewMenuDestination.settings => context.push('/settings'),
      TodoViewMenuDestination.feedback => context.push('/feedback'),
      TodoViewMenuDestination.update =>
        context.push('/update', extra: versionUpdate!),
      TodoViewMenuDestination.deleteList => showAnimatedDialog<bool>(
          context: context,
          builder: (context) => DeleteTodoListDialog(
            listName: currentTodoList?.name ?? strings.lists,
          ),
        ).then<void>((delete) async {
          if (delete == true) {
            final id = currentTodoList!.id;

            final toDelete = todoDb
                .values()
                .where((t) => t.listId == id)
                .map((t) => t.id)
                .toList();

            await listDb.delete(id);
            await todoDb.deleteAll(toDelete);

            final updated = settings.extensions.todoListsOrder.value!.toList()
              ..remove(id);

            settings.extensions
              ..todoListsOrder.value = updated
              ..lastTodoList.value = null;
            if (mounted) setState(() => currentTodoList = null);
          }
        }),
      TodoViewMenuDestination.editList => showAnimatedDialog<TodoList>(
          context: context,
          animationType: DialogTransitionType.fadeScale,
          builder: (context) => TodoListDialog.edit(todoList: currentTodoList!),
        ).then((newList) {
          if (newList == null) return;
          listDb.store(newList);
          setState(() => currentTodoList = newList);
        }),
      TodoViewMenuDestination.export ||
      TodoViewMenuDestination.divider ||
      TodoViewMenuDestination.selectAll =>
        null,
    };
  }

  void showTodoDetails(Todo todo, {bool wasNotification = false}) =>
      context.maybePush('/details/${todo.id}', extra: wasNotification);

  void showTodoDetailsModalBottomSheet(Todo todo) {
    showModalBottomSheet<bool>(
      context: context,
      enableDrag: true,
      isScrollControlled: true,
      backgroundColor: Colors.transparent,
      elevation: 0,
      builder: (context) => TodoDetailBottomSheet(
        todoId: todo.id,
        mediaQuery: MediaQuery.of(this.context),
      ),
    );
  }

  void onTodoLongPress(Todo todo) {
    if (selectionState.contains(todo.id)) {
      selectionState.remove(todo.id);
    } else {
      selectionState.add(todo.id);
    }
  }

  Future<void> onNeonWipe({required bool deleteTodos}) async {
    if (!mounted) return;
    final settings = Settings.of(context);
    final secureStorage = ref.read(secureStorageDbProvider);
    final todoDb = ref.read(todoDatabaseProvider);
    final listDb = ref.read(todoListDatabaseProvider);

    await secureStorage.deleteNextcloud();
    settings.backup.nextcloud.value = false;

    if (deleteTodos) await Future.wait<int>([todoDb.clear(), listDb.clear()]);
  }
}

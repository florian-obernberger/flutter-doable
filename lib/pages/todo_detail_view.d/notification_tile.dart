import 'package:flutter/material.dart';
import 'package:material_symbols_icons/symbols.dart';

import '/strings/strings.dart';
import '/state/settings.dart';
import '/types/sorted_list.dart';
import '/data/notifications/todo_notification.dart';
import '/data/todo.dart';
import '/util/formatters/format_date.dart';

import 'notification_bottom_sheet.dart';

class NotificationTile extends StatefulWidget {
  const NotificationTile({
    required this.todo,
    required this.onUpdate,
    required this.readerMode,
    this.maxNotificationsBeforeCollapse = 2,
    this.opacity = 1,
    super.key,
  });

  final Todo todo;
  final ValueChanged<Set<TodoNotifMeta>> onUpdate;
  final int maxNotificationsBeforeCollapse;
  final double opacity;
  final bool readerMode;

  @override
  State<NotificationTile> createState() => _NotificationTileState();
}

class _NotificationTileState extends State<NotificationTile> {
  Todo get todo => widget.todo;

  late SortedList<TodoNotifMeta> notifications = sortNotifications();

  late bool isExpanded =
      notifications.length <= widget.maxNotificationsBeforeCollapse;

  late bool readerMode = widget.readerMode;

  SortedList<TodoNotifMeta> sortNotifications([Set<TodoNotifMeta>? metadata]) {
    int comparator(TodoNotifMeta a, TodoNotifMeta b) {
      if (a.isRelative && b.isRelative) {
        final comp = a.duration!.compareTo(b.duration!);
        if (comp != 0) return comp;
      }

      return b.scheduledDate.compareTo(b.scheduledDate);
    }

    final notificationMetadata = metadata ?? todo.notificationMetadata;

    if (notificationMetadata == null) return SortedList(comparator);
    return SortedList.fromList(comparator, notificationMetadata.toList());
  }

  @override
  void didUpdateWidget(covariant NotificationTile oldWidget) {
    super.didUpdateWidget(oldWidget);

    final didChange =
        widget.todo.notificationMetadata?.length != notifications.length ||
            (widget.todo.notificationMetadata == null &&
                notifications.isNotEmpty) ||
            widget.todo.notificationMetadata != notifications.toSet();

    if (didChange) {
      notifications = sortNotifications(widget.todo.notificationMetadata);
      setState(() {});
    }

    if (widget.readerMode != readerMode) {
      setState(() => readerMode = widget.readerMode);
    }
  }

  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context)!;
    final ThemeData(colorScheme: scheme, textTheme: text) = Theme.of(context);

    final nrOfNotifications = Text(
      strings.nrOfNotifications(notifications.length),
    );

    if (readerMode) {
      final decoration = InputDecoration(
        labelText: strings.notifications,
        labelStyle: text.bodyLarge!.copyWith(color: scheme.onSurfaceVariant),
        floatingLabelStyle: text.bodySmall!.copyWith(
          color: scheme.onSurfaceVariant,
        ),
        suffixIconColor: scheme.onSurfaceVariant,
        border: InputBorder.none,
      );

      return wrapWithParent(
        child: Padding(
          padding: const EdgeInsets.fromLTRB(16, 4, 8, 4),
          child: InputDecorator(
            decoration: decoration,
            child: DefaultTextStyle(
              style: text.bodyLarge!.copyWith(color: scheme.onSurface),
              child: Container(
                height: 28,
                alignment: Alignment.centerLeft,
                child: nrOfNotifications,
              ),
            ),
          ),
        ),
      );
    }

    return wrapWithParent(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          ListTile(
            enableFeedback: true,
            enabled: notifications.isNotEmpty,
            title: nrOfNotifications,
            onTap: () => setState(() => isExpanded = !isExpanded),
            trailing: AnimatedRotation(
              turns: isExpanded && notifications.isNotEmpty ? 0.5 : 0,
              duration: Durations.short4,
              curve: Easing.standard,
              child: const Icon(Symbols.arrow_drop_down),
            ),
          ),
          AnimatedSize(
            duration: Durations.long3,
            curve: Curves.easeInOutCubicEmphasized,
            alignment: Alignment.topCenter,
            child: isExpanded
                ? Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      for (final metadata in notifications)
                        buildNotification(metadata)
                    ],
                  )
                : const SizedBox(width: double.infinity, height: 0),
          ),
          ListTile(
            onTap: addNotification,
            title: Text(strings.addNotification),
          )
        ],
      ),
    );
  }

  Widget wrapWithParent({required Widget child}) => Card(
        margin: EdgeInsets.zero,
        clipBehavior: Clip.antiAlias,
        child: child,
      );

  Widget buildNotification(TodoNotifMeta metadata) {
    final strings = Strings.of(context)!;
    final settings = Settings.of(context);
    final titleStyle = ListTileTheme.of(context).titleTextStyle;
    final ThemeData(colorScheme: scheme, :disabledColor) = Theme.of(context);

    final shouldNotify = metadata.shouldNotify();

    return ListTile(
      contentPadding: const EdgeInsetsDirectional.only(start: 24, end: 12),
      visualDensity: VisualDensity.comfortable,
      enabled: shouldNotify,
      titleTextStyle: titleStyle?.copyWith(
        color: shouldNotify ? scheme.onSurfaceVariant : disabledColor,
      ),
      trailing: IconButton(
        tooltip: strings.cancel,
        onPressed: () => removeNotification(metadata),
        icon: const Icon(Symbols.cancel, size: 20),
      ),
      title: Text(metadata.isRelative
          ? metadata.duration!.localized(context)
          : formatDateTime(
              settings.dateTime,
              strings,
              metadata.scheduledDate,
              separator: ', ',
            )),
    );
  }

  void removeNotification(TodoNotifMeta metadata) async {
    final removed = notifications.remove(metadata);
    if (!removed) return;

    setState(() {});
    widget.onUpdate(notifications.toSet());
  }

  Future<void> addNotification() async {
    final metadata = await showAnimatedNotificationBottomSheet(
      context: context,
      todo: todo,
    );

    if (metadata == null) return;

    notifications.add(metadata);
    setState(() {});
    widget.onUpdate(notifications.toSet());
  }
}

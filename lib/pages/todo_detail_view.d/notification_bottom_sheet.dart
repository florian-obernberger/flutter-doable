import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:timezone/timezone.dart';

import '/strings/strings.dart';
import '/state/settings.dart';
import '/data/base_notification.dart';
import '/data/notifications/todo_notification.dart';
import '/data/todo.dart';
import '/widgets/date_time_picker.dart';

Future<TodoNotifMeta?> showAnimatedNotificationBottomSheet({
  required BuildContext context,
  required Todo todo,
}) async {
  if (todo.relevantDateTime == null) {
    final scheduledDate = await _showDateTimePicker(context: context);
    if (scheduledDate == null) return null;
    return TodoNotifMeta(NotifId.generate(), scheduledDate);
  } else {
    return await showModalBottomSheet<TodoNotifMeta>(
      context: context,
      isDismissible: true,
      isScrollControlled: true,
      showDragHandle: true,
      enableDrag: true,
      useSafeArea: true,
      builder: (context) => NotificationBottomSheet(
        todo.relevantDateTime!,
      ),
      constraints: const BoxConstraints(maxWidth: 640),
    );
  }
}

class NotificationBottomSheet extends StatelessWidget {
  final DateTime relevantDate;

  const NotificationBottomSheet(this.relevantDate, {super.key});

  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context)!;
    final mediaQuery = MediaQuery.of(context);

    const contentPadding = EdgeInsets.only(left: 20, right: 12);
    final durations = TodoDuration.values.toList();
    durations.sort();

    return Padding(
      padding: EdgeInsets.only(bottom: 16 + mediaQuery.viewPadding.bottom),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          for (final duration in durations)
            ListTile(
              contentPadding: contentPadding,
              title: Text(duration.localized(context)),
              onTap: () => context.pop(TodoNotifMeta(
                todoRelevantId(relevantDate, duration),
                TZDateTime.from(relevantDate, local),
                isRelative: true,
                duration: duration,
              )),
            ),
          ListTile(
            contentPadding: contentPadding,
            title: Text(strings.customNotificationTime),
            onTap: () async {
              final scheduledDate = await _showDateTimePicker(context: context);
              if (context.mounted) {
                context.pop(scheduledDate != null
                    ? TodoNotifMeta(NotifId.generate(), scheduledDate)
                    : null);
              }
            },
          ),
        ],
      ),
    );
  }
}

Future<TZDateTime?> _showDateTimePicker({
  required BuildContext context,
}) async {
  final now = DateTime.now();
  final strings = Strings.of(context)!;
  final scheme = Theme.of(context).colorScheme;
  final defaultTime =
      Settings.of(context).extensions.defaultNotificationTime.value;

  final picked = await showAnimatedDateTimePicker(
    context: context,
    initialDate: now,
    firstDate: now,
    lastDate: DateTime(now.year + 100),
    timeDefaultTime: defaultTime,
    timeInitialTime: defaultTime,
    builder: (context, child) => Theme(
      data: Theme.of(context).copyWith(
        splashFactory: NoSplash.splashFactory,
        colorScheme: scheme.copyWith(
          surface: ElevationOverlay.applySurfaceTint(
            scheme.surface,
            scheme.surfaceTint,
            3,
          ),
        ),
      ),
      child: child,
    ),
    cancelText: strings.cancel,
    confirmText: strings.ok,
    helpText: strings.addNotification,
  );

  if (picked == null) return null;
  var (date, time) = picked;
  time ??= defaultTime;

  date = date.copyWith(
    hour: time.hour,
    minute: time.minute,
    second: 0,
    millisecond: 0,
    microsecond: 0,
  );

  return TZDateTime.from(date, local);
}

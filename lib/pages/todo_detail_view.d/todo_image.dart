import 'dart:typed_data';

import 'package:flutter/material.dart';

import '/data/applications_directory.dart';

class TodoImage extends StatelessWidget {
  TodoImage({
    required String id,
    this.onTap,
    this.borderRadius,
    this.borderColor,
    super.key,
  }) : _image = Image.file(todoImagesDirectory.image(id), fit: BoxFit.cover);

  TodoImage.buffer({
    required Uint8List image,
    this.onTap,
    this.borderRadius,
    this.borderColor,
    super.key,
  }) : _image = Image.memory(image, fit: BoxFit.cover);

  final VoidCallback? onTap;
  final BorderRadius? borderRadius;
  final Color? borderColor;

  final Image _image;

  @override
  Widget build(BuildContext context) {
    final scheme = Theme.of(context).colorScheme;

    final borderRadius = this.borderRadius ?? BorderRadius.circular(12);

    return Material(
      borderOnForeground: true,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12),
        side: BorderSide(color: borderColor ?? scheme.outline),
      ),
      child: InkWell(
        onTap: onTap,
        borderRadius: borderRadius,
        child: ClipRRect(
          borderRadius: borderRadius,
          child: _image,
        ),
      ),
    );
  }
}

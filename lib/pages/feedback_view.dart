import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:gap/gap.dart';
import 'package:go_router/go_router.dart';

import '/strings/strings.dart';
import '/state/settings.dart';
import '/state/secure_storage.dart';
import '/classes/bug_report.dart';
import '/classes/feedback.dart';
import '/util/build_text_selection_menu.dart';
import '/util/animated_dialog.dart';
import '/widgets/nav_button.dart';
import '/widgets/cancel_changes_dialog.dart';

class FeedbackView extends ConsumerStatefulWidget {
  const FeedbackView({super.key});

  @override
  ConsumerState<FeedbackView> createState() => _FeedbackViewState();
}

class _FeedbackViewState extends ConsumerState<FeedbackView> {
  final _formKey = GlobalKey<FormState>();

  FeedbackWay feedbackWay = FeedbackWay.email;
  Set<FeedbackExtra> feedbackExtras = {...FeedbackExtra.values};

  final titleController = TextEditingController();
  final bodyController = TextEditingController();

  var submitted = false;

  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context)!;
    final scheme = Theme.of(context).colorScheme;
    final text = Theme.of(context).textTheme;

    final titleStyle = text.titleMedium?.copyWith(color: scheme.onSurface);
    final descriptionStyle =
        text.titleSmall?.copyWith(color: scheme.onSurfaceVariant);

    final submit =
        _formKey.currentState?.validate() ?? false ? submitFeedback : null;

    return Scaffold(
      appBar: AppBar(
        title: Text(strings.giveFeedback),
        leading: NavButton.close(
          onNav: () => (canPop)
              ? context.pop()
              : checkDiscard().then((close) {
                  if (mounted && close) context.pop();
                }),
        ),
      ),
      persistentFooterButtons: <Widget>[
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
          child: Row(
            children: <Widget>[
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(4),
                  child: submitted
                      ? OutlinedButton(
                          onPressed: submit,
                          child: Text(strings.submitFeedback),
                        )
                      : FilledButton(
                          onPressed: submit,
                          child: Text(strings.submitFeedback),
                        ),
                ),
              ),
              if (submitted)
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(4),
                    child: FilledButton(
                      onPressed: () {
                        context.pop();
                        ScaffoldMessenger.of(context).showSnackBar(
                          SnackBar(
                            duration: const Duration(seconds: 2),
                            dismissDirection: DismissDirection.vertical,
                            margin: const EdgeInsets.fromLTRB(24, 8, 24, 18),
                            content: Text(strings.thankYouForFeedback),
                          ),
                        );
                      },
                      child: Text(strings.done),
                    ),
                  ),
                ),
            ],
          ),
        )
      ],
      body: PopScope(
        canPop: canPop,
        onPopInvoked: (didPop) async {
          if (didPop) return;

          final close = await checkDiscard();
          if (close && context.mounted) Navigator.of(context).pop();
        },
        child: SingleChildScrollView(
          padding: const EdgeInsets.all(24).add(
            EdgeInsets.only(bottom: MediaQuery.of(context).viewPadding.bottom),
          ),
          physics: const ClampingScrollPhysics(),
          child: Form(
            key: _formKey,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  strings.feedbackHeader,
                  style: titleStyle,
                ),
                const Divider(height: 16),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 8.0),
                  child: TextFormField(
                    contextMenuBuilder: buildTextSelectionMenu,
                    minLines: 1,
                    maxLines: 2,
                    controller: titleController,
                    validator: (title) => title == null || title.isEmpty
                        ? strings.titleMayNotBeEmpty
                        : null,
                    keyboardType: TextInputType.text,
                    onChanged: (_) => setState(() {}),
                    autocorrect: true,
                    autovalidateMode: AutovalidateMode.disabled,
                    textInputAction: TextInputAction.next,
                    decoration: InputDecoration(
                      labelText: strings.markRequired(strings.title),
                      hintText: strings.feedbackTitleHelp,
                      alignLabelWithHint: true,
                      helperText: strings.required,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 8.0),
                  child: TextFormField(
                    contextMenuBuilder: buildTextSelectionMenu,
                    minLines: 4,
                    maxLines: 4,
                    controller: bodyController,
                    keyboardType: TextInputType.multiline,
                    autocorrect: true,
                    textInputAction: TextInputAction.newline,
                    decoration: InputDecoration(
                      labelText: strings.description,
                      hintText: strings.feedbackDescriptionHelp,
                      alignLabelWithHint: true,
                    ),
                  ),
                ),
                const Gap(8),
                // FeedbackExtras
                Text(
                  strings.chooseFeedbackExtrasTitle,
                  style: titleStyle,
                ),
                Text(
                  strings.chooseFeedbackExtrasDescription,
                  style: descriptionStyle,
                ),
                const Gap(8),
                buildFeedbackExtraButton(strings),
                const Gap(16),
                // FeedbackWay
                Text(
                  strings.chooseFeedbackWay,
                  style: titleStyle,
                  maxLines: 2,
                ),
                const Gap(8),
                buildFeedbackWayButton(strings),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget buildFeedbackExtraButton(Strings strings) {
    return SizedBox(
      width: double.infinity,
      child: SegmentedButton<FeedbackExtra>(
        segments: <ButtonSegment<FeedbackExtra>>[
          ButtonSegment(
            value: FeedbackExtra.systemInfo,
            label: Text(strings.systemInfo),
          ),
          ButtonSegment(
            value: FeedbackExtra.logOutputs,
            label: Text(strings.logOutputs),
          ),
          ButtonSegment(
            value: FeedbackExtra.settings,
            label: Text(strings.settings),
          ),
        ],
        selected: feedbackExtras,
        multiSelectionEnabled: true,
        emptySelectionAllowed: true,
        onSelectionChanged: (extras) {
          HapticFeedback.selectionClick();
          setState(() => feedbackExtras = extras);
        },
      ),
    );
  }

  Widget buildFeedbackWayButton(Strings strings) {
    return SizedBox(
      width: double.infinity,
      child: SegmentedButton<FeedbackWay>(
        segments: <ButtonSegment<FeedbackWay>>[
          ButtonSegment(
            value: FeedbackWay.email,
            label: Text(strings.email),
          ),
          ButtonSegment(
            value: FeedbackWay.codeberg,
            label: Text(strings.codeberg),
          ),
        ],
        selected: {feedbackWay},
        onSelectionChanged: (way) {
          HapticFeedback.selectionClick();
          setState(() => feedbackWay = way.first);
        },
      ),
    );
  }

  bool get canPop =>
      !(titleController.text.isNotEmpty || bodyController.text.isNotEmpty);

  Future<bool> checkDiscard() async {
    final strings = Strings.of(context)!;

    return showAnimatedDialog<bool>(
      context: context,
      builder: (context) => CancelChangesDialog(
        title: strings.aboutToCancelFeedback,
        description: strings.aboutToCancelFeedbackDescription,
      ),
    ).then((value) => value == true);
  }

  void submitFeedback() async {
    final inAppBrowser = Settings.of(context).general.inAppBrowser.value;
    final secureStorage = ref.read(secureStorageDbProvider);
    final report = await BugReport.fromContext(
      context,
      title: titleController.text.trim(),
      description: bodyController.text.toLowerCase(),
      includeLogs: feedbackExtras.contains(FeedbackExtra.logOutputs),
      includeSettings: feedbackExtras.contains(FeedbackExtra.settings),
      includeSystemInfo: feedbackExtras.contains(FeedbackExtra.systemInfo),
    );

    final res = await giveFeedback(
      way: feedbackWay,
      report: report,
      secureStorage: secureStorage,
      inAppBrowser: inAppBrowser,
    );

    if (res.isOk() && mounted) setState(() => submitted = true);
  }
}

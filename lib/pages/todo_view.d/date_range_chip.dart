import 'package:flutter/material.dart';
import 'package:material_symbols_icons/symbols.dart';

import '/strings/strings.dart';
import '/state/settings.dart';
import '/util/formatters/format_date.dart';
import '/widgets/date_range_picker.dart';

enum DateRangeType { overdue, yesterday, today, next3Days, range }

class DateRangeChip extends StatelessWidget {
  const DateRangeChip({
    required this.type,
    required this.onSelect,
    required this.onDeselect,
    this.selected = false,
    super.key,
  });

  final DateRangeType type;
  final ValueChanged<DateTimeRange> onSelect;
  final VoidCallback onDeselect;
  final bool selected;

  @override
  Widget build(BuildContext context) {
    final ThemeData(colorScheme: scheme, textTheme: text) = Theme.of(context);
    final Widget chip;

    if (type == DateRangeType.range) {
      chip = _DateRangeRangeChip(
        onSelect: onSelect,
        onDeselect: onDeselect,
        selected: selected,
      );
    } else {
      final strings = Strings.of(context)!;

      chip = FilterChip(
        visualDensity: VisualDensity.compact,
        onSelected: (selected) => !selected ? onDeselect() : select(),
        selected: selected,
        label: Text(strings.typeLabel(type)),
      );
    }

    return ChipTheme(
      data: ChipTheme.of(context).copyWith(
        showCheckmark: true,
        deleteIconColor:
            selected ? scheme.onSecondaryContainer : scheme.onSurfaceVariant,
        labelStyle: text.labelLarge!.copyWith(
          color:
              selected ? scheme.onSecondaryContainer : scheme.onSurfaceVariant,
        ),
        backgroundColor: Colors.transparent,
      ),
      child: chip,
    );
  }

  Future<void> select() async {
    final today = DateTime.now();
    final yesterday = today.subtract(const Duration(days: 1));

    final range = switch (type) {
      DateRangeType.overdue => DateTimeRange(
          start: DateTime.fromMillisecondsSinceEpoch(0),
          end: yesterday.endOfDay,
        ),
      DateRangeType.yesterday => DateTimeRange(
          start: yesterday.startOfDay,
          end: yesterday.endOfDay,
        ),
      DateRangeType.today => DateTimeRange(
          start: today.startOfDay,
          end: today.endOfDay,
        ),
      DateRangeType.next3Days => DateTimeRange(
          start: today.startOfDay,
          end: today.add(const Duration(days: 3)).endOfDay,
        ),
      DateRangeType.range => null,
    };

    if (range != null) onSelect(range);
  }
}

class _DateRangeRangeChip extends StatefulWidget {
  const _DateRangeRangeChip({
    required this.onSelect,
    required this.onDeselect,
    this.selected = false,
  });

  final ValueChanged<DateTimeRange> onSelect;
  final VoidCallback onDeselect;
  final bool selected;

  @override
  State<_DateRangeRangeChip> createState() => __DateRangeRangeChipState();
}

class __DateRangeRangeChipState extends State<_DateRangeRangeChip> {
  DateTimeRange? range;

  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context)!;

    return FilterChip(
      selected: widget.selected,
      onSelected: (selected) => !selected ? deselect() : select(),
      visualDensity: VisualDensity.compact,
      onDeleted: select,
      deleteIcon: const Icon(Symbols.arrow_drop_down, size: 18),
      label: Text(
        range != null
            ? formatDateRange(Settings.of(context).dateTime, strings, range!)
            : strings.typeLabel(DateRangeType.range),
      ),
    );
  }

  void deselect() {
    setState(() => range = null);
    widget.onDeselect();
  }

  Future<void> select() async {
    final DateTimeRange? initial;

    if (range != null && !widget.selected) {
      initial = range;
    } else {
      initial = null;
    }

    final selected = initial ??
        await showAnimatedDateRangePicker(
          context: context,
          initialDateRange: range,
          firstDate: DateTime.fromMillisecondsSinceEpoch(0),
          lastDate: DateTime.now().add(const Duration(days: 365 * 10)).endOfDay,
        );

    if (selected != null && mounted) {
      setState(() => range = selected);
      widget.onSelect(selected);
    }
  }
}

extension _DateRangeTypeLabelExtension on Strings {
  String typeLabel(DateRangeType type) => switch (type) {
        DateRangeType.overdue => sortOverdue,
        DateRangeType.yesterday => yesterday,
        DateRangeType.today => today,
        DateRangeType.next3Days => next3Days,
        DateRangeType.range => dateRange,
      };
}

extension _StartEndOfDayExtension on DateTime {
  DateTime get startOfDay => copyWith(
        hour: 0,
        minute: 0,
        second: 0,
        millisecond: 0,
        microsecond: 0,
      );

  DateTime get endOfDay => copyWith(
        hour: 23,
        minute: 59,
        second: 59,
        millisecond: 999,
        microsecond: 999,
      );
}

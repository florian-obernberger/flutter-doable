import 'package:animated_list_plus/animated_list_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';

import '/state/settings.dart';
import '/data/todo.dart';
import '/data/sorting/todo_sorter.dart';
import '/data/todo_list.dart';
import '/util/extensions/todo_importance.dart';
import '/util/extensions/list.dart';

import 'todo_item.dart';
import 'selection_state.dart';
import 'empty_list.dart';

typedef TodoCallback = void Function(Todo todo);
typedef TodoEventCallback = void Function(TodoItemEvent event, Todo todo);

class TodoListView extends ConsumerWidget {
  final List<Todo> todos;
  final TodoList? currentList;
  final SelectionState selectionState;

  final ScrollPhysics physics;

  // [TodoItem] related
  final TodoCallback onOpen;
  final TodoCallback onSelect;
  final TodoEventCallback onEvent;

  const TodoListView({
    required this.todos,
    required this.currentList,
    required this.selectionState,
    required this.physics,
    required this.onOpen,
    required this.onSelect,
    required this.onEvent,
    required super.key,
  });

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final sorting = Settings.of(context).sorting;

    return ListenableBuilder(
      listenable: Listenable.merge([
        sorting.groupBy,
        sorting.sortBy,
        sorting.sortCompleted,
      ]),
      builder: (context, _) => _build(context, ref),
    );
  }

  Widget _build(BuildContext context, WidgetRef ref) {
    final settings = Settings.of(context);
    final accessability = settings.accessability;
    final extensions = settings.extensions;
    final miscellaneous = settings.miscellaneous;

    final sorter = TodoSorter.from(settings.sorting);

    final sorted = sorter.sort(todos);
    final uncompleted = sorted.uncompleted;
    final completed = sorter.sortCompleted(sorted.completed.toList());

    final showCompleted = miscellaneous.showCompleted.value;

    // Check if there are no more Todos to display.
    if (uncompleted.isEmpty && (!showCompleted || completed.isEmpty)) {
      return SizedBox(
        key: key,
        child: SingleChildScrollView(
          physics: physics,
          child: const EmptyList(),
        ),
      );
    }

    final showDivider =
        showCompleted && completed.isNotEmpty && uncompleted.isNotEmpty;

    final padding = const EdgeInsets.all(12) +
        EdgeInsets.only(bottom: MediaQuery.of(context).padding.bottom + 80) -
        EdgeInsets.only(top: extensions.enableDateFilters.value ? 4 : 0);

    final children = animatedChildren(
      accessability: accessability,
      children: <Widget>[
        ...uncompleted.map((todo) => buildTodo(extensions, todo)),
        if (showDivider) const Divider(indent: 16, endIndent: 16),
        if (showCompleted)
          ...completed.map((todo) => buildTodo(extensions, todo))
      ],
    );

    return AnimationLimiter(
      key: ValueKey(currentList?.id),
      child: SizedBox(
        key: key,
        child: ImplicitlyAnimatedList<Widget>(
          padding: padding,
          physics: physics,
          items: children,
          shrinkWrap: false,
          spawnIsolate: false,
          areItemsTheSame: (oldItem, newItem) => oldItem.key == newItem.key,
          insertDuration: Durations.medium3,
          removeDuration: Durations.medium3,
          itemBuilder: (context, animation, item, i) {
            if (accessability.reduceMotion.value) {
              return FadeTransition(opacity: animation, child: item);
            }

            final sizeFactor = CurvedAnimation(
              parent: animation,
              curve: Easing.standard,
            );

            final opacity = CurvedAnimation(
              parent: animation,
              curve: Easing.standardAccelerate,
            );

            return SizeTransition(
              axisAlignment: 0.75,
              sizeFactor: sizeFactor,
              key: item.key,
              child: FadeTransition(
                opacity: opacity,
                child: item,
              ),
            );
          },
        ),
      ),
    );
  }

  Widget buildTodo(ExtensionsPreferences extensions, Todo todo) {
    final local = todo.clone();

    return ListenableBuilder(
      listenable: selectionState,
      builder: (context, todoItem) {
        final isInSelection = selectionState.isNotEmpty;

        final TodoCardState state = selectionState.contains(local.id)
            ? TodoCardState.selected
            : TodoCardState.fromImportance(local.importanceOf(context));

        return TodoCard(
          state: state,
          completed: local.isCompleted,
          onSelect: () => onSelect(local),
          onOpen: () => isInSelection ? onSelect(todo) : onOpen(todo),
          allowSwipeActions: !isInSelection,
          item: todoItem as TodoItem,
        );
      },
      child: TodoItem(
        key: ValueKey('item:${local.hashCode}:${local.isCompleted}'),
        todo: local,
        canInteract: () => !selectionState.isNotEmpty,
        showList: () => extensions.enableLists.value && currentList == null,
        onEvent: onEvent,
      ),
    );
  }

  List<Widget> animatedChildren({
    required AccessabilityPreferences accessability,
    required List<Widget> children,
  }) {
    if (accessability.reduceMotion.value) return children;

    const delay = Duration(milliseconds: 36);

    return children.indexed.map((elem) {
      final (index, widget) = elem;
      return SizedBox(
        key: widget.key,
        child: AnimationConfiguration.staggeredList(
          position: index,
          duration: const Duration(milliseconds: 225),
          delay: delay,
          child: childAnimationBuilder(widget),
        ),
      );
    }).toList();
  }

  Widget childAnimationBuilder(widget) => SlideAnimation(
        curve: Curves.easeInOutCubicEmphasized,
        duration: Durations.medium1,
        verticalOffset: 52,
        child: FadeInAnimation(
          duration: Durations.medium3,
          curve: Curves.easeInOutCubicEmphasized,
          child: widget,
        ),
      );
}

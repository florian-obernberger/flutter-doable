import 'package:flutter/material.dart';
import 'package:gap/gap.dart';
import 'package:go_router/go_router.dart';

import '/strings/strings.dart';
import '/state/settings.dart';
import '/util/build_text_selection_menu.dart';

typedef FedialogResponse = ({String instanceUrl, bool rememberInstance});

class Fedialog extends StatefulWidget {
  const Fedialog({super.key});

  @override
  State<Fedialog> createState() => _FedialogState();
}

class _FedialogState extends State<Fedialog> {
  Settings? _settings;
  Settings get settings => _settings!;

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  late final TextEditingController instanceController;
  late bool rememberInstance;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    if (_settings == null) {
      _settings = Settings.of(context);

      instanceController = TextEditingController(
        text: settings.fediverse.instanceUrl,
      );

      rememberInstance = settings.fediverse.instanceUrl != null;
    }
  }

  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context)!;
    final scheme = Theme.of(context).colorScheme;
    final text = Theme.of(context).textTheme;

    final titleTextStyle = text.bodyLarge;

    return AlertDialog(
      contentPadding: const EdgeInsets.fromLTRB(24, 24, 24, 18),
      title: SizedBox(
        width: 480,
        child: Text(strings.fediChooseInstance),
      ),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Form(
            key: _formKey,
            child: TextFormField(
              contextMenuBuilder: buildTextSelectionMenu,
              controller: instanceController,
              validator: urlValidator,
              onChanged: (_) => setState(() {}),
              onEditingComplete: onToot(),
              keyboardType: TextInputType.text,
              autocorrect: true,
              autofocus: !rememberInstance,
              autovalidateMode: AutovalidateMode.onUserInteraction,
              style: titleTextStyle?.copyWith(color: scheme.onSurface),
              decoration: InputDecoration(
                // helperText: strings.markRequired(strings.fediInstanceUrl),
                hintText: 'https://...',
                hintStyle: titleTextStyle?.copyWith(
                  color: scheme.onSurfaceVariant,
                ),
                helperText: strings.markRequired(strings.fediInstanceUrl),
                border: const UnderlineInputBorder(),
              ),
            ),
          ),
          const Gap(12),
          SwitchListTile(
            value: rememberInstance,
            onChanged: (value) => setState(() => rememberInstance = value),
            title: Text(strings.fediRememberInstance),
            contentPadding: const EdgeInsetsDirectional.only(start: 4),
          ),
        ],
      ),
      actions: <Widget>[
        TextButton(
          onPressed: Navigator.of(context).pop,
          child: Text(strings.cancel),
        ),
        FilledButton(
          onPressed: onToot(),
          child: Text(strings.post),
        ),
      ],
    );
  }

  void Function()? onToot() {
    return (_formKey.currentState?.validate() ?? rememberInstance)
        ? () => context.pop<FedialogResponse>((
              instanceUrl: instanceController.text,
              rememberInstance: rememberInstance,
            ))
        : null;
  }

  String? urlValidator(String? text) {
    final strings = Strings.of(context)!;

    if (text == null || text.isEmpty) return strings.mayNotBeEmpty;

    if (Uri.tryParse(text) == null) {
      return strings.notAValidUrl;
    }

    return null;
  }
}

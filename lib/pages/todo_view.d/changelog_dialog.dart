import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';

import '/strings/strings.dart';
import '/util/app_info.dart';
import '/util/get_localized_changelog.dart';
import '/widgets/async_builder.dart';
import '/widgets/markdown_text.dart';

final separator = '=' * 80;

class ChangelogDialog extends ConsumerWidget {
  const ChangelogDialog({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final strings = Strings.of(context)!;

    final (changelog, version) =
        ref.watch(appInfoProvider).when<(Future<String>, String)>(
              data: (info) {
                final changelog = rootBundle
                    .loadString('assets/info/whats_new.txt')
                    .then((changelogs) => changelogs
                        .split(separator)
                        .asMap()[localizedChangelogPosition(context)]!
                        .trim());

                return (changelog, (info.version));
              },
              error: (_, __) => (Future.value(''), '?.??.?'),
              loading: () => (Future.value(''), '?.??.?'),
            );

    final body = changelog.mapDataOrElse(
      data: (context, changelog) => MarkdownText(
        changelog.replaceAll('• ', '- '),
        maxLines: 999,
        softWrap: true,
        supportCodebergIssues: true,
      ),
      orElse: (_) => const SizedBox.square(
        dimension: 72,
        child: Center(child: CircularProgressIndicator()),
      ),
    );

    return AlertDialog(
      title: Text(strings.whatsNewIn(version)),
      icon: const Text(
        '🔖',
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: 24),
      ),
      content: ConstrainedBox(
        constraints: const BoxConstraints(maxWidth: 480, maxHeight: 360),
        child: Column(
          children: <Widget>[
            const Divider(indent: 0, endIndent: 0),
            Expanded(
              child: SizedBox(
                width: 480,
                child: SingleChildScrollView(
                  physics: const ClampingScrollPhysics(),
                  child: body,
                ),
              ),
            ),
            const Divider(indent: 0, endIndent: 0),
          ],
        ),
      ),
      actions: <Widget>[
        FilledButton(
          onPressed: context.pop,
          child: Text(strings.gotIt),
        ),
      ],
    );
  }
}

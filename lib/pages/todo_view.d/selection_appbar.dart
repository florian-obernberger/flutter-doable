import 'dart:async';

import 'package:add_2_calendar/add_2_calendar.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:material_symbols_icons/symbols.dart';
import 'package:rrule/rrule.dart' as rrule;
import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:result/anyhow.dart';

import '/strings/strings.dart';
import '/state/settings.dart';
import '/state/todo_db.dart';
import '/data/todo.dart';
import '/classes/logger.dart';
import '/types/window_breakpoint.dart';

import '../settings_view.d/backup_and_sync_view.d/import_export.d/import_export.dart';

import 'snackbar_with_undo.dart';
import 'todo_view_menu.dart';

AppBar selectionAppBar({
  required WidgetRef ref,
  required TodoDatabase todoDatabase,
  required Set<String> selectedTodos,
  required VoidCallback selectAll,
  required VoidCallback clearSelection,
  required int maxSelection,
  required VoidCallback onShare,
  Widget? leading,
  Iterable<Todo>? todos,
  FutureOr<void> Function(List<Todo> restored)? onRestored,
  PreferredSizeWidget? bottom,
}) {
  final context = ref.context;
  final strings = Strings.of(context)!;
  final materialStrings = MaterialLocalizations.of(context);
  final breakpoint = MediaQuery.of(context).breakpoint;

  final oneSelected = selectedTodos.length == 1;
  final allSelected = maxSelection == selectedTodos.length;
  final selectedColor = Theme.of(context).colorScheme.primary;

  final title =
      allSelected ? strings.allSelected : selectedTodos.length.toString();

  final actions = <Widget>[
    AnimatedOpacity(
      opacity: oneSelected ? 1 : 0,
      duration: Durations.short4,
      curve: Easing.standard,
      child: IconButton(
        onPressed: () async {
          if (!oneSelected) return;

          final todo = todoDatabase.get(selectedTodos.first)!;

          final date = todo.relevantDateTime ?? DateTime.now();

          final rRule = todo.rRule;

          final res = await Guard.guardAsync<bool>(() {
            return Add2Calendar.addEvent2Cal(Event(
              title: todo.title,
              description: todo.description,
              startDate: date,
              endDate: date,
              recurrence: rRule == null
                  ? null
                  : Recurrence(
                      frequency: switch (rRule.frequency) {
                        rrule.Frequency.daily => Frequency.daily,
                        rrule.Frequency.weekly => Frequency.weekly,
                        rrule.Frequency.monthly => Frequency.monthly,
                        rrule.Frequency.yearly => Frequency.yearly,
                        _ => Frequency.weekly,
                      },
                      endDate: rRule.until,
                      interval: rRule.interval ?? 1,
                      ocurrences: rRule.count,
                      rRule: rRule.toString(),
                    ),
            ));
          }).context('Could not add todo with id ${todo.id} to calendar');

          if (res case Err(:final error)) {
            logger.ah(error, level: LogLevel.debug);
          }

          if (res case Ok(value: false) || Err() when context.mounted) {
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              duration: const Duration(seconds: 4),
              dismissDirection: DismissDirection.vertical,
              margin: const EdgeInsetsDirectional.fromSTEB(24, 8, 24, 18),
              content: Text(strings.couldNotAddTodoToCalendar),
            ));
          }
        },
        tooltip: strings.todoToCalendar,
        icon: const Icon(Symbols.calendar_add_on),
      ),
    ),
    IconButton(
      onPressed: onShare,
      tooltip: strings.share,
      icon: const Icon(Symbols.share),
    ),
    IconButton(
      onPressed: () {
        final deletedTodos = todoDatabase
            .getAll(selectedTodos)
            .map((t) => t.clone(newId: true).copyWith(hadInitialSync: false))
            .toList();

        todoDatabase.deleteAll(selectedTodos.toList());

        showSnackBarWithUndo(
          context: context,
          duration: const Duration(seconds: 4),
          content: Text(strings.deletedNTodos(selectedTodos.length)),
          onUndo: () async {
            await todoDatabase.storeAll(deletedTodos);
            // await todoDatabase.addToParent(deletedTodos);
            await onRestored?.call(deletedTodos);
          },
        );

        clearSelection();
      },
      icon: const Icon(Symbols.delete, weight: 500),
      tooltip: materialStrings.deleteButtonTooltip,
    ),
    if (breakpoint.isExpanded || (breakpoint.isMedium && !oneSelected))
      IconButton(
        onPressed: () {
          if (!allSelected) {
            selectAll();
          }
        },
        tooltip: materialStrings.selectAllButtonLabel,
        color: allSelected ? selectedColor : null,
        icon: TodoViewMenuDestination.selectAll.icon(),
      ),
    if (breakpoint.isExpanded)
      IconButton(
        onPressed: () async {
          Iterable<Todo> values() sync* {
            for (final id in selectedTodos) {
              final todo = todoDatabase.get(id);
              if (todo != null) yield todo;
            }
          }

          await exportData(ref, {ExportData.todos}, todos: values().toList());
        },
        tooltip: strings.export,
        icon: TodoViewMenuDestination.export.icon(),
      ),
  ];

  List<Widget>? animatedActions;

  if (!Settings.of(context).accessability.reduceMotion.value) {
    animatedActions = AnimationConfiguration.toStaggeredList(
      delay: Durations.short2,
      duration: Durations.extralong1,
      childAnimationBuilder: (widget) => SlideAnimation(
        curve: Curves.easeInOutCubicEmphasized,
        duration: Durations.long1,
        verticalOffset: -48,
        child: FadeInAnimation(
          duration: Durations.short3,
          curve: Easing.linear,
          child: widget,
        ),
      ),
      children: actions.reversed.toList(),
    ).reversed.toList();
  }

  return AppBar(
    elevation: 4,
    bottom: bottom,
    notificationPredicate: (_) => false,
    scrolledUnderElevation: 4,
    title: Text(title),
    leadingWidth: 56,
    leading: IconButton(
      onPressed: clearSelection,
      tooltip: strings.clearSelection,
      icon: leading ?? const Icon(Symbols.close),
    ),
    actions: <Widget>[
      AnimationLimiter(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          mainAxisSize: MainAxisSize.min,
          children: animatedActions ?? actions,
        ),
      ),
      if (!breakpoint.isExpanded)
        TodoViewMenu(
          destinations: [
            if (!allSelected &&
                (breakpoint.isCompact || (breakpoint.isMedium && oneSelected)))
              TodoViewMenuDestination.selectAll,
            TodoViewMenuDestination.export,
          ],
          onSelected: (destination) async {
            if (destination == TodoViewMenuDestination.export) {
              Iterable<Todo> values() sync* {
                for (final id in selectedTodos) {
                  final todo = todoDatabase.get(id);
                  if (todo != null) yield todo;
                }
              }

              await exportData(
                ref,
                {ExportData.todos},
                todos: values().toList(),
              );
            } else if (destination == TodoViewMenuDestination.selectAll) {
              if (!allSelected) {
                selectAll();
              }
            }
          },
        ),
    ],
  );
}

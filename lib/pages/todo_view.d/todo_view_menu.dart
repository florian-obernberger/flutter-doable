import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:material_symbols_icons/symbols.dart';

import '/strings/strings.dart';
import '/resources/resources.dart';
import '/types/window_breakpoint.dart';
import '/widgets/svg_icon.dart';

enum TodoViewMenuDestination {
  showCompleted(SingleActivator(LogicalKeyboardKey.keyH, control: true)),
  hideCompleted(SingleActivator(LogicalKeyboardKey.keyH, control: true)),
  deleteCompleted,
  settings(SingleActivator(LogicalKeyboardKey.keyS, control: true)),
  feedback,
  export(SingleActivator(LogicalKeyboardKey.keyE, control: true, shift: true)),
  divider,
  update,
  deleteList,
  selectAll(SingleActivator(LogicalKeyboardKey.keyA, control: true)),
  editList;

  const TodoViewMenuDestination([this.shortcut]);
  final ShortcutActivator? shortcut;

  Widget icon([double size = 18]) {
    return switch (this) {
      showCompleted => Icon(Symbols.visibility, size: size),
      hideCompleted => Icon(Symbols.visibility_off, size: size),
      settings => Icon(Symbols.tune, size: size),
      feedback => Icon(Symbols.chat, size: size),
      deleteCompleted => Icon(Symbols.delete_forever, weight: 500, size: size),
      selectAll => Icon(Symbols.select_all, size: size),
      update => Icon(Symbols.download, size: size),
      editList => SvgIcon(icon: SvgIcons.editNoteW400, size: size),
      deleteList => SvgIcon(icon: SvgIcons.removeNoteW400, size: size),
      export => Icon(Symbols.export_notes, size: size),
      divider => SizedBox(height: size, width: size),
    };
  }

  static Iterable<TodoViewMenuDestination> get shortcuts sync* {
    for (final destination in values) {
      if (destination.shortcut != null) yield destination;
    }
  }
}

class TodoViewMenu extends StatelessWidget {
  const TodoViewMenu({
    required this.destinations,
    this.onSelected,
    this.icon,
    this.bottomSheetController,
    this.disabled,
    super.key,
  });

  final PopupMenuItemSelected<TodoViewMenuDestination>? onSelected;
  final List<TodoViewMenuDestination> destinations;

  final AnimationController? bottomSheetController;
  final Set<TodoViewMenuDestination>? disabled;

  final Widget? icon;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 8),
      child: PopupMenuButton<TodoViewMenuDestination>(
        offset: const Offset(-8, 8),
        onSelected: onSelected,
        icon: icon ?? const Icon(Symbols.more_vert, weight: 800),
        clipBehavior: Clip.antiAlias,
        itemBuilder: (context) => [
          for (final destination in destinations)
            buildItem(destination, context),
        ],
      ),
    );
  }

  PopupMenuEntry<TodoViewMenuDestination> buildItem(
    TodoViewMenuDestination destination,
    BuildContext context,
  ) {
    if (destination == TodoViewMenuDestination.divider) {
      return const PopupMenuDivider();
    }

    final theme = Theme.of(context);
    final scheme = theme.colorScheme;
    final text = theme.textTheme;
    final platform = theme.platform;

    final content = destinationText(context, destination);

    final showAndroidKeys = platform == TargetPlatform.android &&
        WindowBreakpoint.fromMediaQuery(MediaQuery.of(context))
            .isMediumOrExpanded;

    final showKeys = const {
          TargetPlatform.linux,
          TargetPlatform.windows,
          TargetPlatform.macOS
        }.contains(platform) ||
        showAndroidKeys;
    final keyText = shortcutText(context, destination.shortcut);

    final enabled = !(disabled?.contains(destination) ?? false);

    return PopupMenuItem(
      enabled: enabled,
      padding: const EdgeInsets.symmetric(horizontal: 12),
      height: 48,
      value: destination,
      child: ListTile(
        visualDensity: VisualDensity.compact,
        minVerticalPadding: 0,
        contentPadding: EdgeInsets.zero,
        title: Text(
          content,
          style: text.labelLarge?.copyWith(
            color: enabled ? scheme.onSurface : Theme.of(context).disabledColor,
          ),
        ),
        trailing: showKeys && keyText != null
            ? Text(
                keyText,
                style: text.labelMedium?.copyWith(
                  color: scheme.onSurfaceVariant,
                ),
              )
            : null,
        leading: destination.icon(),
        minLeadingWidth: 18,
      ),
    );
  }

  String? shortcutText(BuildContext context, ShortcutActivator? shortcut) {
    final strings = MaterialLocalizations.of(context);

    if (shortcut == null) return null;

    if (shortcut is SingleActivator) {
      final keyLabel = shortcut.triggers.first.keyLabel;
      final parts = <String>[
        if (shortcut.meta) strings.keyboardKeyMeta,
        if (shortcut.control) strings.keyboardKeyControl,
        if (shortcut.alt) strings.keyboardKeyAlt,
        if (shortcut.shift) strings.keyboardKeyShift,
        keyLabel,
      ];

      return parts.join('+');
    }

    if (shortcut is CharacterActivator) {
      final keyLabel = shortcut.character;

      final parts = <String>[
        if (shortcut.meta) strings.keyboardKeyMeta,
        if (shortcut.control) strings.keyboardKeyControl,
        if (shortcut.alt) strings.keyboardKeyAlt,
        keyLabel,
      ];

      return parts.join('+');
    }

    return null;
  }

  String destinationText(
      BuildContext context, TodoViewMenuDestination destination) {
    final strings = Strings.of(context)!;
    final materialStrings = MaterialLocalizations.of(context);

    return switch (destination) {
      TodoViewMenuDestination.showCompleted => strings.showCompletedTodos,
      TodoViewMenuDestination.hideCompleted => strings.popupMenuHideCompleted,
      TodoViewMenuDestination.deleteCompleted =>
        strings.popupMenuDeleteCompleted,
      TodoViewMenuDestination.settings => strings.settings,
      TodoViewMenuDestination.export => strings.export,
      TodoViewMenuDestination.feedback => strings.giveFeedback,
      TodoViewMenuDestination.update => strings.updateDoable,
      TodoViewMenuDestination.divider => '',
      TodoViewMenuDestination.deleteList => strings.deleteTodoList,
      TodoViewMenuDestination.editList => strings.editTodoList,
      TodoViewMenuDestination.selectAll => materialStrings.selectAllButtonLabel,
    };
  }
}

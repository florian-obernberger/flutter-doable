import 'package:flutter/material.dart';

import 'date_range_chip.dart';

class DateFilterRow extends StatelessWidget implements PreferredSizeWidget {
  static const double preferredSizeHeight = 8 + 40 + 8;

  const DateFilterRow({
    required this.selected,
    required this.onSelect,
    required this.onDeselect,
    this.controller,
    super.key,
  });

  final ScrollController? controller;
  final DateRangeType? selected;
  final void Function(DateRangeType type, DateTimeRange range) onSelect;
  final VoidCallback onDeselect;

  @override
  Size get preferredSize => const Size.fromHeight(preferredSizeHeight);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 40 + 16,
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            color: Theme.of(context).colorScheme.outlineVariant,
          ),
        ),
      ),
      child: ListView(
        padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 8),
        controller: controller,
        physics: const ClampingScrollPhysics(),
        scrollDirection: Axis.horizontal,
        children: <Widget>[
          for (final type in DateRangeType.values)
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 4),
              child: DateRangeChip(
                type: type,
                selected: type == selected,
                onSelect: (range) => onSelect(type, range),
                onDeselect: onDeselect,
              ),
            ),
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';

import '/strings/strings.dart';

void showSnackBarWithUndo({
  required BuildContext context,
  required Duration duration,
  required VoidCallback onUndo,
  required Widget content,
}) {
  final strings = Strings.of(context)!;

  final messenger = ScaffoldMessenger.of(context);

  messenger.hideCurrentSnackBar();

  messenger.showSnackBar(
    SnackBar(
      duration: duration,
      dismissDirection: DismissDirection.vertical,
      margin: const EdgeInsets.fromLTRB(24, 8, 24, 18),
      content: content,
      action: SnackBarAction(label: strings.undo, onPressed: onUndo),
    ),
  );
}

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:material_symbols_icons/symbols.dart';
import 'package:result/result.dart';

import '/strings/strings.dart';
import '/state/settings.dart';
import '/state/todo_list_db.dart';
import '/data/todo_list.dart';
import '/resources/resources.dart';
import '/widgets/svg_icon.dart';
import '/widgets/markdown_text.dart';
import '/util/formatters/sanitize_text.dart';

class TodoListBottomSheet extends ConsumerWidget {
  const TodoListBottomSheet(
      {this.sharedText, this.chooseAList = false, super.key});

  final String? sharedText;
  final bool chooseAList;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final strings = Strings.of(context)!;
    final settings = Settings.of(context);
    final scheme = Theme.of(context).colorScheme;
    final text = Theme.of(context).textTheme;

    final order = settings.extensions.todoListsOrder.value!.asMap();

    final db = ref.watch(todoListDatabaseProvider);
    final todoLists = [for (var idx in order.keys) db.get(order[idx]!)!];
    final useMarkdown = settings.extensions.enableMarkdown.value;

    const padding = EdgeInsets.symmetric(horizontal: 20);
    final mediaQuery = MediaQuery.of(context);

    final Widget title;
    final EdgeInsetsGeometry titlePadding;
    if (sharedText != null) {
      title = Text(
        strings.chooseAList,
        style: text.titleLarge?.copyWith(
          color: scheme.onSurface,
          fontWeight: FontWeight.w500,
        ),
        textAlign: TextAlign.left,
      );
      titlePadding = const EdgeInsets.symmetric(horizontal: 20);
    } else {
      title = Text(
        chooseAList ? strings.chooseAList : strings.list,
        style: text.titleMedium?.copyWith(
          color: scheme.onSurface,
          fontWeight: FontWeight.bold,
        ),
      );
      titlePadding = const EdgeInsets.symmetric(horizontal: 20, vertical: 8);
    }

    return Padding(
      padding: EdgeInsets.only(bottom: 16 + mediaQuery.viewPadding.bottom),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            width: double.infinity,
            padding: titlePadding,
            child: title,
          ),
          if (sharedText != null) ...<Widget>[
            const Divider(indent: 20, endIndent: 20, height: 24),
            SizedBox(
              width: double.infinity,
              child: Padding(
                padding: padding,
                child: MarkdownText(
                  sanitizeText(sharedText!, supportMarkdown: useMarkdown),
                  maxLines: 3,
                  softWrap: true,
                  baseStyle: text.bodyLarge?.copyWith(
                    color: scheme.onSurfaceVariant,
                  ),
                  supportMarkdown: useMarkdown,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
            ),
            const Divider(indent: 20, endIndent: 20, height: 24),
          ],
          Flexible(
            child: ListView(
              padding: EdgeInsets.zero,
              physics: const ClampingScrollPhysics(),
              shrinkWrap: true,
              children: <Widget>[
                ListTile(
                  leading: Icon(
                    Symbols.block,
                    weight: 300,
                    size: 24,
                    color: scheme.error,
                  ),
                  contentPadding: padding,
                  onTap: () => save(context, null),
                  title: Text(strings.noList),
                ),
                for (var list in todoLists)
                  ListTile(
                    leading: list.isHidden
                        ? const SvgIcon(icon: SvgIcons.listHiddenW300, size: 24)
                        : const SvgIcon(icon: SvgIcons.listW300, size: 24),
                    contentPadding: padding,
                    onTap: () => save(context, list),
                    title: Text(list.name, maxLines: 2),
                  ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  void save(BuildContext context, TodoList? list) {
    context.pop(Option.fromNullable(list));
  }
}

import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:fuzzywuzzy/fuzzywuzzy.dart';
import 'package:remove_emoji/remove_emoji.dart';

import '/strings/strings.dart';
import '/data/todo.dart';
import '/widgets/nav_button.dart';
import '/util/extensions/todo_importance.dart';
import '/util/build_text_selection_menu.dart';

import 'todo_item.dart';

class TodoSearch extends StatefulWidget {
  const TodoSearch({
    required this.todos,
    required this.close,
    required this.showList,
    super.key,
  });

  final CloseContainerActionCallback<String> close;
  final List<Todo> todos;
  final bool showList;

  @override
  State<TodoSearch> createState() => _TodoSearchState();
}

class _TodoSearchState extends State<TodoSearch> {
  final searchController = TextEditingController();

  String get query => searchController.text;
  bool isEmpty = true;

  @override
  void initState() {
    super.initState();

    searchController.addListener(() {
      if (isEmpty != searchController.text.isEmpty) {
        setState(() => isEmpty = query.isEmpty);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final scheme = Theme.of(context).colorScheme;

    return Scaffold(
      backgroundColor: scheme.surfaceContainerHigh,
      appBar: buildSearchBar(),
      body: buildSuggestions(),
    );
  }

  AppBar buildSearchBar() {
    final strings = Strings.of(context)!;
    final scheme = Theme.of(context).colorScheme;
    final text = Theme.of(context).textTheme;

    return AppBar(
      backgroundColor: scheme.surfaceContainerHigh,
      notificationPredicate: (_) => false,
      toolbarHeight: 72,
      title: TextFormField(
        autofocus: true,
        contextMenuBuilder: buildTextSelectionMenu,
        controller: searchController,
        style: text.bodyLarge?.copyWith(color: scheme.onSurface),
        decoration: InputDecoration(
          hintText: strings.searchTodos,
          hintStyle: text.bodyLarge?.copyWith(color: scheme.onSurfaceVariant),
          border: InputBorder.none,
        ),
      ),
      leading: Padding(
        padding: const EdgeInsetsDirectional.only(start: 8),
        child: NavButton.back(onNav: widget.close, color: scheme.onSurface),
      ),
      actions: <Widget>[
        AnimatedOpacity(
          opacity: isEmpty ? 0 : 1,
          duration: Durations.short4,
          curve: Easing.standard,
          child: Padding(
            padding: const EdgeInsetsDirectional.only(end: 8),
            child: NavButton.close(
              color: scheme.onSurfaceVariant,
              tooltip: strings.clearInputField,
              onNav: searchController.clear,
            ),
          ),
        ),
      ],
      bottom: const PreferredSize(
        preferredSize: Size.fromHeight(1),
        child: Divider(),
      ),
    );
  }

  Widget buildSuggestions() {
    if (query.isEmpty) return const SizedBox();

    final mediaQuery = MediaQuery.of(context);
    final scheme = Theme.of(context).colorScheme;

    final searchResult = extractAllSorted<Todo>(
      query: query,
      choices: widget.todos,
      getter: (t) => t.title.removEmoji,
    ).where((r) => r.score > 65).map((r) => r.choice).toList();

    return ListView.builder(
      physics: const ClampingScrollPhysics(),
      padding: const EdgeInsets.all(12).add(
        EdgeInsets.only(bottom: mediaQuery.padding.bottom),
      ),
      itemCount: searchResult.length,
      itemBuilder: (context, i) {
        final todo = searchResult[i];

        return TodoCard(
          state: TodoCardState.fromImportance(todo.importanceOf(context)),
          getStyleFrom: (state, completed) => switch (state) {
            TodoCardState.important when !completed => (
                scheme.surfaceContainerLowest,
                1
              ),
            TodoCardState.selected => (scheme.surfaceContainerHigh, 0.5),
            _ => (scheme.surfaceContainerHigh, 0),
          },
          allowSwipeActions: false,
          completed: todo.isCompleted,
          onOpen: () => widget.close(returnValue: todo.id),
          item: TodoItem(
            key: ValueKey('item:${todo.hashCode}'),
            todo: todo,
            canInteract: () => false,
            showList: () => widget.showList,
            showCheck: false,
          ),
        );
      },
    );
  }
}

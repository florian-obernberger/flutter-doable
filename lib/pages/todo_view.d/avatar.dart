import 'package:flutter/material.dart';
import 'package:material_symbols_icons/symbols.dart';

import '/state/settings.dart';
import '/widgets/nextcloud_avatar.dart';

class Avatar extends StatelessWidget {
  const Avatar({this.shape = AvatarShape.circle8, super.key});

  final AvatarShape shape;

  @override
  Widget build(BuildContext context) => ValueListenableBuilder(
        valueListenable: Settings.of(context).backup.nextcloud,
        builder: (context, markdown, child) =>
            markdown ? child! : const Icon(Symbols.more_vert, weight: 800),
        child: NextcloudAvatar(imageSize: 30, iconSize: 30, shape: shape),
      );
}

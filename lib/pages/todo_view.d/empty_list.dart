import 'dart:async';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:gap/gap.dart';
import 'package:material_symbols_icons/symbols.dart';
import 'package:path/path.dart' as path;

import '/strings/strings.dart';
import '/state/settings.dart';
import '/classes/motion.dart';
import '/resources/resources.dart';
import '/util/extensions/iterable.dart';
import '/util/extensions/string.dart';
import '/util/animated_dialog.dart';
import '/util/launch.dart';

import 'fedialog.dart';

class EmptyList extends StatelessWidget {
  const EmptyList({super.key});

  String get dynamicImageName {
    final DateTime(:hour, :minute) = DateTime.now();
    final time = hour * 60 + minute;

    final timedImages = DynamicImages.values.mapBy(
      (name) => path.basenameWithoutExtension(name).substring(1).parse<int>(),
    );

    final currentKey = timedImages.keys.where((img) => img - time <= 0).max();
    return timedImages[currentKey]!;
  }

  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context)!;
    final scheme = Theme.of(context).colorScheme;
    final text = Theme.of(context).textTheme;
    final orientation = MediaQuery.of(context).orientation;

    final settings = Settings.of(context);

    const diameter = 232.0;

    final imageName = dynamicImageName;

    return ValueListenableBuilder(
        valueListenable: settings.fediverse.showButton,
        builder: (context, showFediButton, _) {
          final image = Container(
            clipBehavior: Clip.antiAliasWithSaveLayer,
            width: diameter,
            height: diameter,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: scheme.primaryContainer,
            ),
            child: Stack(
              alignment: Alignment.center,
              children: <Widget>[
                AnimatedSwitcher(
                  duration: Motion.long1,
                  reverseDuration: Motion.medium4,
                  switchInCurve: Easing.standardDecelerate,
                  switchOutCurve: Easing.standardAccelerate,
                  child: Image.asset(imageName, key: ValueKey(imageName)),
                ),
                ImageFiltered(
                  imageFilter: ImageFilter.blur(
                    sigmaX: 4,
                    sigmaY: 4,
                    tileMode: TileMode.decal,
                  ),
                  child: Container(
                    width: diameter - 12.5,
                    height: diameter - 12.5,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(color: Colors.white, width: 5.5),
                    ),
                  ),
                ),
                Container(
                  width: diameter - 16,
                  height: diameter - 16,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    border: Border.all(color: Colors.white, width: 2),
                  ),
                ),
              ],
            ),
          );

          final info = Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(
                strings.noTodosLeft,
                style: text.titleLarge?.copyWith(color: scheme.onSurface),
                maxLines: 2,
                textAlign: TextAlign.center,
              ),
              const Gap(20),
              if (showFediButton)
                FilledButton.tonalIcon(
                  style: const ButtonStyle(
                    visualDensity: VisualDensity.standard,
                    iconSize: WidgetStatePropertyAll(18),
                  ),
                  onPressed: () async {
                    final toot = await showAnimatedDialog<FedialogResponse>(
                      context: context,
                      builder: (context) => const Fedialog(),
                    );

                    if (toot == null) return;
                    final (
                      instanceUrl: instanceUrl,
                      rememberInstance: rememberInstance
                    ) = toot;

                    settings.fediverse.instanceUrl =
                        rememberInstance ? instanceUrl : null;
                    final url = "${path.join(instanceUrl, 'share')}"
                        "?text=${Uri.encodeFull(strings.fediDoablePost).replaceAll('#', '%23')}";

                    unawaited(launchUrl(
                      url,
                      inAppBrowser: settings.general.inAppBrowser.value,
                    ));
                  },
                  icon: const Icon(Symbols.send),
                  label: Text(strings.post),
                ),
            ],
          );

          return SizedBox(
            child: Center(
              child: orientation == Orientation.landscape
                  ? Row(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsetsDirectional.only(
                            start: 72,
                            top: 24,
                          ),
                          child: image,
                        ),
                        Padding(
                          padding: const EdgeInsetsDirectional.only(
                            start: 64,
                            end: 24,
                          ),
                          child: info,
                        ),
                      ],
                    )
                  : Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(top: 112),
                          child: image,
                        ),
                        const Gap(28),
                        info,
                      ],
                    ),
            ),
          );
        });
  }
}

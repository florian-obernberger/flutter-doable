import 'package:flutter/material.dart';
import 'package:material_symbols_icons/symbols.dart';

import '/strings/strings.dart';

class DeleteCompletedDialog extends StatelessWidget {
  const DeleteCompletedDialog({super.key});

  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context)!;

    return AlertDialog(
      title: Text(strings.attention),
      content: Text(strings.aboutToDeleteCompleted),
      actions: [
        TextButton(
          onPressed: Navigator.of(context).pop,
          child: Text(strings.cancel),
        ),
        TextButton(
          onPressed: () => Navigator.of(context).pop(true),
          child: Text(strings.doContinue),
        ),
      ],
    );
  }
}

class DeleteSelectedDialog extends StatelessWidget {
  const DeleteSelectedDialog(this.count, {super.key});

  final int count;

  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context)!;

    return AlertDialog(
      title: Text(strings.attention),
      icon: const Icon(Symbols.error_outline, size: 24),
      content: Text(strings.aboutToDeleteNSelected(count)),
      actions: [
        OutlinedButton.icon(
          onPressed: Navigator.of(context).pop,
          icon: const Icon(Symbols.close, size: 18),
          label: Text(strings.cancel),
        ),
        FilledButton.icon(
          onPressed: () => Navigator.of(context).pop(true),
          icon: const Icon(Symbols.delete, fill: 0, size: 18),
          label: Text(strings.doContinue),
        ),
      ],
    );
  }
}

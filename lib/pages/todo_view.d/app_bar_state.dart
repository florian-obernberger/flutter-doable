import 'package:flutter/foundation.dart';

class AppBarState with ChangeNotifier {
  final ValueNotifier<bool> loading;
  final ValueNotifier<bool> pullingFromSyncs;

  AppBarState({bool loading = true, bool pullingFromSyncs = false})
      : loading = ValueNotifier(loading),
        pullingFromSyncs = ValueNotifier(pullingFromSyncs);

  @override
  void addListener(VoidCallback listener) {
    loading.addListener(listener);
    pullingFromSyncs.addListener(listener);
  }

  @override
  void removeListener(VoidCallback listener) {
    loading.removeListener(listener);
    pullingFromSyncs.removeListener(listener);
  }

  @override
  void dispose() {
    loading.dispose();
    pullingFromSyncs.dispose();
    super.dispose();
  }
}

import 'package:flutter/material.dart';

import '/classes/motion.dart';

import '../todo_detail_view.dart';

class TodoDetailBottomSheet extends StatelessWidget {
  const TodoDetailBottomSheet({
    required this.mediaQuery,
    required this.todoId,
    super.key,
  });

  final MediaQueryData mediaQuery;
  final String todoId;

  @override
  Widget build(BuildContext context) {
    final scheme = Theme.of(context).colorScheme;

    final details = TodoDetailsView(todoId: todoId, wasNotification: false);
    final controller = DraggableScrollableController();

    return DraggableScrollableSheet(
      expand: false,
      initialChildSize: 1,
      minChildSize: 0.75,
      snap: true,
      snapAnimationDuration: Motion.short3,
      controller: controller,
      builder: (context, scrollController) {
        return _TodoDetailSheet(
          scheme: scheme,
          mediaQuery: mediaQuery,
          controller: controller,
          scrollController: scrollController,
          child: details,
        );
      },
    );
  }
}

class _TodoDetailSheet extends StatefulWidget {
  const _TodoDetailSheet({
    required this.scheme,
    required this.mediaQuery,
    required this.controller,
    required this.scrollController,
    required this.child,
  });

  final ColorScheme scheme;
  final MediaQueryData mediaQuery;
  final DraggableScrollableController controller;
  final ScrollController scrollController;
  final Widget child;

  @override
  State<_TodoDetailSheet> createState() => _TodoDetailSheetState();
}

class _TodoDetailSheetState extends State<_TodoDetailSheet> {
  double borderRadius = 0;
  bool showHandle = false;

  @override
  void initState() {
    super.initState();

    widget.controller.addListener(() {
      final height = widget.controller.pixelsToSize(widget.controller.pixels);
      final r = (-180 * height) + 180;
      setState(() {
        borderRadius = r.clamp(0, 28);
        showHandle = height != 1;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    final scheme = Theme.of(context).colorScheme;

    return AnimatedContainer(
      duration: Motion.short2,
      decoration: BoxDecoration(
        color: widget.scheme.surface,
        borderRadius: BorderRadius.circular(borderRadius),
      ),
      height: widget.mediaQuery.size.height,
      width: widget.mediaQuery.size.width,
      child: SingleChildScrollView(
        physics: const NeverScrollableScrollPhysics(),
        controller: widget.scrollController,
        child: SizedBox(
          height: widget.mediaQuery.size.height,
          width: widget.mediaQuery.size.width,
          child: Column(
            children: <Widget>[
              SizedBox(
                height: widget.mediaQuery.viewPadding.top,
                child: Center(
                  child: AnimatedOpacity(
                    opacity: showHandle ? 1 : 0,
                    duration: Motion.short2,
                    child: Container(
                      height: 4,
                      width: 32,
                      decoration: BoxDecoration(
                        color: scheme.onSurfaceVariant.withOpacity(0.4),
                        borderRadius: BorderRadius.circular(28),
                      ),
                    ),
                  ),
                ),
              ),
              Expanded(child: widget.child),
            ],
          ),
        ),
      ),
    );
  }
}

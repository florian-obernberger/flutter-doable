import 'package:flex_color_picker/flex_color_picker.dart';
import 'package:flutter/material.dart';
import 'package:gap/gap.dart';
import 'package:result/result.dart';

import '/strings/strings.dart';

class ColorPickerDialog extends StatefulWidget {
  const ColorPickerDialog({required this.initialColor, super.key});

  final Color initialColor;

  @override
  State<ColorPickerDialog> createState() => _ColorPickerDialogState();
}

class _ColorPickerDialogState extends State<ColorPickerDialog> {
  late Color selectedColor = widget.initialColor;

  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context)!;
    final scheme = Theme.of(context).colorScheme;
    final text = Theme.of(context).textTheme;

    return AlertDialog(
      backgroundColor: scheme.surfaceContainerHigh,
      content: ConstrainedBox(
        constraints: const BoxConstraints(minWidth: 280, maxWidth: 560),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            ColorPicker(
              color: selectedColor,
              title: Text(
                strings.pickAColor,
                style: text.headlineSmall?.copyWith(color: scheme.onSurface),
              ),
              borderRadius: 8,
              padding: EdgeInsets.zero,
              onColorChanged: (color) => setState(() => selectedColor = color),
              subheading: const SizedBox(height: 16),
              wheelSubheading: const SizedBox(height: 16),
              pickersEnabled: const <ColorPickerType, bool>{
                ColorPickerType.both: false,
                ColorPickerType.primary: true,
                ColorPickerType.accent: false,
                ColorPickerType.bw: false,
                ColorPickerType.custom: false,
                ColorPickerType.wheel: true,
              },
              pickerTypeLabels: <ColorPickerType, String>{
                ColorPickerType.primary: strings.pickerPrimary,
                ColorPickerType.wheel: strings.pickerWheel,
              },
              copyPasteBehavior: const ColorPickerCopyPasteBehavior(
                copyFormat: ColorPickerCopyFormat.numHexRRGGBB,
                ctrlC: true,
                ctrlV: true,
                editFieldCopyButton: false,
                secondaryMenu: true,
                pasteButton: false,
                copyButton: false,
              ),
            ),
          ],
        ),
      ),
      actionsAlignment: MainAxisAlignment.spaceBetween,
      actions: <Widget>[
        TextButton(
          onPressed: () => Navigator.of(context).pop(const Some<Color?>(null)),
          child: Text(strings.reset),
        ),
        Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            TextButton(
              onPressed: Navigator.of(context).pop,
              child: Text(strings.cancel),
            ),
            const Gap(8),
            TextButton(
              onPressed: () => Navigator.of(context).pop(Some(selectedColor)),
              child: Text(strings.ok),
            ),
          ],
        ),
      ],
    );
  }
}

import 'package:flutter/foundation.dart';

class SelectionState with ChangeNotifier {
  SelectionState();

  final _selected = <String>{};

  Set<String> get selected => _selected;
  List<String> get selectedList => _selected.toList();

  void clear() {
    _selected.clear();
    notifyListeners();
  }

  bool contains(String value) => _selected.contains(value);

  bool remove(String value) {
    final res = _selected.remove(value);
    notifyListeners();
    return res;
  }

  bool add(String value) {
    final res = _selected.add(value);
    notifyListeners();
    return res;
  }

  void addAll(Iterable<String> values) {
    _selected.addAll(values);
    notifyListeners();
  }

  bool get isNotEmpty => _selected.isNotEmpty;
  bool get isEmpty => _selected.isEmpty;
}

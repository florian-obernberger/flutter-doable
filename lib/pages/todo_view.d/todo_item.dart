import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_swipe_action_cell/flutter_swipe_action_cell.dart';
import 'package:gap/gap.dart';
import 'package:material_symbols_icons/symbols.dart';

import '/classes/motion.dart';
import '/data/todo.dart';
import '/types/swipe_action.dart' as sa;
import '/state/settings.dart';
import '/state/todo_list_db.dart';
import '/strings/strings.dart';
import '/widgets/markdown_text.dart';
import '/widgets/todo_chips.dart';
import '/util/extensions/todo.dart';
import '/util/extensions/todo_importance.dart';
import '/util/formatters/format_date.dart';
import '/util/formatters/sanitize_text.dart';

import '../todo_detail_view.d/todo_image.dart';

Duration _transitionDuration(bool reduceMotion) =>
    reduceMotion ? Duration.zero : Motion.medium3;

Duration _reverseTransitionDuration(bool reduceMotion) =>
    reduceMotion ? Duration.zero : Motion.medium2;

enum TodoCardState {
  none,
  important,
  // expanded,
  dateImportant,
  selected;

  factory TodoCardState.fromImportance(TodoImportance importance) =>
      switch (importance) {
        TodoImportance.not => none,
        TodoImportance.date => dateImportant,
        TodoImportance.starred => important
      };
}

enum TodoItemEvent { complete, /* expand, */ star, delete }

extension SwipeActionEvent on sa.SwipeAction {
  TodoItemEvent get event => switch (this) {
        sa.SwipeAction.star => TodoItemEvent.star,
        sa.SwipeAction.complete => TodoItemEvent.complete,
        sa.SwipeAction.delete => TodoItemEvent.delete,
      };
}

class TodoCard extends StatelessWidget {
  const TodoCard({
    required this.item,
    required this.state,
    required this.completed,
    required this.allowSwipeActions,
    this.getStyleFrom,
    this.onOpen,
    this.onSelect,
    super.key,
  });

  final TodoItem item;
  final TodoCardState state;
  // ignore: avoid_positional_boolean_parameters
  final (Color, double) Function(TodoCardState state, bool completed)?
      getStyleFrom;
  final bool allowSwipeActions;
  final bool completed;
  final VoidCallback? onOpen;
  final VoidCallback? onSelect;

  @override
  Widget build(BuildContext context) {
    final settings = Settings.of(context);
    final ThemeData(colorScheme: scheme) = Theme.of(context);

    final enableSwipeActions = settings.extensions.enableSwipeActions.value;

    final opacity = completed ? 0.6 : 1.0;
    final (color, elevation) =
        getStyleFrom?.call(state, completed) ?? getStyle(scheme);
    final border = getBorder(scheme);
    final borderRadius = BorderRadius.circular(12);
    final shape = RoundedRectangleBorder(borderRadius: borderRadius);

    final reduceMotion = settings.accessability.reduceMotion.value;

    final showActions = state != TodoCardState.selected && allowSwipeActions;

    final keyData = StringBuffer()
      ..write(item.todo.id)
      ..write(':')
      ..write(showActions && enableSwipeActions)
      ..write(':')
      ..write(completed &&
          settings.miscellaneous.showCompleted.value &&
          enableSwipeActions);

    final key = ValueKey(keyData.toString());

    return Padding(
      padding: const EdgeInsets.all(4),
      child: Material(
        color: color.withOpacity(opacity),
        shape: shape,
        clipBehavior: Clip.antiAliasWithSaveLayer,
        animationDuration: _transitionDuration(reduceMotion),
        type: MaterialType.card,
        elevation: elevation,
        child: InkWell(
          onTap: onOpen,
          customBorder: shape,
          onLongPress: onSelect,
          child: SwipeActionCell(
            key: key,
            closeAnimationCurve: Curves.easeInOutCubicEmphasized,
            closeAnimationDuration: Durations.medium4.inMilliseconds,
            openAnimationCurve: Easing.standard,
            isDraggable: showActions,
            backgroundColor: Colors.transparent,
            trailingActions: enableSwipeActions
                ? settings.extensions.selectedSwipeActions.value.reversed
                    .map(buildAction(context))
                    .toList()
                : null,
            child: AnimatedContainer(
              clipBehavior: Clip.antiAliasWithSaveLayer,
              duration: _transitionDuration(reduceMotion),
              curve: Curves.easeInOutCubicEmphasized,
              decoration: BoxDecoration(
                borderRadius: borderRadius,
                border: border,
              ),
              child: item,
            ),
          ),
        ),
      ),
    );
  }

  SwipeAction Function(sa.SwipeAction) buildAction(BuildContext context) =>
      (action) {
        return SwipeAction(
          onTap: (handler) async {
            unawaited(HapticFeedback.selectionClick());
            unawaited(handler(action.hidden));
            await Future.delayed(
              action.hidden ? Durations.medium2 : Duration.zero,
              () => item.onEvent?.call(action.event, item.todo),
            );
          },
          closeOnTap: true,
          color: action.color(context),
          icon: action.icon(context, negated: action.shouldNegate(item.todo)),
        );
      };

  (Color, double) getStyle(ColorScheme scheme) => switch (state) {
        TodoCardState.important when !completed => (
            scheme.surfaceContainerHigh,
            0
          ),
        TodoCardState.dateImportant when !completed => (
            scheme.surfaceContainer,
            0
          ),
        // TodoCardState.expanded => (scheme.surfaceContainerLow, 0),
        TodoCardState.selected => (scheme.surfaceContainerLow, 1),
        TodoCardState.none ||
        TodoCardState.important ||
        TodoCardState.dateImportant =>
          (scheme.surface, 0),
      };

  EdgeInsetsGeometry getPadding() => switch (state) {
        TodoCardState.important when !completed =>
          const EdgeInsetsDirectional.only(start: 16, end: 24),
        TodoCardState.selected =>
          const EdgeInsetsDirectional.only(start: 16, end: 24),
        _ => const EdgeInsetsDirectional.only(start: 12, end: 8),
      };

  BoxBorder? getBorder(ColorScheme scheme) => switch (state) {
        TodoCardState.selected => Border.all(color: scheme.secondary),
        _ => null,
      };
}

class TodoItem extends ConsumerStatefulWidget {
  const TodoItem({
    required this.todo,
    required this.canInteract,
    required this.showList,
    this.showCheck = true,
    this.onEvent,
    super.key,
  });

  final Todo todo;
  final bool Function() canInteract;
  final bool Function() showList;
  final bool showCheck;
  final void Function(TodoItemEvent event, Todo todo)? onEvent;

  @override
  ConsumerState<TodoItem> createState() => _TodoItemState();
}

class _TodoItemState extends ConsumerState<TodoItem> {
  late var todo = widget.todo;
  bool? _expanded;

  static const _factor = 1.15;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    if (widget.todo != todo) setState(() => todo = widget.todo);
  }

  double get opacity => todo.isCompleted ? 0.6 : 1.0;

  bool canExpand() => widget.canInteract() && (hasDescription || hasImage);
  bool get isExpanded {
    _expanded ??= Settings.of(context).general.expandTodosByDefault.value;
    return _expanded!;
  }

  CrossFadeState get contentState =>
      isExpanded ? CrossFadeState.showSecond : CrossFadeState.showFirst;

  bool get hasList => (todo.listId != null &&
      widget.showList() &&
      ref.read(todoListDatabaseProvider).get(todo.listId ?? '') != null);

  bool get hasDate => todo.relevantDate != null;

  bool get hasDescription => todo.description != null;

  bool get hasImage =>
      Settings.of(context).extensions.enableImages.value && todo.image != null;

  bool get hasRecurrence =>
      Settings.of(context).extensions.enableRecurringTasks.value &&
      todo.rRuleString != null &&
      hasDate;

  bool get hasBottom => hasDate || hasList || hasImage || hasRecurrence;

  bool get hasContent => hasDescription || hasBottom;

  @override
  Widget build(BuildContext context) {
    final settings = Settings.of(context);
    final reduceMotion = settings.accessability.reduceMotion.value;
    final scheme = Theme.of(context).colorScheme;
    final titleStyle = Theme.of(context).textTheme.bodyLarge!;

    var (short, long) = getContent();
    final (leading, trailing) = getActions();

    var padding = const EdgeInsetsDirectional.symmetric(
      horizontal: 12,
      vertical: 8,
    );

    final expandTitle = !(hasBottom || hasDescription);

    return AnimatedOpacity(
      duration: Motion.short4,
      opacity: opacity,
      child: GestureDetector(
        onDoubleTap: canExpand() ? onExpand : null,
        child: Padding(
          padding: padding,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              ListTile(
                minVerticalPadding: 0,
                minLeadingWidth: 0,
                isThreeLine: hasDescription && hasBottom,
                title: _ExpandableText(
                  short: MarkdownText(
                    todo.title,
                    maxLines: expandTitle ? 2 : 1,
                    softWrap: expandTitle,
                    isTitle: true,
                    overflow:
                        expandTitle ? TextOverflow.ellipsis : TextOverflow.fade,
                    useTap: isExpanded && widget.canInteract(),
                  ),
                  long: MarkdownText(
                    todo.title,
                    softWrap: true,
                    isTitle: true,
                    useTap: isExpanded && widget.canInteract(),
                  ),
                  state: contentState,
                ),
                titleTextStyle: titleStyle.copyWith(
                  fontSize: titleStyle.fontSize! * _factor,
                  color: scheme.onSurface,
                ),
                subtitle: hasContent
                    ? _ExpandableText(
                        short: short,
                        long: long,
                        state: contentState,
                      )
                    : null,
                trailing: trailing,
                leading: leading,
                visualDensity: const VisualDensity(vertical: -4),
                contentPadding: EdgeInsets.zero,
                // style: listItemStyle,
              ),
              AnimatedSize(
                duration: _transitionDuration(reduceMotion),
                reverseDuration: _reverseTransitionDuration(reduceMotion),
                child: AnimatedOpacity(
                  opacity: isExpanded ? 1 : 0,
                  duration: _transitionDuration(reduceMotion),
                  curve: Curves.easeInOutCubicEmphasized,
                  child: Divider(height: isExpanded ? null : 0),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void onExpand() {
    HapticFeedback.selectionClick();
    setState(() => _expanded = !isExpanded);
  }

  Widget? get dateTime {
    if (!hasDate) return null;

    final dateTime = todo.relevantDateTime!;
    final showTime = todo.relevantTime != null;

    final strings = Strings.of(context)!;
    final settings = Settings.of(context);
    final scheme = Theme.of(context).colorScheme;
    final text = Theme.of(context).textTheme;
    final locale = Localizations.localeOf(context).countryCode;

    final content = showTime
        ? formatDateTime(settings.dateTime, strings, dateTime,
            locale: locale, separator: ', ')
        : formatDate(settings.dateTime, strings, dateTime, locale);

    final Color color;

    if (todo.isOverdue) {
      color = scheme.error;
    } else if (todo.isToday) {
      color = scheme.secondary;
    } else {
      color = scheme.onSurfaceVariant;
    }

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 2),
      child: Text(content, style: text.labelMedium?.copyWith(color: color)),
    );
  }

  Widget? get list {
    if (!hasList) return null;
    final list = ref.read(todoListDatabaseProvider).get(todo.listId!);
    if (list == null) return null;

    return ListChip(list);
  }

  Widget? get recurrence =>
      hasRecurrence ? RecurrenceChip(todo.rRule!.frequency) : null;

  Widget? get image => hasImage ? const ImageChip() : null;

  (Widget, Widget) getContent() {
    final text = todo.description;
    return (_getContent(text, false), _getContent(text, true));
  }

  (Widget?, Widget?) getActions() {
    final settings = Settings.of(context);

    final check = getCheck();
    final progress = getProgress();

    return settings.general.checkMarkRight.value
        ? (progress, check)
        : (check, progress);
  }

  Widget? getCheck() {
    if (!widget.showCheck) return null;

    final strings = Strings.of(context)!;
    final settings = Settings.of(context);
    final scheme = Theme.of(context).colorScheme;

    if (settings.extensions.enableSwipeActions.value &&
        settings.extensions.selectedSwipeActions.value
            .contains(sa.SwipeAction.complete) &&
        settings.extensions.hideCheckWhenCompleteSelected.value) {
      return null;
    }

    return Theme(
      data: Theme.of(context).copyWith(
        splashFactory: NoSplash.splashFactory,
      ),
      child: IconButton(
        isSelected: todo.isCompleted,
        tooltip:
            todo.isCompleted ? strings.markUncompleted : strings.markCompleted,
        onPressed: widget.canInteract()
            ? () {
                HapticFeedback.mediumImpact();
                widget.onEvent?.call(TodoItemEvent.complete, todo);
              }
            : null,
        icon: Icon(Symbols.circle, color: scheme.onSurfaceVariant),
        selectedIcon: Icon(Symbols.check, color: scheme.secondary),
      ),
    );
  }

  Widget? getProgress() {
    if (!Settings.of(context).extensions.enableProgressBar.value) return null;

    final scheme = Theme.of(context).colorScheme;
    const radius = BorderRadius.all(Radius.circular(4));
    const width = 4.0;

    return Container(
      decoration: BoxDecoration(
        color: scheme.secondaryContainer,
        borderRadius: radius,
      ),
      height: double.infinity,
      alignment: Alignment.bottomCenter,
      width: width,
      child: FractionallySizedBox(
        heightFactor: todo.progress ?? 0,
        child: Container(
          alignment: Alignment.bottomCenter,
          decoration: BoxDecoration(
            color: scheme.primary,
            borderRadius: radius,
          ),
        ),
      ),
    );
  }

  Widget _getContent(String? desc, bool expanded) {
    final settings = Settings.of(context);
    final scheme = Theme.of(context).colorScheme;
    final bodyMedium = Theme.of(context).textTheme.bodyMedium!;

    return Padding(
      padding: const EdgeInsets.only(bottom: 2),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          if (desc != null)
            ValueListenableBuilder(
              valueListenable: settings.extensions.enableMarkdown,
              builder: (context, value, _) => MarkdownText(
                expanded
                    ? desc
                    : sanitizeText(
                        desc,
                        supportMarkdown:
                            settings.extensions.enableMarkdown.value,
                      ),
                todo: todo,
                supportMarkdown: value,
                useTap: expanded && widget.canInteract(),
                maxLines: expanded ? null : 1,
                softWrap: expanded,
                baseStyle: bodyMedium.copyWith(
                  color: scheme.onSurfaceVariant,
                  fontSize: bodyMedium.fontSize! * _factor,
                ),
              ),
            ),
          if (hasImage && expanded) ...[
            const Gap(2),
            TodoImage(id: todo.id),
            const Gap(2),
          ],
          if (hasBottom)
            Padding(
              padding: const EdgeInsets.only(top: 2),
              child: _Bottom(
                children: <Widget>[
                  if (hasList) list!,
                  if (hasRecurrence) recurrence!,
                  if (hasImage) image!,
                  if (hasDate) dateTime!,
                ],
              ),
            ),
        ],
      ),
    );
  }
}

class _Bottom extends StatelessWidget {
  const _Bottom({required this.children}) : assert(children.length > 0);

  final List<Widget> children;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        children.first,
        for (var child in children.sublist(1)) ...[const Gap(4), child]
      ],
    );
  }
}

class _ExpandableText extends StatelessWidget {
  const _ExpandableText({
    required this.short,
    required this.long,
    required this.state,
  });

  final Widget short;
  final Widget long;
  final CrossFadeState state;

  @override
  Widget build(BuildContext context) {
    if (short == long) return short;

    final reduceMotion = Settings.of(context).accessability.reduceMotion.value;

    return AnimatedCrossFade(
      firstChild: short,
      secondChild: long,
      crossFadeState: state,
      duration: _transitionDuration(reduceMotion),
      reverseDuration: _reverseTransitionDuration(reduceMotion),
      sizeCurve: Easing.standard,
      firstCurve: Easing.standardAccelerate,
      secondCurve: Easing.standardDecelerate,
    );
  }
}

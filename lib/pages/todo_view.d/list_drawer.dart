import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:material_symbols_icons/symbols.dart';

import '/strings/strings.dart';
import '/state/todo_list_db.dart';
import '/state/settings.dart';
import '/data/todo_list.dart';
import '/classes/logger.dart';
import '/widgets/navigation_drawer.dart' as nd;
import '/widgets/svg_icon.dart';
import '/resources/resources.dart';
import '/util/animated_dialog.dart';

import 'list_dialog.dart';

enum TodoListDrawerInput { overview, todoList }

class TodoListDrawerEvent {
  const TodoListDrawerEvent(this.input, this.todoList);

  const TodoListDrawerEvent.noList(this.input) : todoList = null;
  const TodoListDrawerEvent.list(this.todoList)
      : input = TodoListDrawerInput.todoList;

  final TodoListDrawerInput input;
  final TodoList? todoList;
}

class TodoListDrawer extends ConsumerStatefulWidget {
  const TodoListDrawer({
    required this.onListSelected,
    required this.closeOnSelected,
    this.initialList,
    this.isModal = true,
    super.key,
  });

  final bool closeOnSelected;
  final void Function(TodoListDrawerEvent event) onListSelected;
  final TodoList? initialList;
  final bool isModal;

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => TodoListDrawerState();
}

class TodoListDrawerState extends ConsumerState<TodoListDrawer> {
  int selectedList = 0;

  Settings? _settings;
  Settings get settings => _settings!;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    if (_settings == null) {
      _settings = Settings.of(context);

      settings.extensions.todoListsOrder.value ??=
          ref.read(todoListDatabaseProvider).values().map((l) => l.id).toList();

      settings.design.fontPairing.addListener(listener);
      settings.extensions.todoListsOrder.addListener(listener);
    }

    final lists = getLists(ref.read(todoListDatabaseProvider));
    if (widget.initialList != null) {
      selectedList = lists.indexOf(widget.initialList!) + 1;
    }
  }

  @override
  void dispose() {
    settings.design.fontPairing.removeListener(listener);
    settings.extensions.todoListsOrder.removeListener(listener);

    super.dispose();
  }

  void listener() => setState(() {});

  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context)!;
    final scheme = Theme.of(context).colorScheme;

    final lists = getLists(ref.watch(todoListDatabaseProvider));
    final listWidgets = lists
        .map(
          (list) => nd.NavigationDrawerDestination(
            icon: SvgIcon(
              icon: list.isHidden ? SvgIcons.listHiddenW300 : SvgIcons.listW300,
              color: scheme.onSurfaceVariant,
              size: 24,
              matchTextDirection: true,
            ),
            selectedIcon: SvgIcon(
              icon: list.isHidden ? SvgIcons.listHiddenW400 : SvgIcons.listW400,
              color: scheme.onSecondaryContainer,
              size: 24,
            ),
            label: Expanded(
              child: Padding(
                padding: const EdgeInsetsDirectional.only(end: 24),
                child: Text(
                  list.name,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 1,
                ),
              ),
            ),
          ),
        )
        .toList();

    return DrawerTheme(
      data: DrawerTheme.of(context).copyWith(
        shape: widget.isModal ? null : const RoundedRectangleBorder(),
      ),
      child: nd.NavigationDrawer(
        elevation: widget.isModal ? null : 0,
        selectedIndex: selectedList,
        onDestinationSelected: (value) {
          if (value == lists.length + 1) {
            newList();
            return;
          }

          setState(() => selectedList = value);

          final TodoListDrawerEvent event;

          // Check if the selected "list" is "Overview"
          if (selectedList == 0) {
            event = const TodoListDrawerEvent.noList(
              TodoListDrawerInput.overview,
            );
          } else {
            event = TodoListDrawerEvent.list(lists[selectedList - 1]);
          }

          widget.onListSelected(event);
          if (widget.closeOnSelected) Navigator.of(context).pop();
        },
        children: <Widget>[
          SizedBox(
            height: 56,
            child: Align(
              alignment: Alignment.centerLeft,
              child: Padding(
                padding: const EdgeInsetsDirectional.only(start: 28, end: 16),
                child: Row(
                  children: <Widget>[
                    Text(
                      strings.appTitle,
                      style: settings
                          .design.accentFontTheme.textTheme.headlineSmall
                          ?.copyWith(color: scheme.onSurface),
                    ),
                    const Spacer(),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 4),
                      child: IconButton(
                        onPressed: () => context
                          ..pop()
                          ..push('/todoListSettings'),
                        tooltip: strings.settings,
                        icon: const Icon(Symbols.tune),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          nd.NavigationDrawerDestination(
            icon: Icon(
              Symbols.overview_key,
              weight: 300,
              color: scheme.onSurfaceVariant,
            ),
            selectedIcon: Icon(
              Symbols.overview_key,
              color: scheme.onSecondaryContainer,
            ),
            label: Text(strings.overview),
          ),
          const Divider(indent: 28, endIndent: 28, height: 16),
          ...listWidgets,
          nd.NavigationDrawerDestination(
            icon: const Icon(Symbols.add, weight: 300, size: 24),
            label: Text(strings.newTodoList),
          ),
        ],
      ),
    );
  }

  List<TodoList> getLists(TodoListDatabase db) {
    final order = getOrder();
    final lists = <TodoList>[];
    final gone = <String>{};

    for (final MapEntry(value: id) in order.entries) {
      final list = db.get(id);
      if (list == null) {
        logger.w('Could not find list $id but it was in order');
        gone.add(id);

        continue;
      }

      lists.add(list);
    }

    if (gone.isNotEmpty) {
      final current = order.values.toList();
      for (final id in gone) {
        current.remove(id);
      }

      settings.extensions.todoListsOrder.value = current;
    }

    return lists;
  }

  Map<int, String> getOrder() =>
      settings.extensions.todoListsOrder.value!.asMap();

  Future<void> newList() {
    return showAnimatedDialog<TodoList>(
      context: context,
      animationType: DialogTransitionType.fadeScale,
      builder: (context) => const TodoListDialog(),
    ).then<void>((newList) {
      if (newList == null) return;
      ref.read(todoListDatabaseProvider).store(newList);
      settings.extensions.todoListsOrder.value!.add(newList.id);
      widget.onListSelected(TodoListDrawerEvent.list(newList));
      if (widget.closeOnSelected && mounted) Navigator.of(context).pop();
    });
  }
}

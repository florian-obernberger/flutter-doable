import 'dart:math';

import 'package:flex_color_picker/flex_color_picker.dart';
import 'package:flutter/material.dart';
import 'package:gap/gap.dart';
import 'package:result/result.dart';

import '/strings/strings.dart';
import '/state/settings.dart';
import '/resources/resources.dart';
import '/data/todo_list.dart';
import '/widgets/save_button.dart';
import '/widgets/rich_tooltip.dart';
import '/widgets/svg_icon.dart';
import '/widgets/cancel_changes_dialog.dart';
import '/util/build_text_selection_menu.dart';
import '/util/animated_dialog.dart';
import '/util/extensions/color.dart';

import 'color_picker_dialog.dart';

enum ListDialogMode { newList, editList }

class TodoListDialog extends StatefulWidget {
  const TodoListDialog.edit({
    required TodoList this.todoList,
    super.key,
  }) : mode = ListDialogMode.editList;

  const TodoListDialog({super.key})
      : todoList = null,
        mode = ListDialogMode.newList;

  final TodoList? todoList;
  final ListDialogMode mode;

  @override
  State<TodoListDialog> createState() => _TodoListDialogState();
}

class _TodoListDialogState extends State<TodoListDialog> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  late final TextEditingController titleController;

  late Color? listColor = widget.todoList?.color;
  late final initialColor = randomColor();

  late bool listHidden = widget.todoList?.isHidden ?? false;

  @override
  void initState() {
    super.initState();

    titleController = TextEditingController(text: widget.todoList?.name);
  }

  bool get hasChanges {
    final list = widget.todoList;
    if (list == null) return titleController.text.isNotEmpty;
    return titleController.text.isEmpty || titleController.text != list.name;
  }

  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context)!;
    final settings = Settings.of(context);
    final theme = Theme.of(context);
    final contextScheme = theme.colorScheme;
    final text = theme.textTheme;

    final listScheme = listColor?.listSchemeFromBrightness(
      contextScheme.brightness,
    );

    final scheme = listScheme ?? contextScheme;

    const contentPadding = EdgeInsets.symmetric(horizontal: 24);
    final titleStyle = text.bodyLarge!.copyWith(color: scheme.onSurface);

    return PopScope(
      canPop: false,
      onPopInvoked: (didPop) async {
        if (didPop) return;
        if (!hasChanges) {
          Navigator.of(context).pop();
          return;
        }

        final shouldPop = await showAnimatedDialog<bool>(
          context: context,
          builder: (context) => CancelChangesDialog(
            title: strings.aboutToCancelChanges,
            description: strings.aboutToCancelChangesToList,
          ),
        ).then((value) => value == true);

        if (shouldPop && context.mounted) Navigator.of(context).pop();
      },
      child: Theme(
        data: theme.copyWith(colorScheme: scheme),
        child: Builder(builder: (context) {
          return AlertDialog(
            backgroundColor: scheme.surfaceContainerHigh,
            contentPadding: const EdgeInsets.only(top: 8, bottom: 12),
            titlePadding: const EdgeInsetsDirectional.fromSTEB(24, 24, 16, 0),
            title: SizedBox(
              width: 480,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(switch (widget.mode) {
                    ListDialogMode.editList => strings.editTodoList,
                    ListDialogMode.newList => strings.newTodoList
                  }),
                  ValueListenableBuilder(
                      valueListenable:
                          settings.extensions.hadListVisibilityIntroduction,
                      builder: (context, hadIntro, _) {
                        return RichTooltip(
                          triggerMode: hadIntro
                              ? TooltipTriggerMode.longPress
                              : TooltipTriggerMode.tap,
                          subhead:
                              listHidden ? strings.showList : strings.hideList,
                          supportingText: listHidden
                              ? strings.showListDescription
                              : strings.hideListDescription,
                          actions: hadIntro
                              ? null
                              : <Widget>[
                                  TextButton(
                                    style: TextButton.styleFrom(
                                      padding: const EdgeInsets.all(8),
                                      minimumSize: const Size(12, 36),
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(4),
                                      ),
                                    ),
                                    child: Text(strings.gotIt),
                                    onPressed: () => settings
                                        .extensions
                                        .hadListVisibilityIntroduction
                                        .value = true,
                                  ),
                                ],
                          child: IconButton(
                            icon: SvgIcon(
                              icon: SvgIcons.listW300,
                              color: scheme.secondary,
                            ),
                            selectedIcon:
                                const SvgIcon(icon: SvgIcons.listHiddenW300),
                            isSelected: listHidden,
                            onPressed: hadIntro
                                ? () => setState(() => listHidden = !listHidden)
                                : null,
                          ),
                        );
                      }),
                ],
              ),
            ),
            content: Form(
              key: _formKey,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: contentPadding,
                    child: TextFormField(
                      contextMenuBuilder: buildTextSelectionMenu,
                      controller: titleController,
                      validator: (title) => title == null || title.isEmpty
                          ? strings.titleMayNotBeEmpty
                          : null,
                      onChanged: (_) => setState(() {}),
                      onEditingComplete: onSave,
                      keyboardType: TextInputType.text,
                      autocorrect: true,
                      autofocus: true,
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      style: titleStyle,
                      decoration: InputDecoration(
                        hintText: strings.markRequired(strings.title),
                        hintStyle: titleStyle.copyWith(
                          color: scheme.onSurfaceVariant,
                        ),
                        helperText: strings.required,
                        border: const UnderlineInputBorder(),
                      ),
                    ),
                  ),
                  const Gap(4),
                  ListTile(
                    contentPadding: contentPadding,
                    title: Text(strings.listColor),
                    titleTextStyle: titleStyle,
                    trailing: ColorIndicator(
                      color: listColor ?? Colors.transparent,
                      borderColor: scheme.outline,
                      hasBorder: listColor == null,
                      borderRadius: 8,
                    ),
                    onTap: () async {
                      final colorOptb =
                          await showAnimatedDialog<Option<Color?>>(
                        context: context,
                        animationType: DialogTransitionType.fadeScale,
                        builder: (context) => ColorPickerDialog(
                          initialColor: listColor ?? initialColor,
                        ),
                      );

                      if (colorOptb == null) return;
                      setState(() => listColor = switch (colorOptb) {
                            Some(value: final color) => color,
                            None() => null,
                          });
                    },
                  ),
                ],
              ),
            ),
            actions: [
              TextButton(
                onPressed: Navigator.of(context).pop,
                child: Text(strings.cancel),
              ),
              SaveButton(
                onSave: (_formKey.currentState?.validate() ?? false)
                    ? onSave
                    : null,
              ),
            ],
          );
        }),
      ),
    );
  }

  Color randomColor() =>
      Colors.primaries[Random().nextInt(Colors.accents.length)];

  void onSave() {
    if (!(_formKey.currentState?.validate() ?? false)) return;

    final newName = titleController.value.text.trim();

    final list = widget.todoList
            ?.copyWith(name: newName, isHidden: listHidden)
            .withColor(listColor) ??
        TodoList.fromName(newName, color: listColor, isHidden: listHidden);

    Navigator.of(context).pop(list);
  }
}

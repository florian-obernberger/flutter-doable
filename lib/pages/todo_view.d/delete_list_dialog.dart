import 'package:flutter/material.dart';

import '/strings/strings.dart';

class DeleteTodoListDialog extends StatelessWidget {
  const DeleteTodoListDialog({required this.listName, super.key});

  final String listName;

  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context)!;

    return AlertDialog(
      title: Text(strings.attention),
      content: Text(
        strings.aboutToDeleteList(
            "${strings.quoteDoubleLeft}$listName${strings.quoteDoubleRight}"),
      ),
      actions: [
        TextButton(
          onPressed: () => Navigator.of(context).pop(false),
          child: Text(strings.cancel),
        ),
        TextButton(
          onPressed: () => Navigator.of(context).pop(true),
          child: Text(strings.doContinue),
        ),
      ],
    );
  }
}

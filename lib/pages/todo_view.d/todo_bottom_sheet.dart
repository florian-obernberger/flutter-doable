import 'dart:typed_data';

import 'package:crypto/crypto.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:image_picker/image_picker.dart';
import 'package:material_symbols_icons/symbols.dart';

import '/strings/strings.dart';
import '/state/settings.dart';
import '/state/todo_list_db.dart';
import '/data/todo.dart';
import '/data/notifications/todo_notification.dart';
import '/data/applications_directory.dart';
import '/widgets/save_button.dart';
import '/widgets/star.dart';
import '/widgets/date_picker.dart';
import '/widgets/todo_chips.dart';
import '/widgets/cancel_changes_dialog.dart';
import '/util/formatters/format_text.dart';
import '/util/animated_dialog.dart';
import '/util/build_text_selection_menu.dart';
import '/util/all_but_edge_insets.dart';

class TodoBottomSheet extends ConsumerStatefulWidget {
  const TodoBottomSheet({
    this.isStarred,
    this.listId,
    this.sharedText,
    this.image,
    super.key,
  });

  final bool? isStarred;
  final String? listId;
  final String? sharedText;
  final Uint8List? image;

  @override
  ConsumerState<TodoBottomSheet> createState() => _TodoBottomSheetState();
}

class _TodoBottomSheetState extends ConsumerState<TodoBottomSheet> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  Todo? _todo;
  Todo? _oldTodo;

  Todo get todo {
    if (_todo == null) {
      final sharedTitle = Settings.of(context).general.sharedTextAsTitle.value;

      _todo = Todo(
        creationDate: DateTime.now(),
        title: (sharedTitle ? widget.sharedText : null) ?? '',
        description: sharedTitle ? null : widget.sharedText,
        lastModified: DateTime.now(),
        isImportant: widget.isStarred,
        listId: widget.listId,
      );
    }

    return _todo!;
  }

  Todo get oldTodo {
    _oldTodo ??= todo.clone();
    return _oldTodo!;
  }

  late Uint8List? image = widget.image;

  late final listId = widget.listId;

  bool hadFirstEdit = false;

  bool get hasChips => widget.listId != null || image != null;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final strings = Strings.of(context)!;
    final settings = Settings.of(context);
    final scheme = theme.colorScheme;
    final text = theme.textTheme;
    final mediaQuery = MediaQuery.of(context);

    final titleTextStyle = text.titleLarge;
    final descriptionTextStyle = text.bodyMedium;

    final listDb = ref.watch(todoListDatabaseProvider);

    final contentConfiguration = settings.extensions.enableImages.value
        ? ContentInsertionConfiguration(onContentInserted: (content) {
            if (content.hasData) setState(() => image = content.data!);
          })
        : null;

    return PopScope(
      canPop: !hasEdits,
      onPopInvokedWithResult: (didPop, _) async {
        if (didPop) return;
        final close = await showCloseDialog();
        if (close && context.mounted) Navigator.of(context).pop();
      },
      child: Padding(
        padding: EdgeInsets.only(
          bottom: mediaQuery.viewInsets.bottom,
        ),
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    TextFormField(
                      initialValue: todo.title,
                      contextMenuBuilder: buildTextSelectionMenu,
                      contentInsertionConfiguration: contentConfiguration,
                      validator: (title) {
                        if (title == null) return null;
                        final text = formatText(
                          title,
                          trimText: settings.general.trimTodoTexts.value,
                        );
                        return text.isEmpty ? strings.titleMayNotBeEmpty : null;
                      },
                      onChanged: (title) => setState(
                        () {
                          hadFirstEdit = true;
                          todo.title = formatText(
                            title,
                            trimText: settings.general.trimTodoTexts.value,
                          );
                        },
                      ),
                      onFieldSubmitted: (title) {
                        if (!(_formKey.currentState?.validate() ?? false)) {
                          return;
                        }
                        todo.title = title;
                        Navigator.of(context).pop(todo);
                      },
                      keyboardType: TextInputType.text,
                      autocorrect: true,
                      autofocus: true,
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      style: titleTextStyle?.copyWith(color: scheme.onSurface),
                      minLines: 1,
                      maxLines: 2,
                      decoration: InputDecoration(
                        hintText: strings.markRequired(strings.title),
                        hintStyle: titleTextStyle?.copyWith(
                          color: scheme.onSurfaceVariant,
                        ),
                        helperText: strings.required,
                        border: const UnderlineInputBorder(),
                      ),
                    ),
                    TextFormField(
                      initialValue: todo.description,
                      contextMenuBuilder: (context, editableTextState) =>
                          buildTextSelectionMenu(
                        context,
                        editableTextState,
                        markdownEnabled:
                            settings.extensions.enableMarkdown.value,
                      ),
                      contentInsertionConfiguration: contentConfiguration,
                      onChanged: (desc) {
                        if (desc.isNotEmpty) {
                          todo.description = formatText(
                            desc,
                            trimText: settings.general.trimTodoTexts.value,
                          );
                        } else {
                          todo.description = null;
                        }

                        if (!hadFirstEdit) {
                          setState(() => hadFirstEdit = true);
                        }
                      },
                      autocorrect: true,
                      style: descriptionTextStyle?.copyWith(
                          color: scheme.onSurface),
                      decoration: InputDecoration(
                        hintText: strings.description,
                        hintStyle: descriptionTextStyle?.copyWith(
                          color: scheme.onSurfaceVariant,
                        ),
                        border: InputBorder.none,
                      ),
                      maxLines: 4,
                      minLines: 1,
                      textInputAction: TextInputAction.newline,
                    ),
                  ],
                ),
              ),
              if (hasChips)
                Padding(
                  padding: const EdgeInsetsDirectional.fromSTEB(20, 4, 20, 2),
                  child: Wrap(
                    spacing: 4,
                    runSpacing: 4,
                    children: <Widget>[
                      if (listId != null && listDb.contains(listId!))
                        ListChip(listDb.get(widget.listId!)!),
                      if (image != null)
                        ImageChip.avatar(
                          image!,
                          onDelete: () => setState(() {
                            todo.image = image = null;
                          }),
                        ),
                      if (todo.relevantDate != null)
                        DueDateChip(
                          todo.relevantDateTime!,
                          showTime: todo.relevantTime != null,
                          onDelete: clearDate,
                        )
                    ],
                  ),
                ),
              Container(
                padding: const AllButEdgeInsets(4, end: 16, start: 8)
                    .add(EdgeInsets.only(bottom: mediaQuery.padding.bottom)),
                child: Theme(
                  data: theme.copyWith(
                    buttonTheme: theme.buttonTheme.copyWith(
                      padding: EdgeInsets.zero,
                    ),
                    tooltipTheme: theme.tooltipTheme.copyWith(
                      preferBelow: false,
                    ),
                  ),
                  child: Row(
                    children: <Widget>[
                      StarButton(
                        isStarred: todo.isImportant,
                        onTap: (isImportant) {
                          setState(() => todo.isImportant = !isImportant);
                          return Future.value(!isImportant);
                        },
                        unselectedColor: scheme.onSurfaceVariant,
                      ),
                      if (settings.extensions.enableImages.value)
                        IconButton(
                          onPressed: pickImage,
                          tooltip: strings.addImage,
                          isSelected: image != null,
                          icon: const Icon(Symbols.wallpaper, weight: 500),
                          selectedIcon: Icon(
                            Symbols.wallpaper,
                            weight: 500,
                            fill: 1,
                            color: ImageChip.colors(scheme).background,
                          ),
                        ),
                      TodoDatePicker(
                        todo: todo,
                        onCleared: clearDate,
                        showChip: false,
                        onSelected: (selectedDate, selectedTime) =>
                            setState(() {
                          removeRelevantNotificationsFromTodo(todo);

                          todo
                            ..relevantDate = selectedDate
                            ..relevantTime = selectedTime;

                          addRelevantNotificationsToTodo(
                            todo,
                            settings.extensions,
                          );
                        }),
                        chipOnRight: true,
                      ),
                      const Spacer(),
                      buildSaveButton(strings),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildSaveButton(Strings strings) {
    final enabled =
        hadFirstEdit ? _formKey.currentState?.validate() ?? false : false;

    return SaveButton(
      onSave: enabled
          ? () async {
              if (image != null) {
                final image = this.image!;
                final file = todoImagesDirectory.image(todo.id);

                todo.image = sha256.convert(image).toString();

                await file.create(recursive: true);
                await file.writeAsBytes(image);
                if (todo.image != oldTodo.image) await FileImage(file).evict();
              }

              if (mounted) Navigator.of(context).pop(todo);
            }
          : null,
      label: strings.save,
    );
  }

  bool get hasEdits => todo != oldTodo;

  Future<void> pickImage() async {
    final imagePicker = ImagePicker();
    final imageFile = await imagePicker.pickImage(source: ImageSource.gallery);
    if (imageFile == null) return;

    final imageData = await imageFile.readAsBytes();
    final imageHash = sha256.convert(imageData).toString();
    if (image == null ||
        todo.image != imageHash ||
        image!.lengthInBytes != imageData.lengthInBytes ||
        image! != imageData) {
      todo.image = imageHash;
      setState(() => image = imageData);
    }
  }

  Future<bool> showCloseDialog() {
    final strings = Strings.of(context)!;

    return showAnimatedDialog<bool>(
      context: context,
      builder: (context) => CancelChangesDialog(
        title: strings.aboutToCancelChanges,
        description: strings.aboutToCancelChangesDescription,
      ),
    ).then((value) => value == true);
  }

  void clearDate() => setState(() {
        removeRelevantNotificationsFromTodo(todo);

        todo
          ..relevantDate = null
          ..relevantTime = null;
      });
}

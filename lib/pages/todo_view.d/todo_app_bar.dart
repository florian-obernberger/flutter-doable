import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:share_plus/share_plus.dart';

import '/strings/strings.dart';
import '/state/settings.dart';
import '/state/todo_db.dart';
import '/data/todo_list.dart';
import '/types/window_breakpoint.dart';
import '/util/todo_to_human_strings.dart';
import '/widgets/pride/pride_month.dart';
import '/widgets/pride/pride_banner.dart';

import 'app_bar_state.dart';
import 'selection_state.dart';
import 'date_filter_row.dart';
import 'selection_appbar.dart';
import 'todo_view_menu.dart';

typedef TodoId = String;

final class TodoAppBar extends ConsumerWidget implements PreferredSizeWidget {
  static const double appBarHeight = 64;

  final AppBarState appBarState;
  final AnimationController appBarIconAnimationController;
  final SelectionState selectionState;
  final ScrollNotificationPredicate notificationPredicate;
  final List<TodoId> maxSelection;

  final DateFilterRow? dateFilterRow;
  final Widget? searchIcon;
  final TodoViewMenu todoViewMenu;

  final TodoList? currentList;
  final VoidCallback openDrawer;

  const TodoAppBar({
    required this.appBarState,
    required this.appBarIconAnimationController,
    required this.selectionState,
    required this.notificationPredicate,
    required this.maxSelection,
    required this.dateFilterRow,
    required this.searchIcon,
    required this.todoViewMenu,
    required this.currentList,
    required this.openDrawer,
    super.key,
  });

  @override
  Size get preferredSize => Size.fromHeight(appBarHeight + preferredHeight);

  double get preferredHeight {
    double height = 0;
    if (dateFilterRow != null) height += DateFilterRow.preferredSizeHeight;
    return height;
  }

  bool get _isInSelection => selectionState.isNotEmpty;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return ListenableBuilder(
      listenable: Listenable.merge([
        appBarState,
        selectionState,
        appBarIconAnimationController,
      ]),
      builder: (context, _) => _build(context, ref),
    );
  }

  Widget _build(BuildContext context, WidgetRef ref) {
    final settings = Settings.of(context);
    final strings = Strings.of(context)!;
    final breakpoint = MediaQuery.of(context).breakpoint;

    final appBarIcon = this.appBarIcon(settings);

    if (_isInSelection) {
      return selectionAppBar(
        ref: ref,
        bottom: dateFilterRow,
        leading: appBarIcon,
        todoDatabase: ref.watch(todoDatabaseProvider),
        selectedTodos: selectionState.selected,
        maxSelection: maxSelection.length,
        selectAll: () => selectionState.addAll(maxSelection),
        onShare: () async {
          final selected = selectionState.selected;
          final text = todosToHumanReadableString(
            ref: ref,
            strings: strings,
            showListNames: currentList == null,
            todoIds: selected,
          );

          if (selected.length == 1 && settings.extensions.enableImages.value) {
            final todo = ref.read(todoDatabaseProvider).get(selected.first);
            if (todo != null && todo.image != null) {
              await Share.shareXFiles(
                [XFile(todo.image!, mimeType: 'image/png')],
                text: text,
              );

              return;
            }
          }

          await Share.share(text);
        },
        clearSelection: selectionState.clear,
      );
    }

    final Widget title;
    final accentTextTheme = settings.design.accentFontTheme.textTheme;

    if (currentList != null) {
      final textTheme = settings.design.accentFontTheme.type.isTextAccent
          ? accentTextTheme
          : settings.design.primaryFontTheme.textTheme;

      title = Text(currentList!.name, style: textTheme.titleLarge);
    } else {
      final text = switch (PrideMonth.currentMonth) {
        PrideMonth() when !settings.accessability.enablePrideFlags.value =>
          strings.appTitle,
        PrideMonth.queer => 'Happy Pride!',
        PrideMonth.disability => 'Happy Disability Pride!',
        null => strings.appTitle,
      };

      title = Text(
        text,
        style: accentTextTheme.headlineSmall,
      );
    }
    return AppBar(
      flexibleSpace: PrideBanner.currentOf(context),
      notificationPredicate: notificationPredicate,
      scrolledUnderElevation:
          settings.extensions.enableDateFilters.value ? 0 : null,
      title: title,
      bottom: dateFilterRow,
      centerTitle: shouldCenterTitle(settings, breakpoint),
      leading: buildLeading(settings, context, breakpoint, appBarIcon),
      leadingWidth:
          settings.extensions.enableLists.value || breakpoint.isMediumOrExpanded
              ? 56
              : settings.extensions.enableTodoSearch.value
                  ? 16
                  : 56,
      actions: <Widget>[
        if (searchIcon != null) searchIcon!,
        todoViewMenu,
      ],
    );
  }

  Widget? appBarIcon(Settings settings) {
    if (!settings.extensions.enableLists.value ||
        settings.accessability.reduceMotion.value) {
      return null;
    }

    return AnimatedIcon(
      size: 24,
      progress: CurvedAnimation(
        curve: Easing.linear,
        parent: appBarIconAnimationController,
      ),
      icon: AnimatedIcons.menu_close,
    );
  }

  Widget? buildLeading(
    Settings settings,
    BuildContext context,
    WindowBreakpoint breakpoint,
    Widget? appBarIcon,
  ) {
    if (settings.extensions.enableLists.value && appBarIcon != null) {
      return IconButton(
        onPressed: openDrawer,
        tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
        icon: appBarIcon,
      );
    }
    if ((settings.extensions.enableLists.value ||
        !settings.extensions.enableTodoSearch.value)) return null;
    return const SizedBox.shrink();
  }

  bool shouldCenterTitle(Settings settings, WindowBreakpoint breakpoint) {
    if (currentList != null) return false;
    if (settings.extensions.enableTodoSearch.value && breakpoint.isCompact) {
      return false;
    }
    if (!settings.extensions.enableTodoSearch.value) return true;
    return breakpoint.isMediumOrExpanded;
  }
}

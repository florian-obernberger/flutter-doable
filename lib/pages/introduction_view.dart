import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

import '/strings/strings.dart';
import '/state/settings.dart';
import '/data/todo.dart';
import '/pages/settings_view.d/backup_and_sync_view.d/nextcloud.dart';
import '/pages/settings_view.d/settings.dart';
import '/pages/todo_view.d/todo_item.dart';
import '/widgets/star.dart';
import '/widgets/link.dart';
import '/types/window_breakpoint.dart';
import '/classes/motion.dart';

import 'introduction_view.d/side_screen.dart';
import 'introduction_view.d/page.dart';

class IntroductionView extends StatefulWidget {
  const IntroductionView({super.key});

  @override
  State<IntroductionView> createState() => _IntroductionViewState();
}

class _IntroductionViewState extends State<IntroductionView> {
  int currentIndex = 0;
  int pageNumber = 0;
  late final controller = PageController(initialPage: currentIndex);
  Todo? todo;

  bool starIsStarred = false;

  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context)!;
    final settings = Settings.of(context);
    final scheme = Theme.of(context).colorScheme;
    final text = Theme.of(context).textTheme;

    final accentTextTheme = settings.design.accentFontTheme.textTheme;

    final pages = <Widget>[
      IntroPage(
        accentTextTheme: accentTextTheme,
        titleWidget: Text(
          strings.introWelcome,
          style: accentTextTheme.displayMedium?.copyWith(
            color: scheme.primary,
            fontWeight: FontWeight.normal,
          ),
          textAlign: TextAlign.center,
        ),
        description: strings.introWelcomeDescription,
      ),
      IntroPage(
        accentTextTheme: accentTextTheme,
        title: strings.introStar,
        description: strings.introStarDescription,
        footer: ConstrainedBox(
          constraints: const BoxConstraints(maxWidth: 52),
          child: StarButton(
            isStarred: starIsStarred,
            onTap: (isLiked) async {
              setState(() => starIsStarred = !isLiked);
              return !isLiked;
            },
            buttonSize: 36,
            iconSize: 36,
          ),
        ),
      ),
      IntroPage(
        accentTextTheme: accentTextTheme,
        title: strings.introLinks,
        description: strings.introLinksDescription,
        footer: Tel(number: '+1-513-273-7724', baseStyle: text.titleMedium!),
      ),
      IntroPage(
        accentTextTheme: accentTextTheme,
        title: strings.introDoubleTap,
        description: strings.introDoubleTapDescription,
        footer: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            const Divider(),
            todoCard,
          ],
        ),
      ),
      IntroPage(
        accentTextTheme: accentTextTheme,
        title: strings.introNextcloud,
        description: strings.introNextcloudDescription,
        footer: const NextcloudStorage(),
      ),
      IntroPage(
        accentTextTheme: accentTextTheme,
        title: strings.introExtensions,
        description: strings.introExtensionsDescription,
        footer: OutlinedButton(
          onPressed: () {
            context
              ..go('/')
              ..push('/settings', extra: SettingsPage.features);
            if (!settings.miscellaneous.hadIntroduction.value) {
              settings.miscellaneous.migratedToRemotes.value = true;
            }
            settings.miscellaneous.hadIntroduction.value = true;
          },
          child: Text(strings.introExtensionsButton),
        ),
      ),
    ];

    pageNumber = pages.length - 1; // 0 indexing

    final breakpoint = WindowBreakpoint.fromMediaQuery(MediaQuery.of(context));

    return Row(
      children: <Widget>[
        Expanded(
          child: Scaffold(
            appBar: AppBar(toolbarHeight: 0),
            resizeToAvoidBottomInset: true,
            bottomSheet:
                breakpoint.isExpanded ? null : buildBottomBar(pages.length),
            body: SafeArea(
              child: PageView(
                controller: controller,
                onPageChanged: (index) => setState(() => currentIndex = index),
                pageSnapping: true,
                children: pages
                    .map((page) => ConstrainedBox(
                          constraints: const BoxConstraints(maxWidth: 640),
                          child: page,
                        ))
                    .toList(),
              ),
            ),
          ),
        ),
        if (breakpoint.isExpanded) ...[
          // VerticalDivider(width: 1, color: scheme.outline),
          IntroSideSheet(
            items: <IntroItem>[
              IntroItem(title: strings.introWelcome, position: 0),
              IntroItem(title: strings.introStar, position: 1),
              IntroItem(title: strings.introLinks, position: 2),
              IntroItem(title: strings.introDoubleTap, position: 3),
              IntroItem(title: strings.introNextcloud, position: 4),
              IntroItem(title: strings.introExtensions, position: 5),
            ],
            currentPosition: currentIndex,
            onFinish: done,
            onNext: next,
            onBack: back,
            onJump: (newPosition) => controller.animateToPage(
              newPosition,
              duration: Motion.long2,
              curve: Curves.easeInOutCubicEmphasized,
            ),
          ),
        ],
      ],
    );
  }

  Widget get todoCard {
    final strings = Strings.of(context)!;

    todo ??= Todo(
      isCompleted: false,
      creationDate: DateTime.now(),
      title: strings.introDoubleTapTodoTitle,
      description: strings.introDoubleTapTodoDescription,
      lastModified: DateTime.now(),
    );

    return TodoCard(
      state: TodoCardState.none,
      completed: todo!.isCompleted,
      allowSwipeActions: true,
      onOpen: null,
      onSelect: null,
      item: TodoItem(
        todo: todo!,
        key: ValueKey(todo!.hashCode),
        canInteract: () => true,
        showList: () => false,
        onEvent: (event, local) {
          if (event == TodoItemEvent.complete) {
            todo!.isCompleted ? todo!.uncomplete() : todo!.complete();
            setState(() {});
          }
        },
      ),
    );
  }

  Widget buildBottomBar(int pageCount) {
    final strings = Strings.of(context)!;
    final scheme = Theme.of(context).colorScheme;

    final bottom = MediaQuery.of(context).viewPadding.bottom;

    final isFirst = currentIndex == 0;
    final isLast = currentIndex == pageNumber;

    return SizedBox(
      width: double.infinity,
      height: 64 + bottom,
      child: Padding(
        padding: const EdgeInsets.all(8).add(
          EdgeInsets.only(bottom: bottom),
        ),
        child: Stack(
          children: <Widget>[
            Align(
              alignment: AlignmentDirectional.centerStart,
              child: TextButton(
                onPressed: isFirst ? null : back,
                child: Text(
                  strings.back,
                  maxLines: 1,
                ),
              ),
            ),
            Align(
              alignment: Alignment.center,
              child: SmoothPageIndicator(
                controller: controller, // PageController
                count: pageCount,
                effect: WormEffect(
                  activeDotColor: scheme.primary,
                  dotColor: scheme.primaryContainer,
                  dotHeight: 8,
                  dotWidth: 8,
                  spacing: 6,
                ),
              ),
            ),
            Align(
              alignment: AlignmentDirectional.centerEnd,
              child: isLast
                  ? TextButton(
                      onPressed: done,
                      child: Text(strings.done, maxLines: 1),
                    )
                  : TextButton(
                      onPressed: next,
                      child: Text(strings.next, maxLines: 1),
                    ),
            )
          ],
        ),
      ),
    );
  }

  void done() {
    final settings = Settings.of(context);

    if (!settings.miscellaneous.hadIntroduction.value) {
      settings.miscellaneous.migratedToRemotes.value = true;
    }
    settings.miscellaneous.hadIntroduction.value = true;
    context.pushReplacement('/');
  }

  void back() {
    currentIndex -= 1;
    changePage();
  }

  void next() {
    currentIndex += 1;
    changePage();
  }

  changePage() => controller.animateToPage(
        currentIndex,
        curve: Curves.easeOut,
        duration: const Duration(milliseconds: 200),
      );
}

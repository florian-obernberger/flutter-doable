import 'dart:async';

import 'package:animated_list_plus/animated_list_plus.dart';
import 'package:flutter/foundation.dart';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:material_symbols_icons/symbols.dart';
import 'package:share_plus/share_plus.dart';

import '/strings/strings.dart';
import '/state/settings.dart';
import '/classes/logger.dart';
import '/classes/motion.dart';
import '/classes/bug_report.dart';
import '/types/window_breakpoint.dart';
import '/util/copy_text.dart';
import '/util/animated_dialog.dart';
import '/util/formatters/format_date.dart';
import '/widgets/delete_dialog.dart';
import '/widgets/rich_tooltip.dart';

import 'log_view.d/log_card.dart';
import 'log_view.d/bug_report_dialog.dart';
import 'log_view.d/entry_or_day.dart';

const maxLogCount = 250;

enum LogViewPopupEntry { copySettings, copySystemInfo }

class SettingsLogView extends ConsumerStatefulWidget {
  const SettingsLogView({super.key});

  @override
  ConsumerState<SettingsLogView> createState() => _SettingsLogViewState();
}

class _SettingsLogViewState extends ConsumerState<SettingsLogView> {
  final _controller = ScrollController();

  @override
  void initState() {
    super.initState();

    logger.addListener(() => mounted ? setState(() {}) : null);
  }

  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context)!;
    final scheme = Theme.of(context).colorScheme;
    final text = Theme.of(context).textTheme;

    var logs = logger.logs
        .take(maxLogCount)
        // only show debug messages in debug mode
        .where((log) => kDebugMode || log.level != LogLevel.debug)
        .where((log) => log.message != null)
        .toList()
      ..sort();

    final logsAndDays = splitLogsPerDay(logs).toList();

    return Scaffold(
      floatingActionButton: RichTooltip(
        preferBelow: false,
        subhead: strings.createSummaryForBugReport,
        supportingText: strings.createSummaryForBugReportDetails,
        child: FloatingActionButton(
          elevation: 0,
          onPressed: createBugReport,
          child: const Icon(Symbols.lab_profile),
        ),
      ),
      bottomNavigationBar: buildBottomAppBar(logs),
      floatingActionButtonLocation: FloatingActionButtonLocation.endContained,
      body: logs.isEmpty
          ? buildEmptyList()
          : AnimationLimiter(
              child: ImplicitlyAnimatedList<EntryOrDay>(
                physics: const ClampingScrollPhysics(),
                controller: _controller,
                padding: const EdgeInsets.all(12),
                areItemsTheSame: (oldItem, newItem) => oldItem.id == newItem.id,
                items: logsAndDays,
                insertDuration: Motion.medium2,
                removeDuration: Motion.medium2,
                itemBuilder: (context, animation, item, pos) => animateItem(
                  item,
                  pos,
                  switch (item) {
                    Day() => Padding(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 12,
                          vertical: 8,
                        ),
                        child: Text(
                          formatDate(
                            Settings.of(context).dateTime,
                            strings,
                            item.day,
                          ),
                          style: text.labelLarge?.copyWith(
                            color: scheme.onSurfaceVariant,
                          ),
                        ),
                      ),
                    Entry() => LogCard(
                        entry: item.log,
                        delete: () => logger.delete(item.id),
                        position: getLogCardPosition(logsAndDays, pos),
                      ),
                  },
                  animation,
                ),
              ),
            ),
    );
  }

  LogCardPosition getLogCardPosition(List<EntryOrDay> entries, final int pos) {
    // `pos` is always the location of an [Entry].
    final len = entries.length;
    if (pos >= len) return LogCardPosition.single;

    final (EntryOrDay, EntryOrDay) neighbors;

    if (pos == 0 && pos + 1 >= len) {
      // assert : pos == 0 and pos + 1 ≥ len

      // The current position is 0 and there is no `next`.
      neighbors = (Day.now(), Day.now());
    } else if (pos == 0 && pos + 1 < len) {
      // assert : pos == 0 and pos + 1 < len

      // The current position is 0 but there is a `next`.
      neighbors = (Day.now(), entries[pos + 1]);
    } else if (pos + 1 >= len) {
      // assert : pos ≠ 0 and pos + 1 ≥ len

      // The current position is **not** 0 but there is no `next`.
      neighbors = (entries[pos - 1], Day.now());
    } else {
      // assert : pos ≠ 0 and pos + 1 < len

      // The current position is not 0 **and** there is a `next`.
      neighbors = (entries[pos - 1], entries[pos + 1]);
    }

    return switch (neighbors) {
      (Day(), Day()) => LogCardPosition.single,
      (Entry(), Day()) => LogCardPosition.end,
      (Day(), Entry()) => LogCardPosition.start,
      (Entry(), Entry()) => LogCardPosition.middle,
    };
  }

  Iterable<EntryOrDay> splitLogsPerDay(Iterable<LogEntry> logs) sync* {
    DateTime? currentDay;
    for (final log in logs) {
      if (currentDay == null || !log.logTime.isSameDay(currentDay)) {
        yield Day(log.logTime);
        currentDay = log.logTime;

        // Need to yield the log as well, otherwise it will get lost completely.
        yield Entry(log);
        continue;
      }

      yield Entry(log);
    }
  }

  Widget animateItem(
    EntryOrDay entryOrDay,
    int pos,
    Widget child,
    Animation<double> animation,
  ) {
    if (Settings.of(context).accessability.reduceMotion.value) {
      return FadeTransition(opacity: animation, child: child);
    }

    var widget = AnimationConfiguration.staggeredList(
      key: ValueKey(entryOrDay.id),
      position: pos,
      duration: const Duration(milliseconds: 225),
      delay: const Duration(milliseconds: 36),
      child: SlideAnimation(
        curve: Curves.easeInOutCubicEmphasized,
        duration: Motion.medium1,
        verticalOffset: 52,
        child: FadeInAnimation(
          duration: Motion.medium3,
          curve: Curves.easeInOutCubicEmphasized,
          child: child,
        ),
      ),
    );

    final sizeFactor = CurvedAnimation(
      parent: animation,
      curve: Easing.standard,
    );

    final opacity = CurvedAnimation(
      parent: animation,
      curve: Easing.standardAccelerate,
    );

    return SizeTransition(
      axisAlignment: 0.75,
      sizeFactor: sizeFactor,
      child: FadeTransition(
        opacity: opacity,
        child: widget,
      ),
    );
  }

  TooltipTheme buildBottomAppBar(List<LogEntry> logs) {
    final theme = Theme.of(context);
    final strings = Strings.of(context)!;

    return TooltipTheme(
      data: theme.tooltipTheme.copyWith(preferBelow: false),
      child: AnimatedOpacity(
        opacity: logs.isEmpty ? 0 : 1,
        duration: Motion.short4,
        child: BottomAppBar(
          child: Row(
            children: <Widget>[
              buildPopupMenu(),
              IconButton(
                padding: EdgeInsets.zero,
                tooltip: strings.shareAllLogs,
                onPressed: () async => await Share.share(
                  (await BugReport.fromContext(
                    context,
                    includeSettings: false,
                    includeSystemInfo: false,
                  ))
                      .toJson(),
                ),
                icon: const Icon(Symbols.forward),
              ),
              IconButton(
                padding: EdgeInsets.zero,
                tooltip: strings.copyAllLogs,
                onPressed: () async {
                  final content = await BugReport.fromContext(
                    context,
                    includeSettings: false,
                    includeSystemInfo: false,
                  );

                  if (mounted) await copyText(context, content.toJson());
                },
                icon: const Icon(Symbols.copy_all),
              ),
              IconButton(
                onPressed: () => showDeleteDialog(
                  context: context,
                  description: strings.aboutToClearAllLogs,
                ).then((delete) {
                  if (delete == true) {
                    logger.clear();
                    _controller.jumpTo(0);
                  }
                }),
                tooltip: strings.clearAllLogs,
                icon: const Icon(Symbols.clear_all),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> createBugReport() async {
    await showAnimatedDialog(
      context: context,
      animationType: DialogTransitionType.fadeScale,
      builder: (context) => const BugReportDialog(),
    );
  }

  Widget buildEmptyList() {
    final strings = Strings.of(context)!;
    final scheme = Theme.of(context).colorScheme;
    final text = Theme.of(context).textTheme;
    final mediaQuery = MediaQuery.of(context);

    final isExpanded = WindowBreakpoint.of(context).isExpanded;

    return GestureDetector(
      onDoubleTap: () {
        HapticFeedback.lightImpact();
        logger.log(
          LogLevel.log,
          message: "Hello from Doable's log system!",
          tag: 'Welcome',
          error: "Here is where you'd be shown the error message! "
              'If there is one, that is.',
        );
      },
      child: Container(
        color: Colors.transparent,
        width: double.infinity,
        height: mediaQuery.size.height -
            mediaQuery.viewPadding.top -
            mediaQuery.viewPadding.bottom -
            (Theme.of(context).appBarTheme.toolbarHeight ?? 64),
        child: Padding(
          padding: isExpanded
              ? const EdgeInsets.only(top: 64)
              : const EdgeInsets.only(bottom: 32),
          child: Center(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Icon(
                  Symbols.inbox,
                  weight: 100,
                  size: 64,
                  color: scheme.secondary,
                ),
                FractionallySizedBox(
                  widthFactor: 0.6,
                  child: Text(
                    strings.noLogsYet,
                    textAlign: TextAlign.center,
                    style: text.titleMedium?.copyWith(
                      color: scheme.onSurfaceVariant,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<void> onPopupSelected(LogViewPopupEntry entry) async {
    final report = await BugReport.fromContext(
      context,
      includeLogs: false,
      includeSettings: entry == LogViewPopupEntry.copySettings,
      includeSystemInfo: entry == LogViewPopupEntry.copySystemInfo,
    );

    if (mounted) return copyText(context, report.toJson());
  }

  Widget buildPopupMenu() {
    return PopupMenuButton<LogViewPopupEntry>(
      icon: Icon(
        Symbols.more_vert,
        weight: 800,
        color: Theme.of(context).colorScheme.onSurfaceVariant,
      ),
      onSelected: onPopupSelected,
      enableFeedback: true,
      clipBehavior: Clip.antiAlias,
      position: PopupMenuPosition.under,
      itemBuilder: (context) =>
          LogViewPopupEntry.values.map(buildPopupEntry).toList(),
    );
  }

  PopupMenuEntry<LogViewPopupEntry> buildPopupEntry(LogViewPopupEntry entry) {
    final strings = Strings.of(context)!;

    final content = switch (entry) {
      LogViewPopupEntry.copySettings => strings.copySettings,
      LogViewPopupEntry.copySystemInfo => strings.copySystemInfo,
    };

    return PopupMenuItem(
      padding: const EdgeInsets.symmetric(horizontal: 12),
      height: 48,
      value: entry,
      child: Text(content),
    );
  }
}

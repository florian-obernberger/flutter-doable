import 'package:flutter/material.dart';

import '/strings/strings.dart';
import '/state/settings.dart';
import '/util/formatters/format_duration.dart';

const _availableDurations = [
  Duration(minutes: 3),
  Duration(minutes: 5),
  Duration(minutes: 10),
  Duration(minutes: 15),
  Duration(minutes: 30),
  Duration(hours: 1),
  Duration(hours: 6),
  Duration(hours: 12),
  Duration(days: 1),
  Duration(days: 3),
  Duration(days: 7),
  Duration(days: 14),
  Duration(days: 30),
  Duration(days: 60),
  Duration(days: 90),
  Duration(days: 120),
];

class DeleteAfterDialog extends StatefulWidget {
  const DeleteAfterDialog({super.key});

  @override
  State<DeleteAfterDialog> createState() => _DeleteAfterDialogState();
}

class _DeleteAfterDialogState extends State<DeleteAfterDialog> {
  late Duration duration;

  Settings? _settings;
  Settings get settings => _settings!;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    if (_settings == null) {
      _settings = Settings.of(context);

      duration = settings.general.deleteCompletedAfter.value ??
          const Duration(days: -1);
      if (duration.isNegative) duration = const Duration(days: -1);
    }
  }

  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context)!;
    final scheme = Theme.of(context).colorScheme;

    return AlertDialog(
      clipBehavior: Clip.antiAliasWithSaveLayer,
      contentPadding: const EdgeInsets.fromLTRB(0, 16, 0, 24),
      title: Text(strings.deleteCompletedAfter),
      content: SizedBox(
        height: 56 * 6.75,
        child: Column(
          children: <Widget>[
            Divider(color: scheme.outline),
            Expanded(
              child: Material(
                color: DialogTheme.of(context).backgroundColor,
                child: SingleChildScrollView(
                  physics: const ClampingScrollPhysics(),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      RadioListTile<Duration>(
                        tileColor: Colors.transparent,
                        value: const Duration(days: -1),
                        groupValue: duration,
                        onChanged: changeDuration,
                        title: Text(strings.never),
                      ),
                      for (var dur in _availableDurations)
                        RadioListTile<Duration>(
                          tileColor: Colors.transparent,
                          value: dur,
                          groupValue: duration,
                          onChanged: changeDuration,
                          title: Text(formatDuration(context, dur)),
                        ),
                    ],
                  ),
                ),
              ),
            ),
            Divider(color: scheme.outline),
          ],
        ),
      ),
      actions: [
        TextButton(
          onPressed: Navigator.of(context).pop,
          child: Text(strings.cancel),
        ),
        TextButton(
          onPressed: () => Navigator.of(context).pop(duration),
          child: Text(strings.ok),
        ),
      ],
    );
  }

  void changeDuration(Duration? dur) {
    if (dur == null) return;

    setState(() {
      duration = dur;
    });
  }
}

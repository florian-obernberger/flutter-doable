import 'package:flutter/material.dart';

import '/strings/strings.dart';
import '/state/settings.dart';
import '/types/app_language.dart';

class LanguageDialog extends StatefulWidget {
  const LanguageDialog({super.key});

  @override
  State<LanguageDialog> createState() => _LanguageDialogState();
}

class _LanguageDialogState extends State<LanguageDialog> {
  late var appLanguage = Settings.of(context).general.appLanguage.value;

  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context)!;
    final scheme = Theme.of(context).colorScheme;

    final languages = [
      AppLanguage.system,
      ...AppLanguage.values.where((l) => l != AppLanguage.system).toList()
        ..sort(
          (a, b) => a.locale!.languageCode.compareTo(b.locale!.languageCode),
        ),
    ];

    return AlertDialog(
      clipBehavior: Clip.antiAliasWithSaveLayer,
      contentPadding: const EdgeInsets.fromLTRB(0, 16, 0, 24),
      title: Text(strings.appLanguage),
      content: Material(
        color: DialogTheme.of(context).backgroundColor,
        child: SizedBox(
          height: 56 * 6.75,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Divider(color: scheme.outline),
              // for (var language in languages)
              //   RadioListTile<AppLanguage>(
              //     tileColor: Colors.transparent,
              //     value: language,
              //     title: Text(language.localized(strings)),
              //     groupValue: appLanguage,
              //     onChanged: onChanged,
              //   ),
              Expanded(
                child: Material(
                  color: DialogTheme.of(context).backgroundColor,
                  child: SingleChildScrollView(
                    physics: const ClampingScrollPhysics(),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        for (var language in languages)
                          RadioListTile<AppLanguage>(
                            tileColor: Colors.transparent,
                            value: language,
                            title: Text(language.nativeName(context)),
                            subtitle: language == AppLanguage.system
                                ? null
                                : Text(language.englishName(context)),
                            groupValue: appLanguage,
                            onChanged: onChanged,
                          ),
                      ],
                    ),
                  ),
                ),
              ),
              Divider(color: scheme.outline),
            ],
          ),
        ),
      ),
      actions: [
        TextButton(
          onPressed: Navigator.of(context).pop,
          child: Text(strings.cancel),
        ),
        TextButton(
          onPressed: () => Navigator.of(context).pop(appLanguage),
          child: Text(strings.ok),
        ),
      ],
    );
  }

  void onChanged(AppLanguage? language) {
    if (language != null) {
      setState(() => appLanguage = language);
    }
  }
}

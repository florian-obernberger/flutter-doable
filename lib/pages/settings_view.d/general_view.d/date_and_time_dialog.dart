import 'package:flutter/material.dart';

import '/strings/strings.dart';
import '/types/date_and_time_formats.dart';
import '/state/settings.dart';

final exampleDateTime = DateTime(1937, 11, 23, 15, 30);
final exampleDateTimeAfternoon = DateTime(1937, 11, 23, 3, 30);

class DateFormatDialog extends StatefulWidget {
  const DateFormatDialog({super.key});

  @override
  State<DateFormatDialog> createState() => _DateFormatDialogState();
}

class _DateFormatDialogState extends State<DateFormatDialog> {
  late var dateFormat = Settings.of(context).dateTime.dateFormat.value;

  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context)!;

    return AlertDialog(
      clipBehavior: Clip.antiAliasWithSaveLayer,
      contentPadding: const EdgeInsets.fromLTRB(0, 16, 0, 24),
      title: Text(strings.dateFormat),
      content: Material(
        color: DialogTheme.of(context).backgroundColor,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            for (var format in DateFormat.values)
              RadioListTile<DateFormat>(
                tileColor: Colors.transparent,
                value: format,
                title: Text(format.format(exampleDateTime)),
                groupValue: dateFormat,
                onChanged: onChanged,
              ),
          ],
        ),
      ),
      actions: [
        TextButton(
          onPressed: Navigator.of(context).pop,
          child: Text(strings.cancel),
        ),
        TextButton(
          onPressed: () => Navigator.of(context).pop(dateFormat),
          child: Text(strings.ok),
        ),
      ],
    );
  }

  void onChanged(DateFormat? format) {
    if (format != null) {
      setState(() => dateFormat = format);
    }
  }
}

class TimeFormatDialog extends StatefulWidget {
  const TimeFormatDialog({super.key});

  static getExample(TimeFormat format) =>
      '${format.format(exampleDateTimeAfternoon)} / ${format.format(exampleDateTime)}';

  @override
  State<TimeFormatDialog> createState() => _TimeFormatDialogState();
}

class _TimeFormatDialogState extends State<TimeFormatDialog> {
  late var timeFormat = Settings.of(context).dateTime.timeFormat.value;

  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context)!;

    return AlertDialog(
      clipBehavior: Clip.antiAliasWithSaveLayer,
      contentPadding: const EdgeInsets.fromLTRB(0, 16, 0, 24),
      title: Text(strings.timeFormat),
      content: Material(
        color: DialogTheme.of(context).backgroundColor,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            for (var format in TimeFormat.values)
              RadioListTile<TimeFormat>(
                tileColor: Colors.transparent,
                value: format,
                title: Text(TimeFormatDialog.getExample(format)),
                groupValue: timeFormat,
                onChanged: onChanged,
              ),
          ],
        ),
      ),
      actions: [
        TextButton(
          onPressed: Navigator.of(context).pop,
          child: Text(strings.cancel),
        ),
        TextButton(
          onPressed: () => Navigator.of(context).pop(timeFormat),
          child: Text(strings.ok),
        ),
      ],
    );
  }

  void onChanged(TimeFormat? format) {
    if (format != null) {
      setState(() => timeFormat = format);
    }
  }
}

import 'package:flutter/material.dart';
import 'package:result/result.dart';

import '/strings/strings.dart';
import '/types/weekday.dart';

class StartOfTheWeekPickerDialog extends StatefulWidget {
  const StartOfTheWeekPickerDialog(this.initial, {super.key});

  final Weekday? initial;

  @override
  State<StartOfTheWeekPickerDialog> createState() =>
      _StartOfTheWeekPickerDialogState();
}

class _StartOfTheWeekPickerDialogState
    extends State<StartOfTheWeekPickerDialog> {
  late Weekday? current = widget.initial;

  Iterable<Weekday?> get weekdays sync* {
    final localizations = MaterialLocalizations.of(context);

    yield null;

    int days = 0;

    for (int i = localizations.firstDayOfWeekIndex;
        days < DateTime.daysPerWeek;
        i = (i + 1) % DateTime.daysPerWeek) {
      yield Weekday.fromIndex(i);
      days++;
    }
  }

  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context)!;
    final scheme = Theme.of(context).colorScheme;
    final locale = Localizations.localeOf(context).toLanguageTag();

    return AlertDialog(
      clipBehavior: Clip.antiAliasWithSaveLayer,
      contentPadding: const EdgeInsets.fromLTRB(0, 16, 0, 24),
      title: Text(strings.startOfTheWeek),
      content: Material(
        color: DialogTheme.of(context).backgroundColor,
        child: SizedBox(
          height: 56 * 6.75,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Divider(color: scheme.outline),
              Expanded(
                child: Material(
                  color: DialogTheme.of(context).backgroundColor,
                  child: SingleChildScrollView(
                    physics: const ClampingScrollPhysics(),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        for (var weekday in weekdays)
                          RadioListTile<Weekday?>(
                            tileColor: Colors.transparent,
                            value: weekday,
                            title: Text(
                                weekday?.format(locale) ?? strings.appLanguage),
                            groupValue: current,
                            onChanged: onChanged,
                          ),
                      ],
                    ),
                  ),
                ),
              ),
              Divider(color: scheme.outline),
            ],
          ),
        ),
      ),
      actions: [
        TextButton(
          onPressed: () => Navigator.of(context).pop(const None<Weekday>()),
          child: Text(strings.cancel),
        ),
        TextButton(
          onPressed: () => Navigator.of(context).pop(Some(current)),
          child: Text(strings.ok),
        ),
      ],
    );
  }

  void onChanged(Weekday? weekday) {
    setState(() => current = weekday);
  }
}

import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:material_symbols_icons/symbols.dart';

import '/strings/strings.dart';

typedef ActionBuilder<T> = Widget Function(
  BuildContext context,
  T value,
  void Function(T) valueChangedCallback,
);

class SelectionSettings<T> extends StatefulWidget {
  final List<T> initialSelected;
  final List<T> values;
  final String Function(T value) valueToString;
  final int Function(T value1, T value2) valueCompareTo;
  final bool selectionReorderable;
  final void Function(List<T> selected)? onSelectionChange;
  final ActionBuilder<T>? actionBuilder;
  final String? selectedTitle;
  final String? unselectedTitle;

  const SelectionSettings({
    required this.initialSelected,
    required this.values,
    required this.valueToString,
    required this.valueCompareTo,
    this.actionBuilder,
    this.selectionReorderable = false,
    this.onSelectionChange,
    this.selectedTitle,
    this.unselectedTitle,
    super.key,
  });

  @override
  State<SelectionSettings<T>> createState() => _SelectionSettingsState<T>();
}

class _SelectionSettingsState<T> extends State<SelectionSettings<T>> {
  late List<T> selected;
  late List<T> unselected;

  bool get allSelected => unselected.isEmpty;
  bool get nothingSelected => selected.isEmpty;

  @override
  void initState() {
    super.initState();
    setupLists();
    _sortListsRaw();
  }

  @override
  void didUpdateWidget(covariant SelectionSettings<T> oldWidget) {
    super.didUpdateWidget(oldWidget);

    setupLists();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context)!;
    final ThemeData(colorScheme: scheme, textTheme: text) = Theme.of(context);
    final labelStyle = text.labelLarge?.copyWith(
      color: scheme.onSurfaceVariant,
    );

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
          child: Text(
            widget.selectedTitle ?? strings.selected,
            style: labelStyle,
          ),
        ),
        buildCard(selected, selected: true),
        Padding(
          padding: const EdgeInsets.fromLTRB(8, 8, 8, 4),
          child: Text(
            widget.unselectedTitle ?? strings.unselected,
            style: labelStyle,
          ),
        ),
        buildCard(unselected, selected: false),
      ],
    );
  }

  void setupLists() {
    selected = widget.initialSelected.toList();
    unselected = widget.values
        .where(
            (e1) => !selected.any((e2) => widget.valueCompareTo(e1, e2) == 0))
        .toList();
  }

  void _sortListsRaw() {
    if (!widget.selectionReorderable) selected.sort(widget.valueCompareTo);
    unselected.sort(widget.valueCompareTo);
  }

  void sortLists() {
    _sortListsRaw();
    setState(() {});
    widget.onSelectionChange?.call(selected);
  }

  void reorderSelected(int oldIndex, int newIndex) {
    if (oldIndex == newIndex) return;

    setState(() {
      if (oldIndex < newIndex) newIndex--;

      final item = selected.removeAt(oldIndex);
      selected.insert(newIndex, item);
    });
    widget.onSelectionChange?.call(selected);
  }

  Widget buildCard(
    List<T> values, {
    required bool selected,
  }) {
    final strings = Strings.of(context)!;
    final ThemeData(colorScheme: scheme, textTheme: text) = Theme.of(context);

    final draggable =
        widget.selectionReorderable && selected && values.isNotEmpty;

    final options = <Widget>[
      if (values.isEmpty)
        Text(
          selected ? strings.nothingSelected : strings.allSelected,
          style: text.bodyLarge?.copyWith(
            color: scheme.onSurfaceVariant,
          ),
        )
      else
        for (final (index, value) in values.indexed)
          buildOption(
            value,
            index,
            selected: selected,
            draggable: draggable && values.length > 1,
          ),
    ];

    return Padding(
      padding: const EdgeInsets.all(4),
      child: Material(
        color: selected ? scheme.selectedCard : scheme.unselectedCard,
        borderRadius: BorderRadius.circular(12),
        clipBehavior: Clip.hardEdge,
        child: SizedBox(
          width: double.infinity,
          height: values.isEmpty ? 56 : null,
          child: Padding(
            padding:
                values.isNotEmpty ? EdgeInsets.zero : const EdgeInsets.all(16),
            child: AnimatedSize(
                duration: Durations.long3,
                curve: Curves.easeInOutCubicEmphasized,
                alignment: Alignment.topCenter,
                child: draggable
                    ? ReorderableListView(
                        physics: const NeverScrollableScrollPhysics(),
                        onReorder: reorderSelected,
                        buildDefaultDragHandles: false,
                        shrinkWrap: true,
                        onReorderStart: (_) => HapticFeedback.vibrate(),
                        proxyDecorator: (child, _, animation) =>
                            AnimatedBuilder(
                          animation: animation,
                          builder: (context, _) {
                            final time = Curves.easeInOutCubicEmphasized
                                .transform(animation.value);

                            final value = lerpDouble(1, 4, time)!;

                            final color = Color.lerp(
                              scheme.selectedCard,
                              ElevationOverlay.applySurfaceTint(
                                scheme.selectedCard,
                                scheme.surfaceTint,
                                value,
                              ),
                              lerpDouble(0, 1, time)!,
                            );

                            return Padding(
                              padding: EdgeInsets.symmetric(horizontal: value),
                              child: Material(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(12),
                                ),
                                color: color,
                                child: child,
                              ),
                            );
                          },
                        ),
                        children: options,
                      )
                    : Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: options,
                      )),
          ),
        ),
      ),
    );
  }

  Widget buildOption(
    T value,
    int index, {
    required bool selected,
    required bool draggable,
  }) {
    final actions = <Widget>[
      if (selected && widget.actionBuilder != null)
        widget.actionBuilder!(context, value, (updated) {
          this.selected[index] = updated;
          sortLists();
        }),
      if (draggable)
        ReorderableDragStartListener(
          index: index,
          child: const SizedBox(
            width: 24,
            height: 48,
            child: Icon(Symbols.drag_handle),
          ),
        )
    ];

    final secondary = actions.isNotEmpty
        ? Row(mainAxisSize: MainAxisSize.min, children: actions)
        : null;

    return CheckboxListTile(
      key: ValueKey(value.hashCode + index),
      controlAffinity: ListTileControlAffinity.leading,
      secondary: secondary,
      value: selected,
      visualDensity: VisualDensity.standard,
      onChanged: (_) {
        if (selected) {
          this.selected.remove(value);
          this.unselected.add(value);
        } else {
          this.unselected.remove(value);
          this.selected.add(value);
        }

        sortLists();
      },
      title: Text(widget.valueToString(value)),
    );
  }
}

extension on ColorScheme {
  Color get selectedCard => surfaceContainerHigh;
  Color get unselectedCard => surfaceContainer;
}

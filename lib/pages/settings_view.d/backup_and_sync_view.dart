import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '/strings/strings.dart';
import '/state/settings.dart';
import '/state/secure_storage.dart';
import '/widgets/back_button.dart' as back;

import 'backup_and_sync_view.d/import_export.dart';
import 'backup_and_sync_view.d/auto_export.dart';
import 'backup_and_sync_view.d/nextcloud.dart';
import 'backup_and_sync_view.d/webdav.dart';

class SettingsBackupAndSyncView extends ConsumerWidget {
  const SettingsBackupAndSyncView({this.showAppBar = true, super.key});

  final bool showAppBar;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final scheme = Theme.of(context).colorScheme;
    final text = Theme.of(context).textTheme;
    final strings = Strings.of(context)!;
    final settings = Settings.of(context);

    final titleStyle = text.titleMedium?.copyWith(color: scheme.onSurface);

    return PopScope(
      canPop: true,
      onPopInvoked: (_) => clearSyncs(settings, ref),
      child: Scaffold(
        appBar: showAppBar
            ? AppBar(
                title: Text(strings.syncAndBackup),
                leading: const back.BackButton(),
              )
            : null,
        body: ListView(
          physics: const ClampingScrollPhysics(),
          padding: EdgeInsets.only(
            bottom: MediaQuery.of(context).padding.bottom,
          ),
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.fromLTRB(12, 12, 12, 0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 12,
                      vertical: 8,
                    ),
                    child: Text(strings.synchronization, style: titleStyle),
                  ),
                  const NextcloudStorage(),
                  const WebDAVStorage(),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(12, 0, 12, 12),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 12,
                      vertical: 8,
                    ),
                    child: Text(strings.backup, style: titleStyle),
                  ),
                  const ImportExport(),
                  const AutoExport(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> clearSyncs(Settings settings, WidgetRef ref) async {
    if (!settings.backup.nextcloud.value) {
      await ref.read(secureStorageDbProvider).deleteNextcloud();
    }

    if (!settings.backup.webdav.value) {
      await ref.read(secureStorageDbProvider).deleteWebdav();
    }
  }
}

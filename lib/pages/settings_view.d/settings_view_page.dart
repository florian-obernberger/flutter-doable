// import 'package:flutter/material.dart';
// import 'package:go_router/go_router.dart';

// import '/strings/strings.dart';

// import 'general_view.dart';
// import 'design_view.dart';
// import 'sorting_view.dart';
// import 'licenses_view.dart';
// import 'backup_and_sync_view.dart';
// import 'about_view.dart';
// import 'contributor_view.dart';
// import 'extension_view.dart';

// enum SettingsPage {
//   general,
//   sortOrder,
//   design,
//   extensions,
//   backupAndSync,
//   about,
//   contributors,
//   licenses
// }

// class SettingsPageView extends StatelessWidget {
//   const SettingsPageView(this.page, {super.key});

//   final SettingsPage page;

//   @override
//   Widget build(BuildContext context) {
//     final strings = Strings.of(context)!;
//     final scheme = Theme.of(context).colorScheme;

//     return Scaffold(
//       appBar: AppBar(
//         title: Text(strings.general),
//         leading: IconButton(
//           icon: Icon(Icons.arrow_back, color: scheme.onSurface),
//           onPressed: context.pop,
//         ),
//       ),
//       body: getPage(),
//     );
//   }

//   Widget getPage() {
//     return switch(page) {
//       SettingsPage.general       => const SettingsGeneralView(),
//       SettingsPage.sortOrder     => const SettingsSortView(),
//       SettingsPage.design        => const SettingsDesignView(),
//       SettingsPage.extensions    => const SettingsExtensionView(),
//       SettingsPage.backupAndSync => const SettingsBackupAndSyncView(),
//       SettingsPage.about         => const SettingsAboutView(),
//       SettingsPage.contributors  => const SettingsContributorView(),
//       SettingsPage.licenses      => const LicensesView(),
//     };
//   }
// }

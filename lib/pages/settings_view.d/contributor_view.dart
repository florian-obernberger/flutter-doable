import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gap/gap.dart';
import 'package:material_symbols_icons/symbols.dart';
import 'package:result/result.dart';
import 'package:skeletonizer/skeletonizer.dart';

import '/strings/strings.dart';
import '/state/settings.dart';
import '/widgets/link.dart';
import '/classes/logger.dart';
import '/util/contributors_decode.dart';
import '/util/fetch.dart';
import '/util/launch.dart';
import '/util/extensions/iterable.dart';

class SettingsContributorView extends StatefulWidget {
  const SettingsContributorView({super.key});

  @override
  State<SettingsContributorView> createState() =>
      _SettingsContributorViewState();
}

class _SettingsContributorViewState extends State<SettingsContributorView>
    with SingleTickerProviderStateMixin {
  bool _loading = true;

  List<Contributor> contributors = List.generate(
    10,
    (i) => Contributor(
      name: BoneMock.fullName,
      link: BoneMock.words(2),
      roles: [if (i == 0) 'Owner' else BoneMock.words(i % 3 == 0 ? 3 : 2)],
    ),
  );

  @override
  void initState() {
    super.initState();
    loadContributors();
  }

  Future<void> loadContributors() async {
    const url = 'https://codeberg.org/florian-obernberger'
        '/flutter-doable/raw/branch/main/CONTRIBUTORS';

    contributors = await fetchCached(url)
        .timeout(const Duration(seconds: 24),
            onTimeout: () => const Err(NoInternetException(url)))
        .then<String>((res) async => res.inspectErr((err) {
              if (err is RequestException) {
                logger.e(
                  err,
                  stackTrace: err.stack,
                  message: "Couldn't fetch contributors",
                );
              }
            }).mapOrElse(
              (rp) => rp.text(utf8),
              (err) => rootBundle.loadString('CONTRIBUTORS'),
            ))
        .then(Contributor.fromFile);

    if (mounted) setState(() => _loading = false);
  }

  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context)!;
    final settings = Settings.of(context);
    final scheme = Theme.of(context).colorScheme;
    final text = Theme.of(context).textTheme;
    final textAccent = settings.design.textAccentFontTheme.textTheme;

    final roleBuilder = buildRoleBuilder(scheme, text);

    const shape = RoundedRectangleBorder(
      borderRadius: BorderRadius.all(Radius.circular(16)),
    );

    final header = <Widget>[
      Card.filled(
        color: scheme.primaryFixed,
        shape: shape,
        child: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 24,
            vertical: 20,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(
                strings.contributeWantYourNameHere,
                style: textAccent.titleLarge?.copyWith(
                  color: scheme.onPrimaryFixed,
                ),
              ),
              const Gap(4),
              Text(
                strings.contributeWantYourNameHereDescription,
                style: text.bodyLarge?.copyWith(
                  color: scheme.onPrimaryFixedVariant,
                ),
              ),
            ],
          ),
        ),
      ),
      const Gap(8)
    ];

    final groupedContributors = contributors
        .expand((contr) => contr.roles.map((role) => MapEntry(role, contr)))
        .collectGrouped()
        .map((k, v) => MapEntry(k, v..sort()))
        .entries
        .toList()
      ..sort((a, b) {
        if (a.key.isOwner) return -1;
        if (b.key.isOwner) return 1;
        return a.key.compareTo(b.key);
      });

    return Scaffold(
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () => contribute(settings),
        label: Text(strings.contribute),
        icon: const Icon(Symbols.handshake),
      ),
      body: ListView(
        physics: const ClampingScrollPhysics(),
        padding: const EdgeInsets.all(12).add(
          EdgeInsets.only(
            bottom: MediaQuery.of(context).padding.bottom + 72,
          ),
        ),
        children: <Widget>[
          ...header,
          Skeletonizer(
            enabled: _loading,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                ...groupedContributors
                    .expand((entry) => roleBuilder(entry.key, entry.value))
              ],
            ),
          ),
        ],
      ),
    );
  }

  Iterable<Widget> Function(String, List<Contributor>) buildRoleBuilder(
    ColorScheme scheme,
    TextTheme text,
  ) {
    final titleStyle = text.labelLarge!.copyWith(
      color: scheme.onSurfaceVariant,
    );
    final titleLabelStyle = text.labelMedium!.copyWith(
      color: scheme.onSurfaceVariant,
    );
    final baseStyle = text.bodyMedium!.copyWith(color: scheme.onSurfaceVariant);

    return (role, contributors) sync* {
      role = contributors.length == 1 ? role : '${role}s';

      yield Padding(
        padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 8),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              role,
              style: titleStyle.copyWith(
                color: role.isOwner ? scheme.secondary : null,
              ),
            ),
            if (role.isNotOwner)
              Text(contributors.length.toString(), style: titleLabelStyle),
          ],
        ),
      );

      for (final contributor in contributors) {
        yield Skeleton.leaf(
          child: Card(
            child: ListTile(
              trailing: Icon(Link.icon(contributor.link), size: 18),
              title: Text(
                contributor.name,
                style: text.titleMedium!.copyWith(color: scheme.onSurface),
              ),
              isThreeLine: false,
              subtitle: Link(url: contributor.link, baseStyle: baseStyle),
            ),
          ),
        );
      }
    };
  }

  void contribute(Settings settings) {
    launchUrl(
      'https://www.codeberg.org/florian-obernberger/flutter-doable#contribute',
      inAppBrowser: settings.general.inAppBrowser.value,
    );
  }
}

extension on String {
  bool get isOwner => this == 'Owner';
  bool get isNotOwner => !isOwner;
}

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:material_symbols_icons/symbols.dart';

import '/strings/strings.dart';
import '/state/settings.dart';
import '/data/sorting/todo_sorter.dart';
import '/resources/resources.dart';
import '/widgets/svg_icon.dart';
import '/widgets/selectable_tab.dart';
import '/util/all_but_edge_insets.dart';
import '/util/extensions/strings.dart';

import 'selection_settings.dart';
import 'info_card.dart';

class SettingsSortView extends StatefulWidget {
  const SettingsSortView({this.showAppBar = true, super.key});

  final bool showAppBar;

  @override
  State<SettingsSortView> createState() => _SettingsSortViewState();
}

class _SettingsSortViewState extends State<SettingsSortView>
    with SingleTickerProviderStateMixin {
  late final tabController = TabController(vsync: this, length: 2);

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context)!;
    final sorting = Settings.of(context).sorting;

    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 0,
        bottom: TabBar(
          controller: tabController,
          tabs: <Widget>[
            SelectableTab(
              controller: tabController,
              index: 0,
              text: strings.groupsLabel,
              icon: const Icon(Symbols.note_stack),
              activeIcon: const Icon(Symbols.note_stack, fill: 1),
            ),
            SelectableTab(
              controller: tabController,
              index: 1,
              text: strings.sortingLabel,
              icon: const Icon(Symbols.swap_vert),
              activeIcon: const Icon(Symbols.swap_vert, fill: 1),
            ),
          ],
        ),
      ),
      body: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          ValueListenableBuilder(
            valueListenable: sorting.hideHelpInformation,
            builder: (context, hide, widget) => Visibility(
              visible: !hide,
              child: widget!,
            ),
            child: Padding(
              padding: const AllButEdgeInsets(12, bottom: 0),
              child: InfoCard(
                title: strings.sortingHelp,
                body: strings.sortingHelpDescription(
                  strings.quote(strings.groupsLabel),
                  strings.quote(strings.sortingLabel),
                ),
                onHide: () => sorting.hideHelpInformation.value = true,
              ),
            ),
          ),
          Padding(
            padding: const AllButEdgeInsets(12, bottom: 4),
            child: Card.outlined(
              clipBehavior: Clip.antiAlias,
              child: ValueListenableBuilder(
                valueListenable: sorting.sortCompleted,
                builder: (context, sortCompleted, _) => SwitchListTile(
                  contentPadding: const EdgeInsetsDirectional.only(
                    start: 16,
                    end: 12,
                  ),
                  value: sortCompleted,
                  onChanged: (value) => sorting.sortCompleted.value = value,
                  title: Text(strings.sortCompleted),
                  subtitle: Text(strings.sortCompletedDescription),
                ),
              ),
            ),
          ),
          Expanded(
            child: TabBarView(
              controller: tabController,
              children: <Widget>[
                SingleChildScrollView(
                  padding: const EdgeInsets.all(12),
                  physics: const ClampingScrollPhysics(),
                  child: SelectionSettings<SortingGroup>(
                    initialSelected: sorting.groupBy.value,
                    values: SortingGroup.values,
                    onSelectionChange: (selection) =>
                        sorting.groupBy.value = selection,
                    selectionReorderable: true,
                    valueCompareTo: Enum.compareByIndex,
                    valueToString: (value) => switch (value) {
                      SortingGroup.starredOverdue => strings.sortStarredOverdue,
                      SortingGroup.starredToday => strings.sortStarredToday,
                      SortingGroup.starredDate =>
                        strings.sortStarredRelevantDate,
                      SortingGroup.starredNoDate => strings.sortStarred,
                      SortingGroup.overdue => strings.sortOverdue,
                      SortingGroup.today => strings.sortToday,
                      SortingGroup.date => strings.sortRelevantDate,
                      SortingGroup.noDate => strings.sortNoRelevantDate,
                    },
                  ),
                ),
                SingleChildScrollView(
                  padding: const EdgeInsets.all(12),
                  physics: const ClampingScrollPhysics(),
                  child: SelectionSettings<SortingMetric>(
                    initialSelected: sorting.sortBy.value,
                    values: SortingMetric.values,
                    onSelectionChange: (selection) =>
                        sorting.sortBy.value = selection,
                    actionBuilder: (context, metric, onChanged) => IconButton(
                      tooltip: switch (metric.order) {
                        SortOrder.ascending => strings.sortAscending,
                        SortOrder.descending => strings.sortDescending
                      },
                      onPressed: () {
                        HapticFeedback.selectionClick();
                        onChanged(metric.reversedOrder);
                      },
                      icon: SvgIcon(
                        icon: switch (metric.order) {
                          SortOrder.ascending => SvgIcons.sortAscendingW400,
                          SortOrder.descending => SvgIcons.sortDescendingW400
                        },
                      ),
                    ),
                    selectionReorderable: true,
                    valueCompareTo: (m1, m2) => Enum.compareByIndex(
                      m1.attribute,
                      m2.attribute,
                    ),
                    valueToString: (value) => switch (value.attribute) {
                      SortingAttr.relevantDate => strings.sortByRelevantDate,
                      SortingAttr.lastEdited => strings.sortByLastEdited,
                      SortingAttr.titleName => strings.sortByTitle,
                      SortingAttr.creationDate => strings.sortByCreationDate,
                    },
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

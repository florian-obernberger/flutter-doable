import 'dart:io';

import 'package:flutter/material.dart';
import 'package:result/result.dart';

import '/strings/strings.dart';
import '/types/app_language.dart';
import '/types/date_and_time_formats.dart';
import '/types/weekday.dart';
import '/util/formatters/format_duration.dart';
import '/util/animated_dialog.dart';
import '/state/settings.dart';
import '/widgets/back_button.dart' as back;

import 'general_view.d/language_dialog.dart';
import 'general_view.d/delete_completed_dialog.dart';
import 'general_view.d/date_and_time_dialog.dart';
import 'general_view.d/start_of_the_week_picker_dialog.dart';

import 'item.dart';

class SettingsGeneralView extends StatefulWidget {
  const SettingsGeneralView({this.showAppBar = true, super.key});

  final bool showAppBar;

  @override
  State<SettingsGeneralView> createState() => _SettingsGeneralViewState();
}

class _SettingsGeneralViewState extends State<SettingsGeneralView> {
  Settings? _settings;
  Settings get settings => _settings!;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    _settings ??= Settings.of(context);
  }

  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context)!;

    final appLanguage = SettingsItem(
      title: strings.appLanguage,
      description: settings.general.appLanguage.value.localizedFor(context),
      onTap: () {
        showAnimatedDialog<AppLanguage>(
          context: context,
          animationType: DialogTransitionType.fadeScale,
          builder: (context) => const LanguageDialog(),
        ).then((value) {
          if (value != null) {
            settings.general.appLanguage.value = value;
            setState(() {});
          }
        });
      },
      mode: SettingsItemMode.popup,
    );

    final dateFormat = SettingsItem(
      title: strings.dateFormat,
      description: settings.dateTime.dateFormat.value.format(exampleDateTime),
      onTap: () {
        showAnimatedDialog<DateFormat>(
          context: context,
          animationType: DialogTransitionType.fadeScale,
          builder: (context) => const DateFormatDialog(),
        ).then((value) {
          if (value != null) {
            settings.dateTime.dateFormat.value = value;
            setState(() {});
          }
        });
      },
      mode: SettingsItemMode.popup,
    );

    final timeFormat = SettingsItem(
      title: strings.timeFormat,
      description:
          TimeFormatDialog.getExample(settings.dateTime.timeFormat.value),
      onTap: () {
        showAnimatedDialog<TimeFormat>(
          context: context,
          animationType: DialogTransitionType.fadeScale,
          builder: (context) => const TimeFormatDialog(),
        ).then((value) {
          if (value != null) {
            settings.dateTime.timeFormat.value = value;
            setState(() {});
          }
        });
      },
      mode: SettingsItemMode.popup,
    );

    final locale = Localizations.localeOf(context).toLanguageTag();

    final langSow = Weekday.fromIndex(
      MaterialLocalizations.of(context).firstDayOfWeekIndex,
    ).format(locale);

    final sowFallback = Directionality.of(context) == TextDirection.ltr
        ? '$langSow (${strings.appLanguage})'
        : '(${strings.appLanguage}) $langSow';

    final startOfTheWeek = ValueListenableBuilder(
        valueListenable: settings.general.startOfTheWeek,
        builder: (context, weekday, _) {
          return SettingsItem(
            title: strings.startOfTheWeek,
            description: weekday?.format(locale) ?? sowFallback,
            onTap: () => showAnimatedDialog<Option<Weekday?>>(
              context: context,
              animationType: DialogTransitionType.fadeScale,
              builder: (context) => StartOfTheWeekPickerDialog(weekday),
            ).then((value) {
              if (value != null && value.isSome()) {
                settings.general.startOfTheWeek.value = value.unwrap();
              }
            }),
            mode: SettingsItemMode.popup,
          );
        });

    final androidAutoUpdates = SettingsItem(
      title: strings.updateChecker,
      description: strings.autoUpdatesDescription,
      onSwitchChanged: (isSelected) {
        settings.updates.autoUpdates.value = isSelected;
        if (settings.updates.autoUpdates.value) {
          settings.updates.updateSkip.value = null;
        }
        setState(() {});
      },
      toggleValue: settings.updates.autoUpdates.value,
      mode: SettingsItemMode.toggle,
    );

    final androidCustomTabs = SettingsItem(
      title: strings.useCustomTabs,
      description: strings.useCustomTabsDescription,
      onSwitchChanged: (isSelected) {
        settings.general.inAppBrowser.value = isSelected;
        setState(() {});
      },
      toggleValue: settings.general.inAppBrowser.value,
      mode: SettingsItemMode.toggle,
    );

    final androidSharedTextAsTitle = SettingsItem(
      title: strings.sharedTextAsTitle,
      description: strings.sharedTextAsTitleDescription,
      onSwitchChanged: (isSelected) {
        settings.general.sharedTextAsTitle.value = isSelected;
        setState(() {});
      },
      toggleValue: settings.general.sharedTextAsTitle.value,
      mode: SettingsItemMode.toggle,
    );

    final deleteAfterCompleted = SettingsItem(
      title: strings.deleteCompletedAfter,
      description: deleteCompletedAfterDescription(strings),
      onTap: () {
        showAnimatedDialog<Duration>(
          context: context,
          animationType: DialogTransitionType.fadeScale,
          builder: (context) => const DeleteAfterDialog(),
        ).then((value) {
          if (value != null) {
            settings.general.deleteCompletedAfter.value = value;
            setState(() {});
          }
        });
      },
      mode: SettingsItemMode.popup,
    );

    final alignTodoCheck = SettingsItem(
      title: strings.alignTodoCheck,
      description: strings.alignTodoCheckDescription,
      onSwitchChanged: (isSelected) {
        settings.general.checkMarkRight.value = isSelected;
        setState(() {});
      },
      toggleValue: settings.general.checkMarkRight.value,
      mode: SettingsItemMode.toggle,
    );

    final highlightToday = SettingsItem(
      title: strings.highlightToday,
      description: strings.highlightTodayDescription,
      onSwitchChanged: (isSelected) {
        settings.general.highlightToday.value = isSelected;
        setState(() {});
      },
      toggleValue: settings.general.highlightToday.value,
      mode: SettingsItemMode.toggle,
    );

    final highlightOverdue = SettingsItem(
      title: strings.highlightOverdue,
      description: strings.highlightOverdueDescription,
      onSwitchChanged: (isSelected) {
        settings.general.highlightOverdue.value = isSelected;
        setState(() {});
      },
      toggleValue: settings.general.highlightOverdue.value,
      mode: SettingsItemMode.toggle,
    );

    final trimTodoTexts = SettingsItem(
      title: strings.trimTodoTexts,
      description: strings.trimTodoTextsDescription,
      onSwitchChanged: (isSelected) {
        settings.general.trimTodoTexts.value = isSelected;
        setState(() {});
      },
      toggleValue: settings.general.trimTodoTexts.value,
      mode: SettingsItemMode.toggle,
    );

    final closeOnComplete = SettingsItem(
      title: strings.closeDetailsOnCompelte,
      description: strings.closeDetailsOnCompelteDescription,
      onSwitchChanged: (isSelected) {
        settings.general.closeDetailsOnComplete.value = isSelected;
        setState(() {});
      },
      toggleValue: settings.general.closeDetailsOnComplete.value,
      mode: SettingsItemMode.toggle,
    );

    final expandTodosByDefault = SettingsItem(
      title: strings.expandTodosByDefault,
      description: strings.expandTodosByDefaultDescription,
      onSwitchChanged: (isSelected) {
        settings.general.expandTodosByDefault.value = isSelected;
        setState(() {});
      },
      toggleValue: settings.general.expandTodosByDefault.value,
      mode: SettingsItemMode.toggle,
    );

    return Scaffold(
      appBar: widget.showAppBar
          ? AppBar(
              title: Text(strings.general),
              leading: const back.BackButton(),
            )
          : null,
      body: ListView(
        physics: const ClampingScrollPhysics(),
        padding: EdgeInsets.only(
          bottom: MediaQuery.of(context).padding.bottom + 8,
        ),
        children: <Widget>[
          appLanguage,
          dateFormat,
          timeFormat,
          startOfTheWeek,
          if (Platform.isAndroid) ...[
            const Divider(),
            androidAutoUpdates,
            androidCustomTabs,
            androidSharedTextAsTitle,
          ],
          const Divider(),
          deleteAfterCompleted,
          closeOnComplete,
          expandTodosByDefault,
          trimTodoTexts,
          const Divider(),
          highlightToday,
          highlightOverdue,
          alignTodoCheck,
        ],
      ),
    );
  }

  String deleteCompletedAfterDescription(Strings strings) {
    final after = settings.general.deleteCompletedAfter;

    final current = after.value;

    if (current == null || current.isNegative) {
      return strings.never;
    }

    return strings.deleteCompletedAfterDescription(
      formatDuration(context, after.value!),
    );
  }
}

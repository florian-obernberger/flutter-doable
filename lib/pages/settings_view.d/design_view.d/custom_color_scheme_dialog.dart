import 'dart:convert';
import 'dart:io';

import 'package:flex_color_picker/flex_color_picker.dart';
import 'package:flex_seed_scheme/flex_seed_scheme.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gap/gap.dart';
import 'package:material_symbols_icons/symbols.dart';
import 'package:pick_or_save/pick_or_save.dart';
import 'package:result/result.dart';

import '/strings/strings.dart';
import '/classes/logger.dart';
import '/state/settings.dart';
import '/theme/color_schemes.dart';
import '/util/extensions/color_scheme.dart';
import '/util/animated_dialog.dart';
import '/widgets/save_button.dart';
import '/widgets/delete_dialog.dart';
import '/widgets/nav_button.dart';
import '/widgets/selectable_tab.dart';

import '../../todo_view.d/snackbar_with_undo.dart';
import '../item.dart';
import '../info_card.dart';

class CustomColorSchemeDialog extends StatefulWidget {
  const CustomColorSchemeDialog({super.key});

  @override
  State<CustomColorSchemeDialog> createState() =>
      _CustomColorSchemeDialogState();
}

class _CustomColorSchemeDialogState extends State<CustomColorSchemeDialog>
    with SingleTickerProviderStateMixin {
  late ColorScheme lightScheme;
  late ColorScheme darkScheme;

  late final bool isDark;
  late TabController _controller;

  Settings? _settings;
  Settings get settings => _settings!;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    if (_settings == null) {
      _settings = Settings.of(context);

      isDark = switch (settings.design.themeMode.value.themeMode) {
        ThemeMode.dark => true,
        ThemeMode.light => false,
        ThemeMode.system =>
          PlatformDispatcher.instance.platformBrightness == Brightness.dark,
      };

      _controller = TabController(
        length: 2,
        vsync: this,
        initialIndex: isDark ? 1 : 0,
      );

      initSchemes();
    }
  }

  void initSchemes({bool reset = false}) {
    final customColorSchemes = settings.design.customColorSchemes.value;
    final customAccent = settings.design.customAccentColor.value;

    if (customColorSchemes != null && !reset) {
      lightScheme = customColorSchemes.lightScheme;
      darkScheme = customColorSchemes.darkScheme;
    } else if (customAccent != null) {
      lightScheme = SeedColorScheme.fromSeeds(
        primaryKey: customAccent,
        secondaryKey: settings.design.customSecondaryAccentColor.value,
        tertiaryKey: settings.design.customTertiaryAccentColor.value,
        brightness: Brightness.light,
        tones: FlexTones.material(Brightness.light),
      );
      darkScheme = SeedColorScheme.fromSeeds(
        primaryKey: customAccent,
        secondaryKey: settings.design.customSecondaryAccentColor.value,
        tertiaryKey: settings.design.customTertiaryAccentColor.value,
        brightness: Brightness.dark,
        tones: FlexTones.material(Brightness.dark),
      );
    } else {
      lightScheme = lightColorScheme;
      darkScheme = darkColorScheme;
    }
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context)!;

    return PopScope(
      canPop: false,
      onPopInvoked: (didPop) async {
        if (didPop) return;

        final close = await showDeleteDialog(
              context: context,
              description: strings.discardCustomColorTheme,
            ) ??
            false;

        if (close && context.mounted) Navigator.of(context).pop();
      },
      child: Theme(
        data: Theme.of(context).copyWith(
          colorScheme: isDark ? darkScheme : lightScheme,
        ),
        child: Builder(builder: (context) {
          final backgroundColor =
              Theme.of(context).colorScheme.surfaceContainerHigh;

          final overlayStyle =
              (isDark ? SystemUiOverlayStyle.light : SystemUiOverlayStyle.dark)
                  .copyWith(systemNavigationBarColor: Colors.transparent);

          final footerButtons = <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 4),
              child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                physics: const ClampingScrollPhysics(),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    const Gap(8),
                    OutlinedButton.icon(
                      style: OutlinedButton.styleFrom(
                          minimumSize: const Size(8, 40)),
                      onPressed: import,
                      label: Text(strings.import),
                      icon: const Icon(Symbols.upload),
                    ),
                    const Gap(8),
                    OutlinedButton.icon(
                      style: OutlinedButton.styleFrom(
                          minimumSize: const Size(8, 40)),
                      onPressed: export,
                      label: Text(strings.export),
                      icon: const Icon(Symbols.download),
                    ),
                    const Gap(8),
                    OutlinedButton.icon(
                      style: OutlinedButton.styleFrom(
                          minimumSize: const Size(8, 40)),
                      onPressed: reset,
                      label: Text(strings.reset),
                      icon: const Icon(Symbols.delete, weight: 500),
                    ),
                    const Gap(8),
                  ],
                ),
              ),
            )
          ];

          return Dialog.fullscreen(
              backgroundColor: backgroundColor,
              child: Scaffold(
                backgroundColor: backgroundColor,
                persistentFooterButtons: footerButtons,
                appBar: AppBar(
                  notificationPredicate: (_) => false,
                  systemOverlayStyle: overlayStyle,
                  scrolledUnderElevation: 5,
                  bottom: TabBar(
                    controller: _controller,
                    tabs: <Tab>[
                      SelectableTab(
                        controller: _controller,
                        index: 0,
                        icon: const Icon(Symbols.light_mode),
                        activeIcon: const Icon(Symbols.light_mode, fill: 1),
                        child: Text(strings.lightMode),
                      ),
                      SelectableTab(
                        controller: _controller,
                        index: 1,
                        icon: const Icon(Symbols.dark_mode),
                        activeIcon: const Icon(Symbols.dark_mode, fill: 1),
                        child: Text(strings.darkMode),
                      ),
                    ],
                  ),
                  leading: NavButton.close(
                    onNav: () async {
                      final shouldClose = await showDeleteDialog(
                        context: context,
                        description: strings.discardCustomColorTheme,
                      );

                      if (shouldClose == true && context.mounted) {
                        Navigator.of(context).pop();
                      }
                    },
                  ),
                  title: Text(strings.customColorTheme),
                  actions: <Widget>[
                    Padding(
                      padding: const EdgeInsetsDirectional.only(end: 16),
                      child: SaveButton(onSave: save, isFilled: false),
                    )
                  ],
                  backgroundColor: backgroundColor,
                ),
                body: ValueListenableBuilder(
                  valueListenable: settings.design.hideCustomColorSchemeHint,
                  builder: (context, hide, _) {
                    final padding = hide
                        ? null
                        // info-tile height = 64 + 2 * 12pd padding
                        : const EdgeInsets.only(top: 64 + 24);

                    return Stack(
                      children: <Widget>[
                        TabBarView(
                          controller: _controller,
                          children: <Widget>[
                            SingleChildScrollView(
                              padding: padding,
                              physics: const ClampingScrollPhysics(),
                              child: Column(
                                children: lightSchemeButtons(),
                              ),
                            ),
                            SingleChildScrollView(
                              padding: padding,
                              physics: const ClampingScrollPhysics(),
                              child: Column(children: darkSchemeButtons()),
                            ),
                          ],
                        ),
                        Visibility(
                          visible: !hide,
                          child: Align(
                            alignment: Alignment.topCenter,
                            child: Padding(
                              padding: const EdgeInsets.all(12),
                              child: InfoCard(
                                title: strings.info,
                                body: strings.customThemeInfo,
                                backgroundColor: backgroundColor,
                                margin: EdgeInsets.zero,
                                supportMarkdown: true,
                                onHide: () => settings.design
                                    .hideCustomColorSchemeHint.value = true,
                              ),
                            ),
                          ),
                        ),
                      ],
                    );
                  },
                ),
              ));
        }),
      ),
    );
  }

  void import() async {
    final strings = Strings.of(context)!;
    final messanger = ScaffoldMessenger.of(context);

    final errSnackBar = SnackBar(
      duration: const Duration(seconds: 4),
      dismissDirection: DismissDirection.vertical,
      margin: const EdgeInsets.fromLTRB(24, 8, 24, 18),
      content: Text(strings.customThemeFuckedFile),
    );

    final file = await PickOrSave().filePicker(
      params: FilePickerParams(
        mimeTypesFilter: ['application/json'],
        allowedExtensions: ['json'],
        enableMultipleSelection: false,
        getCachedFilePath: true,
      ),
    );

    if (file == null || file.isEmpty) return;
    final dynamic json;

    try {
      json = jsonDecode(await File(file.first).readAsString());
    } catch (err, stack) {
      logger.log(LogLevel.debug, error: err, stackTrace: stack, tag: 'Theme');
      return;
    }

    try {
      final {'lightScheme': lightScheme, 'darkScheme': darkScheme} = json;

      final schemes = ColorSchemeJson.fromJson(lightScheme)
          .ok()
          .zip(ColorSchemeJson.fromJson(darkScheme).ok());

      switch (schemes) {
        case Some():
          setState(() {
            this.lightScheme = schemes.value.$1;
            this.darkScheme = schemes.value.$2;
          });
        case None():
          logger.i('File did not match expected structure', tag: 'Theme');
          messanger.showSnackBar(errSnackBar);
      }
    } on StateError catch (e) {
      logger.e(
        e,
        message: 'File did not match expected structure',
        tag: 'Theme',
      );
      messanger.showSnackBar(errSnackBar);
    }
  }

  void export() async {
    final json = jsonEncode({
      'lightScheme': lightScheme.exportToJson(),
      'darkScheme': darkScheme.exportToJson(),
    });

    await PickOrSave().fileSaver(
        params: FileSaverParams(
      mimeTypesFilter: ['application/json'],
      saveFiles: [
        SaveFileInfo(
          fileData: Uint8List.fromList(utf8.encode(json)),
          fileName: 'doable_color_theme.json',
        )
      ],
    ));
  }

  void save() {
    settings.design.customColorSchemes.value = (
      lightScheme: lightScheme,
      darkScheme: darkScheme,
    );

    if (mounted) Navigator.of(context).pop();
  }

  void reset() async {
    final strings = Strings.of(context)!;
    final schemes = (lightScheme: lightScheme, darkScheme: darkScheme);

    setState(() => initSchemes(reset: true));

    showSnackBarWithUndo(
      context: context,
      duration: const Duration(seconds: 4),
      onUndo: () => setState(() {
        lightScheme = schemes.lightScheme;
        darkScheme = schemes.darkScheme;
      }),
      content: Text(strings.deletedCustomColorTheme),
    );
  }

  List<Widget> lightSchemeButtons() => [
        _ColorPickerTile(
          key: ValueKey('primary@${lightScheme.primary}'),
          color: lightScheme.primary,
          onChanged: (color) => setState(
            () => lightScheme = lightScheme.copyWith(
              primary: color,
            ),
          ),
          title: 'Primary',
        ),
        _ColorPickerTile(
          key: ValueKey('onPrimary@${lightScheme.onPrimary}'),
          color: lightScheme.onPrimary,
          onChanged: (color) => setState(
            () => lightScheme = lightScheme.copyWith(
              onPrimary: color,
            ),
          ),
          title: 'On primary',
        ),
        _ColorPickerTile(
          key: ValueKey('primaryContainer@${lightScheme.primaryContainer}'),
          color: lightScheme.primaryContainer,
          onChanged: (color) => setState(
            () => lightScheme = lightScheme.copyWith(
              primaryContainer: color,
            ),
          ),
          title: 'Primary container',
        ),
        _ColorPickerTile(
          key: ValueKey('onPrimaryContainer@${lightScheme.onPrimaryContainer}'),
          color: lightScheme.onPrimaryContainer,
          onChanged: (color) => setState(
            () => lightScheme = lightScheme.copyWith(
              onPrimaryContainer: color,
            ),
          ),
          title: 'On primary container',
        ),
        _ColorPickerTile(
          key: ValueKey('secondary@${lightScheme.secondary}'),
          color: lightScheme.secondary,
          onChanged: (color) => setState(
            () => lightScheme = lightScheme.copyWith(
              secondary: color,
            ),
          ),
          title: 'Secondary',
        ),
        _ColorPickerTile(
          key: ValueKey('onSecondary@${lightScheme.onSecondary}'),
          color: lightScheme.onSecondary,
          onChanged: (color) => setState(
            () => lightScheme = lightScheme.copyWith(
              onSecondary: color,
            ),
          ),
          title: 'On secondary',
        ),
        _ColorPickerTile(
          key: ValueKey('secondaryContainer@${lightScheme.secondaryContainer}'),
          color: lightScheme.secondaryContainer,
          onChanged: (color) => setState(
            () => lightScheme = lightScheme.copyWith(
              secondaryContainer: color,
            ),
          ),
          title: 'Secondary container',
        ),
        _ColorPickerTile(
          key: ValueKey(
              'onSecondaryContainer@${lightScheme.onSecondaryContainer}'),
          color: lightScheme.onSecondaryContainer,
          onChanged: (color) => setState(
            () => lightScheme = lightScheme.copyWith(
              onSecondaryContainer: color,
            ),
          ),
          title: 'On secondary container',
        ),
        _ColorPickerTile(
          key: ValueKey('tertiary@${lightScheme.tertiary}'),
          color: lightScheme.tertiary,
          onChanged: (color) => setState(
            () => lightScheme = lightScheme.copyWith(
              tertiary: color,
            ),
          ),
          title: 'Tertiary',
        ),
        _ColorPickerTile(
          key: ValueKey('onTertiary@${lightScheme.onTertiary}'),
          color: lightScheme.onTertiary,
          onChanged: (color) => setState(
            () => lightScheme = lightScheme.copyWith(
              onTertiary: color,
            ),
          ),
          title: 'On tertiary',
        ),
        _ColorPickerTile(
          key: ValueKey('tertiaryContainer@${lightScheme.tertiaryContainer}'),
          color: lightScheme.tertiaryContainer,
          onChanged: (color) => setState(
            () => lightScheme = lightScheme.copyWith(
              tertiaryContainer: color,
            ),
          ),
          title: 'Tertiary container',
        ),
        _ColorPickerTile(
          key: ValueKey(
              'onTertiaryContainer@${lightScheme.onTertiaryContainer}'),
          color: lightScheme.onTertiaryContainer,
          onChanged: (color) => setState(
            () => lightScheme = lightScheme.copyWith(
              onTertiaryContainer: color,
            ),
          ),
          title: 'On tertiary container',
        ),
        _ColorPickerTile(
          key: ValueKey('error@${lightScheme.error}'),
          color: lightScheme.error,
          onChanged: (color) => setState(
            () => lightScheme = lightScheme.copyWith(
              error: color,
            ),
          ),
          title: 'Error',
        ),
        _ColorPickerTile(
          key: ValueKey('errorContainer@${lightScheme.errorContainer}'),
          color: lightScheme.errorContainer,
          onChanged: (color) => setState(
            () => lightScheme = lightScheme.copyWith(
              errorContainer: color,
            ),
          ),
          title: 'Error container',
        ),
        _ColorPickerTile(
          key: ValueKey('onError@${lightScheme.onError}'),
          color: lightScheme.onError,
          onChanged: (color) => setState(
            () => lightScheme = lightScheme.copyWith(
              onError: color,
            ),
          ),
          title: 'On error',
        ),
        _ColorPickerTile(
          key: ValueKey('onErrorContainer@${lightScheme.onErrorContainer}'),
          color: lightScheme.onErrorContainer,
          onChanged: (color) => setState(
            () => lightScheme = lightScheme.copyWith(
              onErrorContainer: color,
            ),
          ),
          title: 'On error container',
        ),
        _ColorPickerTile(
          key: ValueKey('background@${lightScheme.surface}'),
          color: lightScheme.surface,
          onChanged: (color) => setState(
            () => lightScheme = lightScheme.copyWith(
              surface: color,
            ),
          ),
          title: 'Background',
        ),
        _ColorPickerTile(
          key: ValueKey('onBackground@${lightScheme.onSurface}'),
          color: lightScheme.onSurface,
          onChanged: (color) => setState(
            () => lightScheme = lightScheme.copyWith(
              onSurface: color,
            ),
          ),
          title: 'On background',
        ),
        _ColorPickerTile(
          key: ValueKey('surface@${lightScheme.surface}'),
          color: lightScheme.surface,
          onChanged: (color) => setState(
            () => lightScheme = lightScheme.copyWith(
              surface: color,
            ),
          ),
          title: 'Surface',
        ),
        _ColorPickerTile(
          key: ValueKey('onSurface@${lightScheme.onSurface}'),
          color: lightScheme.onSurface,
          onChanged: (color) => setState(
            () => lightScheme = lightScheme.copyWith(
              onSurface: color,
            ),
          ),
          title: 'On surface',
        ),
        _ColorPickerTile(
          key:
              ValueKey('surfaceVariant@${lightScheme.surfaceContainerHighest}'),
          color: lightScheme.surfaceContainerHighest,
          onChanged: (color) => setState(
            () => lightScheme = lightScheme.copyWith(
              surfaceContainerHighest: color,
            ),
          ),
          title: 'Surface variant',
        ),
        _ColorPickerTile(
          key: ValueKey('onSurfaceVariant@${lightScheme.onSurfaceVariant}'),
          color: lightScheme.onSurfaceVariant,
          onChanged: (color) => setState(
            () => lightScheme = lightScheme.copyWith(
              onSurfaceVariant: color,
            ),
          ),
          title: 'On surface variant',
        ),
        _ColorPickerTile(
          key: ValueKey('outline@${lightScheme.outline}'),
          color: lightScheme.outline,
          onChanged: (color) => setState(
            () => lightScheme = lightScheme.copyWith(
              outline: color,
            ),
          ),
          title: 'Outline',
        ),
        _ColorPickerTile(
          key: ValueKey('onInverseSurface@${lightScheme.onInverseSurface}'),
          color: lightScheme.onInverseSurface,
          onChanged: (color) => setState(
            () => lightScheme = lightScheme.copyWith(
              onInverseSurface: color,
            ),
          ),
          title: 'On inverse surface',
        ),
        _ColorPickerTile(
          key: ValueKey('inverseSurface@${lightScheme.inverseSurface}'),
          color: lightScheme.inverseSurface,
          onChanged: (color) => setState(
            () => lightScheme = lightScheme.copyWith(
              inverseSurface: color,
            ),
          ),
          title: 'Inverse surface',
        ),
        _ColorPickerTile(
          key: ValueKey('inversePrimary@${lightScheme.inversePrimary}'),
          color: lightScheme.inversePrimary,
          onChanged: (color) => setState(
            () => lightScheme = lightScheme.copyWith(
              inversePrimary: color,
            ),
          ),
          title: 'Inverse primary',
        ),
        _ColorPickerTile(
          key: ValueKey('shadow@${lightScheme.shadow}'),
          color: lightScheme.shadow,
          onChanged: (color) => setState(
            () => lightScheme = lightScheme.copyWith(
              shadow: color,
            ),
          ),
          title: 'Shadow',
        ),
        _ColorPickerTile(
          key: ValueKey('surfaceTint@${lightScheme.surfaceTint}'),
          color: lightScheme.surfaceTint,
          onChanged: (color) => setState(
            () => lightScheme = lightScheme.copyWith(
              surfaceTint: color,
            ),
          ),
          title: 'Surface tint',
        ),
        _ColorPickerTile(
          key: ValueKey('outlineVariant@${lightScheme.outlineVariant}'),
          color: lightScheme.outlineVariant,
          onChanged: (color) => setState(
            () => lightScheme = lightScheme.copyWith(
              outlineVariant: color,
            ),
          ),
          title: 'Outline variant',
        ),
        _ColorPickerTile(
          key: ValueKey('scrim@${lightScheme.scrim}'),
          color: lightScheme.scrim,
          onChanged: (color) => setState(
            () => lightScheme = lightScheme.copyWith(
              scrim: color,
            ),
          ),
          title: 'Scrim',
        ),
      ];

  List<Widget> darkSchemeButtons() => [
        _ColorPickerTile(
          key: ValueKey('primary@${darkScheme.primary}'),
          color: darkScheme.primary,
          onChanged: (color) => setState(
            () => darkScheme = darkScheme.copyWith(
              primary: color,
            ),
          ),
          title: 'Primary',
        ),
        _ColorPickerTile(
          key: ValueKey('onPrimary@${darkScheme.onPrimary}'),
          color: darkScheme.onPrimary,
          onChanged: (color) => setState(
            () => darkScheme = darkScheme.copyWith(
              onPrimary: color,
            ),
          ),
          title: 'On primary',
        ),
        _ColorPickerTile(
          key: ValueKey('primaryContainer@${darkScheme.primaryContainer}'),
          color: darkScheme.primaryContainer,
          onChanged: (color) => setState(
            () => darkScheme = darkScheme.copyWith(
              primaryContainer: color,
            ),
          ),
          title: 'Primary container',
        ),
        _ColorPickerTile(
          key: ValueKey('onPrimaryContainer@${darkScheme.onPrimaryContainer}'),
          color: darkScheme.onPrimaryContainer,
          onChanged: (color) => setState(
            () => darkScheme = darkScheme.copyWith(
              onPrimaryContainer: color,
            ),
          ),
          title: 'On primary container',
        ),
        _ColorPickerTile(
          key: ValueKey('secondary@${darkScheme.secondary}'),
          color: darkScheme.secondary,
          onChanged: (color) => setState(
            () => darkScheme = darkScheme.copyWith(
              secondary: color,
            ),
          ),
          title: 'Secondary',
        ),
        _ColorPickerTile(
          key: ValueKey('onSecondary@${darkScheme.onSecondary}'),
          color: darkScheme.onSecondary,
          onChanged: (color) => setState(
            () => darkScheme = darkScheme.copyWith(
              onSecondary: color,
            ),
          ),
          title: 'On secondary',
        ),
        _ColorPickerTile(
          key: ValueKey('secondaryContainer@${darkScheme.secondaryContainer}'),
          color: darkScheme.secondaryContainer,
          onChanged: (color) => setState(
            () => darkScheme = darkScheme.copyWith(
              secondaryContainer: color,
            ),
          ),
          title: 'Secondary container',
        ),
        _ColorPickerTile(
          key: ValueKey(
              'onSecondaryContainer@${darkScheme.onSecondaryContainer}'),
          color: darkScheme.onSecondaryContainer,
          onChanged: (color) => setState(
            () => darkScheme = darkScheme.copyWith(
              onSecondaryContainer: color,
            ),
          ),
          title: 'On secondary container',
        ),
        _ColorPickerTile(
          key: ValueKey('tertiary@${darkScheme.tertiary}'),
          color: darkScheme.tertiary,
          onChanged: (color) => setState(
            () => darkScheme = darkScheme.copyWith(
              tertiary: color,
            ),
          ),
          title: 'Tertiary',
        ),
        _ColorPickerTile(
          key: ValueKey('onTertiary@${darkScheme.onTertiary}'),
          color: darkScheme.onTertiary,
          onChanged: (color) => setState(
            () => darkScheme = darkScheme.copyWith(
              onTertiary: color,
            ),
          ),
          title: 'On tertiary',
        ),
        _ColorPickerTile(
          key: ValueKey('tertiaryContainer@${darkScheme.tertiaryContainer}'),
          color: darkScheme.tertiaryContainer,
          onChanged: (color) => setState(
            () => darkScheme = darkScheme.copyWith(
              tertiaryContainer: color,
            ),
          ),
          title: 'Tertiary container',
        ),
        _ColorPickerTile(
          key:
              ValueKey('onTertiaryContainer@${darkScheme.onTertiaryContainer}'),
          color: darkScheme.onTertiaryContainer,
          onChanged: (color) => setState(
            () => darkScheme = darkScheme.copyWith(
              onTertiaryContainer: color,
            ),
          ),
          title: 'On tertiary container',
        ),
        _ColorPickerTile(
          key: ValueKey('error@${darkScheme.error}'),
          color: darkScheme.error,
          onChanged: (color) => setState(
            () => darkScheme = darkScheme.copyWith(
              error: color,
            ),
          ),
          title: 'Error',
        ),
        _ColorPickerTile(
          key: ValueKey('errorContainer@${darkScheme.errorContainer}'),
          color: darkScheme.errorContainer,
          onChanged: (color) => setState(
            () => darkScheme = darkScheme.copyWith(
              errorContainer: color,
            ),
          ),
          title: 'Error container',
        ),
        _ColorPickerTile(
          key: ValueKey('onError@${darkScheme.onError}'),
          color: darkScheme.onError,
          onChanged: (color) => setState(
            () => darkScheme = darkScheme.copyWith(
              onError: color,
            ),
          ),
          title: 'On error',
        ),
        _ColorPickerTile(
          key: ValueKey('onErrorContainer@${darkScheme.onErrorContainer}'),
          color: darkScheme.onErrorContainer,
          onChanged: (color) => setState(
            () => darkScheme = darkScheme.copyWith(
              onErrorContainer: color,
            ),
          ),
          title: 'On error container',
        ),
        _ColorPickerTile(
          key: ValueKey('background@${darkScheme.surface}'),
          color: darkScheme.surface,
          onChanged: (color) => setState(
            () => darkScheme = darkScheme.copyWith(
              surface: color,
            ),
          ),
          title: 'Background',
        ),
        _ColorPickerTile(
          key: ValueKey('onBackground@${darkScheme.onSurface}'),
          color: darkScheme.onSurface,
          onChanged: (color) => setState(
            () => darkScheme = darkScheme.copyWith(
              onSurface: color,
            ),
          ),
          title: 'On background',
        ),
        _ColorPickerTile(
          key: ValueKey('surface@${darkScheme.surface}'),
          color: darkScheme.surface,
          onChanged: (color) => setState(
            () => darkScheme = darkScheme.copyWith(
              surface: color,
            ),
          ),
          title: 'Surface',
        ),
        _ColorPickerTile(
          key: ValueKey('onSurface@${darkScheme.onSurface}'),
          color: darkScheme.onSurface,
          onChanged: (color) => setState(
            () => darkScheme = darkScheme.copyWith(
              onSurface: color,
            ),
          ),
          title: 'On surface',
        ),
        _ColorPickerTile(
          key: ValueKey('surfaceVariant@${darkScheme.surfaceContainerHighest}'),
          color: darkScheme.surfaceContainerHighest,
          onChanged: (color) => setState(
            () => darkScheme = darkScheme.copyWith(
              surfaceContainerHighest: color,
            ),
          ),
          title: 'Surface variant',
        ),
        _ColorPickerTile(
          key: ValueKey('onSurfaceVariant@${darkScheme.onSurfaceVariant}'),
          color: darkScheme.onSurfaceVariant,
          onChanged: (color) => setState(
            () => darkScheme = darkScheme.copyWith(
              onSurfaceVariant: color,
            ),
          ),
          title: 'On surface variant',
        ),
        _ColorPickerTile(
          key: ValueKey('outline@${darkScheme.outline}'),
          color: darkScheme.outline,
          onChanged: (color) => setState(
            () => darkScheme = darkScheme.copyWith(
              outline: color,
            ),
          ),
          title: 'Outline',
        ),
        _ColorPickerTile(
          key: ValueKey('onInverseSurface@${darkScheme.onInverseSurface}'),
          color: darkScheme.onInverseSurface,
          onChanged: (color) => setState(
            () => darkScheme = darkScheme.copyWith(
              onInverseSurface: color,
            ),
          ),
          title: 'On inverse surface',
        ),
        _ColorPickerTile(
          key: ValueKey('inverseSurface@${darkScheme.inverseSurface}'),
          color: darkScheme.inverseSurface,
          onChanged: (color) => setState(
            () => darkScheme = darkScheme.copyWith(
              inverseSurface: color,
            ),
          ),
          title: 'Inverse surface',
        ),
        _ColorPickerTile(
          key: ValueKey('inversePrimary@${darkScheme.inversePrimary}'),
          color: darkScheme.inversePrimary,
          onChanged: (color) => setState(
            () => darkScheme = darkScheme.copyWith(
              inversePrimary: color,
            ),
          ),
          title: 'Inverse primary',
        ),
        _ColorPickerTile(
          key: ValueKey('shadow@${darkScheme.shadow}'),
          color: darkScheme.shadow,
          onChanged: (color) => setState(
            () => darkScheme = darkScheme.copyWith(
              shadow: color,
            ),
          ),
          title: 'Shadow',
        ),
        _ColorPickerTile(
          key: ValueKey('surfaceTint@${darkScheme.surfaceTint}'),
          color: darkScheme.surfaceTint,
          onChanged: (color) => setState(
            () => darkScheme = darkScheme.copyWith(
              surfaceTint: color,
            ),
          ),
          title: 'Surface tint',
        ),
        _ColorPickerTile(
          key: ValueKey('outlineVariant@${darkScheme.outlineVariant}'),
          color: darkScheme.outlineVariant,
          onChanged: (color) => setState(
            () => darkScheme = darkScheme.copyWith(
              outlineVariant: color,
            ),
          ),
          title: 'Outline variant',
        ),
        _ColorPickerTile(
          key: ValueKey('scrim@${darkScheme.scrim}'),
          color: darkScheme.scrim,
          onChanged: (color) => setState(
            () => darkScheme = darkScheme.copyWith(
              scrim: color,
            ),
          ),
          title: 'Scrim',
        ),
      ];
}

class _ColorPickerTile extends StatefulWidget {
  const _ColorPickerTile({
    required this.title,
    required this.color,
    required this.onChanged,
    super.key,
  });

  final String title;
  final Color color;
  final void Function(Color newColor) onChanged;

  @override
  State<_ColorPickerTile> createState() => __ColorPickerTileState();
}

class __ColorPickerTileState extends State<_ColorPickerTile> {
  final colorSquareSize = 40.0;
  final colorSquareBorderRadius = 8.0;
  late Color color = widget.color;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SettingsItem(
      mode: SettingsItemMode.custom,
      title: widget.title,
      onTap: showColorPicker,
      trailing: ColorIndicator(
        key: widget.key,
        borderColor: Theme.of(context).colorScheme.outline,
        hasBorder: true,
        width: colorSquareSize,
        height: colorSquareSize,
        borderRadius: colorSquareBorderRadius,
        color: color,
        onSelectFocus: false,
      ),
    );
  }

  Future<void> showColorPicker() async {
    final strings = Strings.of(context)!;
    final scheme = Theme.of(context).colorScheme;
    final text = Theme.of(context).textTheme;

    await showAnimatedDialog(
      context: context,
      animationType: DialogTransitionType.fadeScale,
      builder: (context) => StatefulBuilder(builder: (context, setState) {
        return AlertDialog(
          content: ConstrainedBox(
            constraints: const BoxConstraints(minWidth: 280, maxWidth: 560),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                ColorPicker(
                  color: color,
                  title: Text(
                    strings.pickAColor,
                    style:
                        text.headlineSmall?.copyWith(color: scheme.onSurface),
                  ),
                  height: colorSquareSize,
                  width: colorSquareSize,
                  borderRadius: colorSquareBorderRadius,
                  padding: EdgeInsets.zero,
                  onColorChanged: (newColor) =>
                      setState(() => color = newColor),
                  subheading: const SizedBox(height: 16),
                  wheelSubheading: const SizedBox(height: 16),
                  pickersEnabled: const <ColorPickerType, bool>{
                    ColorPickerType.both: true,
                    ColorPickerType.primary: false,
                    ColorPickerType.accent: false,
                    ColorPickerType.bw: false,
                    ColorPickerType.custom: false,
                    ColorPickerType.wheel: true,
                  },
                  pickerTypeLabels: <ColorPickerType, String>{
                    ColorPickerType.both: strings.pickerPrimary,
                    ColorPickerType.wheel: strings.pickerWheel,
                  },
                  copyPasteBehavior: const ColorPickerCopyPasteBehavior(
                    copyFormat: ColorPickerCopyFormat.numHexRRGGBB,
                    ctrlC: true,
                    ctrlV: true,
                    editFieldCopyButton: false,
                    secondaryMenu: true,
                    pasteButton: false,
                    copyButton: false,
                  ),
                ),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              onPressed: Navigator.of(context).pop,
              child: Text(strings.cancel),
            ),
            TextButton(
              onPressed: () {
                widget.onChanged(color);
                Navigator.of(context).pop();
              },
              child: Text(strings.ok),
            ),
          ],
        );
      }),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:gap/gap.dart';

import '/strings/strings.dart';
import '/theme/text_theme.dart';
import '/state/settings.dart';

class FontPairCard extends ConsumerStatefulWidget {
  const FontPairCard({
    required this.pair,
    required this.selected,
    required this.onSelect,
    super.key,
  });

  final bool selected;
  final FontPairing pair;
  final VoidCallback onSelect;

  @override
  ConsumerState<FontPairCard> createState() => _FontPairCardState();
}

class _FontPairCardState extends ConsumerState<FontPairCard> {
  var showFontNames = false;

  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context)!;
    final settings = Settings.of(context);
    final scheme = Theme.of(context).colorScheme;

    final factor = settings.accessability.textScaleFactor.value ?? 1;
    final width = 192 * factor;
    final height = (width - width ~/ 8) * factor;

    final content = InkWell(
      onTap: widget.onSelect,
      onLongPress: () {
        HapticFeedback.selectionClick();
        setState(() => showFontNames = !showFontNames);
      },
      child: SizedBox(
        height: height,
        width: width,
        child: Padding(
          padding: const EdgeInsets.all(16),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                showFontNames
                    ? widget.pair.accent.displayName
                    : widget.pair.displayName,
                style: widget.pair.accent.textTheme.titleLarge,
                textAlign: TextAlign.center,
              ),
              const Gap(8),
              Text(
                showFontNames ? widget.pair.mono.displayName : 'print(doable);',
                style: widget.pair.mono.textTheme.bodyLarge,
              ),
              const Gap(8),
              Text(
                showFontNames
                    ? widget.pair.primary.displayName
                    : strings.doable,
                style: widget.pair.primary.textTheme.bodyMedium,
                textAlign: TextAlign.center,
              ),
            ],
          ),
        ),
      ),
    );

    final defaultStyle = widget.pair.primary.textTheme.bodyMedium!;

    if (widget.selected) {
      return Card.filled(
        key: ValueKey(widget.selected),
        color: scheme.primaryContainer,
        clipBehavior: Clip.antiAliasWithSaveLayer,
        child: DefaultTextStyle(
          style: defaultStyle.copyWith(color: scheme.onPrimaryContainer),
          child: content,
        ),
      );
    } else {
      return Card.outlined(
        key: ValueKey(widget.selected),
        clipBehavior: Clip.antiAliasWithSaveLayer,
        child: DefaultTextStyle(
          style: defaultStyle.copyWith(color: scheme.onSurface),
          child: content,
        ),
      );
    }
  }
}

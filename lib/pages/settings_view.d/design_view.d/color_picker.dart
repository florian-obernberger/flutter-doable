import 'package:flutter/material.dart';
import 'package:flex_color_picker/flex_color_picker.dart';
import 'package:gap/gap.dart';
import 'package:notified_preferences/notified_preferences.dart';

import '/strings/strings.dart';
import '/util/animated_dialog.dart';

import '../item.dart';

class ColorPickerTile extends StatefulWidget {
  const ColorPickerTile({
    required this.isEnabled,
    required this.notifier,
    required this.title,
    this.fallbackColor,
    this.description,
    super.key,
  });

  final bool isEnabled;
  final String title;
  final String? description;
  final Color? fallbackColor;
  final PreferenceNotifier<Color?> notifier;

  @override
  State<ColorPickerTile> createState() => _ColorPickerTileState();
}

class _ColorPickerTileState extends State<ColorPickerTile> {
  final colorSquareSize = 40.0;
  final colorSquareBorderRadius = 8.0;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final scheme = Theme.of(context).colorScheme;

    final fallback = widget.fallbackColor ?? scheme.primary;

    return SettingsItem(
      mode: SettingsItemMode.custom,
      title: widget.title,
      description: widget.description,
      onTap: showColorPicker,
      isEnabled: widget.isEnabled,
      trailing: ColorIndicator(
        width: colorSquareSize,
        height: colorSquareSize,
        borderRadius: colorSquareBorderRadius,
        color: fallback.withOpacity(widget.isEnabled ? 1 : 0.38),
        onSelectFocus: false,
      ),
    );
  }

  Future<void> showColorPicker() async {
    final strings = Strings.of(context)!;
    final scheme = Theme.of(context).colorScheme;
    final text = Theme.of(context).textTheme;
    Color? pickedColor;

    final fallback = widget.fallbackColor ?? scheme.primary;

    final color =
        widget.notifier.value != null ? widget.notifier.value! : fallback;

    await showAnimatedDialog(
      context: context,
      animationType: DialogTransitionType.fadeScale,
      builder: (context) => AlertDialog(
        backgroundColor: scheme.surfaceContainerHigh,
        content: ConstrainedBox(
          constraints: const BoxConstraints(minWidth: 280, maxWidth: 560),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              ColorPicker(
                color: color,
                title: Text(
                  strings.pickAColor,
                  style: text.headlineSmall?.copyWith(color: scheme.onSurface),
                ),
                height: colorSquareSize,
                width: colorSquareSize,
                borderRadius: colorSquareBorderRadius,
                padding: EdgeInsets.zero,
                onColorChanged: (color) => pickedColor = color,
                subheading: const SizedBox(height: 16),
                wheelSubheading: const SizedBox(height: 16),
                pickersEnabled: const <ColorPickerType, bool>{
                  ColorPickerType.both: true,
                  ColorPickerType.primary: false,
                  ColorPickerType.accent: false,
                  ColorPickerType.bw: false,
                  ColorPickerType.custom: false,
                  ColorPickerType.wheel: true,
                },
                pickerTypeLabels: <ColorPickerType, String>{
                  ColorPickerType.both: strings.pickerPrimary,
                  ColorPickerType.wheel: strings.pickerWheel,
                },
                copyPasteBehavior: const ColorPickerCopyPasteBehavior(
                  copyFormat: ColorPickerCopyFormat.numHexRRGGBB,
                  ctrlC: true,
                  ctrlV: true,
                  editFieldCopyButton: false,
                  secondaryMenu: true,
                  pasteButton: false,
                  copyButton: false,
                ),
              ),
            ],
          ),
        ),
        actionsAlignment: MainAxisAlignment.spaceBetween,
        actions: <Widget>[
          TextButton(
            onPressed: () {
              widget.notifier.value = null;
              Navigator.of(context).pop();
            },
            child: Text(strings.reset),
          ),
          Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              TextButton(
                onPressed: Navigator.of(context).pop,
                child: Text(strings.cancel),
              ),
              const Gap(8),
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                  if (pickedColor != null) {
                    widget.notifier.value = pickedColor!;
                  }
                },
                child: Text(strings.ok),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

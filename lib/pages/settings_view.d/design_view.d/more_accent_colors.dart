import 'package:flutter/material.dart';
import 'package:material_symbols_icons/symbols.dart';

import '/strings/strings.dart';
import '/classes/motion.dart';
import '/state/settings.dart';
import '/widgets/detailed_switch_list_tile.dart';
import '/util/animated_dialog.dart';

import 'custom_color_scheme_dialog.dart';
import 'color_picker.dart';

class MoreAccentColors extends StatefulWidget {
  const MoreAccentColors({required this.isEnabled, super.key});

  final bool isEnabled;

  @override
  State<MoreAccentColors> createState() => _MoreAccentColorsState();
}

class _MoreAccentColorsState extends State<MoreAccentColors>
    with SingleTickerProviderStateMixin {
  bool isExpanded = false;

  late final Animation<double> expansionAnimation;
  late final Animation<double> fadeAnimation;
  late final AnimationController _controller;
  late final Duration buttonDuration;

  Settings? _settings;
  Settings get settings => _settings!;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    if (_settings == null) {
      _settings = Settings.of(context);

      final reduceMotion = settings.accessability.reduceMotion.value;

      buttonDuration = reduceMotion ? Duration.zero : Motion.medium2;

      final duration = reduceMotion ? Duration.zero : Motion.long1;

      _controller = AnimationController(vsync: this, duration: duration);
      expansionAnimation = CurvedAnimation(
        parent: _controller,
        curve: Easing.standard,
        reverseCurve: Easing.standard,
      );
      fadeAnimation = CurvedAnimation(
        parent: _controller,
        curve: Easing.standardDecelerate,
      );
    }
  }

  @override
  void dispose() {
    _controller.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context)!;
    final materialStrings = MaterialLocalizations.of(context);
    final scheme = Theme.of(context).colorScheme;

    final useCustomTheme = settings.design.enableCustomColorScheme.value;

    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        ListTile(
          enabled: widget.isEnabled,
          onTap: showDetails,
          title: Text(strings.advancedColorOptions),
          trailing: IconButton(
            padding: EdgeInsets.zero,
            onPressed: widget.isEnabled ? showDetails : null,
            color: scheme.onSurface,
            tooltip: isExpanded
                ? materialStrings.expandedIconTapHint
                : materialStrings.collapsedIconTapHint,
            icon: AnimatedRotation(
              turns: isExpanded ? 0.5 : 0,
              duration: buttonDuration,
              curve: Easing.standard,
              child: const Icon(Symbols.arrow_drop_down),
            ),
          ),
        ),
        FadeTransition(
          opacity: fadeAnimation,
          child: SizeTransition(
            sizeFactor: expansionAnimation,
            axisAlignment: -1,
            child: Theme(
              data: Theme.of(context),
              child: Padding(
                padding: const EdgeInsetsDirectional.only(start: 16),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    ValueListenableBuilder(
                      valueListenable:
                          settings.design.customSecondaryAccentColor,
                      builder: (_, __, ___) => ColorPickerTile(
                        title: strings.customSecondaryAccentPicker,
                        notifier: settings.design.customSecondaryAccentColor,
                        isEnabled: !useCustomTheme && widget.isEnabled,
                        fallbackColor: scheme.secondary,
                      ),
                    ),
                    ValueListenableBuilder(
                      valueListenable:
                          settings.design.customTertiaryAccentColor,
                      builder: (_, __, ___) => ColorPickerTile(
                        title: strings.customTertiaryAccentPicker,
                        notifier: settings.design.customTertiaryAccentColor,
                        isEnabled: !useCustomTheme && widget.isEnabled,
                        fallbackColor: scheme.tertiary,
                      ),
                    ),
                    ValueListenableBuilder(
                      valueListenable: settings.design.enableCustomColorScheme,
                      builder: (context, useCustom, _) {
                        return DetailedSwitchListTile(
                          title: Text(strings.customColorTheme),
                          value: useCustom,
                          enabled: widget.isEnabled,
                          onSwitchChanged: (value) => settings
                              .design.enableCustomColorScheme.value = value,
                          onShowDetails: () {
                            showAnimatedDialog(
                              useSafeArea: false,
                              context: context,
                              builder: (context) =>
                                  const CustomColorSchemeDialog(),
                              animationType:
                                  DialogTransitionType.slideFromBottomFade,
                            );
                          },
                        );
                      },
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  void showDetails() {
    setState(() {
      isExpanded = !isExpanded;
    });

    if (isExpanded) {
      _controller.forward();
    } else {
      _controller.reverse();
    }
  }
}

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:format/format.dart';
import 'package:gap/gap.dart';
import 'package:go_router/go_router.dart';
import 'package:material_symbols_icons/symbols.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:simple_icons/simple_icons.dart';
import 'package:result/anyhow.dart';

import '/strings/strings.dart';
import '/state/settings.dart';
import '/classes/logger.dart';
import '/types/version.dart';
import '/types/abi.dart';
import '/util/app_info.dart';
import '/util/get_localized_changelog.dart';
import '/widgets/back_button.dart' as back;
import '/widgets/markdown_text.dart';
import '/widgets/async_builder.dart';

import 'about_view.d/about_card.dart';

final separator = '=' * 80;

class _Urls {
  static const repo = Url(
    'https://codeberg.org/florian-obernberger/flutter-doable',
  );

  static const privacy = Url(
    'https://codeberg.org/florian-obernberger/flutter-doable/src/branch/main/PRIVACY.md',
  );

  static const fdroid = Url(
    'https://codeberg.org/florian-obernberger/fdroid-repo',
  );

  static const mastodon = Url('https://floss.social/@doable');

  static const email = Url('mailto:issues@doable.at');
}

class SettingsAboutView extends ConsumerWidget {
  const SettingsAboutView({this.showAppBar = true, super.key});

  final bool showAppBar;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final strings = Strings.of(context)!;
    final settings = Settings.of(context);
    final scheme = Theme.of(context).colorScheme;
    final text = Theme.of(context).textTheme;

    final (whatsNew, version) =
        ref.watch(appInfoProvider).when<(Widget, Widget)>(
              data: (info) {
                final changelog = rootBundle
                    .loadString('assets/info/whats_new.txt')
                    .then((changelogs) => changelogs
                        .split(separator)
                        .asMap()[localizedChangelogPosition(context)]!
                        .trim());

                final whatsNew = AboutCard.elevated(
                  title: strings.whatsNew,
                  content: changelog.mapDataOrElse(
                    data: (context, data) => MarkdownText(
                      data.replaceAll('• ', '- '),
                      maxLines: 999,
                      softWrap: true,
                      supportCodebergIssues: true,
                    ),
                    orElse: (_) => const SizedBox.shrink(),
                  ),
                );

                return (whatsNew, Text(getVersion(context, info)));
              },
              error: (_, __) => (
                AboutCard.filled(title: strings.whatsNew),
                const Text('🔖?.??.?'),
              ),
              loading: () => (
                AboutCard.filled(title: strings.whatsNew),
                const Text('🔖?.??.?'),
              ),
            );

    return Scaffold(
      appBar: showAppBar
          ? AppBar(
              title: Text(strings.about),
              leading: const back.BackButton(),
            )
          : null,
      persistentFooterButtons: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
          child: SizedBox(
            width: double.infinity,
            child: OutlinedButton(
              onPressed: () => GoRouter.of(context).go('/introduction'),
              child: Text(strings.showIntroScreen),
            ),
          ),
        ),
      ],
      body: SingleChildScrollView(
        physics: const ClampingScrollPhysics(),
        padding: const EdgeInsets.all(12),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            const Gap(16),
            Text(
              strings.appTitle,
              style: settings.design.accentFontTheme.textTheme.displayLarge!
                  .copyWith(color: scheme.primary),
            ),
            const Gap(4),
            DefaultTextStyle(
              style: text.titleMedium!.copyWith(
                color: scheme.secondary,
                fontStyle: FontStyle.italic,
              ),
              child: version,
            ),
            const Gap(12),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                AboutUrlAction.filled(
                  label: strings.email,
                  url: _Urls.email,
                  icon: const Icon(Symbols.mail_lock, fill: 1),
                  hasIcon: true,
                ),
                const Gap(8),
                AboutUrlAction.filled(
                  label: strings.mastodon,
                  url: _Urls.mastodon,
                  icon: const Icon(SimpleIcons.mastodon),
                  hasIcon: true,
                ),
              ],
            ),
            const Gap(12),
            Text(
              'Made with ❤️ by Florian Obernberger\nPowered by Flutter',
              style: text.bodyMedium!.copyWith(
                color: scheme.secondary,
                fontStyle: FontStyle.italic,
              ),
              textAlign: TextAlign.center,
            ),
            const Gap(24),
            whatsNew,
            AboutCard.elevated(
              title: strings.openSource,
              content: Text(strings.aboutOpenSource),
              actions: <Widget>[
                AboutUrlAction.outlined(
                  label: strings.sourceCode,
                  url: _Urls.repo,
                ),
                const Gap(8),
                AboutUrlAction.outlined(
                  label: strings.fdroid,
                  url: _Urls.fdroid,
                  icon: const Icon(SimpleIcons.fdroid),
                )
              ],
            ),
            AboutCard.elevated(
              title: strings.privacy,
              content: MarkdownText(strings.aboutPrivacy, softWrap: true),
              actions: <Widget>[
                AboutUrlAction.outlined(
                  label: strings.privacyPolicy,
                  url: _Urls.privacy,
                  icon: const Icon(Symbols.security, fill: 0),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }

  String getVersion(BuildContext context, PackageInfo info) {
    final abi = Abi.fromIndex(info.buildNumber.split('').last)
        .context('Could not parse Abi from package info');

    if (abi case Err(:final error)) {
      logger.ah(error, level: LogLevel.debug, tag: 'AboutView');
    }

    final fmt = switch (Directionality.of(context)) {
      TextDirection.ltr => '🔖{version} {abi}',
      TextDirection.rtl => '{abi} {version}🔖'
    };

    return format(fmt, {
      'version': Version.fromPackageInfo(info),
      'abi': abi.unwrapOr(Abi.universal),
    });
  }
}

import 'package:flutter/material.dart';
import 'package:gap/gap.dart';
import 'package:material_symbols_icons/symbols.dart';

import '/util/extensions/build_context.dart';

class Url {
  const Url(this.url);

  final String url;

  Future<void> open(BuildContext context) => context.launchUrl(url);
}

enum _AboutUrlActionType { filled, filledTonal, outlined, text }

class AboutUrlAction extends StatelessWidget {
  const AboutUrlAction.filled({
    required this.label,
    required this.url,
    this.icon,
    this.hasIcon = true,
    this.expanded = false,
    this.minimumSize,
    super.key,
  }) : _type = _AboutUrlActionType.filled;

  const AboutUrlAction.filledTonal({
    required this.label,
    required this.url,
    this.icon,
    this.hasIcon = true,
    this.expanded = false,
    this.minimumSize,
    super.key,
  }) : _type = _AboutUrlActionType.filledTonal;

  const AboutUrlAction.outlined({
    required this.label,
    required this.url,
    this.icon,
    this.hasIcon = true,
    this.expanded = false,
    this.minimumSize,
    super.key,
  }) : _type = _AboutUrlActionType.outlined;

  const AboutUrlAction.text({
    required this.label,
    required this.url,
    this.icon,
    this.hasIcon = true,
    this.expanded = false,
    this.minimumSize,
    super.key,
  }) : _type = _AboutUrlActionType.text;

  final String label;
  final Url url;
  final Widget? icon;
  final bool hasIcon;
  final bool expanded;
  final Size? minimumSize;

  final _AboutUrlActionType _type;

  @override
  Widget build(BuildContext context) {
    Widget child;

    if (!hasIcon) {
      child = switch (_type) {
        _AboutUrlActionType.filled => FilledButton(
            style: FilledButton.styleFrom(minimumSize: minimumSize),
            onPressed: () => url.open(context),
            child: Text(label),
          ),
        _AboutUrlActionType.filledTonal => FilledButton.tonal(
            style: FilledButton.styleFrom(minimumSize: minimumSize),
            onPressed: () => url.open(context),
            child: Text(label),
          ),
        _AboutUrlActionType.outlined => OutlinedButton(
            style: OutlinedButton.styleFrom(minimumSize: minimumSize),
            onPressed: () => url.open(context),
            child: Text(label),
          ),
        _AboutUrlActionType.text => TextButton(
            style: TextButton.styleFrom(minimumSize: minimumSize),
            onPressed: () => url.open(context),
            child: Text(label),
          )
      };
    } else {
      child = switch (_type) {
        _AboutUrlActionType.filled => FilledButton.icon(
            style: FilledButton.styleFrom(minimumSize: minimumSize),
            onPressed: () => url.open(context),
            label: Text(label),
            icon: icon ?? const Icon(Symbols.open_in_browser),
          ),
        _AboutUrlActionType.filledTonal => FilledButton.tonalIcon(
            style: FilledButton.styleFrom(minimumSize: minimumSize),
            onPressed: () => url.open(context),
            label: Text(label),
            icon: icon ?? const Icon(Symbols.open_in_browser),
          ),
        _AboutUrlActionType.outlined => OutlinedButton.icon(
            style: OutlinedButton.styleFrom(minimumSize: minimumSize),
            onPressed: () => url.open(context),
            label: Text(label),
            icon: icon ?? const Icon(Symbols.open_in_browser),
          ),
        _AboutUrlActionType.text => TextButton.icon(
            style: TextButton.styleFrom(minimumSize: minimumSize),
            onPressed: () => url.open(context),
            label: Text(label),
            icon: icon ?? const Icon(Symbols.open_in_browser),
          )
      };
    }

    if (expanded) child = Expanded(child: child);

    return IconTheme(
      data: IconTheme.of(context).copyWith(size: 18),
      child: child,
    );
  }
}

enum _AboutCardType { elevated, filled, outlined }

class AboutCard extends StatelessWidget {
  const AboutCard.elevated({
    required this.title,
    this.content,
    this.actions,
    super.key,
  }) : _type = _AboutCardType.elevated;

  const AboutCard.filled({
    required this.title,
    this.content,
    this.actions,
    super.key,
  }) : _type = _AboutCardType.filled;

  const AboutCard.outlined({
    required this.title,
    this.content,
    this.actions,
    super.key,
  }) : _type = _AboutCardType.outlined;

  final String title;
  final Widget? content;
  final List<Widget>? actions;

  final _AboutCardType _type;

  @override
  Widget build(BuildContext context) {
    final scheme = Theme.of(context).colorScheme;
    final text = Theme.of(context).textTheme;

    final titleStyle = text.headlineSmall!.copyWith(
      color: scheme.onSurface,
      // fontWeight: FontWeight.w500,
    );

    final contentStyle = text.bodyMedium!.copyWith(
      color: _type == _AboutCardType.filled
          ? scheme.onSurface
          : scheme.onSurfaceVariant,
    );

    final child = Padding(
      padding: const EdgeInsets.all(16),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(title, style: titleStyle),
          if (content != null) ...[
            const Gap(4),
            DefaultTextStyle(
              style: contentStyle,
              child: content!,
            ),
          ],
          if (actions != null) ...[
            if (content != null) const Gap(8),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: actions!,
            ),
          ]
        ],
      ),
    );

    return SizedBox(
      width: double.infinity,
      child: switch (_type) {
        _AboutCardType.elevated => Card(child: child),
        _AboutCardType.filled => Card.filled(child: child),
        _AboutCardType.outlined => Card.outlined(child: child),
      },
    );
  }
}

import 'package:flutter/material.dart';
import 'package:gap/gap.dart';
import 'package:go_router/go_router.dart';
import 'package:flutter_material_design_icons/flutter_material_design_icons.dart';

import '/widgets/link.dart';
import '/widgets/markdown_text.dart';
import '/resources/oss_licenses.dart';

import 'dev_icon.dart';

class LicenseItem extends StatelessWidget {
  const LicenseItem(
    this.package, {
    this.replace = false,
    this.onSelected,
    super.key,
  });

  final void Function(Package package)? onSelected;

  final Package package;
  final bool replace;

  @override
  Widget build(BuildContext context) {
    const location = '/settings/licenses/details';

    final scheme = Theme.of(context).colorScheme;
    final text = Theme.of(context).textTheme;

    final titleStyle = text.titleMedium?.copyWith(color: scheme.onSurface);
    final descriptionStyle = text.bodyMedium?.copyWith(
      color: scheme.onSurfaceVariant,
    );
    final versionStyle = text.bodyMedium!.copyWith(
      color: scheme.onSurfaceVariant,
    );

    return Card(
      clipBehavior: Clip.antiAliasWithSaveLayer,
      child: InkWell(
        onTap: () {
          if (onSelected != null) {
            onSelected!(package);
          } else {
            replace
                ? context.pushReplacement(location, extra: package)
                : context.push(location, extra: package);
          }
        },
        child: Padding(
          padding: const EdgeInsets.all(12),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                children: <Widget>[
                  if (package.isDevDependency) ...[
                    const DevIcon(),
                    const Gap(4)
                  ],
                  Expanded(child: Text(package.name.trim(), style: titleStyle)),
                ],
              ),
              MarkdownText(
                package.description.trim(),
                baseStyle: descriptionStyle,
                maxLines: 8,
                softWrap: true,
                overflow: TextOverflow.ellipsis,
              ),
              const Divider(),
              Row(
                children: <Widget>[
                  Text(package.version, style: versionStyle),
                  if (package.homepage != null) ...[
                    Icon(
                      MdiIcons.circleSmall,
                      color: versionStyle.color,
                      size: 18,
                    ),
                    Expanded(
                      child:
                          Link(url: package.homepage!, baseStyle: versionStyle),
                    )
                  ],
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:fuzzywuzzy/fuzzywuzzy.dart';

import '/widgets/material_search.dart';
import '/resources/oss_licenses.dart';

import 'item.dart';

class LicensesSearchDelegate extends MaterialSearchDelegate<Package> {
  LicensesSearchDelegate({
    super.searchFieldLabel,
    super.searchFieldStyle,
    super.searchFieldDecorationTheme,
    super.keyboardType,
    super.textInputAction,
    this.onSelected,
    this.onClose,
    required this.packages,
  });

  final List<Package> packages;
  final void Function(Package package)? onSelected;
  final void Function(BuildContext context, Package? result)? onClose;

  @override
  void close(BuildContext context, Package? result) {
    if (onClose != null) return onClose!(context, result);
    return super.close(context, result);
  }

  @override
  Widget buildResults(BuildContext context) {
    return buildSuggestions(context);
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    final sorted = extractAllSorted<Package>(
      cutoff: 10,
      query: query,
      choices: packages,
      getter: (p) => p.name,
    ).where((r) => r.score > 70).map((r) => r.choice);

    return ListView(
      padding: const EdgeInsets.all(12).add(
        EdgeInsets.only(bottom: MediaQuery.of(context).padding.bottom),
      ),
      physics: const ClampingScrollPhysics(),
      children: [
        for (final package in sorted)
          LicenseItem(package, onSelected: onSelected)
      ],
    );
  }
}

import 'package:flutter/material.dart';

import '/state/settings.dart';

class DevIcon extends StatelessWidget {
  const DevIcon({this.size = 24, super.key});

  final double size;

  @override
  Widget build(BuildContext context) {
    final settings = Settings.of(context);
    final scheme = Theme.of(context).colorScheme;

    final style = settings.design.monoFontTheme.textTheme.labelLarge!.copyWith(
      fontSize: _fontSize(),
      color: scheme.onSecondary,
    );

    return Container(
      padding: const EdgeInsets.symmetric(
        horizontal: 4,
        vertical: 1,
      ),
      decoration: BoxDecoration(
        color: scheme.secondary,
        borderRadius: BorderRadius.circular(4),
      ),
      child: Text('DEV', style: style),
    );
  }

  double _fontSize() => 12 + (size - 24) / 3;
}

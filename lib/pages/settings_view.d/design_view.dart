import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gap/gap.dart';
import 'package:material_symbols_icons/symbols.dart';
import 'package:notified_preferences/notified_preferences.dart';

import '/theme/text_theme.dart';
import '/strings/strings.dart';
import '/state/settings.dart';
import '/classes/system_info.dart';
import '/util/animated_dialog.dart';
import '/widgets/pride/pride_month.dart';
import '/widgets/restart_dialog.dart';
import '/widgets/back_button.dart' as back;
import '/data/doable_theme_mode.dart';

import 'design_view.d/color_picker.dart';
import 'design_view.d/more_accent_colors.dart';
import 'design_view.d/font_pair_card.dart';

import 'item.dart';

class SettingsDesignView extends StatefulWidget {
  const SettingsDesignView({this.showAppBar = true, super.key});

  final bool showAppBar;

  @override
  State<SettingsDesignView> createState() => _SettingsDesignViewState();
}

class _SettingsDesignViewState extends State<SettingsDesignView> {
  final systemInfo = SystemInfo.instance;

  Settings? _settings;
  Settings get settings => _settings!;

  late final ScrollController fontController;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    if (_settings == null) {
      _settings = Settings.of(context);

      settings.accessability.reduceMotion.addListener(showRestartDialog);
      settings.accessability.forceHighestRefreshRate
          .addListener(showRestartDialog);

      fontController = ScrollController(
        initialScrollOffset: fontOffset(settings.design.fontPairing.value),
      );
    }
  }

  double fontOffset(FontPairing pairing) {
    final position = FontPairing.values.indexOf(pairing);

    return switch (position) {
      0 => 0,
      <= 4 => 200.0 * position - 48,
      _ => 812,
    };
  }

  @override
  void dispose() {
    settings.accessability.reduceMotion.removeListener(showRestartDialog);
    settings.accessability.forceHighestRefreshRate
        .removeListener(showRestartDialog);

    super.dispose();
  }

  void showRestartDialog() {
    showAnimatedDialog(
      context: context,
      builder: (context) => const RestartAppDialog(),
    );
  }

  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context)!;
    final scheme = Theme.of(context).colorScheme;

    return Scaffold(
      appBar: widget.showAppBar
          ? AppBar(
              title: Text(strings.design),
              leading: const back.BackButton(),
            )
          : null,
      body: LayoutBuilder(builder: (context, constraints) {
        return ListView(
          padding: EdgeInsets.only(
            bottom: MediaQuery.of(context).padding.bottom + 16,
          ),
          physics: const ClampingScrollPhysics(),
          children: <Widget>[
            ListTile(
              contentPadding: EdgeInsets.zero,
              title: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: Text(strings.font),
              ),
              subtitle: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsetsDirectional.only(start: 16),
                    child: Text(
                      strings.fontDescription,
                      style: ListTileTheme.of(context).subtitleTextStyle,
                    ),
                  ),
                  SingleChildScrollView(
                    physics: const ClampingScrollPhysics(),
                    controller: fontController,
                    padding: const EdgeInsetsDirectional.only(
                      start: 12, // +4 from the Cards itself -> 16px
                      end: 12, // +4 from the Cards itself -> 16px
                      top: 4,
                    ),
                    scrollDirection: Axis.horizontal,
                    child: Row(
                      children: <Widget>[
                        for (final pair in FontPairing.values)
                          FontPairCard(
                            pair: pair,
                            selected: settings.design.fontPairing.value == pair,
                            onSelect: () {
                              HapticFeedback.heavyImpact();
                              settings.design.fontPairing.value = pair;
                            },
                          ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            if (systemInfo.supportsSystemFont)
              ValueListenableBuilder<bool>(
                valueListenable: settings.design.useSystemFont,
                builder: (context, useSystemFont, _) {
                  return SettingsItem(
                    mode: SettingsItemMode.toggle,
                    toggleValue: useSystemFont,
                    title: strings.useSystemFont,
                    description: strings.useSystemFontDescription,
                    onSwitchChanged: (value) =>
                        settings.design.useSystemFont.value = value,
                  );
                },
              ),
            const Divider(),
            if (systemInfo.supportsDynamicColor)
              _Toggle(
                notifier: settings.design.dynamicColors,
                title: strings.dynamicColors,
                description: strings.dynamicColorsDescription,
              ),
            ColorPickerTile(
              isEnabled: (!systemInfo.supportsDynamicColor ||
                      settings.design.dynamicColors.value == false) &&
                  !settings.design.enableCustomColorScheme.value,
              notifier: settings.design.customAccentColor,
              title: strings.customAccentPicker,
              description: strings.customAccentPickerDescription,
            ),
            MoreAccentColors(
                isEnabled: !systemInfo.supportsDynamicColor ||
                    settings.design.dynamicColors.value == false),
            const Divider(),
            ListTile(
              isThreeLine: true,
              title: Text(strings.darkMode),
              subtitle: Padding(
                padding: const EdgeInsets.only(top: 4),
                child: Row(
                  children: <Widget>[
                    buildChip(strings.on, ThemeMode.dark),
                    const Gap(8),
                    buildChip(strings.off, ThemeMode.light),
                    const Gap(8),
                    buildChip(strings.system, ThemeMode.system),
                  ],
                ),
              ),
            ),
            ValueListenableBuilder<DoableThemeMode?>(
              valueListenable: settings.design.themeMode,
              builder: (context, mode, _) {
                final themeMode =
                    mode ?? settings.design.themeMode.initialValue;

                return SettingsItem(
                  mode: SettingsItemMode.toggle,
                  toggleValue: themeMode.darkMode == DarkMode.black,
                  title: strings.trueBlackMode,
                  isEnabled: !themeMode.isLight,
                  description: strings.trueBlackModeDescription,
                  onSwitchChanged: (value) =>
                      settings.design.themeMode.value = themeMode.withDarkMode(
                    themeMode.darkMode == DarkMode.black
                        ? DarkMode.dark
                        : DarkMode.black,
                  ),
                );
              },
            ),
            const Divider(),
            _Toggle(
              notifier: settings.accessability.enablePrideFlags,
              title: strings.enablePrideFlags,
              description: strings.enablePrideFlagsDescription,
            ),
            ListTile(
              isThreeLine: true,
              title: Text(strings.prideFlagRestOfYear),
              subtitle: Padding(
                padding: const EdgeInsets.only(top: 4),
                child: Row(
                  children: <Widget>[
                    buildPrideChip('LGBTQIA+', PrideMonth.queer),
                    const Gap(8),
                    buildPrideChip('Disability', PrideMonth.disability),
                    const Gap(8),
                    buildPrideChip('None', null),
                  ],
                ),
              ),
            ),
            const Divider(),
            _Toggle(
              notifier: settings.fediverse.showButton,
              title: strings.showFediButton,
              description: strings.showFediButtonDescription,
            ),
            _Toggle(
              notifier: settings.accessability.reduceMotion,
              title: strings.reduceMotion,
              description: strings.reduceMotionDescription,
            ),
            _Toggle(
              notifier: settings.accessability.forceHighestRefreshRate,
              title: strings.forceHighestRefreshRate,
              description: strings.forceHighestRefreshRateDescription,
            ),
            ValueListenableBuilder(
              valueListenable: settings.accessability.textScaleFactor,
              builder: (context, fontSize, _) {
                final useSystem = fontSize == null;

                const minFontSize = .75;
                const maxFontSize = 1.5;
                const step = .25;

                void Function()? buttonPress({required bool isAdd}) {
                  return switch (fontSize) {
                    null => null,
                    >= maxFontSize when isAdd => null,
                    < maxFontSize when isAdd => () => settings
                        .accessability.textScaleFactor.value = fontSize + step,
                    <= minFontSize when !isAdd => null,
                    > minFontSize when !isAdd => () => settings
                        .accessability.textScaleFactor.value = fontSize - step,
                    double() => null,
                  };
                }

                return Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    SettingsItem(
                      title: strings.fontSize,
                      description: strings.fontSizeDescription,
                      mode: SettingsItemMode.toggle,
                      toggleValue: !useSystem,
                      onSwitchChanged: (fs) => settings
                          .accessability.textScaleFactor.value = fs ? 1 : null,
                    ),
                    Container(
                      height: 64,
                      width: double.infinity,
                      color: scheme.surface,
                      padding: const EdgeInsets.all(12),
                      child: Row(
                        children: <Widget>[
                          IconButton(
                            onPressed: buttonPress(isAdd: false),
                            icon: const Icon(Symbols.remove),
                            tooltip: strings.decrease,
                          ),
                          const Gap(12),
                          Expanded(
                            child: Slider(
                              value: fontSize ?? 1,
                              min: minFontSize,
                              max: maxFontSize,
                              divisions: 3,
                              label: useSystem
                                  ? null
                                  : '${(fontSize * 100).toStringAsFixed(0)}%',
                              onChanged: useSystem
                                  ? null
                                  : (fs) => settings
                                      .accessability.textScaleFactor.value = fs,
                            ),
                          ),
                          const Gap(12),
                          IconButton(
                            onPressed: buttonPress(isAdd: true),
                            icon: const Icon(Symbols.add),
                            tooltip: strings.increase,
                          ),
                        ],
                      ),
                    ),
                  ],
                );
              },
            ),
          ],
        );
      }),
    );
  }

  Widget buildChip(String label, ThemeMode mode) {
    final scheme = Theme.of(context).colorScheme;
    final text = Theme.of(context).textTheme;

    return ValueListenableBuilder<DoableThemeMode?>(
      valueListenable: settings.design.themeMode,
      builder: (context, themeMode, _) {
        themeMode ??= settings.design.themeMode.initialValue;
        final isSelected = themeMode.themeMode == mode;

        return InputChip(
          visualDensity: VisualDensity.compact,
          materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
          label: Text(
            label,
            style: text.labelLarge?.copyWith(
              color:
                  isSelected ? scheme.onSecondaryContainer : scheme.onSurface,
            ),
          ),
          backgroundColor:
              isSelected ? scheme.secondaryContainer : scheme.surface,
          selected: isSelected,
          side: BorderSide(
            color: isSelected ? scheme.secondaryContainer : scheme.outline,
          ),
          onSelected: (value) {
            if (value) {
              settings.design.themeMode.value = themeMode!.withThemeMode(mode);
            }
          },
        );
      },
    );
  }

  Widget buildPrideChip(String label, PrideMonth? month) {
    final scheme = Theme.of(context).colorScheme;
    final text = Theme.of(context).textTheme;

    return ValueListenableBuilder<PrideMonth?>(
      valueListenable: settings.accessability.constantPrideFlag,
      builder: (context, prideMonth, _) {
        final isSelected = prideMonth == month;

        return InputChip(
          visualDensity: VisualDensity.compact,
          materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
          label: Text(
            label,
            style: text.labelLarge?.copyWith(
              color:
                  isSelected ? scheme.onSecondaryContainer : scheme.onSurface,
            ),
          ),
          backgroundColor:
              isSelected ? scheme.secondaryContainer : scheme.surface,
          selected: isSelected,
          side: BorderSide(
            color: isSelected ? scheme.secondaryContainer : scheme.outline,
          ),
          onSelected: (value) => value
              ? settings.accessability.constantPrideFlag.value = month
              : null,
        );
      },
    );
  }
}

class _Toggle extends StatelessWidget {
  final PreferenceNotifier<bool> notifier;
  final String title;
  final String? description;
  final bool enabled;

  const _Toggle({
    required this.notifier,
    required this.title,
    this.description,
    this.enabled = true,
  });

  @override
  Widget build(BuildContext context) => ValueListenableBuilder<bool?>(
        valueListenable: notifier,
        builder: (context, value, _) => SettingsItem(
          title: title,
          description: description,
          mode: SettingsItemMode.toggle,
          toggleValue: value,
          isEnabled: enabled,
          onSwitchChanged: (v) => notifier.value = v,
        ),
      );
}

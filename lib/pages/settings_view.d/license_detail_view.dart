import 'package:flutter/material.dart';
import 'package:gap/gap.dart';

import '/widgets/link.dart';
import '/widgets/nav_button.dart';
import '/widgets/markdown_text.dart';
import '/util/build_text_selection_menu.dart';
import '/resources/oss_licenses.dart';

import 'licenses_view.d/dev_icon.dart';

class LicenseDetailView extends StatelessWidget {
  const LicenseDetailView(
    this.package, {
    this.onBack,
    this.isDialog = false,
    this.showAppBar = true,
    super.key,
  });

  final Package package;
  final VoidCallback? onBack;
  final bool isDialog;
  final bool showAppBar;

  @override
  Widget build(BuildContext context) {
    final scheme = Theme.of(context).colorScheme;
    final text = Theme.of(context).textTheme;

    final style = text.bodyMedium?.copyWith(color: scheme.onSurfaceVariant);

    return Scaffold(
      appBar: showAppBar
          ? AppBar(
              title: Row(
                children: <Widget>[
                  if (package.isDevDependency) ...[
                    const DevIcon(),
                    const Gap(4)
                  ],
                  Expanded(child: Text(package.name.trim())),
                ],
              ),
              leading: NavButton(
                type: isDialog ? NavButtonType.close : NavButtonType.back,
                onNav: onBack,
              ),
            )
          : null,
      body: ListView(
        padding: const EdgeInsets.all(16).add(
          EdgeInsets.only(bottom: MediaQuery.of(context).padding.bottom),
        ),
        physics: const ClampingScrollPhysics(),
        children: <Widget>[
          MarkdownText(
            package.description.trim(),
            baseStyle: style,
            softWrap: true,
          ),
          if (package.homepage != null) ...[
            const Divider(height: 32),
            Link(
              url: package.homepage!,
              baseStyle: style!,
              showIcon: true,
              maxLines: 2,
            ),
          ],
          if (package.license != null) ...[
            const Divider(height: 32),
            SelectableText(
              package.license!.trim(),
              contextMenuBuilder: buildTextSelectionMenu,
              style: style,
            )
          ],
        ],
      ),
    );
  }
}

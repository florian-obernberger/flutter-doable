import 'package:flutter/material.dart';
import 'package:material_symbols_icons/symbols.dart';

import '/strings/strings.dart';
import '/widgets/detailed_switch_list_tile.dart';

import 'settings.dart';

enum SettingsItemMode { popup, page, toggle, detailedToggle, custom }

// ignore: avoid_positional_boolean_parameters
typedef ToggleCallback = void Function(bool toggleValue);

class SettingsItem extends StatelessWidget {
  const SettingsItem({
    required this.title,
    this.description,
    this.onTap,
    this.onSwitchChanged,
    required this.mode,
    this.toggleValue,
    this.showDivider = false,
    this.isEnabled = true,
    this.trailing,
    super.key,
  }) : assert((mode == SettingsItemMode.toggle ||
                mode == SettingsItemMode.detailedToggle) !=
            (toggleValue == null));

  factory SettingsItem.fromSettingsPage(
      SettingsPage page, BuildContext context) {
    final strings = Strings.of(context)!;

    return SettingsItem(
      title: page.title(strings),
      description: page.description(strings),
      onTap: () => Navigator.of(context).pushNamed(page.route),
      mode: SettingsItemMode.page,
    );
  }

  final String title;
  final String? description;
  final bool? toggleValue;
  final bool showDivider;
  final bool isEnabled;
  final Widget? trailing;

  /// This will be called in the following cases:
  ///
  /// - [mode] is `popup`, `page`, or `custom`
  /// - [mode] is `detailedSwitch` and the tile is being pressed
  final VoidCallback? onTap;

  /// This will be called in the following cases:
  ///
  /// - [mode] is `switch`
  /// - [mode] is `detailedSwitch` and the switch is being pressed
  final ToggleCallback? onSwitchChanged;
  final SettingsItemMode mode;

  @override
  Widget build(BuildContext context) {
    final scheme = Theme.of(context).colorScheme;
    final text = Theme.of(context).textTheme;
    final listTileTheme = ListTileTheme.of(context);

    final disabledColor = scheme.onSurface.withOpacity(0.38);

    final titleStyle = listTileTheme.titleTextStyle?.copyWith(
      color: isEnabled ? null : disabledColor,
    );
    final subtitleStyle = listTileTheme.subtitleTextStyle?.copyWith(
      color: isEnabled ? null : disabledColor,
    );

    return LayoutBuilder(
      builder: (context, constraints) {
        final trailing = switch (mode) {
          SettingsItemMode.page => const Icon(Symbols.chevron_right),
          SettingsItemMode.custom => this.trailing,
          _ => null,
        };

        bool isThreeLine;
        final tp = TextPainter(
          textDirection: TextDirection.ltr,
          text: TextSpan(
            text: this.description,
            style: text.bodyMedium,
          ),
          maxLines: 1,
        );

        tp.layout(maxWidth: constraints.maxWidth - calculateSpace());

        isThreeLine = tp.didExceedMaxLines;

        final description = this.description != null
            ? Text(
                this.description!,
                softWrap: true,
                maxLines: 5,
                overflow: TextOverflow.ellipsis,
                style: subtitleStyle,
              )
            : null;

        final Widget tile;

        if (mode == SettingsItemMode.toggle) {
          tile = SwitchListTile(
            value: toggleValue!,
            onChanged: isEnabled ? onSwitchChanged : null,
            // showDivider: showDivider,
            title: Text(title, style: titleStyle),
            subtitle: description,
            isThreeLine: isThreeLine,
          );
        } else if (mode == SettingsItemMode.detailedToggle) {
          tile = DetailedSwitchListTile(
            value: toggleValue!,
            onShowDetails: onTap!,
            enabled: isEnabled,
            onSwitchChanged: onSwitchChanged!,
            title: Text(title),
            subtitle: description,
            isThreeLine: isThreeLine,
          );
        } else {
          tile = ListTile(
            onTap: onTap,
            enabled: isEnabled,
            trailing: trailing,
            title: Text(title),
            subtitle: description,
            isThreeLine: isThreeLine,
          );
        }

        if (showDivider) {
          return Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              tile,
              const Divider(),
            ],
          );
        }

        return tile;
      },
    );
  }

  int calculateSpace() {
    const listItemPadding = 38;

    final trailingSpace = switch (mode) {
      SettingsItemMode.toggle => 52,
      != SettingsItemMode.custom && != SettingsItemMode.popup => 24,
      _ => trailing == null ? 0 : 24,
    };

    final trailingPadding = trailingSpace == 0 ? 0 : 32;

    return listItemPadding + trailingSpace + trailingPadding;
  }
}

import 'package:flutter/material.dart';
import 'package:gap/gap.dart';

import '/strings/strings.dart';

import 'import_export.dart';

class ExportDataDialog extends StatefulWidget {
  const ExportDataDialog({super.key});

  @override
  State<ExportDataDialog> createState() => _ExportDataDialogState();
}

class _ExportDataDialogState extends State<ExportDataDialog> {
  final selected = <ExportData>{...ExportData.values};

  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context)!;

    return AlertDialog(
      title: Text(strings.export),
      contentPadding: const EdgeInsets.only(top: 16, bottom: 20),
      content: ConstrainedBox(
        constraints: const BoxConstraints(minWidth: 280, maxWidth: 560),
        child: SingleChildScrollView(
          physics: const ClampingScrollPhysics(),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 24, bottom: 16, right: 24),
                child: Text(
                  strings.selectWhatToExport,
                  maxLines: 10,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
              const Gap(16),
              for (final (pos, data) in ExportData.values.indexed) ...[
                if (pos != 0) const Divider(indent: 24, endIndent: 24),
                ListTile(
                  contentPadding: const EdgeInsets.fromLTRB(24, 0, 16, 0),
                  title: Text(switch (data) {
                    ExportData.todos => strings.todos,
                    ExportData.lists => strings.lists,
                    ExportData.settings => strings.settings,
                  }),
                  onTap: () => change(data),
                  trailing: Checkbox(
                    value: selected.contains(data),
                    onChanged: (value) => change(data, contains: value),
                  ),
                ),
              ],
            ],
          ),
        ),
      ),
      actions: <Widget>[
        TextButton(
          onPressed: Navigator.of(context).pop,
          child: Text(strings.cancel),
        ),
        FilledButton(
          onPressed: selected.isEmpty
              ? null
              : () => Navigator.of(context).pop(selected),
          child: Text(strings.exportFile),
        ),
      ],
    );
  }

  void change(ExportData data, {bool? contains}) {
    contains ??= selected.contains(data);

    setState(() => contains! ? selected.remove(data) : selected.add(data));
  }
}

import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:pick_or_save/pick_or_save.dart';
import 'package:result/anyhow.dart';

import '/classes/logger.dart';
import '/data/converters/json/todo.dart';
import '/data/converters/json/todo_list.dart';
import '/data/converters/toml/todo.dart' as toml;
import '/data/converters/toml/todo_list.dart' as toml;
import '/data/todo.dart';
import '/data/todo_list.dart';
import '/state/settings.dart';
import '/state/todo_db.dart';
import '/state/todo_list_db.dart';
import '/strings/strings.dart';

enum ExportData { todos, lists, settings }

Future<void> exportData(
  WidgetRef ref,
  Set<ExportData> dataToExport, {
  List<Todo>? todos,
  List<TodoList>? lists,
  SettingsData? settingsData,
  String? directory,
}) async {
  final todoDb = ref.read(todoDatabaseProvider);
  final listDb = ref.read(todoListDatabaseProvider);
  final settings = Settings.of(ref.context);

  final json = <String, dynamic>{};
  for (final data in dataToExport) {
    final value = switch (data) {
      ExportData.todos =>
        (todos ?? todoDb.values()).map((todo) => todo.toMap()).toList(),
      ExportData.lists =>
        (lists ?? listDb.values()).map((list) => list.toMap()).toList(),
      ExportData.settings => settingsData ?? settings.export(),
    };

    json[data.name] = value;
  }

  final date = DateTime.now()
      .copyWith(
        millisecond: 0,
        microsecond: 0,
      )
      .toIso8601String();

  try {
    await PickOrSave().fileSaver(
      params: FileSaverParams(
        mimeTypesFilter: ['application/json'],
        saveFiles: [
          SaveFileInfo(
            fileData: Uint8List.fromList(utf8.encode(todoJsonEncode(json))),
            fileName: 'doable_export_$date.json',
          ),
        ],
        directoryUri: directory,
      ),
    );
  } on Exception catch (e, stack) {
    logger.ah(
      Error(e).context('Could not export data'),
      level: LogLevel.debug,
      tag: 'Export',
      stack: stack,
    );
  }
}

Future<void> importData(WidgetRef ref, {bool withId = true}) async {
  final strings = Strings.of(ref.context)!;
  final messenger = ScaffoldMessenger.of(ref.context);

  final todoDb = ref.read(todoDatabaseProvider);
  final listDb = ref.read(todoListDatabaseProvider);
  final settings = Settings.of(ref.context);

  void showSnackBar([String? text]) {
    messenger.showSnackBar(SnackBar(
      duration: const Duration(seconds: 2),
      dismissDirection: DismissDirection.vertical,
      margin: const EdgeInsets.fromLTRB(24, 8, 24, 18),
      content: Text(text ?? strings.importLocalBackupError),
    ));
  }

  final result = await PickOrSave().filePicker(
    params: FilePickerParams(
      allowedExtensions: ['json', 'toml'],
      mimeTypesFilter: [
        'application/json',
        'application/toml',
        'text/toml',
        'text/*',
        'application/*'
      ],
      enableMultipleSelection: false,
      getCachedFilePath: true,
    ),
  );

  if (result == null || result.isEmpty) return;

  final String source;

  try {
    source = await File(result.first).readAsString();
  } catch (e, stack) {
    logger.ah(
      Error(e).context('Could not export data'),
      level: LogLevel.debug,
      tag: 'Import',
      stack: stack,
    );

    return showSnackBar();
  } finally {
    await File(result.first).delete();
  }

  final todosOptb =
      parseTodos(source).orElse(() => toml.parseTodos(source)).map(
            (value) => value.fold(<Todo>[], (todos, res) {
              res.inspectErr((err) => logger.i(
                    "Skipped because couldn't decode Todo",
                    error: err,
                  ));

              if (res case BaseOk(value: final todo)) {
                todos.add(todo.copyWith(
                  id: TodoDatabase.sanitizeCurrentId(todo.id),
                ));
              }
              return todos;
            }).toList(),
          );

  final listsOptb = parseTodoLists(source)
      .orElse(() => toml.parseTodoLists(source))
      .map((value) => value.fold(<TodoList>[], (lists, res) {
            res.inspectErr((err) => logger.i(
                  "Skipped because couldn't decode TodoList",
                  error: err,
                ));

            if (res case BaseOk(value: final list)) lists.add(list);
            return lists;
          }).toList());

  final settingsOptb = () {
    try {
      final json = todoJsonDecode(source);
      if (!json.containsKey('settings')) return const None<SettingsData>();
      return Some((json['settings']! as Map).cast<String, dynamic>());
    } catch (err) {
      logger.i('Error decoding settings', error: err, tag: 'Import');
      return const None<SettingsData>();
    }
  }();

  if (todosOptb.isNone() && listsOptb.isNone() && settingsOptb.isNone()) {
    logger.i(
      'File did neither contain Todos, Lists, nor Settings',
      tag: 'Import',
    );

    return showSnackBar();
  }

  final futures = <Future>[];

  int todoCount = 0;

  if (todosOptb case Some(value: final todos)) {
    todoCount = todos.length;

    for (final todo in todos) {
      futures.add(todoDb.store(todo));
    }

    // futures.add(todoDb.storeAll(todos));
  }

  if (listsOptb case Some(value: final lists)) {
    futures.add(listDb.storeAll(lists));
  }

  if (settingsOptb case Some(value: final settingsData)) {
    futures.add(Future.value(settings.import(settingsData)));
  }

  await Future.wait(futures);

  return showSnackBar(strings.importedNTodos(todoCount));
}

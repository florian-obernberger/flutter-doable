import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:gap/gap.dart';

import '/strings/strings.dart';
import '/util/animated_dialog.dart';

import 'import_export.d/import_export.dart';
import 'import_export.d/export_data_dialog.dart';

class ImportExport extends ConsumerWidget {
  const ImportExport({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final strings = Strings.of(context)!;
    final scheme = Theme.of(context).colorScheme;
    final text = Theme.of(context).textTheme;
    final messanger = ScaffoldMessenger.of(context);

    const buttonSize = Size.fromHeight(40);
    return Card(
      margin: const EdgeInsets.all(4),
      child: Padding(
        padding: const EdgeInsets.all(16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(
              strings.importExport,
              style: text.titleLarge?.copyWith(color: scheme.onSurface),
            ),
            const Gap(8),
            Row(
              children: <Widget>[
                Expanded(
                  child: FilledButton(
                    onPressed: () => importData(ref),
                    style: FilledButton.styleFrom(
                      minimumSize: buttonSize,
                    ),
                    child: Text(
                      strings.importTodosFromFile,
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
                const Gap(8),
                Expanded(
                  child: FilledButton(
                    onPressed: () => showAnimatedDialog<Set<ExportData>>(
                      context: context,
                      builder: (context) => const ExportDataDialog(),
                    ).then<bool>((dataToExport) async {
                      if (dataToExport == null || dataToExport.isEmpty) {
                        return false;
                      }

                      await exportData(ref, dataToExport);
                      return true;
                    }).then((exported) {
                      if (!exported) return;

                      messanger.showSnackBar(SnackBar(
                        duration: const Duration(seconds: 2),
                        dismissDirection: DismissDirection.vertical,
                        margin: const EdgeInsets.fromLTRB(24, 8, 24, 18),
                        content: Text(strings.exportedFile),
                      ));
                    }),
                    style: FilledButton.styleFrom(
                      minimumSize: buttonSize,
                    ),
                    child: Text(
                      strings.exportFile,
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

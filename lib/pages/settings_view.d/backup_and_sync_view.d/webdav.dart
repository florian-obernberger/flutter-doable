import 'package:flutter/material.dart';

import '/state/settings.dart';

import 'webdav.d/logged_out.dart';
import 'webdav.d/logged_in.dart';

class WebDAVStorage extends StatelessWidget {
  const WebDAVStorage({super.key});

  @override
  Widget build(BuildContext context) {
    final settings = Settings.of(context);

    return ValueListenableBuilder(
      valueListenable: settings.backup.webdav,
      builder: (context, isLoggedIn, _) => Card(
        margin: const EdgeInsets.all(4),
        child: Padding(
          padding: const EdgeInsets.all(16),
          child: isLoggedIn ? const LoggedIn() : const LoggedOut(),
        ),
      ),
    );
  }
}

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:gap/gap.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:result/result.dart';

import '/strings/strings.dart';
import '/state/settings.dart';
import '/state/secure_storage.dart';
import '/remote/neon/neon_login.dart';
import '/remote/util/handle_remote_exception.dart';
import '/classes/bat_optimization.dart';
import '/widgets/http_warning_dialog.dart';
import '/widgets/disable_bat_optimization_dialog.dart';
import '/util/build_text_selection_menu.dart';
import '/util/animated_dialog.dart';

import 'init_neon_dialog.dart';

class LoggedOut extends ConsumerStatefulWidget {
  const LoggedOut({super.key});

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _LoggedOutState();
}

class _LoggedOutState extends ConsumerState<LoggedOut> {
  final formKey = GlobalKey<FormState>();

  final baseUrlController = TextEditingController();
  final folderController = TextEditingController();

  bool oldValid = false;

  bool isCheckingServer = false;

  @override
  void initState() {
    super.initState();

    initControllers();
  }

  Future<void> initControllers() async {
    final neon = await ref.read(secureStorageDbProvider).readNextcloudRemote();

    baseUrlController.text = neon.baseUrl;
    folderController.text = neon.folder;

    if (baseUrlController.text != '' || folderController.text != '') {
      onChanged('');
    }
  }

  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context)!;
    final scheme = Theme.of(context).colorScheme;
    final text = Theme.of(context).textTheme;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Text(
          strings.nextcloud,
          style: text.titleLarge?.copyWith(color: scheme.onSurface),
        ),
        Form(
          key: formKey,
          child: Table(
            defaultVerticalAlignment: TableCellVerticalAlignment.middle,
            columnWidths: const {
              0: FlexColumnWidth(2),
              1: FlexColumnWidth(5),
            },
            children: <TableRow>[
              buildFormField(
                strings.ncBaseUrl,
                baseUrlController,
                hintText: 'https://example.com/nextcloud',
              ),
              buildFormField(
                strings.ncDirectory,
                folderController,
                isLast: true,
                isOptional: true,
                validator: (_) => null,
                onSubmit: (_) => (formKey.currentState?.validate() ?? false)
                    ? logIn()
                    : null,
                hintText: strings.forExample('Apps/${strings.appTitle}'),
              ),
            ],
          ),
        ),
        const Gap(16),
        FilledButton(
          onPressed:
              formKey.currentState?.validate() ?? oldValid ? logIn : null,
          style: FilledButton.styleFrom(
            minimumSize: const Size.fromHeight(40),
          ),
          child: isCheckingServer
              ? SizedBox(
                  height: 24,
                  width: 24,
                  child: CircularProgressIndicator(
                    strokeWidth: 2.5,
                    color: scheme.onPrimary,
                    backgroundColor: Colors.transparent,
                  ),
                )
              : Text(strings.logIn),
        ),
      ],
    );
  }

  TableRow buildFormField(
    String title,
    TextEditingController controller, {
    bool isLast = false,
    bool isOptional = false,
    bool isPassword = false,
    String? Function(String?)? validator,
    String? hintText,
    ValueChanged<String>? onSubmit,
  }) {
    final strings = Strings.of(context)!;
    final scheme = Theme.of(context).colorScheme;
    final text = Theme.of(context).textTheme;
    final titleStyle = text.bodyLarge?.copyWith(color: scheme.onSurface);
    final inputStyle = text.bodyMedium?.copyWith(color: scheme.onSurface);

    validator ??= this.validator;

    return TableRow(
      children: <Widget>[
        RichText(
          text: TextSpan(
            children: <TextSpan>[
              TextSpan(text: title, style: titleStyle),
              if (isOptional)
                TextSpan(
                  text: '\n${strings.optional}',
                  style: text.labelMedium?.copyWith(
                    color: scheme.onSurfaceVariant,
                    fontStyle: FontStyle.italic,
                  ),
                ),
            ],
          ),
        ),
        TextFormField(
          contextMenuBuilder: buildTextSelectionMenu,
          maxLines: 1,
          enabled: !isCheckingServer,
          controller: controller,
          validator: validator,
          onChanged: onChanged,
          keyboardType: TextInputType.url,
          obscureText: isPassword,
          autocorrect: true,
          autovalidateMode: AutovalidateMode.onUserInteraction,
          style: inputStyle,
          textInputAction: isLast ? TextInputAction.done : TextInputAction.next,
          onFieldSubmitted: onSubmit,
          decoration: InputDecoration(
            hintText: hintText,
            border: const UnderlineInputBorder(),
          ),
        ),
      ],
    );
  }

  String? validator(String? text) {
    final strings = Strings.of(context)!;

    if (text == null || text.isEmpty) return strings.mayNotBeEmpty;

    final uri = Uri.tryParse(text);
    if (uri == null) return strings.notAValidUrl;
    if (!uri.hasScheme) return strings.notAValidUrl;
    if (!uri.isAbsolute) return strings.notAValidUrl;
    if (uri.host.isEmpty) return strings.notAValidUrl;
    if (uri.hasQuery) return strings.notAValidUrl;

    return null;
  }

  void onChanged(String _) {
    if (formKey.currentState?.validate() != oldValid) {
      setState(() => oldValid = formKey.currentState!.validate());
    }
  }

  Future<void> logIn() async {
    if (isCheckingServer) return; // Avoid multiple log in screens.

    final strings = Strings.of(context)!;
    final settings = Settings.of(context);
    final messanger = ScaffoldMessenger.of(context);

    final folder =
        folderController.text.isEmpty ? null : folderController.text.trim();

    final neon = NeonLogin(
      context,
      baseUrlController.text.trim(),
      folder,
      settings.general.appLanguage.value.locale?.toLanguageTag(),
    );

    setState(() => isCheckingServer = true);

    if (!await InternetConnectionChecker().hasConnection) {
      setState(() => isCheckingServer = false);
      messanger.showSnackBar(
        SnackBar(
          duration: const Duration(seconds: 4),
          dismissDirection: DismissDirection.vertical,
          margin: const EdgeInsets.fromLTRB(24, 8, 24, 18),
          content: Text(strings.syncNoInternetConnection),
        ),
      );

      return;
    }

    if (!mounted) return;

    bat:
    if (await BatOptimization.anyEnabled.isTrue) {
      if (!mounted) return;

      final disable = await showAnimatedDialog<bool>(
        context: context,
        builder: (context) => const DisableBatOptimizationDialog(),
      ).isTrue;

      if (!disable) break bat;

      if (await BatOptimization.isEnabled.isTrue) {
        await BatOptimization.disableBatteryOptimizations();
      }

      if (await BatOptimization.isManufacturerEnabled.isTrue) {
        await BatOptimization.disableManufacturerOptimizations(
          title: strings.disable,
          description: strings.batteryOptimizationDescription,
        );
      }
    }

    if (!mounted) return;

    final baseUrl = baseUrlController.text.trim();
    final baseUri = Uri.parse(baseUrl);

    if (baseUri.isScheme('HTTP')) {
      final useHttp = await showAnimatedDialog<bool>(
        context: context,
        builder: (context) => const HttpWarningDialog(),
      );

      if (useHttp != true) {
        if (mounted) setState(() => isCheckingServer = false);

        return;
      }
    }

    final res = await neon.validate();

    if (!mounted) return;

    setState(() => isCheckingServer = false);

    switch (res) {
      case Err(error: final err):
        unawaited(handleRemoteException(context, err));
      case Ok(value: final validated):
        if (!validated) {
          messanger.showSnackBar(SnackBar(
            duration: const Duration(seconds: 4),
            dismissDirection: DismissDirection.vertical,
            margin: const EdgeInsets.fromLTRB(24, 8, 24, 18),
            content: Text(strings.syncNotNcServer),
          ));
          return;
        }

        await showAnimatedDialog(
          context: context,
          builder: (context) => InitNeonDialog(neon),
        );
    }
  }
}

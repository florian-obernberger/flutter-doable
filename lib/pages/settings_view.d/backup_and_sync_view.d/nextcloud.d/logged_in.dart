import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_material_design_icons/flutter_material_design_icons.dart';
import 'package:path/path.dart' as path;

import '/strings/strings.dart';
import '/state/settings.dart';
import '/state/secure_storage.dart';
import '/remote/neon/neon_remote.dart';
import '/widgets/nextcloud_avatar.dart';
import '/util/build_text_selection_menu.dart';
import '/util/animated_dialog.dart';
import '/util/extensions/string.dart';

import 'logout_dialog.dart';
import 'reload_neon_dialog.dart';

class LoggedIn extends ConsumerStatefulWidget {
  const LoggedIn({super.key});

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _LoggedInState();
}

class _LoggedInState extends ConsumerState<LoggedIn> {
  NeonRemote? neon;

  bool isChangingDir = false;
  bool isLoggingOut = false;
  late final TextEditingController dirController;

  @override
  void initState() {
    super.initState();

    initSecureStorage();
  }

  Future<void> initSecureStorage() async {
    neon = await ref.read(secureStorageDbProvider).readNextcloudRemote();
    dirController = TextEditingController(text: neon!.folder);
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context)!;
    final scheme = Theme.of(context).colorScheme;
    final text = Theme.of(context).textTheme;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Expanded(
              child: Text(
                strings.nextcloud,
                style: text.titleLarge?.copyWith(color: scheme.onSurface),
              ),
            ),
            IconButton(
              onPressed: isChangingDir || isLoggingOut
                  ? null
                  : () => setState(() => isChangingDir = true),
              tooltip: strings.changeNextcloudDirectory,
              color: scheme.onSurfaceVariant,
              icon: const Icon(MdiIcons.folderEditOutline),
            ),
          ],
        ),
        neon == null
            ? const SizedBox(
                height: 72,
                child: Center(child: CircularProgressIndicator()),
              )
            : buildUser(strings),
        OutlinedButton(
          onPressed: buttonPress(),
          style: OutlinedButton.styleFrom(
            minimumSize: const Size.fromHeight(40),
          ),
          child: buttonTitle(strings),
        ),
      ],
    );
  }

  void Function()? buttonPress() {
    if (isLoggingOut) return null;
    if (isChangingDir) return () => changeDirectory(dirController.text);
    return logout;
  }

  Widget buttonTitle(Strings strings) {
    if (isLoggingOut) return const CircularProgressIndicator();
    if (isChangingDir) return Text(strings.save);
    return Text(strings.logOut);
  }

  ListTile buildUser(Strings strings) {
    final scheme = Theme.of(context).colorScheme;
    final text = Theme.of(context).textTheme;

    final Widget directory;

    if (!isChangingDir) {
      directory = Text(
        path.join('~', neon!.folder.withTrailingSlash()),
        maxLines: 1,
        softWrap: false,
        overflow: TextOverflow.fade,
      );
    } else {
      directory = TextFormField(
        autofocus: true,
        contextMenuBuilder: buildTextSelectionMenu,
        maxLines: 1,
        controller: dirController,
        keyboardType: TextInputType.url,
        autocorrect: true,
        style: text.bodyMedium?.copyWith(color: scheme.onSurfaceVariant),
        textInputAction: TextInputAction.done,
        onFieldSubmitted: changeDirectory,
        decoration: InputDecoration.collapsed(
          hintText: strings.forExample('Apps/${strings.appTitle}'),
          border: const UnderlineInputBorder(),
        ),
      );
    }

    return ListTile(
      minLeadingWidth: 40,
      title: Text(formatUser(neon!)),
      subtitle: directory,
      leading: const NextcloudAvatar(imageSize: 40, iconSize: 36),
      contentPadding: EdgeInsets.zero,
    );
  }

  Future<void> changeDirectory(String value) async {
    final folder = value.trim();
    if (neon!.folder == folder) return setState(() => isChangingDir = false);

    await showAnimatedDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) => ReloadNeonDialog(folder),
    ).then((_) => mounted ? setState(() => isChangingDir = false) : null);
  }

  Future<void> logout() async {
    if (isLoggingOut) return;

    final shouldLogout = await showAnimatedDialog<bool>(
      context: context,
      builder: (context) => const LogoutDialog(),
    );

    if (shouldLogout != true || !mounted) return;
    setState(() => isLoggingOut = true);
    final settings = Settings.of(context);

    await ref.read(secureStorageDbProvider).deleteNextcloud();
    await neon?.logout();
    await NextcloudAvatar.clearCache(ref);

    settings.backup.nextcloud.value = false;
    setState(() => isLoggingOut = false);
  }

  String formatUser(NeonRemote neon) {
    String url = neon.baseUrl;
    if (url.startsWith('https://')) url = url.replaceFirst('https://', '');
    if (url.startsWith('http://')) url = url.replaceFirst('http://', '');

    return '${neon.user}@$url';
  }
}

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:result/anyhow.dart';

import '/strings/strings.dart';
import '/state/settings.dart';
import '/state/secure_storage.dart';
import '/state/todo_db.dart';
import '/state/todo_list_db.dart';
import '/remote/neon/neon_login.dart';
import '/remote/neon/neon_credentials.dart';
import '/remote/neon/neon_remote.dart';
import '/remote/util/handle_remote_exception.dart';
import '/widgets/simple_loading_dialog.dart';

enum _InitState { login, init, cancelled }

class InitNeonDialog extends ConsumerStatefulWidget {
  const InitNeonDialog(this.neon, {super.key});

  final NeonLogin neon;

  @override
  ConsumerState<ConsumerStatefulWidget> createState() =>
      _InitRemoteDialogState();
}

class _InitRemoteDialogState extends ConsumerState<InitNeonDialog> {
  final state = ValueNotifier(_InitState.login);

  Settings? _settings;
  Settings get settings => _settings!;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    _settings ??= Settings.of(context);

    login();
  }

  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context)!;

    return ValueListenableBuilder(
        valueListenable: state,
        builder: (context, stateVal, _) => PopScope(
              canPop: false,
              onPopInvoked: (didPop) {
                if (stateVal == _InitState.login) {
                  widget.neon.cancel();
                  state.value = _InitState.cancelled;
                }
              },
              child: ValueListenableBuilder(
                valueListenable: state,
                builder: (context, state, _) =>
                    SimpleLoadingDialog(switch (state) {
                  _InitState.login => strings.loggingNextcloudIn,
                  _InitState.init => strings.initializingNextcloud,
                  _InitState.cancelled => strings.cancelling,
                }),
              ),
            ));
  }

  Future<void> login() async {
    final login = await widget.neon.login();
    if (state.value == _InitState.cancelled && mounted) return context.pop();

    switch (login) {
      case Ok(value: final credentials):
        return initialize(credentials);
      case Err(error: final err):
        if (mounted) {
          unawaited(handleRemoteException(context, err));
          context.pop();
        }
    }
  }

  Future<void> initialize(NeonCredentials credentials) async {
    final strings = Strings.of(context)!;
    final messanger = ScaffoldMessenger.of(context);

    final secureStorage = ref.read(secureStorageDbProvider);
    final todoDb = ref.read(todoDatabaseProvider);
    final listDb = ref.read(todoListDatabaseProvider);

    state.value = _InitState.init;

    await Future.wait([todoDb.init(), listDb.init()]);

    final neon = NeonRemote.fromCredentials(credentials);

    final init =
        await neon.initialize().context('Could not initialize Nextcloud');
    switch (init) {
      case Ok():
        await secureStorage.writeNextcloud(
          baseUrl: credentials.baseUrl,
          user: credentials.user,
          password: credentials.password,
          folder: credentials.folder,
          language: credentials.language,
        );
        settings.backup.nextcloud.value = true;
        settings.miscellaneous.migratedToRemotes.value = true;

        final synced = await neon.sync(todoDb.values(), listDb.values());

        switch (synced) {
          case Ok():
            messanger.showSnackBar(SnackBar(
              duration: const Duration(seconds: 4),
              dismissDirection: DismissDirection.vertical,
              margin: const EdgeInsets.fromLTRB(24, 8, 24, 18),
              content: Text(strings.syncSuccessful),
            ));
          case Err(error: final err):
            if (mounted) {
              unawaited(handleRemoteException(context, err, remote: neon));
            }
        }
      case Err(error: final err):
        if (mounted) unawaited(handleRemoteException(context, err));
        await secureStorage.deleteNextcloud();
        settings.backup.nextcloud.value = false;
        unawaited(neon.logout());
    }

    if (mounted) context.pop();
  }
}

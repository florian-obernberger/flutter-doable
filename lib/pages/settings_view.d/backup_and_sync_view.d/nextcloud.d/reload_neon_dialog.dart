import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:result/anyhow.dart';

import '/strings/strings.dart';
import '/state/secure_storage.dart';
import '/state/todo_db.dart';
import '/state/todo_list_db.dart';
import '/remote/util/handle_remote_exception.dart';
import '/widgets/simple_loading_dialog.dart';

class ReloadNeonDialog extends ConsumerStatefulWidget {
  const ReloadNeonDialog(this.folder, {this.message, super.key});

  final String? folder;
  final String? message;

  @override
  ConsumerState<ConsumerStatefulWidget> createState() =>
      _ReloadRemoteDialogState();
}

class _ReloadRemoteDialogState extends ConsumerState<ReloadNeonDialog> {
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    initialize();
  }

  @override
  Widget build(BuildContext context) => PopScope(
        canPop: false,
        child: SimpleLoadingDialog(
          widget.message ?? Strings.of(context)!.initializingNextcloud,
        ),
      );

  Future<void> initialize() async {
    final strings = Strings.of(context)!;
    final messanger = ScaffoldMessenger.of(context);

    final secureStorage = ref.read(secureStorageDbProvider);
    final todoDb = ref.read(todoDatabaseProvider);
    final listDb = ref.read(todoListDatabaseProvider);

    if (widget.folder != null) {
      await secureStorage.updateNextcloudFolder(widget.folder!);
    }

    final neon = await secureStorage.readNextcloudRemote();

    final init = await neon.initialize();
    switch (init) {
      case Ok():
        final synced = await neon.sync(todoDb.values(), listDb.values());

        switch (synced) {
          case Ok():
            messanger.showSnackBar(SnackBar(
              duration: const Duration(seconds: 4),
              dismissDirection: DismissDirection.vertical,
              margin: const EdgeInsets.fromLTRB(24, 8, 24, 18),
              content: Text(strings.syncSuccessful),
            ));
          case Err(error: final err):
            if (mounted) {
              unawaited(handleRemoteException(context, err, remote: neon));
            }
        }
      case Err(error: final err):
        if (mounted) unawaited(handleRemoteException(context, err));
    }

    if (mounted) context.pop();
  }
}

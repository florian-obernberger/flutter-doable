import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:result/anyhow.dart';

import '/strings/strings.dart';
import '/state/settings.dart';
import '/state/secure_storage.dart';
import '/state/todo_db.dart';
import '/state/todo_list_db.dart';
import '/remote/types.dart';
import '/remote/neon/neon_remote.dart';
import '/remote/util/handle_remote_exception.dart';
import '/classes/logger.dart';
import '/sync/base_sync.dart';
import '/sync/nextcloud_sync.dart';
import '/widgets/simple_loading_dialog.dart';

@Deprecated('Transition to Remote')
class InitSyncDialog extends ConsumerStatefulWidget {
  const InitSyncDialog(this.nc, {super.key});

  final NextcloudSync nc;

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _InitSyncDialogState();
}

@Deprecated('Transition to Remote')
class _InitSyncDialogState extends ConsumerState<InitSyncDialog> {
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    synchronize();
  }

  Future<void> synchronize() async {
    final messanger = ScaffoldMessenger.of(context);
    final strings = Strings.of(context)!;

    unawaited(initSync(widget.nc).then<void>((initRes) {
      if (mounted) Navigator.of(context).pop();

      switch (initRes) {
        case BaseErr(:final error):
          final (err, remote) = error;
          if (mounted) handleRemoteException(context, err, remote: remote);
        case BaseOk():
          messanger.showSnackBar(
            SnackBar(
              duration: const Duration(seconds: 4),
              dismissDirection: DismissDirection.vertical,
              margin: const EdgeInsets.fromLTRB(24, 8, 24, 18),
              content: Text(strings.syncSuccessful),
            ),
          );
          ref.read(secureStorageDbProvider).writeNextcloudSync(widget.nc);
      }
    }));
  }

  BaseFutureResult<(), (Error, NeonRemote)> initSync(
    NextcloudSync nc,
  ) async {
    await ref.read(secureStorageDbProvider).writeNextcloudSync(nc);
    if (mounted) Settings.of(context).backup.nextcloud.value = true;

    final todoDb = ref.read(todoDatabaseProvider);
    final listDb = ref.read(todoListDatabaseProvider);

    await todoDb.init();
    await listDb.init();

    await nc
        .initSync()
        .map((_) => Sync.unchanged)
        .andThen((_) => todoDb.syncTodos(forceSync: true))
        .andThen((_) => listDb.syncLists(forceSync: true));

    final remote =
        await ref.read(secureStorageDbProvider).readNextcloudRemote();

    final initRes = await remote.initialize();
    if (initRes case Err(:final error)) return BaseErr((error, remote));

    return remote
        .sync(todoDb.values(), listDb.values())
        .inspectErr((error) {
          final (level, message) = error
              .rootCause()
              .downcast<RemoteException>()
              .map((exception) => (exception.logLevel, exception.logMessage))
              .unwrapOr((LogLevel.error, 'Error'));

          logger.log(level, message: message, error: error);
        })
        .mapErr((err) => (err, remote))
        .map((_) => const ());
  }

  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context)!;

    return SimpleLoadingDialog(strings.initializingNextcloud);
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_material_design_icons/flutter_material_design_icons.dart';
import 'package:path/path.dart' as path;

import '/strings/strings.dart';
import '/state/settings.dart';
import '/state/secure_storage.dart';
import '/remote/webdav/webdav_remote.dart';
import '/util/build_text_selection_menu.dart';
import '/util/animated_dialog.dart';
import '/util/extensions/string.dart';

import 'logout_dialog.dart';
import 'init_webdav_dialog.dart';

class LoggedIn extends ConsumerStatefulWidget {
  const LoggedIn({super.key});

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _LoggedInState();
}

class _LoggedInState extends ConsumerState<LoggedIn> {
  WebdavRemote? wd;

  bool isChangingDir = false;
  late final TextEditingController dirController;

  @override
  void initState() {
    super.initState();

    initSecureStorage();
  }

  Future<void> initSecureStorage({bool initial = true}) async {
    wd = await ref.read(secureStorageDbProvider).readWebdavRemote();
    if (initial) dirController = TextEditingController(text: wd!.folder);
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context)!;
    final scheme = Theme.of(context).colorScheme;
    final text = Theme.of(context).textTheme;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Expanded(
              child: Text(
                strings.webdav,
                style: text.titleLarge?.copyWith(color: scheme.onSurface),
              ),
            ),
            IconButton(
              onPressed: isChangingDir
                  ? null
                  : () => setState(() => isChangingDir = true),
              tooltip: strings.changeNextcloudDirectory,
              color: scheme.onSurfaceVariant,
              icon: const Icon(MdiIcons.folderEditOutline),
            ),
          ],
        ),
        wd == null
            ? const SizedBox(
                height: 72,
                child: Center(child: CircularProgressIndicator()),
              )
            : buildUser(strings),
        OutlinedButton(
          onPressed: isChangingDir
              ? () => changeDirectory(dirController.text)
              : logout,
          style: OutlinedButton.styleFrom(
            minimumSize: const Size.fromHeight(40),
          ),
          child: Text(isChangingDir ? strings.save : strings.logOut),
        ),
      ],
    );
  }

  ListTile buildUser(Strings strings) {
    final scheme = Theme.of(context).colorScheme;
    final text = Theme.of(context).textTheme;

    final Widget directory;

    if (!isChangingDir) {
      directory = Text(
        path.join('~', wd!.folder.withTrailingSlash()),
        maxLines: 1,
        softWrap: false,
        overflow: TextOverflow.fade,
      );
    } else {
      directory = TextFormField(
        autofocus: true,
        contextMenuBuilder: buildTextSelectionMenu,
        maxLines: 1,
        controller: dirController,
        keyboardType: TextInputType.url,
        autocorrect: true,
        style: text.bodyMedium?.copyWith(color: scheme.onSurfaceVariant),
        textInputAction: TextInputAction.done,
        onFieldSubmitted: changeDirectory,
        decoration: InputDecoration.collapsed(
          hintText: strings.forExample('Apps/${strings.appTitle}'),
          border: const UnderlineInputBorder(),
        ),
      );
    }

    return ListTile(
      title: Text(
        formatUser(wd!),
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
      ),
      subtitle: directory,
      contentPadding: EdgeInsets.zero,
    );
  }

  String formatUser(WebdavRemote wd) {
    String url = wd.baseUrl;
    if (url.startsWith('https://')) url = url.replaceFirst('https://', '');
    if (url.startsWith('http://')) url = url.replaceFirst('http://', '');

    return '${wd.user}@$url';
  }

  Future<void> changeDirectory(String value) async {
    final folder = value.trim();
    if (wd!.folder == folder) return setState(() => isChangingDir = false);

    await showAnimatedDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) => InitWebdavDialog(
        wd!,
        type: WebdavInitType.folderChange,
        folder: folder,
      ),
    ).then((_) {
      if (!mounted) return;
      setState(() => isChangingDir = false);
      initSecureStorage(initial: false);
    });
  }

  Future<void> logout() async {
    final shouldLogout = await showAnimatedDialog<bool>(
      context: context,
      builder: (context) => const LogoutDialog(),
    );

    if (shouldLogout != true || !mounted) return;
    final settings = Settings.of(context);

    await ref.read(secureStorageDbProvider).deleteWebdav();

    settings.backup.webdav.value = false;
  }
}

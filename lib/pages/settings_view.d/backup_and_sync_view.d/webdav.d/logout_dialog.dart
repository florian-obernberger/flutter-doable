import 'package:flutter/material.dart';

import '/strings/strings.dart';

class LogoutDialog extends StatelessWidget {
  const LogoutDialog({super.key});

  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context)!;

    return AlertDialog(
      title: Text(strings.logOut),
      content: Text(strings.webdavLogOutMessage),
      actions: [
        TextButton(
          onPressed: Navigator.of(context).pop,
          child: Text(strings.cancel),
        ),
        TextButton(
          onPressed: () => Navigator.of(context).pop(true),
          child: Text(strings.logOut),
        ),
      ],
    );
  }
}

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:gap/gap.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:result/result.dart';

import '/strings/strings.dart';
import '/state/secure_storage.dart';
import '/remote/webdav/webdav_remote.dart';
import '/remote/util/handle_remote_exception.dart';
import '/widgets/http_warning_dialog.dart';
import '/util/animated_dialog.dart';
import '/util/build_text_selection_menu.dart';

import 'init_webdav_dialog.dart';

class LoggedOut extends ConsumerStatefulWidget {
  const LoggedOut({super.key});

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _LoggedOutState();
}

class _LoggedOutState extends ConsumerState<LoggedOut> {
  final formKey = GlobalKey<FormState>();

  final baseUrlController = TextEditingController();
  final folderController = TextEditingController();
  final userController = TextEditingController();
  final passwordController = TextEditingController();

  bool oldValid = false;

  bool isValidatingWebdav = false;

  @override
  void initState() {
    super.initState();

    initControllers();
  }

  Future<void> initControllers() async {
    final wd = await ref.read(secureStorageDbProvider).readWebdavRemote();

    baseUrlController.text = wd.baseUrl;
    folderController.text = wd.folder;
    userController.text = wd.user;
    passwordController.text = wd.password;

    if (baseUrlController.text.isNotEmpty ||
        folderController.text.isNotEmpty ||
        userController.text.isNotEmpty ||
        passwordController.text.isNotEmpty) {
      onChanged('');
    }
  }

  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context)!;
    final scheme = Theme.of(context).colorScheme;
    final text = Theme.of(context).textTheme;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Text(
          strings.webdav,
          style: text.titleLarge?.copyWith(color: scheme.onSurface),
        ),
        Form(
          key: formKey,
          child: Table(
            defaultVerticalAlignment: TableCellVerticalAlignment.middle,
            columnWidths: const {
              0: FlexColumnWidth(2),
              1: FlexColumnWidth(5),
            },
            children: <TableRow>[
              buildFormField(
                strings.ncBaseUrl,
                baseUrlController,
                validator: urlValidator,
                hintText: strings.webdavUrlHint,
                keyboardType: TextInputType.url,
              ),
              buildFormField(
                strings.ncDirectory,
                folderController,
                isOptional: true,
                validator: (_) => null,
                hintText: strings.forExample('Apps/${strings.appTitle}'),
                keyboardType: TextInputType.url,
              ),
              buildFormField(
                strings.ncUser,
                userController,
                hintText: strings.ncUser,
              ),
              buildFormField(
                strings.ncPassword,
                passwordController,
                isLast: true,
                isPassword: true,
                onSubmit: (_) => (formKey.currentState?.validate() ?? false)
                    ? logIn()
                    : null,
                hintText: strings.ncPassword,
              ),
            ],
          ),
        ),
        const Gap(16),
        FilledButton(
          onPressed:
              formKey.currentState?.validate() ?? oldValid ? logIn : null,
          style: FilledButton.styleFrom(
            minimumSize: const Size.fromHeight(40),
          ),
          child: isValidatingWebdav
              ? SizedBox(
                  height: 24,
                  width: 24,
                  child: CircularProgressIndicator(
                    strokeWidth: 2.5,
                    color: scheme.onPrimary,
                    backgroundColor: Colors.transparent,
                  ),
                )
              : Text(strings.logIn),
        ),
      ],
    );
  }

  TableRow buildFormField(
    String title,
    TextEditingController controller, {
    bool isLast = false,
    bool isOptional = false,
    bool isPassword = false,
    String? Function(String?)? validator,
    String? hintText,
    TextInputType? keyboardType,
    ValueChanged<String>? onSubmit,
  }) {
    final strings = Strings.of(context)!;
    final scheme = Theme.of(context).colorScheme;
    final text = Theme.of(context).textTheme;
    final titleStyle = text.bodyLarge?.copyWith(color: scheme.onSurface);
    final inputStyle = text.bodyMedium?.copyWith(color: scheme.onSurface);

    validator ??= this.validator;

    return TableRow(
      children: <Widget>[
        RichText(
          text: TextSpan(
            children: <TextSpan>[
              TextSpan(text: title, style: titleStyle),
              if (isOptional)
                TextSpan(
                  text: '\n${strings.optional}',
                  style: text.labelMedium?.copyWith(
                    color: scheme.onSurfaceVariant,
                    fontStyle: FontStyle.italic,
                  ),
                ),
            ],
          ),
        ),
        TextFormField(
          contextMenuBuilder: buildTextSelectionMenu,
          maxLines: 1,
          enabled: !isValidatingWebdav,
          controller: controller,
          validator: validator,
          onChanged: onChanged,
          keyboardType: keyboardType ?? TextInputType.text,
          obscureText: isPassword,
          autocorrect: true,
          autovalidateMode: AutovalidateMode.onUserInteraction,
          style: inputStyle,
          textInputAction: isLast ? TextInputAction.done : TextInputAction.next,
          onFieldSubmitted: onSubmit,
          decoration: InputDecoration(
            hintText: hintText,
            border: const UnderlineInputBorder(),
          ),
        ),
      ],
    );
  }

  String? urlValidator(String? text) {
    final strings = Strings.of(context)!;

    if (text == null || text.isEmpty) return strings.mayNotBeEmpty;

    final uri = Uri.tryParse(text);
    if (uri == null) return strings.notAValidUrl;
    if (!uri.hasScheme) return strings.notAValidUrl;
    if (!uri.isAbsolute) return strings.notAValidUrl;
    if (uri.host.isEmpty) return strings.notAValidUrl;
    if (uri.hasQuery) return strings.notAValidUrl;

    return null;
  }

  String? validator(String? text) {
    final strings = Strings.of(context)!;

    if (text == null || text.isEmpty) return strings.mayNotBeEmpty;
    return null;
  }

  void onChanged(String _) {
    if (formKey.currentState?.validate() != oldValid) {
      setState(() => oldValid = formKey.currentState!.validate());
    }
  }

  Future<void> logIn() async {
    if (isValidatingWebdav) return; // Avoid multiple log in screens.

    final messanger = ScaffoldMessenger.of(context);
    final strings = Strings.of(context)!;

    setState(() => isValidatingWebdav = true);

    if (!await InternetConnectionChecker().hasConnection) {
      setState(() => isValidatingWebdav = false);
      messanger.showSnackBar(
        SnackBar(
          duration: const Duration(seconds: 4),
          dismissDirection: DismissDirection.vertical,
          margin: const EdgeInsets.fromLTRB(24, 8, 24, 18),
          content: Text(strings.syncNoInternetConnection),
        ),
      );

      return;
    }

    if (!mounted) return;

    final baseUrl = baseUrlController.text.trim();
    final baseUri = Uri.parse(baseUrl);

    if (baseUri.isScheme('HTTP')) {
      final useHttp = await showAnimatedDialog<bool>(
        context: context,
        builder: (context) => const HttpWarningDialog(),
      );

      if (useHttp != true) {
        if (mounted) setState(() => isValidatingWebdav = false);

        return;
      }
    }

    final folder =
        folderController.text.isEmpty ? null : folderController.text.trim();

    final wd = WebdavRemote(
      baseUrl,
      folder: folder,
      user: userController.text.trim(),
      password: passwordController.text.trim(),
    );

    final res = await wd.validate();

    if (!mounted) return;

    switch (res) {
      case Err(error: final err):
        unawaited(handleRemoteException(context, err));
        setState(() => isValidatingWebdav = false);

        return;
      case Ok(value: final validated):
        if (!validated) {
          messanger.showSnackBar(SnackBar(
            duration: const Duration(seconds: 4),
            dismissDirection: DismissDirection.vertical,
            margin: const EdgeInsets.fromLTRB(24, 8, 24, 18),
            content: Text(strings.syncWrongUserOrPassword),
          ));
          setState(() => isValidatingWebdav = false);

          return;
        }
    }

    await showAnimatedDialog(
      context: context,
      builder: (context) => InitWebdavDialog(wd),
    );

    if (mounted) setState(() => isValidatingWebdav = false);
  }
}

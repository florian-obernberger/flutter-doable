import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:result/anyhow.dart';

import '/strings/strings.dart';
import '/state/settings.dart';
import '/state/secure_storage.dart';
import '/state/todo_db.dart';
import '/state/todo_list_db.dart';
import '/remote/webdav/webdav_remote.dart';
import '/remote/util/handle_remote_exception.dart';
import '/widgets/simple_loading_dialog.dart';

enum WebdavInitType { init, folderChange }

class InitWebdavDialog extends ConsumerStatefulWidget {
  const InitWebdavDialog(
    this.wd, {
    this.folder,
    this.type = WebdavInitType.init,
    this.message,
    super.key,
  }) : assert((type == WebdavInitType.folderChange && folder != null) ||
            type == WebdavInitType.init);

  final WebdavRemote? wd;
  final String? folder;
  final WebdavInitType type;
  final String? message;

  @override
  ConsumerState<ConsumerStatefulWidget> createState() =>
      _InitWebdavDialogState();
}

class _InitWebdavDialogState extends ConsumerState<InitWebdavDialog> {
  late WebdavRemote? _wd = widget.wd;
  WebdavRemote get wd => _wd!;

  Settings? _settings;
  Settings get settings => _settings!;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    _settings ??= Settings.of(context);

    initialize();
  }

  @override
  Widget build(BuildContext context) => PopScope(
        canPop: false,
        child: SimpleLoadingDialog(
          widget.message ?? Strings.of(context)!.initializingWebdav,
        ),
      );

  Future<void> initialize() async {
    final secureStorage = ref.read(secureStorageDbProvider);

    if (widget.type == WebdavInitType.folderChange) {
      await secureStorage.updateWebdavFolder(widget.folder!);
      _wd = await secureStorage.readWebdavRemote();
    } else {
      _wd ??= await secureStorage.readWebdavRemote();
    }

    await _initialize();
  }

  Future<void> _initialize() async {
    final strings = Strings.of(context)!;
    final messanger = ScaffoldMessenger.of(context);

    final secureStorage = ref.read(secureStorageDbProvider);
    final todoDb = ref.read(todoDatabaseProvider);
    final listDb = ref.read(todoListDatabaseProvider);

    final init = await wd.initialize();

    switch (init) {
      case Ok():
        await secureStorage.writeWebdav(
          baseUrl: wd.baseUrl,
          folder: wd.folder,
          user: wd.user,
          password: wd.password,
        );
        settings.backup.webdav.value = true;

        final synced = await wd.sync(todoDb.values(), listDb.values());

        switch (synced) {
          case Ok():
            messanger.showSnackBar(SnackBar(
              duration: const Duration(seconds: 4),
              dismissDirection: DismissDirection.vertical,
              margin: const EdgeInsets.fromLTRB(24, 8, 24, 18),
              content: Text(strings.syncSuccessful),
            ));
          case Err(:final error):
            if (mounted) {
              unawaited(handleRemoteException(context, error, remote: wd));
            }
        }
      case Err(:final error):
        if (mounted) unawaited(handleRemoteException(context, error));
        await secureStorage
            .deleteWebdav()
            .then((_) => settings.backup.webdav.value = false);
    }

    if (mounted) context.pop();
  }
}

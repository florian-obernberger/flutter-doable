import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:gap/gap.dart';
import 'package:pick_or_save/pick_or_save.dart';

import '/classes/logger.dart';
import '/state/settings.dart';
import '/strings/strings.dart';
import '/util/formatters/format_duration.dart';
import '/widgets/duration_picker.dart';

class AutoExport extends ConsumerWidget {
  const AutoExport({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    const buttonSize = Size.fromHeight(40);
    return ValueListenableBuilder(
      valueListenable: Settings.of(context).backup.autoExportPath,
      builder: (context, path, _) => AnimatedCrossFade(
        firstChild: const _ChooseDirectory(buttonSize),
        secondChild: _ExportSettings(buttonSize, path ?? 'Oops a Daisy'),
        crossFadeState:
            path == null ? CrossFadeState.showFirst : CrossFadeState.showSecond,
        duration: Durations.short4,
      ),
    );
  }
}

class _ExportSettings extends ConsumerWidget {
  const _ExportSettings(this.buttonSize, this.path);

  final Size buttonSize;
  final String path;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final settings = Settings.of(context);
    final strings = Strings.of(context)!;
    final scheme = Theme.of(context).colorScheme;
    final text = Theme.of(context).textTheme;

    return Card(
      margin: const EdgeInsets.all(4),
      child: Padding(
        padding: const EdgeInsets.all(16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(
              strings.autoExport,
              style: text.titleLarge?.copyWith(color: scheme.onSurface),
            ),
            const Gap(8),
            ValueListenableBuilder(
              valueListenable: settings.backup.autoExportDuration,
              builder: (context, duration, directory) => ListTile(
                contentPadding: EdgeInsets.zero,
                title: directory,
                subtitle: Text(
                  strings.exportEvery(formatDuration(
                    context,
                    duration,
                    applyGermanFix: false,
                  )),
                ),
              ),
              child: buildDirectory(path),
            ),
            Row(
              children: <Widget>[
                Expanded(
                  child: OutlinedButton(
                    style: OutlinedButton.styleFrom(minimumSize: buttonSize),
                    onPressed: () async {
                      try {
                        await PickOrSave().uriPermissionStatus(
                          params: UriPermissionStatusParams(
                            uri: settings.backup.autoExportPath.value!,
                            releasePermission: true,
                          ),
                        );
                      } on Exception catch (err, stack) {
                        logger.e(
                          err,
                          stackTrace: stack,
                          message: "Couldn't release permission",
                          tag: 'Backup',
                        );
                      }
                      settings.backup.autoExportPath.value = null;
                      settings.backup.autoExportDuration.value =
                          settings.backup.autoExportDuration.initialValue;
                    },
                    child: Text(
                      strings.forgetDirectory,
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
                const Gap(8),
                Expanded(
                  child: FilledButton.tonal(
                    style: FilledButton.styleFrom(minimumSize: buttonSize),
                    onPressed: () => showAnimatedDurationPicker(
                      context: context,
                      title: strings.changeDuration,
                      parts: [
                        DurationPart.day,
                        DurationPart.hour,
                        DurationPart.minute,
                      ],
                      initialDuration: settings.backup.autoExportDuration.value,
                    ).then<void>(
                      (duration) {
                        if (duration == null) return;
                        settings.backup.autoExportDuration.value = duration;
                      },
                      onError: (error) => logger.e(
                        error,
                        message: 'Could not pick duration',
                        tag: 'Backup',
                      ),
                    ),
                    child: Text(
                      strings.changeDuration,
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget buildDirectory(String directory) {
    final formatted = Uri.decodeFull(
      Uri.parse(directory).path,
    ).split('/tree/primary:').last;

    return Text('~/$formatted/');
  }
}

class _ChooseDirectory extends StatelessWidget {
  const _ChooseDirectory(this.buttonSize);

  final Size buttonSize;

  @override
  Widget build(BuildContext context) {
    final settings = Settings.of(context);
    final strings = Strings.of(context)!;
    final scheme = Theme.of(context).colorScheme;
    final text = Theme.of(context).textTheme;

    return Card(
      margin: const EdgeInsets.all(4),
      child: Padding(
        padding: const EdgeInsets.all(16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(
              strings.autoExport,
              style: text.titleLarge?.copyWith(color: scheme.onSurface),
            ),
            const Gap(4),
            Text(
              strings.autoExportDescription,
              style: text.bodyMedium?.copyWith(
                color: scheme.onSurfaceVariant,
              ),
            ),
            const Gap(8),
            FilledButton(
              style: FilledButton.styleFrom(minimumSize: buttonSize),
              onPressed: () {
                PickOrSave().directoryPicker().then<void>(
                  (dir) {
                    if (dir != null) settings.backup.autoExportPath.value = dir;
                  },
                  onError: (error) => logger.e(
                    error,
                    message: 'Could not choose directory',
                    tag: 'Backup',
                  ),
                );
              },
              child: Text(
                strings.chooseDirectory,
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';

import '/state/settings.dart';

import 'nextcloud.d/logged_out.dart';
import 'nextcloud.d/logged_in.dart';

class NextcloudStorage extends StatelessWidget {
  const NextcloudStorage({super.key});

  @override
  Widget build(BuildContext context) {
    final settings = Settings.of(context);

    return ValueListenableBuilder(
      valueListenable: settings.backup.nextcloud,
      builder: (context, isLoggedIn, _) => Card(
        margin: const EdgeInsets.all(4),
        child: Padding(
          padding: const EdgeInsets.all(16),
          child: isLoggedIn ? const LoggedIn() : const LoggedOut(),
        ),
      ),
    );
  }
}

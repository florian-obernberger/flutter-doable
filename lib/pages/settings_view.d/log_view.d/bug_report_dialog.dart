import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:gap/gap.dart';
import 'package:path_provider/path_provider.dart';
import 'package:share_plus/share_plus.dart';

import '/strings/strings.dart';
import '/util/extensions/iterable.dart';
import '/classes/bug_report.dart';
import '/classes/feedback.dart';
import '/classes/logger.dart';
import '/types/date_and_time_formats.dart';
import '/data/applications_directory.dart';
import '/widgets/markdown_text.dart';

class BugReportDialog extends ConsumerStatefulWidget {
  const BugReportDialog({super.key});

  @override
  ConsumerState<BugReportDialog> createState() => _BugReportDialogState();
}

class _BugReportDialogState extends ConsumerState<BugReportDialog> {
  final now = DateTime.now();
  Set<FeedbackExtra> extras = FeedbackExtra.values.toSet();

  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context)!;

    return AlertDialog(
      title: Text(strings.bugReport),
      contentPadding: const EdgeInsets.only(top: 16, bottom: 20),
      content: ConstrainedBox(
        constraints: const BoxConstraints(minWidth: 280, maxWidth: 560),
        child: SingleChildScrollView(
          physics: const ClampingScrollPhysics(),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 24, bottom: 16, right: 24),
                child: MarkdownText(
                  strings.bugReportSupportingText,
                  maxLines: 10,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
              const Gap(16),
              for (final (pos, extra) in FeedbackExtra.values.enumerate()) ...[
                if (pos != 0) const Divider(indent: 24, endIndent: 24),
                ListTile(
                  contentPadding: const EdgeInsets.fromLTRB(24, 0, 16, 0),
                  title: Text(switch (extra) {
                    FeedbackExtra.systemInfo => strings.systemInfo,
                    FeedbackExtra.logOutputs => strings.logOutputs,
                    FeedbackExtra.settings => strings.settings,
                  }),
                  onTap: () => setState(() => extras.contains(extra)
                      ? extras.remove(extra)
                      : extras.add(extra)),
                  trailing: Checkbox(
                    value: extras.contains(extra),
                    onChanged: (value) => setState(() => (value ?? false)
                        ? extras.add(extra)
                        : extras.remove(extra)),
                  ),
                ),
              ],
            ],
          ),
        ),
      ),
      actions: <Widget>[
        TextButton(
          onPressed: Navigator.of(context).pop,
          child: Text(strings.cancel),
        ),
        FilledButton(
          onPressed: copyBugReport,
          child: Text(strings.shareBugReport),
        ),
      ],
    );
  }

  Future<void> copyBugReport() async {
    final date = DateFormat.international.formatWithTime(
      now,
      TimeFormat.military,
    );

    final report = await BugReport.fromContext(
      context,
      title: 'Bug report from $date',
      includeLogs: extras.contains(FeedbackExtra.logOutputs),
      includeSettings: extras.contains(FeedbackExtra.settings),
      includeSystemInfo: extras.contains(FeedbackExtra.systemInfo),
    );

    if (mounted) Navigator.of(context).pop();

    final file = (await getTemporaryDirectory()).join('${report.title!}.json');
    await file.create();
    await file.writeAsBytes(report.toBytes());

    try {
      await Share.shareXFiles(
        [
          XFile(
            file.path,
            mimeType: 'application/json',
            name: '${report.title}.json',
            lastModified: now,
          )
        ],
        subject: report.title,
      );
    } on Exception catch (err, stack) {
      logger.e(err, stackTrace: stack, message: 'Could not share bug report');
    } finally {
      await file.delete();
    }
  }
}

import '/data/logger.dart';

sealed class EntryOrDay {
  const EntryOrDay(this.id);

  final String id;
}

class Entry implements EntryOrDay {
  const Entry(this.log);

  final LogEntry log;
  @override
  String get id => log.id;
}

class Day implements EntryOrDay {
  const Day(this.day);

  factory Day.now() => Day(DateTime.now());

  final DateTime day;
  @override
  String get id => day.toIso8601String();
}

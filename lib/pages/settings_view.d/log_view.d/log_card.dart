import 'package:flutter/material.dart';
import 'package:material_symbols_icons/symbols.dart';
import 'package:share_plus/share_plus.dart';
import 'package:flutter_material_design_icons/flutter_material_design_icons.dart';

import '/strings/strings.dart';
import '/theme/rainbow_colors.dart';
import '/state/settings.dart';
import '/classes/motion.dart';
import '/util/copy_text.dart';
import '/util/formatters/format_date.dart';
import '/util/group_shape.dart';
import '/widgets/mini_chip.dart';
import '/widgets/circle.dart';
import '/data/logger.dart';

enum LogCardPosition { start, middle, end, single }

class LogCard extends StatefulWidget {
  const LogCard({
    required this.entry,
    required this.delete,
    this.position = LogCardPosition.single,
    super.key,
  });

  final LogEntry entry;
  final VoidCallback delete;

  final LogCardPosition position;

  @override
  State<LogCard> createState() => _LogCardState();
}

class _LogCardState extends State<LogCard> with SingleTickerProviderStateMixin {
  late final String message = widget.entry.message!;
  late final LogLevel level = widget.entry.level;
  late final String? error = widget.entry.error;
  late final String? tag = widget.entry.tag;
  late final DateTime logTime = widget.entry.logTime;

  bool get hasError => error != null;
  bool isExpanded = false;

  late final Animation<double> expansionAnimation;
  late final Animation<double> fadeAnimation;
  late final AnimationController _controller;
  late final Duration buttonDuration;

  Settings? _settings;
  Settings get settings => _settings!;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    if (_settings == null) {
      _settings = Settings.of(context);

      final reduceMotion = settings.accessability.reduceMotion;

      buttonDuration = reduceMotion.value ? Duration.zero : Motion.medium2;
      final duration = reduceMotion.value ? Duration.zero : Motion.long1;

      _controller = AnimationController(vsync: this, duration: duration);
      expansionAnimation = CurvedAnimation(
        parent: _controller,
        curve: Easing.standard,
        reverseCurve: Easing.standard,
      );
      fadeAnimation = CurvedAnimation(
        parent: _controller,
        curve: Easing.standardDecelerate,
      );
    }
  }

  @override
  void dispose() {
    _controller.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) => LayoutBuilder(builder: (context, size) {
        final theme = Theme.of(context);
        final scheme = theme.colorScheme;
        final text = theme.textTheme;
        final monoText = settings.design.monoFontTheme.textTheme;

        final strings = Strings.of(context)!;
        final materialStrings = MaterialLocalizations.of(context);

        final (iconBackground, iconForeground) = iconColors(
          scheme,
          theme.extension<RainbowColors>()!,
        );
        final msgStyle = text.titleMedium?.copyWith(color: scheme.onSurface);

        final maxWidth = size.maxWidth - (4 + 12 + 40 + 16 + 16 + 48 + 12 + 4);

        final tp = TextPainter(
          textDirection: TextDirection.ltr,
          text: TextSpan(
            text: message,
            style: msgStyle,
          ),
          maxLines: 1,
        );

        tp.layout(maxWidth: maxWidth);

        final isThreeLine = tp.didExceedMaxLines && tag != null;

        final (margin, shape) = cardShape();

        return Card.filled(
          clipBehavior: Clip.antiAliasWithSaveLayer,
          color: scheme.surfaceContainer,
          shape: shape,
          margin: margin,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              InkWell(
                onTap: showDetails,
                child: ListTile(
                  contentPadding: const EdgeInsets.fromLTRB(12, 8, 12, 8),
                  leading: Circle(
                    diameter: 40,
                    color: iconBackground,
                    child: Icon(levelIcon, color: iconForeground, weight: 600),
                  ),
                  minLeadingWidth: 40,
                  trailing: IconButton(
                    padding: EdgeInsets.zero,
                    onPressed: showDetails,
                    color: scheme.onSurface,
                    tooltip: isExpanded
                        ? materialStrings.expandedIconTapHint
                        : materialStrings.collapsedIconTapHint,
                    icon: Ink(
                      height: 40,
                      width: 40,
                      decoration: BoxDecoration(
                        color: scheme.surface,
                        shape: BoxShape.circle,
                      ),
                      child: AnimatedRotation(
                        turns: isExpanded ? 0.5 : 0,
                        duration: buttonDuration,
                        curve: Easing.standard,
                        child: const Icon(Symbols.arrow_drop_down),
                      ),
                    ),
                  ),
                  title: Text(
                    message,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    style: msgStyle,
                  ),
                  subtitle: tag == null
                      ? null
                      : Align(
                          alignment: AlignmentDirectional.centerStart,
                          child: MiniChip(text: tag!),
                        ),
                  isThreeLine: isThreeLine,
                ),
              ),
              FadeTransition(
                opacity: fadeAnimation,
                child: SizeTransition(
                  sizeFactor: expansionAnimation,
                  axisAlignment: -1,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      if (hasError)
                        Padding(
                          padding: const EdgeInsets.all(16),
                          child: Text(
                            error!,
                            style: monoText.bodyMedium?.copyWith(
                              color: scheme.onSurfaceVariant,
                            ),
                            softWrap: true,
                            maxLines: 999,
                          ),
                        ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 16),
                        child: Text(
                          formatTime(settings.dateTime, logTime),
                          style: text.bodyMedium?.copyWith(
                            color: scheme.outline,
                            fontStyle: FontStyle.italic,
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(16),
                        child: Row(
                          children: <Widget>[
                            FilledButton.tonal(
                              onPressed: widget.delete,
                              child: Text(materialStrings.deleteButtonTooltip),
                            ),
                            const Spacer(),
                            IconButton(
                              padding: EdgeInsets.zero,
                              color: scheme.secondary,
                              tooltip: strings.share,
                              onPressed: () async => await Share.share(
                                widget.entry.toString(),
                                subject: message,
                              ),
                              icon: Transform.scale(
                                scaleX: -1,
                                child: const Icon(Symbols.reply, weight: 500),
                              ),
                            ),
                            IconButton(
                              padding: EdgeInsets.zero,
                              color: scheme.secondary,
                              tooltip: materialStrings.copyButtonLabel,
                              onPressed: () async => await copyText(
                                context,
                                widget.entry.toString(),
                              ),
                              icon: const Icon(
                                Symbols.content_copy,
                                weight: 500,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        );
      });

  (GroupEdgeInsets, RoundedRectangleBorder) cardShape() {
    const double rGroup = 8;
    const double rSingle = 16;

    const double mGroup = 2;
    const double mSingle = 4;
    const double mSides = mSingle;

    return switch (widget.position) {
      LogCardPosition.start => const (
          GroupEdgeInsets(top: mSingle, bottom: mGroup, sides: mSides),
          RoundedRectangleBorder(
            borderRadius: GroupBorderRadius(
              top: Radius.circular(rSingle),
              bottom: Radius.circular(rGroup),
            ),
          ),
        ),
      LogCardPosition.middle => const (
          GroupEdgeInsets(top: mGroup, bottom: mGroup, sides: mSides),
          RoundedRectangleBorder(
            borderRadius: GroupBorderRadius(
              top: Radius.circular(rGroup),
              bottom: Radius.circular(rGroup),
            ),
          ),
        ),
      LogCardPosition.end => const (
          GroupEdgeInsets(top: mGroup, bottom: mSingle, sides: mSides),
          RoundedRectangleBorder(
            borderRadius: GroupBorderRadius(
              top: Radius.circular(rGroup),
              bottom: Radius.circular(rSingle),
            ),
          ),
        ),
      LogCardPosition.single => const (
          GroupEdgeInsets(top: 8, bottom: 8, sides: mSides),
          RoundedRectangleBorder(
            borderRadius: GroupBorderRadius(
              top: Radius.circular(12),
              bottom: Radius.circular(12),
            ),
          ),
        ),
    };
  }

  (Color, Color) iconColors(ColorScheme scheme, RainbowColors rainbow) {
    rainbow = rainbow.harmonized(scheme);

    return switch (level) {
      LogLevel.info => (rainbow.blue, rainbow.onBlue),
      LogLevel.warn => (rainbow.yellow, rainbow.onYellow),
      LogLevel.error => (rainbow.red, rainbow.onRed),
      LogLevel.log => (rainbow.purple, rainbow.onPurple),
      LogLevel.debug => (rainbow.brown, rainbow.onBrown),
      LogLevel.wtf => (rainbow.green, rainbow.onGreen),
    };
  }

  IconData get levelIcon => switch (level) {
        LogLevel.info => Symbols.lightbulb_outline,
        LogLevel.warn => Symbols.warning,
        LogLevel.error => Symbols.error_outline,
        LogLevel.log => Symbols.sticky_note_2,
        LogLevel.debug => Symbols.bug_report,
        LogLevel.wtf => MdiIcons.alienOutline,
      };

  void showDetails() {
    setState(() {
      isExpanded = !isExpanded;
    });

    if (isExpanded) {
      _controller.forward();
    } else {
      _controller.reverse();
    }
  }
}

import 'package:flutter/material.dart';

import '/strings/strings.dart';
import '/state/settings.dart';
import '/widgets/markdown_text.dart';
import '/widgets/back_button.dart' as back;

class SettingsMarkdownView extends StatelessWidget {
  const SettingsMarkdownView({super.key});

  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context)!;
    final settings = Settings.of(context);

    final enableMarkdown = settings.extensions.enableMarkdown;

    return Scaffold(
      appBar: AppBar(
        title: Text(strings.markdown),
        leading: const back.BackButton(),
      ),
      body: ListView(
        physics: const ClampingScrollPhysics(),
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.fromLTRB(16, 16, 16, 8),
            child: MarkdownText(
              strings.mdHelpMessage,
            ),
          ),
          const Divider(),
          ValueListenableBuilder(
            valueListenable: enableMarkdown,
            builder: (context, value, _) => SwitchListTile(
              value: value,
              title: Text(strings.markdown),
              subtitle: Text(strings.markdownDescription),
              onChanged: (value) => enableMarkdown.value = value,
            ),
          ),
        ],
      ),
    );
  }
}

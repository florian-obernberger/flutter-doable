import 'package:flutter/material.dart';
import 'package:gap/gap.dart';
import 'package:material_symbols_icons/symbols.dart';

import '/strings/strings.dart';
import '/state/settings.dart';
import '/widgets/markdown_text.dart';
import '/util/all_but_edge_insets.dart';

typedef ChildBuilder = Widget Function(BuildContext context, Widget child);

class InfoCard extends StatefulWidget {
  const InfoCard({
    required this.title,
    required this.body,
    this.initiallyExpanded = false,
    this.supportMarkdown = false,
    this.margin,
    this.backgroundColor,
    this.builder,
    this.onHide,
    super.key,
  });

  final String title;

  /// Shown when the [InfoCard] gets expanded.
  final String body;

  final bool initiallyExpanded;

  final EdgeInsetsGeometry? margin;
  final Color? backgroundColor;
  final ChildBuilder? builder;

  final bool supportMarkdown;

  /// If this is non-null a `Hide` button will be shown.
  /// This should be used for permanently hiding this
  /// [InfoCard] and indicates, that the user no longer
  /// needs this information.
  final VoidCallback? onHide;

  @override
  State<InfoCard> createState() => _InfoCardState();
}

class _InfoCardState extends State<InfoCard>
    with SingleTickerProviderStateMixin {
  late bool isExpanded = widget.initiallyExpanded;
  bool isHidden = false;

  late final Animation<double> expansionAnimation;
  late final Animation<double> fadeAnimation;
  late final AnimationController _controller;
  late final Duration duration;

  Settings? _settings;
  Settings get settings => _settings!;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    if (_settings == null) {
      _settings = Settings.of(context);

      final reduceMotion = settings.accessability.reduceMotion;

      duration = reduceMotion.value ? Duration.zero : Durations.long1;

      _controller = AnimationController(vsync: this, duration: duration);
      expansionAnimation = CurvedAnimation(
        parent: _controller,
        curve: Easing.standard,
      );
      fadeAnimation = CurvedAnimation(
        parent: _controller,
        curve: Easing.standardDecelerate,
      );

      if (isExpanded) _showDetails();
    }
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (isHidden) return const SizedBox.shrink();

    final strings = Strings.of(context)!;
    final materialStrings = MaterialLocalizations.of(context);
    final ThemeData(colorScheme: scheme, textTheme: text) = Theme.of(context);

    final helpTitleStyle = text.titleMedium!.copyWith(color: scheme.onSurface);
    final helpBodyStyle = text.bodyMedium!.copyWith(
      color: scheme.onSurfaceVariant,
    );

    final Widget title, body;

    if (widget.supportMarkdown) {
      title = MarkdownText(widget.title);
      body = MarkdownText(widget.body, softWrap: true);
    } else {
      title = Text(widget.title);
      body = Text(widget.body, style: helpBodyStyle);
    }

    final child = Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        ListTile(
          onTap: showDetails,
          contentPadding: const EdgeInsets.fromLTRB(16, 4, 16, 4),
          title: title,
          titleTextStyle: helpTitleStyle,
          leading: Icon(Symbols.info, color: scheme.secondaryFixed),
          trailing: Tooltip(
            message: isExpanded
                ? materialStrings.expandedIconTapHint
                : materialStrings.collapsedIconTapHint,
            child: AnimatedRotation(
              turns: isExpanded ? 0.5 : 0,
              duration: duration,
              curve: Easing.standard,
              child: const Icon(Symbols.arrow_drop_down),
            ),
          ),
        ),
        FadeTransition(
          opacity: fadeAnimation,
          child: SizeTransition(
            sizeFactor: expansionAnimation,
            axisAlignment: -1,
            child: Padding(
              padding: AllButEdgeInsets(
                16,
                top: 4,
                bottom: widget.onHide != null ? 12 : null,
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  body,
                  if (widget.onHide != null) ...[
                    const Gap(8),
                    const Divider(),
                    const Gap(4),
                    TextButton.icon(
                      onPressed: hide,
                      label: Text(strings.hideForever),
                      icon: const Icon(Symbols.do_not_disturb),
                    ),
                  ]
                ],
              ),
            ),
          ),
        ),
      ],
    );

    return Card.outlined(
      color: widget.backgroundColor,
      margin: widget.margin,
      clipBehavior: Clip.antiAlias,
      child: widget.builder?.call(context, child) ?? child,
    );
  }

  Future<void> hide() async {
    await showDetails();
    if (mounted) setState(() => isHidden = true);
    widget.onHide?.call();
  }

  TickerFuture _showDetails() =>
      isExpanded ? _controller.forward() : _controller.reverse();

  TickerFuture showDetails() {
    setState(() => isExpanded = !isExpanded);
    return _showDetails();
  }
}

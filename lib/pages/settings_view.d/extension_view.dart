import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:gap/gap.dart';
import 'package:material_symbols_icons/symbols.dart';
import 'package:notified_preferences/notified_preferences.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

import '/state/settings.dart';
import '/state/todo_db.dart';
import '/strings/strings.dart';
import '/util/animated_dialog.dart';
import '/widgets/back_button.dart' as back;
import '/widgets/markdown_text.dart';
import '/widgets/nav_button.dart';

import '../todo_list_settings_view.dart';
import 'extension_view.d/notifications_settings_dialog.dart';
import 'extension_view.d/reader_mode_settings_dialog.dart';
import 'extension_view.d/swipe_actions_settings_dialog.dart';

class SettingsExtensionView extends ConsumerWidget {
  const SettingsExtensionView({this.showAppBar = true, super.key});

  final bool showAppBar;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final strings = Strings.of(context)!;
    final settings = Settings.of(context).extensions;

    return Scaffold(
      appBar: showAppBar
          ? AppBar(
              title: Text(strings.additionalFeatures),
              leading: const back.BackButton(),
            )
          : null,
      body: ListView(
        physics: const ClampingScrollPhysics(),
        padding: const EdgeInsets.all(12).add(EdgeInsets.only(
          bottom: MediaQuery.of(context).viewPadding.bottom,
        )),
        children: <Widget>[
          _Extension(
            title: strings.lists,
            settingsDialog: const TodoListsSettingsView(standalone: false),
            description: strings.listsDescription,
            prefNotifier: settings.enableLists,
          ),
          _Extension(
              title: strings.notifications,
              description: strings.notificationsDescription,
              prefNotifier: settings.enableNotifications,
              isBeta: true,
              settingsDialog: const NotificationsSettingsDialog(),
              onEnable: () =>
                  ref.read(todoDatabaseProvider).scheduleAllNotifications(ref),
              onDisable: FlutterLocalNotificationsPlugin().cancelAll,
              test: () async {
                if (await Permission.notification.isGranted) return true;

                return await Permission.notification
                    .request()
                    .then((status) => status.isGranted);
              }),
          _Extension(
            title: strings.recurringTodos,
            description: strings.recurringTodosDescription,
            prefNotifier: settings.enableRecurringTasks,
            isBeta: true,
          ),
          _Extension(
            title: strings.markdown,
            description: strings.mdHelpMessage,
            prefNotifier: settings.enableMarkdown,
          ),
          _Extension(
            title: strings.todoImages,
            description: strings.todoImagesDescription,
            prefNotifier: settings.enableImages,
            isBeta: true,
          ),
          _Extension(
            title: strings.swipeActions,
            settingsDialog: const SwipeActionsSettingsDialog(),
            description: strings.swipeActionsDescription,
            prefNotifier: settings.enableSwipeActions,
          ),
          _Extension(
            title: strings.dateFilters,
            description: strings.dateFiltersDescription,
            prefNotifier: settings.enableDateFilters,
          ),
          _Extension(
            title: strings.progressBar,
            description: strings.progressBarDescription,
            prefNotifier: settings.enableProgressBar,
          ),
          _Extension(
            title: strings.readerMode,
            settingsDialog: const ReaderModeSettingsDialog(),
            description: strings.readerModeDescription,
            prefNotifier: settings.enableReaderModer,
          ),
          _Extension(
            title: strings.todoSearch,
            description: strings.todoSearchDescription,
            prefNotifier: settings.enableTodoSearch,
          ),
        ],
      ),
    );
  }
}

class _Extension extends StatelessWidget {
  const _Extension({
    required this.title,
    required this.description,
    required this.prefNotifier,
    this.isBeta = false,
    this.settingsDialog,
    this.test,
    this.onDisable,
    this.onEnable,
  });

  final String title;
  final String description;
  final PreferenceNotifier<bool> prefNotifier;
  final bool isBeta;
  final Future<bool> Function()? test;
  final Widget? settingsDialog;
  final Future<void> Function()? onDisable;
  final Future<void> Function()? onEnable;

  bool get hasSettings => settingsDialog != null;

  @override
  Widget build(BuildContext context) => ValueListenableBuilder<bool>(
        valueListenable: prefNotifier,
        builder: (context, enabled, _) {
          final strings = Strings.of(context)!;
          final scheme = Theme.of(context).colorScheme;
          final text = Theme.of(context).textTheme;

          final onOffButton = AnimatedCrossFade(
            alignment: Alignment.centerLeft,
            duration: Durations.short4,
            crossFadeState:
                enabled ? CrossFadeState.showSecond : CrossFadeState.showFirst,
            firstChild: FilledButton(
              onPressed: changeNotifier(context),
              style: FilledButton.styleFrom(
                minimumSize: const Size(double.infinity, 40),
              ),
              child: Text(strings.enable),
            ),
            secondChild: OutlinedButton(
              onPressed: changeNotifier(context),
              style: OutlinedButton.styleFrom(
                minimumSize: Size(!enabled ? 48 : double.infinity, 40),
              ),
              child: Text(strings.disable),
            ),
          );

          final settingsButton = FilledButton.tonal(
            onPressed: () => showSettings(context),
            style: FilledButton.styleFrom(
              minimumSize: const Size(double.infinity, 40),
            ),
            child: Text(strings.settings),
          );

          return Card(
            margin: const EdgeInsets.all(4),
            child: Padding(
              padding: const EdgeInsets.all(16),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        title,
                        style: text.headlineSmall?.copyWith(
                          color: scheme.onSurface,
                        ),
                      ),
                      if (isBeta)
                        Container(
                          padding: const EdgeInsets.symmetric(
                            vertical: 4,
                            horizontal: 12,
                          ),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(28),
                            color: scheme.secondary,
                          ),
                          child: Text(
                            strings.beta,
                            style: text.labelLarge?.copyWith(
                              color: scheme.onSecondary,
                            ),
                          ),
                        ),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 8, top: 4),
                    child: MarkdownText(
                      description,
                      softWrap: true,
                      baseStyle: text.bodyMedium?.copyWith(
                        color: scheme.onSurfaceVariant,
                      ),
                    ),
                  ),
                  SizedBox(
                    width: double.infinity,
                    child: Row(
                      children: <Widget>[
                        Expanded(child: onOffButton),
                        if (hasSettings && prefNotifier.value) ...[
                          const Gap(8),
                          Flexible(child: settingsButton)
                        ],
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      );

  void showSettings(BuildContext context) async {
    await showAnimatedDialog(
      useSafeArea: false,
      context: context,
      builder: (context) {
        final scheme = Theme.of(context).colorScheme;

        return Dialog.fullscreen(
          backgroundColor: scheme.surface,
          child: Scaffold(
            backgroundColor: scheme.surface,
            appBar: AppBar(
              backgroundColor: scheme.surfaceContainerHigh,
              title: Text(title),
              leading: const NavButton.close(),
            ),
            body: settingsDialog!,
          ),
        );
      },
      curve: Easing.emphasizedDecelerate,
      animationType: DialogTransitionType.slideFromBottomFade,
    );
  }

  void Function() changeNotifier(BuildContext context) => () async {
        unawaited(HapticFeedback.selectionClick());

        if (prefNotifier.value) {
          // The extension is enabled

          prefNotifier.value = false;
          await onDisable?.call();
          return;
        }

        // The extension is disabled

        if (isBeta) {
          final strings = Strings.of(context)!;

          final enable = await showAnimatedDialog<bool>(
                context: context,
                builder: (context) => AlertDialog(
                  title: Text(strings.extensionBetaWarning),
                  icon: const Icon(Symbols.error_outline),
                  content: ConstrainedBox(
                    constraints:
                        const BoxConstraints(minWidth: 280, maxWidth: 560),
                    child: Text(strings.extensionBetaWarningDescription),
                  ),
                  actions: [
                    TextButton(
                      onPressed: Navigator.of(context).pop,
                      child: Text(strings.cancel),
                    ),
                    TextButton(
                      onPressed: () => Navigator.of(context).pop(true),
                      child: Text(strings.enable),
                    ),
                  ],
                ),
              ) ??
              false;

          if (!enable) return;
        }

        if (test != null) {
          final enable = await test!();

          if (!enable) return;
        }

        prefNotifier.value = true;
        await onEnable?.call();
      };
}

import '/strings/strings.dart';

String _general(Strings strings) => strings.general;
String _generalDescription(Strings strings) => strings.generalDescription;
String _sorting(Strings strings) => strings.sorting;
String _sortingDescription(Strings strings) => strings.sortingDescription;
String _design(Strings strings) => strings.design;
String _designDescription(Strings strings) => strings.designDescription;
String _features(Strings strings) => strings.additionalFeatures;
String _featuresDescription(Strings strings) =>
    strings.additionalFeaturesDescription;
String _backup(Strings strings) => strings.syncAndBackup;
String _about(Strings strings) => strings.about;
String _aboutDescription(Strings strings) => strings.aboutDescription;
String _backupDescription(Strings strings) => strings.syncAndBackupDescription;
String _contrib(Strings strings) => strings.contributors;
String _contribDescription(Strings strings) => strings.contributorDescription;
String _licenses(Strings strings) => strings.licenses;
String _licensesDescription(Strings strings) => strings.licensesDescription;
String _licenseDetail(Strings strings) => '';
String _licenseDetailDescription(Strings strings) => '';
String _logs(Strings strings) => strings.logs;
String _logsDescription(Strings strings) => strings.logsDescription;

enum SettingsPage {
  general('/settings/general', _general, _generalDescription),
  sorting('/settings/sorting', _sorting, _sortingDescription),
  design('/settings/design', _design, _designDescription),
  features('/settings/features', _features, _featuresDescription),
  backupAndSync('/settings/backAndSync', _backup, _backupDescription),
  about('/settings/about', _about, _aboutDescription),
  contributors('/settings/contributors', _contrib, _contribDescription),
  logs('/settings/logs', _logs, _logsDescription),
  licenses('/settings/licenses', _licenses, _licensesDescription),
  licenseDetail(
    '/settings/licenses/details',
    _licenseDetail,
    _licenseDetailDescription,
  );

  const SettingsPage(this.route, this.title, this.description);

  final String route;
  final String Function(Strings strings) title;
  final String Function(Strings strings) description;

  static List<SettingsPage> get pages => [
        general,
        sorting,
        design,
        features,
        backupAndSync,
        about,
        contributors,
        logs,
        licenses,
      ];
}

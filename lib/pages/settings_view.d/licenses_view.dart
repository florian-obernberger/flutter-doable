import 'package:flutter/material.dart';
import 'package:material_symbols_icons/symbols.dart';

import '/strings/strings.dart';
import '/resources/oss_licenses.dart';
import '/widgets/search.dart' as search;
import '/widgets/back_button.dart' as back;

import 'licenses_view.d/item.dart';
import 'licenses_view.d/search.dart';

final packages = ossLicenses.where((p) => p.isDirectDependency).toList()
  ..sort((a, b) => a.name.toLowerCase().compareTo(b.name.toLowerCase()));

class SettingsLicensesView extends StatelessWidget {
  const SettingsLicensesView({
    this.showAppBar = true,
    this.onSelected,
    this.onSearch,
    super.key,
  });

  final bool showAppBar;
  final void Function(Package package)? onSelected;
  final VoidCallback? onSearch;

  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context)!;
    final scheme = Theme.of(context).colorScheme;
    final text = Theme.of(context).textTheme;

    final searchDelegate = LicensesSearchDelegate(
      packages: packages,
      searchFieldStyle: text.bodyLarge?.copyWith(color: scheme.onSurface),
    );

    return Scaffold(
      appBar: showAppBar
          ? AppBar(
              title: Text(strings.licenses),
              leading: const back.BackButton(),
              actions: [
                IconButton(
                  onPressed: () {
                    if (onSearch != null) {
                      onSearch!();
                    } else {
                      search.showSearch(
                        context: context,
                        delegate: searchDelegate,
                      );
                    }
                  },
                  icon: const Icon(Symbols.search, fill: 0),
                ),
              ],
            )
          : null,
      body: Scrollbar(
        interactive: true,
        child: ListView.builder(
          padding: const EdgeInsets.all(12).add(
            EdgeInsets.only(bottom: MediaQuery.of(context).padding.bottom),
          ),
          primary: true,
          physics: const ClampingScrollPhysics(),
          itemCount: packages.length,
          cacheExtent: packages.length * 2,
          shrinkWrap: false,
          itemBuilder: (context, i) => LicenseItem(
            packages[i],
            onSelected: onSelected,
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:gap/gap.dart';
import 'package:material_symbols_icons/symbols.dart';
import 'package:result/result.dart';

import '/strings/strings.dart';
import '/state/settings.dart';
import '/state/todo_db.dart';
import '/data/notifications/todo_notification.dart';
import '/data/todo_list.dart';
import '/data/base_notification.dart';
import '/util/formatters/format_duration.dart';
import '/widgets/duration_picker.dart';
import '/util/animated_time_picker.dart';
import '/util/formatters/format_date.dart';

import '../selection_settings.dart';
import '../../todo_view.d/todo_list_bottom_sheet.dart';

class NotificationsSettingsDialog extends ConsumerStatefulWidget {
  const NotificationsSettingsDialog({super.key});

  @override
  ConsumerState<ConsumerStatefulWidget> createState() =>
      _NotificationsSettingsDialogState();
}

class _NotificationsSettingsDialogState
    extends ConsumerState<NotificationsSettingsDialog> {
  TodoList? currentList;

  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context)!;
    final settings = Settings.of(context);
    final extensions = settings.extensions;

    final enableLists = extensions.enableLists.value;

    final infoBuilder = StringBuffer(strings.defaultNotificationsDescription);
    if (enableLists) {
      infoBuilder
        ..write(' ')
        ..write(strings.defaultNotificationsDescriptionIfLists);
    }

    return ListView(
      physics: const ClampingScrollPhysics(),
      children: <Widget>[
        const Gap(4),
        ListTile(
          isThreeLine: true,
          title: Text(strings.defaultNotifications),
          subtitle: Text(infoBuilder.toString()),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 12),
          child: buildDefaultNotificationSelector(
            enableLists: enableLists,
            extensions: extensions,
          ),
        ),
        const Gap(4),
        const Divider(),
        ValueListenableBuilder(
          valueListenable: extensions.defaultNotificationTime,
          builder: (context, time, _) => ListTile(
            title: Text(strings.defaultNotificationTime),
            subtitle: Text(formatTimeOfDay(settings.dateTime, time)),
            onTap: () => showAnimatedTimePicker(
              context: context,
              initialTime: time,
            ).then(updateDefaultNotifTime(ref)),
          ),
        ),
        ValueListenableBuilder(
          valueListenable: extensions.notificationSnoozeDuration,
          builder: (context, duration, _) => ListTile(
            title: Text(strings.snoozeDuration),
            isThreeLine: true,
            subtitle: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(strings.snoozeDurationDescription),
                Text(formatDuration(context, duration)),
              ],
            ),
            onTap: () => showAnimatedDurationPicker(
              context: context,
              title: strings.snoozeDuration,
              initialDuration: duration,
            ).then((picked) {
              if (picked == null) return;
              extensions.notificationSnoozeDuration.value = picked;
            }),
          ),
        ),
      ],
    );
  }

  Widget buildDefaultNotificationSelector({
    required bool enableLists,
    required ExtensionsPreferences extensions,
  }) {
    final strings = Strings.of(context)!;
    final ThemeData(colorScheme: scheme, textTheme: text) = Theme.of(context);

    return Card.outlined(
      child: Padding(
        padding: const EdgeInsets.all(12),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            if (enableLists) ...[
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 4),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      strings.notificationsFor,
                      style: text.titleMedium!.copyWith(
                        color: scheme.onSurface,
                      ),
                    ),
                    const Gap(8),
                    Flexible(
                      child: AnimatedSize(
                        duration: Durations.short4,
                        curve: Easing.standard,
                        alignment: Alignment.centerRight,
                        child: FilledButton.tonalIcon(
                          onPressed: () async => showModalSelection<TodoList>(
                            builder: (context) => const TodoListBottomSheet(),
                            handler: (list) => switch (list) {
                              None() => setState(() => currentList = null),
                              Some(value: final list) =>
                                setState(() => currentList = list),
                              null => null,
                            },
                          ),
                          label: Text(
                            currentList?.name ?? strings.allTodos,
                            overflow: TextOverflow.ellipsis,
                          ),
                          icon: const Icon(Symbols.arrow_drop_down),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              const Divider(height: 20),
            ],
            SelectionSettings<TodoDuration>(
              initialSelected: getData(extensions),
              values: TodoDuration.values,
              valueToString: (value) => value.localized(context),
              valueCompareTo: (v1, v2) => v1.compareTo(v2),
              onSelectionChange: (durations) => setData(extensions, durations),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(8, 8, 8, 4),
              child: Row(
                children: <Widget>[
                  TextButton.icon(
                    onPressed: () {
                      setData(extensions, [TodoDuration.atTimeOfEvent]);
                      setState(() {});
                    },
                    label: Text(strings.reset),
                    icon: const Icon(
                      Symbols.settings_backup_restore,
                      weight: 350,
                    ),
                  ),
                  const Gap(4),
                  AnimatedOpacity(
                    opacity: currentList != null ? 1 : 0,
                    duration: Durations.short3,
                    curve: Easing.standard,
                    child: TextButton.icon(
                      onPressed: () {
                        // Ensure that all notifications get cancelled
                        final current = extensions.defaultNotifications.value;
                        current.removeList(currentList!.id);
                        extensions.defaultNotifications.value = current;
                        setState(() => currentList = null);
                        updateTodos(extensions);
                      },
                      label: Text(strings.deleteListSettings),
                      icon: const Icon(
                        Symbols.delete,
                        weight: 350,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  List<TodoDuration> getData(ExtensionsPreferences extensions) =>
      extensions.defaultNotifications.value.getListOrGeneral(currentList?.id);

  Future<void> setData(
    ExtensionsPreferences extensions,
    List<TodoDuration> durations,
  ) async {
    final notifSettings = extensions.defaultNotifications.value;

    if (currentList != null) {
      notifSettings.setList(currentList!.id, durations);
    } else {
      notifSettings.setGeneral(durations);
    }

    extensions.defaultNotifications.value = notifSettings;
    await updateTodos(extensions);
  }

  Future<void> updateTodos(ExtensionsPreferences extensions) async {
    final notifSettings = extensions.defaultNotifications.value;
    final todoDb = ref.read(todoDatabaseProvider);
    final lists = notifSettings.getAllListIds();

    final todos = todoDb.values().where((tbm) =>
        tbm.notificationMetadata != null &&
        tbm.notificationMetadata!.isNotEmpty &&
        tbm.relevantDate != null);

    final toBeModified = switch (currentList) {
      TodoList(:final id) => todos.where((t) => t.listId == id),
      null => todos.where((t) => t.listId == null || !lists.contains(t.listId)),
    };

    for (final todo in toBeModified) {
      removeRelevantNotificationsFromTodo(todo);
      await addRelevantNotificationsToTodo(todo, extensions)
          .map((meta) => TodoNotification.from(meta, ref, todo))
          .scheduleAll();
    }
  }

  Future<void> Function(TimeOfDay?) updateDefaultNotifTime(WidgetRef ref) =>
      (TimeOfDay? pickedTime) async {
        if (pickedTime == null) return;

        Settings.of(ref.context).extensions.defaultNotificationTime.value =
            pickedTime;

        return ref.read(todoDatabaseProvider).scheduleAllNotifications(ref);
      };

  Future<void> showModalSelection<T>({
    required WidgetBuilder builder,
    required void Function(Option<T>? op) handler,
  }) =>
      showModalBottomSheet<Option<T>>(
        context: context,
        builder: builder,
        isDismissible: true,
        isScrollControlled: true,
        enableDrag: true,
        showDragHandle: true,
        useSafeArea: true,
        constraints: const BoxConstraints(maxWidth: 640),
      ).then(handler);
}

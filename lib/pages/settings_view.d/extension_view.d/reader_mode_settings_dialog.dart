import 'package:flutter/material.dart';
import 'package:material_symbols_icons/symbols.dart';

import '/state/settings.dart';
import '/strings/strings.dart';
import '/types/reader_mode.dart';
import '/widgets/segmented_selection.dart';

class ReaderModeSettingsDialog extends StatelessWidget {
  const ReaderModeSettingsDialog({super.key});

  @override
  Widget build(BuildContext context) {
    final settings = Settings.of(context);
    final strings = Strings.of(context)!;

    return Column(
      children: <Widget>[
        ValueListenableBuilder(
          valueListenable: settings.extensions.readerMode,
          builder: (context, _, __) {
            return SegmentedSelection(
              title: strings.readerModeDescriptionShort,
              items: SegmentedItem.fromValues(
                values: ReaderMode.values,
                toTitle: (mode) => mode.localizeTitle(strings),
                toIcon: (mode) => switch (mode) {
                  ReaderMode.edit => Symbols.edit,
                  ReaderMode.reader => Symbols.notes,
                  ReaderMode.saved => Symbols.history,
                },
              ),
              selected: settings.extensions.readerMode.value,
              onSelect: (mode) => settings.extensions.readerMode.value = mode,
            );
          },
        ),
        if (settings.extensions.enableNotifications.value)
          ValueListenableBuilder(
            valueListenable:
                settings.extensions.alwaysOpenNotificationsInReaderMode,
            builder: (context, alwaysOpen, _) => SwitchListTile(
              value: alwaysOpen,
              isThreeLine: true,
              title: Text(strings.notificationsAlwaysPreview),
              subtitle: Text(strings.notificationsAlwaysPreviewDescription),
              onChanged: (newValue) => settings.extensions
                  .alwaysOpenNotificationsInReaderMode.value = newValue,
            ),
          ),
      ],
    );
  }
}

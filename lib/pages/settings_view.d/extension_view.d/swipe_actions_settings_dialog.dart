import 'package:flutter/material.dart';

import '/state/settings.dart';
import '/strings/strings.dart';
import '/types/swipe_action.dart';

import '../item.dart';
import '../selection_settings.dart';

class SwipeActionsSettingsDialog extends StatelessWidget {
  const SwipeActionsSettingsDialog({super.key});

  @override
  Widget build(BuildContext context) {
    final settings = Settings.of(context);
    final strings = Strings.of(context)!;

    return ListView(
      physics: const ClampingScrollPhysics(),
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(12),
          child: SelectionSettings(
            initialSelected: settings.extensions.selectedSwipeActions.value,
            values: SwipeAction.values,
            valueToString: (value) => value.title(strings),
            valueCompareTo: Enum.compareByIndex,
            selectionReorderable: true,
            onSelectionChange: (selection) =>
                settings.extensions.selectedSwipeActions.value = selection,
            selectedTitle: strings.selectedSwipeActions,
            unselectedTitle: strings.unselectedSwipeActions,
          ),
        ),
        const Divider(),
        ValueListenableBuilder(
          valueListenable: settings.extensions.hideCheckWhenCompleteSelected,
          builder: (context, hide, _) => SettingsItem(
            mode: SettingsItemMode.toggle,
            toggleValue: hide,
            onSwitchChanged: (value) =>
                settings.extensions.hideCheckWhenCompleteSelected.value = value,
            title: strings.swipeActionsHideCheck,
            description: strings.swipeActionsHideCheckDescription(
              (StringBuffer()
                    ..write(strings.quoteDoubleLeft)
                    ..write(SwipeAction.complete.title(strings))
                    ..write(strings.quoteDoubleRight))
                  .toString(),
            ),
          ),
        ),
      ],
    );
  }
}

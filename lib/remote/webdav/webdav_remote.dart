import 'dart:convert';
import 'dart:io' as io;

import 'package:dio/dio.dart';
import 'package:dio/io.dart';
import 'package:webdav_client/webdav_client.dart';
import 'package:flutter/foundation.dart' show kDebugMode;
import 'package:path/path.dart' as p;
import 'package:result/anyhow.dart';

import '/data/converters/json/todo.dart';
import '/data/converters/json/todo_list.dart';
import '/util/extensions/iterable.dart';

import '../remote.dart';
import '../types.dart';
import '../handle_io_exception.dart';

String _fixSlash(String s) => s.endsWith('/') ? s : '$s/';

class WebdavRemote extends Remote {
  const WebdavRemote.fromClient(Client client, [String? folder])
      : _client = client,
        _folder = folder;

  factory WebdavRemote(
    String baseUrl, {
    required String user,
    required String password,
    String? folder,
  }) {
    final dio = WdDio(debug: kDebugMode);

    (dio.httpClientAdapter as IOHttpClientAdapter).createHttpClient = () {
      return HttpClient()
        ..badCertificateCallback = (cert, host, port) {
          return cert.issuer == cert.subject; // checks if it's self-signed
        };
    };

    final client = Client(
      uri: _fixSlash(baseUrl),
      c: dio,
      auth: BasicAuth(user: user, pwd: password),
    );

    client
      ..setHeaders({'accept-charset': 'utf-8'})
      ..setConnectTimeout(8000)
      ..setSendTimeout(8000)
      ..setReceiveTimeout(8000);

    return WebdavRemote.fromClient(client, folder);
  }

  final Client _client;
  final String? _folder;

  String get folder => _folder ?? '';
  String get baseUrl {
    final url = _client.uri;
    return url == '/' ? '' : url;
  }

  String get user => _client.auth.user;
  String get password => _client.auth.pwd;

  String _pathTo(
    String path, [
    String? path1,
    String? path2,
    String? path3,
  ]) =>
      _folder != null
          ? p.join('/', _folder, path, path1, path2, path3)
          : p.join('/', path, path1, path2, path3);

  String _todoPath(String id) => p.join(Remote.todosDirectory, '$id.todo');
  String _listPath(String id) => p.join(Remote.todoListsDirectory, '$id.list');

  RemoteExc _mapDioException(DioException err) {
    return switch (err.type) {
      DioExceptionType.connectionTimeout => onTimeOut(),
      DioExceptionType.sendTimeout => onTimeOut(),
      DioExceptionType.receiveTimeout => onTimeOut(),
      DioExceptionType.badCertificate => RemoteException.unexpectedError,
      DioExceptionType.badResponse => switch (err.response?.statusCode) {
          null => RemoteException.unknownError,
          io.HttpStatus.notFound => RemoteException.notFound,
          _ => RemoteException.unknownError,
        },
      DioExceptionType.cancel => RemoteException.unexpectedError,
      DioExceptionType.connectionError => RemoteException.connectionError,
      DioExceptionType.unknown => RemoteException.unknownError,
    };
  }

  FutureResult<String> _get(String path, {String? id}) async {
    if (!shouldMakeRequest) return bail(Remote.tooManyTimeOuts(id?.ids));

    try {
      final file = await _client.read(_pathTo(path));

      return Ok(utf8.decode(file));
    } on DioException catch (e) {
      return bail<String>(e).context(_mapDioException(e)(id?.ids));
    } on io.SocketException catch (e) {
      final (type, sanitized) = handleSocketException(e, onTimeOut);
      return bail<String>(sanitized).context(type(id?.ids));
    } on io.HttpException catch (e) {
      final (type, sanitized) = handleHttpException(e, onTimeOut);
      return bail<String>(sanitized).context(type(id?.ids));
    } catch (e) {
      return bail<String>(e).context(RemoteException.unknownError(id?.ids));
    }
  }

  FutureResult<()> _put(String path, String content, {String? id}) async {
    if (!shouldMakeRequest) return bail(Remote.tooManyTimeOuts(id?.ids));

    try {
      await _client.write(_pathTo(path), utf8.encode(content));

      return const Ok(());
    } on DioException catch (e) {
      return bail<()>(e).context(_mapDioException(e)(id?.ids));
    } on io.SocketException catch (e) {
      final (type, sanitized) = handleSocketException(e, onTimeOut);
      return bail<()>(sanitized).context(type(id?.ids));
    } on io.HttpException catch (e) {
      final (type, sanitized) = handleHttpException(e, onTimeOut);
      return bail<()>(sanitized).context(type(id?.ids));
    } catch (e) {
      return bail<()>(e).context(RemoteException.unknownError(id?.ids));
    }
  }

  FutureResult<()> _delete(String path, {String? id}) async {
    if (!shouldMakeRequest) return bail(Remote.tooManyTimeOuts(id?.ids));

    try {
      await _client.remove(_pathTo(path));

      return const Ok(());
    } on DioException catch (e) {
      return bail<()>(e).context(_mapDioException(e)(id?.ids));
    } on io.SocketException catch (e) {
      final (type, sanitized) = handleSocketException(e, onTimeOut);
      return bail<()>(sanitized).context(type(id?.ids));
    } on io.HttpException catch (e) {
      final (type, sanitized) = handleHttpException(e, onTimeOut);
      return bail<()>(sanitized).context(type(id?.ids));
    } catch (e) {
      return bail<()>(e).context(RemoteException.unknownError(id?.ids));
    }
  }

  FutureResult<()> _mkcol(String path) async {
    if (!shouldMakeRequest) return bail(Remote.tooManyTimeOuts());

    try {
      await _client.mkdirAll(_pathTo(path));

      return const Ok(());
    } on DioException catch (e) {
      return bail<()>(e).context(_mapDioException(e)());
    } on io.SocketException catch (e) {
      final (type, sanitized) = handleSocketException(e, onTimeOut);
      return bail<()>(sanitized).context(type());
    } on io.HttpException catch (e) {
      final (type, sanitized) = handleHttpException(e, onTimeOut);
      return bail<()>(sanitized).context(type());
    } catch (e) {
      return bail<()>(e).context(const RemoteException.unknownError());
    }
  }

  FutureResult<bool> _exists(String path) async {
    if (!shouldMakeRequest) return bail(Remote.tooManyTimeOuts());

    try {
      await _client.readProps(_pathTo(path));

      return const Ok(true);
    } on DioException catch (e) {
      return switch (_mapDioException(e)()) {
        RemoteException.notFound => const Ok((false)),
        RemoteException mapped => bail<bool>(e).context(mapped)
      };
    } on io.SocketException catch (e) {
      final (type, sanitized) = handleSocketException(e, onTimeOut);
      return bail<bool>(sanitized).context(type());
    } on io.HttpException catch (e) {
      final (type, sanitized) = handleHttpException(e, onTimeOut);
      return bail<bool>(sanitized).context(type());
    } catch (e) {
      return bail<bool>(e).context(const RemoteException.unknownError());
    }
  }

  FutureResult<bool> validate() async {
    try {
      await _client.ping();

      return const Ok(true);
    } on DioException catch (e) {
      return switch (e.type) {
        DioExceptionType.connectionTimeout ||
        DioExceptionType.sendTimeout ||
        DioExceptionType.receiveTimeout =>
          bail<bool>(e).context(const RemoteException.connectionTimedOut()),
        DioExceptionType.badCertificate =>
          bail<bool>(e).context(const RemoteException.unexpectedError()),
        DioExceptionType.badResponse => switch (e.response?.statusCode) {
            io.HttpStatus.badRequest ||
            io.HttpStatus.internalServerError =>
              bail<bool>(e).context(const RemoteException.internalError()),
            io.HttpStatus.notFound =>
              bail<bool>(e).context(const RemoteException.notFound()),
            _ => const Ok(false),
          },
        _ => const Ok(false),
      };
    } on io.SocketException catch (e) {
      final (type, sanitized) = handleSocketException(e, onTimeOut);
      return bail<bool>(sanitized).context(type());
    } on io.HttpException catch (e) {
      final (type, sanitized) = handleHttpException(e, onTimeOut);
      return bail<bool>(sanitized).context(type());
    } catch (e) {
      return bail<bool>(e).context(const RemoteException.unknownError());
    }
  }

  @override
  FutureResult<()> initialize() async {
    for (final directory in const [
      Remote.todosDirectory,
      Remote.todoListsDirectory,
    ]) {
      final res = await _mkcol(directory)
          .context("Couldn't create directory $directory");
      if (res.isErr()) return res;
    }

    for (final file in const [
      Remote.settingsFile,
      Remote.todosStoreFile,
      Remote.todoListsStoreFile
    ]) {
      final exists =
          await _exists(file).context("Couldn't check if $file exists");
      if (exists case Err(:final error)) return Err(error);

      if (!exists.unwrap()) {
        final res = await _put(file, '').context("Couldn't create file $file");
        if (res.isErr()) return res;
      }
    }

    return const Ok(());
  }

  @override
  FutureResult<StoreFile> fetchTodosStoreFile() => _get(Remote.todosStoreFile)
      .map(StoreFile.fromFile)
      .context("Couldn't fetch todos store file");

  @override
  FutureResult<StoreFile> fetchTodoListsStoreFile() =>
      _get(Remote.todoListsStoreFile)
          .map(StoreFile.fromFile)
          .context("Couldn't fetch lists store file");

  @override
  FutureResult<()> pushTodosStoreFile(StoreFile storeFile) => _put(
        Remote.todosStoreFile,
        storeFile.toStoreFile(),
      ).context("Couldn't push todos store file");

  @override
  FutureResult<()> pushTodoListsStoreFile(StoreFile storeFile) => _put(
        Remote.todoListsStoreFile,
        storeFile.toStoreFile(),
      ).context("Couldn't push lists store file");

  @override
  FutureResult<SettingsData> fetchSettings() => _get(Remote.settingsFile)
          .context("Couldn't fetch settings file")
          .andThen((source) {
        if (source.isEmpty) return const Ok({});
        return Guard.guard(() => json.decode(source) as SettingsData)
            .context("Couldn't decode settings file");
      });

  @override
  FutureResult<()> pushSettings(SettingsData settings) => _put(
        Remote.settingsFile,
        const JsonEncoder.withIndent('    ').convert(settings),
      ).context("Couldn't push settings file");

  @override
  FutureResult<Todo> fetchTodo(String id) => _get(_todoPath(id), id: id)
      .andThen((file) => parseTodo(file).mapErr(Error.new))
      .context("Couldn't fetch todo with id $id");

  @override
  FutureResult<TodoList> fetchTodoList(String id) => _get(_listPath(id), id: id)
      .andThen((file) => parseTodoList(file).mapErr(Error.new))
      .context("Couldn't fetch list with id $id");

  @override
  FutureResult<Iterable<Todo>> fetchTodos(Iterable<String> ids) async {
    final files = Stream.fromFutures(ids.map(
      (id) => _get(_todoPath(id), id: id)
          .context("Couldn't fetch todo with id $id")
          .map((file) => (file, id)),
    ));

    final todos = <Todo>[];

    await for (final file in files) {
      if (file case Err(error: final err)) return Err(err);

      final (content, id) = file.unwrap();
      final parsed = parseTodo(content);

      if (parsed case BaseErr(:final error)) {
        return bail<Iterable<Todo>>(error).context(
          RemoteException.corruptedFile(id.ids),
        );
      }

      todos.add(parsed.unwrap());
    }

    return Ok(todos);
  }

  @override
  FutureResult<Iterable<TodoList>> fetchTodoLists(Iterable<String> ids) async {
    final files = Stream.fromFutures(ids.map(
      (id) => _get(_listPath(id), id: id)
          .context("Couldn't fetch list with id $id")
          .map((file) => (file, id)),
    ));

    final lists = <TodoList>[];

    await for (final file in files) {
      if (file case Err(error: final err)) return Err(err);

      final (content, id) = file.unwrap();
      final parsed = parseTodoList(content);

      if (parsed case BaseErr(:final error)) {
        return bail<Iterable<TodoList>>(error).context(
          RemoteException.corruptedFile(id.ids),
        );
      }

      lists.add(parsed.unwrap());
    }

    return Ok(lists);
  }

  @override
  FutureResult<Iterable<Todo>> fetchNewTodos(
    Iterable<String> current, [
    Result<StoreFile>? currentStoreFile,
  ]) async {
    final storeFile = currentStoreFile != null
        ? Future.value(currentStoreFile)
        : fetchTodosStoreFile();

    return storeFile
        .andThen((store) => fetchTodos(store.without(current).ids))
        .context("Couldn't fetch new todos");
  }

  @override
  FutureResult<Iterable<TodoList>> fetchNewTodoLists(
    Iterable<String> current, [
    Result<StoreFile>? currentStoreFile,
  ]) async {
    final storeFile = currentStoreFile != null
        ? Future.value(currentStoreFile)
        : fetchTodoListsStoreFile();

    return storeFile
        .andThen((store) => fetchTodoLists(store.without(current).ids))
        .context("Couldn't fetch new lists");
  }

  @override
  FutureResult<Iterable<Todo>> fetchUpdatedTodos(
    Map<String, DateTime> current, [
    Result<StoreFile>? currentStoreFile,
  ]) async {
    final storeFile = currentStoreFile != null
        ? Future.value(currentStoreFile)
        : fetchTodosStoreFile();

    return storeFile
        .map((file) => file
            .only(current.keys)
            .where((store) => store.lastModified > current[store.id]!)
            .ids)
        .andThen(fetchTodos)
        .context("Couldn't fetch updated todos");
  }

  @override
  FutureResult<Iterable<TodoList>> fetchUpdatedTodoLists(
    Map<String, DateTime> current, [
    Result<StoreFile>? currentStoreFile,
  ]) async {
    final storeFile = currentStoreFile != null
        ? Future.value(currentStoreFile)
        : fetchTodoListsStoreFile();

    return storeFile
        .map((file) => file
            .only(current.keys)
            .where((store) => store.lastModified > current[store.id]!)
            .ids)
        .andThen(fetchTodoLists)
        .context("Couldn't fetch updated lists");
  }

  @override
  FutureResult<Iterable<String>> fetchDeletedTodos(
    Iterable<String> current, [
    Result<StoreFile>? currentStoreFile,
  ]) async {
    final storeFile = currentStoreFile != null
        ? Future.value(currentStoreFile)
        : fetchTodosStoreFile();

    return storeFile
        .map((file) => current.toSet().difference(file.ids.toSet()))
        .context("Couldn't fetch deleted todos");
  }

  @override
  FutureResult<Iterable<String>> fetchDeletedTodoLists(
    Iterable<String> current, [
    Result<StoreFile>? currentStoreFile,
  ]) async {
    final storeFile = currentStoreFile != null
        ? Future.value(currentStoreFile)
        : fetchTodoListsStoreFile();

    return storeFile
        .map((file) => current.toSet().difference(file.ids.toSet()))
        .context("Couldn't fetch deleted lists");
  }

  @override
  FutureResult<RemoteUpdate<Todo>> fetchTodoUpdate(
    Map<String, DateTime> current, [
    Result<StoreFile>? currentStoreFile,
  ]) async {
    final ids = current.keys;

    final update = await Future.wait([
      fetchNewTodos(ids, currentStoreFile),
      fetchUpdatedTodos(current, currentStoreFile),
      fetchDeletedTodos(ids, currentStoreFile)
    ]).merge().context("Couldn't fetch todo update");

    return update.map((update) {
      final [added, updated, deleted] = update;

      return (
        added: added.cast<Todo>(),
        updated: updated.cast<Todo>(),
        deleted: deleted.cast<String>(),
      );
    });
  }

  @override
  FutureResult<RemoteUpdate<TodoList>> fetchTodoListUpdate(
    Map<String, DateTime> current, [
    Result<StoreFile>? currentStoreFile,
  ]) async {
    final ids = current.keys;

    final update = await Future.wait([
      fetchNewTodoLists(ids, currentStoreFile),
      fetchUpdatedTodoLists(current, currentStoreFile),
      fetchDeletedTodoLists(ids, currentStoreFile)
    ]).merge().context("Couldn't fetch list update");

    return update.map((update) {
      final [added, updated, deleted] = update;

      return (
        added: added.cast<TodoList>(),
        updated: updated.cast<TodoList>(),
        deleted: deleted.cast<String>(),
      );
    });
  }

  @override
  FutureResult<()> pushTodo(
    Todo todo, [
    Result<StoreFile>? currentStoreFile,
  ]) async {
    final storeFile = currentStoreFile ?? await fetchTodosStoreFile();
    if (storeFile case Err(:final error)) return Err(error);

    final updatedStoreFile = storeFile.unwrap()..update(Store.fromTodo(todo));

    return _put(
      _todoPath(todo.id),
      storeTodo(todo),
      id: todo.id,
    ).context("Couldn't push todo with id ${todo.id}").and(
        pushTodosStoreFile(updatedStoreFile)
            .context("Couldn't update todos store file to include ${todo.id}"));
  }

  @override
  FutureResult<()> pushTodoList(
    TodoList list, [
    Result<StoreFile>? currentStoreFile,
  ]) async {
    final storeFile = currentStoreFile ?? await fetchTodoListsStoreFile();
    if (storeFile case Err(:final error)) return Err(error);

    final updatedStoreFile = storeFile.unwrap()
      ..update(Store.fromTodoList(list));

    return _put(
      _listPath(list.id),
      storeTodoList(list),
      id: list.id,
    ).context("Couldn't push list with id ${list.id}").and(
        pushTodoListsStoreFile(updatedStoreFile)
            .context("Couldn't update lists store file to include ${list.id}"));
  }

  @override
  FutureResult<()> pushTodos(
    Iterable<Todo> todos, [
    Result<StoreFile>? currentStoreFile,
  ]) async {
    if (!shouldMakeRequest) return bail(Remote.tooManyTimeOuts(todos.ids));
    if (todos.isEmpty) return const Ok(());

    final storeFile = currentStoreFile ?? await fetchTodosStoreFile();
    if (storeFile case Err(error: final err)) return Err(err);

    final values = todos.toList();

    final updateStoreFile = storeFile.unwrap()
      ..updateAll(values.map(Store.fromTodo));

    final files = Stream.fromFutures(values.map(
      (todo) => _put(_todoPath(todo.id), storeTodo(todo), id: todo.id)
          .context("Couldn't push todo with id ${todo.id}")
          .map((file) => (file, todo.id)),
    ));

    await for (final file in files) {
      if (file case Err(:final error)) return Err(error);
    }

    return pushTodosStoreFile(updateStoreFile);
  }

  @override
  FutureResult<()> pushTodoLists(
    Iterable<TodoList> lists, [
    Result<StoreFile>? currentStoreFile,
  ]) async {
    if (!shouldMakeRequest) return bail(Remote.tooManyTimeOuts(lists.ids));
    if (lists.isEmpty) return const Ok(());

    final storeFile = currentStoreFile ?? await fetchTodoListsStoreFile();
    if (storeFile case Err(error: final err)) return Err(err);

    final values = lists.toList();

    final updateStoreFile = storeFile.unwrap()
      ..updateAll(values.map(Store.fromTodoList));

    final files = Stream.fromFutures(values.map(
      (list) => _put(_listPath(list.id), storeTodoList(list), id: list.id)
          .context("Couldn't push list with id ${list.id}")
          .map((file) => (file, list.id)),
    ));

    await for (final file in files) {
      if (file case Err(:final error)) return Err(error);
    }

    return pushTodoListsStoreFile(updateStoreFile);
  }

  @override
  FutureResult<()> deleteTodo(
    String id, [
    Result<StoreFile>? currentStoreFile,
  ]) async {
    if (!shouldMakeRequest) return bail(Remote.tooManyTimeOuts(id.ids));

    final storeFile = currentStoreFile != null
        ? Future.value(currentStoreFile)
        : fetchTodosStoreFile();

    return storeFile.andThen((file) {
      if (file.contains(id)) return const Ok(());

      file.remove(id);
      return _delete(_todoPath(id))
          .context("Couldn't delete todo with id $id")
          .and(pushTodosStoreFile(file)
              .context("Couldn't remove $id from todos store file"));
    });
  }

  @override
  FutureResult<()> deleteTodos(
    Iterable<String> ids, [
    Result<StoreFile>? currentStoreFile,
    int retries = 0,
  ]) async {
    if (!shouldMakeRequest) return bail(Remote.tooManyTimeOuts(ids));

    final storeFileRes = currentStoreFile ?? await fetchTodosStoreFile();
    if (storeFileRes case Err(:final error)) return Err(error);

    final errors = <Error>[];

    final storeFile = storeFileRes.unwrap();

    final stream = Stream.fromFutures(ids.map((id) =>
        _delete(_todoPath(id), id: id)
            .context("Couldn't delete todo with id $id")
            .map((_) => id)));

    await for (final res in stream) {
      switch (res) {
        case Ok(:final value):
          storeFile.remove(value);
        case Err(:final error):
          errors.add(error);
      }
    }

    if (errors.isEmpty) {
      return pushTodosStoreFile(storeFile)
          .context('Couldn\'t update todos store file for ids $ids');
    }

    if (retries < 4) {
      final ids = errors
          .map((error) => error.downcast<RemoteException>())
          .whereOk()
          .expand((exception) => exception.ids ?? const <String>[]);

      return deleteTodos(ids, Ok(storeFile), retries + 1);
    }

    final exceptions = errors
        .map((error) => error.downcast<RemoteException>())
        .whereOk()
        .toList();

    return bail(RemoteException.multipleExceptions(exceptions));
  }

  @override
  FutureResult<()> deleteTodoList(
    String id, [
    Result<StoreFile>? currentStoreFile,
  ]) async {
    if (!shouldMakeRequest) return bail(Remote.tooManyTimeOuts(id.ids));

    final storeFile = currentStoreFile != null
        ? Future.value(currentStoreFile)
        : fetchTodoListsStoreFile();

    return storeFile.andThen((file) {
      if (file.contains(id)) return const Ok(());

      file.remove(id);
      return _delete(_listPath(id))
          .context("Couldn't delete list with id $id")
          .and(pushTodoListsStoreFile(file)
              .context("Couldn't remove $id from lists store file"));
    });
  }

  @override
  FutureResult<RemoteUpdate<Todo>> syncTodos(Iterable<Todo> current) async {
    if (!shouldMakeRequest) return bail(Remote.tooManyTimeOuts(current.ids));

    final storeFile = await fetchTodosStoreFile();
    if (storeFile case Err(:final error)) return Err(error);

    final currentStoreFile = storeFile.unwrap();
    final todos = current.toList();
    final modifications = {for (var todo in todos) todo.id: todo.lastModified};

    final toBePushed = todos.where((todo) {
      final isNew = !currentStoreFile.contains(todo.id);
      final isNewer = currentStoreFile.get(todo.id)?.lastModified.isBefore(
            todo.lastModified,
          );

      return isNew || (isNewer ?? false);
    });

    return pushTodos(toBePushed)
        .context("Couldn't push todos with ids ${toBePushed.ids}")
        .and(fetchTodoUpdate(
          modifications,
          toBePushed.isEmpty ? Ok(currentStoreFile) : null,
        ))
        .context("Couldn't sync todos");
  }

  @override
  FutureResult<RemoteUpdate<TodoList>> syncTodoLists(
    Iterable<TodoList> current,
  ) async {
    if (!shouldMakeRequest) return bail(Remote.tooManyTimeOuts(current.ids));

    final storeFile = await fetchTodoListsStoreFile();
    if (storeFile case Err(:final error)) return Err(error);

    final currentStoreFile = storeFile.unwrap();
    final lists = current.toList();
    final modifications = {for (var list in lists) list.id: list.lastModified};

    final toBePushed = lists.where((list) {
      final isNew = !currentStoreFile.contains(list.id);
      final isNewer = currentStoreFile.get(list.id)?.lastModified.isBefore(
            list.lastModified,
          );

      return isNew || (isNewer ?? false);
    });

    return pushTodoLists(toBePushed)
        .context("Couldn't push lists with ids ${toBePushed.ids}")
        .and(fetchTodoListUpdate(
          modifications,
          toBePushed.isEmpty ? Ok(currentStoreFile) : null,
        ))
        .context("Couldn't sync lists");
  }

  @override
  FutureResult<(RemoteUpdate<Todo>, RemoteUpdate<TodoList>)> sync(
    Iterable<Todo> currentTodos,
    Iterable<TodoList> currentLists,
  ) =>
      Future.wait<Result<RemoteUpdate>>([
        syncTodos(currentTodos),
        syncTodoLists(currentLists),
      ])
          .merge()
          .map((result) => (
                result.first as RemoteUpdate<Todo>,
                result.last as RemoteUpdate<TodoList>
              ))
          .context('Couldn\'t sync todos and / or lists');

  @override
  FutureResult<()> reset() async {
    String ctx(String path) => "Couldn't reset $path";

    final res = await Future.wait([
      Remote.todosDirectory,
      Remote.todoListsDirectory,
      Remote.settingsFile,
      Remote.todosStoreFile,
      Remote.todoListsStoreFile,
    ].map((path) => _delete(path).context(ctx(path)))).merge();

    return switch (res) {
      Ok() => const Ok(()),
      Err(:final error) => Err(error)
    };
  }
}

extension on String {
  Iterable<String> get ids => SingleIterable(this);
}

extension on Iterable<Todo> {
  Iterable<String> get ids => map((todo) => todo.id);
}

extension on Iterable<TodoList> {
  Iterable<String> get ids => map((todo) => todo.id);
}

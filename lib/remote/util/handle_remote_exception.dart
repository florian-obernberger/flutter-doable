import 'dart:async';

import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:result/anyhow.dart';

import '/strings/strings.dart';
import '/state/settings.dart';
import '/classes/logger.dart';
import '/util/animated_dialog.dart';

import '../remote.dart';
import '../neon/neon_remote.dart';
import '../types.dart';
import '../widgets/corrupt_file_dialog.dart';
import '../widgets/neon_wipe_dialog.dart';

Future<void> handleRemoteException(
  BuildContext context,
  Error err, {
  Remote? remote,
  FutureOr<void> Function({required bool deleteTodos})? onWipe,
}) async {
  final strings = Strings.of(context)!;
  final downcasted = err.downcast<RemoteException>();

  if (downcasted case Err(:final error)) {
    logger.ah(
      error,
      level: LogLevel.wtf,
      message: 'No RemoteException was found in the Error chain',
      tag: remote?.runtimeType.toString(),
    );
    return;
  }

  final exception = downcasted.unwrap();

  if (exception is NextcloudWipeInstruction) {
    assert(
      remote != null,
      'If at any point Nextcloud can be wiped, the remote cannot be null',
    );

    assert(
      onWipe != null,
      'If at any point Nextcloud can be wiped, the onWipe function cannot be null',
    );

    final neon = remote! as NeonRemote;

    final deleteTodos = await showAnimatedDialog<bool>(
      context: context,
      barrierDismissible: false,
      builder: (context) => const NeonWipeDialog(),
    );

    await onWipe!(deleteTodos: deleteTodos!);
    final res = await neon.confirmWipe();
    if (res case Err(:final error) when context.mounted) {
      return handleRemoteException(
        context,
        error,
        remote: neon,
        onWipe: onWipe,
      );
    }

    return;
  }

  final String message = switch (exception) {
    WrongUserOrPassword() => strings.syncWrongUserOrPassword,
    WrongUrl() => strings.syncWrongUrl,
    NotFound() => strings.syncFileDoesNotExist,
    CorruptedFile() => strings.syncCorruptedFile,
    Maintenance() => strings.syncNcMaintenance,
    Unavailable() => strings.syncServiceUnavailable,
    FileLocked() => strings.syncFileLocked,
    UnknownError() => strings.syncUnknownError,
    NoInternetConnection() => strings.syncNoInternetConnection,
    ConnectionTimedOut() => strings.syncConnectionTimedOut,
    TooManyConnectionTimeOuts() => strings.syncTooManyTimeOuts,
    InternalError() => strings.syncInternalError,
    ConnectionError() => strings.syncConnectionError,
    NotNextcloud() => strings.syncNotNcServer,
    Forbidden() => strings.syncUnknownError,
    UnexpectedError() => strings.syncUnknownError,
    MultipleExceptions() => strings.syncMultipleErrors,
    NextcloudWipeInstruction() => throw const ExpectedException(
        'Nextcloud wipe should already have been handled',
      ),
  };

  logger.ah(
    err,
    level: exception.logLevel,
    message: exception.logMessage,
    tag: remote?.runtimeType.toString(),
  );

  final backup = Settings.of(context).backup;

  final action = switch (exception) {
    CorruptedFile(:final Ids ids) when backup.synchronize && remote != null =>
      SnackBarAction(
        label: strings.restore,
        onPressed: () => showAnimatedDialog(
          context: context,
          barrierDismissible: false,
          builder: (context) => CorruptFileDialog(
            remote: remote,
            ids: ids.toList(),
            onError: handleRemoteException,
          ),
        ),
      ),
    NotFound(:final Ids ids) when backup.synchronize && remote != null =>
      SnackBarAction(
        label: strings.restore,
        onPressed: () => showAnimatedDialog(
          context: context,
          barrierDismissible: false,
          builder: (context) => CorruptFileDialog(
            remote: remote,
            ids: ids.toList(),
            onError: handleRemoteException,
          ),
        ),
      ),
    InternalError() => SnackBarAction(
        label: strings.giveFeedback,
        onPressed: () => context.push('/feedback'),
      ),
    _ => null,
  };

  ScaffoldMessenger.of(context).showSnackBar(SnackBar(
    duration: const Duration(seconds: 4),
    dismissDirection: DismissDirection.vertical,
    margin: const EdgeInsetsDirectional.fromSTEB(24, 8, 24, 18),
    content: Text(message),
    action: action,
  ));
}

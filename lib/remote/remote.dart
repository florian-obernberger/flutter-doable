import 'package:meta/meta.dart';
import 'package:result/anyhow.dart';

import '/data/todo.dart';
import '/data/todo_list.dart';
import '/state/time_out.dart';
export '/data/todo.dart';
export '/data/todo_list.dart';

import 'types.dart';

abstract class BareRemote {
  const BareRemote();

  @protected
  bool get shouldMakeRequest => !timedOutConnectionStore.hitTimeOutLimit(
        runtimeType.toString(),
      );

  @protected
  @mustCallSuper
  RemoteExc onTimeOut() {
    timedOutConnectionStore.timedOut(runtimeType.toString());

    return shouldMakeRequest
        ? RemoteException.connectionTimedOut
        : RemoteException.tooManyConnectionTimeOuts;
  }

  // Implementation methods

  /// Initialize the remote.
  FutureResult<()> initialize();

  /// Performs a complete synchronization between the remote and the local.
  FutureResult<(RemoteUpdate<Todo>, RemoteUpdate<TodoList>)> sync(
    Iterable<Todo> currentTodos,
    Iterable<TodoList> currentLists,
  );
}

/// Represents a remote that has the following directory structure to allow
/// for syncing Todos and TodoLists.
///
/// **Structure**
///
/// ```
/// .
/// ├── lists
/// │   ├── 3c221df0-c057-11ed-b4ee-472f8b616ed1.list
/// │   ├── 75921780-c03d-11ed-8190-fbc63b728665.list
/// │   ├── 9a4b66a0-a28b-11ed-86a8-3145d95a583e.list
/// │   └── ...
/// ├── lists.store
/// ├── todos
/// │   ├── 58704620-fcd5-11ed-aea6-db132a853136.todo
/// │   ├── 9f6b32f0-a28b-11ed-86a8-3145d95a583e.todo
/// │   ├── a93107b0-a28b-11ed-86a8-3145d95a583e.todo
/// │   └── ...
/// ├── todos.store
/// └── settings.json
/// ```
abstract class Remote extends BareRemote {
  @protected
  static const String todosDirectory = 'todos';

  @protected
  static const String todosStoreFile = 'todos.store';

  @protected
  static const String todoListsDirectory = 'lists';

  @protected
  static const String todoListsStoreFile = 'lists.store';

  @protected
  static const String settingsFile = 'settings.json';

  @protected
  static const RemoteExc tooManyTimeOuts =
      RemoteException.tooManyConnectionTimeOuts;

  const Remote();

  // Implementation methods

  @protected
  FutureResult<StoreFile> fetchTodosStoreFile();

  @protected
  FutureResult<StoreFile> fetchTodoListsStoreFile();

  @protected
  FutureResult<()> pushTodosStoreFile(StoreFile storeFile);

  @protected
  FutureResult<()> pushTodoListsStoreFile(StoreFile storeFile);

  /// Fetches the latest settings from the remote.
  FutureResult<SettingsData> fetchSettings();

  /// Pushes the given [settings] to the remote.
  FutureResult<()> pushSettings(SettingsData settings);

  /// Fetches the Todo with the given [id] from the remote.
  FutureResult<Todo> fetchTodo(String id);

  /// Fetches the TodoList with the given [id] from the remote.
  FutureResult<TodoList> fetchTodoList(String id);

  /// Fetches the Todos with the given [ids] from the remote.
  FutureResult<Iterable<Todo>> fetchTodos(Iterable<String> ids);

  /// Fetches the TodoLists with the given [ids] from the remote.
  FutureResult<Iterable<TodoList>> fetchTodoLists(Iterable<String> ids);

  /// Fetches all Todos that are not yet in the [current] ids.
  FutureResult<Iterable<Todo>> fetchNewTodos(Iterable<String> current);

  /// Fetches all TodoLists that are not yet in the [current] ids.
  FutureResult<Iterable<TodoList>> fetchNewTodoLists(Iterable<String> current);

  /// Fetches all Todos that are in [current] but have newer versions on the
  /// remote.
  FutureResult<Iterable<Todo>> fetchUpdatedTodos(Map<String, DateTime> current);

  /// Fetches all TodoLists that are in [current] but have newer versions on the
  /// remote.
  FutureResult<Iterable<TodoList>> fetchUpdatedTodoLists(
      Map<String, DateTime> current);

  /// Returns the ids of all Todos in [current] that have been deleted from
  /// the remote. Should only be called with ids of Todos where
  /// [Todo.hadInitialSync] is `true`.
  FutureResult<Iterable<String>> fetchDeletedTodos(Iterable<String> current);

  /// Returns the ids of all TodoLists in [current] that have been deleted from
  /// the remote. Should only be called with ids of TodoLists where
  /// [TodoList.hadInitialSync] is `true`.
  FutureResult<Iterable<String>> fetchDeletedTodoLists(
      Iterable<String> current);

  /// Returns all added, update and deleted Todos.
  FutureResult<RemoteUpdate<Todo>> fetchTodoUpdate(
      Map<String, DateTime> current);

  /// Returns all added, update and deleted TodoLists.
  FutureResult<RemoteUpdate<TodoList>> fetchTodoListUpdate(
      Map<String, DateTime> current);

  /// Pushes the given [todo] to the remote.
  FutureResult<()> pushTodo(Todo todo);

  /// Pushes the given [todoList] to the remote.
  FutureResult<()> pushTodoList(TodoList list);

  /// Pushes the given [todos] to the remote.
  FutureResult<()> pushTodos(Iterable<Todo> todos);

  /// Pushes the given [todoLists] to the remote.
  FutureResult<()> pushTodoLists(Iterable<TodoList> lists);

  /// Removes the Todo with the given [id] from the remote.
  FutureResult<()> deleteTodo(String id);

  /// Removes the Todos with the given [ids] from the remote.
  FutureResult<()> deleteTodos(Iterable<String> ids);

  /// Removes the TodoList with the given [id] from the remote.
  FutureResult<()> deleteTodoList(String id);

  /// Pushes the given Todos [current] and fetches an update.
  FutureResult<RemoteUpdate<Todo>> syncTodos(Iterable<Todo> current);

  /// Pushes the given TodoLists [current] and fetches an update.
  FutureResult<RemoteUpdate<TodoList>> syncTodoLists(
      Iterable<TodoList> current);

  /// Resets the remote.
  ///
  /// This includes:
  /// - deleting all [Todo]s
  /// - deleting all [TodoList]s
  /// - deleting the settings
  /// - deleting all [Store] files
  FutureResult<()> reset();
}

export 'types/remote_exception.dart';

export 'types/store_file.dart';
export 'types/remote_impl.dart';
export '/util/extensions/date_time.dart';

typedef SettingsData = Map<String, dynamic>;

typedef RemoteUpdate<T> = ({
  Iterable<T> added,
  Iterable<T> updated,
  Iterable<String> deleted
});

enum RemoteAction { delete, update, add, sync }

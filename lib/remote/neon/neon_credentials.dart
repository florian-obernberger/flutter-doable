class NeonCredentials {
  const NeonCredentials(
    this.baseUrl, {
    required this.user,
    required this.password,
    required this.userAgent,
    this.folder,
    this.language,
  });

  final String baseUrl;
  final String? folder;
  final String password;
  final String user;
  final String userAgent;
  final String? language;
}

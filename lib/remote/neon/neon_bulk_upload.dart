import 'dart:convert';
import 'dart:math';
import 'dart:typed_data';

import 'package:crypto/crypto.dart';
import 'package:http/http.dart' as http;
// ignore: implementation_imports
import 'package:http/src/boundary_characters.dart' as http;
import 'package:nextcloud/nextcloud.dart';
import 'package:nextcloud/webdav.dart';

part 'neon_bulk_upload/bulk_file.dart';
part 'neon_bulk_upload/bulk_request.dart';
part 'neon_bulk_upload/bulk_response.dart';

extension BulkExtension on WebDavClient {
  Future<List<BulkResponse>> bulk(List<BulkFile> files) async {
    final uri = _constructUri(rootClient.baseURL, PathUri.parse('bulk'));

    final request = BulkRequest(
      'POST',
      uri,
      headers: {...?rootClient.authentications?.firstOrNull?.headers},
    )..files.addAll(files);

    final response = await rootClient.httpClient.send(request);

    if (response.statusCode >= 300) {
      throw DynamiteStatusCodeException(
        await http.Response.fromStream(response),
      );
    }

    return BulkResponse.fromStreamedResponse(response).toList();
  }

  static Uri _constructUri(Uri baseURL, [PathUri? path]) {
    final segments = baseURL.pathSegments.toList()
      ..addAll(PathUri.parse('/remote.php/dav').pathSegments);
    if (path != null) {
      segments.addAll(path.pathSegments);
    }
    return baseURL.replace(
      pathSegments: segments.where((s) => s.isNotEmpty),
    );
  }
}

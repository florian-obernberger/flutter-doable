import 'dart:convert';
import 'dart:collection' as col;
import 'dart:io' as io;
import 'dart:typed_data';

import 'package:http/http.dart' as http;
import 'package:http/io_client.dart' as http;
import 'package:path/path.dart' as p;
import 'package:nextcloud/webdav.dart';
import 'package:nextcloud/core.dart';
import 'package:nextcloud/nextcloud.dart';
import 'package:nextcloud/provisioning_api.dart';
import 'package:result/anyhow.dart';

import '/data/converters/json/todo.dart';
import '/data/converters/json/todo_list.dart';
import '/util/extensions/iterable.dart';

import '../remote.dart';
import '../types.dart';
import '../handle_io_exception.dart';
import 'neon_credentials.dart';
import 'neon_bulk_upload.dart';

typedef AvatarUrl = ({Uri url, Map<String, String> headers});

class NeonRemote extends Remote {
  const NeonRemote._(
    NextcloudClient client,
    this.user,
    this._appPassword, [
    String? folder,
  ])  : _client = client,
        _folder = folder;

  factory NeonRemote(
    String baseUrl, {
    required String user,
    required String password,
    String? folder,
    String? userAgent,
    String? language,
  }) {
    final httpClient = http.IOClient(HttpClient()
      ..userAgent = userAgent
      ..badCertificateCallback = (cert, host, port) {
        return cert.issuer == cert.subject; // checks if it's self-signed
      });

    final client = NextcloudClient(
      Uri.parse(baseUrl),
      loginName: user,
      appPassword: password,
      httpClient: httpClient,
      userAgent: userAgent,
    );

    return NeonRemote._(client, user, password, folder);
  }

  factory NeonRemote.fromCredentials(NeonCredentials credentials) => NeonRemote(
        credentials.baseUrl,
        user: credentials.user,
        password: credentials.password,
        folder: credentials.folder,
        userAgent: credentials.userAgent,
        language: credentials.language,
      );

  final NextcloudClient _client;
  final String? _folder;
  final String _appPassword;

  String get baseUrl => _client.baseURL.toString();
  String get folder => _folder ?? '';
  final String user;

  static _jsonEncode(Object? object) =>
      const JsonEncoder.withIndent('    ').convert(
        object,
      );

  PathUri _pathTo(
    String path, [
    String? path1,
    String? path2,
    String? path3,
  ]) =>
      PathUri.parse(_folder != null
          ? p.join('/', _folder, path, path1, path2, path3)
          : p.join('/', path, path1, path2, path3));

  String _todoPath(String id) => p.join(Remote.todosDirectory, '$id.todo');
  String _listPath(String id) => p.join(Remote.todoListsDirectory, '$id.list');

  FutureResult<String> _get(String path, {String? id}) async {
    if (!shouldMakeRequest) bail(Remote.tooManyTimeOuts(id?.ids));

    final realPath = _pathTo(path);

    try {
      final file = await _client.webdav.get(realPath);

      return Ok((utf8.decode(file)));
    } on FormatException catch (e) {
      return bail<String>(e).context(RemoteException.corruptedFile(id?.ids));
    } on DynamiteStatusCodeException catch (e) {
      return bail<String>(e).context(switch (e.statusCode) {
        io.HttpStatus.forbidden ||
        io.HttpStatus.unauthorized =>
          await _checkWipe(e.statusCode),
        io.HttpStatus.notFound => RemoteException.notFound,
        io.HttpStatus.locked => RemoteException.fileLocked,
        _ => RemoteException.unknownError
      }(id?.ids));
    } on DynamiteApiException catch (e) {
      return bail<String>(e).context(RemoteException.connectionError(id?.ids));
    } on http.ClientException catch (e) {
      return bail<String>(e).context(RemoteException.connectionError(id?.ids));
    } on io.SocketException catch (e) {
      final (type, sanitized) = handleSocketException(e, onTimeOut);
      return bail<String>(sanitized).context(type(id?.ids));
    } on io.HttpException catch (e) {
      final (type, sanitized) = handleHttpException(e, onTimeOut);
      return bail<String>(sanitized).context(type(id?.ids));
    } catch (e) {
      return bail<String>(e).context(RemoteException.unknownError(id?.ids));
    }
  }

  FutureResult<()> _put(
    String path,
    String content, {
    String? id,
    DateTime? lastModified,
    DateTime? created,
  }) async {
    if (!shouldMakeRequest) bail(Remote.tooManyTimeOuts(id?.ids));

    final realPath = _pathTo(path);

    try {
      await _client.webdav.put(
        utf8.encode(content),
        realPath,
        lastModified: lastModified,
        created: created,
      );

      return const Ok(());
    } on DynamiteStatusCodeException catch (e) {
      return bail<()>(e).context(switch (e.statusCode) {
        io.HttpStatus.forbidden ||
        io.HttpStatus.unauthorized =>
          await _checkWipe(e.statusCode),
        io.HttpStatus.notFound => RemoteException.notFound,
        io.HttpStatus.locked => RemoteException.fileLocked,
        _ => RemoteException.unknownError
      }(id?.ids));
    } on DynamiteApiException catch (e) {
      return bail<()>(e).context(RemoteException.connectionError(id?.ids));
    } on http.ClientException catch (e) {
      return bail<()>(e).context(RemoteException.connectionError(id?.ids));
    } on io.SocketException catch (e) {
      final (type, sanitized) = handleSocketException(e, onTimeOut);
      return bail<()>(sanitized).context(type(id?.ids));
    } on io.HttpException catch (e) {
      final (type, sanitized) = handleHttpException(e, onTimeOut);
      return bail<()>(sanitized).context(type(id?.ids));
    } catch (e) {
      return bail<()>(e).context(RemoteException.unknownError(id?.ids));
    }
  }

  FutureResult<List<BulkResponse>> _bulk(
    List<BulkFile> files, {
    List<String>? ids,
  }) async {
    if (!shouldMakeRequest) return bail(Remote.tooManyTimeOuts(ids));

    try {
      final res = await _client.webdav.bulk(files);

      return Ok(res);
    } on DynamiteStatusCodeException catch (e) {
      return bail<List<BulkResponse>>(e).context(switch (e.statusCode) {
        io.HttpStatus.forbidden ||
        io.HttpStatus.unauthorized =>
          await _checkWipe(e.statusCode),
        io.HttpStatus.notFound => RemoteException.notFound,
        io.HttpStatus.locked => RemoteException.fileLocked,
        _ => RemoteException.unknownError
      }(ids));
    } on DynamiteApiException catch (e) {
      return bail<List<BulkResponse>>(e).context(
        RemoteException.connectionError(ids),
      );
    } on http.ClientException catch (e) {
      return bail<List<BulkResponse>>(e).context(
        RemoteException.connectionError(ids),
      );
    } on io.SocketException catch (e) {
      final (type, sanitized) = handleSocketException(e, onTimeOut);
      return bail<List<BulkResponse>>(sanitized).context(type(ids));
    } on io.HttpException catch (e) {
      final (type, sanitized) = handleHttpException(e, onTimeOut);
      return bail<List<BulkResponse>>(sanitized).context(type(ids));
    } catch (e) {
      return bail<List<BulkResponse>>(e).context(
        RemoteException.unknownError(ids),
      );
    }
  }

  FutureResult<()> _delete(String path, {String? id}) async {
    if (!shouldMakeRequest) return bail(Remote.tooManyTimeOuts(id?.ids));

    final realPath = _pathTo(path);

    try {
      await _client.webdav.delete(realPath);

      return const Ok(());
    } on DynamiteStatusCodeException catch (e) {
      final sanitized = e.sanitize();

      if (sanitized.statusCode == io.HttpStatus.notFound) return const Ok(());
      return bail<()>(sanitized).context(switch (sanitized.statusCode) {
        io.HttpStatus.forbidden ||
        io.HttpStatus.unauthorized =>
          await _checkWipe(sanitized.statusCode),
        io.HttpStatus.locked => RemoteException.fileLocked,
        _ => RemoteException.unknownError
      }(id?.ids));
    } on DynamiteApiException catch (e) {
      return bail<()>(e).context(RemoteException.connectionError(id?.ids));
    } on http.ClientException catch (e) {
      return bail<()>(e).context(RemoteException.connectionError(id?.ids));
    } on io.SocketException catch (e) {
      final (type, sanitized) = handleSocketException(e, onTimeOut);
      return bail<()>(sanitized).context(type(id?.ids));
    } on io.HttpException catch (e) {
      final (type, sanitized) = handleHttpException(e, onTimeOut);
      return bail<()>(sanitized).context(type(id?.ids));
    } catch (e) {
      return bail<()>(e).context(RemoteException.unknownError(id?.ids));
    }
  }

  FutureResult<()> _mkcol(String path) async {
    if (!shouldMakeRequest) return bail(Remote.tooManyTimeOuts());

    final realPath = _pathTo(path);

    try {
      await _client.webdav.mkcol(realPath);

      return const Ok(());
    } on DynamiteStatusCodeException catch (e) {
      final sanitized = e.sanitize();

      if (sanitized.statusCode == io.HttpStatus.methodNotAllowed) {
        return const Ok(());
      }

      return bail<()>(sanitized).context(switch (sanitized.statusCode) {
        io.HttpStatus.forbidden ||
        io.HttpStatus.unauthorized =>
          (await _checkWipe(sanitized.statusCode))(),
        io.HttpStatus.conflict ||
        io.HttpStatus.unsupportedMediaType ||
        io.HttpStatus.insufficientStorage =>
          const RemoteException.unexpectedError(),
        _ => const RemoteException.unknownError(),
      });
    } on DynamiteApiException catch (e) {
      return bail<()>(e).context(const RemoteException.connectionError());
    } on http.ClientException catch (e) {
      return bail<()>(e).context(const RemoteException.connectionError());
    } on io.SocketException catch (e) {
      final (type, sanitized) = handleSocketException(e, onTimeOut);
      return bail<()>(sanitized).context(type());
    } on io.HttpException catch (e) {
      final (type, sanitized) = handleHttpException(e, onTimeOut);
      return bail<()>(sanitized).context(type());
    } catch (e) {
      return bail<()>(e).context(const RemoteException.unknownError());
    }
  }

  FutureResult<bool> _exists(String path) async {
    if (!shouldMakeRequest) return bail(Remote.tooManyTimeOuts());

    final realPath = _pathTo(path);

    try {
      await _client.webdav.propfind(realPath, depth: WebDavDepth.zero);
      return const Ok(true);
    } on DynamiteStatusCodeException catch (e) {
      if (e.statusCode == io.HttpStatus.forbidden ||
          e.statusCode == io.HttpStatus.unauthorized) {
        final type = await _checkWipe(e.statusCode);
        if (type == RemoteException.nextcloudWipeInstruction) {
          return bail<bool>(e).context(type());
        }
      }

      return const Ok(false);
    } on DynamiteApiException catch (e) {
      return bail<bool>(e).context(const RemoteException.connectionError());
    } on http.ClientException catch (e) {
      return bail<bool>(e).context(const RemoteException.connectionError());
    } on io.SocketException catch (e) {
      final (type, sanitized) = handleSocketException(e, onTimeOut);
      return bail<bool>(sanitized).context(type());
    } on io.HttpException catch (e) {
      final (type, sanitized) = handleHttpException(e, onTimeOut);
      return bail<bool>(sanitized).context(type());
    } catch (e) {
      return bail<bool>(e).context(const RemoteException.unknownError());
    }
  }

  @override
  FutureResult<()> initialize() async {
    if (_folder != null && _folder.isNotEmpty) {
      final pathBuilder = StringBuffer('/');

      for (final segment in p.split(_folder)) {
        pathBuilder
          ..write(segment)
          ..write('/');

        final path = pathBuilder.toString();
        final res = await _mkcol(path).context('Couldn\'t create path $path');
        if (res.isErr()) res;
      }
    }

    for (final directory in const [
      Remote.todosDirectory,
      Remote.todoListsDirectory,
    ]) {
      final res = await _mkcol(directory)
          .context('Couldn\'t create directory $directory');
      if (res.isErr()) return res;
    }

    for (final file in const [
      Remote.settingsFile,
      Remote.todosStoreFile,
      Remote.todoListsStoreFile
    ]) {
      final exists =
          await _exists(file).context('Couldn\'t check if $file exists');
      if (exists case Err(:final error)) return Err(error);

      if (!exists.unwrap()) {
        final res = await _put(file, '').context('Couldn\'t create file $file');
        if (res.isErr()) return res;
      }
    }

    return const Ok(());
  }

  FutureResult<()> logout() async {
    try {
      await _client.core.appPassword.deleteAppPassword();

      return const Ok(());
    } catch (e) {
      return bail<()>(e)
          .context(const RemoteException.unknownError())
          .context('Couldn\'t log out of Nextcloud');
    }
  }

  FutureResult<String> _getCurrentUserId() async {
    try {
      final user = await _client.provisioningApi.users.getCurrentUser();
      if (user.statusCode != 200) {
        return bail<String>(
          const RemoteException.connectionError(),
        ).context(user);
      }

      return Ok(user.body.ocs.data.id);
    } on DynamiteStatusCodeException catch (e) {
      final sanitized = e.sanitize();

      return bail<String>(sanitized).context(switch (sanitized.statusCode) {
        io.HttpStatus.notFound => RemoteException.notFound,
        _ => RemoteException.unknownError,
      }());
    } on DynamiteApiException catch (e) {
      return bail<String>(e).context(const RemoteException.connectionError());
    } on http.ClientException catch (e) {
      return bail<String>(e).context(const RemoteException.connectionError());
    } on io.SocketException catch (e) {
      final (type, sanitized) = handleSocketException(e, onTimeOut);
      return bail<String>(sanitized).context(type());
    } on io.HttpException catch (e) {
      final (type, sanitized) = handleHttpException(e, onTimeOut);
      return bail<String>(sanitized).context(type());
    } catch (e) {
      return bail<String>(e).context(const RemoteException.unknownError());
    }
  }

  FutureResult<AvatarUrl> fetchAvatarUrl([int size = 512]) async {
    final userIdRes =
        await _getCurrentUserId().context('Couldn\'t get current user id');
    if (userIdRes case Err(:final error)) return Err(error);

    final userId = userIdRes.unwrap();

    final headers = <String, String>{
      'Accept': '*/*',
      ...?_client.authentications?.firstOrNull?.headers,
    };

    final url = Uri.parse(
      '${_client.baseURL}/index.php/avatar/$userId/$size',
    );

    return Ok((url: url, headers: headers));
  }

  FutureResult<Uint8List> fetchAvatar([int size = 512]) async {
    try {
      final userIdRes =
          await _getCurrentUserId().context('Couldn\'t get current user id');
      if (userIdRes case Err(:final error)) return Err(error);

      final userId = userIdRes.unwrap();
      final avatar = await _client.core.avatar.getAvatar(
        userId: userId,
        size: size,
      );

      return switch (avatar.statusCode) {
        io.HttpStatus.ok => Ok(avatar.body),
        _ => bail<Uint8List>(
            const RemoteException.unknownError(),
          ).context(avatar)
      };
    } on DynamiteStatusCodeException catch (e) {
      final sanitized = e.sanitize();

      return bail<Uint8List>(sanitized).context(switch (sanitized.statusCode) {
        io.HttpStatus.notFound => RemoteException.notFound,
        _ => RemoteException.unknownError,
      }());
    } on DynamiteApiException catch (e) {
      return bail<Uint8List>(e).context(
        const RemoteException.connectionError(),
      );
    } on http.ClientException catch (e) {
      return bail<Uint8List>(e).context(
        const RemoteException.connectionError(),
      );
    } on io.SocketException catch (e) {
      final (type, sanitized) = handleSocketException(e, onTimeOut);
      return bail<Uint8List>(sanitized).context(type());
    } on io.HttpException catch (e) {
      final (type, sanitized) = handleHttpException(e, onTimeOut);
      return bail<Uint8List>(sanitized).context(type());
    } catch (e) {
      return bail<Uint8List>(e).context(const RemoteException.unknownError());
    }
  }

  @override
  FutureResult<StoreFile> fetchTodosStoreFile() => _get(Remote.todosStoreFile)
      .map(StoreFile.fromFile)
      .context('Couldn\'t fetch todos store file');

  @override
  FutureResult<StoreFile> fetchTodoListsStoreFile() =>
      _get(Remote.todoListsStoreFile)
          .map(StoreFile.fromFile)
          .context('Couldn\'t fetch lists store file');

  @override
  FutureResult<()> pushTodosStoreFile(StoreFile storeFile) => _put(
        Remote.todosStoreFile,
        storeFile.toStoreFile(),
      ).context('Couldn\'t push todos store file');

  @override
  FutureResult<()> pushTodoListsStoreFile(StoreFile storeFile) => _put(
        Remote.todoListsStoreFile,
        storeFile.toStoreFile(),
      ).context('Couldn\'t push lists store file');

  @override
  FutureResult<SettingsData> fetchSettings() => _get(Remote.settingsFile)
          .context('Couldn\'t fetch settings file')
          .andThen((source) {
        if (source.isEmpty) return const Ok({});
        return Guard.guard(() => json.decode(source) as SettingsData)
            .context('Couldn\'t decode settings file');
      });

  @override
  FutureResult<()> pushSettings(SettingsData settings) => _put(
        Remote.settingsFile,
        _jsonEncode(settings),
      ).context('Couldn\'t push settings file');

  @override
  FutureResult<Todo> fetchTodo(String id) => _get(_todoPath(id), id: id)
      .andThen((file) => parseTodo(file).mapErr(Error.new))
      .context('Couldn\'t fetch todo with id $id');

  @override
  FutureResult<TodoList> fetchTodoList(String id) => _get(_listPath(id), id: id)
      .andThen((file) => parseTodoList(file).mapErr(Error.new))
      .context('Couldn\'t fetch list with id $id');

  @override
  FutureResult<Iterable<Todo>> fetchTodos(Iterable<String> ids) async {
    final files = Stream.fromFutures(ids.map(
      (id) => _get(_todoPath(id), id: id)
          .context('Couldn\'t fetch todo with id $id')
          .map((file) => (file, id)),
    ));

    final todos = <Todo>[];

    await for (final file in files) {
      if (file case Err(:final error)) return Err(error);

      final (content, id) = file.unwrap();
      final parsed = parseTodo(content);

      if (parsed case BaseErr(:final error)) {
        return bail<Iterable<Todo>>(error).context(
          RemoteException.corruptedFile(id.ids),
        );
      }

      todos.add(parsed.unwrap());
    }

    return Ok(todos);
  }

  @override
  FutureResult<Iterable<TodoList>> fetchTodoLists(Iterable<String> ids) async {
    final files = Stream.fromFutures(ids.map(
      (id) => _get(_listPath(id), id: id)
          .context('Couldn\'t fetch list with id $id')
          .map((file) => (file, id)),
    ));

    final lists = <TodoList>[];

    await for (final file in files) {
      if (file case Err(:final error)) return Err(error);

      final (content, id) = file.unwrap();
      final parsed = parseTodoList(content);

      if (parsed case BaseErr(:final error)) {
        return bail<Iterable<TodoList>>(error).context(
          RemoteException.corruptedFile(id.ids),
        );
      }

      lists.add(parsed.unwrap());
    }

    return Ok(lists);
  }

  @override
  FutureResult<Iterable<Todo>> fetchNewTodos(
    Iterable<String> current, [
    Result<StoreFile>? currentStoreFile,
  ]) async {
    final storeFile = currentStoreFile != null
        ? Future.value(currentStoreFile)
        : fetchTodosStoreFile();

    return storeFile
        .andThen((store) => fetchTodos(store.without(current).ids))
        .context('Couldn\'t fetch new todos');
  }

  @override
  FutureResult<Iterable<TodoList>> fetchNewTodoLists(
    Iterable<String> current, [
    Result<StoreFile>? currentStoreFile,
  ]) async {
    final storeFile = currentStoreFile != null
        ? Future.value(currentStoreFile)
        : fetchTodoListsStoreFile();

    return storeFile
        .andThen((store) => fetchTodoLists(store.without(current).ids))
        .context('Couldn\'t fetch new lists');
  }

  @override
  FutureResult<Iterable<Todo>> fetchUpdatedTodos(
    Map<String, DateTime> current, [
    Result<StoreFile>? currentStoreFile,
  ]) async {
    final storeFile = currentStoreFile != null
        ? Future.value(currentStoreFile)
        : fetchTodosStoreFile();

    return storeFile
        .map((file) => file
            .only(current.keys)
            .where((store) => store.lastModified > current[store.id]!)
            .ids)
        .andThen(fetchTodos)
        .context('Couldn\'t fetch updated todos');
  }

  @override
  FutureResult<Iterable<TodoList>> fetchUpdatedTodoLists(
    Map<String, DateTime> current, [
    Result<StoreFile>? currentStoreFile,
  ]) async {
    final storeFile = currentStoreFile != null
        ? Future.value(currentStoreFile)
        : fetchTodoListsStoreFile();

    return storeFile
        .map((file) => file
            .only(current.keys)
            .where((store) => store.lastModified > current[store.id]!)
            .ids)
        .andThen(fetchTodoLists)
        .context('Couldn\'t fetch updated lists');
  }

  @override
  FutureResult<Iterable<String>> fetchDeletedTodos(
    Iterable<String> current, [
    Result<StoreFile>? currentStoreFile,
  ]) async {
    final storeFile = currentStoreFile != null
        ? Future.value(currentStoreFile)
        : fetchTodosStoreFile();

    return storeFile
        .map((file) => current.toSet().difference(file.ids.toSet()))
        .context('Couldn\'t fetch deleted todos');
  }

  @override
  FutureResult<Iterable<String>> fetchDeletedTodoLists(
    Iterable<String> current, [
    Result<StoreFile>? currentStoreFile,
  ]) async {
    final storeFile = currentStoreFile != null
        ? Future.value(currentStoreFile)
        : fetchTodoListsStoreFile();

    return storeFile
        .map((file) => current.toSet().difference(file.ids.toSet()))
        .context('Couldn\'t fetch deleted lists');
  }

  @override
  FutureResult<RemoteUpdate<Todo>> fetchTodoUpdate(
    Map<String, DateTime> current, [
    Result<StoreFile>? currentStoreFile,
  ]) async {
    final ids = current.keys;

    final update = await Future.wait([
      fetchNewTodos(ids, currentStoreFile),
      fetchUpdatedTodos(current, currentStoreFile),
      fetchDeletedTodos(ids, currentStoreFile)
    ]).merge().context('Couldn\'t fetch todo update');

    return update.map((update) {
      final [added, updated, deleted] = update;

      return (
        added: added.cast<Todo>(),
        updated: updated.cast<Todo>(),
        deleted: deleted.cast<String>(),
      );
    });
  }

  @override
  FutureResult<RemoteUpdate<TodoList>> fetchTodoListUpdate(
    Map<String, DateTime> current, [
    Result<StoreFile>? currentStoreFile,
  ]) async {
    final ids = current.keys;

    final update = await Future.wait([
      fetchNewTodoLists(ids, currentStoreFile),
      fetchUpdatedTodoLists(current, currentStoreFile),
      fetchDeletedTodoLists(ids, currentStoreFile)
    ]).merge().context('Couldn\'t fetch list update');

    return update.map((update) {
      final [added, updated, deleted] = update;

      return (
        added: added.cast<TodoList>(),
        updated: updated.cast<TodoList>(),
        deleted: deleted.cast<String>(),
      );
    });
  }

  @override
  FutureResult<()> pushTodo(
    Todo todo, [
    Result<StoreFile>? currentStoreFile,
  ]) async {
    final storeFile = currentStoreFile ?? await fetchTodosStoreFile();
    if (storeFile case Err(:final error)) return Err(error);

    final updatedStoreFile = storeFile.unwrap()..update(Store.fromTodo(todo));

    return _put(
      _todoPath(todo.id),
      storeTodo(todo),
      id: todo.id,
    ).context('Couldn\'t push todo with id ${todo.id}').and(
        pushTodosStoreFile(updatedStoreFile).context(
            'Couldn\'t update todos store file to include ${todo.id}'));
  }

  @override
  FutureResult<()> pushTodoList(
    TodoList list, [
    Result<StoreFile>? currentStoreFile,
  ]) async {
    final storeFile = currentStoreFile ?? await fetchTodoListsStoreFile();
    if (storeFile case Err(:final error)) return Err(error);

    final updatedStoreFile = storeFile.unwrap()
      ..update(Store.fromTodoList(list));

    return _put(
      _listPath(list.id),
      storeTodoList(list),
      id: list.id,
    ).context('Couldn\'t push list with id ${list.id}').and(
        pushTodoListsStoreFile(updatedStoreFile).context(
            'Couldn\'t update lists store file to include ${list.id}'));
  }

  @override
  FutureResult<()> pushTodos(
    Iterable<Todo> todos, [
    Result<StoreFile>? currentStoreFile,
  ]) async {
    if (!shouldMakeRequest) return bail(Remote.tooManyTimeOuts(todos.ids));
    if (todos.isEmpty) return const Ok(());

    final storeFile = currentStoreFile ?? await fetchTodosStoreFile();
    if (storeFile case Err(:final error)) return Err(error);

    final values = todos.toList();

    final updateStoreFile = storeFile.unwrap()
      ..updateAll(values.map(Store.fromTodo));

    final ids = values.map((todo) => todo.id).toList();
    final files = values
        .map((todo) => BulkFile.fromString(
              _pathTo(_todoPath(todo.id)),
              storeTodo(todo),
              todo.lastModified,
            ))
        .toList();

    final res = await _bulk(files, ids: ids)
        .context('Couldn\'t push todos with ids $ids');

    if (res case Err(:final error)) return Err(error);

    final responses = res.unwrap();
    for (final error in responses.where((res) => res.error)) {
      final id = p.basename(error.filepath).split('.').firstOrNull;
      return bail<()>(error)
          .context(const RemoteException.unknownError())
          .context('Couldn\'t upload todo with id $id');
    }

    return pushTodosStoreFile(updateStoreFile);
  }

  @override
  FutureResult<()> pushTodoLists(
    Iterable<TodoList> lists, [
    Result<StoreFile>? currentStoreFile,
  ]) async {
    if (!shouldMakeRequest) return bail(Remote.tooManyTimeOuts(lists.ids));
    if (lists.isEmpty) return const Ok(());

    final storeFile = currentStoreFile ?? await fetchTodoListsStoreFile();
    if (storeFile case Err(:final error)) return Err(error);

    final values = lists.toList();

    final updateStoreFile = storeFile.unwrap()
      ..updateAll(values.map(Store.fromTodoList));

    final ids = values.map((list) => list.id).toList();
    final files = values
        .map((list) => BulkFile.fromString(
              _pathTo(_listPath(list.id)),
              storeTodoList(list),
              list.lastModified,
            ))
        .toList();

    final res = await _bulk(files, ids: ids)
        .context('Couldn\'t push lists with ids $ids');

    if (res case Err(:final error)) return Err(error);

    final responses = res.unwrap();
    for (final error in responses.where((res) => res.error)) {
      final id = p.basename(error.filepath).split('.').firstOrNull;
      return bail<()>(error)
          .context(const RemoteException.unknownError())
          .context('Couldn\'t upload list with id $id');
    }

    return pushTodoListsStoreFile(updateStoreFile);
  }

  @override
  FutureResult<()> deleteTodo(
    String id, [
    Result<StoreFile>? currentStoreFile,
  ]) async {
    if (!shouldMakeRequest) return bail(Remote.tooManyTimeOuts(id.ids));

    final storeFile = currentStoreFile != null
        ? Future.value(currentStoreFile)
        : fetchTodosStoreFile();

    return storeFile.andThen((file) {
      if (file.contains(id)) return const Ok(());

      file.remove(id);
      return _delete(_todoPath(id))
          .context('Couldn\'t delete todo with id $id')
          .and(pushTodosStoreFile(file)
              .context('Couldn\'t remove $id from todos store file'));
    });
  }

  @override
  FutureResult<()> deleteTodos(
    Iterable<String> ids, [
    Result<StoreFile>? currentStoreFile,
    int retries = 0,
  ]) async {
    if (!shouldMakeRequest) return bail(Remote.tooManyTimeOuts(ids));

    final storeFileRes = currentStoreFile ?? await fetchTodosStoreFile();
    if (storeFileRes case Err(:final error)) return Err(error);

    final errors = <Error>[];

    final storeFile = storeFileRes.unwrap();

    final stream = Stream.fromFutures(ids.map((id) =>
        _delete(_todoPath(id), id: id)
            .context('Couldn\'t delete todo with id $id')
            .map((_) => id)));

    await for (final res in stream) {
      switch (res) {
        case Ok(:final value):
          storeFile.remove(value);
        case Err(:final error):
          errors.add(error);
      }
    }

    if (errors.isEmpty) {
      return pushTodosStoreFile(storeFile)
          .context('Couldn\'t update todos store file for ids $ids');
    }

    if (retries < 4) {
      final ids = errors
          .map((error) => error.downcast<RemoteException>())
          .whereOk()
          .expand((exception) => exception.ids ?? const <String>[]);

      return deleteTodos(ids, Ok(storeFile), retries + 1);
    }

    final exceptions = errors
        .map((error) => error.downcast<RemoteException>())
        .whereOk()
        .toList();

    return bail(RemoteException.multipleExceptions(exceptions));
  }

  @override
  FutureResult<()> deleteTodoList(
    String id, [
    Result<StoreFile>? currentStoreFile,
  ]) async {
    if (!shouldMakeRequest) return bail(Remote.tooManyTimeOuts(id.ids));

    final storeFile = currentStoreFile != null
        ? Future.value(currentStoreFile)
        : fetchTodoListsStoreFile();

    return storeFile.andThen((file) {
      if (file.contains(id)) return const Ok(());

      file.remove(id);
      return _delete(_listPath(id))
          .context('Couldn\'t delete list with id $id')
          .and(pushTodoListsStoreFile(file)
              .context('Couldn\'t remove $id from lists store file'));
    });
  }

  @override
  FutureResult<RemoteUpdate<Todo>> syncTodos(Iterable<Todo> current) async {
    if (!shouldMakeRequest) return bail(Remote.tooManyTimeOuts(current.ids));

    final storeFile = await fetchTodosStoreFile();
    if (storeFile case Err(:final error)) return Err(error);

    final currentStoreFile = storeFile.unwrap();
    final todos = current.toList();
    final modifications = {for (var todo in todos) todo.id: todo.lastModified};

    final toBePushed = todos.where((todo) {
      final isNew = !currentStoreFile.contains(todo.id);
      final isNewer = currentStoreFile.get(todo.id)?.lastModified.isBefore(
            todo.lastModified,
          );

      return isNew || (isNewer ?? false);
    });

    return pushTodos(toBePushed)
        .context('Couldn\'t push todos with ids ${toBePushed.ids}')
        .and(fetchTodoUpdate(
          modifications,
          toBePushed.isEmpty ? Ok(currentStoreFile) : null,
        ))
        .context('Couldn\'t sync todos');
  }

  @override
  FutureResult<RemoteUpdate<TodoList>> syncTodoLists(
    Iterable<TodoList> current,
  ) async {
    if (!shouldMakeRequest) return bail(Remote.tooManyTimeOuts(current.ids));

    final storeFile = await fetchTodoListsStoreFile();
    if (storeFile case Err(:final error)) return Err(error);

    final currentStoreFile = storeFile.unwrap();
    final lists = current.toList();
    final modifications = {for (var list in lists) list.id: list.lastModified};

    final toBePushed = lists.where((list) {
      final isNew = !currentStoreFile.contains(list.id);
      final isNewer = currentStoreFile.get(list.id)?.lastModified.isBefore(
            list.lastModified,
          );

      return isNew || (isNewer ?? false);
    });

    return pushTodoLists(toBePushed)
        .context('Couldn\'t push lists with ids ${toBePushed.ids}')
        .and(fetchTodoListUpdate(
          modifications,
          toBePushed.isEmpty ? Ok(currentStoreFile) : null,
        ))
        .context('Couldn\'t sync lists');
  }

  @override
  FutureResult<(RemoteUpdate<Todo>, RemoteUpdate<TodoList>)> sync(
    Iterable<Todo> currentTodos,
    Iterable<TodoList> currentLists,
  ) =>
      Future.wait<Result<RemoteUpdate>>([
        syncTodos(currentTodos),
        syncTodoLists(currentLists),
      ])
          .merge()
          .map((result) => (
                result.first as RemoteUpdate<Todo>,
                result.last as RemoteUpdate<TodoList>
              ))
          .context('Couldn\'t sync todos and / or lists');

  @override
  FutureResult<()> reset() async {
    String ctx(String path) => "Couldn't reset $path";

    final res = await Future.wait([
      Remote.todosDirectory,
      Remote.todoListsDirectory,
      Remote.settingsFile,
      Remote.todosStoreFile,
      Remote.todoListsStoreFile,
    ].map((path) => _delete(path).context(ctx(path)))).merge();

    return switch (res) {
      Ok() => const Ok(()),
      Err(:final error) => Err(error)
    };
  }

  FutureResult<()> confirmWipe() => Guard.guardAsync(
        () async => _client.core.wipe.wipeDone(
          $body: WipeWipeDoneRequestApplicationJson(
            (b) => b..token = _appPassword,
          ),
        ),
      ).context('Couldn\'t confirm wipe').andThen((res) => res.statusCode == 200
          ? const Ok(())
          : bail<()>(const RemoteException.unknownError()).context(res));

  Future<RemoteException Function([Ids?])> _checkWipe(int statusCode) => _client
      .core.wipe
      .checkWipe(
        $body: WipeCheckWipeRequestApplicationJson(
          (b) => b..token = _appPassword,
        ),
      )
      .then((response) => response.statusCode == 200
          ? RemoteException.nextcloudWipeInstruction
          : switch (statusCode) {
              io.HttpStatus.forbidden => RemoteException.forbidden,
              _ => RemoteException.unknownError,
            })
      .onError<DynamiteApiException>((_, __) => RemoteException.connectionError)
      .onError<Exception>((_, __) => RemoteException.unknownError);
}

extension on DynamiteStatusCodeException {
  DynamiteStatusCodeException sanitize() =>
      DynamiteStatusCodeException(http.Response(
        response.body,
        response.statusCode,
        reasonPhrase: response.reasonPhrase,
      ));
}

extension on String {
  Iterable<String> get ids => SingleIterable(this);
}

extension on Iterable<Todo> {
  Iterable<String> get ids => map((todo) => todo.id);
}

extension on Iterable<TodoList> {
  Iterable<String> get ids => map((todo) => todo.id);
}

part of '../neon_bulk_upload.dart';

class BulkResponse {
  const BulkResponse({
    required this.filepath,
    required this.etag,
    required this.error,
    this.fileid,
    this.permissions,
  });

  static Stream<BulkResponse> fromStreamedResponse(
      http.StreamedResponse response) async* {
    final bytes = await response.stream.toBytes();
    final parsed = json.decode(utf8.decode(bytes));

    if (parsed case Map<String, dynamic> files) {
      for (final file in files.keys) {
        final data = files[file];

        if (data case {'error': bool error, 'etag': String etag}) {
          yield BulkResponse(
            filepath: file,
            etag: etag,
            error: error,
            fileid: data['fileid'] as String?,
            permissions: data['permissions'] as String?,
          );
        }
      }
    }
  }

  final String filepath;
  final String etag;
  final bool error;
  final String? fileid;
  final String? permissions;

  Map<String, dynamic> toMap() => {
        'filepath': filepath,
        'etag': etag,
        'error': error,
        'fileid': fileid,
        'permissions': permissions,
      };

  @override
  String toString() => toMap().toString();
}

part of '../neon_bulk_upload.dart';

class BulkRequest extends http.BaseRequest {
  static const int _boundaryLength = 40;
  static final _random = Random();

  BulkRequest(
    super.method,
    super.url, {
    Map<String, String>? headers,
  }) {
    if (headers != null) this.headers.addAll(headers);
  }

  final files = <BulkFile>[];

  @override
  int get contentLength {
    int length = 0;

    for (final file in files) {
      length += '--'.length +
          _boundaryLength +
          '\r\n'.length +
          utf8.encode(_headerForFile(file)).length +
          file.contentLength +
          '\r\n'.length;
    }

    return length + '--'.length + _boundaryLength + '--\r\n'.length;
  }

  @override
  set contentLength(int? value) {
    throw UnsupportedError(
      'Cannot set the contentLength property of multipart requests.',
    );
  }

  @override
  http.ByteStream finalize() {
    final boundary = _boundaryString();
    headers['Content-Type'] = 'multipart/related; boundary=$boundary';
    super.finalize();

    return http.ByteStream(_finalize(boundary));
  }

  Stream<List<int>> _finalize(String boundary) async* {
    const line = [13, 10]; // \r\n
    final separator = utf8.encode('--$boundary\r\n');
    final close = utf8.encode('--$boundary--\r\n');

    for (final file in files) {
      yield separator;
      yield utf8.encode(_headerForFile(file));
      yield* http.ByteStream.fromBytes(file.dataBytes);
      yield line;
    }
    yield close;
  }

  String _headerForFile(BulkFile file) => ''
      'X-File-Path: ${file.path}\r\n'
      'X-OC-Mtime: ${file.modified.millisecondsSinceEpoch ~/ Duration.millisecondsPerSecond}\r\n'
      'X-File-Md5: ${file.md5Hash}\r\n'
      'Content-Length: ${file.contentLength}\r\n'
      '\r\n';

  String _boundaryString() {
    var prefix = 'doable-bulk-boundary-';
    var list = List<int>.generate(
      _boundaryLength - prefix.length,
      (_) => http.boundaryCharacters[_random.nextInt(
        http.boundaryCharacters.length,
      )],
      growable: false,
    );

    return '$prefix${String.fromCharCodes(list)}';
  }
}

part of '../neon_bulk_upload.dart';

class BulkFile {
  BulkFile(this.path, this.dataBytes, this.modified, {this.encoding = utf8});

  BulkFile.fromString(
    this.path,
    String data,
    this.modified, {
    this.encoding = utf8,
  }) : dataBytes = Uint8List.fromList(encoding.encode(data));

  final PathUri path;
  final DateTime modified;
  final Uint8List dataBytes;
  final Encoding encoding;

  String get md5Hash => md5.convert(dataBytes).toString();
  int get contentLength => dataBytes.length;
  String get data => encoding.decode(dataBytes);
}

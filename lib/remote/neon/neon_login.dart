import 'dart:io' as io;
import 'dart:async';

import 'package:http/io_client.dart' as http;
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:nextcloud/core.dart';
import 'package:nextcloud/nextcloud.dart';
import 'package:result/anyhow.dart';

import '/strings/strings.dart';
import '/state/settings.dart';
import '/classes/system_info.dart';
import '/util/launch.dart';

import '../types.dart';
import '../handle_io_exception.dart';
import 'neon_credentials.dart';

class NeonLogin {
  NeonLogin._(
    this._client,
    this._folder,
    this._userAgent,
    this._language,
    this._inAppBrowser,
  );

  factory NeonLogin(
    BuildContext context,
    String baseUrl, [
    String? folder,
    String? language,
  ]) {
    final strings = Strings.of(context)!;

    const debugAddOn = kDebugMode ? ' DEBUG' : '';
    final modelAddon = ' (${SystemInfo.instance.deviceModelName})';
    final userAgent = '${strings.appTitle}$debugAddOn$modelAddon';

    if (folder != null && folder.startsWith('/')) {
      folder = folder.substring(1);
    }

    final httpClient = http.IOClient(HttpClient()
      ..userAgent = userAgent
      ..badCertificateCallback = (cert, host, port) {
        return cert.issuer == cert.subject; // checks if it's self-signed
      });

    final client = NextcloudClient(
      Uri.parse(baseUrl),
      httpClient: httpClient,
      userAgent: userAgent,
    );

    return NeonLogin._(
      client,
      folder,
      userAgent,
      language,
      Settings.of(context).general.inAppBrowser.value,
    );
  }

  final NextcloudClient _client;
  final String? _folder;
  final String _userAgent;
  final String? _language;
  final bool _inAppBrowser;

  bool _canceled = false;

  FutureResult<bool> validate() async {
    const context = "Couldn't validate Nextcloud server";

    try {
      final status = await _client.core.getStatus();
      print(status);

      return Ok(status.body.versionCheck.isSupported);
    } on DynamiteApiException catch (_) {
      return const Ok(false);
    } on io.SocketException catch (e) {
      final (type, sanitized) = handleSocketException(e);
      return bail<bool>(type()).context(sanitized).context(context);
    } on io.HttpException catch (e) {
      final (type, sanitized) = handleHttpException(e);
      return bail<bool>(type()).context(sanitized).context(context);
    } catch (e) {
      return bail<bool>(const RemoteException.unknownError())
          .context(e)
          .context(context);
    }
  }

  FutureResult<NeonCredentials> login() async {
    try {
      final init = await _client.core.clientFlowLoginV2.init();

      final res = await launchUrl(init.body.login, inAppBrowser: _inAppBrowser);
      if (res case BaseErr(:final error)) {
        return bail<NeonCredentials>(
          const RemoteException.unexpectedError(),
        ).context(error);
      }

      final watch = Stopwatch()..start();
      NeonCredentials? neon;

      while (!_canceled && neon == null && watch.elapsed.inMinutes <= 20) {
        try {
          final login = await _client.core.clientFlowLoginV2.poll(
            $body: ClientFlowLoginV2PollRequestApplicationJson(
              (b) => b..token = init.body.poll.token,
            ),
          );

          // throws if unsuccessful

          neon = NeonCredentials(
            login.body.server,
            user: login.body.loginName,
            password: login.body.appPassword,
            folder: _folder,
            userAgent: _userAgent,
            language: _language,
          );

          await Future.delayed(const Duration(seconds: 1));
        } on DynamiteStatusCodeException catch (e) {
          if (e.statusCode != io.HttpStatus.notFound) {
            return bail<NeonCredentials>(
              const RemoteException.unknownError(),
            ).context(e);
          }
        } on io.SocketException catch (e) {
          final (type, sanitized) = handleSocketException(e);
          return bail<NeonCredentials>(type()).context(sanitized);
        } on io.HttpException catch (e) {
          final (type, sanitized) = handleHttpException(e);
          return bail<NeonCredentials>(type()).context(sanitized);
        } catch (e) {
          return bail<NeonCredentials>(
            const RemoteException.unknownError(),
          ).context(e);
        }
      }

      return switch (neon) {
        NeonCredentials() => Ok(neon),
        null => bail<NeonCredentials>(
            const RemoteException.connectionTimedOut(),
          ),
      }
          .context('Could not get nextcloud credentials');
    } catch (e) {
      return bail<NeonCredentials>(
        const RemoteException.unknownError(),
      ).context(e);
    }
  }

  void cancel() => _canceled = true;
}

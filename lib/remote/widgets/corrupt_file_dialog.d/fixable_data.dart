part of '../corrupt_file_dialog.dart';

sealed class FixableData<T> {
  final T value;

  const FixableData(this.value);
}

final class FixableTodo extends FixableData<Todo> {
  const FixableTodo(super.value);
}

final class FixableList extends FixableData<TodoList> {
  const FixableList(super.value);
}

final class DoesNotExist extends FixableData<String> {
  const DoesNotExist(super.value);
}

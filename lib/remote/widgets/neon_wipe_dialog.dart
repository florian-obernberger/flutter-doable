import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:material_symbols_icons/symbols.dart';

import '/strings/strings.dart';

class NeonWipeDialog extends StatelessWidget {
  const NeonWipeDialog({super.key});

  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context)!;
    final ThemeData(colorScheme: scheme) = Theme.of(context);
    final DialogTheme(:titleTextStyle, :contentTextStyle) =
        DialogTheme.of(context);

    return PopScope(
      canPop: false,
      child: AlertDialog(
        titleTextStyle: titleTextStyle?.copyWith(
          color: scheme.onErrorContainer,
        ),
        contentTextStyle: contentTextStyle?.copyWith(
          color: scheme.onErrorContainer,
        ),
        backgroundColor: scheme.errorContainer,
        icon: const Icon(Symbols.crisis_alert),
        iconColor: scheme.error,
        title: Text(strings.nextcloudWipe),
        content: Text(strings.nextcloudWipeDescription),
        actions: <Widget>[
          FilledButton.icon(
            onPressed: () => context.pop(false),
            icon: const Icon(Symbols.keep),
            label: Text(strings.keepData),
          ),
          FilledButton.icon(
            style: FilledButton.styleFrom(
              backgroundColor: scheme.error,
              foregroundColor: scheme.onError,
            ),
            onPressed: () => context.pop(true),
            icon: const Icon(Symbols.delete_forever),
            label: Text(strings.wipeData),
          ),
        ],
      ),
    );
  }
}

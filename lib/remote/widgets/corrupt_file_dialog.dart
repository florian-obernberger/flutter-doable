import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:flutter_material_design_icons/flutter_material_design_icons.dart';
import 'package:result/anyhow.dart';

import '/strings/strings.dart';
import '/state/todo_db.dart';
import '/state/todo_list_db.dart';
import '/classes/logger.dart';
import '/util/extensions/iterable.dart';

import '../remote.dart';
import '../types.dart';

part 'corrupt_file_dialog.d/fixable_data.dart';

typedef OnRemoteException = void Function(
  BuildContext context,
  Error err, {
  Remote remote,
});

typedef TodoId = String;

enum CorruptDataType { todo, list, doesNotExist }

class CorruptFileDialog extends ConsumerStatefulWidget {
  const CorruptFileDialog({
    required this.remote,
    required this.ids,
    required this.onError,
    super.key,
  });

  CorruptFileDialog.single({
    required this.remote,
    required String id,
    required this.onError,
    super.key,
  }) : ids = <String>[id];

  final Remote remote;

  final List<String> ids;

  final OnRemoteException onError;

  @override
  ConsumerState<CorruptFileDialog> createState() => _CorruptFileDialogState();
}

class _CorruptFileDialogState extends ConsumerState<CorruptFileDialog> {
  bool isFixing = false;

  final fixed = StreamController<TodoId>();
  final unfixable = StreamController<TodoId>();

  late final idCount = widget.ids.length;
  final progress = ValueNotifier<double>(0.0);
  final done = ValueNotifier<bool>(false);

  @override
  void initState() {
    super.initState();

    done.addListener(() {
      if (!mounted) return;

      context.pop();

      final strings = Strings.of(context)!;
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        duration: const Duration(seconds: 2),
        dismissDirection: DismissDirection.vertical,
        margin: const EdgeInsets.fromLTRB(24, 8, 24, 18),
        content: Text(strings.fixedBrokenRemoteFile),
      ));
    });

    fixed.stream.listen((id) {
      progress.value = (progress.value + 1.0) / idCount;
    });

    unfixable.stream.listen((id) {
      if (!mounted) return;

      final strings = Strings.of(context)!;

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        duration: const Duration(seconds: 4),
        dismissDirection: DismissDirection.vertical,
        margin: const EdgeInsets.fromLTRB(24, 8, 24, 18),
        content: Text(strings.unfixableRemoteFile),
        action: SnackBarAction(
          label: strings.giveFeedback,
          onPressed: () => context.push('/feedback'),
        ),
      ));
    });
  }

  @override
  void dispose() {
    fixed.close();
    unfixable.close();
    done.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context)!;
    final scheme = Theme.of(context).colorScheme;

    return PopScope(
      canPop: false,
      child: AlertDialog(
        icon: Icon(MdiIcons.fileAlertOutline, color: scheme.error),
        title: Text(strings.whatWentWrong),
        content: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            // if (isFixing)
            //   ValueListenableBuilder(
            //     valueListenable: progress,
            //     builder: (context, progress, _) => LinearProgressIndicator(
            //       value: progress,
            //     ),
            //   ),
            Text(strings.fixBrokenRemoteFile),
          ],
        ),
        actions: <Widget>[
          TextButton(
            style: TextButton.styleFrom(foregroundColor: scheme.error),
            onPressed: isFixing ? null : Navigator.of(context).pop,
            child: Text(strings.cancel),
          ),
          FilledButton(
            style: FilledButton.styleFrom(
              backgroundColor: scheme.error,
              foregroundColor: scheme.onError,
            ),
            onPressed: fixData,
            child: Stack(
              alignment: Alignment.center,
              children: <Widget>[
                if (isFixing)
                  SizedBox(
                    height: 24,
                    width: 24,
                    child: CircularProgressIndicator(
                      strokeWidth: 2.5,
                      color: scheme.onError,
                      backgroundColor: Colors.transparent,
                    ),
                  ),
                Visibility(
                  visible: !isFixing,
                  maintainSize: true,
                  maintainAnimation: true,
                  maintainState: true,
                  child: Text(strings.doContinue),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Future<void> fixData() async {
    setState(() => isFixing = true);
    await _fixData();
    if (mounted) setState(() => isFixing = false);
  }

  Future<void> _fixData() async {
    final todoDb = ref.read(todoDatabaseProvider);
    final listDb = ref.read(todoListDatabaseProvider);

    final stream = Stream<Result<TodoId>>.fromFutures(
      widget.ids.map((id) async {
        final todo = todoDb.get(id);
        final list = listDb.get(id);

        final dataType = switch ((todo, list)) {
          (Todo(), null) => FixableTodo(todo!),
          (null, TodoList()) => FixableList(list!),
          (null, null) => DoesNotExist(id),
          _ => null,
        } as FixableData?; // I don't get why I have to do this…

        if (dataType == null) {
          logger.w(
            'Could not fix corrupt data',
            error: '$id is both a Todo and a TodoList',
          );

          return bail<TodoId>(
            RemoteException.corruptedFile(SingleIterable(id)),
          ).context('Could not fix ccorrupt data');
        }

        final Result<()> delete = await switch (dataType) {
          FixableTodo(:final value) => widget.remote.deleteTodo(value.id),
          FixableList(:final value) => widget.remote.deleteTodoList(value.id),
          DoesNotExist(:final value) => widget.remote
              .deleteTodo(value)
              .orElse((_) => widget.remote.deleteTodoList(value)),
        };

        if (delete case Err(:final error)) {
          if (error.downcast<NotFound>().isErr()) return Err(error);
        }

        if (dataType case DoesNotExist()) return Ok(id);

        return await switch (dataType) {
          FixableTodo(:final value) => widget.remote.pushTodo(value),
          FixableList(:final value) => widget.remote.pushTodoList(value),
          _ => throw const ExpectedException('Unreachable case'),
        }
            .map((_) => id);
      }),
    );

    await for (final res in stream) {
      switch (res) {
        case Ok(value: final id):
          fixed.add(id);
        case Err(:final error):
          final rootException = error.downcast<RemoteException>();
          switch (rootException) {
            case Ok(:CorruptedFile value):
              value.ids?.forEach(unfixable.add);
            case Ok():
              widget.onError(context, error, remote: widget.remote);
            case Err():
              continue;
          }
      }
    }

    done.value = true;
  }
}

import 'dart:io';
export 'dart:io';

import 'types.dart';

(RemoteExc, SocketException) handleSocketException(
  SocketException err, [
  RemoteExc Function()? onTimeOut,
]) =>
    (
      switch (err.osError?.errorCode) {
        // No IP adress associated
        // nearly always indicates that the user has no internet connection
        7 => RemoteException.noInternetConnection,
        // Software caused connection abort
        103 => RemoteException.connectionError,
        // Connection timed out
        110 => onTimeOut?.call() ?? RemoteException.connectionTimedOut,
        // Connection refused
        111 => RemoteException.connectionError,
        // Host is down
        _ => RemoteException.unknownError,
      },
      SocketException(err.message, osError: err.osError)
    );

(RemoteExc, HttpException) handleHttpException(
  HttpException err, [
  RemoteExc Function()? onTimeOut,
]) {
  final sanitized = HttpException(err.message);

  if (err.message.contains('timed out')) {
    return (
      onTimeOut?.call() ?? RemoteException.connectionTimedOut,
      sanitized,
    );
  }

  if (err.message.contains('closed before full header')) {
    return (RemoteException.connectionError, sanitized);
  }

  return (RemoteException.unknownError, sanitized);
}

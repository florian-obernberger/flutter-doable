import '/data/logger.dart';

typedef Id = String;
typedef Ids = Iterable<Id>;

typedef RemoteExc = RemoteException Function([Ids?]);

sealed class RemoteException implements Exception {
  final Ids? ids;

  const RemoteException([this.ids]);

  LogLevel get logLevel;
  String get logMessage;

  const factory RemoteException.wrongUserOrPassword([Ids ids]) =
      WrongUserOrPassword._;
  const factory RemoteException.wrongUrl([Ids? ids]) = WrongUrl._;
  const factory RemoteException.notFound([Ids? ids]) = NotFound._;
  const factory RemoteException.corruptedFile([Ids? ids]) = CorruptedFile._;
  const factory RemoteException.maintenance([Ids? ids]) = Maintenance._;
  const factory RemoteException.unavailable([Ids? ids]) = Unavailable._;
  const factory RemoteException.fileLocked([Ids? ids]) = FileLocked._;
  const factory RemoteException.unknownError([Ids? ids]) = UnknownError._;
  const factory RemoteException.noInternetConnection([Ids? ids]) =
      NoInternetConnection._;
  const factory RemoteException.connectionTimedOut([Ids? ids]) =
      ConnectionTimedOut._;
  const factory RemoteException.tooManyConnectionTimeOuts([Ids? ids]) =
      TooManyConnectionTimeOuts._;
  const factory RemoteException.internalError([Ids? ids]) = InternalError._;
  const factory RemoteException.connectionError([Ids? ids]) = ConnectionError._;
  const factory RemoteException.notNextcloud([Ids? ids]) = NotNextcloud._;
  const factory RemoteException.forbidden([Ids? ids]) = Forbidden._;
  const factory RemoteException.nextcloudWipeInstruction([Ids? ids]) =
      NextcloudWipeInstruction._;
  const factory RemoteException.unexpectedError([Ids? ids]) = UnexpectedError._;
  const factory RemoteException.multipleExceptions(
      List<RemoteException> exceptions) = MultipleExceptions._;

  @override
  String toString() {
    if (ids != null) return '$runtimeType: $ids';
    return '$runtimeType';
  }
}

final class WrongUserOrPassword extends RemoteException {
  const WrongUserOrPassword._([super.ids]);

  @override
  LogLevel get logLevel => LogLevel.error;

  @override
  String get logMessage => 'Wrong username or password';
}

final class WrongUrl extends RemoteException {
  const WrongUrl._([super.ids]);

  @override
  LogLevel get logLevel => LogLevel.warn;

  @override
  String get logMessage => 'Wrong url';
}

final class NotFound extends RemoteException {
  const NotFound._([super.ids]);

  @override
  LogLevel get logLevel => LogLevel.error;

  @override
  String get logMessage => 'File or folder does not exist';
}

final class CorruptedFile extends RemoteException {
  const CorruptedFile._([super.ids]);

  @override
  LogLevel get logLevel => LogLevel.error;

  @override
  String get logMessage => 'Backup file corrupted';
}

final class Maintenance extends RemoteException {
  const Maintenance._([super.ids]);

  @override
  LogLevel get logLevel => LogLevel.info;

  @override
  String get logMessage => 'Nextcloud is in maintenance';
}

final class Unavailable extends RemoteException {
  const Unavailable._([super.ids]);

  @override
  LogLevel get logLevel => LogLevel.info;

  @override
  String get logMessage => 'Server is unavailable';
}

final class FileLocked extends RemoteException {
  const FileLocked._([super.ids]);

  @override
  LogLevel get logLevel => LogLevel.debug;

  @override
  String get logMessage => 'Backup file is locked';
}

final class UnknownError extends RemoteException {
  const UnknownError._([super.ids]);

  @override
  LogLevel get logLevel => LogLevel.wtf;

  @override
  String get logMessage => 'Unknown error';
}

final class NoInternetConnection extends RemoteException {
  const NoInternetConnection._([super.ids]);

  @override
  LogLevel get logLevel => LogLevel.info;

  @override
  String get logMessage => 'No internet connection';
}

final class ConnectionTimedOut extends RemoteException {
  const ConnectionTimedOut._([super.ids]);

  @override
  LogLevel get logLevel => LogLevel.info;

  @override
  String get logMessage => 'Connection timed out';
}

final class TooManyConnectionTimeOuts extends RemoteException {
  const TooManyConnectionTimeOuts._([super.ids]);

  @override
  LogLevel get logLevel => LogLevel.warn;

  @override
  String get logMessage => 'Too many connection time outs';
}

final class InternalError extends RemoteException {
  const InternalError._([super.ids]);

  @override
  LogLevel get logLevel => LogLevel.wtf;

  @override
  String get logMessage => 'Internal error';
}

final class ConnectionError extends RemoteException {
  const ConnectionError._([super.ids]);

  @override
  LogLevel get logLevel => LogLevel.info;

  @override
  String get logMessage => 'Connection error';
}

final class NotNextcloud extends RemoteException {
  const NotNextcloud._([super.ids]);

  @override
  LogLevel get logLevel => LogLevel.info;

  @override
  String get logMessage => 'Not a Nextcloud server';
}

final class Forbidden extends RemoteException {
  const Forbidden._([super.ids]);

  @override
  LogLevel get logLevel => LogLevel.debug;

  @override
  String get logMessage => 'Forbidden';
}

final class NextcloudWipeInstruction extends RemoteException {
  const NextcloudWipeInstruction._([super.ids]);

  @override
  LogLevel get logLevel => LogLevel.warn;

  @override
  String get logMessage => 'Nextcloud server requested wipe of this device';
}

final class UnexpectedError extends RemoteException {
  const UnexpectedError._([super.ids]);

  @override
  LogLevel get logLevel => LogLevel.wtf;

  @override
  String get logMessage => 'Unexpected error';
}

final class MultipleExceptions extends RemoteException {
  final List<RemoteException> exceptions;

  const MultipleExceptions._(this.exceptions);

  @override
  Ids get ids => exceptions
      .where((exception) => exception.ids != null)
      .expand((exception) => exception.ids!);

  @override
  LogLevel get logLevel => LogLevel.error;

  @override
  String get logMessage => 'Multiple exceptions';
}

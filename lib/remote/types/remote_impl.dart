import '/state/settings.dart';

enum RemoteImpl {
  nextcloud,
  webDAV,
  nextcloudTasks;

  static Set<RemoteImpl> fromSettings(Settings settings) {
    final impls = <RemoteImpl>{};
    if (settings.backup.nextcloud.value) impls.add(nextcloud);
    if (settings.backup.webdav.value) impls.add(webDAV);

    return impls;
  }
}

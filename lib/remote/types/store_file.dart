import 'package:result/result.dart';

import '/data/todo.dart';
import '/data/todo_list.dart';

extension type StoreFile(Map<String, Store> _stores) {
  factory StoreFile.fromFile(String source) {
    final lines = source.split('\n');
    final stores = <String, Store>{};

    for (final line in lines) {
      final parsed = Store.fromString(line);
      if (parsed.isSome()) {
        final store = parsed.unwrap();
        stores[store.id] = store;
      }
    }

    return StoreFile(stores);
  }

  /// Weather the file contains a [Store] with matching id to [store].
  bool contains(String id) => _stores.containsKey(id);

  /// Weather the file contains all of the given [ids].
  bool containsAll(Iterable<String> ids) =>
      _stores.keys.toSet().containsAll(ids);

  Store? get(String id) => _stores[id];

  /// Returns `true` if the [store] has been added and `false` if it was
  /// already in the file.
  bool add(Store store) {
    if (_stores.containsKey(store.id)) return false;
    _stores[store.id] = store;
    return true;
  }

  /// Returns `true` if the [store]'s lastModified was updated and `false` if
  /// the [store] was added instead.
  bool update(Store store) {
    final old = _stores[store.id];

    _stores[store.id] = old?.copyWith(
          lastModified: store.lastModified,
        ) ??
        store;

    return old != null;
  }

  /// Updates all given stores or adds them.
  void updateAll(Iterable<Store> stores) => stores.forEach(update);

  /// Returns the [store] if it was removed.
  Store? remove(String id) => _stores.remove(id);

  String toStoreFile() {
    final lines = <String>[];
    for (final store in _stores.values) {
      lines.add(store.toStoreString());
    }

    return lines.join('\n');
  }

  /// Returns a new [StoreFile] without the given [ids].
  StoreFile without(Iterable<String> ids) {
    final stores = <String, Store>{};
    final blacklist = ids.toSet();

    for (final MapEntry(key: id, value: store) in _stores.entries) {
      if (blacklist.contains(id)) continue;
      stores[id] = store;
    }

    return StoreFile(stores);
  }

  /// Returns a new [StoreFile] only with the given [ids].
  StoreFile only(Iterable<String> ids) {
    final stores = <String, Store>{};
    final whitelist = ids.toSet();

    for (final MapEntry(key: id, value: store) in _stores.entries) {
      if (whitelist.contains(id)) stores[id] = store;
    }

    return StoreFile(stores);
  }

  /// Returns a new [StoreFile] where the given [test] holds.
  StoreFile where(bool Function(Store store) test) {
    final stores = Map.fromEntries(
      _stores.entries.where((store) => test(store.value)),
    );

    return StoreFile(stores);
  }

  Iterable<String> get ids => _stores.keys;

  bool get isEmpty => _stores.isEmpty;
  bool get isNotEmpty => _stores.isNotEmpty;
}

class Store {
  const Store({
    required this.id,
    required this.creationDate,
    required this.lastModified,
    required this.uploadDate,
  });

  factory Store.fromTodo(Todo todo, [DateTime? uploadDate]) => Store(
        id: todo.id,
        creationDate: todo.creationDate,
        lastModified: todo.lastModified,
        uploadDate: uploadDate ?? DateTime.now(),
      );

  factory Store.fromTodoList(TodoList list, [DateTime? uploadDate]) => Store(
        id: list.id,
        creationDate: list.creationDate,
        lastModified: list.lastModified,
        uploadDate: uploadDate ?? DateTime.now(),
      );

  static Option<Store> fromString(String source) {
    final parts = source.split(';');
    if (parts.length != 4) return const None();

    final [id, creationDateStr, lastModifiedStr, uploadDateStr] = parts;
    final creationDate = DateTime.tryParse(creationDateStr);
    final lastModified = DateTime.tryParse(lastModifiedStr);
    final uploadDate = DateTime.tryParse(uploadDateStr);

    if (creationDate == null || lastModified == null || uploadDate == null) {
      return const None();
    }

    return Some(Store(
      id: id,
      creationDate: creationDate,
      lastModified: lastModified,
      uploadDate: uploadDate,
    ));
  }

  final String id;
  final DateTime creationDate;
  final DateTime lastModified;
  final DateTime uploadDate;

  String toStoreString() {
    return '$id;'
        '${creationDate.toIso8601String()};'
        '${lastModified.toIso8601String()};'
        '${uploadDate.toIso8601String()}';
  }

  @override
  bool operator ==(Object other) => switch (other) {
        Store() => other.id == id,
        _ => false,
      };

  @override
  int get hashCode => id.hashCode;

  Store copyWith({
    String? id,
    DateTime? creationDate,
    DateTime? lastModified,
    DateTime? uploadDate,
  }) =>
      Store(
        id: id ?? this.id,
        creationDate: creationDate ?? this.creationDate,
        lastModified: lastModified ?? this.lastModified,
        uploadDate: uploadDate ?? this.uploadDate,
      );
}

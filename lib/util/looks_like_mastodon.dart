import 'regex_helper.dart';

// https://mastodon.foo.bar/@User
// https://mastodon.foo.bar/@User/43456787654678
// https://pleroma.foo.bar/users/User
// https://pleroma.foo.bar/users/9qTHT2ANWUdXzENqC0
// https://pleroma.foo.bar/notice/9sBHWIlwwGZi5QGlHc
// https://pleroma.foo.bar/objects/d4643c42-3ae0-4b73-b8b0-c725f5819207
// https://friendica.foo.bar/profile/user
// https://friendica.foo.bar/display/d4643c42-3ae0-4b73-b8b0-c725f5819207
// https://misskey.foo.bar/notes/83w6r388br (always lowercase)
// https://pixelfed.social/p/connyduck/391263492998670833
// https://pixelfed.social/connyduck
// https://gts.foo.bar/@goblin/statuses/01GH9XANCJ0TA8Y95VE9H3Y0Q2
// https://gts.foo.bar/@goblin
// https://foo.microblog.pub/o/5b64045effd24f48a27d7059f6cb38f5
//
// COPIED FROM https://github.com/tuskyapp/Tusky/blob/develop/app/src/main/java/com/keylesspalace/tusky/util/LinkHelper.kt
bool looksLikeMastodonUrl(String url) {
  final uri = Uri.tryParse(url);
  if (uri == null) return false;

  final path = uri.path;
  return path.matches(r'^/@[^/]+$'.toRegex()) ||
      path.matches(r'^/@[^/]+/\\d+$'.toRegex()) ||
      path.matches(r'^/users/[^/]+/statuses/\\d+$'.toRegex()) ||
      path.matches(r'^/users/\\w+$'.toRegex()) ||
      path.matches(r'^/notice/[a-zA-Z0-9]+$'.toRegex()) ||
      path.matches(r'^/objects/[-a-f0-9]+$'.toRegex()) ||
      path.matches(r'^/notes/[a-z0-9]+$'.toRegex()) ||
      path.matches(r'^/display/[-a-f0-9]+$'.toRegex()) ||
      path.matches(r'^/profile/\\w+$'.toRegex()) ||
      path.matches(r'^/p/\\w+/\\d+$'.toRegex()) ||
      path.matches(r'^/\\w+$'.toRegex()) ||
      path.matches(r'^/@[^/]+/statuses/[a-zA-Z0-9]+$'.toRegex()) ||
      path.matches(r'^/o/[a-f0-9]+$'.toRegex());
}

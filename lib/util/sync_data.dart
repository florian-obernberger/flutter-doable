import 'package:result/result.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';

import '/state/recently_deleted_db.dart';
import '/state/settings.dart';
import '/sync/base_sync.dart';
import '/sync/nextcloud_sync.dart';
import '/classes/logger.dart';
import '/classes/syncable.dart';

class SyncUpdate<T extends Syncable<T>> {
  const SyncUpdate({required this.updated, required this.deleted});

  final List<T> updated;
  final List<T> deleted;
}

@Deprecated('Transition to Remote')
FutureResult<SyncUpdate<T>?, SyncError> syncData<T extends Syncable<T>>({
  required BackupPreferences backup,
  required BaseSync<T> bs,
  required RecentlyDeletedDatabase rd,
  required Iterable<T> data,
  bool forceSync = false,
}) async {
  // Check if there is an active internet connection. If there isn't one, return.
  if (!await InternetConnectionChecker().hasConnection) {
    return const Err(SyncError.noInternetConnection);
  }

  final lastSync = backup.lastSync.value;
  final now = DateTime.now();

  if (!forceSync && lastSync != null) {
    final nextSync = lastSync.add(const Duration(minutes: 1));
    if (!nextSync.isBefore(now)) return const Ok(null);
  }

  backup.lastSync.value = now;

  final firstPull = await bs.pull();
  if (firstPull.isErr()) {
    // If the result is an error it could be the first time that we are pulling.
    // In this case a does-not-exits error should've been thrown so we check
    // for that. If that is the case we issue a push. Should this one fail, too,
    // the error was not caused by it being the initial pull.
    if (firstPull.unwrapErr() == SyncError.doesNotExist) {
      logger.i('Initializing files on ${bs.runtimeType}');

      if (bs is NextcloudSync) {
        final nc = bs as NextcloudSync;
        await nc.initSync();
      }

      return bs.push(data).map((_) => null).inspectErr((err) {
        logger.w(
          'An unexpected error occurred while doing the initial push to ${bs.runtimeType}',
          error: err,
        );
      });
    }

    return firstPull.map((_) => null);
  }

  final remote = <String, T>{for (var v in firstPull.unwrap()) v.id: v};
  final current = <String, T>{for (var v in data) v.id: v};
  final synced = <String, T>{};

  for (var MapEntry(key: id, value: value) in remote.entries) {
    if (rd.contains(id)) {
      // Skip this value since it was recently deleted.
      continue;
    }

    if (current.containsKey(id)) {
      final currentValue = current[id]!;

      // Check which of the two values was edited more recently and use that.
      if (currentValue.lastModified.isAfter(value.lastModified)) {
        synced[id] = currentValue;
      } else {
        synced[id] = value;
      }

      // Removing the value from the current list to ensure no double uses.
      current.remove(id);
    } else {
      // The value was not found on the remote which means it has to be a new
      // value.
      synced[id] = value;
    }
  }

  bool canDelete(MapEntry<String, T> entry) {
    if (rd.contains(entry.key)) return true;
    // If it had an initial sync it means that the Todo was not created just now.
    return entry.value.hadInitialSync;
  }

  final deleted = current.entries
      .where(canDelete)
      .map(
        (entry) => entry.value,
      )
      .toList();

  // Remove deleted Todos.
  current.removeWhere((key, value) => canDelete(MapEntry(key, value)));

  final updated = synced.values
      .map((value) => value.copyWith(hadInitialSync: true))
      .toList();
  updated.addAll(current.values);

  final push = await bs.push(updated);
  if (push.isErr()) {
    logger.i(
      'An error occurred while pushing to ${bs.runtimeType}',
      error: push.unwrapErr(),
    );

    return push.map((_) => null);
  }

  return Ok(SyncUpdate(updated: updated, deleted: deleted));
}

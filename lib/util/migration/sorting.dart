// ignore_for_file: deprecated_member_use_from_same_package

import '/state/settings.dart';
import '/types/sortable_element.dart';
import '/data/sorting/todo_sorter.dart';

typedef SortingPrefs = ({
  List<SortingGroup> groupBy,
  List<SortingMetric> sortBy
});

SortingGroup _fromSortableElement(SortableElement element) => switch (element) {
      SortableElement.starred => SortingGroup.starredNoDate,
      SortableElement.today => SortingGroup.today,
      SortableElement.overdue => SortingGroup.overdue,
      SortableElement.relevantDate => SortingGroup.date,
      SortableElement.noRelevantDate => SortingGroup.noDate,
    };

SortingPrefs migrateSorting(SortOrderPreferences sortOrder) {
  final groupBy = sortOrder.order.value.map(_fromSortableElement).toList();

  final order =
      sortOrder.newestFirst.value ? SortOrder.ascending : SortOrder.descending;
  final sortBy = [
    if (sortOrder.reflectChanges.value)
      SortingMetric(SortingAttr.lastEdited, order),
    SortingMetric(SortingAttr.creationDate, order),
  ];

  return (groupBy: groupBy, sortBy: sortBy);
}

import 'package:flutter/material.dart';

import '/widgets/text_selection_menu.dart';

TextSelectionMenu buildTextSelectionMenu(
  BuildContext _,
  EditableTextState editableTextState, {
  bool markdownEnabled = false,
}) =>
    TextSelectionMenu(
      editableTextState: editableTextState,
      anchors: editableTextState.contextMenuAnchors,
      textEditingValue: editableTextState.textEditingValue,
      contextMenuButtonItems: editableTextState.contextMenuButtonItems,
      copyEnabled: editableTextState.copyEnabled,
      markdownEnabled: markdownEnabled,
    );

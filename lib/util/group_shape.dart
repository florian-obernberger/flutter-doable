import 'package:flutter/widgets.dart';

class GroupBorderRadius extends BorderRadius {
  const GroupBorderRadius({
    Radius top = Radius.zero,
    Radius bottom = Radius.zero,
  }) : super.only(
          topLeft: top,
          topRight: top,
          bottomLeft: bottom,
          bottomRight: bottom,
        );
}

class GroupEdgeInsets extends EdgeInsets {
  const GroupEdgeInsets({super.top, super.bottom, double sides = 0})
      : super.only(left: sides, right: sides);
}

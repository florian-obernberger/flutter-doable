extension _StringExtension on String {
  String _sanitizeWhitespace() => replaceAll(RegExp(r'[ \t\r\f\v]+'), ' ');
  String _sanitizeNewLines() => replaceAllMapped(
        RegExp('(\n+)'),
        (match) => '  ' * (match.group(1)!.length),
      );

  String _replaceRegExpWithGroup(String pattern, int group) =>
      replaceAllMapped(RegExp(pattern), (match) => match.group(group)!);
}

String sanitizeText(String text, {required bool supportMarkdown}) {
  if (!supportMarkdown) return text._sanitizeWhitespace()._sanitizeNewLines();

  return text
      .replaceAllMapped(
        RegExp(r'```(\S*?)\n([\s\S]*?)```'),
        (match) {
          final code = match.group(2)!._sanitizeWhitespace();
          return '`$code`';
        },
      )
      .replaceAllMapped(RegExp(r'(((^|\n)> (.*))+)'), (match) {
        final quote = match
            .group(1)!
            .replaceAll(RegExp(r'(^|\n)> '), ' ')
            ._sanitizeWhitespace()
            .trim();
        return '"$quote"';
      })
      .replaceAllMapped(RegExp(r'(((\n|^)[-\+\*] \[([ xX])\] (.*))+)'),
          (match) {
        final list = match
            .group(1)!
            .replaceFirstMapped(RegExp(r'[-\+\*] \[([ xX])\] '),
                (match) => "<::>${match.group(1)! == ' ' ? '☐' : '✓'} ")
            .replaceAllMapped(RegExp(r'\n[-\+\*] \[([ xX])\] '),
                (match) => "</::>, <::>${match.group(1)! == ' ' ? '☐' : '✓'} ")
            ._sanitizeWhitespace();
        return '$list</::>';
      })
      .replaceAllMapped(RegExp(r'(((^|\n)[-\+\*] (.*))+)'), (match) {
        final list = match
            .group(1)!
            .replaceFirst(RegExp(r'[-\+\*] '), '')
            .replaceAll(RegExp(r'\n[-\+\*] '), ', ')
            ._sanitizeWhitespace();
        return list;
      })
      .replaceAllMapped(RegExp(r'(((^|\n)([0-9]+\.) (.*))+)'), (match) {
        final list = match
            .group(1)!
            .replaceFirst(RegExp(r'([0-9]+\.) '), '')
            .replaceAll(RegExp(r'\n([0-9]+\.) '), ', ')
            ._sanitizeWhitespace();
        return list;
      })
      .replaceAll(RegExp(r'(\n|^)(---|___)(\n|$)'), ' ')
      ._sanitizeWhitespace()
      ._sanitizeNewLines();
}

String sanitizeMarkdownText(String text, {required bool supportMarkdown}) {
  if (!supportMarkdown) return text._sanitizeWhitespace();

  return text
      ._sanitizeWhitespace()
      ._replaceRegExpWithGroup(r'(\n|^)\> (.*)', 2)
      ._replaceRegExpWithGroup(r'(\*\*\*|___)(.*?)(\*\*\*|___)', 2)
      ._replaceRegExpWithGroup(r'(\*\*|__)(.*?)(\*\*|__)', 2)
      ._replaceRegExpWithGroup(r'```(\S*?)\n([\s\S]*?)```', 2)
      ._replaceRegExpWithGroup(r'\\(.)', 1)
      ._replaceRegExpWithGroup(r'(==)(.*?)(==)', 2)
      ._replaceRegExpWithGroup(r'(`)(.*?)(`)', 2)
      ._replaceRegExpWithGroup(r'(\*|_)(.*?)(\*|_)', 2)
      ._replaceRegExpWithGroup(r'\[(.+?)\]\((.*?)\)', 1)
      ._replaceRegExpWithGroup(r'(~~)(.*?)(~~)', 2)
      ._sanitizeWhitespace();
}

import 'package:flutter/material.dart';

import '/strings/strings.dart';
import '/state/settings.dart';

Color getDateColor(
  ColorScheme colorScheme,
  DateTime dateTime,
  TimeOfDay? timeOfDay, {
  required bool done,
}) {
  DateTime date;
  if (timeOfDay != null) {
    date = DateTime(
      dateTime.year,
      dateTime.month,
      dateTime.day,
      timeOfDay.hour,
      timeOfDay.minute,
    );
    DateTime now = DateTime.now();

    if (date.isBefore(now) && !done) {
      return colorScheme.error;
    }
  } else {
    date = dateTime;
    if (date.isBeforeToday() && !done) {
      return colorScheme.error;
    }
  }

  if (date.isToday()) {
    return colorScheme.primary.withOpacity(0.9);
  } else {
    return colorScheme.onSurfaceVariant;
  }
}

String formatDateRange(
  DateTimePreferences dateTime,
  Strings strings,
  DateTimeRange range, {
  String? locale,
  bool useNames = true,
}) {
  final start = formatDate(dateTime, strings, range.start, locale, useNames);
  final end = formatDate(dateTime, strings, range.end, locale, useNames);

  return '$start - $end';
}

String formatDate(
  DateTimePreferences dateTime,
  Strings strings,
  DateTime date, [
  String? locale,
  // ignore: avoid_positional_boolean_parameters
  bool useNames = true,
]) {
  final now = DateTime.now();
  final dateFormat = dateTime.dateFormat.value;

  if (!useNames) return dateFormat.format(date);

  if (checkIfSameDay(now, date)) return strings.today;
  if (checkIfSameDay(now.add(const Duration(days: 1)), date)) {
    return strings.tomorrow;
  }
  if (checkIfSameDay(now.subtract(const Duration(days: 1)), date)) {
    return strings.yesterday;
  }

  return dateFormat.format(date);
}

String formatTime(
  DateTimePreferences dateTime,
  DateTime date,
) =>
    dateTime.timeFormat.value.format(date);

String formatTimeOfDay(
  DateTimePreferences dateTime,
  TimeOfDay time,
) =>
    dateTime.timeFormat.value.formatTimeOfDay(time);

String formatDateTime(
  DateTimePreferences dateTime,
  Strings strings,
  DateTime date, {
  String? locale,
  String separator = ' ',
  bool useNames = true,
}) {
  final timeFormat = dateTime.timeFormat.value;

  final dateString = formatDate(dateTime, strings, date, locale, useNames);
  final timeString = timeFormat.format(date);

  return dateString + separator + timeString;
}

bool checkIfSameDay(DateTime one, DateTime two) =>
    one.day == two.day && one.month == two.month && one.year == two.year;

bool checkIfIsBeforeToday(DateTime date) {
  DateTime now = DateTime.now();
  DateTime today = DateTime(now.year, now.month, now.day);
  if (date.year < today.year) {
    return true;
  } else if (date.year <= today.year && date.month < today.month) {
    return true;
  } else if (date.year <= today.year &&
      date.month <= today.month &&
      date.day < today.day) {
    return true;
  } else {
    return false;
  }
}

extension AccurateComparison on DateTime {
  bool isToday() {
    DateTime now = DateTime.now();
    DateTime today = DateTime(now.year, now.month, now.day);
    return isSameDay(today);
  }

  bool isBeforeToday() {
    DateTime now = DateTime.now();
    DateTime today = DateTime(now.year, now.month, now.day);
    if (year < today.year) {
      return true;
    } else if (year <= today.year && month < today.month) {
      return true;
    } else if (year <= today.year && month <= today.month && day < today.day) {
      return true;
    } else {
      return false;
    }
  }

  bool isSameDay(DateTime other) =>
      day == other.day && month == other.month && year == other.year;
}

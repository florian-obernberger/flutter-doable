/// I think the name says it all... xD
const _zeroWidthSpace = '​';

String formatText(String text, {required bool trimText}) {
  return trimText ? text.trim().replaceAll(_zeroWidthSpace, '') : text;
}

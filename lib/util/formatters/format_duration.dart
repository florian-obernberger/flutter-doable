import 'package:duration/duration.dart';
import 'package:duration/locale.dart';
import 'package:flutter/material.dart';

String formatDuration(
  BuildContext context,
  Duration duration, {
  bool abbreviated = false,
  bool applyGermanFix = true,
}) {
  return prettyDuration(
    duration,
    abbreviated: abbreviated,
    upperTersity: DurationTersity.day,
    locale: DurationLocale.fromLanguageCode(
          Localizations.localeOf(context).toLanguageTag(),
        ) ??
        const EnglishDurationLocale(),
  ).replaceAll('Tage', applyGermanFix ? 'Tagen' : 'Tage');
}

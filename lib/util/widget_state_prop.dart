import 'package:flutter/material.dart';

int _stateComparator(WidgetState a, WidgetState b) {
  if (a == WidgetState.selected) return -1;
  if (b == WidgetState.selected) return 1;

  return Enum.compareByIndex(a, b);
}

extension WidgetStateProp on Never {
  static WidgetStateProperty<T?> resolveWith<T>(
    Map<WidgetState, T> statesMap, {
    T? fallback,
    WidgetStateProperty<T?>? fallbackProperty,
    Comparator<WidgetState> stateComparator = _stateComparator,
  }) {
    return WidgetStateProperty.resolveWith<T?>((states) {
      if (statesMap.isEmpty) return null;
      if (states.isEmpty) return fallbackProperty?.resolve(states) ?? fallback;

      if (states.length == 1) {
        return statesMap[states.first] ??
            fallbackProperty?.resolve(states) ??
            fallback;
      }

      final sortedStates = states.toList()..sort(stateComparator);
      return statesMap[sortedStates.first] ??
          fallbackProperty?.resolve(states) ??
          fallback;
    });
  }
}

import 'package:flutter/material.dart';

import '/data/todo.dart';
import '/state/settings.dart';

import 'todo.dart';

enum TodoImportance {
  starred,
  date,
  not;

  bool toBool() => this != TodoImportance.not;
}

extension TodoImportanceExtension on Todo {
  /// Return all Todo Importance's in ordered way.
  ///
  /// Returns an empty iterator if there are no importance.
  static Iterable<TodoImportance> todoImportanceOrdered(
    Todo todo,
    GeneralPreferences general,
  ) sync* {
    final highToday = general.highlightToday.value;
    final highOverdue = general.highlightOverdue.value;

    final isImportant = todo.isImportant;

    if (isImportant) yield TodoImportance.starred;
    if (highToday && todo.isToday) yield TodoImportance.date;
    if (highOverdue && todo.isOverdue) yield TodoImportance.date;
  }

  static TodoImportance todoImportance(Todo todo, GeneralPreferences general) {
    final highToday = general.highlightToday.value;
    final highOverdue = general.highlightOverdue.value;

    final isImportant = todo.isImportant;

    if (isImportant) return TodoImportance.starred;
    if (highToday && todo.isToday) return TodoImportance.date;
    if (highOverdue && todo.isOverdue) return TodoImportance.date;
    return TodoImportance.not;
  }

  TodoImportance importanceOf(BuildContext context) =>
      todoImportance(this, Settings.of(context).general);

  Iterable<TodoImportance> orderedImportanceOf(BuildContext context) =>
      todoImportanceOrdered(this, Settings.of(context).general);
}

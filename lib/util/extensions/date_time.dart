extension DateTimeOperators on DateTime {
  bool operator <(DateTime other) =>
      copyWith(microsecond: 0).isBefore(other.copyWith(microsecond: 0));

  bool operator >(DateTime other) =>
      copyWith(microsecond: 0).isAfter(other.copyWith(microsecond: 0));

  bool operator >=(DateTime other) => this > other || this == other;
  bool operator <=(DateTime other) => this < other || this == other;

  DateTime operator -(Duration duration) => subtract(duration);
  DateTime operator +(Duration duration) => add(duration);

  Duration operator %(DateTime other) => difference(other);
}

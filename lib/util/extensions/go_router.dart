import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

extension GoRouterFromContext on BuildContext {
  GoRouter get goRouter => GoRouter.of(this);
}

extension GoRouterLocationExtension on GoRouter {
  String get location {
    final RouteMatch lastMatch = routerDelegate.currentConfiguration.last;
    final RouteMatchList matchList = lastMatch is ImperativeRouteMatch
        ? lastMatch.matches
        : routerDelegate.currentConfiguration;
    final String location = matchList.uri.toString();
    return location;
  }
}

extension GoRouterMaybePush on GoRouter {
  Future<T?> maybePush<T extends Object?>(
    String location, {
    Object? extra,
  }) async =>
      this.location == location ? null : push<T>(location, extra: extra);
}

extension GoRouterContextMaybePush on BuildContext {
  Future<T?> maybePush<T extends Object?>(String location, {Object? extra}) =>
      goRouter.maybePush(location, extra: extra);
}

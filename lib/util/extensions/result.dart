import 'dart:async';

import 'package:result/result.dart';

extension ResultOperatorExtensions<T, E> on Result<T, E> {
  Result<T, E> operator &(Result<T, E> other) => and(other);
  Result<T, E> operator |(Result<T, E> other) => or(other);
}

extension FutureResultOperatorExtensions<T, E> on FutureResult<T, E> {
  FutureResult<T, E> operator &(FutureOr<Result<T, E>> other) => and(other);
  FutureResult<T, E> operator |(FutureOr<Result<T, E>> other) => or(other);
}

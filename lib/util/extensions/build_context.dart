import 'package:result/result.dart';
import 'package:flutter/material.dart';

import '/state/settings.dart';
import '/util/launch.dart' as launch;

export '/util/launch.dart';

extension UrlLaunchExtension on BuildContext {
  FutureResult<(), launch.UrlLaunchException> launchUrl(
    String url, {
    bool? inAppBrowser,
  }) =>
      launch.launchUrl(
        url,
        inAppBrowser:
            inAppBrowser ?? Settings.of(this).general.inAppBrowser.value,
      );
}

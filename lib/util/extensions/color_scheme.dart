import 'package:flutter/material.dart';
import 'package:result/anyhow.dart';

import '../enum_from_string.dart';

extension ColorSchemeJson on ColorScheme {
  static Result<ColorScheme> fromJson(Map<String, dynamic> json) {
    try {
      return Ok(ColorScheme(
        brightness: enumFromString(
          Brightness.values,
          json['brightness'],
          Brightness.light,
        ),
        primary: Color(json['primary'] as int),
        onPrimary: Color(json['onPrimary'] as int),
        primaryContainer: Color(json['primaryContainer'] as int),
        onPrimaryContainer: Color(json['onPrimaryContainer'] as int),
        secondary: Color(json['secondary'] as int),
        onSecondary: Color(json['onSecondary'] as int),
        secondaryContainer: Color(json['secondaryContainer'] as int),
        onSecondaryContainer: Color(json['onSecondaryContainer'] as int),
        tertiary: Color(json['tertiary'] as int),
        onTertiary: Color(json['onTertiary'] as int),
        tertiaryContainer: Color(json['tertiaryContainer'] as int),
        onTertiaryContainer: Color(json['onTertiaryContainer'] as int),
        error: Color(json['error'] as int),
        errorContainer: Color(json['errorContainer'] as int),
        onError: Color(json['onError'] as int),
        onErrorContainer: Color(json['onErrorContainer'] as int),
        surface: Color(json['surface'] as int),
        onSurface: Color(json['onSurface'] as int),
        surfaceContainerHighest: Color(json['surfaceVariant'] as int),
        onSurfaceVariant: Color(json['onSurfaceVariant'] as int),
        outline: Color(json['outline'] as int),
        onInverseSurface: Color(json['onInverseSurface'] as int),
        inverseSurface: Color(json['inverseSurface'] as int),
        inversePrimary: Color(json['inversePrimary'] as int),
        shadow: Color(json['shadow'] as int),
        surfaceTint: Color(json['surfaceTint'] as int),
        outlineVariant: Color(json['outlineVariant'] as int),
        scrim: Color(json['scrim'] as int),
      ));
    } on Exception catch (e) {
      return bail<ColorScheme>(e)
          .context('Could not parse ColorScheme from Json');
    }
  }

  Map<String, dynamic> toJson() => exportToJson();

  Map<String, Object> exportToJson() {
    return {
      'brightness': brightness.name,
      'primary': primary.value,
      'onPrimary': onPrimary.value,
      'primaryContainer': primaryContainer.value,
      'onPrimaryContainer': onPrimaryContainer.value,
      'secondary': secondary.value,
      'onSecondary': onSecondary.value,
      'secondaryContainer': secondaryContainer.value,
      'onSecondaryContainer': onSecondaryContainer.value,
      'tertiary': tertiary.value,
      'onTertiary': onTertiary.value,
      'tertiaryContainer': tertiaryContainer.value,
      'onTertiaryContainer': onTertiaryContainer.value,
      'error': error.value,
      'errorContainer': errorContainer.value,
      'onError': onError.value,
      'onErrorContainer': onErrorContainer.value,
      'surface': surface.value,
      'onSurface': onSurface.value,
      'surfaceVariant': surfaceContainerHighest.value,
      'onSurfaceVariant': onSurfaceVariant.value,
      'outline': outline.value,
      'onInverseSurface': onInverseSurface.value,
      'inverseSurface': inverseSurface.value,
      'inversePrimary': inversePrimary.value,
      'shadow': shadow.value,
      'surfaceTint': surfaceTint.value,
      'outlineVariant': outlineVariant.value,
      'scrim': scrim.value,
    };
  }
}

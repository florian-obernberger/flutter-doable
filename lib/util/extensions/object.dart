extension ApplyFunction<T> on T {
  S apply<S>(S Function(T value) func) => func(this);
}

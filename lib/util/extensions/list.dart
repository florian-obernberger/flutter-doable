import '/data/todo.dart';

extension TodoListPartsExtension on List<Todo> {
  Iterable<Todo> get completed => where((todo) => todo.isCompleted);
  Iterable<Todo> get uncompleted => where((todo) => !todo.isCompleted);
}

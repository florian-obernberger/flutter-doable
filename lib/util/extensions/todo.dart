import '/data/todo.dart';

import '../formatters/format_date.dart';

extension OverdueExtension on Todo {
  bool get isOverdue {
    if (isCompleted || relevantDate == null) return false;
    if (relevantTime != null) return relevantDateTime!.isBefore(DateTime.now());
    return relevantDate!.isBeforeToday();
  }
}

extension TodayExtension on Todo {
  bool get isToday {
    if (isCompleted || relevantDate == null) return false;
    return relevantDate!.isToday();
  }
}

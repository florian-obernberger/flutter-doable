import 'package:flutter/material.dart';
import 'package:flex_seed_scheme/flex_seed_scheme.dart';

import '/theme/color_schemes.dart';

extension ListColorExtension on Color {
  ColorScheme listSchemeFromBrightness(Brightness brightness) {
    if ({red, green, blue}.length == 1) {
      return brightness == Brightness.light
          ? monochromeLightScheme
          : monochromeDarkScheme;
    }

    return SeedColorScheme.fromSeeds(
      primaryKey: this,
      brightness: brightness,
      tones: FlexTones.soft(brightness),
    );
  }

  ColorScheme listSchemeOf(BuildContext context) => listSchemeFromBrightness(
        Theme.of(context).colorScheme.brightness,
      );

  ({ColorScheme lightScheme, ColorScheme darkScheme}) listSchemes() {
    return (
      lightScheme: listSchemeFromBrightness(Brightness.light),
      darkScheme: listSchemeFromBrightness(Brightness.dark),
    );
  }
}

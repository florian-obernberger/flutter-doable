import 'package:flutter/material.dart';

import '/data/todo.dart';

import 'date_time.dart';

extension TodoDateTimeRange on DateTimeRange {
  bool containsTodo(Todo todo) {
    final date = todo.relevantDateTime;
    if (date == null) return false;

    return start <= date && date <= end;
  }
}

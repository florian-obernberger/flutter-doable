extension AddSlashUrlExtension on String {
  String withTrailingSlash() {
    if (endsWith('/')) return this;
    return '$this/';
  }
}

extension StringParseExtension on String {
  T parse<T>() => switch (T) {
        const (double) => double.parse(this),
        const (int) => int.parse(this),
        const (num) => num.parse(this),
        const (bool) => bool.parse(this),
        const (DateTime) => DateTime.parse(this),
        const (Uri) => Uri.parse(this),
        _ => throw UnimplementedError('Parsing not implemented for type $T'),
      } as T;

  T? tryParse<T>() => switch (T) {
        const (double) => double.tryParse(this),
        const (int) => int.tryParse(this),
        const (num) => num.tryParse(this),
        const (bool) => bool.tryParse(this),
        const (DateTime) => DateTime.tryParse(this),
        const (Uri) => Uri.tryParse(this),
        _ => throw UnimplementedError('Parsing not implemented for type $T'),
      } as T?;
}

extension RemoveStringPartsExtension on String {
  String removeFirst(Pattern pattern) => replaceFirst(pattern, '');
  String removeAll(Pattern pattern) => replaceAll(pattern, '');
}

extension CapitalizedStringExtension on String {
  String toCapitalized({bool applyToAllWords = false}) {
    if (applyToAllWords) {
      return split(' ').map((string) => string.toCapitalized()).join(' ');
    }

    return '${this[0].toUpperCase()}${substring(1)}';
  }
}

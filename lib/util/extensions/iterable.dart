import 'package:result/result.dart';

extension Enumeration<T> on Iterable<T> {
  Iterable<(int, T)> enumerate([int start = 0]) sync* {
    for (var elem in this) {
      yield (start, elem);
      start++;
    }
  }
}

extension FunctionalIterable<E> on Iterable<E> {
  Iterable<E> get tail => skip(1);

  Iterable<(E, T)> zip<T>(Iterable<T> other) => _ZippedIterable(this, other);

  Option<E> find(bool Function(E element) test) {
    for (final value in this) {
      if (test(value)) return Some(value);
    }

    return const None();
  }

  Map<K, List<E>> groupBy<K>(K Function(E element) keyFunction) {
    final groups = <K, List<E>>{};

    for (final value in this) {
      groups.update(
        keyFunction(value),
        (list) => list..add(value),
        ifAbsent: () => <E>[value],
      );
    }

    return groups;
  }

  Map<K, E> mapBy<K>(K Function(E element) keyFunction) => Map.fromEntries(map(
        (e) => MapEntry(keyFunction(e), e),
      ));

  Iterable<E> precededBy(Iterable<E> other) => other.followedBy(this);

  Iterable<E> precedeWith(E element) sync* {
    yield element;
    yield* this;
  }

  Iterable<E> followWith(E element) sync* {
    yield* this;
    yield element;
  }
}

extension FlattenMap<K, V> on Iterable<MapEntry<K, V>> {
  Map<K, List<V>> collectGrouped() {
    final flattened = <K, List<V>>{};

    for (final entry in this) {
      flattened.putIfAbsent(entry.key, () => <V>[]).add(entry.value);
    }

    return flattened;
  }
}

extension Numbers on Iterable<num> {
  num max() => fold(first, (p, e) => e > p ? e : p);
  num min() => fold(first, (p, e) => e < p ? e : p);
  num sum([num? start]) => fold(start ?? 0, (p, e) => p + e);

  /// Calculate the average value.
  double average() => sum() / length;

  /// Calculate the mean (average) value.
  double mean() => average();

  /// The data value separating the upper half of a data set from the lower
  /// half.
  double median() {
    if (length == 1) return first.toDouble();
    if (length == 2) return average();

    var data = toList(growable: false);
    data.sort();

    final i = data.length ~/ 2;

    if (length % 2 == 0) {
      return (data[i] + data[i - 1]) / 2;
    } else {
      return data[i].toDouble();
    }
  }
}

extension NumbersInt on Iterable<int> {
  int max() => fold(first, (p, e) => e > p ? e : p);
  int min() => fold(first, (p, e) => e < p ? e : p);
  int sum([int? start]) => fold(start ?? 0, (p, e) => p + e);
}

extension NumbersDouble on Iterable<double> {
  double max() => fold(first, (p, e) => e > p ? e : p);
  double min() => fold(first, (p, e) => e < p ? e : p);
  double sum([double? start]) => fold(start ?? 0, (p, e) => p + e);
}

class _ZippedIterable<E, T> extends Iterable<(E, T)> {
  final Iterable<E> _iterable1;
  final Iterable<T> _iterable2;

  const _ZippedIterable(this._iterable1, this._iterable2);

  @override
  Iterator<(E, T)> get iterator => _ZippedIterator(
        _iterable1.iterator,
        _iterable2.iterator,
      );
}

class _ZippedIterator<E, T> implements Iterator<(E, T)> {
  final Iterator<E> _iterator1;
  final Iterator<T> _iterator2;

  const _ZippedIterator(this._iterator1, this._iterator2);

  @override
  bool moveNext() => _iterator1.moveNext() && _iterator2.moveNext();

  @override
  (E, T) get current => (_iterator1.current, _iterator2.current);
}

class SingleIterable<T> extends Iterable<T> {
  final T value;

  const SingleIterable(this.value);

  @override
  Iterator<T> get iterator => _SingleIterator(value);
}

class _SingleIterator<T> implements Iterator<T> {
  final T _value;
  bool _used;

  _SingleIterator(this._value) : _used = false;

  @override
  bool moveNext() => !_used && (_used = true);

  @override
  T get current => _value;
}

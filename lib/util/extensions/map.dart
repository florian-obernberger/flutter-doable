extension MapExtensions<K, V> on Map<K, V> {
  V putIfAbsentAndUpdate(
    K key, {
    required V Function(V value) update,
    required V Function() ifAbsent,
  }) {
    putIfAbsent(key, ifAbsent);
    return this.update(key, update);
  }

  Map<K, V2> mapValues<V2>(V2 Function(V value) converter) =>
      map((key, value) => MapEntry(key, converter(value)));

  Map<K2, V> mapKeys<K2>(K2 Function(K value) converter) =>
      map((key, value) => MapEntry(converter(key), value));

  Map<K2, V2> mapEntries<K2, V2>((K2, V2) Function(K, V) converter) =>
      map((key, value) {
        final (key2, value2) = converter(key, value);
        return MapEntry(key2, value2);
      });
}

import '/strings/strings.dart';

extension QuoteStrings on Strings {
  String quote(Object source, {bool singleQuote = false}) {
    final buffer = StringBuffer()
      ..write(singleQuote ? quoteSingleLeft : quoteDoubleLeft)
      ..write(source)
      ..write(singleQuote ? quoteSingleRight : quoteDoubleRight);

    return buffer.toString();
  }
}

import 'package:flutter/widgets.dart';

import '/types/app_language.dart';

String localizedChangelog(BuildContext context) {
  final lang = AppLanguage.of(context);

  switch (lang) {
    case AppLanguage.german:
      return 'de';
    case AppLanguage.spanish:
      return 'es';
    default:
      return 'en-US';
  }
}

int localizedChangelogPosition(BuildContext context) {
  final lang = AppLanguage.of(context);

  switch (lang) {
    case AppLanguage.german:
      return 1;
    case AppLanguage.spanish:
      return 2;
    default:
      return 0;
  }
}

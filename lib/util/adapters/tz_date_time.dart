import 'package:hive_flutter/hive_flutter.dart';
import 'package:timezone/timezone.dart';

const _typeId = 100;

final class TimeZoneAdapter extends TypeAdapter<TimeZone> {
  @override
  TimeZone read(BinaryReader reader) => TimeZone(
        reader.readInt(),
        isDst: reader.readBool(),
        abbreviation: reader.readString(),
      );

  @override
  void write(BinaryWriter writer, TimeZone obj) {
    writer
      ..writeInt(obj.offset)
      ..writeBool(obj.isDst)
      ..writeString(obj.abbreviation);
  }

  @override
  int get typeId => _typeId + 0;
}

final class LocationAdapter extends TypeAdapter<Location> {
  @override
  Location read(BinaryReader reader) => Location(
        reader.readString(),
        reader.readIntList(),
        reader.readIntList(),
        reader.readList().cast<TimeZone>(),
      );

  @override
  void write(BinaryWriter writer, Location obj) {
    writer
      ..writeString(obj.name)
      ..writeIntList(obj.transitionAt)
      ..writeIntList(obj.transitionZone)
      ..writeList(obj.zones);
  }

  @override
  int get typeId => _typeId + 1;
}

final class TZDateTimeAdapter extends TypeAdapter<TZDateTime> {
  @override
  TZDateTime read(BinaryReader reader) => TZDateTime.from(
        DateTime.fromMillisecondsSinceEpoch(reader.readInt()),
        reader.read() as Location,
      );

  @override
  void write(BinaryWriter writer, TZDateTime obj) {
    writer
      ..writeInt(obj.millisecondsSinceEpoch)
      ..write(obj.location);
  }

  @override
  int get typeId => _typeId + 2;
}

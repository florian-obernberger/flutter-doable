import 'package:flutter_riverpod/flutter_riverpod.dart';

import '/strings/strings.dart';
import '/state/settings.dart';
import '/state/todo_db.dart';
import '/state/todo_list_db.dart';
import '/data/todo.dart';
import '/data/sorting/todo_sorter.dart';
import '/data/todo_list.dart';

import 'extensions/list.dart';
import 'formatters/format_date.dart';

String todosToHumanReadableString({
  required WidgetRef ref,
  required Strings strings,
  required bool showListNames,
  required Iterable<String> todoIds,
}) {
  final settings = Settings.of(ref.context);
  final sorter = TodoSorter.from(settings.sorting);

  final sorted = sorter.sort(
    ref.read(todoDatabaseProvider).getAll(todoIds).toList(),
  );
  final todos = [...sorted.completed, ...sorted.uncompleted];

  final todoListDb = ref.read(todoListDatabaseProvider);

  final stringifiedTodos = <String>[];

  for (final todo in todos) {
    final showList = showListNames &&
        settings.extensions.enableLists.value &&
        todo.listId != null;
    final todoList = showList ? todoListDb.get(todo.listId!) : null;

    stringifiedTodos.add(
      _todoToString(
        settings.dateTime,
        todo,
        strings: strings,
        showList: showList && todoList != null,
        todoList: todoList,
      ),
    );
  }

  return stringifiedTodos.join('\n\n\n');
}

String _todoToString(
  DateTimePreferences dateTime,
  Todo todo, {
  required Strings strings,
  required bool showList,
  required TodoList? todoList,
}) {
  final star = todo.isImportant ? '⭐ ' : '';
  final check = todo.isCompleted
      ? star.isEmpty
          ? '✅ '
          : '✅'
      : '';
  final list = showList ? '${todoList!.name}: ' : '';
  final title = '$check$star$list${todo.title}';
  String formatted = '';

  if (todo.relevantDate != null) {
    formatted = todo.relevantTime != null
        ? formatDateTime(dateTime, strings, todo.relevantDateTime!)
        : formatDate(dateTime, strings, todo.relevantDate!);
    formatted = '\n$formatted';
  }

  final description = todo.description != null ? '\n\n${todo.description}' : '';

  return '$title$formatted$description';
}

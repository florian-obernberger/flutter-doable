import 'package:flutter/material.dart';

String saveColor(Color color) {
  return "0x${color.value.toRadixString(16).padLeft(8, '0')}";
}

Color loadColor(String color) {
  return Color(int.parse(color));
}

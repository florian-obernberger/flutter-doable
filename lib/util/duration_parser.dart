/// Parses a [String] to a [Duration].
///
/// **Everything smaller than seconds will be ignored!**
///
/// This is simply because I do not need that!
Duration parseDuration(String duration) {
  final parts = duration.split(':');

  return Duration(
    hours: int.parse(parts[0]),
    minutes: int.parse(parts[1]),
    seconds: int.parse(parts[2].split('.').first),
  );
}

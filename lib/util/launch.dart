import 'package:result/result.dart';
import 'package:url_launcher/url_launcher_string.dart';

import '/classes/logger.dart';

class UrlLaunchException implements Exception {
  const UrlLaunchException();
}

FutureResult<(), UrlLaunchException> launchUrl(
  String url, {
  required bool inAppBrowser,
}) async {
  bool isMailOrPhone = false;

  if (url.startsWith('mailto:') || url.startsWith('tel:')) {
    isMailOrPhone = true;
  } else if (!url.startsWith(RegExp(r'https?://'))) {
    url = 'https://$url';
  }

  try {
    await launchUrlString(url,
        mode: inAppBrowser && !isMailOrPhone
            ? LaunchMode.inAppBrowserView
            : LaunchMode.externalApplication);

    return const Ok(());
  } on Exception catch (e, s) {
    const custom = 'in-app browser';
    const extern = 'external browser';

    logger.e(
      e,
      message:
          'Tried launching $url in an ${inAppBrowser ? custom : extern} but encountered an error.',
      stackTrace: s,
    );

    return const Err(UrlLaunchException());
  }
}

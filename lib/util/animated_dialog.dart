// ignore: dangling_library_doc_comments
/// This code was originally created by Jack Jonson <yswing1@gmail.com>
/// This code is licenses under the Apache-2.0 license and can be found
/// here: https://github.com/JackJonson/flutter_animated_dialog
///
/// I changed and adapted the code as follows:
///
/// 1. I adapted the code to the latest version of Dart and made it compliant
///    Sound Null Safety.
/// 2. I modified the code so that it is possible to specify different durations
///    for entry and exit.
/// 3. I removed some of the animation types.
/// 4. Reduce motion if required.
///
/// Last edited: 15.02.2023, 11:13pm.
///

import 'dart:async';

import 'package:flutter/material.dart';

import '/state/settings.dart';
import '/classes/motion.dart';

///Is dialog showing
bool isShowing = false;

enum DialogTransitionType {
  ///Fade animation
  fade,

  ///Slide from top animation
  slideFromTop,

  ///Slide from top fade animation
  slideFromTopFade,

  ///Slide from bottom animation
  slideFromBottom,

  ///Slide from bottom fade animation
  slideFromBottomFade,

  ///Slide from left animation
  slideFromLeft,

  ///Slide from left fade animation
  slideFromLeftFade,

  ///Slide from right animation
  slideFromRight,

  ///Slide from right fade animation
  slideFromRightFade,

  ///Scale animation
  scale,

  ///Fade scale animation
  fadeScale,

  ///Size animation
  size,

  ///Size fade animation
  sizeFade,

  ///No animation
  none,
}

/// Displays a Material dialog above the current contents of the app
Future<T?> showAnimatedDialog<T>({
  required BuildContext context,
  bool barrierDismissible = true,
  bool useSafeArea = true,
  required WidgetBuilder builder,
  DialogTransitionType animationType = DialogTransitionType.slideFromBottomFade,
  Curve curve = Curves.easeInOutCubicEmphasized,
  Duration duration = Motion.long2,
  Duration? reverseDuration = Motion.short4,
  Alignment alignment = Alignment.center,
  Axis? axis,
}) {
  assert(debugCheckHasMaterialLocalizations(context));

  final ThemeData theme = Theme.of(context);
  final reduceMotion = Settings.of(context).accessability.reduceMotion.value;

  isShowing = true;
  return _showAnimatedDialog<T>(
    context: context,
    useSafeArea: useSafeArea,
    pageBuilder: (BuildContext buildContext, Animation<double> animation,
        Animation<double> secondaryAnimation) {
      final Widget pageChild = Builder(builder: builder);
      return SafeArea(
        top: false,
        bottom: useSafeArea,
        left: useSafeArea,
        right: useSafeArea,
        child: Builder(builder: (BuildContext context) {
          return Theme(data: theme, child: pageChild);
        }),
      );
    },
    barrierDismissible: barrierDismissible,
    barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
    barrierColor: Colors.black54,
    transitionDuration: duration,
    reverseTransitionDuration: reverseDuration,
    transitionBuilder: (BuildContext context, Animation<double> animation,
        Animation<double> secondaryAnimation, Widget child) {
      if (reduceMotion) {
        return FadeTransition(
          opacity: CurveTween(curve: curve).animate(animation),
          child: child,
        );
      }

      switch (animationType) {
        case DialogTransitionType.fade:
          return FadeTransition(
            opacity: CurveTween(curve: curve).animate(animation),
            child: child,
          );
        case DialogTransitionType.slideFromRight:
          return SlideTransition(
            transformHitTests: false,
            position: Tween<Offset>(
              begin: const Offset(1.0, 0.0),
              end: Offset.zero,
            ).chain(CurveTween(curve: curve)).animate(animation),
            child: child,
          );
        case DialogTransitionType.slideFromLeft:
          return SlideTransition(
            transformHitTests: false,
            position: Tween<Offset>(
              begin: const Offset(-1.0, 0.0),
              end: Offset.zero,
            ).chain(CurveTween(curve: curve)).animate(animation),
            child: child,
          );
        case DialogTransitionType.slideFromRightFade:
          return SlideTransition(
            position: Tween<Offset>(
              begin: const Offset(1.0, 0.0),
              end: Offset.zero,
            ).chain(CurveTween(curve: curve)).animate(animation),
            child: FadeTransition(
              opacity: CurveTween(curve: curve).animate(animation),
              child: child,
            ),
          );
        case DialogTransitionType.slideFromLeftFade:
          return SlideTransition(
            position: Tween<Offset>(
              begin: const Offset(-1.0, 0.0),
              end: Offset.zero,
            ).chain(CurveTween(curve: curve)).animate(animation),
            child: FadeTransition(
              opacity: CurveTween(curve: curve).animate(animation),
              child: child,
            ),
          );
        case DialogTransitionType.slideFromTop:
          return SlideTransition(
            transformHitTests: false,
            position: Tween<Offset>(
              begin: const Offset(0.0, -1.0),
              end: Offset.zero,
            ).chain(CurveTween(curve: curve)).animate(animation),
            child: child,
          );
        case DialogTransitionType.slideFromTopFade:
          return SlideTransition(
            position: Tween<Offset>(
              begin: const Offset(0.0, -1.0),
              end: Offset.zero,
            ).chain(CurveTween(curve: curve)).animate(animation),
            child: FadeTransition(
              opacity: CurveTween(curve: curve).animate(animation),
              child: child,
            ),
          );
        case DialogTransitionType.slideFromBottom:
          return SlideTransition(
            transformHitTests: false,
            position: Tween<Offset>(
              begin: const Offset(0.0, 1.0),
              end: Offset.zero,
            ).chain(CurveTween(curve: curve)).animate(animation),
            child: child,
          );
        case DialogTransitionType.slideFromBottomFade:
          return SlideTransition(
            position: Tween<Offset>(
              begin: const Offset(0.0, 1.0),
              end: Offset.zero,
            ).chain(CurveTween(curve: curve)).animate(animation),
            child: FadeTransition(
              opacity: CurveTween(curve: curve).animate(animation),
              child: child,
            ),
          );
        case DialogTransitionType.scale:
          return ScaleTransition(
            alignment: alignment,
            scale: CurvedAnimation(
              parent: animation,
              curve: Interval(
                0.00,
                0.50,
                curve: curve,
              ),
            ),
            child: child,
          );
        case DialogTransitionType.fadeScale:
          return ScaleTransition(
            alignment: alignment,
            scale: CurvedAnimation(
              parent: animation,
              curve: Interval(
                0.00,
                0.50,
                curve: curve,
              ),
            ),
            child: FadeTransition(
              opacity: CurveTween(curve: curve).animate(animation),
              child: child,
            ),
          );
        case DialogTransitionType.size:
          return Align(
            alignment: alignment,
            child: SizeTransition(
              sizeFactor: CurvedAnimation(
                parent: animation,
                curve: curve,
              ),
              axis: axis ?? Axis.vertical,
              child: child,
            ),
          );
        case DialogTransitionType.sizeFade:
          return Align(
            alignment: alignment,
            child: SizeTransition(
              sizeFactor: CurvedAnimation(
                parent: animation,
                curve: curve,
              ),
              child: FadeTransition(
                opacity: CurveTween(curve: curve).animate(animation),
                child: child,
              ),
            ),
          );
        case DialogTransitionType.none:
          return child;
        default:
          return FadeTransition(
            opacity: CurveTween(curve: curve).animate(animation),
            child: child,
          );
      }
    },
  );
}

class AnimatedDialogRoute<T> extends RawDialogRoute<T> {
  AnimatedDialogRoute({
    required BuildContext context,
    required RoutePageBuilder pageBuilder,
    bool useSafeArea = true,
    CapturedThemes? themes,
    super.barrierDismissible = true,
    super.barrierColor = const Color(0x80000000),
    String? barrierLabel,
    super.transitionDuration = const Duration(milliseconds: 500),
    Duration? reverseTransitionDuration = const Duration(milliseconds: 200),
    super.transitionBuilder,
    super.settings,
    super.anchorPoint,
    super.traversalEdgeBehavior,
  })  : _reverseTransitionDuration =
            reverseTransitionDuration ?? transitionDuration,
        super(
          pageBuilder: (BuildContext buildContext, Animation<double> animation,
              Animation<double> secondaryAnimation) {
            final Widget pageChild = Builder(
              builder: (context) => pageBuilder(
                context,
                animation,
                secondaryAnimation,
              ),
            );
            Widget dialog = themes?.wrap(pageChild) ?? pageChild;
            if (useSafeArea) {
              dialog = SafeArea(child: dialog);
            }
            return dialog;
          },
          barrierLabel: barrierLabel ??
              MaterialLocalizations.of(context).modalBarrierDismissLabel,
        );

  final Duration _reverseTransitionDuration;

  @override
  Duration get reverseTransitionDuration => this._reverseTransitionDuration;
}

Future<T?> _showAnimatedDialog<T extends Object?>({
  required BuildContext context,
  required RoutePageBuilder pageBuilder,
  bool useSafeArea = true,
  bool barrierDismissible = false,
  String? barrierLabel,
  Color barrierColor = const Color(0x80000000),
  Duration transitionDuration = const Duration(milliseconds: 500),
  Duration? reverseTransitionDuration = const Duration(milliseconds: 200),
  RouteTransitionsBuilder? transitionBuilder,
  bool useRootNavigator = true,
  RouteSettings? routeSettings,
  Offset? anchorPoint,
}) {
  assert(!barrierDismissible || barrierLabel != null);
  return Navigator.of(context, rootNavigator: useRootNavigator)
      .push<T>(AnimatedDialogRoute<T>(
    context: context,
    useSafeArea: useSafeArea,
    pageBuilder: pageBuilder,
    barrierDismissible: barrierDismissible,
    barrierLabel: barrierLabel,
    barrierColor: barrierColor,
    transitionDuration: transitionDuration,
    reverseTransitionDuration: reverseTransitionDuration,
    transitionBuilder: transitionBuilder,
    settings: routeSettings,
    anchorPoint: anchorPoint,
  ));
}

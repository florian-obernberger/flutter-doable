T? maybeEnumFromString<T extends Enum>(List<T> values, String source) {
  for (T value in values) {
    if (value.name == source) return value;
  }

  return null;
}

T enumFromString<T extends Enum>(
        List<T> values, String source, T defaultValue) =>
    maybeEnumFromString(values, source) ?? defaultValue;

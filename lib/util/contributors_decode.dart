import 'package:result/result.dart';

class Contributor implements Comparable<Contributor> {
  final String name;
  final String link;
  final List<String> roles;

  const Contributor({
    required this.name,
    required this.link,
    required this.roles,
  });

  static Option<Contributor> fromString(String source) {
    source = source.trim();

    final roles = <String>[];
    String name;
    String link;
    try {
      if (source.startsWith('(')) {
        roles.addAll(source
            .substring(1, source.indexOf(')'))
            .trim()
            .split(RegExp(r', |,')));
        source = source.replaceFirst(RegExp(r'\(.*?\)'), '');
      }

      link = RegExp('<(.*?)>').firstMatch(source)!.group(1)!.trim();
      source = source.replaceAll('<$link>', '');
      name = source.trim();
    } on Exception catch (_) {
      return const None();
    }

    return Some(Contributor(name: name, link: link, roles: roles));
  }

  static List<Contributor> fromFile(String source) {
    final contributors = <Contributor>[];

    for (final line in source.split('\n')) {
      if (line.startsWith('#')) continue;
      if (line.isEmpty) continue;

      if (Contributor.fromString(line) case Some(value: final contributor)) {
        contributors.add(contributor);
      }
    }

    return contributors;
  }

  @override
  String toString() => '(${roles.join(', ')}) $name <$link>';

  @override
  int compareTo(Contributor other) {
    int compare = name.toLowerCase().compareTo(other.name.toLowerCase());
    if (compare != 0) return compare;

    compare = link.compareTo(other.link);
    if (compare != 0) return compare;

    return roles.length.compareTo(other.roles.length);
  }
}

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '/strings/strings.dart';

import '/classes/system_info.dart';

Future<void> copyText(BuildContext context, String text) async {
  final strings = Strings.of(context)!;
  final messanger = ScaffoldMessenger.of(context);

  await HapticFeedback.mediumImpact();
  await Clipboard.setData(ClipboardData(text: text));

  await SystemInfo.instance.init();

  if (!SystemInfo.instance.hasClipboardPopup) {
    messanger.showSnackBar(
      SnackBar(
        duration: const Duration(seconds: 2),
        dismissDirection: DismissDirection.vertical,
        margin: const EdgeInsets.fromLTRB(24, 8, 24, 18),
        content: Text(strings.copiedToClipboard),
      ),
    );
  }
}

extension RegExpMatcher on String {
  bool matches(RegExp pattern) {
    return pattern.hasMatch(this);
  }

  RegExp toRegex() => RegExp(this);
}

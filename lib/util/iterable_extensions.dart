extension EmptyExtension<T> on Iterable<T> {
  /// Returns null if the Iterable is empty.
  Iterable<T>? nullIfEmpty() => isEmpty ? null : this;
}

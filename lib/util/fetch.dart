import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:http/http.dart' as http;
import 'package:result/result.dart';

sealed class FetchException implements Exception {
  const FetchException(this.url, this.message);

  final String message;
  final String url;

  @override
  String toString() => "Couldn't fetch <$url>: $message";
}

class NoInternetException extends FetchException {
  const NoInternetException(String url) : super(url, 'No internet connection');
}

class RequestException extends FetchException {
  const RequestException(
    super.url,
    super.message, {
    this.stack,
    this.statusCode,
  });

  final StackTrace? stack;
  final int? statusCode;

  @override
  String toString() => "Couldn't fetch <$url>:\n\tStatus code: "
      '$statusCode\n\tMessage: $message';
}

class Response {
  const Response(
    this.body,
    this.statusCode, {
    this.headers = const {},
    this.contentLength,
    this.reasonPhrase,
  });

  factory Response._fromHttpResponse(http.Response response) => Response(
        response.bodyBytes,
        response.statusCode,
        headers: response.headers,
        reasonPhrase: response.reasonPhrase,
        contentLength: response.contentLength,
      );

  final Uint8List body;
  final int statusCode;
  final String? reasonPhrase;
  final int? contentLength;
  final Map<String, String> headers;

  String text([Encoding fallback = latin1]) {
    final charset = (headers['content-type'] ?? headers['Content-Type'])
        ?.split(RegExp(r';\s*'))
        .firstWhere((p) => p.startsWith('charset='))
        .substring(8);
    return _encodingForCharset(charset, fallback).decode(body);
  }

  Encoding _encodingForCharset(String? charset, Encoding fallback) {
    if (charset == null) return fallback;
    return Encoding.getByName(charset) ?? fallback;
  }
}

typedef FetchResult = Result<Response, FetchException>;

/// Fetches the given url by sending a `GET` request to it.
/// If the request causes a [ClientException] it will be returned as an
/// [Err]. **Any other exception will still throw an error**.
Future<FetchResult> fetch(String url, {Map<String, String>? headers}) =>
    InternetConnectionChecker().hasConnection.then((hasConnection) {
      if (!hasConnection) return Err(NoInternetException(url));
      return http
          .get(Uri.parse(url), headers: headers)
          .then<FetchResult>((res) => Ok(Response._fromHttpResponse(res)))
          .onError<http.ClientException>((err, stack) => Err(
                RequestException(url, err.message, stack: stack),
              ));
    });

Future<FetchResult> fetchCached(
  String url, {
  Map<String, String>? headers,
  String? key,
}) =>
    InternetConnectionChecker().hasConnection.then((hasConnection) {
      if (!hasConnection) return Err(NoInternetException(url));
      return DefaultCacheManager()
          .getSingleFile(url, headers: headers, key: key)
          .then<FetchResult>((file) async {
        final res = await file.readAsBytes();
        return Ok(Response(
          res,
          200,
          reasonPhrase: 'OK',
          contentLength: res.lengthInBytes,
        ));
      }).onError<HttpExceptionWithStatus>(
        (err, stack) => Err(RequestException(
          url,
          err.message,
          stack: stack,
          statusCode: err.statusCode,
        )),
      );
    });

import 'package:flutter/material.dart';

class AllButEdgeInsets extends EdgeInsetsDirectional {
  const AllButEdgeInsets(
    double value, {
    double? start,
    double? top,
    double? end,
    double? bottom,
  }) : super.fromSTEB(
          start ?? value,
          top ?? value,
          end ?? value,
          bottom ?? value,
        );
}

package at.flobii.doable_debug

import android.os.Build
import android.os.Bundle
import at.flobii.doable_debug.R
import io.flutter.embedding.android.FlutterFragmentActivity
import android.content.Intent.FLAG_ACTIVITY_NEW_TASK

class MainActivity : FlutterFragmentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        if (intent.getIntExtra("org.chromium.chrome.extra.TASK_ID", -1) == this.taskId) {
            this.finish()
            intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }

        super.onCreate(savedInstanceState)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            window.setDecorFitsSystemWindows(false)
            window.setBackgroundDrawableResource(R.drawable.launch_background)
        }
    }
}

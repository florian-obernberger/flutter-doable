curl \
    -H "Authorization: Token wlu_MBd9PseGw49rjeXJEF1nRrrxaXdZeY6MKZ2N" \
    https://translate.codeberg.org/api/components/doable/values/translations/ |
    jq ".results | map_values({name: .language.name, locale: .language.code, translated: .translated_percent}) | map(select(.translated >= 51)) | sort_by(.translated) | reverse"

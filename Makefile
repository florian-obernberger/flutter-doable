# Dependencies
# ------------
#
# - yq (pip3 install yq)
#   - jq (sudo apt install jq)
#

.PHONY: release clean strings licenses metadata

version := $(shell yq -r .version pubspec.yaml)
version_name := $(shell echo ${version} | cut -d '+' -f 1)
version_code := $(shell echo ${version} | cut -d '+' -f 2)

app_base_name := doable-${version_name}

v7a_release := build/app/outputs/flutter-apk/app-armeabi-v7a-release.apk
arm64_release := build/app/outputs/flutter-apk/app-arm64-v8a-release.apk
x86_release := build/app/outputs/flutter-apk/app-x86_64-release.apk
release := build/app/outputs/flutter-apk/app-release.apk

codeberg := outputs/codeberg
fdroid := outputs/fdroid

col_green := '\033[0;32m'
col_no := '\033[0m'

release:
	flutter clean
	flutter build apk --split-debug-info=stacktraces/${version}-codeberg/ --split-per-abi
	@cp ${v7a_release} ${codeberg}/${app_base_name}-armeabi-v7a.apk
	@cp ${arm64_release} ${codeberg}/${app_base_name}-arm64-v8a.apk
	@cp ${x86_release} ${codeberg}/${app_base_name}-x86_64.apk

	flutter clean
	flutter build apk --split-debug-info=stacktraces/${version}/
	@cp ${release} ${codeberg}/${app_base_name}-universal.apk

	@gpg -a --detach-sign ${codeberg}/${app_base_name}-armeabi-v7a.apk
	@gpg -a --detach-sig ${codeberg}/${app_base_name}-arm64-v8a.apk
	@gpg -a --detach-sign ${codeberg}/${app_base_name}-x86_64.apk
	@gpg -a --detach-sign ${codeberg}/${app_base_name}-universal.apk

	@cp ${codeberg}/${app_base_name}-armeabi-v7a.apk ${fdroid}/${app_base_name}-armeabi-v7a.apk
	@cp ${codeberg}/${app_base_name}-arm64-v8a.apk ${fdroid}/${app_base_name}-arm64-v8a.apk
	@cp ${codeberg}/${app_base_name}-x86_64.apk ${fdroid}/${app_base_name}-x86_64.apk
	@cp ${codeberg}/${app_base_name}-universal.apk ${fdroid}/${app_base_name}-universal.apk
	@cp ${codeberg}/${app_base_name}-universal.apk ${fdroid}/at.flobii.doable_${version_code}.apk

	@echo
	@echo -e ${col_green}✓  Built ${codeberg}/${app_base_name}-armeabi-v7a.apk${col_no}
	@echo -e ${col_green}✓  Built ${codeberg}/${app_base_name}-arm64-v8a.apk${col_no}
	@echo -e ${col_green}✓  Built ${codeberg}/${app_base_name}-x86_64.apk${col_no}
	@echo -e ${col_green}✓  Built ${codeberg}/${app_base_name}-universal.apk${col_no}
	@echo -e ${col_green}✓  Built ${codeberg}/${app_base_name}.apk${col_no}
	@echo -e ${col_green}✓  Built ${fdroid}/${app_base_name}.apk${col_no}

clean:
	flutter clean
	@echo Deleting ${codeberg}...
	@rm ${codeberg}/* -f
	@echo Deleting ${fdroid}...
	@rm ${fdroid}/* -f

strings:
	@git pull weblate main
	@flutter gen-l10n
	@./fetch_translations.sh | ./app-lang-creator -o lib/types/app_language.dart
	@dart fix --apply lib/strings
	@git add lib/strings/* || true
	@git commit lib/strings/* lib/types/app_language.dart -m "feat(strings): fetch latest strings" || true

licenses:
	@flutter pub get
	@dart run flutter_oss_licenses:generate --output lib/resources/oss_licenses.dart --extra font_licenses.json --format
#   @git commit lib/resources/oss_licenses.dart pubspec.lock pubspec.yaml -m "Update dependencies and licenses" || true

metadata:
	@./doable_metadata_creator metadata assets/info/whats_new.txt ${version_code}

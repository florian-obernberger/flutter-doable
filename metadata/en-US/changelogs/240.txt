**Features**

- New "Swipe actions" extension
- Custom icon for hidden lists in the lists drawer and settings
- Fresh and redesigned settings for lists
- Redesigned feedback and bug report system

**Fixes**

- Fixed the null error in the backup and settings view
- Settings can now be imported and will be synced properly
- Navigation bar on Android 8 will now work accordingly (#82)
• Removed unused setting (use 24 hour format).
• Added "about" section.
• Added option to escape any character when Markdown support is enabled.
• Code and Fenced code blocks can now be copied to clipboard via long press.
• Added ability to sync with Nextcloud.
• Delete completed Todos now actually works!
• When completing a Todo a snackbar appears now to allow you to undo your action.
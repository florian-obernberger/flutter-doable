Changes:
- Titles can now be two lines when creating a new Todo.
- When exporting / importing files via Settings / Backup & Sync lists will now also be exported / imported!

Bug fixes:
- You're no longer stuck in the Nextcloud login screen if the folder you have entered doesn't exist.
- Disallow empty Todos being created (including zero width spaces).
- Fixed #5, #23, #24, and #26.
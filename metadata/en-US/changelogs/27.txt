• You can now create a new (starred) Todo via quick actions!
• Added icons to programming languages in fenced code blocks.
• Add a synchronization option for any WebDAV interface.
• Bug fixes including artificial waiting time when logging in to Nextcloud and deleting completed Todos when they shouldn't be deleted.
_Neuheiten_
- Man kann jetzt Text mit Doable teilen und eine neue Aufgabe damit erstellen!
- Doable ist jetzt auf Polnisch verfügbar!
- (Listen) Listen sind jetzt aus der Beta und können zwischen Geräten synchronisiert werden.
- (Markdown) Blockzitate unterstützen jetzt auch Markdown im Inneren.
- (Markdown) Aufgaben werden jetzt besser formatiert, wenn die Aufgabe kollabiert ist.
- (Markdown) Horizontale Linie hinzugefügt (`---` / `___`).

_Änderungen_
- Es gibt eine neue Akzentschriftart!
- Dialoge animieren jetzt von unten nach oben, anstatt von oben nach unten.
- Logs werden jetzt im Temporär Ordner (Cache) gespeichert.

_Behobene Probleme_
- (Aufgaben Suche) Die Suche wird jetzt auf die aktuelle Liste limitiert.
- (Markdown) Inline Code wird jetzt nicht mehr abgeschnitten.
- Runterziehen um zu Aktualisieren ist jetzt auch möglich, wenn alle Aufgaben erledigt sind.
**Neuerungen**

- Neue "Wischgesten" Erweiterung
- Eigene Icons für versteckte Listen sowohl im Drawer als auch den Einstellungen
- Modernisierte Einstellungen für Listen
- Neues Rück- und Bugmeldungs System

**Fixes**

- Den Null Fehler in den Sicherungseinstellungen behoben
- Einstellungen können nun importiert und synchronisiert werden
- Die Navigationsleiste in Android 8 hat jetzt die richtigen Farben (#82)
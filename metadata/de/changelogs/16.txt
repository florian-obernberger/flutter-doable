• Updates für den Login mit Nextcloud.
• In-App-Browser Unterstützung.
• Datum wird markiert, wenn es in der Vergangenheit liegt.
• Zur Zwischenablage hinzugefügt wird jetzt beim kopieren angezeigt, wenn das Gerät nicht Android 13 hat.
• Neue Sortieroption wurde hinzugefügt.
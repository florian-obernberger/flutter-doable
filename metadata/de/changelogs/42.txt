_Änderungen_
- Der Dialog zur Erstellung von Aufgaben wurde überarbeitet.
- Die alte Seitenübergangsanimation ist nicht länger verfügbar.

_Behobene Probleme_
- Man kann keine Aufgaben ohne Title mehr erstellen.
- Die Issues #19, #20, #21 und #22 wurden behoben.
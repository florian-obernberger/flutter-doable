• "True Black" (komplett schwarzer Hintergrund) wurde hinzugefügt.
• Die App ist jetzt in Ukrainisch komplett übersetzt.
• Spanisch, Französisch und Niederländisch sind teilweise unterstützt.
• Doables Mastodon Account ist jetzt in den Einstellungen zu finden!
• QOL Verbesserungen.
• Markdown ist jetzt unter den Erweiterungen zu finden.
• Issue #7 wurde behoben.
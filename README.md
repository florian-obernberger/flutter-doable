<img src="assets/app_icons/app_icon.png" alt="Logo of Doable" title="Logo" width="96">

# Doable

<a rel="me" href="https://floss.social/@doable"><img alt="Follow Doable on https://floss.social/@doable" src="https://img.shields.io/mastodon/follow/109514549599874572?color=ff5544&domain=https%3A%2F%2Ffloss.social&label=%40doable%40floss.social&style=for-the-badge&logo=mastodon&logoColor=ff5544"></a>
[![Translation status](https://img.shields.io/weblate/progress/doable?color=ffdd11&logo=weblate&logoColor=ffdd11&server=https%3A%2F%2Ftranslate.codeberg.org&style=for-the-badge)](https://translate.codeberg.org/engage/doable/)
[![License](https://img.shields.io/badge/License-MIT-44bb55?logo=codeberg&style=for-the-badge&logoColor=44bb55)](./LICENSE)
[![F-Droid version](https://img.shields.io/badge/dynamic/xml?color=5544ff&label=F-Droid&query=%2F%2Ffdroid%2Fapplication%5B%40id%3D%22at.flobii.doable%22%5D%2Fmarketversion&url=https%3A%2F%2Fcodeberg.org%2Fflorian-obernberger%2Ffdroid-repo%2Fraw%2Fbranch%2Fmain%2Frepo%2Findex.xml&style=for-the-badge&logo=fdroid&logoColor=5544ff)](https://www.codeberg.org/florian-obernberger/fdroid-repo)
[![IzzyOnDroid version](https://img.shields.io/endpoint?style=for-the-badge&url=https%3A%2F%2Fapt.izzysoft.de%2Ffdroid%2Fapi%2Fv1%2Fshield%2Fat.flobii.doable&logo=data%3Aimage%2Fpng%3Bbase64%2CiVBORw0KGgoAAAANSUhEUgAAAEsAAABGCAYAAACE0Gk0AAAAAXNSR0IArs4c6QAAAAlwSFlzAAALEwAACxMBAJqcGAAAAAd0SU1FB94DARYXJ6PeJHUAAAAGYktHRAD%2FAP8A%2F6C9p5MAABGmSURBVBgZ7cF5kORnfd%2Fx9%2Bd5nt%2BvZ2Zn9tZqpV0dIFkgiW4khGU5im2hEIKwqbI5TIJBSlQCVziTCkkFW64kLowLGxQlgmAFE4Epm1RkbJdcqsJJKcZY2CAsYU3rsIVWaCXNnnP29PHr3%2FF8szoW945mdmdmpzf%2F%2BPXi762eGIamjWCMg00grgdFfsjaGE8idTjOrIvEgAqjQgSwgro7ygAz9pgxJiEgAypgDCiAkmPMEDAiIQaYIaArMQpEM0yiABIzCglvRnSO%2FSwR2EiT9q8Q%2FxY4F3GMeDmBOJHEy4iX6BLgKCf6jsQeTkJiWRInkPghiRdI%2FAHwdpYInK5mfC%2Fo14BzwTyIDfRL1PV9BphxG7CHITLjPSxDrNdk%2FBdIv4qxFzEsnroiL4mRHcC0hAFiOG6Q%2BDrLCKxV087C%2BCrYPwIzJDaYAcLszTRcZIDEI4ABYjj2SXydFQTWYjK%2BHuO7CANxjNh4AvbRcH%2FCADM%2BbMZuiWEwIJe4mJNwrNZk%2FBDou4jnieEpqetiBpixE%2FivEkNhhoDPcwqO1Zi0TyPdgTCGygD7IkuY8S2GxyQelvjXnII4laZ9CLM7kAwQw2IYsgPU3V4GmPEO4G6G6zUSj3IKjpNp2mXAHQgDxDDJhHETA8wYAX6PITEDM26TeJRVECtpRgfqAjXOjN%2BlrvcwwIwvAjczPAaMSvRZhcCK9GGgxhlhYLyXATEyDtzMcF0p0WeVHMtpxouB2zlTjBtoOGOAxAMMkRl%2FIfEwaxBYjulTCAPEBnrPxBxvH7ubexYv4a7udTwvwP7nLnisn83%2B%2BQfyqjMKZdy56RtnwZcvhaMMgQE957iWNRLLaZoxBJ%2FbOcnrR%2F4bneIp3nT4f%2FOV3Uf5qZEpeuVRetUcebVAt5yhlT1HjCUTtRqv2Pwwe7Y8heNZvnfwYxzu7OfNF98D9FkPM5D4isSNrJFYatI%2BgPgcQzAi%2BNquKynSh%2Fnx2nfwtp1%2BtUC%2FnCWPc%2FRji251iE5xhLycp1ct0KvmqXLj9ef%2BFE8vNtk3%2FR0%2BfFUOzLBOUxJ7WQfHUuK9DMl%2F2jZLWp5FrWb8oP9ZKutg9DDXBxkoJ1oH7yM%2BRNIkMpIKX1vgzw58loP5vbzlonOBGdbLjHexToGlzK5BYqPdtvM53r7pGeBWquoDjIRzyG2RSEakT2SRIs4hV0DMcb4kKGIuUqqAvAMWeGBqgou2jgFdTsaMF0hgxgskPuMc32KdAoOa9jaG4CdrC7wl%2FBVlNYb3CSFup1AXs5ySjCLO04%2BzmMswK3HkiAKnHE9JSmDM76HXy5iuHrO7H3vjE%2B%2B87J6rWcIMJxE5RsJJzHNMjIwABWCcBseJ%2FjlD8Oubv03fFsnjPHk5Rx4XKOICpbXIqxlymyaqQ1QXXBe5HOf6OF%2FiQkGa5oyMVqQJ1FKvmeqhV33v4BdvlmhJtCRaEi3nmJdoSbQk5nmJc2TOUUlETkNgkHE5wgCxQfaGiCsep0hSIMe5hJiXdNs5W3akZPEgRkFUB7Mupj74DFmB1MORIy%2B2BMfYppR%2Bv6LIsb%2Be%2Fu1bgds5gwKDRAKIDXR9%2BFtyFiCOgPqM18a58Wc%2Fz1nbdzG2I%2BcT%2F%2BXdLBSPEulhroepA%2FSQ9ZAKEhmPPzDCBZeI2rY2I7VAWZrmrL2jbQfPG9c5z3KGOE60iQ22VwfIY5ciLpLHRR5pPsnZuwP%2F%2BEP7KXQU50aAgqg2FR2MDMiQy0lrBV%2F59Rztfxd3fiyhNvdGJtx5pKkRkoyf%2BcH4ozRthqbN0LQZmjZD02Zo2gyTNkMzZjTt37FBAkPmLaOoWkg1TAnnvSqAd3zj9wt%2B%2FJqfJKsOUGqOija4HMhAPUwZzmd05uHxud%2Fi6nfm0B9nIr2QvNxPkpT0i3KClRloP3X9BhskcKLIBkupyK2NtxKZp5c7br3zKhK241zBYvUYhc2ByxE5phxThpRRFBU3%2FjL8wW3nkIcd7P7Zip4O4oMRXCBRyoqEMD7CUk27BbOKhruLNQqcqMcGaxFo9Z%2FG6WwqUsy6dNs9XKhw3vBEnK%2BAAikHFcjlGAXOl2zdJW75zTk2MUqHv6Kww3jnwWo8mo2wIrOv0nB%2FzKBJ%2BzGwL%2FC8yfjzwNtouB6rFDhRZIN9%2F4lDD84vzFx1TaNHmnpGg0hCwFdGIGKhxFHhKHGUyJWgEslwOMBB7NPjGSIFZRS9vnFgOmHGHMszQB9lKdndIBDPezPQZtI20VDGKgRONAdcwAZ419hB2ovTfPCi1oHSv%2BOq5r79fOOh%2F8tPv2UnW8Z6OAc%2BQAgFwRvBl8hVOBeRAydDeMwgxooiOooy5f7vZPzNvh6XXelhJyvQDTR0lEGT8XcwzkP8HTMH6tC0c6jrCKcgBk3ah8DuQGK9fmnrET6y9RCZzdEuDvHtffewf3rSfu51H1XXpnnw0cc52H6GQ9NT5HmPzZsLdu0eZfs2MTYaQFBWkXbbmD6cc3QmUpSGq5XgumzeXDA2UbIl1tm9%2FTrO3nY2z%2FVbPGuv4DPV%2B8kiz9LQ%2BQxq2l7gGV4kTmTAYeo6h1MQg5q2CzjMOp3lKh6%2F8EkKy%2BhXs%2FSreWb6f8vT83%2FOK7e8iaxcII9tyrhIqR595pmZ63Dg0Cwzc23avQ6VFdQSx9h4wtYdJemmWYqyot8tKQte5OAfnv8%2B9m5r0CuP0o%2BzHOk16ZevspurO8doKOO4piXAAtgoiJN4lLpew0kEBtV1hKY9AlwOiDW6brRDRU5pHSrrUFqHNNTwLnJg8VG2ju7GyKkspx%2B7lGXB2EjO%2Bec59uwJRKsR8Zh55CArF%2BlWBZZHJEctMcDYufV8zt92Nb14iOgWgS5bRrcytXi%2FaCjjRFcAoyBO4XKa9i3qupYVOF7uXkCsw6G8R7%2FsUFSLVHSItIksIldSVB2q2CdagVEgK5FKnCqcq1Aw8IYLjiSFEAz5jFoikhRGakaaQlKDsXSMrJqmsg4VGbge3mfUQtVnUNM2YzzA6v0DmvF1rMDxcp9knZxS61YHyW2W3OYoNUduh1DSIcY%2BZewSrY%2FRB3JMOVKJVOFUEXxF4o3gjZCUuCRjNIGREailRm3EqKUQAmTlLAWzmOaRb0GYI%2FXjxgns04i1MT3IChxL1dXC7GuAsUZP2xa1sgP0q3mKOI%2FFRfpMEXxBETuURZ9oPSr6RHKgILoC%2BRLnK7wTIYg0GAUz1BJIRyJjo1CrQRIgDcLL07cZqtgiahGFHvgFsKrNcZPxUtD7WCsBTfsgy3AsR%2FqPYGJtbH%2Fu7surRfpxhtxmyd00JYfxoUsRW%2FRjj5KMij4ox5RjLkeuwLkKH8A7wBc4l5EECN4YG3OMjcBITaSpEcnpxxkqNwuuRXRHCWlGjW09jhNfA4z1uZ1lOJZT1yPAfayNgJuj5WTVYQqbphefRKFFSDJK2hSxS2kZZhlRfVCOV4lzFd4b3kMIRmGzhGCEACFA8JGxMcfICCQJRCvIbY5K85hrYa6FnGxr7ZUFz2vaR0GXAmJ9Ak17B0s4VmK8k7UwbqahZ4rYO9AvZymYJucwLmSkSUQho6i6VLFHVIaRYcqRSpwiwUHiPbgeLs3wHryHxIskgZERY7Qm0hSiVRgdnO9TuQVCEN5VCmx9hBd9itNlfJglHCtpuDmgweqUyL7EMYa%2Bn9ssvWoKc12ci4QE0qQkL7tU1sMsw%2BiDClCFDxA8yPcpOIJ3kCQiCZAkRuLBe2N0xEgTkDy4nKJawDnwAgHj4cIZmnYUo8bp%2BzGWcJxMXU3MPokZJxEx9lB3xjHRut%2Ft2zyFzeK94b0jCSKtQV5mlDGjIgMVSBXORZwT8lDEo%2FggggPnjODBBxG8CE6ERNRSYUTK2MW8kECCGMV%2BfjQFdiJOn6jRtDcwwHEqDffLSH%2FIcsw45us0dISXOBud6lbzmEACyXCC4EqK2CGvekTloALnDDmQy8niQQg53hvBQ%2BIhePAyvAfvDe8N542qysGVeBlOII9VpecTnat%2BgY1k9m4GOFajrrdh3MuJDKlNXT%2FNgG8UV%2FQ6vTZVhLJ0xMoRLSJBt1ikjH1eIAcycF165SFwOd5BcOC98F7IQ%2FDgvOE8BG94gUIX1zt3vtuZ6HZaW%2FqzB3cf%2FUHrticeyoKxcQx4AwMCq9XQz9C0LwC38CIBb2eJ24uf%2F9hn06eYmf7vtEcdtaSC%2FhiPHHiWoswoQ4mSBJ%2BA9yXeGc5D8BAceAfeGxI4B8EBDpxAHCPwSvlnr%2F6LayU9xvOa9ibgT9hYQkoZINaqGV%2BH8SDwWzTcv2RQ0z4JfJxjtriS1HqAo15rc8W%2B3SQexsZgx3ZRS40QwHsIDkIA7yF4cA7kIDhwDpxAAgnm2zBzcA%2Fvu2xKHDdpzyH2sNGMWRrawUsCa1V3D9GMF2EcYNBk3AT270EGaCEGYILn7avGuDokSCVgVBUgfkgSkiGBBAgk4ZzhHDhAgjJCvw9FkfBDTfsy2B4QG06IAYH1qLuneLmHQGIZ85XYsmk3re4UMRp5YYxGgTdeIOMFEpLhBA4jRohRxAhFCVkOcy2jLCcqnteM5wE3ggwQG85yBgQ2wmR8C9IlrGAhwsT4O2ln%2F5nKIOtDCEYtQhKgiuAdhAoKB0hIYNGoKqMoIMvF%2FCI8%2BQykc8ldNE3APl4khkIFAwKnq2kpcC%2Bn8Kv2K7w%2F%2BzPa1WOwLYdFo19GUiecM7wHOcM5EGARcoMyFwsLgekjgSLLKQojTdIAXAkkDNcIAwKn71ZWIaPGtpGdjMfX0Jo7xEw1z2zWQ5kjmxnlsutuoIxG8BPM6ULSmR6f%2F8yvsTUbJRSBzmKXSo5zL3a89a27R8EeBDFEBrQZEDgdTXsF8CusQq6Uhx%2Fq8MzUUdqLGVYGLKS4pGLL2QWfSL5Kwd95fe0%2BPv3xJzg4%2FzSt3hylzTM6UuO5g4fJdl14DYghE9i3GRA4PfcCBohTKPFc%2F6OX0XrtFnrVHP1ykf2HnqLdMZzvsYs2U4xz3ES6wyZsl8qxghqBThlodaeRizwb917A0BnHfJUBgfVq2ifBLgWxGiXQCq8k7zxJXvYpqpK8yAiJp%2Bhuoso7kI5z3JO1H9H%2F%2BMN7uWTPFqQu7c4iB6f6NB%2FyXPP%2BLsMnqOseBgTWy%2BzjSKzFX85u4747H%2BD8cyZIreTQoZQjUyVveKvYHA9xiLM57tUTtS%2B%2F55%2FccNNU6%2FscmZlCFOx8ZcVP7K3YNdHnLCs4qoShMb7JEoH1aMZHMNZs10UN3n%2FzK%2Bj1u7Q6C1zQL%2FESlfV4Vf8AT4y8lmMM0F17%2Fe1%2F9IMjN7kyJw0ZZbVILCMKkXayk6NKGCpxF0s41qpp%2FxTjciTW6i%2BT1xJjj5EkEJIcmSjKikhJnm7mJQIu35u6v95SO%2Fv%2FVGXFXOcw47VxLjr3KtIrv8S9536UIetS15dYQqxF00aBLmCAWIdfnL%2BDsHAfR7pTlGXO9rFXc%2BSCj%2FDH4Vpe8l3qupqXmJmueNJ%2B89lC%2F2a24gyxX6Dufo8lxFo07XeBdzMcBhapu8CgyXgF0vc4c1rUtYVlOFaraW8EezfDI9AHWUr6U84ou5wVOFbL7B6QMRwGfJO67mRQ0z6F2VbOnFupu%2BdYgViNpn0BuIVhMvbS0BTHNW0cmAM8IIbKwPSnNHQ9JxE4laZdAtzCcP0iDU1xom8CgTNCkzR0PacgTmYyCmkG2AqI4ZjFbCcNZxzXtGuB%2BzkTzP6GhruUVXCcjHQTsA0Qw1EAF9FwxnFN2wncz5lg9nUa7lJWybGSSRsBfhuMoTG7n7rmOYF9huEyXvQfaLgbWIPAiuxzIA9iCAxjgYa7nkGT8TqMGxFDYIZJiOcwfo6GHmSNAsuZjD%2BBdDNDY0K6iaXE1wADxMYwQGBg7Ef8BnV9nnUKLEfcCxgghuNO6rqHQZN2N7CdDWGAOKYDPAj6AA09xmkSS03G%2F4n0LoYnYraJhss4rmnjwCIbxex%2FIX2Our7JBhJLTcYxpADmMSogQyxDnhelgONFEcyBeIFZCeojY0Cg7roMmozbkByGgRkSmCVIFRA5KQMjBwoko66Cv%2Ff%2F3%2F8DVlJdkNqPmjUAAAAASUVORK5CYII%3D)](https://apt.izzysoft.de/fdroid/index/apk/at.flobii.doable)

At its core Doable is a modern and easy to use Todo app.

It features a beautiful design that follows the latest Material 3 guidelines, a simple user
interface that allows you to manage your tasks with ease and last but not least a seamless
integration with Nextcloud.

When you open Doable for the first time you might be mistaken to think that it lacks some features
you might've been hoping for like separate lists. But fear not! Doable does indeed support such
things! The are just not enabled by default. To enable lists and other amazing features check out
the "Extensions" section in Doable's settings!

Doable also supports integration with Nextcloud which allows you to backup and synchronize your
tasks across multiple devices – or just keep a handy backup of them in case something goes wrong.
Simple log in to your Nextcloud account and boom! Your Todos will now be synchronized automatically.
(You can also use any other backup server that supports the WebDAV interface.)

## Features

### Design and interface

Doable features a simple yet powerful interface in a beautiful design that closely follows
Androids Material 3 guidelines.

<table>
  <tr>
    <td width="25%" height="100%"><img src="metadata/en-US/images/phoneScreenshots/1.png" alt="Introduction to the app" /></td>
    <td width="25%" height="100%"><img src="metadata/en-US/images/phoneScreenshots/2.png" alt="Adding a new task" /></td>
    <td width="25%" height="100%"><img src="metadata/en-US/images/phoneScreenshots/3.png" alt="A task being selected" /></td>
    <td width="25%" height="100%"><img src="metadata/en-US/images/phoneScreenshots/4.png" alt="Detail view of a task with Markdown" /></td>
  </tr>
  <tr>
    <td width="25%">Introduction to the app</td>
    <td width="25%">Adding a new task</td>
    <td width="25%">A task being selected</td>
    <td width="25%">Detail view of a task with Markdown</td>
  </tr>
    <tr>
    <td width="25%" height="100%"><img src="metadata/en-US/images/phoneScreenshots/5.png" alt="Adding a due date and time to a task" /></td>
    <td width="25%" height="100%"><img src="metadata/en-US/images/phoneScreenshots/6.png" alt="Creating a new list" /></td>
    <td width="25%" height="100%"><img src="metadata/en-US/images/phoneScreenshots/7.png" alt="Adding a task to an already existing list" /></td>
    <td width="25%" height="100%"><img src="metadata/en-US/images/phoneScreenshots/8.png" alt="Selecting which list a task belongs to" /></td>
  </tr>
  <tr>
    <td width="25%">Adding a due date and time to a task</td>
    <td width="25%">Creating a new list</td>
    <td width="25%">Adding a task to an already existing list</td>
    <td width="25%">Selecting which list a task belongs to</td>
  </tr>
</table>

### Settings and customization

  You can customize many aspects of Doable including

- How tasks will be sorted
- The colors of the app
- How to handle certain cases as tasks being overdue or being due today
- ...

### Extensions

Doable allows you to further customize your experience by enabling certain extensions.
Extensions can change the behavior of the app and allow you to user features that are not
included out of the box. The current extensions consist of the following:

- **Lists**

  Create lists and sort your Todos with them! You can create as many lists as you'd like to manage
  all of your task needs!

- **Notifications (beta)**

  Doable will notify you when your tasks are due so you never forget that important todo again.
  A long awaited feature that is now finally in beta and you can try it out for yourself!

- **Recurring Todos (beta)**

  Another long awaited feature is finally here! You can now finally create Sisyphus tasks and
  complete the same task every day / week / month / year!

  *In a future update you will also be able to customize those options and choose exactly how and
  when you want your tasks to repeat.*

- **Markdown**

  One of the most important features in any app that supports text input: Markdown! You can use
  all your favorite styling options to add flavor to your todos. While Doable does not support the
  entire syntax, it supports most of it. Here is a complete list:

  | Feature                                        | Code                         |
  | :--------------------------------------------- | :--------------------------- |
  | **Bold**                                       | `**Bold**`                   |
  | *Italic*                                       | `*Italic*`                   |
  | <u>Underline</u>                               | `__Underline__`              |
  | <blockquote>Blockquote</blockquote>            | `> Blockquote`               |
  | Ordered / Unordered lists                      | `- ...` / `1. ...`           |
  | <input type="checkbox" disabled=""> Task lists | `- [ ] To do` / `- [x] Done` |
  | [Links](https://doable.at)                     | `[Links](https://doable.at)` |
  | <mark>Highlighting</mark>                      | `==Highlighting==`           |
  | ~~Strikethrough~~                              | `~~Strikethrough~~`          |

- **Images (beta)**

  Want to add additional context to your tasks? Then this extension is for you; allowing you to add
  an image to every task!

  *Unfortunately images will not yet be synchronized and are unique to each device.*

- **Swipe actions**

  We all love to swipe, swipe, swipe! So with this extension you can swipe your tasks around to
  perform different actions of your choosing! You can star / unstar a task, delete it or simply
  compelte / uncomplete it!

- **Progress bar**

  Need more fine grained control over the completion state of your task? This lovely extension
  adds an individual progess bar to each and every task so you can track any and all progress. 

- **Reader mode**

  With this extension you can switch between edit and reader mode when opening a task. This allows
  for a better overview over larger tasks as well as giving you the option to simply admire what
  you've wrote.

- **Todo search**

  Find yourself searching for that one specific todo you still haven't done? Well then search might
  be for you! Simply search for part of the todo to find it.

### Nextcloud integration

You can log into your Nextcloud account to automatically synchronize your tasks across devices.

## Installation

### From a release

You can simply download the latest release apk from the [releases][releases] page and install it!
You might have to go into the settings and allow installing apps from an external source tho.

### F-Droid

To install this app via F-Droid simply add my repository to the app
and search for "Doable"!

You can find the repository **[here][f-droid]**.

### Build it yourself

Since this is open and free software you can also just compile the program yourself! To do that
you simply need to be on the latest flutter master branch – or any other branch, assuming it
includes all the features used here.

You can switch the channel flutter uses with the following two commands.

```bash
flutter channel master
flutter upgrade
```

After that you can simply run `flutter build apk` to generate an apk ready to be installed on your
phone.

**Be careful if you choose the `main` branch** for compiling your apk, be aware that the app name
and its android id will be different from the ones you would get from the `stable` branch. Since I
use the `main` as my default branch and to test my app, I changed the app name / label to
"Doable DEBUG" and the id to `at.flobii.doable_debug`. If you want to change those, either compile
from the stable branch or simply change them. Below you can find a table detailing what needs to be
changed.

| File: `line(s)`                                                              | What you need to change                                                  |
| ---------------------------------------------------------------------------- | ------------------------------------------------------------------------ |
| [android/app/build.gradle][build.gradle]: `53`                               | `at.flobii.doable_debug` → `at.flobii.doable`                            |
| [android/app/src/main/AndroidManifest.xml][main.man]: `1, 20`                | `at.flobii.doable_debug` → `at.flobii.doable`, "Doable DEBUG" → "Doable" |
| [android/app/src/debug/AndroidManifest.xml][debug.man]: `1`                  | `at.flobii.doable_debug` → `at.flobii.doable`                            |
| [android/app/src/profile/AndroidManifest.xml][profile.man]: `1`              | `at.flobii.doable_debug` → `at.flobii.doable`                            |
| [android/app/src/main/kotlin/at/flobii/doable/MainActivity.kt][main.kt]: `1` | `at.flobii.doable_debug` → `at.flobii.doable`                            |

Besides the name changes you should also consider **updating the version name and code** in the
[`pubspec.yaml`][pubspec] file, since those are also only changed on the `stable` branch.

## Contribute

You want to contribute to Doable? Great! You don't know how you can help? Well, here are your
options!

### Create an issue

The simplest way to support and help Doable is by [creating an issue][new-issue] whenever you have an
idea / feature request or you want to provide feedback or maybe you've found a bug / problem!

### Translating

If you speak a language Doable is not yet translated into you can help by translating it! Simply
follow [this link to Doable's Weblate page][weblate] to find out more!

#### Translation status

[![Translation status](https://translate.codeberg.org/widgets/doable/-/values/horizontal-auto.svg 'Translation statistics')](https://translate.codeberg.org/engage/doable/)

### Pull requests

PRs are also appreciated! If you do make one, please provide a description why this PR is necessary
so that I can understand its purpose quicker! And thanks in advance for spending your time on
contributing!

### `CONTRIBUTORS` file

The [`CONTRIBUTORS`][contributors] file features a (hopefully) complete list of all the people that
contributed to Doable. If you feel like you should be included you can either:

- Open an issue
- Make a PR
- Write me a message on Doable's Mastodon account

[debug.man]: android/app/src/debug/AndroidManifest.xml
[main.man]: android/app/src/main/AndroidManifest.xml
[profile.man]: android/app/src/profile/AndroidManifest.xml
[main.kt]: android/app/src/main/kotlin/at/flobii/doable/MainActivity.kt
[build.gradle]: android/app/build.gradle
[pubspec]: pubspec.yaml
[f-droid]: https://codeberg.org/florian-obernberger/fdroid-repo
[releases]: https://codeberg.org/florian-obernberger/flutter-doable/releases
[new-issue]: https://codeberg.org/florian-obernberger/flutter-doable/issues/new/choose
[weblate]: https://translate.codeberg.org/engage/doable/
[contributors]: CONTRIBUTORS

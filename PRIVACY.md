# Privacy Policy

## Definitions

For the purposes of this privacy policy:

- **Doable** refers to Doable, the application this privacy policy is for.
- **I / the developer / Florian** refers to the developer of Doable, Florian Obernberger.
- **You / the user** any user of Doable.
- **Device / phone** the device of a user.
- **Extension** a part of Doable that extends its base functionality and can be turned on or off.
- **Logs** a piece of text with additional information that is created by Doable to allow the developer to better understand what went wrong / what happened.

## Statement

I am not interested in collecting any of your personal data or information. Because of that Doable does not collect, store or transmit any data. Neither does Doable include any advertising or analytics software that talks to third parties.

## What Information Does Doable Collect?

As stated above Doable does not collect any information about you, your personal interests, your data, etc.

## Crash Reports, Logging

To allow me to create a better user experience and fix any issues or problems Doable provides the option to send crash reports to the developer. In those crash reports the user has the option to include information about the device as well as any logs that Doable has written. While those options are enabled by default, the user is always free to refuse to share this information. Here it is important to mention, that **Doable does not send any crash reports automatically.**

### What Information Is Included In The Aforementioned Crash Reports?

When the user agrees to share information about their device with the developer the following information will be gathered:

- The Android version the device
- The Software development kit (SDK) version
- The codename for the SDK version
- The brand of the device
- The name of the device model
- The language the device is set to

Additionally the user can share the settings they set inside of Doable. This includes information such as which extensions are turned on or which date format they selected.

### Where Is This Information Being Sent?

The information provided by the user is either sent to the developer via E-Mail or is posted as a publicly visible issue to [Codeberg.org](https://codeberg.org).

You can find their privacy policy [here][codeberg].

### Do Logs Contain Personal Information?

No, logs do not contain any personal information. The logs only contain information about the error that occurred, as well as a message defined by the developer. The user can also always check out each individual log that has been created inside the app. Additionally they can delete any individual log or delete all of them at once.

## Nextcloud Integration

As part of Doable's functionality the application provides a way to synchronize the tasks stored inside it with a Nextcloud instance. To provide this service Doable stores credentials used to authenticate the the Nextcloud instance. This information is **only stored on the device** and is never and under no circumstances shared with anyone. The user can at any point revoke those credentials either by deleting them from Nextcloud's end or by logging out of Nextcloud inside of Doable, which will delete the information on the device and delete the access token from the Nextcloud instance. On Android this login information is stored in the devices KeyStore. You can find out more about this native Android feature [here][keystore].

This also means that the instance that the user chose has access to their tasks and their content. Doable does not send any information that is not required to be sent to any Nextcloud server. Doable is also not responsible for the specific Nextcloud instance the user chooses to synchronize with. For more information you need to ask the maintainer of your Nextcloud instance.

## WebDAV Integration

Similar to the [Nextcloud Integration](#nextcloud-integration) mentioned above, Doable allows the user to synchronize with any WebDAV interface the user provides. Here the same rules apply: Doable stores any provided login information in Android's [KeyStore][keystore] and allows the user at any point to delete this information from Doable. Again, Doable does not have any influence or control over how the server behind the WebDAV interface stores and handles the content uploaded by Doable.

## Changes To This Privacy Policy

I may change and adapt this privacy policy from time to time to make sure it is always on the latest adn most correct stand point. This version was originally written on 18th of December 2023 and has not been updated yet. You can also see any published updates and historic versions in [Doable's repository][repo].

## Final Words And Contact Information

If you find any violations of this privacy policy from my / Doable's end, please notify the developer immediately, as this is not intentional and will be fixed as soon as possible.

Please keep in mind, that Doable is currently developed by a single person (me). While I try my best to make sure everything is in order and nothing goes wrong, I am only human and humans do make mistakes from time to time.

If you have any questions, concerns or suggestions, you can contact me any of the following ways:

- Write me an email at [doable@doable.at][contact]
- Write me a (private) message on [Doable's Mastodon profile][mastodon]
- Open an issue on [Doable's Codeberg repository][repo]

If you have any questions or concerns, please feel free to contact us.

[codeberg]: https://codeberg.org/Codeberg/org/src/branch/main/PrivacyPolicy.md "Codeberg.org's Privacy Policy"
[contact]: mailto:Doable<doable@doable.at> "Doable <doable@doable.at>"
[mastodon]: https://floss.social/@doable "@doable@floss.social"
[repo]: https://codeberg.org/florian-obernberger/flutter-doable "Doable's repository on Codeberg.org"
[keystore]: https://developer.android.com/privacy-and-security/keystore "Android KeyStore Documentation"

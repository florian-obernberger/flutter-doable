#!/bin/env python3

from sys import argv
from os.path import basename
from subprocess import run, PIPE


def bash(cmd: str) -> str:
    return run(cmd, stdin=PIPE, stdout=PIPE, universal_newlines=True, shell=True).stdout


def main() -> None:
    args = argv[1:]

    sort: bool = "-s" in args or "--sort" in args

    if sort:
        if "-s" in args: args.remove("-s")
        else: args.remove("--sort")

    if len(args) == 0:
        print("Please supply at least one apk!")
        exit(1)

    apks: dict[str, int] = {}

    for apk in args:
        if not apk.endswith("apk"):
            print(f"{apk} is not an apk, skipping.")
            continue

        res: str = bash(f"aapt dump badging {apk}")
        info = res.split("\n")[0]
        info = info.removeprefix("package: ")
        attrs = {i.split("=")[0]: i.split("=")[1] for i in info.split(" ")}

        apks[basename(apk)] = int(attrs["versionCode"].strip("'"))

    if sort:
        apks = dict(sorted(apks.items(), key=lambda apk: apk[1]))

    for (apk, versionCode) in apks.items():
        print(f"""{apk} -> {versionCode}""")


if __name__ == "__main__":
    main()

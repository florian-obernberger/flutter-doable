import 'dart:io';

import 'package:flutter_test/flutter_test.dart';
import 'package:doable/resources/resources.dart';

void main() {
  test('pride assets test', () {
    expect(File(Pride.disabilityPrideBanner).existsSync(), isTrue);
    expect(File(Pride.prideBanner).existsSync(), isTrue);
  });
}

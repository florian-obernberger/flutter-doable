import 'dart:io';

import 'package:flutter_test/flutter_test.dart';
import 'package:doable/resources/resources.dart';

void main() {
  test('dynamic_images assets test', () {
    expect(File(DynamicImages.m0).existsSync(), isTrue);
    expect(File(DynamicImages.m1020).existsSync(), isTrue);
    expect(File(DynamicImages.m1080).existsSync(), isTrue);
    expect(File(DynamicImages.m1140).existsSync(), isTrue);
    expect(File(DynamicImages.m1200).existsSync(), isTrue);
    expect(File(DynamicImages.m390).existsSync(), isTrue);
    expect(File(DynamicImages.m450).existsSync(), isTrue);
    expect(File(DynamicImages.m480).existsSync(), isTrue);
    expect(File(DynamicImages.m510).existsSync(), isTrue);
    expect(File(DynamicImages.m540).existsSync(), isTrue);
    expect(File(DynamicImages.m600).existsSync(), isTrue);
    expect(File(DynamicImages.m660).existsSync(), isTrue);
    expect(File(DynamicImages.m720).existsSync(), isTrue);
    expect(File(DynamicImages.m900).existsSync(), isTrue);
    expect(File(DynamicImages.m930).existsSync(), isTrue);
    expect(File(DynamicImages.m960).existsSync(), isTrue);
  });
}

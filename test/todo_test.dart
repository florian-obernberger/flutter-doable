import 'package:flutter_test/flutter_test.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:doable/state/settings.dart';
import 'package:doable/util/extensions/todo_importance.dart';
import 'package:doable/data/todo.dart';

class Testable<T, S> {
  const Testable(this.result, this.expected);

  final T result;
  final S expected;
}

void main() async {
  final settings = GeneralPreferences();

  TestWidgetsFlutterBinding.ensureInitialized();

  SharedPreferences.setMockInitialValues(<String, Object>{
    settings.highlightToday.key: settings.highlightToday.initialValue,
    settings.highlightOverdue.key: settings.highlightOverdue.initialValue,
  });

  await settings.initialize(SharedPreferences.getInstance());

  group("TodoSorter.isTodoImportant", () {
    test("today with highlightToday set to true", () {
      final todo = Todo(
        creationDate: DateTime.now(),
        title: "today",
        lastModified: DateTime.now(),
      );
      settings.highlightToday.value = true;

      final res =
          TodoImportanceExtension.todoImportance(todo, settings).toBool();
      expect(res, true);
    });

    test("today with highlightToday set to false", () {
      final todo = Todo(
        creationDate: DateTime.now(),
        title: "today",
        lastModified: DateTime.now(),
      );
      settings.highlightToday.value = false;

      final res =
          TodoImportanceExtension.todoImportance(todo, settings).toBool();
      expect(res, false);
    });

    test("yesterday with highlightOverdue set to true", () {
      final now = DateTime.now();
      final todo = Todo(
        creationDate: now.copyWith(day: now.day - 1),
        title: "yesterday",
        lastModified: now,
      );
      settings.highlightOverdue.value = true;

      final res =
          TodoImportanceExtension.todoImportance(todo, settings).toBool();
      expect(res, true);
    });

    test("yesterday with highlightOverdue set to false", () {
      final now = DateTime.now();
      final todo = Todo(
        creationDate: now.copyWith(day: now.day - 1),
        title: "yesterday",
        lastModified: now,
      );
      settings.highlightOverdue.value = false;

      final res =
          TodoImportanceExtension.todoImportance(todo, settings).toBool();
      expect(res, false);
    });

    test("day in the future, isImportant set to false", () {
      final now = DateTime.now();
      final todo = Todo(
        creationDate: now.copyWith(day: now.day + 10),
        title: "day in future",
        isImportant: false,
        lastModified: now,
      );

      final res =
          TodoImportanceExtension.todoImportance(todo, settings).toBool();
      expect(res, false);
    });

    test("day in the future, isImportant set to true", () {
      final now = DateTime.now();
      final todo = Todo(
        creationDate: now.copyWith(day: now.day + 10),
        title: "day in future",
        isImportant: true,
        lastModified: now,
      );

      final res =
          TodoImportanceExtension.todoImportance(todo, settings).toBool();
      expect(res, true);
    });
  });
}

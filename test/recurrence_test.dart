import 'package:doable/state/todo_db.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:rrule/rrule.dart';

void main() async {
  final rrule = RecurrenceRule(
    frequency: Frequency.daily,
    interval: 2,
  );

  const regularId = 'eafeb9a5-f47c-4706-9f26-e463823ef612';
  const corruptRawId = 'e4f2690d-3e0b-402d-90c0-b3462830a72d';
  const corruptId = '$corruptRawId'
      '-recurr-FREQ=DAILY-0'
      '-recurr-FREQ=DAILY-1'
      '-recurr-FREQ=DAILY-1'
      '-recurr-FREQ=DAILY-1'
      '-recurr-FREQ=DAILY-1'
      '-recurr-FREQ=DAILY-1'
      '-recurr-FREQ=DAILY-1'
      '-recurr-FREQ=DAILY-1'
      '-recurr-FREQ=DAILY-1'
      '-recurr-FREQ=DAILY-1'
      '-recurr-FREQ=DAILY-1'
      '-recurr-FREQ=DAILY-1'
      '-recurr-FREQ=DAILY-';

  for (int step = 1; step < 4; step++) {
    test('id = $regularId, step = $step', () {
      final expected = '$regularId;rrule;$step;DAILY;2';
      final next = TodoDatabase.getNextId(regularId, rrule, step);

      expect(next, expected);
    });
  }

  for (int step = 1; step < 4; step++) {
    test('id = $corruptId, step = $step', () {
      final expected = '$corruptRawId;rrule;$step;DAILY;2';
      final next = TodoDatabase.getNextId(corruptRawId, rrule, step);

      expect(next, expected);
    });
  }
}

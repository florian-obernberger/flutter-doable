import 'dart:io';

import 'package:flutter_test/flutter_test.dart';
import 'package:doable/resources/resources.dart';

void main() {
  test('svg_icons assets test', () {
    expect(File(SvgIcons.editNoteW400).existsSync(), isTrue);
    expect(File(SvgIcons.listHiddenW300).existsSync(), isTrue);
    expect(File(SvgIcons.listHiddenW400).existsSync(), isTrue);
    expect(File(SvgIcons.listW300).existsSync(), isTrue);
    expect(File(SvgIcons.listW400).existsSync(), isTrue);
    expect(File(SvgIcons.removeNoteW400).existsSync(), isTrue);
    expect(File(SvgIcons.sortAscendingW400).existsSync(), isTrue);
    expect(File(SvgIcons.sortDescendingW400).existsSync(), isTrue);
    expect(File(SvgIcons.undoDoneW500).existsSync(), isTrue);
  });
}

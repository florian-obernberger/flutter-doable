import 'dart:io';

import 'package:flutter_test/flutter_test.dart';
import 'package:doable/resources/resources.dart';

void main() {
  test('signing_keys assets test', () {
    expect(File(SigningKeys.florianObernbergerPubkey).existsSync(), isTrue);
  });
}
